﻿namespace App.Utility.Constants
{
    public static class CustomDocumentFieldTypes
    {
        public const string Location = "LOCATION";
        public const string DegreeDetails = "DEGREES";
        public const string Institutions = "INSTITUT";
    }
}