﻿namespace App.Utility.Constants
{
    public static class UserRoles
    {
        public const string Admin = "Admin";
        public const string User = "User";
    }

    public enum ContentType : short
    {
        Experience = 1,
        Degree = 2,
        Education = 3,
        Skills = 4,
        Summary = 4,
    }

    public enum SourceType : short
    {
        ONET = 1,
        GENERIC = 2,
        CUSTOM = 3,
        USERGENERATED = 4
    }

    public static class Pages
    {
        public const string Home = "HOME";
        public const string Journey = "JOURNY";
        public const string Contact = "USER";
        public const string Template = "FORMAT";
        public const string Academics = "ACAD";
        public const string AcademicList = "ACADS";
        public const string Experience = "EXPR";
        public const string ExperienceList = "EXPRS";
        public const string Competencies = "COMPT";
        public const string Summary = "SUMMRY";
        public const string Preview = "PRVW";
        public const string Dashboard = "DASHBD";
        public const string CustomSection = "CSTM";
        public const string Plans = "PLANS";
        public const string Checkout = "CHECK";
        public const string Premium = "PREM";
    }

    public static class DocumentFieldTypes
    {
        public const string Name = "NAME";
        public const string Email = "EMAIL"; 
        public const string Address = "ADDRESS";
        public const string Description = "DESCRP";
        public const string AlternatePhone = "ALTPHONE";
        public const string CertificationName = "CERTNAME";
        public const string CGPA = "CGPA";
        public const string City = "CITY"; 
        public const string State = "STATE"; 
        public const string ZipCode = "ZIPCD";
        public const string CompanyLocation = "COMPLOC";
        public const string CompanyName = "COMPNAME";
        public const string CellPhone = "CELLPH";
        public const string CareerField = "CARFIELD";
        public const string CareerLevel = "CARLVL";
        public const string StartMonth = "STMONTH";
        public const string StartYear = "STYEAR";
        public const string EndMonth = "ENDMONTH";
        public const string EndYear = "ENDYEAR";
        public const string Degree = "DEGREE";
        public const string DegreeSpecialization = "DEGSPC";
        public const string EmploymentType = "EMPTYPE";
        public const string FirstName = "FNAME";
        public const string From = "FROM";
        public const string Headline = "HEADLN";
        public const string HighestEducation = "HIGHEDU";
        public const string HomePhone = "HOMEPH";
        public const string InstitutionName = "INTSNAME";
        public const string JobTitle = "JOBTIT";
        public const string JobType = "JOBTP";
        public const string LastName = "LNAME";
        public const string ProficiencyLevel = "PROFLVL";
        public const string SchoolName = "SCHNAME";
        public const string SchoolLocation = "SCHLOC";
        public const string SkillName = "SKLNAME";
        public const string SkillLevel = "SKLLVL";
        public const string PreferredSpeciality = "PRFDSPC";
        public const string FieldOfStudy = "FLDSTD";
        public const string University = "UNIV";
        public const string WebsiteOrPortfolio = "WEBPORT";
    }

    public static class SectionTypes
    {
        public const string Accomplishments = "ACCM";
        public const string AdditionalInformation = "ADDI";
        public const string Affiliations = "AFIL";
        public const string Awards = "AWAR";
        public const string Certifications = "CERT";
        public const string Contact = "CNTC";
        public const string Dissertation = "DIST";
        public const string Education = "EDUC";
        public const string ExtraCurricular = "EXCL";
        public const string Experience = "EXPR";
        public const string Skills = "SKILL";
        public const string Interests = "INTR";
        public const string Language = "LANG";
        public const string Name = "NAME";
        public const string Other = "OTHR";
        public const string PersonalInformation = "PRIN";
        public const string Presentations = "PRSN";
        public const string Portfolio = "PTFL";
        public const string Publication = "PUBL";
        public const string References = "REFE";
        public const string Summary = "SUMM";
        public const string Training = "TRNG";
        public const string Volunteer = "VLTR";
        public const string CommunityService = "VLWK";
    }
    
    public static class DocumentTypes
    {
        public const string Resume = "RSME";
        public const string Letter = "LETR";
    }

    public static class EventType
    {
        /// <summary>
        /// Save the data for the end user's action on different buttons
        /// </summary>
        public static string Navigation = "NAVIG";
        public static string Email_Guid = "EMLGD";
        public static string PaymentAction = "PAYMT";
        public static string PrintAction = "PRINT";
        public static string DownloadAction = "DOWLD";
        public static string EmailAction = "EMAIL";
        public static string PasswordEmailGuid = "PWDGD";
        public static string WelcomeBackFlow = "WELCB";
    }
}