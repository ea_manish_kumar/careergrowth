﻿using System.Collections.Generic;
using System.Linq;

namespace App.Utility.Extensions
{
    public static class LinqExtensions
    {
        public static bool IsNotNullOrEmpty<T>(this IEnumerable<T> source)
        {
            return source != null && source.Any();
        }

        public static bool IsNullOrEmpty<T>(this IEnumerable<T> source)
        {
            return source == null || !source.Any();
        }
    }
}