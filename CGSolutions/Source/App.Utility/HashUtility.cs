﻿using System;
using System.Security.Cryptography;

namespace App.Utility
{
    public static class HashUtility
    {
        private const int SALT_BYTES = 24;
        private const int HASH_BYTES = 24;
        private const int PBKDF2_ITERATIONS = 10000;
        private const int TOKEN_ITERATIONS = 1000;
        private const int ITERATION_INDEX = 0;
        private const int SALT_INDEX = 1;
        private const int PBKDF2_INDEX = 2;
        private const int TOKEN_INDEX = 2;

        public static string HashPassword(string password)
        {
            if (!string.IsNullOrEmpty(password))
            {
                var salt = GenerateSalt();
                if (salt != null)
                {
                    var hash = PBKDF2(password, salt, PBKDF2_ITERATIONS, HASH_BYTES);
                    return string.Join(":", PBKDF2_ITERATIONS.ToString(), Convert.ToBase64String(salt), Convert.ToBase64String(hash));
                }
            }

            return null;
        }

        public static bool ValidatePassword(string password, string goodHash)
        {
            char[] delimiter = { ':' };
            var split = goodHash.Split(delimiter);
            var iterations = int.Parse(split[ITERATION_INDEX]);
            byte[] salt = Convert.FromBase64String(split[SALT_INDEX]);
            byte[] hash = Convert.FromBase64String(split[PBKDF2_INDEX]);
            byte[] testHash = PBKDF2(password, salt, iterations, hash.Length);

            return SlowEquals(hash, testHash);
        }

        public static string HashToken(string token)
        {
            if (!string.IsNullOrEmpty(token))
            {
                var salt = GenerateSalt();
                if (salt != null)
                {
                    var hash = PBKDF2(token, salt, TOKEN_ITERATIONS, HASH_BYTES);
                    return string.Join(":", TOKEN_ITERATIONS.ToString(), Convert.ToBase64String(salt), Convert.ToBase64String(hash));
                }
            }

            return null;
        }

        public static bool ValidateToken(string token, string goodHash)
        {
            char[] delimiter = { ':' };
            var split = goodHash.Split(delimiter);
            var iterations = int.Parse(split[ITERATION_INDEX]);
            byte[] salt = Convert.FromBase64String(split[SALT_INDEX]);
            byte[] hash = Convert.FromBase64String(split[TOKEN_INDEX]);
            byte[] testHash = PBKDF2(token, salt, iterations, hash.Length);

            return SlowEquals(hash, testHash);
        }

        private static byte[] GenerateSalt()
        {
            byte[] salt = new byte[SALT_BYTES];
            // Create a new instance of the RNGCryptoServiceProvider.
            var rngCsp = new RNGCryptoServiceProvider();
            // Fill the array with random values.
            rngCsp.GetBytes(salt);

            return salt;
        }

        private static byte[] PBKDF2(string password, byte[] salt, int iterations, int outputBytes)
        {
            var pbkdf2 = new Rfc2898DeriveBytes(password, salt)
            {
                IterationCount = iterations
            };
            return pbkdf2.GetBytes(outputBytes);
        }

        private static bool SlowEquals(byte[] a, byte[] b)
        {
            var diff = (uint)a.Length ^ (uint)b.Length;
            for (var i = 0; i < a.Length && i < b.Length; i++)
                diff |= (uint)(a[i] ^ b[i]);
            return diff == 0;
        }
    }
}