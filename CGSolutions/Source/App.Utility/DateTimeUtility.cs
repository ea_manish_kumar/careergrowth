﻿using System;

namespace App.Utility
{
    public static class DateTimeUtility
    {
        public static string GetFormattedTimePeriod(TimeSpan timespan)
        {
            var formattedPeriod = new System.Text.StringBuilder();
            if (timespan.Days == 1)
                formattedPeriod.AppendFormat("{0} day ", timespan.Days);
            else if (timespan.Days > 1)
                formattedPeriod.AppendFormat("{0} days ", timespan.Days);
            if (timespan.Hours == 1)
                formattedPeriod.AppendFormat("{0} hour ", timespan.Hours);
            else if (timespan.Hours > 1)
                formattedPeriod.AppendFormat("{0} hours ", timespan.Hours);
            if (timespan.Minutes == 1)
                formattedPeriod.AppendFormat("{0} minute ", timespan.Minutes);
            else if (timespan.Minutes > 1)
                formattedPeriod.AppendFormat("{0} minutes ", timespan.Minutes);
            return formattedPeriod.ToString().Trim();
        }
    }
}