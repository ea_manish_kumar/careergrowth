﻿using System.Runtime.Caching;

namespace App.Utility
{
    public class CacheUtility
    {
        private static readonly MemoryCache CacheStore = MemoryCache.Default;

        public static T Get<T>(string key)
        {
            if (CacheStore.Get(key) is T data)
                return data;
            return default;
        }

        public static void Set<T>(string key, T data)
        {
            CacheStore.Set(key, data, ObjectCache.InfiniteAbsoluteExpiration);
        }

        public static void Remove(string key)
        {
            CacheStore.Remove(key);
        }
    }
}