﻿using System.Text;

namespace DomainModel.Content
{
    public class OnetJobTitleSocCode : EntityBase<int, OnetJobTitleSocCode>
    {
        #region ToString() Implementation

        /// <summary>
        ///     This methods returns a string representation of this class.
        /// </summary>
        /// <returns>A string representation of this class.</returns>
        public new virtual string ToString()
        {
            var sb = new StringBuilder();

            sb.AppendLine("------------ OnetJobTitleSocCode dump starts---------------\n");
            sb.AppendLine(base.ToString());
            sb.AppendLine("OnetJobTitleID: " + OnetJobTitleID);
            sb.AppendLine("OnetSocCode: " + (OnetSocCode != null ? OnetSocCode : "<NULL>"));
            sb.AppendLine("SortIndex: " + (SortIndex != null ? SortIndex : "<NULL>"));
            sb.AppendLine("RelevanceScore: " + RelevanceScore);
            sb.AppendLine("------------ OnetJobTitleSocCode dump ends---------------\n");

            return sb.ToString();
        }

        #endregion ToString() Implementation

        #region Property Declarations

        public virtual int OnetJobTitleID { get; set; }
        public virtual string OnetSocCode { get; set; }
        public virtual string SortIndex { get; set; }
        public virtual short RelevanceScore { get; set; }

        #endregion Property Declarations
    }
}