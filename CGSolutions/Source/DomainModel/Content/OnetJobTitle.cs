﻿using System.Text;

namespace DomainModel.Content
{
    public class OnetJobTitle : EntityBase<int, OnetJobTitle>
    {
        #region ToString() Implementation

        /// <summary>
        ///     This methods returns a string representation of this class.
        /// </summary>
        /// <returns>A string representation of this class.</returns>
        public new virtual string ToString()
        {
            var sb = new StringBuilder();

            sb.AppendLine("------------ OnetJobTitle dump starts---------------\n");
            sb.AppendLine(base.ToString());
            sb.AppendLine("OnetJobTitleID: " + OnetJobTitleID);
            sb.AppendLine("JobTitle: " + (JobTitle != null ? JobTitle : "<NULL>"));
            sb.AppendLine("IsCustom: " + IsCustom);
            sb.AppendLine("------------ OnetJobTitle dump ends---------------\n");

            return sb.ToString();
        }

        #endregion ToString() Implementation

        #region Property Declarations

        public virtual int OnetJobTitleID { get; set; }
        public virtual string JobTitle { get; set; }
        public virtual bool IsCustom { get; set; }

        #endregion Property Declarations
    }
}