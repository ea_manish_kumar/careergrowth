using System;
using System.Text;

namespace DomainModel.Content
{
    [Serializable]
    public class Text : EntityBase<int, Text>
    {
        #region ToString() Implementation

        /// <summary>
        ///     This methods returns a string representation of this class.
        /// </summary>
        /// <returns>A string representation of this class.</returns>
        public new virtual string ToString()
        {
            var sb = new StringBuilder(5);

            sb.AppendLine("------------Text dump starts---------------");
            sb.AppendLine("Id: " + base.ToString());
            sb.AppendLine("Description: " + (Description != null ? Description : "<NULL>"));
            sb.AppendLine("TextTypeID: " + (TextTypeID != null ? TextTypeID.ToString() : "<NULL>"));
            if (DefaultText != null)
                sb.AppendLine("DefaultText: " + DefaultText);

            sb.AppendLine("LanguageText: " + (LanguageText != null ? LanguageText : "<NULL>"));

            sb.AppendLine("------------Text dump ends---------------");

            return sb.ToString();
        }

        #endregion ToString() Implementation

        #region GetClone() Implementation

        ///// <summary>
        ///// This methods returns a cloned instance of this class.
        ///// </summary>
        ///// <returns>A string cloned instance of this class.</returns>
        //public virtual Text GetClone()
        //{
        //    return GetClone(false, false);
        //}

        /// <summary>
        ///     Creates a deep copy of the Text object
        /// </summary>
        /// <param name="isForFlex">If set to True, the Associations not relevant to Flex will be set to NULL</param>
        /// <param name="IgnoreIDs">
        ///     If set to True, the ID property values will not be copied to the Clone.
        ///     So the cloned Text can be saved to the DB as a new record
        /// </param>
        /// <returns></returns>
        public override Text GetClone(bool isForFlex, bool IgnoreIDs)
        {
            var clone = new Text();

            if (!IgnoreIDs)
                clone.Id = Id;

            clone.Description = Description;
            clone.TextTypeID = TextTypeID;

            clone.DefaultText = DefaultText;
            return clone;
        }

        #endregion GetClone() Implementation

        #region GetHashCode() implementation.

        /// <summary>
        ///     Serves as a hash function for a particular type.
        /// </summary>
        /// <returns>
        ///     A hash code for the current Text.
        /// </returns>
        public override int GetHashCode()
        {
            return (GetType().FullName + Id).GetHashCode();
        }

        #endregion GetHashCode() Implementation

        #region Equals() implementation.

        /// <summary>
        ///     Determines whether the specified Text is equal to the current Text.
        /// </summary>
        /// <param name="obj">The Text to compare with the current Text.</param>
        /// <returns>
        ///     true if the specified Text is equal to the current Text; otherwise, false.
        /// </returns>
        /// <remarks>
        ///     Tests whether this and another object are equal in a way that will still pass
        ///     when proxy objects are being used.
        /// </remarks>
        public override bool Equals(object obj)
        {
            //Same reference check !
            if (ReferenceEquals(this, obj)) return true;

            //Type check !
            var other = obj as Text;
            if (other == null) return false;

            if (Id == 0)
                //Do a per-property search
                return
                    Description == other.Description &&
                    TextTypeID == other.TextTypeID;
            return Id == other.Id;
        }

        #endregion Equals() Implementation

        #region Constructors

        /// <summary>
        ///     Class constructor...
        /// </summary>
        public Text()
        {
        }

        /// <summary>
        ///     The parameterized constructor.
        /// </summary>
        public Text(int id, string description, int? textTypeID)
        {
            Id = id;
            Description = description;
            TextTypeID = textTypeID;
        }

        #endregion

        #region Property Declarations

        /// <summary>
        ///     Private variable to hold the value for property Description.
        /// </summary>
        private string _description;

        /// <summary>
        ///     the text description.
        /// </summary>
        public virtual string Description
        {
            get => _description;
            set => _description = value;
        }

        /// <summary>
        ///     Private variable to hold the value for property TextTypeID.
        /// </summary>
        private int? _textTypeID;

        /// <summary>
        ///     The text type id.
        /// </summary>
        public virtual int? TextTypeID
        {
            get => _textTypeID;
            set => _textTypeID = value;
        }

        private string _defaulttext;

        /// <summary>
        ///     Gets or sets the default text.
        /// </summary>
        /// <value>The default text.</value>
        public virtual string DefaultText
        {
            get => _defaulttext;
            set => _defaulttext = value;
        }

        /// <summary>
        ///     Private variable to hold value for property LanguageText
        /// </summary>
        private string _languageText;

        public virtual string LanguageText
        {
            get => _languageText;
            set => _languageText = value;
        }

        #endregion Property Declarations
    }
}