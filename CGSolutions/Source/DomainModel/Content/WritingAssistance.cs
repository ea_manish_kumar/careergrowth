﻿using System;
using System.Text;

namespace DomainModel.Content
{
    [Serializable]
    public class WritingAssistance : EntityBase<int, WritingAssistance>
    {
        #region Constructors

        #endregion

        #region ToString() Implementation

        /// <summary>
        ///     This methods returns a string representation of this class.
        /// </summary>
        /// <returns>A string representation of this class.</returns>
        public new virtual string ToString()
        {
            var sb = new StringBuilder(7);

            sb.AppendLine("------------WritingAssistance dump starts---------------");
            sb.AppendLine("ID: " + base.Id);
            sb.AppendLine("WritingAssistanceCD: " +
                          (WritingAssistanceTypeCD != null ? WritingAssistanceTypeCD : "<NULL>"));
            sb.AppendLine("TemplateCD: " + (TemplateCD != null ? TemplateCD : "<NULL>"));
            sb.AppendLine("TemplateFormatCD: " + (TemplateFormatCD != null ? TemplateFormatCD : "<NULL>"));
            sb.AppendLine("SectionTypeCD: " + (SectionTypeCD != null ? SectionTypeCD : "<NULL>"));
            sb.AppendLine("SortIndex: " + SortIndex);
            sb.AppendLine("Content: " + (Content != null ? Content : "<NULL>"));
            sb.AppendLine("CreatedOn: " + (DateCreated != null ? DateCreated.ToString() : "<NULL>"));
            sb.AppendLine("DateModified: " + (DateModified != null ? DateModified.ToString() : "<NULL>"));
            sb.AppendLine("Status: " + (Status != null ? Status : "<NULL>"));
            sb.AppendLine("LocalizationKey: " + (LocalizationKey != null ? LocalizationKey : "<NULL>"));
            sb.AppendLine("------------WritingAssistance dump ends---------------");

            return sb.ToString();
        }

        #endregion ToString() Implementation

        #region GetClone() Implementation

        /// <summary>
        ///     Gets the clone.
        /// </summary>
        /// <param name="isForFlex">if set to <c>true</c> [is for flex].</param>
        /// <param name="IgnoreIDs">if set to <c>true</c> [ignore I ds].</param>
        /// <returns></returns>
        public override WritingAssistance GetClone(bool isFlex, bool bIgnoreId)
        {
            var clone = new WritingAssistance();

            clone.Id = Id;

            clone.WritingAssistanceTypeCD = WritingAssistanceTypeCD;
            clone.SortIndex = SortIndex;
            clone.TemplateCD = TemplateCD;
            clone.TemplateFormatCD = TemplateFormatCD;
            clone.SectionTypeCD = TemplateCD;
            clone.SortIndex = SortIndex;
            clone.Content = Content;
            clone.DateCreated = DateCreated;
            clone.DateModified = DateModified;
            clone.Status = Status;
            clone.LocalizationKey = LocalizationKey;

            return clone;
        }

        #endregion GetClone() Implementation

        #region GetHashCode() implementation.

        /// <summary>
        ///     Serves as a hash function for a particular type.
        /// </summary>
        /// <returns>
        ///     A hash code for the current WritingAssistance.
        /// </returns>
        public override int GetHashCode()
        {
            return (GetType().FullName + WritingAssistanceTypeCD).GetHashCode();
        }

        #endregion GetHashCode() Implementation

        #region Equals() implementation.

        /// <summary>
        ///     Determines whether the specified WritingAssistance is equal to the current WritingAssistance.
        /// </summary>
        /// <param name="obj">The WritingAssistance to compare with the current WritingAssistance.</param>
        /// <returns>
        ///     true if the specified WritingAssistance is equal to the current WritingAssistance; otherwise, false.
        /// </returns>
        /// <remarks>
        ///     Tests whether this and another object are equal in a way that will still pass
        ///     when proxy objects are being used.
        /// </remarks>
        public override bool Equals(object obj)
        {
            return true;
        }

        #endregion Equals() Implementation

        #region Property Declarations

        public virtual string WritingAssistanceTypeCD { get; set; }
        public virtual string TemplateCD { get; set; }
        public virtual string TemplateFormatCD { get; set; }
        public virtual string SectionTypeCD { get; set; }
        public virtual short SortIndex { get; set; }
        public virtual string Content { get; set; }
        public virtual DateTime? DateCreated { get; set; }
        public virtual DateTime? DateModified { get; set; }
        public virtual string Status { get; set; }
        public virtual string LocalizationKey { get; set; }

        #endregion Property Declarations
    }
}