﻿using System;
using System.Text;

namespace DomainModel.Content
{
    /// <summary>
    ///     Represents Content Groups
    ///     A little different from Career Fields
    /// </summary>
    [Serializable]
    public class ContentGroup : EntityBase<string, ContentGroup>
    {
        #region Constructors

        #endregion

        #region ToString() Implementation

        /// <summary>
        ///     This methods returns a string representation of this class.
        /// </summary>
        /// <returns>A string representation of this class.</returns>
        public new virtual string ToString()
        {
            var sb = new StringBuilder(7);

            sb.AppendLine("------------ContentGroup dump starts---------------");
            sb.AppendLine("ContentGroupCD: " + (ContentGroupCD ?? "<NULL>"));
            sb.AppendLine(" ContentGroupType: " + (ContentGroupType ?? "<NULL>"));
            sb.AppendLine(" LocalizationKey: " + (LocalizationKey ?? "<NULL>"));
            sb.AppendLine("------------ContentGroup dump ends---------------");

            return sb.ToString();
        }

        #endregion ToString() Implementation

        #region GetClone() Implementation

        public override ContentGroup GetClone(bool isForFlex, bool IgnoreIDs)
        {
            var clone = new ContentGroup();

            clone.ContentGroupCD = ContentGroupCD;
            clone.ContentGroupType = ContentGroupType;
            clone.LocalizationKey = LocalizationKey;

            return clone;
        }

        #endregion GetClone() Implementation

        #region GetHashCode() implementation.

        /// <summary>
        ///     Serves as a hash function for a particular type.
        /// </summary>
        /// <returns>
        ///     A hash code for the current CareerField.
        /// </returns>
        public override int GetHashCode()
        {
            return (GetType().FullName + ContentGroupCD + ContentGroupType + LocalizationKey).GetHashCode();
        }

        #endregion GetHashCode() Implementation

        #region Equals() implementation.

        /// <summary>
        ///     Determines whether the specified CareerField is equal to the current CareerField.
        /// </summary>
        /// <param name="obj">The CareerField to compare with the current CareerField.</param>
        /// <returns>
        ///     true if the specified CareerField is equal to the current CareerField; otherwise, false.
        /// </returns>
        /// <remarks>
        ///     Tests whether this and another object are equal in a way that will still pass
        ///     when proxy objects are being used.
        /// </remarks>
        public override bool Equals(object obj)
        {
            return true;
        }

        #endregion Equals() Implementation

        #region Property Declarations

        public virtual string ContentGroupCD { get; set; }
        public virtual string ContentGroupType { get; set; }
        public virtual string LocalizationKey { get; set; }

        #endregion Property Declarations
    }
}