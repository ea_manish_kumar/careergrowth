﻿using System.Text;

namespace DomainModel.Content
{
    public class OnetContent : EntityBase<int, OnetContent>
    {
        public virtual string OnetSocCode { get; set; }
        public virtual string JobTitle { get; set; }
        public virtual string SectionTypeCD { get; set; }
        public virtual string Example { get; set; }
        public virtual decimal? Importance { get; set; }
        public virtual int CultureID { get; set; }


        public new virtual string ToString()
        {
            var sb = new StringBuilder(14);

            sb.Append("------------ OnetContent dump starts---------------\n");
            sb.AppendLine(base.ToString());
            sb.AppendLine("OnetSocCode: " + (OnetSocCode != null ? OnetSocCode : "<NULL>"));
            sb.AppendLine("JobTitle: " + (JobTitle != null ? JobTitle : "<NULL>"));
            sb.AppendLine("SectionTypeCD: " + (SectionTypeCD != null ? SectionTypeCD : "<NULL>"));
            sb.AppendLine("Example: " + (Example != null ? Example : "<NULL>"));
            sb.AppendLine("Importance: " + (Importance != null ? Importance.ToString() : "<NULL>"));
            sb.AppendLine("CultureID: " + CultureID);
            sb.Append("------------ OnetContent dump ends---------------\n");

            return sb.ToString();
        }
    }
}