﻿using System;

namespace DomainModel.Content
{
    [Serializable]
    public class JobTitles : EntityBase<int, JobTitles>
    {
        public virtual int JobTitleId { get; set; }
        public virtual int OccupationId { get; set; }
        public virtual string OnetSoc_Code { get; set; }
        public virtual string JobTitle { get; set; }
    }
}