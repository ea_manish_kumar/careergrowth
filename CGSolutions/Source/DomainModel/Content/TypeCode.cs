using System;
using System.Text;

namespace DomainModel.Content
{
    [Serializable]
    public class TypeCode : EntityBase<string, TypeCode>
    {
        #region ToString() Implementation

        /// <summary>
        ///     This methods returns a string representation of this class.
        /// </summary>
        /// <returns>A string representation of this class.</returns>
        public new virtual string ToString()
        {
            var sb = new StringBuilder(7);

            sb.AppendLine("------------TypeCode dump starts---------------");
            sb.AppendLine("ID: " + base.ToString());
            sb.AppendLine("TypeCD: " + (TypeCD != null ? TypeCD : "<NULL>"));
            sb.AppendLine("CodeGroup: " + (CodeGroup != null ? CodeGroup : "<NULL>"));
            sb.AppendLine("Label: " + (Label != null ? Label : "<NULL>"));
            sb.AppendLine("Description: " + (Description != null ? Description : "<NULL>"));
            sb.AppendLine("SortIndex: " + (SortIndex != null ? SortIndex.ToString() : "<NULL>"));
            sb.AppendLine("LocalizationKey: " + (LocalizationKey != null ? LocalizationKey : "<NULL>"));
            sb.AppendLine("------------TypeCode dump ends---------------");

            return sb.ToString();
        }

        #endregion ToString() Implementation

        #region GetClone() Implementation

        public override TypeCode GetClone(bool isForFlex, bool IgnoreIDs)
        {
            var clone = new TypeCode();

            if (!IgnoreIDs)
                clone.Id = Id;

            clone.TypeCD = TypeCD;
            clone.CodeGroup = CodeGroup;
            clone.Label = Label;
            clone.Description = Description;
            clone.SortIndex = SortIndex;
            clone.LocalizationKey = LocalizationKey;

            return clone;
        }

        #endregion GetClone() Implementation

        #region GetHashCode() implementation.

        /// <summary>
        ///     Serves as a hash function for a particular type.
        /// </summary>
        /// <returns>
        ///     A hash code for the current TypeCode.
        /// </returns>
        public override int GetHashCode()
        {
            return (GetType().FullName + TypeCD + CodeGroup).GetHashCode();
        }

        #endregion GetHashCode() Implementation

        #region Equals() implementation.

        /// <summary>
        ///     Determines whether the specified TypeCode is equal to the current TypeCode.
        /// </summary>
        /// <param name="obj">The TypeCode to compare with the current TypeCode.</param>
        /// <returns>
        ///     true if the specified TypeCode is equal to the current TypeCode; otherwise, false.
        /// </returns>
        /// <remarks>
        ///     Tests whether this and another object are equal in a way that will still pass
        ///     when proxy objects are being used.
        /// </remarks>
        public override bool Equals(object obj)
        {
            //Same reference check !
            if (ReferenceEquals(this, obj)) return true;

            //Type check !
            var other = obj as TypeCode;
            if (other == null) return false;

            if ((string.IsNullOrEmpty(TypeCD) && string.IsNullOrEmpty(other.TypeCD)) |
                (string.IsNullOrEmpty(CodeGroup) && string.IsNullOrEmpty(other.CodeGroup)))
                //Do a per-property search
                return
                    Label == other.Label &&
                    Description == other.Description &&
                    SortIndex == other.SortIndex;
            return TypeCD == other.TypeCD && CodeGroup == other.CodeGroup;
        }

        #endregion Equals() Implementation

        #region Constructors

        /// <summary>
        ///     Class constructor...
        /// </summary>
        public TypeCode()
        {
        }


        /// <summary>
        ///     The parameterized constructor.
        /// </summary>
        public TypeCode(string typeCD, string codeGroup, string label, string description, int? sortIndex)
        {
            TypeCD = typeCD;
            CodeGroup = codeGroup;
            Label = label;
            Description = description;
            SortIndex = sortIndex;
        }

        #endregion

        #region Property Declarations

        /// <summary>
        ///     Gets or sets the id.
        /// </summary>
        /// <value>The id.</value>
        public override string Id => TypeCD + "^" + CodeGroup;

        /// <summary>
        ///     Private variable to hold the value for property TypeCD.
        /// </summary>
        private string _typeCD;

        /// <summary>
        ///     The primary key.
        /// </summary>
        public virtual string TypeCD
        {
            get => _typeCD;
            set => _typeCD = value;
        }

        /// <summary>
        ///     Private variable to hold the value for property CodeGroup.
        /// </summary>
        private string _codeGroup;

        /// <summary>
        ///     The code group code..
        /// </summary>
        public virtual string CodeGroup
        {
            get => _codeGroup;
            set => _codeGroup = value;
        }

        /// <summary>
        ///     Private variable to hold the value for property Label.
        /// </summary>
        private string _label;

        /// <summary>
        ///     the type code label..
        /// </summary>
        public virtual string Label
        {
            get => _label;
            set => _label = value;
        }

        /// <summary>
        ///     Private variable to hold the value for property Description.
        /// </summary>
        private string _description;

        /// <summary>
        ///     The type code description.
        /// </summary>
        public virtual string Description
        {
            get => _description;
            set => _description = value;
        }

        /// <summary>
        ///     Private variable to hold the value for property SortIndex.
        /// </summary>
        private int? _sortIndex;

        /// <summary>
        ///     The type code sort Index..
        /// </summary>
        public virtual int? SortIndex
        {
            get => _sortIndex;
            set => _sortIndex = value;
        }

        /// <summary>
        ///     Is Active
        /// </summary>
        public virtual bool? IsActive { get; set; }

        public virtual string LocalizationKey { get; set; }

        #endregion Property Declarations
    }
}