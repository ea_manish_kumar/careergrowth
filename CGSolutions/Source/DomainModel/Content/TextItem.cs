using System;
using System.Text;

namespace DomainModel.Content
{
    [Serializable]
    public class TextItem : EntityBase<int, TextItem>
    {
        #region ToString() Implementation

        /// <summary>
        ///     This methods returns a string representation of this class.
        /// </summary>
        /// <returns>A string representation of this class.</returns>
        public new virtual string ToString()
        {
            var sb = new StringBuilder(6);

            sb.AppendLine("------------TextItem dump starts---------------");
            //sb.AppendLine("TextID: "	 + this.TextID.ToString());
            //sb.AppendLine("LanguageID: "	 + this.LanguageID.ToString());

            if (Text != null)
                sb.AppendLine("TextID: " + Text.Id);

            if (Language != null)
                sb.AppendLine("LanguageID: " + Language.LanguageName);

            sb.AppendLine("TextValue: " + (TextValue != null ? TextValue : "<NULL>"));
            sb.AppendLine("LongText: " + (LongText != null ? LongText : "<NULL>"));
            sb.AppendLine("------------TextItem dump ends---------------");

            return sb.ToString();
        }

        #endregion ToString() Implementation

        #region GetClone() Implementation

        /// <summary>
        ///     This methods returns a cloned instance of this class.
        /// </summary>
        /// <returns>A string cloned instance of this class.</returns>
        public new virtual TextItem GetClone(bool isForFlex, bool IgnoreIDs)
        {
            var clone = new TextItem();

            clone.Language = Language.GetClone(false, false);
            clone.Text = Text; //Parent object, don't call clone again

            //clone.TextID	 = this.TextID;
            //clone.LanguageID	 = this.LanguageID;
            clone.TextValue = TextValue;
            clone.LongText = LongText;

            return clone;
        }

        #endregion GetClone() Implementation

        #region GetHashCode() implementation.

        /// <summary>
        ///     Serves as a hash function for a particular type.
        /// </summary>
        /// <returns>
        ///     A hash code for the current TextItem.
        /// </returns>
        public override int GetHashCode()
        {
            return (GetType().FullName + Text.Id + Language.Id).GetHashCode();
        }

        #endregion GetHashCode() Implementation

        #region Equals() implementation.

        /// <summary>
        ///     Determines whether the specified TextItem is equal to the current TextItem.
        /// </summary>
        /// <param name="obj">The TextItem to compare with the current TextItem.</param>
        /// <returns>
        ///     true if the specified TextItem is equal to the current TextItem; otherwise, false.
        /// </returns>
        /// <remarks>
        ///     Tests whether this and another object are equal in a way that will still pass
        ///     when proxy objects are being used.
        /// </remarks>
        public override bool Equals(object obj)
        {
            //Same reference check !
            if (ReferenceEquals(this, obj)) return true;

            //Type check !
            var other = obj as TextItem;
            if (other == null) return false;

            if ((Text.Id == 0 && other.Text.Id == 0) | (Language.Id == 0 && other.Language.Id == 0))
                //Do a per-property search
                return
                    TextValue == other.TextValue &&
                    LongText == other.LongText;
            return Text.Id == other.Text.Id && Language.Id == other.Language.Id;
        }

        #endregion Equals() Implementation

        #region Constructors

        /// <summary>
        ///     Class constructor...
        /// </summary>
        public TextItem()
        {
        }


        /// <summary>
        ///     The parameterized constructor.
        /// </summary>
        public TextItem(string textValue, string longText)
        {
            //this.TextID = textID;
            //this.LanguageID = languageID;
            TextValue = textValue;
            LongText = longText;
        }

        #endregion

        #region Property Declarations

        public override int Id => Text.Id * Language.Id;

        private Text _text;

        /// <summary>
        ///     the text item's parent text object
        /// </summary>
        public virtual Text Text
        {
            get => _text;
            set => _text = value;
        }

        ///// <summary>
        ///// Private variable to hold the value for property TextID.
        ///// </summary>
        //private int _textID;

        ///// <summary>
        ///// The text ID FK..
        ///// </summary>
        //public virtual int TextID
        //{
        //    get
        //    {
        //        return this._textID;
        //    }
        //    set
        //    {
        //        this._textID = value;
        //    }
        //}

        private Language _language;

        /// <summary>
        ///     The text item's language
        /// </summary>
        public virtual Language Language
        {
            get => _language;
            set => _language = value;
        }

        ///// <summary>
        ///// Private variable to hold the value for property LanguageID.
        ///// </summary>
        //private int _languageID;

        ///// <summary>
        ///// The language ID FK..
        ///// </summary>
        //public virtual int LanguageID
        //{
        //    get
        //    {
        //        return this._languageID;
        //    }
        //    set
        //    {
        //        this._languageID = value;
        //    }
        //}

        /// <summary>
        ///     Private variable to hold the value for property TextValue.
        /// </summary>
        private string _textValue;

        /// <summary>
        ///     The normal text.
        /// </summary>
        public virtual string TextValue
        {
            get => _textValue;
            set => _textValue = value;
        }

        /// <summary>
        ///     Private variable to hold the value for property LongText.
        /// </summary>
        private string _longText;

        /// <summary>
        ///     Long text.
        /// </summary>
        public virtual string LongText
        {
            get => _longText;
            set => _longText = value;
        }

        #endregion Property Declarations
    }
}