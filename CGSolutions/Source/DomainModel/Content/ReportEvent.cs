﻿using System;
using System.Text;

namespace DomainModel.Content
{
    [Serializable]
    public class ReportEvent : EntityBase<int, ReportEvent>
    {
        #region Constructors

        #endregion

        #region ToString() Implementation

        public new virtual string ToString()
        {
            var sb = new StringBuilder();

            sb.AppendLine("------------ContentReportDTO dump starts---------------");
            sb.AppendLine("Value:Value " + ReportEventID);
            sb.AppendLine("Value: " + Value);
            sb.AppendLine("EventCD: " + EventCD);
            sb.AppendLine("ReportCD: " + ReportCD);
            sb.AppendLine("------------ContentReportDTO dump ends---------------");

            return sb.ToString();
        }

        #endregion ToString() Implementation

        #region GetClone() Implementation

        public virtual ReportEvent GetClone()
        {
            var clone = new ReportEvent();
            clone.ReportEventID = _reportEventID;
            clone.ReportCD = ReportCD;
            clone.EventCD = EventCD;
            clone.Value = Value;
            return clone;
        }

        #endregion GetClone() Implementation

        #region GetHashCode() implementation.

        public override int GetHashCode()
        {
            return (GetType().FullName + ReportEventID).GetHashCode();
        }

        #endregion GetHashCode() Implementation

        #region Equals() implementation.

        public override bool Equals(object obj)
        {
            //Same reference check !
            if (ReferenceEquals(this, obj)) return true;

            //Type check !
            var other = obj as ContentReport;
            if (other == null) return false;

            return true;
        }

        #endregion Equals() Implementation

        #region Property Declarations

        private int _reportEventID;

        public virtual int ReportEventID
        {
            get => _reportEventID;
            set => _reportEventID = value;
        }

        private int _value;

        public virtual int Value
        {
            get => _value;
            set => _value = value;
        }

        private string _reportCD;

        public virtual string ReportCD
        {
            get => _reportCD;
            set => _reportCD = value;
        }

        private string _eventCD;

        public virtual string EventCD
        {
            get => _eventCD;
            set => _eventCD = value;
        }

        #endregion Property Declarations
    }
}