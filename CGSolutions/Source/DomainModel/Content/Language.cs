using System;
using System.Text;

namespace DomainModel.Content
{
    [Serializable]
    public class Language : EntityBase<int, Language>
    {
        #region ToString() Implementation

        /// <summary>
        ///     This methods returns a string representation of this class.
        /// </summary>
        /// <returns>A string representation of this class.</returns>
        public new virtual string ToString()
        {
            var sb = new StringBuilder(6);

            sb.AppendLine("------------Language dump starts---------------");
            sb.AppendLine("Id: " + Id);
            sb.AppendLine("LanguageName: " + (LanguageName != null ? LanguageName : "<NULL>"));
            sb.AppendLine("Code: " + (Code != null ? Code : "<NULL>"));
            sb.AppendLine("IsDefault: " + (IsDefault != null ? IsDefault.ToString() : "<NULL>"));
            sb.AppendLine("------------Language dump ends---------------");

            return sb.ToString();
        }

        #endregion ToString() Implementation

        #region GetClone() Implementation

        /// <summary>
        ///     This methods returns a cloned instance of this class.
        /// </summary>
        /// <returns>A string cloned instance of this class.</returns>
        public new virtual Language GetClone(bool isForFlex, bool IgnoreIDs)
        {
            var clone = new Language();
            if (!IgnoreIDs)
                clone.Id = Id;
            clone.LanguageName = LanguageName;
            clone.Code = Code;
            clone.IsDefault = IsDefault;

            return clone;
        }

        #endregion GetClone() Implementation

        #region GetHashCode() implementation.

        /// <summary>
        ///     Serves as a hash function for a particular type.
        /// </summary>
        /// <returns>
        ///     A hash code for the current Language.
        /// </returns>
        public override int GetHashCode()
        {
            return (GetType().FullName + Id).GetHashCode();
        }

        #endregion GetHashCode() Implementation

        #region Equals() implementation.

        /// <summary>
        ///     Determines whether the specified Language is equal to the current Language.
        /// </summary>
        /// <param name="obj">The Language to compare with the current Language.</param>
        /// <returns>
        ///     true if the specified Language is equal to the current Language; otherwise, false.
        /// </returns>
        /// <remarks>
        ///     Tests whether this and another object are equal in a way that will still pass
        ///     when proxy objects are being used.
        /// </remarks>
        public override bool Equals(object obj)
        {
            //Same reference check !
            if (ReferenceEquals(this, obj)) return true;

            //Type check !
            var other = obj as Language;
            if (other == null) return false;

            if (Id == 0)
                //Do a per-property search
                return
                    LanguageName == other.LanguageName &&
                    Code == other.Code &&
                    IsDefault == other.IsDefault;
            return Id == other.Id;
        }

        #endregion Equals() Implementation

        #region Constructors

        /// <summary>
        ///     Class constructor...
        /// </summary>
        public Language()
        {
        }


        /// <summary>
        ///     The parameterized constructor.
        /// </summary>
        public Language(int id, string languageName, string code, bool? isDefault)
        {
            Id = id;
            LanguageName = languageName;
            Code = code;
            IsDefault = isDefault;
        }

        #endregion

        #region Property Declarations

        /// <summary>
        ///     Private variable to hold the value for property LanguageName.
        /// </summary>
        private string _languageName;

        /// <summary>
        ///     The name of the language.
        /// </summary>
        public virtual string LanguageName
        {
            get => _languageName;
            set => _languageName = value;
        }

        /// <summary>
        ///     Private variable to hold the value for property Code.
        /// </summary>
        private string _code;

        /// <summary>
        ///     The language code..
        /// </summary>
        public virtual string Code
        {
            get => _code;
            set => _code = value;
        }

        /// <summary>
        ///     Private variable to hold the value for property IsDefault.
        /// </summary>
        private bool? _isDefault;

        /// <summary>
        ///     Is default language flag..
        /// </summary>
        public virtual bool? IsDefault
        {
            get => _isDefault;
            set => _isDefault = value;
        }

        #endregion Property Declarations
    }
}