﻿using DomainModel.Base;

namespace DomainModel.Content
{
    /// <summary>
    ///     TODO:comments missing
    /// </summary>
    public class TypeCodeGroups : BaseCD
    {
        /// <summary>
        /// </summary>
        public const string ADGP = "ADGP";

        /// <summary>
        ///     TODO:comments missing
        /// </summary>
        public const string AFCM = "AFCM";

        /// <summary>
        ///     TODO:comments missing
        /// </summary>
        public const string AFPT = "AFPT";

        /// <summary>
        ///     TODO:comments missing
        /// </summary>
        public const string AFST = "AFST";

        /// <summary>
        ///     TODO:comments missing
        /// </summary>
        public const string APMP = "APMP";

        /// <summary>
        ///     TODO:comments missing
        /// </summary>
        public const string APOR = "APOR";

        /// <summary>
        ///     TODO:comments missing
        /// </summary>
        public const string APST = "APST";

        /// <summary>
        ///     TODO:comments missing
        /// </summary>
        public const string BASE = "BASE";

        /// <summary>
        ///     TODO:comments missing
        /// </summary>
        public const string BUCK = "BUCK";

        /// <summary>
        ///     TODO:comments missing
        /// </summary>
        public const string CMFD = "CMFD";

        /// <summary>
        ///     TODO:comments missing
        /// </summary>
        public const string CNPG = "CNPG";

        /// <summary>
        ///     TODO:comments missing
        /// </summary>
        public const string CNTP = "CNTP";

        /// <summary>
        ///     TODO:comments missing
        /// </summary>
        public const string FCLM = "FCLM";

        /// <summary>
        ///     TODO:comments missing
        /// </summary>
        public const string IDMP = "IDMP";

        /// <summary>
        ///     TODO:comments missing
        /// </summary>
        public const string LEAD = "LEAD";

        /// <summary>
        ///     TODO:comments missing
        /// </summary>
        public const string LKDD = "LKDD";

        /// <summary>
        ///     TODO:comments missing
        /// </summary>
        public const string LOOK = "LOOK";

        /// <summary>
        ///     TODO:comments missing
        /// </summary>
        public const string NotificationFrequency = "NTFR";

        /// <summary>
        ///     TODO:comments missing
        /// </summary>
        public const string OCLM = "OCLM";

        /// <summary>
        ///     TODO:comments missing
        /// </summary>
        public const string OFED = "OFED";

        /// <summary>
        ///     TODO:comments missing
        /// </summary>
        public const string OFFR = "OFFR";

        /// <summary>
        ///     TODO:comments missing
        /// </summary>
        public const string OFMC = "OFMC";

        /// <summary>
        ///     TODO:comments missing
        /// </summary>
        public const string OFST = "OFST";

        /// <summary>
        ///     TODO:comments missing
        /// </summary>
        public const string OSFT = "OSFT";

        /// <summary>
        ///     TODO:comments missing
        /// </summary>
        public const string OTPT = "OTPT";

        /// <summary>
        ///     TODO:comments missing
        /// </summary>
        public const string PLTP = "PLTP";

        /// <summary>
        ///     TODO:comments missing
        /// </summary>
        public const string PRFL = "PRFL";

        /// <summary>
        ///     TODO:comments missing
        /// </summary>
        public const string PTRF = "PTRF";

        /// <summary>
        ///     TODO:comments missing
        /// </summary>
        public const string QUTP = "QUTP";

        /// <summary>
        ///     TODO:comments missing
        /// </summary>
        public const string REVN = "REVN";

        /// <summary>
        ///     TODO:comments missing
        /// </summary>
        public const string RFST = "RFST";

        /// <summary>
        ///     TODO:comments missing
        /// </summary>
        public const string RFTP = "RFTP";

        /// <summary>
        ///     TODO:comments missing
        /// </summary>
        public const string RLTP = "RLTP";

        /// <summary>
        ///     TODO:comments missing
        /// </summary>
        public const string ResumeWeb = "RSWB";

        /// <summary>
        ///     TODO:comments missing
        /// </summary>
        public const string STAP = "STAP";

        /// <summary>
        ///     TODO:comments missing
        /// </summary>
        public const string STTY = "STTY";

        /// <summary>
        ///     TODO:comments missing
        /// </summary>
        public const string TGTP = "TGTP";

        /// <summary>
        ///     TODO:comments missing
        /// </summary>
        public const string DocumentTypes = "DOCT";

        /// <summary>
        ///     TODO:comments missing
        /// </summary>
        public const string LetterTemplateTypes = "LTPL";

        /// <summary>
        ///     TODO:comments missing
        /// </summary>
        public const string ResumeTemplateTypes = "RTPL";

        /// <summary>
        ///     TODO:comments missing
        /// </summary>
        public const string ProfileTemplateTypes = "PTPL";

        /// <summary>
        ///     TODO:comments missing
        /// </summary>
        public const string WebProfile = "PFWB";

        /// <summary>
        ///     TODO:comments missing
        /// </summary>
        public const string DOCP = "DOCP";

        /// <summary>
        ///     TODO:comments missing
        /// </summary>
        public const string TemplateCategoryTypes_Letter = "TCTL";

        /// <summary>
        ///     TODO:comments missing
        /// </summary>
        public const string TemplateCategoryTypes_Resume = "TCTR";

        /// <summary>
        ///     TODO:comments missing
        /// </summary>
        public const string WritingAssistanceTypes = "WATC";

        /// <summary>
        ///     TODO:comments missing
        /// </summary>
        public const string BillingOrderStatusCD = "ORST";

        /// <summary>
        ///     TODO:comments missing
        /// </summary>
        public const string BillingTransactionTypeCD = "BLTR";

        /// <summary>
        ///     TODO:comments missing
        /// </summary>
        public const string BillingResultTypeCD = "BLRS";

        /// <summary>
        ///     TODO:comments missing
        /// </summary>
        public const string BillingSubscriptionTypeCD = "SBTP";

        /// <summary>
        ///     TODO:comments missing
        /// </summary>
        public const string BillingScheduleTypeCD = "SCHD";

        /// <summary>
        ///     TODO:comments missing
        /// </summary>
        public const string BillingStatusTypeCD = "BLST";

        /// <summary>
        ///     TODO:comments missing
        /// </summary>
        public const string CoverLetterTypeCD = "CLTP";

        public const string RuleCodeGroupCD = "RLCD";

        public const string TemplateFormatTypeCD = "TMPF";

        public const string AcctPrivOptnsCodeGroupCD = "PRIV";

        /// <summary>
        ///     billingcancelation type
        ///     <added>SS</added>
        /// </summary>
        public const string CancellationTypeCD = "BLCL";

        public const string UserReviewCodeGroupCD = "RVST";
    }

    #region TypeCode Definitions

    /// <summary>
    ///     TODO:comments missing
    /// </summary>
    public class TypeCodes : BaseCD
    {
        /// <summary>
        ///     TODO:comments missing
        /// </summary>
        public const string ProfileVisible = "PRVS";

        /// <summary>
        ///     TODO:comments missing
        /// </summary>
        public const string ProfileSearch = "PRSE";

        /// <summary>
        ///     TODO:comments missing
        /// </summary>
        public const string ShowResumegig = "SCIL";

        /// <summary>
        ///     TODO:comments missing
        /// </summary>
        public const string SpecialistView = "ICSV";

        /// <summary>
        ///     TODO:comments missing
        /// </summary>
        public const string ShowContact = "CONT";

        /// <summary>
        ///     TODO:comments missing
        /// </summary>
        public const string NewReview = "NEWR";

        /// <summary>
        ///     TODO:comments missing
        /// </summary>
        public const string ApprovedReview = "APPR";

        /// <summary>
        ///     TODO:comments missing
        /// </summary>
        public const string PostedReview = "PSTD";

        /// <summary>
        ///     TODO:comments missing
        /// </summary>
        public const string RejectedReview = "REJC";

        /// <summary>
        ///     TODO:comments missing
        /// </summary>
        public const string Error = "ERRR";

        public const string UserReviewIntake = "REVI";

        /// <summary>
        ///     MY Perfect Resume TemplateTypeCD
        /// </summary>
        public const string MYPerfectResumeTemplateTypeCD = "MPRT";

        /// <summary>
        ///     MY Perfect Resume Tips
        /// </summary>
        public const string MYPerfectResumeTips = "MPTP";

        /// <summary>
        ///     TODO:comments missing
        /// </summary>
        public const string SellPagePopup = "SPOP";

        public const string SignOutLightBox = "SOLB";

        public const string MprCoverLetterPopup = "MPCL";

        public const string MPRResumePreviewPopup = "MPRP";

        public const string
            PageViewTypeCD =
                "PGVW"; // [LCMAIN-1296] New Design for AB Test - My Resume Home   // To show green check in resumehome-f.aspx page In TODO List for Inteview Video

        public const string
            InterviewVideoTypeCD =
                "IVDO"; // [LCMAIN-1296] New Design for AB Test - My Resume Home   // To show green check in resumehome-f.aspx page In TODO List for Inteview Video
    }

    #endregion
}