﻿using System;
using System.Text;

namespace DomainModel.Content
{
    [Serializable]
    public class SearchJobTitle : EntityBase<int, SearchJobTitle>
    {
        // public virtual string OnetSocCode { get; set; }
        public virtual string Title { get; set; }

        //public virtual string Description { get; set; }
        //public virtual string LayTitles { get; set; }
        //public virtual string Tasks { get; set; }
        public virtual string ContentGroupCD { get; set; }
        public virtual int Boost { get; set; }
        public virtual int ContentGroupSortIndex { get; set; }

        public new virtual string ToString()
        {
            var sb = new StringBuilder(14);

            sb.Append("------------ JobTitle dump starts---------------\n");
            sb.AppendLine(base.ToString());
            sb.AppendLine("Title: " + (Title != null ? Title : "<NULL>"));
            //sb.AppendLine("OnetSocCode: " + (this.OnetSocCode != null ? this.OnetSocCode.ToString() : "<NULL>").ToString());
            //sb.AppendLine("Description: " + (this.Description != null ? this.Description.ToString() : "<NULL>").ToString());
            //sb.AppendLine("OnetSocCode: " + (this.OnetSocCode != null ? this.OnetSocCode.ToString() : "<NULL>").ToString());
            //sb.AppendLine("LayTitles: " + (this.LayTitles != null ? this.LayTitles.ToString() : "<NULL>").ToString());
            sb.AppendLine("ContentGroupCD: " + (ContentGroupCD != null ? ContentGroupCD : "<NULL>"));
            sb.AppendLine("ContentGroupSortIndex: " + ContentGroupSortIndex);

            sb.Append("------------ JobTitle dump ends---------------\n");

            return sb.ToString();
        }
    }
}