﻿using System;
using System.Text;

namespace DomainModel.Content
{
    [Serializable]
    public class ContentReport : EntityBase<int, ContentReport>
    {
        #region Constructors

        #endregion

        #region ToString() Implementation

        public new virtual string ToString()
        {
            var sb = new StringBuilder();

            sb.AppendLine("------------ContentReportDTO dump starts---------------");
            sb.AppendLine("User ID: " + UserID);
            sb.AppendLine("AbtestID: " + AbtestID);
            sb.AppendLine("AbtestCaseIndex: " + AbtestCaseIndex);
            sb.AppendLine("TitleEntry: " + TitleEntry);
            sb.AppendLine("ContentType: " + ContentType);
            sb.AppendLine("TitleChangedCount: " + TitleChangedCount);
            sb.AppendLine("Section: " + SectionCD);
            sb.AppendLine("CareerLevel: " + CareerLevel);
            sb.AppendLine("------------ContentReportDTO dump ends---------------");

            return sb.ToString();
        }

        #endregion ToString() Implementation

        #region GetClone() Implementation

        public virtual ContentReport GetClone()
        {
            var clone = new ContentReport();
            clone.ReportID = ReportID;
            clone.UserID = UserID;
            clone.AbtestID = AbtestID;
            clone.AbtestCaseIndex = AbtestCaseIndex;
            clone.ContentType = ContentType;
            clone.TitleChangedCount = TitleChangedCount;
            clone.SectionCD = SectionCD;
            clone.JobCount = JobCount;
            clone.CareerLevel = CareerLevel;
            return clone;
        }

        #endregion GetClone() Implementation

        #region GetHashCode() implementation.

        public override int GetHashCode()
        {
            return (GetType().FullName + TitleEntry).GetHashCode();
        }

        #endregion GetHashCode() Implementation

        #region Equals() implementation.

        /// <summary>
        ///     Determines whether the specified ContentReportDTO is equal to the current ContentReportDTO.
        /// </summary>
        /// <param name="obj">The ContentReportDTO to compare with the current ContentReportDTO.</param>
        /// <returns>
        ///     true if the specified ContentReportDTO is equal to the current ContentReportDTO; otherwise, false.
        /// </returns>
        /// <remarks>
        ///     Tests whether this and another object are equal in a way that will still pass
        ///     when proxy objects are being used.
        /// </remarks>
        public override bool Equals(object obj)
        {
            //Same reference check !
            if (ReferenceEquals(this, obj)) return true;

            //Type check !
            var other = obj as ContentReport;
            if (other == null) return false;

            return true;
        }

        #endregion Equals() Implementation

        #region Property Declarations

        private int _reportID;

        public virtual int ReportID
        {
            get => _reportID;
            set => _reportID = value;
        }

        private int _userID;

        public virtual int UserID
        {
            get => _userID;
            set => _userID = value;
        }

        private short _abtestID;

        public virtual short AbtestID
        {
            get => _abtestID;
            set => _abtestID = value;
        }

        private short _abtestCaseIndex;

        public virtual short AbtestCaseIndex
        {
            get => _abtestCaseIndex;
            set => _abtestCaseIndex = value;
        }

        private string _titleEntry;

        public virtual string TitleEntry
        {
            get => _titleEntry;
            set => _titleEntry = value;
        }

        private string _contentType;

        public virtual string ContentType
        {
            get => _contentType;
            set => _contentType = value;
        }


        private string _sectionCD;

        public virtual string SectionCD
        {
            get => _sectionCD;
            set => _sectionCD = value;
        }

        private short _titleChangedCount;

        public virtual short TitleChangedCount
        {
            get => _titleChangedCount;
            set => _titleChangedCount = value;
        }

        private short _jobCount;

        public virtual short JobCount
        {
            get => _jobCount;
            set => _jobCount = value;
        }

        private string _careerLevel;

        public virtual string CareerLevel
        {
            get => _careerLevel;
            set => _careerLevel = value;
        }

        #endregion Property Declarations
    }
}