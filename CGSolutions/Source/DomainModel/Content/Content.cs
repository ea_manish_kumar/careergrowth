﻿using System;

namespace DomainModel.Content
{
    [Serializable]
    public class Contents : EntityBase<int, Contents>
    {
        #region Constructors

        #endregion

        #region Property Declarations

        public virtual int OccupationId { get; set; }
        public virtual string OnetSoc_Code { get; set; }
        public virtual int SectionTypeId { get; set; }
        public virtual string Example { get; set; }
        public virtual int Content_Type { get; set; }

        #endregion Property Declarations
    }
}