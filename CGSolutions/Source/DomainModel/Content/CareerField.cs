﻿using System;
using System.Text;

namespace DomainModel.Content
{
    [Serializable]
    public class CareerField : EntityBase<int, CareerField>
    {
        #region Constructors

        #endregion

        #region ToString() Implementation

        /// <summary>
        ///     This methods returns a string representation of this class.
        /// </summary>
        /// <returns>A string representation of this class.</returns>
        public new virtual string ToString()
        {
            var sb = new StringBuilder(7);

            sb.AppendLine("------------CareerField dump starts---------------");
            sb.AppendLine("ID: " + base.ToString());
            sb.AppendLine("CareerField: " + (CareerFieldValue ?? "<NULL>"));
            sb.AppendLine("SortIndex: " + SortIndex);
            sb.AppendLine("ParentID: " + ParentID);
            sb.AppendLine("------------CareerField dump ends---------------");

            return sb.ToString();
        }

        #endregion ToString() Implementation

        #region GetClone() Implementation

        /// <summary>
        ///     Gets the clone.
        /// </summary>
        public override CareerField GetClone(bool isForFlex, bool IgnoreIDs)
        {
            var clone = new CareerField();

            if (!IgnoreIDs)
                clone.Id = Id;

            clone.CareerFieldValue = CareerFieldValue;
            clone.SortIndex = SortIndex;
            clone.ParentID = ParentID;

            return clone;
        }

        #endregion GetClone() Implementation

        #region GetHashCode() implementation.

        /// <summary>
        ///     Serves as a hash function for a particular type.
        /// </summary>
        /// <returns>
        ///     A hash code for the current CareerField.
        /// </returns>
        public override int GetHashCode()
        {
            return (GetType().FullName + CareerFieldValue + ParentID).GetHashCode();
        }

        #endregion GetHashCode() Implementation

        #region Equals() implementation.

        /// <summary>
        ///     Determines whether the specified CareerField is equal to the current CareerField.
        /// </summary>
        /// <param name="obj">The CareerField to compare with the current CareerField.</param>
        /// <returns>
        ///     true if the specified CareerField is equal to the current CareerField; otherwise, false.
        /// </returns>
        /// <remarks>
        ///     Tests whether this and another object are equal in a way that will still pass
        ///     when proxy objects are being used.
        /// </remarks>
        public override bool Equals(object obj)
        {
            return true;
        }

        #endregion Equals() Implementation

        #region Property Declarations

        public virtual string CareerFieldValue { get; set; }
        public virtual int SortIndex { get; set; }
        public virtual int ParentID { get; set; }

        #endregion Property Declarations
    }
}