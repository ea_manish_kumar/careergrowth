﻿using System;

namespace DomainModel.Content
{
    [Serializable]
    public class Tips
    {
        public virtual int TipId { get; set; }
        public virtual string SectionTypeCD { get; set; }
        public virtual string Content { get; set; }
        public virtual int CultureId { get; set; }
    }
}