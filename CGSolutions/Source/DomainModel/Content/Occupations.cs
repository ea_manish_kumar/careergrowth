﻿using System;

namespace DomainModel.Content
{
    [Serializable]
    public class Occupations : EntityBase<int, Occupations>
    {
        public virtual int OccupationId { get; set; }
        public virtual string OnetSoc_Code { get; set; }
        public virtual string Title { get; set; }
        public virtual string Description { get; set; }
    }
}