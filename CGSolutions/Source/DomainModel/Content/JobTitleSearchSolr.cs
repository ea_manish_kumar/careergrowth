﻿using System.Collections.Generic;

namespace DomainModel.Content
{
    public class JobTitleContentSearchSolr
    {
        #region Constructor

        #endregion

        #region Property

        public virtual string jobtitle { get; set; }

        public virtual string sectiontypecd { get; set; }

        public virtual string example { get; set; }

        public virtual decimal importance { get; set; }

        #endregion
    }

    public class JobTitleSearchSolr
    {
        #region Constructor

        #endregion

        #region Property

        public virtual string onetsoccode { get; set; }

        public virtual string jobtitle { get; set; }

        public virtual string description { get; set; }

        public virtual ICollection<string> laytitles { get; set; }

        public virtual ICollection<string> tasks { get; set; }

        public virtual string contentgroupcd { get; set; }

        public virtual int contentgroupsortindex { get; set; }

        #endregion
    }

    public class JobTitleContentSearch
    {
        #region Constructor

        #endregion

        #region Property

        public virtual string contentgroupcd { get; set; }

        public virtual string sectiontypecd { get; set; }

        public virtual string example { get; set; }

        #endregion
    }
}