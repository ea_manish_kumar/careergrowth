﻿using System;

namespace DomainModel.Billing
{
    public class OrderItem : EntityBase<int, OrderItem>
    {
        public virtual int OrderID { get; set; }
        public virtual int PlanID { get; set; }
        public virtual decimal Price { get; set; }
        public virtual DateTime CreationDate { get; set; }
    }
}