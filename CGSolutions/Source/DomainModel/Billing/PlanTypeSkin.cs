﻿namespace DomainModel.Billing
{
    public class PlanTypeSkin : EntityBase<int, PlanTypeSkin>
    {
        public virtual int PlanTypeID { get; set; }
        public virtual int SkinID { get; set; }
        public virtual bool WaterMarkEnabled { get; set; }
    }
}