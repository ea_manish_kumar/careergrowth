﻿namespace DomainModel.Billing
{
    public class Plan : EntityBase<int, Plan>
    {
        public virtual int PlanTypeID { get; set; }
        public virtual string Label { get; set; }
        public virtual string CurrencyCD { get; set; }
        public virtual string CountryCD { get; set; }
        public virtual decimal UnitPrice { get; set; }
        public virtual int ValidityDays { get; set; }
    }
}