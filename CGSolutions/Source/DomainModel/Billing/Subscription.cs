﻿using System;

namespace DomainModel.Billing
{
    public class Subscription : EntityBase<int, Subscription>
    {
        public virtual int UserID { get; set; }
        public virtual int OrderID { get; set; }
        public virtual int PlanTypeID { get; set; }
        public virtual DateTime StartDate { get; set; }
        public virtual DateTime ExpireDate { get; set; }
        public virtual string StatusCD { get; set; }
        public virtual DateTime CreationDate { get; set; }
        public virtual DateTime ModificationDate { get; set; }
    }
}