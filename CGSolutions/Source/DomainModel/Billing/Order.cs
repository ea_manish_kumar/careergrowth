﻿using System;

namespace DomainModel.Billing
{
    public class Order : EntityBase<int, Order>
    {
        public Order()
        {
            CreationDate = DateTime.Now;
        }

        public virtual int UserID { get; set; }
        public virtual decimal Amount { get; set; }
        public virtual string StatusCD { get; set; }
        public virtual Guid SessionId { get; set; }
        public virtual DateTime CreationDate { get; set; }
        public virtual DateTime ModifiedDate { get; set; }
    }
}