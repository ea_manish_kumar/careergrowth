﻿namespace DomainModel.Billing
{
    public class PlanTypeFeature : EntityBase<int, PlanTypeFeature>
    {
        public virtual int PlanTypeID { get; set; }
        public virtual int FeatureID { get; set; }
    }
}