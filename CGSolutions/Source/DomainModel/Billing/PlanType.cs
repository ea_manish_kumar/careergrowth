﻿namespace DomainModel.Billing
{
    public class PlanType : EntityBase<int, PlanType>
    {
        public virtual string PlanTypeCD { get; set; }
        public virtual bool WaterMarkEnabled { get; set; }
    }
}