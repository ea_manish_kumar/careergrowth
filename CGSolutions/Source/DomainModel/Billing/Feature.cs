﻿namespace DomainModel.Billing
{
    public class Feature : EntityBase<int, Feature>
    {
        public virtual string FeatureCD { get; set; }
    }
}