﻿using System;

namespace DomainModel.Billing
{
    public class Transaction : EntityBase<int, Transaction>
    {
        public virtual int UserID { get; set; }
        public virtual int OrderID { get; set; }
        public virtual string StatusCD { get; set; }
        public virtual string VendorCD { get; set; }
        public virtual string VendorReferenceCD { get; set; }
        public virtual decimal Amount { get; set; }
        public virtual string IPAddress { get; set; }
        public virtual int SessionID { get; set; }
        public virtual DateTime CreationDate { get; set; }
        public virtual DateTime ModifiedDate { get; set; }
    }
}