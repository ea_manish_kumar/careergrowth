﻿using System;
using System.Collections.Generic;
using System.Text;

namespace DomainModel.Event
{
    [Serializable]
    public class UserEvent : Event
    {
        #region Constructors

        /// <summary>
        ///     Class constructor...
        /// </summary>
        public UserEvent()
        {
            _eventdatas = new List<EventData>();
        }

        #endregion

        #region ToString() Implementation

        public new virtual string ToString()
        {
            var sb = new StringBuilder(7);

            sb.AppendLine("------------UserEvent dump starts---------------");

            sb.AppendLine(base.ToString());
            sb.AppendLine("UserID: " + UserID);
            foreach (var ed in EventDataCollection) sb.AppendLine(ed.ToString());
            sb.AppendLine("------------UserEvent dump ends---------------");

            return sb.ToString();
        }

        #endregion ToString() Implementation

        #region GetClone() Implementation

        /// <summary>
        ///     This methods returns a cloned instance of this class.
        /// </summary>
        /// <returns>A string cloned instance of this class.</returns>
        public virtual UserEvent GetClone()
        {
            var clone = new UserEvent();

            clone.Id = Id;
            clone.EventCode = EventCode;
            clone.UserID = UserID;
            clone.Timestamp = Timestamp;
            foreach (var objEventData in EventDataCollection)
            {
                var CloneEventData = objEventData.GetClone();
                CloneEventData.UserEvent = clone;
                clone.EventDataCollection.Add(CloneEventData);
            }

            return clone;
        }

        #endregion GetClone() Implementation

        #region GetHashCode() implementation.

        /// <summary>
        ///     Serves as a hash function for a particular type.
        /// </summary>
        /// <returns>
        ///     A hash code for the current UserEvent.
        /// </returns>
        public override int GetHashCode()
        {
            return (GetType().FullName + Id).GetHashCode();
        }

        #endregion GetHashCode() Implementation

        #region Equals() implementation.

        /// <summary>
        ///     Determines whether the specified UserEvent is equal to the current UserEvent.
        /// </summary>
        /// <param name="obj">The UserEvent to compare with the current UserEvent.</param>
        /// <returns>
        ///     true if the specified UserEvent is equal to the current UserEvent; otherwise, false.
        /// </returns>
        /// <remarks>
        ///     Tests whether this and another object are equal in a way that will still pass
        ///     when proxy objects are being used.
        /// </remarks>
        public override bool Equals(object obj)
        {
            //Same reference check !
            if (ReferenceEquals(this, obj)) return true;

            //Type check !
            var other = obj as UserEvent;
            if (other == null) return false;

            if (Id == null)
                //Do a per-property search
                return
                    EventCode == other.EventCode &&
                    UserID == other.UserID &&
                    Timestamp == other.Timestamp &&
                    EventDataCollection.Count == other.EventDataCollection.Count;
            return Id == other.Id;
        }

        #endregion Equals() Implementation

        /// <summary>
        ///     Removes a EventData at the given index
        /// </summary>
        /// <param name="EventDataIndex">The index of the EventData to remove</param>
        public virtual void RemoveEventData(int EventDataIndex)
        {
            if (EventDataCollection.Count < EventDataIndex)
                throw new IndexOutOfRangeException("The provided index is higher than collection count");

            var EventData = EventDataCollection[EventDataIndex];
            RemoveEventData(EventData);
        }

        /// <summary>
        ///     Removes a given EventData from the EventDatas collection.
        /// </summary>
        /// <param name="EventData">The EventData to remove</param>
        public virtual void RemoveEventData(EventData EventData)
        {
            if (!EventDataCollection.Contains(EventData))
                throw new ArgumentNullException("The provided EventData does not exist in the collection");

            EventDataCollection.Remove(EventData);
            EventData.UserEvent = null;
        }

        /// <summary>
        ///     Adds a given EventData to the Document.EventDatas collection.
        /// </summary>
        /// <param name="EventData"></param>
        public virtual void AddEventData(EventData EventData)
        {
            EventData.UserEvent = this;
            EventDataCollection.Add(EventData);
        }

        #region Property Declarations

        /// <summary>
        ///     Private variable to hold the value for property UserID.
        /// </summary>
        private int _userID;

        /// <summary>
        ///     The user id.
        /// </summary>
        /// <value>The user ID.</value>
        public virtual int UserID
        {
            get => _userID;
            set => _userID = value;
        }

        ///// <summary>
        ///// Private variable to hold the value for property EventCode.
        ///// </summary>
        //private EventCode  _eventCode;

        ///// <summary>
        ///// The related event code..
        ///// </summary>
        ///// <value>The event code.</value>
        //public virtual new EventCode EventCode
        //{
        //    get
        //    {
        //        return this._eventCode;
        //    }
        //    set
        //    {
        //        this._eventCode = value;
        //    }
        //}

        private IList<EventData> _eventdatas;

        /// <summary>
        ///     EventDatas collection
        /// </summary>
        public virtual IList<EventData> EventDataCollection
        {
            get => _eventdatas;
            set => _eventdatas = value;
        }

        #endregion Property Declarations
    }
}