﻿namespace DomainModel.Event
{
    /// <summary>
    ///     Enum declares generic event codes, available only to generic events
    ///     which are not related to any PartyID
    /// </summary>
    public enum EventCodeType
    {
        //Following is an Example ONLY and should be deleted in production
        /// <summary>
        ///     1000 website hits receieved today
        /// </summary>
        ThousandHitsReceieved = 1000
    }

    /// <summary>
    ///     Enum declares generic event codes related to only a User and no other entity
    /// </summary>
    public enum UserEventCodeType
    {
        //Following is an Example ONLY and should be deleted in production
        /// <summary>
        ///     User accepts an offer
        /// </summary>
        OfferAccepted = 400,

        /// <summary>
        ///     User Subscribed
        /// </summary>
        UserSubscribed = 23,

        /// <summary>
        ///     Recurring Subscription
        /// </summary>
        RecurringSubscription = 24,

        /// <summary>
        ///     Subscription Cancelled
        /// </summary>
        SubscriptionCancelled = 25,

        /// <summary>
        ///     Refund
        /// </summary>
        BillingTransactionRefund = 30
    }

    /// <summary>
    ///     Enum declares the event codes for hybrid events that add data for more than 1 kind of entity
    /// </summary>
    public enum HybridEventCodeType
    {
        /// <summary>
        ///     Document Viewed Online
        /// </summary>
        DocumentViewedOnlineByNewUser = 8,

        /// <summary>
        ///     Document Viewed Online
        /// </summary>
        DocumentViewedOnlineByReturningUser = 9,

        /// <summary>
        ///     Document Viewed Offline
        /// </summary>
        DocumentViewedOffline = 10,

        /// <summary>
        ///     Document Emailed
        /// </summary>
        EmailSentWithDocument = 11,

        /// <summary>
        ///     Document Faxed
        /// </summary>
        FaxSentWithDocument = 12,

        /// <summary>
        ///     Email Sent With Resume And Cover Letter
        /// </summary>
        EmailSentWithResumeAndCoverLetter = 33
    }

    /// <summary>
    ///     Enum declares the various communication event codes
    /// </summary>
    public enum CommunicationEventCodeType
    {
        /// <summary>
        ///     Email Sent
        /// </summary>
        EmailSent = 4,

        /// <summary>
        ///     Email Receieved
        /// </summary>
        EmailReceieved = 5,

        /// <summary>
        ///     Fax Sent
        /// </summary>
        FaxSent = 6
    }

    /// <summary>
    ///     Enum declares the various document event codes.
    /// </summary>
    public enum DocumentEventCodeType
    {
        /// <summary>
        ///     New document created
        /// </summary>
        DocumentCreated = 1,

        /// <summary>
        ///     Document updated
        /// </summary>
        DocumentUpdated = 2,

        /// <summary>
        ///     Document Downloaded
        /// </summary>
        DocumentDownloaded = 3,

        /// <summary>
        ///     Document Viewed Online by New User
        /// </summary>
        DocumentViewedOnlinebyNewUser = 8,

        /// <summary>
        ///     Document Viewed Online by Reurning User
        /// </summary>
        DocumentViewedOnlinebyReurningUser = 9,

        /// <summary>
        ///     Document Viewed Offline
        /// </summary>
        DocumentViewedOffline = 10,

        /// <summary>
        ///     User contacted
        /// </summary>
        UserContacted = 13,

        /// <summary>
        ///     User Activated resume. Made Online
        /// </summary>
        DocumentActivated = 14,

        /// <summary>
        ///     User Deactiveted resume, Made Offline
        /// </summary>
        DocumentDeactivated = 15,

        /// <summary>
        ///     Document Viewed In Facebook
        /// </summary>
        DocumentViewedInFacebook = 16,

        /// <summary>
        ///     Document Creation Aborted
        /// </summary>
        DocumentCreationAborted = 17,

        /// <summary>
        ///     Document Renamed
        /// </summary>
        DocumentRenamed = 18,

        /// <summary>
        ///     Partner Document Deleted
        /// </summary>
        PartnerDocumentDeleted = 19,

        /// <summary>
        ///     Document Copied
        /// </summary>
        DocumentCopied = 20,

        /// <summary>
        ///     Document Archived
        /// </summary>
        DocumentArchived = 21,

        /// <summary>
        ///     Document Activated
        /// </summary>
        DocumentUnArchived = 22,


        /// <summary>
        ///     Partner Document Edited,Shared Resource Edited
        /// </summary>
        SharedResourceEdited = 26,

        /// <summary>
        ///     Document Sharedon FaceBook
        /// </summary>
        DocumentSharedonFaceBook = 27,

        /// <summary>
        ///     FaceBook Access Authorized
        /// </summary>
        FaceBookAccessAuthorized = 28,

        /// <summary>
        ///     FaceBook Tab Removed
        /// </summary>
        FaceBookTabRemoved = 29,

        /// <summary>
        ///     Billing Transaction Refunded
        /// </summary>
        BillingTransactionRefunded = 30,

        /// <summary>
        ///     Shared Resource Created
        /// </summary>
        SharedResourceCreated = 31,

        /// <summary>
        ///     InComplete Shared Resource Created
        /// </summary>
        InCompleteSharedResourceCreated = 32,

        /// <summary>
        ///     Document Deleted event
        /// </summary>
        DocumentDeleted = 36,

        /// <summary>
        ///     Resume marked as complete
        /// </summary>
        ResumeMarkedComplete = 37,

        /// <summary>
        ///     Resume marked as incomplete
        /// </summary>
        ResumeMarkedInComplete = 38,


        /// <summary>
        ///     Text tuner clicked
        /// </summary>
        TextTunerClicked = 39,

        /// <summary>
        ///     Download Start
        /// </summary>
        DownloadStart = 40,

        /// <summary>
        ///     SharedDocumentDeleted
        ///     Shared prefix added as DocumentDeleted event alreday defined, this is used for the DeleteResume API Methods as per
        ///     PI Doc 6.4
        /// </summary>
        SharedDocumentDeleted = 41,

        /// <summary>
        ///     SharedDocumentActivated
        ///     Shared prefix added as DocumentDeleted event alreday defined, this is used for the ActivateResume API Methods as
        ///     per PI Doc 6.4
        /// </summary>
        SharedDocumentActivated = 42,

        /// <summary>
        ///     SharedDocumentDeactivated
        ///     Shared prefix added as DocumentDeleted event alreday defined, this is used for the DeActivateresume API Methods as
        ///     per PI Doc 6.4
        /// </summary>
        SharedDocumentDeactivated = 43,

        /// <summary>
        ///     DocumentVisibilityChanged
        /// </summary>
        DocumentVisibilityChanged = 44,


        /// <summary>
        ///     SharedDocumentCopied
        ///     Shared prefix added as DocumentCopied event alreday defined,
        ///     this is used for the CopyResume API Methods as per PI Doc 6.4
        /// </summary>
        SharedDocumentCopied = 45,

        /// <summary>
        ///     Document Submitted fro Review
        /// </summary>
        DocumentSubmittedForReview = 46,

        /// <summary>
        ///     Docuemnt Review complted
        /// </summary>
        DocumentReviewCompleted = 47,

        /// <summary>
        ///     Document Review Viewed
        /// </summary>
        DocumentReviewViewed = 48,

        /// <summary>
        ///     Subscription Cancelled by User
        /// </summary>
        SubscriptionCancelledbyUser = 49,

        /// <summary>
        ///     Subscription Cancelled by Admin
        /// </summary>
        SubscriptionCancelledbyAdmin = 50,

        /// <summary>
        ///     Document Shared on LinkedIn
        /// </summary>
        DocumentSharedonLinkedIn = 52,

        /// <summary>
        ///     Document Shared on Twitter
        /// </summary>
        DocumentSharedonTwitter = 53,

        /// <summary>
        ///     Template Changed
        /// </summary>
        TemplateChanged = 54,

        /// <summary>
        ///     Resume Submitted for Writing
        /// </summary>
        ResumeSubmittedforWriting = 55,

        /// <summary>
        ///     Resume Writing Completed
        /// </summary>
        ResumeWritingCompleted = 56,

        /// <summary>
        ///     Resume Writing Viewed
        /// </summary>
        ResumeWritingViewed = 57,

        /// <summary>
        ///     Document Posted
        /// </summary>
        DocumentPosted = 58,

        /// <summary>
        ///     Sell Page Viewed
        /// </summary>
        SellPageViewed = 60
    }

    /// <summary>
    ///     Enum declares the various Page related event codes
    /// </summary>
    public enum PageEventCodeType
    {
        /// <summary>
        ///     Page viewed
        /// </summary>
        PageViewed = 7,

        SwifDownloaded = 40
    }

    /// <summary>
    ///     Enum declares the various Periods available to aggregate the Event Counts by
    /// </summary>
    public enum EventStatisticType
    {
        /// <summary>
        ///     Resume/Profile View statistics
        /// </summary>
        DocumentViews = 1,

        /// <summary>
        ///     Referrer Statistics
        /// </summary>
        DocumentReferrerVisits = 2,

        /// <summary>
        ///     Visitor Statistics
        /// </summary>
        DocumentVisits = 3
    }
}