﻿using System;
using System.Collections.Generic;
using System.Text;

namespace DomainModel.Event
{
    [Serializable]
    public class DocumentEvent : UserEvent
    {
        #region Constructors

        #endregion

        #region ToString() Implementation

        /// <summary>
        ///     This methods returns a string representation of this class.
        /// </summary>
        /// <returns>A string representation of this class.</returns>
        public new virtual string ToString()
        {
            var sb = new StringBuilder(7);

            sb.AppendLine("------------DocumentEvent dump starts---------------");
            sb.AppendLine(base.ToString());
            sb.AppendLine("DocumentID: " + Document.Id);
            sb.AppendLine("------------DocumentEvent dump ends---------------");

            return sb.ToString();
        }

        #endregion ToString() Implementation

        #region Property Declarations

        /// <summary>
        ///     Gets or Sets the first document in the list of Documents
        ///     This property should only be used if the event relates to only 1 document
        /// </summary>
        /// <value>The document.</value>
        public virtual Document Document
        {
            get => Documents[0];
            set
            {
                //Check if there is already a First Document present
                if (Documents.Count == 0)
                    //If not then add a new item
                    Documents.Add(value);
                else
                    //If already present, then replace with this one
                    Documents[0] = value;
            }
        }

        /// <summary>
        ///     Private variable to hold the value for property Documents.
        /// </summary>
        private IList<Document> _documents = new List<Document>();

        /// <summary>
        ///     gets or sets the Collection of documents related to the event
        /// </summary>
        /// <value>The documents.</value>
        public virtual IList<Document> Documents
        {
            get => _documents;
            set => _documents = value;
        }

        #endregion Property Declarations
    }
}