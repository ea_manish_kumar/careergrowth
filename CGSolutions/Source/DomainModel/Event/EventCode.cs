﻿using System;
using System.Text;

namespace DomainModel.Event
{
    [Serializable]
    public class EventCode : EntityBase<int, EventCode>
    {
        #region ToString() Implementation

        /// <summary>
        ///     This methods returns a string representation of this class.
        /// </summary>
        /// <returns>A string representation of this class.</returns>
        public new virtual string ToString()
        {
            var sb = new StringBuilder(6);

            sb.AppendLine("------------EventCode dump starts---------------");
            sb.AppendLine("EventCodeID: " + Id);
            sb.AppendLine("EventCode: " + (EventCodeType != null ? EventCodeType : "<NULL>"));
            sb.AppendLine("Description: " + (Description != null ? Description : "<NULL>"));
            sb.AppendLine("VisibilityTypeCD: " + (VisibilityTypeCD != null ? VisibilityTypeCD : "<NULL>"));
            sb.AppendLine(
                "AdminVisibilityTypeCD: " + (AdminVisibilityTypeCD != null ? AdminVisibilityTypeCD : "<NULL>"));
            sb.AppendLine("------------EventCode dump ends---------------");

            return sb.ToString();
        }

        #endregion ToString() Implementation

        #region GetClone() Implementation

        /// <summary>
        ///     This methods returns a cloned instance of this class.
        /// </summary>
        /// <returns>A string cloned instance of this class.</returns>
        public virtual EventCode GetClone()
        {
            var clone = new EventCode();

            clone.Id = Id;
            clone.EventCodeType = EventCodeType;
            clone.Description = Description;
            clone.VisibilityTypeCD = VisibilityTypeCD;
            clone.AdminVisibilityTypeCD = AdminVisibilityTypeCD;
            return clone;
        }

        #endregion GetClone() Implementation

        #region GetHashCode() implementation.

        /// <summary>
        ///     Serves as a hash function for a particular type.
        /// </summary>
        /// <returns>
        ///     A hash code for the current EventCode.
        /// </returns>
        public override int GetHashCode()
        {
            return (GetType().FullName + Id).GetHashCode();
        }

        #endregion GetHashCode() Implementation

        #region Equals() implementation.

        /// <summary>
        ///     Determines whether the specified EventCode is equal to the current EventCode.
        /// </summary>
        /// <param name="obj">The EventCode to compare with the current EventCode.</param>
        /// <returns>
        ///     true if the specified EventCode is equal to the current EventCode; otherwise, false.
        /// </returns>
        /// <remarks>
        ///     Tests whether this and another object are equal in a way that will still pass
        ///     when proxy objects are being used.
        /// </remarks>
        public override bool Equals(object obj)
        {
            //Same reference check !
            if (ReferenceEquals(this, obj)) return true;

            //Type check !
            var other = obj as EventCode;
            if (other == null) return false;

            if (Id == 0)
                //Do a per-property search
                return
                    EventCodeType == other.EventCodeType &&
                    Description == other.Description &&
                    VisibilityTypeCD == other.VisibilityTypeCD &&
                    AdminVisibilityTypeCD == other.AdminVisibilityTypeCD;
            return Id == other.Id;
        }

        #endregion Equals() Implementation

        #region Constructors

        /// <summary>
        ///     Class constructor...
        /// </summary>
        public EventCode()
        {
            //TODO:Write constructor stuff here..
        }


        /// <summary>
        ///     The parameterized constructor.
        /// </summary>
        public EventCode(int eventCodeID, string eventCode, string description, string visibilityTypeCD,
            string AdminvisibilityTypeCD)
        {
            Id = eventCodeID;
            EventCodeType = eventCode;
            Description = description;
            VisibilityTypeCD = visibilityTypeCD;
            AdminVisibilityTypeCD = AdminVisibilityTypeCD;
        }

        #endregion

        #region Property Declarations

        /// <summary>
        ///     Private variable to hold the value for property EventCode.
        /// </summary>
        private string _eventCode;

        /// <summary>
        ///     Event Code short Notation of Event.
        /// </summary>
        public virtual string EventCodeType
        {
            get => _eventCode;
            set => _eventCode = value;
        }

        /// <summary>
        ///     Private variable to hold the value for property Description.
        /// </summary>
        private string _description;

        /// <summary>
        ///     Description of Event.
        /// </summary>
        public virtual string Description
        {
            get => _description;
            set => _description = value;
        }

        /// <summary>
        ///     Private variable to hold the value for property VisibilityTypeCD.
        /// </summary>
        private string _visibilityTypeCD;

        /// <summary>
        ///     Visibility of Event in Journals.
        /// </summary>
        public virtual string VisibilityTypeCD
        {
            get => _visibilityTypeCD;
            set => _visibilityTypeCD = value;
        }

        private string _adminVisibilityTypeCD;

        /// <summary>
        ///     Admin Visibility of Event in Journals.
        /// </summary>
        public virtual string AdminVisibilityTypeCD
        {
            get => _adminVisibilityTypeCD;
            set => _adminVisibilityTypeCD = value;
        }

        #endregion Property Declarations
    }
}