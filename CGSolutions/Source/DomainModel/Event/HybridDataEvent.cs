﻿using System;
using System.Collections.Generic;
using System.Text;

namespace DomainModel.Event
{
    [Serializable]
    public class HybridDataEvent : UserEvent
    {
        #region Constructors

        #endregion

        #region ToString() Implementation

        /// <summary>
        ///     This methods returns a string representation of this class.
        /// </summary>
        /// <returns>A string representation of this class.</returns>
        public new virtual string ToString()
        {
            var sb = new StringBuilder(8);

            sb.AppendLine("------------HybridDataEvent dump starts---------------");
            sb.AppendLine(base.ToString());
            //sb.AppendLine("PartyID: " + this.PartyID.ToString());
            sb.AppendLine("DocumentCount: " + Documents.Count);
            //sb.AppendLine("EventCode: " + this.EventCode.ToString());
            //sb.AppendLine("Timestamp: " + this.Timestamp.ToString());
            sb.AppendLine("------------HybridDataEvent dump ends---------------");

            return sb.ToString();
        }

        #endregion ToString() Implementation

        #region Property Declarations

        /// <summary>
        ///     Private variable to hold the value for property Documents.
        /// </summary>
        private IList<Document> _documents = new List<Document>();

        /// <summary>
        ///     gets or sets the Collection of documents related to the event
        /// </summary>
        /// <value>The documents.</value>
        public virtual IList<Document> Documents
        {
            get => _documents;
            set => _documents = value;
        }

        ///// <summary>
        ///// Private variable to hold the value for property Communications.
        ///// </summary>
        //private IList<Communication> _communications = new List<Communication>();

        ///// <summary>
        ///// gets or sets the Collection of Communications related to the event
        ///// </summary>
        ///// <value>The communications.</value>
        //public virtual IList<Communication> Communications
        //{
        //    get
        //    {
        //        return this._communications;
        //    }
        //    set
        //    {
        //        this._communications = value;
        //    }
        //}

        private string _EventDescription = string.Empty;

        /// <summary>
        ///     Event description. The Events business method will store the event description string in it.
        /// </summary>
        /// <value>The event description.</value>
        /// <remarks>Added by Kalyan (11 sep 09). Not mapped to DB.</remarks>
        public virtual string EventDescription
        {
            get => _EventDescription;
            set => _EventDescription = value;
        }

        #endregion Property Declarations
    }

    /// <summary>
    ///     Class to hold a Group of HybridDataEvent objects that are grouped by a specified field, usually Date
    /// </summary>
    public class HybridDataEventGroup
    {
        #region Property Declarations

        /// <summary>
        ///     Private variable to hold the value for property GroupName.
        /// </summary>
        private string _groupname;

        /// <summary>
        ///     Represents the Name of the Group, usually the Date by which the Event data has been grouped
        /// </summary>
        /// <value>The name of the group.</value>
        public virtual string GroupName
        {
            get => _groupname;
            set => _groupname = value;
        }

        /// <summary>
        ///     Private variable to hold the value for property HybridDataEvents.
        /// </summary>
        private IList<HybridDataEvent> _HybridDataEvents = new List<HybridDataEvent>();

        /// <summary>
        ///     gets or sets the Collection of HybridDataEvents in the group
        /// </summary>
        /// <value>The hybrid data events.</value>
        public virtual IList<HybridDataEvent> HybridDataEvents
        {
            get => _HybridDataEvents;
            set => _HybridDataEvents = value;
        }

        #endregion
    }
}