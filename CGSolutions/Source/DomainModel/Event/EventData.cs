﻿using System;
using System.Text;

namespace DomainModel.Event
{
    [Serializable]
    public class EventData : EntityBase<Guid, EventData>
    {
        #region ToString() Implementation

        /// <summary>
        ///     This methods returns a string representation of this class.
        /// </summary>
        /// <returns>A string representation of this class.</returns>
        public new virtual string ToString()
        {
            var sb = new StringBuilder(5);

            sb.AppendLine("------------EventData dump starts---------------");
            sb.AppendLine("ID: " + (Id != null ? Id.ToString() : "<NULL>"));
            sb.AppendLine("UserEvent: " + (UserEvent != null ? UserEvent.Id.ToString() : "<NULL>"));

            sb.AppendLine("DataTypeCD: " + (DataTypeCD != null ? DataTypeCD : "<NULL>"));
            sb.AppendLine("EventData: " + (EventDataDetails != null ? EventDataDetails : "<NULL>"));
            sb.AppendLine("------------EventData dump ends---------------");

            return sb.ToString();
        }

        #endregion ToString() Implementation

        #region GetClone() Implementation

        /// <summary>
        ///     This methods returns a cloned instance of this class.
        /// </summary>
        /// <returns>A string cloned instance of this class.</returns>
        public virtual EventData GetClone()
        {
            var clone = new EventData();

            clone.Id = Id;
            clone.DataTypeCD = DataTypeCD;
            clone.EventDataDetails = EventDataDetails;

            return clone;
        }

        #endregion GetClone() Implementation

        #region GetHashCode() implementation.

        public override int GetHashCode()
        {
            return (GetType().FullName + Id + DataTypeCD).GetHashCode();
        }

        #endregion GetHashCode() implementation.

        #region Equals() implementation.

        /// <summary>
        ///     Determines whether the specified EventData is equal to the current EventData.
        /// </summary>
        /// <param name="obj">The EventData to compare with the current EventData.</param>
        /// <returns>
        ///     true if the specified EventData is equal to the current EventData; otherwise, false.
        /// </returns>
        /// <remarks>
        ///     Tests whether this and another object are equal in a way that will still pass
        ///     when proxy objects are being used.
        /// </remarks>
        public override bool Equals(object obj)
        {
            //Same reference check !
            if (ReferenceEquals(this, obj)) return true;

            //Type check !
            var other = obj as EventData;
            if (other == null) return false;

            if (string.IsNullOrEmpty(DataTypeCD) && string.IsNullOrEmpty(other.DataTypeCD))
                //Do a per-property search
                return
                    EventDataDetails == other.EventDataDetails;
            return Id == other.Id && DataTypeCD == other.DataTypeCD;
        }

        #endregion Equals() implementation.

        #region Constructors

        /// <summary>
        ///     Class constructor...
        /// </summary>
        public EventData()
        {
            //TODO:Write constructor stuff here..
            // _userevent = new UserEvent();
        }

        /// <summary>
        ///     The parameterized constructor.
        /// </summary>
        /// <param name="ID">The ID.</param>
        /// <param name="dataTypeCD">The data type CD.</param>
        /// <param name="eventData">The event data.</param>
        public EventData(Guid ID, string dataTypeCD, string eventData)
        {
            Id = ID;
            DataTypeCD = dataTypeCD;
            EventDataDetails = eventData;
        }

        #endregion Constructors

        #region Property Declarations

        /// <summary>
        ///     Private variable to hold the value for property DataTypeCD.
        /// </summary>
        private string _dataTypeCD;

        /// <summary>
        ///     Pkey.
        /// </summary>
        /// <value>The data type CD.</value>
        public virtual string DataTypeCD
        {
            get => _dataTypeCD;
            set => _dataTypeCD = value;
        }

        /// <summary>
        ///     Private variable to hold the value for property EventData.
        /// </summary>
        private string _eventDatalist;

        /// <summary>
        ///     conatins Event data.
        /// </summary>
        /// <value>The event data details.</value>
        public virtual string EventDataDetails
        {
            get => _eventDatalist;
            set => _eventDatalist = value;
        }

        private UserEvent _userevent;

        /// <summary>
        ///     Gets or sets the user event.
        /// </summary>
        /// <value>The user event.</value>
        public virtual UserEvent UserEvent
        {
            get => _userevent;
            set => _userevent = value;
        }

        #endregion Property Declarations
    }
}