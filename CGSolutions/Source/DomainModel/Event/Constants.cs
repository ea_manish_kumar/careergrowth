﻿using DomainModel.Base;

namespace DomainModel.Event
{
    /// <summary>
    ///     Enum states the various document event codes.
    /// </summary>
    public class EventDataTypeCD : BaseCD
    {
        /// <summary>
        ///     Sender
        /// </summary>
        public static string SenderEmailID = "SNDR";

        /// <summary>
        ///     Document ID
        /// </summary>
        public static string Document = "DOCX";

        /// <summary>
        ///     FullName of sender
        /// </summary>
        public static string SenderName = "NAME";

        /// <summary>
        ///     Company Name
        /// </summary>
        public static string SenderCompany = "COMP";

        /// <summary>
        ///     Message
        /// </summary>
        public static string Message = "MESG";

        /// <summary>
        ///     Name of Document
        /// </summary>
        public static string DoumentName = "DCNM";

        /// <summary>
        ///     Name of Document after Rename
        /// </summary>
        public static string DoumentNewName = "DCNN";

        /// <summary>
        ///     Document Format
        /// </summary>
        public static string DoumentFormat = "DCFT";

        /// <summary>
        ///     Partner User ID
        /// </summary>
        public static string PartnerUser = "PUSR";

        /// <summary>
        ///     Subscription ID
        /// </summary>
        public static string Subscription = "SUBS";

        /// <summary>
        ///     Amount
        /// </summary>
        public static string Amount = "AMNT";


        /// <summary>
        ///     SKUID
        /// </summary>
        public static string Sku = "SKUI";

        /// <summary>
        ///     CommisionType
        /// </summary>
        public static string CommisionType = "CMTY";

        /// <summary>
        ///     PRID
        /// </summary>
        public static string PartnerRID = "PRID";

        /// <summary>
        ///     OldResourceId in case of copy xml
        /// </summary>
        public static string ResourceId = "RSID";

        /// <summary>
        ///     Section ID
        /// </summary>
        public static string Section = "SCTN";

        /// <summary>
        ///     Product
        /// </summary>
        public static string Product = "PROD";

        /// <summary>
        ///     From
        /// </summary>
        public static string From = "FROM";
    }

    public class ExternalSiteView
    {
        /// <summary>
        ///     FacebookApp
        /// </summary>
        public static string FacebookApp = "FBAPP";
    }

    /// <summary>
    ///     PartnerUserEventStatusCD
    /// </summary>
    public class PartnerUserEventStatusCD
    {
        /// <summary>
        ///     NewNotification
        /// </summary>
        public static string NewNotification = "NEWN";

        /// <summary>
        ///     PostedToPostDataItem
        /// </summary>
        public static string PostedToPostDataItem = "RECD";

        /// <summary>
        ///     BatchSentSucessfullyToPartner
        /// </summary>
        public static string BatchSentSucessfullyToPartner = "OKAY";

        /// <summary>
        ///     ErrorDuringNotification
        /// </summary>
        public static string ErrorDuringNotification = "ERRR";
    }

    /// <summary>
    ///     MaxPartnerNotificationPerBatch
    /// </summary>
    public class MaxPartnerNotificationPerBatch
    {
        /// <summary>
        ///     MAX_PARTNER_NOTIFICATIONS_PER_BATCH
        /// </summary>
        public static string MAX_PARTNER_NOTIFICATIONS_PER_BATCH = "PartnerNotification";
    }

    /// <summary>
    ///     Classs to define Event Code corresponding to EventCode Table
    /// </summary>
    public class EventCodeCD
    {
        //====================DOCUMENT EVENTS=========================

        /// <summary>
        ///     Shared Resource Edited
        /// </summary>
        public const string SharedResourceEdited = "SHED";

        /// <summary>
        ///     Shared Resource Created
        /// </summary>
        public const string SharedResourceCreated = "SHRC";

        /// <summary>
        ///     Document Creation Aborted
        /// </summary>
        public const string DocumentCreationAborted = "DCAB";

        /// <summary>
        ///     Document Renamed
        /// </summary>
        public const string DocumentRenamed = "DCRN";

        /// <summary>
        ///     DocumentViewedOnlinebyNewUser
        /// </summary>
        public const string DocumentViewedOnlinebyNewUser = "DVON";

        /// <summary>
        ///     DocumentViewedOnlinebyReurningUser
        /// </summary>
        public const string DocumentViewedOnlinebyReurningUser = "DVOR";

        /// <summary>
        ///     DocumentViewedOffline
        /// </summary>
        public const string DocumentViewedOffline = "DVOF";

        /// <summary>
        ///     Document Created
        /// </summary>
        public const string DocumentCreated = "DCCR";

        /// <summary>
        ///     DocumentUpdated
        /// </summary>
        public const string DocumentUpdated = "DCUP";

        /// <summary>
        ///     DocumentDownloaded code  "DCDN"
        ///     reverted the code
        /// </summary>
        public const string DocumentDownloaded = "DCDN";

        /// <summary>
        ///     DocumentEmailed code  "DCEM"
        ///     reverted the code
        /// </summary>
        public const string DocumentEmailed = "DCEM";

        /// <summary>
        ///     DocumentDeleted code "DCDL"
        ///     reverted the code
        /// </summary>
        public const string DocumentDeleted = "DCDL";

        /// <summary>
        ///     ResumeMadeOnline
        /// </summary>
        public const string ResumeMadeOnline = "DONL";

        /// <summary>
        ///     ResumeMadeOffline
        /// </summary>
        public const string ResumeMadeOffline = "DOFF";

        /// <summary>
        ///     EmailSent
        /// </summary>
        public const string EmailSent = "EMSN";

        /// <summary>
        ///     EmailReceived
        /// </summary>
        public const string EmailReceived = "EMRC";

        /// <summary>
        ///     FaxSent
        /// </summary>
        public const string FaxSent = "FXSN";

        /// <summary>
        ///     PageViewed
        /// </summary>
        public const string PageViewed = "PGVW";

        /// <summary>
        ///     EmailSentwithDocument
        /// </summary>
        public const string EmailSentwithDocument = "EMSD";

        /// <summary>
        ///     FaxSentwithDocument
        /// </summary>
        public const string FaxSentwithDocument = "FXSD";

        /// <summary>
        ///     UserContacted
        /// </summary>
        public const string UserContacted = "UCNT";

        /// <summary>
        ///     Document requestedfrom Facebook share functionality
        /// </summary>
        public const string DocumentrequestedfromFacebooksharefunctionality = "DVFB";

        //New Events

        /// <summary>
        ///     Partner Document Deleted
        /// </summary>
        public const string PartnerDocumentDeleted = "PDDL";

        /// <summary>
        ///     Document Copied
        /// </summary>
        public const string DocumentCopied = "DCCP";

        /// <summary>
        ///     FaceBook Access Authorized
        /// </summary>
        public const string FaceBookAccessAuthorized = "FBAU";

        /// <summary>
        ///     FaceBook TabRemoved
        /// </summary>
        public const string FaceBookTabRemoved = "FBTR";

        /// <summary>
        ///     Document Sharedon FaceBook
        /// </summary>
        public const string DocumentSharedonFaceBook = "DSFB";

        /// <summary>
        ///     Document Archived (Document Deactivated)
        /// </summary>
        public const string DocumentArchived = "DCAR";

        /// <summary>
        ///     Document Activated
        /// </summary>
        public const string DocumentActivated = "DCAC";

        /// <summary>
        ///     User Subscribed
        /// </summary>
        public const string UserSubscribed = "USCR";

        /// <summary>
        ///     Subscription Cancelled
        /// </summary>
        public const string SubscriptionCancelled = "SCAN";

        /// <summary>
        ///     Subscription Cancelled by User
        /// </summary>
        public const string SubscriptionCancelledUser = "UCAN";

        /// <summary>
        ///     Subscription Cancelled by admin
        /// </summary>
        public const string SubscriptionCancelledAdmin = "ACAN";

        /// <summary>
        ///     Recurring Subscription
        /// </summary>
        public const string RecurringSubscription = "RECR";

        /// <summary>
        ///     Document Uploaded
        /// </summary>
        public const string DocumentUploaded = "DCUL";

        /// <summary>
        ///     Billing Transaction Refunded
        /// </summary>
        public const string BillingTransactionRefunded = "RFND";

        /// <summary>
        ///     InComplete Shared Resource Created
        /// </summary>
        public const string InCompleteSharedResourceCreated = "SHAB";

        /// <summary>
        ///     Subscription Terminated
        /// </summary>
        public const string SubscriptionTerminated = "STRM";

        /// <summary>
        ///     Commission
        /// </summary>
        public const string Commission = "COMM";


        /// <summary>
        ///     Test Tuner clicked
        /// </summary>
        public const string TexttunerClick = "TTCL";


        /// <summary>
        ///     Swif Downloading Time
        /// </summary>
        public const string SwifDownloaded = "SDWT";

        /// <summary>
        ///     Document Visibility Changed
        /// </summary>
        public const string DocumentVisibilityChanged = "DPVS";

        /// <summary>
        ///     Shared Document Activated
        /// </summary>
        public const string SharedDocumentActivated = "DACT";

        /// <summary>
        ///     Shared Document Deactivated
        /// </summary>
        public const string SharedDocumentDeactivated = "DDAC";


        /// <summary>
        ///     Shared Document Copied
        /// </summary>
        public const string SharedDocumentCopied = "DCOP";

        /// <summary>
        ///     Resume Submitted for Writing
        /// </summary>
        public const string ResumeSubmittedforWriting = "RWSM";

        /// <summary>
        ///     covering letter Submitted for Writing
        /// </summary>
        public const string LetterSubmittedforWriting = "LWSM";

        /// <summary>
        ///     Resume Writing Completed
        /// </summary>
        public const string ResumeWritingCompleted = "RWCM";

        /// <summary>
        ///     Resume Writing Viewed
        /// </summary>
        public const string ResumeWritingViewed = "RWVW";

        /// <summary>
        ///     Sell Page Viewed
        /// </summary>
        public const string SellPageViewed = "SPVD";

        /// <summary>
        ///     Print
        /// </summary>
        public const string PrintDocument = "PRNT";
    }
}