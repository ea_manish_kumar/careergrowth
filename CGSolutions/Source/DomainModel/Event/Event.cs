﻿using System;
using System.Text;

namespace DomainModel.Event
{
    [Serializable]
    public abstract class Event : EntityBase<Guid, Event>
    {
        #region Constructors

        #endregion

        #region ToString() Implementation

        public new virtual string ToString()
        {
            var sb = new StringBuilder(4);

            sb.AppendLine("EventID: " + base.ToString());
            sb.AppendLine("Timestamp: " + Timestamp);
            if (EventCode != null)
                sb.AppendLine("EventCode: " + EventCode.ToString());

            return sb.ToString();
        }

        #endregion ToString() Implementation

        #region Property Declarations

        ///// <summary>
        ///// Private variable to hold the value for property EventID.
        ///// </summary>
        //private int _eventID;

        /// <summary>
        ///     the primary key.
        /// </summary>
        /// <value>The event ID.</value>
        public virtual Guid EventID
        {
            get => Id;
            set => Id = value;
        }


        /// <summary>
        ///     Private variable to hold the value for property Timestamp.
        /// </summary>
        private DateTime _timestamp;

        /// <summary>
        ///     Time stamp.
        /// </summary>
        /// <value>The timestamp.</value>
        public virtual DateTime Timestamp
        {
            get => _timestamp;
            set => _timestamp = value;
        }

        /// <summary>
        ///     Private variable to hold the value for property EventCode.
        /// </summary>
        private EventCode _eventCode;

        /// <summary>
        ///     The related event code..
        /// </summary>
        /// <value>The event code.</value>
        public virtual EventCode EventCode
        {
            get => _eventCode;
            set => _eventCode = value;
        }

        #endregion Property Declarations
    }
}