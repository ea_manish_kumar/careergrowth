﻿using System;

namespace DomainModel.Base.UtilityClasses
{
    /// <summary>
    ///     Represents a Date Range (sort of like TimeSpan)
    /// </summary>
    public class DateRange
    {
        #region Private Helper Methods

        /// <summary>
        ///     If the Month passed in has already passed in the current year, then the Current Year is returned,
        ///     Otherwise the Previous Year is returned
        /// </summary>
        /// <param name="month"></param>
        /// <returns></returns>
        private int GetYearForMonth(Month month)
        {
            //If the current Month is greater than the passed in month then return the Current Year
            if (DateTime.Now.Month >= Convert.ToInt16(month))
                return DateTime.Now.Year;
            //Otherwise return the Previous Year
            return DateTime.Now.Year - 1;
        }

        #endregion

        #region Declarations

        private DateTime _startdate, _enddate;
        private Month _FirstMonth = Month.None;

        #endregion

        #region Constructors

        /// <summary>
        ///     Default constructor assumes a ALL TIME date range or Maximum allowed time
        /// </summary>
        public DateRange()
        {
            _startdate = new DateTime(1900, 1, 1);
            _enddate = new DateTime(2050, 1, 1);
        }

        /// <summary>
        ///     Sets the StartDate and sets end date to 1 day from the StartDate
        ///     To use the TODAY daterange, pass in DateTime.Today to this constructor
        /// </summary>
        /// <param name="startdate">The startdate.</param>
        public DateRange(DateTime startdate)
        {
            _startdate = startdate;
            _enddate = _startdate.Add(new TimeSpan(1, 0, 0, 0));
        }

        /// <summary>
        ///     Explicitly set the date range dates
        /// </summary>
        /// <param name="pStartDate">The p start date.</param>
        /// <param name="pEndDate">The p end date.</param>
        public DateRange(DateTime pStartDate, DateTime pEndDate)
        {
            _startdate = pStartDate;
            _enddate = pEndDate;
        }

        /// <summary>
        ///     Sets a Date Range from X days ago till Today
        /// </summary>
        /// <param name="LastXDays">The last X days.</param>
        public DateRange(int LastXDays)
        {
            _enddate = DateTime.Now;
            _startdate = _enddate.Subtract(new TimeSpan(LastXDays, 0, 0, 0));
        }

        /// <summary>
        ///     Sets a Date range for the specified month
        /// </summary>
        /// <param name="month">The month.</param>
        public DateRange(Month month)
        {
            //Set the FirstMonth property
            FirstMonth = month;

            var Year = GetYearForMonth(month);

            //Set the StartDate and EndDate
            switch (month)
            {
                case Month.January:
                {
                    _startdate = new DateTime(Year, 1, 1);
                    _enddate = new DateTime(Year, 1, 31, 23, 59, 59);
                    break;
                }
                case Month.February:
                {
                    var FebEndDate = 28;
                    if (DateTime.IsLeapYear(Year))
                        FebEndDate = 29;

                    _startdate = new DateTime(Year, 2, 1);
                    _enddate = new DateTime(Year, 2, FebEndDate, 23, 59, 59);
                    break;
                }
                case Month.March:
                {
                    _startdate = new DateTime(Year, 3, 1);
                    _enddate = new DateTime(Year, 3, 31, 23, 59, 59);
                    break;
                }
                case Month.April:
                {
                    _startdate = new DateTime(Year, 4, 1);
                    _enddate = new DateTime(Year, 4, 30, 23, 59, 59);
                    break;
                }
                case Month.May:
                {
                    _startdate = new DateTime(Year, 5, 1);
                    _enddate = new DateTime(Year, 5, 31, 23, 59, 59);
                    break;
                }
                case Month.June:
                {
                    _startdate = new DateTime(Year, 6, 1);
                    _enddate = new DateTime(Year, 6, 30, 23, 59, 59);
                    break;
                }
                case Month.July:
                {
                    _startdate = new DateTime(Year, 7, 1);
                    _enddate = new DateTime(Year, 7, 31, 23, 59, 59);
                    break;
                }
                case Month.August:
                {
                    _startdate = new DateTime(Year, 8, 1);
                    _enddate = new DateTime(Year, 8, 31, 23, 59, 59);
                    break;
                }
                case Month.September:
                {
                    _startdate = new DateTime(Year, 9, 1);
                    _enddate = new DateTime(Year, 9, 30, 23, 59, 59);
                    break;
                }
                case Month.October:
                {
                    _startdate = new DateTime(Year, 10, 1);
                    _enddate = new DateTime(Year, 10, 31, 23, 59, 59);
                    break;
                }
                case Month.November:
                {
                    _startdate = new DateTime(Year, 11, 1);
                    _enddate = new DateTime(Year, 11, 30, 23, 59, 59);
                    break;
                }
                case Month.December:
                {
                    _startdate = new DateTime(Year, 12, 1);
                    _enddate = new DateTime(Year, 12, 31, 23, 59, 59);
                    break;
                }
            }
        }

        #endregion

        #region Property Declarations

        /// <summary>
        ///     Gets or sets the start date.
        /// </summary>
        /// <value>The start date.</value>
        public DateTime StartDate
        {
            get => _startdate;
            set
            {
                if (value > _enddate)
                    throw new ArgumentException("Start Date cannot be greater than End Date");

                _startdate = value;
            }
        }

        /// <summary>
        ///     Gets or sets the end date.
        /// </summary>
        /// <value>The end date.</value>
        public DateTime EndDate
        {
            get => _enddate;
            set
            {
                if (value < _startdate)
                    throw new ArgumentException("End Date cannot be lesser than Start Date");

                _enddate = value;
            }
        }

        /// <summary>
        ///     The Month of the StartDate
        /// </summary>
        /// <value>The first month.</value>
        public Month FirstMonth
        {
            get
            {
                if (_FirstMonth == Month.None)
                    _FirstMonth = (Month) StartDate.Month;

                return _FirstMonth;
            }
            set => _FirstMonth = value;
        }

        /// <summary>
        ///     The Total Number Of Days in the Date Range
        /// </summary>
        /// <value>The number of days.</value>
        public int NumberOfDays => EndDate.Subtract(StartDate).Days;

        /// <summary>
        ///     Represents the Period over which the Count values have been aggregated
        /// </summary>
        /// <value>The type of the period.</value>
        public virtual PeriodType PeriodType
        {
            get
            {
                PeriodType PeriodType;

                if (NumberOfDays == 1)
                    PeriodType = PeriodType.OneDay;
                else if (NumberOfDays == 30 || NumberOfDays == 31)
                    PeriodType = PeriodType.Month;
                else if (NumberOfDays > 10000)
                    PeriodType = PeriodType.AllTime;
                else
                    PeriodType = PeriodType.Days;

                return PeriodType;
            }
        }

        #endregion
    }
}