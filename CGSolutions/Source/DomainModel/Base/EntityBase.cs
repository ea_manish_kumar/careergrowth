using System;

namespace DomainModel
{
    /// <summary>
    /// </summary>
    /// <typeparam name="T"></typeparam>
    /// <typeparam name="U"></typeparam>
    public interface IEntityBase<T, U>
    {
        // Properties
        /// <summary>
        ///     Gets the id.
        /// </summary>
        /// <value>The id.</value>
        T Id { get; }

        // Methods
        /// <summary>
        ///     Gets the hash code.
        /// </summary>
        /// <returns></returns>
        int GetHashCode();

        /// <summary>
        ///     Equalses the specified obj.
        /// </summary>
        /// <param name="obj">The obj.</param>
        /// <returns></returns>
        bool Equals(object obj);

        /// <summary>
        ///     Gets the clone.
        /// </summary>
        /// <param name="isForFlex">if set to <c>true</c> [is for flex].</param>
        /// <param name="IgnoreIDs">if set to <c>true</c> [ignore I ds].</param>
        /// <returns></returns>
        U GetClone(bool isForFlex, bool IgnoreIDs);
    }

    /// <summary>
    ///     Base for all business objects.
    ///     For an explanation of why Equals and GetHashCode are overriden, read the following...
    ///     http://devlicio.us/blogs/billy_mccafferty/archive/2007/04/25/using-equals-gethashcode-effectively.aspx
    ///     Added IComparable implementation :VJ 20/08/2013
    /// </summary>
    /// <typeparam name="T">DataType of the primary key.</typeparam>
    /// <typeparam name="U">Type of the object</typeparam>
    [Serializable]
    public abstract class EntityBase<T, U> : IEntityBase<T, U>, IComparable //, IDisposable
    {
        #region IComparable Members

        int IComparable.CompareTo(object Input)
        {
            var baseInputType = (EntityBase<T, U>) Input;
            if (Id is int)
                return Convert.ToInt32(Id).CompareTo(baseInputType.Id);
            if (Id is string)
                return Id.ToString().CompareTo(baseInputType.Id);
            throw new ArgumentOutOfRangeException("Id other than of type int/string not supported.");
        }

        #endregion

        #region Tostring

        public override string ToString()
        {
            if (Id != null)
                return "ID : " + Id;
            return "ID : " + "<NULL>";
        }

        #endregion

        #region Declarations

        private T _id;
        private int? _hashcodevalue = default(int?);

        // Track whether Dispose has been called. 
        //private bool disposed = false;

        #endregion

        #region Methods

        #region GetHashCode() implementation.

        public override int GetHashCode()
        {
            //If the HashCodeValue has been set explicitly then just return that
            //No need to calculate the value
            if (_hashcodevalue.HasValue)
                return (int) _hashcodevalue;

            //Moved GetHashCode() implementation to entitybase
            if (Id != null)
                return (GetType().FullName + Id).GetHashCode();
            return GetType().FullName.GetHashCode();
        }

        #endregion GetHashCode() Implementation


        public override bool Equals(object obj)
        {
            if (ReferenceEquals(this, obj)) return true;

            return obj != null // 1) Object is not null.
                   && ((EntityBase<T, U>) obj).Id != null // 1) Object.Id is not null.
                   && obj.GetType() == GetType() // 2) Object is of same Type.
                   && (MatchingIds((EntityBase<T, U>) obj) || MatchingHashCodes(obj)); // 3) Ids or Hashcodes match.
        }

        /// <summary>
        ///     Matchings the ids.
        /// </summary>
        /// <param name="obj">The Input.</param>
        /// <returns></returns>
        private bool MatchingIds(EntityBase<T, U> obj)
        {
            return Id != null && !Id.Equals(default(T)) && obj.Id != null && !obj.Id.Equals(default(T)) &&
                   Id.Equals(obj.Id); // 2) Ids match.
        }

        /// <summary>
        ///     Matchings the hash codes.
        /// </summary>
        /// <param name="obj">The Input.</param>
        /// <returns></returns>
        private bool MatchingHashCodes(object obj)
        {
            return GetHashCode().Equals(obj.GetHashCode()); // 1) Hashcodes match.             
        }

        /// <summary>
        ///     Gets the clone.
        /// </summary>
        /// <param name="isForFlex">if set to <c>true</c> [is for flex].</param>
        /// <param name="IgnoreIDs">if set to <c>true</c> [ignore I ds].</param>
        /// <returns></returns>
        public virtual U GetClone(bool isForFlex, bool IgnoreIDs)
        {
            throw new NotImplementedException("GetClone() not implemented.");
        }

        /// <summary>
        ///     CAUTION - USE THIS METHOD ONLY IF YOU'RE VERY SURE OF WHAT YOU'RE DOING!
        ///     Use this method to override the default value calcluated by the GetHashCode method
        /// </summary>
        /// <param name="HashCodeValue">The hash code value.</param>
        public virtual void SetHashCode(int HashCodeValue)
        {
            _hashcodevalue = HashCodeValue;
        }

        #endregion

        #region Properties

        /// <summary>
        ///     Gets or sets the id.
        /// </summary>
        /// <value>The id.</value>
        public virtual T Id
        {
            get => _id;
            set => _id = value;
        }


        /// <summary>
        ///     Gets the type.
        /// </summary>
        /// <value>The type of this .</value>
        public virtual Type Type => typeof(U);

        #endregion
    }
}