﻿namespace DomainModel.Base.UtilityClasses
{
    public static class SqlQueries
    {
        public const string UpdateUserDataByDataTpeCode =
            "UPDATE [dbo].[UserData] SET [Data] = @NewData WHERE [UserID] = @UserID AND [DataTypeCD] = @DataTypeCD AND [Data] = @OldData";

        public const string InsertEComTransactionDetails =
            "INSERT INTO [dbo].[EComTransactionDetails] ([UserId],[ReferenceNumber],[oAuthToken],[ClientCD],[IsSandBoxMode],[EcomResponse],[Request],[Response],[TimeStamp]) VALUES (@UserId,@ReferenceNumber,@oAuthToken,@ClientCD,@IsSandBoxMode,@EcomResponse,@Request,@Response,@TimeStamp)";
    }
}