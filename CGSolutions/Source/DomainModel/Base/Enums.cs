﻿namespace DomainModel.Base
{
    public enum OrderBy
    {
        /// <summary>
        ///     Ascending
        /// </summary>
        Ascending,

        /// <summary>
        ///     Descending
        /// </summary>
        Descending
    }

    public enum Month : short
    {
        /// <summary>
        ///     Default - None
        /// </summary>
        None = 0,

        /// <summary>
        ///     January
        /// </summary>
        January = 1,

        /// <summary>
        ///     February
        /// </summary>
        February = 2,

        /// <summary>
        ///     March
        /// </summary>
        March = 3,

        /// <summary>
        ///     April
        /// </summary>
        April = 4,

        /// <summary>
        ///     May
        /// </summary>
        May = 5,

        /// <summary>
        ///     June
        /// </summary>
        June = 6,

        /// <summary>
        ///     July
        /// </summary>
        July = 7,

        /// <summary>
        ///     August
        /// </summary>
        August = 8,

        /// <summary>
        ///     September
        /// </summary>
        September = 9,

        /// <summary>
        ///     October
        /// </summary>
        October = 10,

        /// <summary>
        ///     November
        /// </summary>
        November = 11,

        /// <summary>
        ///     December
        /// </summary>
        December = 12
    }

    /// <summary>
    ///     Enum declares various Periods that a DateRange object might encompass
    /// </summary>
    public enum PeriodType
    {
        /// <summary>
        ///     1 Day
        /// </summary>
        OneDay = 1,

        /// <summary>
        ///     Multiple Days
        /// </summary>
        Days = 2,

        /// <summary>
        ///     1 Month
        /// </summary>
        Month = 3,

        /// <summary>
        ///     All Time
        /// </summary>
        AllTime = 5
    }

    /// <summary>
    ///     The various Paper Sizes available for Printing
    /// </summary>
    public enum PaperSize
    {
        /// <summary>
        ///     A4
        /// </summary>
        A4 = 1,

        /// <summary>
        ///     A3
        /// </summary>
        A3 = 2,

        /// <summary>
        ///     Letter
        /// </summary>
        Letter = 3,

        /// <summary>
        ///     Legal
        /// </summary>
        Legal = 4
    }
}