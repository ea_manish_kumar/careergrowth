﻿namespace DomainModel.Base
{
    /// <summary>
    ///     The base struct for all TypeCD structs
    /// </summary>
    public abstract class BaseCD
    {
        /// <summary>
        ///     Signifies a Filter of "ALL" typeCDs
        /// </summary>
        public const string All = "All";
    }
}