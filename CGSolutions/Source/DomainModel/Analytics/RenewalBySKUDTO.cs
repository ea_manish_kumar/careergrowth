﻿using System;
using System.Text;

namespace DomainModel.Analytics
{
    [Serializable]
    public class RenewalBySKUDTO
    {
        #region Constructors

        #endregion

        #region ToString() Implementation

        /// <summary>
        ///     This methods returns a string representation of this class.
        /// </summary>
        /// <returns>A string representation of this class.</returns>
        public new virtual string ToString()
        {
            var sb = new StringBuilder(7);

            //sb.AppendLine("------------RenewalBySKUDTO dump starts---------------");
            //sb.AppendLine("Contact: " + (this.Contact != null ? this.Contact.ToString() : "<NULL>").ToString());
            //sb.AppendLine("DateValue: " + (this.DateValue != null ? this.DateValue.ToString() : "<NULL>").ToString());
            //sb.AppendLine("SUBSS: " + this.SUBSS.ToString());

            sb.AppendLine("------------RenewalBySKUDTO dump ends---------------");

            return sb.ToString();
        }

        #endregion ToString() Implementation

        #region GetClone() Implementation

        /// <summary>
        ///     This methods returns a cloned instance of this class.
        /// </summary>
        /// <returns>A string cloned instance of this class.</returns>
        public virtual RenewalBySKUDTO GetClone()
        {
            var clone = new RenewalBySKUDTO();
            clone.Period = Period;
            clone.Initial = Initial;
            clone.Renewal1 = Renewal1;

            return clone;
        }

        #endregion GetClone() Implementation

        #region GetHashCode() implementation.

        /// <summary>
        ///     Serves as a hash function for a particular type.
        /// </summary>
        /// <returns>
        ///     A hash code for the current RenewalBySKUDTO.
        /// </returns>
        public override int GetHashCode()
        {
            return GetType().FullName.GetHashCode();
        }

        #endregion GetHashCode() Implementation

        #region Equals() implementation.

        /// <summary>
        ///     Determines whether the specified RenewalBySKUDTO is equal to the current RenewalBySKUDTO.
        /// </summary>
        /// <param name="obj">The RenewalBySKUDTO to compare with the current RenewalBySKUDTO.</param>
        /// <returns>
        ///     true if the specified RenewalBySKUDTO is equal to the current RenewalBySKUDTO; otherwise, false.
        /// </returns>
        /// <remarks>
        ///     Tests whether this and another object are equal in a way that will still pass
        ///     when proxy objects are being used.
        /// </remarks>
        public override bool Equals(object obj)
        {
            //Same reference check !
            //if (object.ReferenceEquals(this, obj)) return true;

            ////Type check !
            //CardTypes other = obj as CardTypes;
            //if (other == null) return false;

            //if ((Id == 0))
            //    //Do a per-property search
            //    return
            //    (CardType == other.CardType);
            //else
            //    return Id == other.Id;

            return true;
        }

        #endregion Equals() Implementation

        #region Property Declarations

        public virtual string Period { get; set; }
        public virtual string Initial { get; set; }

        public virtual string Renewal1 { get; set; }
        public virtual string Renewal2 { get; set; }
        public virtual string Renewal3 { get; set; }
        public virtual string Renewal4 { get; set; }
        public virtual string Renewal5 { get; set; }
        public virtual string Renewal6 { get; set; }
        public virtual string Renewal7 { get; set; }
        public virtual string Renewal8 { get; set; }
        public virtual string Renewal9 { get; set; }
        public virtual string Renewal10 { get; set; }
        public virtual string Renewal11 { get; set; }
        public virtual string Renewal12 { get; set; }

        #endregion Property Declarations
    }
}