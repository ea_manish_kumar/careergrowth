﻿using System;
using System.Text;

namespace DomainModel.Analytics
{
    [Serializable]
    public class SKUPurchaseDTO
    {
        #region Constructors

        #endregion

        #region ToString() Implementation

        /// <summary>
        ///     This methods returns a string representation of this class.
        /// </summary>
        /// <returns>A string representation of this class.</returns>
        public new virtual string ToString()
        {
            var sb = new StringBuilder(7);

            sb.AppendLine("------------SKUPurchaseDTO dump starts---------------");
            sb.AppendLine("SKU: " + (SKU != null ? SKU : "<NULL>"));
            sb.AppendLine("Purchase: " + Purchase);
            sb.AppendLine("Revenue: " + Revenue);

            sb.AppendLine("------------SKUPurchaseDTO dump ends---------------");

            return sb.ToString();
        }

        #endregion ToString() Implementation

        #region GetClone() Implementation

        /// <summary>
        ///     This methods returns a cloned instance of this class.
        /// </summary>
        /// <returns>A string cloned instance of this class.</returns>
        public virtual SKUPurchaseDTO GetClone()
        {
            var clone = new SKUPurchaseDTO();

            clone.SKU = SKU;
            clone.Purchase = Purchase;
            clone.Revenue = Revenue;
            return clone;
        }

        #endregion GetClone() Implementation

        #region GetHashCode() implementation.

        /// <summary>
        ///     Serves as a hash function for a particular type.
        /// </summary>
        /// <returns>
        ///     A hash code for the current SKUPurchaseDTO.
        /// </returns>
        public override int GetHashCode()
        {
            return GetType().FullName.GetHashCode();
        }

        #endregion GetHashCode() Implementation

        #region Equals() implementation.

        /// <summary>
        ///     Determines whether the specified SKUPurchaseDTO is equal to the current SKUPurchaseDTO.
        /// </summary>
        /// <param name="obj">The SKUPurchaseDTO to compare with the current SKUPurchaseDTO.</param>
        /// <returns>
        ///     true if the specified SKUPurchaseDTO is equal to the current SKUPurchaseDTO; otherwise, false.
        /// </returns>
        /// <remarks>
        ///     Tests whether this and another object are equal in a way that will still pass
        ///     when proxy objects are being used.
        /// </remarks>
        public override bool Equals(object obj)
        {
            //Same reference check !
            //if (object.ReferenceEquals(this, obj)) return true;

            ////Type check !
            //CardTypes other = obj as CardTypes;
            //if (other == null) return false;

            //if ((Id == 0))
            //    //Do a per-property search
            //    return
            //    (CardType == other.CardType);
            //else
            //    return Id == other.Id;

            return true;
        }

        #endregion Equals() Implementation

        #region Property Declarations

        /// <summary>
        ///     Private variable to hold the value for property SKU.
        /// </summary>
        private string _sku = string.Empty;


        public virtual string SKU { get; set; }
        public virtual int Purchase { get; set; }
        public virtual decimal? Revenue { get; set; }

        #endregion Property Declarations
    }
}