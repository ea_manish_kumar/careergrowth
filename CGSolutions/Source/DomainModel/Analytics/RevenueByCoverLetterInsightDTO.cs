﻿using System;
using System.Text;

namespace DomainModel.Analytics
{
    [Serializable]
    public class RevenueByCoverLetterInsightDTO
    {
        #region Constructors

        #endregion

        #region ToString() Implementation

        /// <summary>
        ///     This methods returns a string representation of this class.
        /// </summary>
        /// <returns>A string representation of this class.</returns>
        public new virtual string ToString()
        {
            var sb = new StringBuilder(7);

            sb.AppendLine("------------RevenueByRgCLInsightDTO dump starts---------------");
            sb.AppendLine("NewClicks: " + (NewClicks != null ? NewClicks : "<NULL>"));
            sb.AppendLine("DateValue: " + (DateValue != null ? DateValue.ToString() : "<NULL>"));
            sb.AppendLine("Subscription: " + Subscription);

            sb.AppendLine("------------RevenueByRgCLInsightDTO dump ends---------------");

            return sb.ToString();
        }

        #endregion ToString() Implementation

        #region GetClone() Implementation

        /// <summary>
        ///     This methods returns a cloned instance of this class.
        /// </summary>
        /// <returns>A string cloned instance of this class.</returns>
        public virtual RevenueByCoverLetterInsightDTO GetClone()
        {
            var clone = new RevenueByCoverLetterInsightDTO();

            clone.NewClicks = NewClicks;
            clone.DateValue = DateValue;
            clone.Subscription = Subscription;

            return clone;
        }

        #endregion GetClone() Implementation

        #region GetHashCode() implementation.

        /// <summary>
        ///     Serves as a hash function for a particular type.
        /// </summary>
        /// <returns>
        ///     A hash code for the current RevenueByProductDTO.
        /// </returns>
        public override int GetHashCode()
        {
            return GetType().FullName.GetHashCode();
        }

        #endregion GetHashCode() Implementation

        #region Equals() implementation.

        /// <summary>
        ///     Determines whether the specified RevenueByRgCLInsightDTO is equal to the current RevenueByProductDTO.
        /// </summary>
        /// <param name="obj">The RevenueByRgCLInsightDTO to compare with the current RevenueByProductDTO.</param>
        /// <returns>
        ///     true if the specified RevenueByRgCLInsightDTO is equal to the current RevenueByProductDTO; otherwise, false.
        /// </returns>
        /// <remarks>
        ///     Tests whether this and another object are equal in a way that will still pass
        ///     when proxy objects are being used.
        /// </remarks>
        public override bool Equals(object obj)
        {
            //Same reference check !
            //if (object.ReferenceEquals(this, obj)) return true;

            ////Type check !
            //CardTypes other = obj as CardTypes;
            //if (other == null) return false;

            //if ((Id == 0))
            //    //Do a per-property search
            //    return
            //    (CardType == other.CardType);
            //else
            //    return Id == other.Id;

            return true;
        }

        #endregion Equals() Implementation

        #region Property Declarations

        public virtual string NewClicks { get; set; }
        public virtual string TotalRegistered { get; set; }
        public virtual string Finalize { get; set; }
        public virtual string ResumeHome { get; set; }
        public virtual string Subscription { get; set; }
        public virtual string CLBeforSubscription { get; set; }
        public virtual string CLAfterSubscription { get; set; }
        public virtual DateTime? DateValue { get; set; }

        #endregion Property Declarations
    }
}