﻿using System;
using System.Text;

namespace DomainModel.Analytics
{
    [Serializable]
    public class ReportByCverLetterFunnelDTO //: EntityBase<int, RevenueByProductDTO>
    {
        #region Constructors

        #endregion

        #region ToString() Implementation

        /// <summary>
        ///     This methods returns a string representation of this class.
        /// </summary>
        /// <returns>A string representation of this class.</returns>
        public new virtual string ToString()
        {
            var sb = new StringBuilder(7);

            sb.AppendLine("------------ReportByCLFunnelDTO dump starts---------------");
            sb.AppendLine("Contact: " + (Contact != null ? Contact : "<NULL>"));
            sb.AppendLine("DateValue: " + (DateValue != null ? DateValue : "<NULL>"));

            sb.AppendLine("------------ReportByCLFunnelDTO dump ends---------------");

            return sb.ToString();
        }

        #endregion ToString() Implementation

        #region GetClone() Implementation

        /// <summary>
        ///     This methods returns a cloned instance of this class.
        /// </summary>
        /// <returns>A string cloned instance of this class.</returns>
        public virtual ReportByCverLetterFunnelDTO GetClone()
        {
            var clone = new ReportByCverLetterFunnelDTO();
            clone.Contact = Contact;
            clone.DateValue = DateValue;
            return clone;
        }

        #endregion GetClone() Implementation

        #region GetHashCode() implementation.

        /// <summary>
        ///     Serves as a hash function for a particular type.
        /// </summary>
        /// <returns>
        ///     A hash code for the current RevenueByProductDTO.
        /// </returns>
        public override int GetHashCode()
        {
            return GetType().FullName.GetHashCode();
        }

        #endregion GetHashCode() Implementation

        #region Equals() implementation.

        /// <summary>
        ///     Determines whether the specified ReportByCLFunnelDTO is equal to the current RevenueByProductDTO.
        /// </summary>
        /// <param name="obj">The ReportByCLFunnelDTO to compare with the current RevenueByProductDTO.</param>
        /// <returns>
        ///     true if the specified ReportByCLFunnelDTO is equal to the current RevenueByProductDTO; otherwise, false.
        /// </returns>
        /// <remarks>
        ///     Tests whether this and another object are equal in a way that will still pass
        ///     when proxy objects are being used.
        /// </remarks>
        public override bool Equals(object obj)
        {
            //Same reference check !
            //if (object.ReferenceEquals(this, obj)) return true;

            ////Type check !
            //CardTypes other = obj as CardTypes;
            //if (other == null) return false;

            //if ((Id == 0))
            //    //Do a per-property search
            //    return
            //    (CardType == other.CardType);
            //else
            //    return Id == other.Id;

            return true;
        }

        #endregion Equals() Implementation

        #region Property Declarations

        public virtual int TotalRegistered { get; set; }
        public virtual string Finalize { get; set; }
        public virtual string ResumeHome { get; set; }
        public virtual string StyleSelected { get; set; }
        public virtual string CLType { get; set; }
        public virtual string Contact { get; set; }
        public virtual string Recipient { get; set; }
        public virtual string Subject { get; set; }
        public virtual string Greeting { get; set; }
        public virtual string Opener { get; set; }
        public virtual string CLBody { get; set; }
        public virtual string CallToAction { get; set; }
        public virtual string Closer { get; set; }
        public virtual string CLFinalize { get; set; }
        public virtual string DateValue { get; set; }

        #endregion Property Declarations
    }
}