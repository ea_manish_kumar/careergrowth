﻿using System;
using System.Text;

namespace DomainModel.Analytics
{
    [Serializable]
    public class SubscriptionSnapshotDTO
    {
        #region Constructors

        #endregion

        #region ToString() Implementation

        /// <summary>
        ///     This methods returns a string representation of this class.
        /// </summary>
        /// <returns>A string representation of this class.</returns>
        public new virtual string ToString()
        {
            var sb = new StringBuilder(7);

            sb.AppendLine("------------SubscriptionSnapshotDTO dump starts---------------");
            sb.AppendLine("SKU: " + (SKU != null ? SKU : "<NULL>"));
            sb.AppendLine("Purchase: " + (Initial.ToString() != null ? Initial.ToString() : "<NULL>"));
            sb.AppendLine("Revenue: " + TotalRevenue);

            sb.AppendLine("------------SubscriptionSnapshotDTO dump ends---------------");

            return sb.ToString();
        }

        #endregion ToString() Implementation

        #region GetClone() Implementation

        /// <summary>
        ///     This methods returns a cloned instance of this class.
        /// </summary>
        /// <returns>A string cloned instance of this class.</returns>
        public virtual SubscriptionSnapshotDTO GetClone()
        {
            var clone = new SubscriptionSnapshotDTO();

            clone.SKU = SKU;
            clone.Initial = Initial;
            clone.TotalRevenue = TotalRevenue;
            return clone;
        }

        #endregion GetClone() Implementation

        #region GetHashCode() implementation.

        /// <summary>
        ///     Serves as a hash function for a particular type.
        /// </summary>
        /// <returns>
        ///     A hash code for the current SubscriptionSnapshotDTO.
        /// </returns>
        public override int GetHashCode()
        {
            return GetType().FullName.GetHashCode();
        }

        #endregion GetHashCode() Implementation

        #region Equals() implementation.

        /// <summary>
        ///     Determines whether the specified SubscriptionSnapshotDTO is equal to the current SubscriptionSnapshotDTO.
        /// </summary>
        /// <param name="obj">The SubscriptionSnapshotDTO to compare with the current SubscriptionSnapshotDTO.</param>
        /// <returns>
        ///     true if the specified SubscriptionSnapshotDTO is equal to the current SubscriptionSnapshotDTO; otherwise, false.
        /// </returns>
        /// <remarks>
        ///     Tests whether this and another object are equal in a way that will still pass
        ///     when proxy objects are being used.
        /// </remarks>
        public override bool Equals(object obj)
        {
            //Same reference check !
            //if (object.ReferenceEquals(this, obj)) return true;

            ////Type check !
            //CardTypes other = obj as CardTypes;
            //if (other == null) return false;

            //if ((Id == 0))
            //    //Do a per-property search
            //    return
            //    (CardType == other.CardType);
            //else
            //    return Id == other.Id;

            return true;
        }

        #endregion Equals() Implementation

        #region Property Declarations

        public virtual string SKU { get; set; }
        public virtual decimal? ListPrice { get; set; }
        public virtual string Schedule { get; set; }
        public virtual int Actv { get; set; }
        public virtual int Cgbk { get; set; }
        public virtual int Rtrn { get; set; }
        public virtual int Susp { get; set; }
        public virtual int Expr { get; set; }
        public virtual int Canc { get; set; }
        public virtual int Rtry { get; set; }
        public virtual int Errr { get; set; }
        public virtual int Total { get; set; }
        public virtual decimal? Initial { get; set; }
        public virtual decimal? Refund { get; set; }
        public virtual decimal? Reverse { get; set; }
        public virtual decimal? TotalRevenue { get; set; }
        public virtual decimal? RevenuePerSubs { get; set; }

        #endregion Property Declarations
    }
}