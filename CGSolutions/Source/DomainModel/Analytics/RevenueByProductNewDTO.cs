﻿using System;
using System.Text;

namespace DomainModel.Analytics
{
    [Serializable]
    public class RevenueByProductNewDTO
    {
        #region Constructors

        #endregion

        #region ToString() Implementation

        /// <summary>
        ///     This methods returns a string representation of this class.
        /// </summary>
        /// <returns>A string representation of this class.</returns>
        public new virtual string ToString()
        {
            var sb = new StringBuilder(7);

            sb.AppendLine("------------RevenueByProductDTO dump starts---------------");
            sb.AppendLine("Contact: " + (Contact != null ? Contact : "<NULL>"));
            sb.AppendLine("DateValue: " + (DateValue != null ? DateValue.ToString() : "<NULL>"));
            sb.AppendLine("SUBSS: " + SUBSS);

            sb.AppendLine("------------RevenueByProductDTO dump ends---------------");

            return sb.ToString();
        }

        #endregion ToString() Implementation

        #region GetClone() Implementation

        /// <summary>
        ///     This methods returns a cloned instance of this class.
        /// </summary>
        /// <returns>A string cloned instance of this class.</returns>
        public virtual RevenueByProductNewDTO GetClone()
        {
            var clone = new RevenueByProductNewDTO();

            clone.Contact = Contact;
            clone.DateValue = DateValue;
            clone.SUBSS = SUBSS;

            return clone;
        }

        #endregion GetClone() Implementation

        #region GetHashCode() implementation.

        /// <summary>
        ///     Serves as a hash function for a particular type.
        /// </summary>
        /// <returns>
        ///     A hash code for the current RevenueByProductDTO.
        /// </returns>
        public override int GetHashCode()
        {
            return GetType().FullName.GetHashCode();
        }

        #endregion GetHashCode() Implementation

        #region Equals() implementation.

        /// <summary>
        ///     Determines whether the specified RevenueByProductDTO is equal to the current RevenueByProductDTO.
        /// </summary>
        /// <param name="obj">The RevenueByProductDTO to compare with the current RevenueByProductDTO.</param>
        /// <returns>
        ///     true if the specified RevenueByProductDTO is equal to the current RevenueByProductDTO; otherwise, false.
        /// </returns>
        /// <remarks>
        ///     Tests whether this and another object are equal in a way that will still pass
        ///     when proxy objects are being used.
        /// </remarks>
        public override bool Equals(object obj)
        {
            return true;
        }

        #endregion Equals() Implementation

        #region Property Declarations

        /// <summary>
        ///     Private variable to hold the value for property _newClicks.
        /// </summary>
        private string _newClicks = string.Empty;

        /// <summary>
        ///     Private variable to hold the value for property returningClicks.
        /// </summary>
        private string _returningClicks = string.Empty;

        /// <summary>
        ///     Private variable to hold the value for property _totalClicks.
        /// </summary>
        private string _totalClicks = string.Empty;

        /// <summary>
        ///     Private variable to hold the value for property lp.
        /// </summary>
        private string _lp = string.Empty;

        /// <summary>
        ///     Private variable to hold the value for property HowItWorks.
        /// </summary>
        private string _HowItWorks = string.Empty;

        /// <summary>
        ///     Private variable to hold the value for property StyleSelected.
        /// </summary>
        private string _StyleSelected = string.Empty;

        /// <summary>
        ///     Private variable to hold the value for property ScratchOrUploadWizard.
        /// </summary>
        private string _ScratchOrUploadWizard = string.Empty;

        /// <summary>
        ///     Private variable to hold the value for property Contact.
        /// </summary>
        /// Private variable to hold the value for property Desired Job.
        /// </summary>
        private string _DesiredJob = string.Empty;

        /// <summary>
        private string _Contact = string.Empty;

        /// <summary>
        ///     Private variable to hold the value for property RegisteredA.
        /// </summary>
        private string _RegisteredA = string.Empty;

        /// <summary>
        ///     Private variable to hold the value for property WorkHistory.
        /// </summary>
        private string _WorkHistory = string.Empty;

        /// <summary>
        ///     Private variable to hold the value for property WHTips.
        /// </summary>
        private string _WHTips = string.Empty;

        /// <summary>
        ///     Private variable to hold the value for property SkillTips.
        /// </summary>
        private string _SkillTips = string.Empty;

        /// <summary>
        ///     Private variable to hold the value for property Skills.
        /// </summary>
        private string _Skills = string.Empty;

        /// <summary>
        ///     Private variable to hold the value for property Educ.
        /// </summary>
        private string _Educ = string.Empty;

        /// <summary>
        ///     Private variable to hold the value for property EducTips.
        /// </summary>
        private string _EducTips = string.Empty;

        /// <summary>
        ///     Private variable to hold the value for property SummaryTips.
        /// </summary>
        private string _SummaryTips = string.Empty;

        /// <summary>
        ///     Private variable to hold the value for property Summary.
        /// </summary>
        private string _Summary = string.Empty;

        /// <summary>
        ///     Private variable to hold the value for property RegisteredB.
        /// </summary>
        private string _RegisteredB = string.Empty;

        /// <summary>
        ///     Private variable to hold the value for property Finalize.
        /// </summary>
        private string _Finalize = string.Empty;

        /// <summary>
        ///     Private variable to hold the value for property SellPage.
        /// </summary>
        private string _SellPage = string.Empty;

        public virtual string NewClicks { get; set; }
        public virtual string ReturningClicks { get; set; }
        public virtual string TotalClicks { get; set; }
        public virtual string PaymentConfirmationPage { get; set; }

        public virtual string CreditCardPage { get; set; }

        //public virtual string LP { get; set; }
        public virtual string HowItWorks { get; set; }
        public virtual string StyleSelected { get; set; }
        public virtual string ScratchOrUploadWizard { get; set; }
        public virtual string DesiredJob { get; set; }
        public virtual string Contact { get; set; }
        public virtual string RegisteredA { get; set; }
        public virtual string WorkHistory { get; set; }
        public virtual string WHTips { get; set; }
        public virtual string SkillTips { get; set; }
        public virtual string Skills { get; set; }
        public virtual string Educ { get; set; }
        public virtual string EducTips { get; set; }
        public virtual string SummaryTips { get; set; }
        public virtual string Summary { get; set; }
        public virtual string RegisteredB { get; set; }
        public virtual string Finalize { get; set; }
        public virtual string SellPage { get; set; }
        public virtual int? SUBSS { get; set; }
        public virtual decimal? Subs { get; set; }
        public virtual decimal? Recur { get; set; }
        public virtual decimal? Rfnd { get; set; }
        public virtual decimal? Chbk { get; set; }
        public virtual decimal? TotalSubsRev { get; set; }
        public virtual DateTime? DateValue { get; set; }
        public virtual string EducReview { get; set; }
        public virtual string WHDesc { get; set; }
        public virtual string WHReview { get; set; }

        #endregion Property Declarations
    }
}