﻿using System;
using System.Text;

namespace DomainModel.Analytics
{
    [Serializable]
    public class ABTestDTO
    {
        #region Constructors

        #endregion

        #region ToString() Implementation

        /// <summary>
        ///     This methods returns a string representation of this class.
        /// </summary>
        /// <returns>A string representation of this class.</returns>
        public new virtual string ToString()
        {
            var sb = new StringBuilder(7);

            sb.AppendLine("------------ABTestDTO dump starts---------------");
            sb.AppendLine("ABTestID: " + ABTestID);
            sb.AppendLine("Name: " + (Name != null ? Name : "<NULL>"));
            sb.AppendLine("TotalSubsRevAmount: " + TotalSubsRevAmount);

            sb.AppendLine("------------ABTestDTO dump ends---------------");

            return sb.ToString();
        }

        #endregion ToString() Implementation

        #region GetClone() Implementation

        /// <summary>
        ///     This methods returns a cloned instance of this class.
        /// </summary>
        /// <returns>A string cloned instance of this class.</returns>
        public virtual ABTestDTO GetClone()
        {
            var clone = new ABTestDTO();

            clone.ABTestID = ABTestID;
            clone.Name = Name;
            clone.TotalSubsRevAmount = TotalSubsRevAmount;
            return clone;
        }

        #endregion GetClone() Implementation

        #region GetHashCode() implementation.

        /// <summary>
        ///     Serves as a hash function for a particular type.
        /// </summary>
        /// <returns>
        ///     A hash code for the current ABTestDTO.
        /// </returns>
        public override int GetHashCode()
        {
            return GetType().FullName.GetHashCode();
        }

        #endregion GetHashCode() Implementation

        #region Equals() implementation.

        /// <summary>
        ///     Determines whether the specified ABTestDTO is equal to the current ABTestDTO.
        /// </summary>
        /// <param name="obj">The ABTestDTO to compare with the current ABTestDTO.</param>
        /// <returns>
        ///     true if the specified ABTestDTO is equal to the current ABTestDTO; otherwise, false.
        /// </returns>
        /// <remarks>
        ///     Tests whether this and another object are equal in a way that will still pass
        ///     when proxy objects are being used.
        /// </remarks>
        public override bool Equals(object obj)
        {
            //Same reference check !
            //if (object.ReferenceEquals(this, obj)) return true;

            ////Type check !
            //CardTypes other = obj as CardTypes;
            //if (other == null) return false;

            //if ((Id == 0))
            //    //Do a per-property search
            //    return
            //    (CardType == other.CardType);
            //else
            //    return Id == other.Id;

            return true;
        }

        #endregion Equals() Implementation

        #region Property Declarations

        public virtual string Name { get; set; }
        public virtual string Label { get; set; }
        public virtual int ABTestID { get; set; }
        public virtual int CaseIndex { get; set; }
        public virtual int Par { get; set; }
        public virtual int Reg { get; set; }
        public virtual int Edt { get; set; }
        public virtual int Subs { get; set; }
        public virtual decimal? SubsAmount { get; set; }
        public virtual decimal? RecurAmount { get; set; }
        public virtual decimal? RfndAmount { get; set; }
        public virtual decimal? ChbkAmount { get; set; }
        public virtual decimal? TotalSubsRevAmount { get; set; }

        #endregion Property Declarations
    }
}