﻿using System;
using System.Text;

namespace DomainModel.Analytics
{
    [Serializable]
    public class AdminNotesDTO
    {
        #region Constructors

        #endregion

        #region ToString() Implementation

        /// <summary>
        ///     This methods returns a string representation of this class.
        /// </summary>
        /// <returns>A string representation of this class.</returns>
        public new virtual string ToString()
        {
            var sb = new StringBuilder(6);

            sb.AppendLine("------------AdminNotesDTO dump starts---------------");
            sb.AppendLine("Admin Note: " + Note);
            sb.AppendLine("AdminName: " + AdminName);
            sb.AppendLine("------------AdminNotesDTO dump ends---------------");

            return sb.ToString();
        }

        #endregion ToString() Implementation

        #region GetClone() Implementation

        /// <summary>
        ///     This methods returns a cloned instance of this class.
        /// </summary>
        /// <returns>A string cloned instance of this class.</returns>
        public virtual AdminNotesDTO GetClone()
        {
            var clone = new AdminNotesDTO();
            clone.Note = Note;
            clone.AdminName = AdminName;
            clone.AdminUserID = AdminUserID;

            return clone;
        }

        #endregion GetClone() Implementation

        #region GetHashCode() implementation.

        /// <summary>
        ///     Serves as a hash function for a particular type.
        /// </summary>
        /// <returns>
        ///     A hash code for the current AdminNotesDTO.
        /// </returns>
        public override int GetHashCode()
        {
            return (GetType().FullName + Note).GetHashCode();
        }

        #endregion GetHashCode() Implementation

        #region Equals() implementation.

        /// <summary>
        ///     Determines whether the specified AdminNotesDTO is equal to the current AdminNotesDTO.
        /// </summary>
        /// <param name="obj">The AdminNotesDTO to compare with the current AdminNotesDTO.</param>
        /// <returns>
        ///     true if the specified AdminNotesDTO is equal to the current AdminNotesDTO; otherwise, false.
        /// </returns>
        /// <remarks>
        ///     Tests whether this and another object are equal in a way that will still pass
        ///     when proxy objects are being used.
        /// </remarks>
        public override bool Equals(object obj)
        {
            //Same reference check !
            if (ReferenceEquals(this, obj)) return true;

            //Type check !
            var other = obj as AdminNotesDTO;
            if (other == null) return false;

            return true;
        }

        #endregion Equals() Implementation

        #region Property Declarations

        /// <summary>
        ///     Private variable to hold the value for property AdminUserID.
        /// </summary>
        private int _adminNoteID;

        /// <summary>
        ///     AdminUserID.
        /// </summary>
        public virtual int AdminNoteID
        {
            get => _adminNoteID;
            set => _adminNoteID = value;
        }

        /// <summary>
        ///     Private variable to hold the value for property Note.
        /// </summary>
        private string _note;

        /// <summary>
        ///     Note of AB Test case.
        /// </summary>
        public virtual string Note
        {
            get => _note;
            set => _note = value;
        }

        /// <summary>
        ///     Private variable to hold the value for property AdminUserID.
        /// </summary>
        private int _adminUserID;

        /// <summary>
        ///     AdminUserID.
        /// </summary>
        public virtual int AdminUserID
        {
            get => _adminUserID;
            set => _adminUserID = value;
        }

        /// <summary>
        ///     AdminName
        /// </summary>
        private string _adminName;

        /// <summary>
        ///     AdminName.
        /// </summary>
        public virtual string AdminName
        {
            get => _adminName;
            set => _adminName = value;
        }

        /// <summary>
        ///     Private variable to hold the value for property DateValue.
        /// </summary>
        private DateTime? _timeStamp;

        /// <summary>
        ///     DateValue Given.
        /// </summary>
        public virtual DateTime? TimeStamp
        {
            get => _timeStamp;
            set => _timeStamp = value;
        }

        #endregion Property Declarations
    }
}