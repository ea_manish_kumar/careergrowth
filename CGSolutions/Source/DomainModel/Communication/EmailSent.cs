﻿using System;
using System.Text;

namespace DomainModel.Communication
{
    [Serializable]
    public class EmailSent : EntityBase<int, EmailSent>
    {
        #region Constructors

        #endregion Constructors

        #region ToString() Implementation

        /// <summary>
        ///     This methods returns a string representation of this class.
        /// </summary>
        /// <returns>A string representation of this class.</returns>
        public new virtual string ToString()
        {
            var sb = new StringBuilder(6);

            sb.AppendLine("------------EmailSent dump starts---------------");
            sb.AppendLine("EmailSentID: " + Id);
            sb.AppendLine("UserId: " + UserID);
            sb.AppendLine("OrderId: " + OrderID);
            sb.AppendLine("MailOutId: " + MailoutID);
            sb.AppendLine("TimeStamp: " + (TimeStamp != null ? TimeStamp.ToString() : "<NULL>"));
            sb.AppendLine("------------EmailSent dump ends---------------");

            return sb.ToString();
        }

        #endregion ToString() Implementation

        #region GetClone() Implementation

        /// <summary>
        ///     This methods returns a cloned instance of this class.
        /// </summary>
        /// <returns>A string cloned instance of this class.</returns>
        public virtual EmailSent GetClone()
        {
            var clone = new EmailSent();

            clone.Id = Id;
            clone.UserID = UserID;
            clone.OrderID = OrderID;
            clone.MailoutID = MailoutID;
            clone.TimeStamp = TimeStamp;
            clone.IsResent = IsResent;
            return clone;
        }

        #endregion GetClone() Implementation

        #region GetHashCode() implementation.

        /// <summary>
        ///     Serves as a hash function for a particular type.
        /// </summary>
        /// <returns>
        ///     A hash code for the current EventCode.
        /// </returns>
        public override int GetHashCode()
        {
            return (GetType().FullName + Id).GetHashCode();
        }

        #endregion GetHashCode() Implementation

        #region Equals() implementation.

        /// <summary>
        ///     Determines whether the specified EventCode is equal to the current EventCode.
        /// </summary>
        /// <param name="obj">The EventCode to compare with the current EventCode.</param>
        /// <returns>
        ///     true if the specified EventCode is equal to the current EventCode; otherwise, false.
        /// </returns>
        /// <remarks>
        ///     Tests whether this and another object are equal in a way that will still pass
        ///     when proxy objects are being used.
        /// </remarks>
        public override bool Equals(object obj)
        {
            //Same reference check !
            if (ReferenceEquals(this, obj)) return true;

            //Type check !
            var other = obj as EmailSent;
            if (other == null) return false;

            if (Id == 0)
                //Do a per-property search
                return
                    UserID == other.UserID &&
                    MailoutID == other.MailoutID &&
                    TimeStamp == other.TimeStamp;
            return Id == other.Id;
        }

        #endregion Equals() Implementation

        #region Properties

        public new virtual int Id { get; set; }

        public virtual int UserID { get; set; }

        public virtual int OrderID { get; set; }

        public virtual int MailoutID { get; set; }

        public virtual DateTime TimeStamp { get; set; }

        public virtual bool IsResent { get; set; }

        #endregion
    }
}