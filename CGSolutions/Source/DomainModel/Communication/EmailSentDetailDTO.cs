﻿using System;
using System.Text;

namespace DomainModel.Communication
{
    [Serializable]
    public class EmailSentDetailDTO
    {
        #region Constructors

        #endregion

        #region ToString() Implementation

        /// <summary>
        ///     This methods returns a string representation of this class.
        /// </summary>
        /// <returns>A string representation of this class.</returns>
        public new virtual string ToString()
        {
            var sb = new StringBuilder(7);

            sb.AppendLine("------------EmailSentDetailDTO dump starts---------------");
            sb.AppendLine("UserID: " + UserID);
            sb.AppendLine("MailoutID: " + MailoutID);
            sb.AppendLine("OrderID: " + OrderID);
            sb.AppendLine("DateSent: " + (DateSent != null ? DateSent.ToString() : "<NULL>"));
            sb.AppendLine("EmailName: " + (EmailName != null ? EmailName : "<NULL>"));
            sb.AppendLine("------------EmailSentDetailDTO dump ends---------------");

            return sb.ToString();
        }

        #endregion ToString() Implementation

        #region GetClone() Implementation

        /// <summary>
        ///     This methods returns a cloned instance of this class.
        /// </summary>
        /// <returns>A string cloned instance of this class.</returns>
        public virtual EmailSentDetailDTO GetClone()
        {
            var clone = new EmailSentDetailDTO();

            clone.UserID = UserID;
            clone.MailoutID = MailoutID;
            clone.OrderID = OrderID;
            clone.DateSent = DateSent;
            return clone;
        }

        #endregion GetClone() Implementation

        #region GetHashCode() implementation.

        /// <summary>
        ///     Serves as a hash function for a particular type.
        /// </summary>
        /// <returns>
        ///     A hash code for the current BillingTransactionDetailDTO.
        /// </returns>
        public override int GetHashCode()
        {
            return GetType().FullName.GetHashCode();
        }

        #endregion GetHashCode() Implementation

        #region Equals() implementation.

        /// <summary>
        ///     Determines whether the specified BillingTransactionDetailDTO is equal to the current BillingTransactionDetailDTO.
        /// </summary>
        /// <param name="obj">The BillingTransactionDetailDTO to compare with the current BillingTransactionDetailDTO.</param>
        /// <returns>
        ///     true if the specified BillingTransactionDetailDTO is equal to the current BillingTransactionDetailDTO; otherwise,
        ///     false.
        /// </returns>
        /// <remarks>
        ///     Tests whether this and another object are equal in a way that will still pass
        ///     when proxy objects are being used.
        /// </remarks>
        public override bool Equals(object obj)
        {
            return true;
        }

        #endregion Equals() Implementation

        #region Property Declarations

        public virtual int UserID { get; set; }
        public virtual int OrderID { get; set; }
        public virtual int MailoutID { get; set; }
        public virtual DateTime DateSent { get; set; }
        public virtual string EmailName { get; set; }

        #endregion Property Declarations
    }
}