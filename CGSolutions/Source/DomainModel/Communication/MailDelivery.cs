using System;
using System.Collections.Generic;
using System.Text;

namespace DomainModel.Communication
{
    public class MailDelivery : EntityBase<int, MailDelivery>
    {
        #region ToString() Implementation

        /// <summary>
        ///     This methods returns a string representation of this class.
        /// </summary>
        /// <returns>A string representation of this class.</returns>
        public new virtual string ToString()
        {
            var sb = new StringBuilder(6);

            sb.AppendLine("------------MailDelivery dump starts---------------");
            sb.AppendLine("Id: " + Id);
            sb.AppendLine("UserID: " + UserID);
            sb.AppendLine("MailoutID: " + (MailoutID != null ? MailoutID.ToString() : "<NULL>"));
            sb.AppendLine("DateSent: " + (DateSent != null ? DateSent.ToString() : "<NULL>"));
            sb.AppendLine("------------MailDelivery dump ends---------------");

            return sb.ToString();
        }

        #endregion ToString() Implementation

        #region GetClone() Implementation

        /// <summary>
        ///     This methods returns a cloned instance of this class.
        /// </summary>
        /// <returns>A string cloned instance of this class.</returns>
        public virtual MailDelivery GetClone()
        {
            var clone = new MailDelivery();

            clone.Id = Id;
            clone.UserID = UserID;
            clone.MailoutID = MailoutID;
            clone.DateSent = DateSent;

            return clone;
        }

        #endregion GetClone() Implementation

        #region GetHashCode() implementation.

        /// <summary>
        ///     Serves as a hash function for a particular type.
        /// </summary>
        /// <returns>
        ///     A hash code for the current MailDelivery.
        /// </returns>
        public override int GetHashCode()
        {
            return (GetType().FullName + Id).GetHashCode();
        }

        #endregion GetHashCode() Implementation

        #region Equals() implementation.

        /// <summary>
        ///     Determines whether the specified MailDelivery is equal to the current MailDelivery.
        /// </summary>
        /// <param name="obj">The MailDelivery to compare with the current MailDelivery.</param>
        /// <returns>
        ///     true if the specified MailDelivery is equal to the current MailDelivery; otherwise, false.
        /// </returns>
        /// <remarks>
        ///     Tests whether this and another object are equal in a way that will still pass
        ///     when proxy objects are being used.
        /// </remarks>
        public override bool Equals(object obj)
        {
            //Same reference check !
            if (ReferenceEquals(this, obj)) return true;

            //Type check !
            var other = obj as MailDelivery;
            if (other == null) return false;

            if (Id == 0)
                //Do a per-property search
                return
                    UserID == other.UserID &&
                    MailoutID == other.MailoutID &&
                    DateSent == other.DateSent;
            return Id == other.Id;
        }

        #endregion Equals() Implementation

        #region Constructors

        /// <summary>
        ///     Class constructor...
        /// </summary>
        public MailDelivery()
        {
            //TODO:Write constructor stuff here..
        }


        /// <summary>
        ///     The parameterized constructor.
        /// </summary>
        public MailDelivery(int Id, int userID, int? mailoutID, DateTime? dateSent)
        {
            this.Id = Id;
            UserID = userID;
            MailoutID = mailoutID;
            DateSent = dateSent;
        }

        #endregion

        #region Property Declarations

        ///// <summary>
        ///// Private variable to hold the value for property MailDeliveryID.
        ///// </summary>
        //private int _mailDeliveryID;

        ///// <summary>
        ///// Mail Delivery ID of the particular mail.
        ///// </summary>
        //public virtual int MailDeliveryID
        //{
        //    get
        //    {
        //        return this._mailDeliveryID;
        //    }
        //    set
        //    {
        //        this._mailDeliveryID = value;
        //    }
        //}

        /// <summary>
        ///     Private variable to hold the value for property UserID.
        /// </summary>
        private int _userID;

        /// <summary>
        ///     Party Id to which mail is sent.
        /// </summary>
        public virtual int UserID
        {
            get => _userID;
            set => _userID = value;
        }

        /// <summary>
        ///     Private variable to hold the value for property MailoutID.
        /// </summary>
        private int? _mailoutID;

        /// <summary>
        ///     MailoutId used to send the mail (may be optional).
        /// </summary>
        public virtual int? MailoutID
        {
            get => _mailoutID;
            set => _mailoutID = value;
        }

        /// <summary>
        ///     Private variable to hold the value for property DateSent.
        /// </summary>
        private DateTime? _dateSent;

        /// <summary>
        ///     Date of the mail sent.
        /// </summary>
        public virtual DateTime? DateSent
        {
            get => _dateSent;
            set => _dateSent = value;
        }

        #endregion Property Declarations
    }

    [Serializable]
    public class MailDeliveryCollection : List<MailDelivery>
    {
        /// <summary>
        ///     This methods returns a string representation of this collection.
        /// </summary>
        /// <returns>String representation of this collection as string..</returns>
        public new string ToString()
        {
            var sb = new StringBuilder(Count + 1);

            for (var i = 0; i < Count; i++) sb.Append(this[i].ToString());
            return sb.ToString();
        }
    }
}