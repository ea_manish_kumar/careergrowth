﻿namespace DomainModel.Communication
{
    public class Mailout : EntityBase<int, Mailout>
    {
        #region Properties

        public virtual int MailoutID { get; set; }

        public virtual bool IsActive { get; set; }

        public virtual string Query { get; set; }

        public virtual string TemplateFile { get; set; }

        public virtual string Description { get; set; }

        public virtual bool IsToBeResent { get; set; }

        #endregion
    }
}