﻿using System;
using System.Text;

namespace DomainModel.Communication
{
    [Serializable]
    public class EmailLinkTracking : EntityBase<int, EmailLinkTracking>
    {
        #region Constructors

        #endregion Constructors

        #region ToString() Implementation

        /// <summary>
        ///     This methods returns a string representation of this class.
        /// </summary>
        /// <returns>A string representation of this class.</returns>
        public new virtual string ToString()
        {
            var sb = new StringBuilder();

            sb.AppendLine("------------ EmailLinkTracking Dump Starts ---------------");
            sb.AppendLine("\nEmailLinkTrackingID: " + Id);
            sb.AppendLine("\nEmailLinkID: " + EmailLinkID);
            sb.AppendLine("\nUniqueClicks: " + UniqueClicks);
            sb.AppendLine("\nTotalClicks: " + TotalClicks);
            sb.AppendLine("\nTrackingDate: " + (TrackingDate != null ? TrackingDate.ToString() : "<NULL>"));
            sb.AppendLine("\n------------ EmailLinkTracking Dump Ends ---------------");

            return sb.ToString();
        }

        #endregion ToString() Implementation

        #region GetClone() Implementation

        /// <summary>
        ///     This methods returns a cloned instance of this class.
        /// </summary>
        /// <returns>A string cloned instance of this class.</returns>
        public virtual EmailLinkTracking GetClone()
        {
            var clone = new EmailLinkTracking();

            clone.Id = Id;
            clone.EmailLinkID = EmailLinkID;
            clone.UniqueClicks = UniqueClicks;
            clone.TotalClicks = TotalClicks;
            clone.TrackingDate = TrackingDate;
            return clone;
        }

        #endregion GetClone() Implementation

        #region GetHashCode() implementation.

        /// <summary>
        ///     Serves as a hash function for a particular type.
        /// </summary>
        /// <returns>
        ///     A hash code for the current EmailTracking.
        /// </returns>
        public override int GetHashCode()
        {
            return (GetType().FullName + Id).GetHashCode();
        }

        #endregion GetHashCode() Implementation

        #region Equals() implementation.

        /// <summary>
        ///     Determines whether the specified EmailLinkTracking is equal to the current EmailTracking.
        /// </summary>
        /// <param name="obj">The EmailLinkTracking to compare with the current EmailTracking.</param>
        /// <returns>
        ///     true if the specified EmailLinkTracking is equal to the current EmailTracking; otherwise, false.
        /// </returns>
        /// <remarks>
        ///     Tests whether this and another object are equal in a way that will still pass
        ///     when proxy objects are being used.
        /// </remarks>
        public override bool Equals(object obj)
        {
            //Same reference check !
            if (ReferenceEquals(this, obj)) return true;

            //Type check !
            var other = obj as EmailLinkTracking;
            if (other == null) return false;

            if (Id == 0)
                //Do a per-property search
                return
                    EmailLinkID == other.EmailLinkID &&
                    UniqueClicks == other.UniqueClicks &&
                    TotalClicks == other.TotalClicks &&
                    TrackingDate == other.TrackingDate;
            return Id == other.Id;
        }

        #endregion Equals() Implementation

        #region Properties

        public virtual int EmailLinkID { get; set; }

        public virtual int UniqueClicks { get; set; }

        public virtual int TotalClicks { get; set; }

        public virtual DateTime TrackingDate { get; set; }

        #endregion
    }
}