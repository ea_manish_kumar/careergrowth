﻿using System;
using System.Text;

namespace DomainModel.Communication
{
    [Serializable]
    public class EmailTracking : EntityBase<int, EmailTracking>
    {
        #region Constructors

        #endregion Constructors

        #region ToString() Implementation

        /// <summary>
        ///     This methods returns a string representation of this class.
        /// </summary>
        /// <returns>A string representation of this class.</returns>
        public new virtual string ToString()
        {
            var sb = new StringBuilder();

            sb.AppendLine("------------ EmailTracking Dump Starts ---------------");
            sb.AppendLine("\nEmailTrackingID: " + Id);
            sb.AppendLine("\nMailOutID: " + MailOutID);
            sb.AppendLine("\nUniqueOpens: " + UniqueOpens);
            sb.AppendLine("\nTotalOpens: " + TotalOpens);
            sb.AppendLine("\nTotalSent: " + TotalSent);
            sb.AppendLine("\nUnsubscribedCount: " + UnsubscribedCount);
            sb.AppendLine("\nSubscriptionsPurchased: " + SubscriptionsPurchased);
            sb.AppendLine("\nTrackingDate: " + (TrackingDate != null ? TrackingDate.ToString() : "<NULL>"));
            sb.AppendLine("\n------------ EmailTracking Dump Ends ---------------");

            return sb.ToString();
        }

        #endregion ToString() Implementation

        #region GetClone() Implementation

        /// <summary>
        ///     This methods returns a cloned instance of this class.
        /// </summary>
        /// <returns>A string cloned instance of this class.</returns>
        public virtual EmailTracking GetClone()
        {
            var clone = new EmailTracking();

            clone.Id = Id;
            clone.MailOutID = MailOutID;
            clone.UniqueOpens = UniqueOpens;
            clone.TotalOpens = TotalOpens;
            clone.TotalSent = TotalSent;
            clone.UnsubscribedCount = UnsubscribedCount;
            clone.SubscriptionsPurchased = SubscriptionsPurchased;
            clone.TrackingDate = TrackingDate;
            return clone;
        }

        #endregion GetClone() Implementation

        #region GetHashCode() implementation.

        /// <summary>
        ///     Serves as a hash function for a particular type.
        /// </summary>
        /// <returns>
        ///     A hash code for the current EmailTracking.
        /// </returns>
        public override int GetHashCode()
        {
            return (GetType().FullName + Id).GetHashCode();
        }

        #endregion GetHashCode() Implementation

        #region Equals() implementation.

        /// <summary>
        ///     Determines whether the specified EmailTracking is equal to the current EmailTracking.
        /// </summary>
        /// <param name="obj">The EmailTracking to compare with the current EmailTracking.</param>
        /// <returns>
        ///     true if the specified EmailTracking is equal to the current EmailTracking; otherwise, false.
        /// </returns>
        /// <remarks>
        ///     Tests whether this and another object are equal in a way that will still pass
        ///     when proxy objects are being used.
        /// </remarks>
        public override bool Equals(object obj)
        {
            //Same reference check !
            if (ReferenceEquals(this, obj)) return true;

            //Type check !
            var other = obj as EmailTracking;
            if (other == null) return false;

            if (Id == 0)
                //Do a per-property search
                return
                    MailOutID == other.MailOutID &&
                    UniqueOpens == other.UniqueOpens &&
                    TotalOpens == other.TotalOpens &&
                    TotalSent == other.TotalSent &&
                    UnsubscribedCount == other.UnsubscribedCount &&
                    SubscriptionsPurchased == other.SubscriptionsPurchased &&
                    TrackingDate == other.TrackingDate;
            return Id == other.Id;
        }

        #endregion Equals() Implementation

        #region Properties

        public virtual int MailOutID { get; set; }

        public virtual int UniqueOpens { get; set; }

        public virtual int TotalOpens { get; set; }

        public virtual int TotalSent { get; set; }

        public virtual int UnsubscribedCount { get; set; }

        public virtual int SubscriptionsPurchased { get; set; }

        public virtual DateTime TrackingDate { get; set; }

        #endregion
    }
}