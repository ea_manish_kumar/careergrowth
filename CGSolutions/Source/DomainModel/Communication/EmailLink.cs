﻿using System;
using System.Text;

namespace DomainModel.Communication
{
    [Serializable]
    public class EmailLink : EntityBase<int, EmailLink>
    {
        #region Constructors

        #endregion Constructors

        #region ToString() Implementation

        /// <summary>
        ///     This methods returns a string representation of this class.
        /// </summary>
        /// <returns>A string representation of this class.</returns>
        public new virtual string ToString()
        {
            var sb = new StringBuilder();

            sb.AppendLine("------------ EmailLink Dump Starts ---------------");
            sb.AppendLine("\nEmailLinkID: " + Id);
            sb.AppendLine("\nMailOutID: " + MailOutID);
            sb.AppendLine("\nName: " + Name);
            sb.AppendLine("\nLinkUrl: " + LinkUrl);
            sb.AppendLine("\nIsAuthRequired: " + IsAuthRequired);
            sb.AppendLine("\nIsActive: " + IsActive);
            sb.AppendLine("\n------------ EmailLink Dump Ends ---------------");

            return sb.ToString();
        }

        #endregion ToString() Implementation

        #region GetClone() Implementation

        /// <summary>
        ///     This methods returns a cloned instance of this class.
        /// </summary>
        /// <returns>A string cloned instance of this class.</returns>
        public virtual EmailLink GetClone()
        {
            var clone = new EmailLink();

            clone.Id = Id;
            clone.MailOutID = MailOutID;
            clone.Name = Name;
            clone.LinkUrl = LinkUrl;
            clone.IsAuthRequired = IsAuthRequired;
            clone.IsActive = IsActive;
            return clone;
        }

        #endregion GetClone() Implementation

        #region GetHashCode() implementation.

        /// <summary>
        ///     Serves as a hash function for a particular type.
        /// </summary>
        /// <returns>
        ///     A hash code for the current EmailLink.
        /// </returns>
        public override int GetHashCode()
        {
            return (GetType().FullName + Id).GetHashCode();
        }

        #endregion GetHashCode() Implementation

        #region Equals() implementation.

        /// <summary>
        ///     Determines whether the specified EmailLink is equal to the current EmailLink.
        /// </summary>
        /// <param name="obj">The EmailLink to compare with the current EmailLink.</param>
        /// <returns>
        ///     true if the specified EmailLink is equal to the current EmailLink; otherwise, false.
        /// </returns>
        /// <remarks>
        ///     Tests whether this and another object are equal in a way that will still pass
        ///     when proxy objects are being used.
        /// </remarks>
        public override bool Equals(object obj)
        {
            //Same reference check !
            if (ReferenceEquals(this, obj)) return true;

            //Type check !
            var other = obj as EmailLink;
            if (other == null) return false;

            if (Id == 0)
                //Do a per-property search
                return
                    MailOutID == other.MailOutID &&
                    Name == other.Name &&
                    LinkUrl == other.LinkUrl &&
                    IsAuthRequired == other.IsAuthRequired &&
                    IsActive == other.IsActive;
            return Id == other.Id;
        }

        #endregion Equals() Implementation

        #region Properties

        public virtual int MailOutID { get; set; }

        public virtual string Name { get; set; }

        public virtual string LinkUrl { get; set; }

        public virtual bool IsAuthRequired { get; set; }

        public virtual bool IsActive { get; set; }

        #endregion
    }
}