﻿using System.Text;

namespace DomainModel
{
    public class Template : EntityBase<int, Template>
    {
        public virtual int TemplateID { get; set; }
        public virtual string TemplateCD { get; set; }
        public virtual string Name { get; set; }
        public virtual string TemplateCategoryTypeCD { get; set; }
        public virtual string TemplateFormatCD { get; set; }
        public virtual string DocumentTypeCD { get; set; }
        public virtual int TemplateDocID { get; set; }
        public virtual bool IsActive { get; set; }
        public virtual TemplateExtended TemplateExtended { get; set; }

        public new virtual string ToString()
        {
            var sb = new StringBuilder(7);

            sb.AppendLine("------------Template dump starts---------------");
            sb.AppendLine("TemplateID:" + TemplateID);
            sb.AppendLine("TemplateCD:" + TemplateCD);
            sb.AppendLine("Name: " + (Name != null ? Name : "<NULL>"));
            sb.AppendLine("DocumentTypeCD: " + (DocumentTypeCD != null ? DocumentTypeCD : "<NULL>"));
            sb.AppendLine("TemplateCategoryTypeCD: " +
                          (TemplateCategoryTypeCD != null ? TemplateCategoryTypeCD : "<NULL>"));
            sb.AppendLine("TemplateFormatCD: " + (TemplateFormatCD != null ? TemplateFormatCD : "<NULL>"));
            sb.AppendLine("IsActive:" + IsActive);
            sb.AppendLine("------------Template dump ends---------------");

            return sb.ToString();
        }
    }
}