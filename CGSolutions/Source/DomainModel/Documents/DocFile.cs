﻿using System.Text;

namespace DomainModel
{
    public class DocFile : EntityBase<int, DocFile>
    {
        public virtual string DocFileTypeCD { get; set; }

        /// <summary>
        ///     This property will be used to get/set value of Document (Document)
        /// </summary>
        /// <value>The document.</value>
        public virtual Document Document { get; set; }

        /// <summary>
        ///     This property will be used to get/set value of File (File)
        /// </summary>
        /// <value>The File.</value>
        public virtual File File { get; set; }

        #region ToString() Implementation

        /// <summary>
        ///     This methods returns a string representation of this class.
        /// </summary>
        /// <returns>A string representation of this class.</returns>
        public new virtual string ToString()
        {
            var sb = new StringBuilder(14);

            sb.Append("------------ docfile dump starts---------------\n");
            sb.AppendLine(base.ToString());
            sb.AppendLine("DocFileID: " + Id);
            sb.AppendLine("DocFileTypeCD: " + (DocFileTypeCD != null ? DocFileTypeCD : "<NULL>"));
            //sb.AppendLine("DocumentID: " + (this.DocumentID != null ? this.DocumentID.ToString() : "<NULL>").ToString());
            //sb.AppendLine("FileID: " + (this.FileID != null ? this.FileID.ToString() : "<NULL>").ToString());
            sb.AppendLine("DocumentID: " + Document.Id);
            sb.AppendLine("FileID: " + File.Id);
            sb.Append("------------ docfile dump ends---------------\n");

            return sb.ToString();
        }

        #endregion ToString() Implementation

        #region GetHashCode() implementation.

        /// <summary>
        ///     Serves as a hash function for a particular type.
        /// </summary>
        /// <returns>
        ///     A hash code for the current DocFile.
        /// </returns>
        public override int GetHashCode()
        {
            return (GetType().FullName + Document.Id + File.Id + DocFileTypeCD).GetHashCode();
        }

        #endregion GetHashCode() Implementation
    }
}