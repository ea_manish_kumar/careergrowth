﻿using System;
using System.Text;

namespace DomainModel
{
    [Serializable]
    public class Field : EntityBase<string, Field>
    {
        #region ToString() Implementation

        public new virtual string ToString()
        {
            var sb = new StringBuilder(8);

            sb.AppendLine("------------DocData dump starts---------------");
            sb.AppendLine(base.ToString());
            sb.AppendLine("FieldCD: " + (FieldCD ?? "<NULL>"));
            sb.AppendLine("CharValue: " + (DataTypeCD ?? "<NULL>"));
            sb.AppendLine("MaxLength: " + (MaxLength.HasValue ? MaxLength.Value.ToString() : "<NULL>"));
            sb.AppendLine("Description: " + (Description ?? "<NULL>"));
            sb.AppendLine("Label: " + (Label ?? "<NULL>"));
            sb.AppendLine("------------DocData dump ends---------------");

            return sb.ToString();
        }

        #endregion ToString() Implementation

        #region Properties

        /// <summary>
        ///     Private variable to hold the value for property FieldCD.
        /// </summary>
        private string _fieldCD;

        /// <summary>
        ///     Code identifying the type of field.
        /// </summary>
        /// <value>The field CD.</value>
        public virtual string FieldCD
        {
            get => _fieldCD;
            set => _fieldCD = value;
        }

        /// <summary>
        ///     Private variable to hold the value for property DataTypeCD.
        /// </summary>
        private string _dataTypeCD;

        /// <summary>
        ///     Code for Type of data the field is holding.
        /// </summary>
        /// <value>The data type CD.</value>
        public virtual string DataTypeCD
        {
            get => _dataTypeCD;
            set => _dataTypeCD = value;
        }

        /// <summary>
        ///     Private variable to hold the value for property MaxLength.
        /// </summary>
        private int? _maxLength;

        /// <summary>
        ///     The max number of characters this field can support.
        /// </summary>
        /// <value>The length of the max.</value>
        public virtual int? MaxLength
        {
            get => _maxLength;
            set => _maxLength = value;
        }

        private string _Description;

        /// <summary>
        ///     Code identifying the type of field.
        /// </summary>
        /// <value>The field CD.</value>
        public virtual string Description
        {
            get => _Description;
            set => _Description = value;
        }

        private string _Label;

        /// <summary>
        ///     Code identifying the type of field.
        /// </summary>
        /// <value>The field CD.</value>
        public virtual string Label
        {
            get => _Label;
            set => _Label = value;
        }

        #endregion
    }
}