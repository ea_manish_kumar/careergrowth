using System;
using System.Collections;
using System.Reflection;
using System.Text;
using CommonModules.Exceptions;

namespace DomainModel
{
    public class DocPreference : EntityBase<int, DocPreference>
    {
        #region ToString() Implementation

        /// <summary>
        ///     This methods returns a string representation of this class.
        /// </summary>
        /// <returns>A string representation of this class.</returns>
        public new virtual string ToString()
        {
            var sb = new StringBuilder(6);

            sb.AppendLine("------------DocPreference dump starts---------------");
            sb.AppendLine(base.ToString());
            sb.AppendLine("DocPreferenceTypeCD: " + (DocPreferenceTypeCD != null ? DocPreferenceTypeCD : "<NULL>"));
            sb.AppendLine("Value: " + (Value != null ? Value : "<NULL>"));
            sb.AppendLine("------------DocPreference dump ends---------------");

            return sb.ToString();
        }

        #endregion ToString() Implementation

        #region GetClone() Implementation

        /// <summary>
        ///     Creates a deep copy of the DocPreference object
        /// </summary>
        /// <param name="isForFlex">If set to True, the Associations not relevant to Flex will be set to NULL</param>
        /// <param name="IgnoreIDs">
        ///     If set to True, the Id property values will not be copied to the Clone.
        ///     So the cloned DocPreference can be saved to the DB as a new record
        /// </param>
        /// <returns></returns>
        public override DocPreference GetClone(bool isFlex, bool IgnoreIDs)
        {
            var clone = new DocPreference();
            try
            {
                if (!IgnoreIDs)
                    clone.Id = Id;

                clone.DocPreferenceTypeCD = DocPreferenceTypeCD;
                clone.Value = Value;
            }
            catch (Exception ex)
            {
                var htParams = new Hashtable(1);
                htParams.Add("DocPreference", ToString());
                var objMethod = (MethodInfo) MethodBase.GetCurrentMethod();
                GlobalExceptionHandler.customExceptionHandler(ex, objMethod, htParams, ErrorSeverityType.High);
            }

            return clone;
        }

        #endregion GetClone() Implementation

        #region GetHashCode() implementation.

        /// <summary>
        ///     Serves as a hash function for a particular type.
        /// </summary>
        /// <returns>
        ///     A hash code for the current DocPreference.
        /// </returns>
        public override int GetHashCode()
        {
            return (GetType().FullName + Id).GetHashCode();
        }

        #endregion GetHashCode() Implementation

        #region Equals() implementation.

        /// <summary>
        ///     Determines whether the specified DocPreference is equal to the current DocPreference.
        /// </summary>
        /// <param name="obj">The DocPreference to compare with the current DocPreference.</param>
        /// <returns>
        ///     true if the specified DocPreference is equal to the current DocPreference; otherwise, false.
        /// </returns>
        /// <remarks>
        ///     Tests whether this and another object are equal in a way that will still pass
        ///     when proxy objects are being used.
        /// </remarks>
        public override bool Equals(object obj)
        {
            //Same reference check !
            if (ReferenceEquals(this, obj)) return true;

            //Type check !
            var other = obj as DocPreference;
            if (other == null) return false;

            if (Id == 0)
                //Do a per-property search
                return
                    DocumentID == other.DocumentID &&
                    DocPreferenceTypeCD == other.DocPreferenceTypeCD &&
                    Value == other.Value;
            return Id == other.Id;
        }

        #endregion Equals() Implementation

        #region Constructors

        /// <summary>
        ///     Class constructor...
        /// </summary>
        public DocPreference()
        {
        }


        /// <summary>
        ///     The parameterized constructor.
        /// </summary>
        /// <param name="id">The id.</param>
        /// <param name="documentID">The document Id.</param>
        /// <param name="docPreferenceTypeCD">The doc preference type CD.</param>
        /// <param name="value">The value.</param>
        public DocPreference(int documentID, string docPreferenceTypeCD, string value)
        {
            DocumentID = documentID;
            DocPreferenceTypeCD = docPreferenceTypeCD;
            Value = value;
        }

        #endregion


        #region Property Declarations

        /// <summary>
        ///     Private variable to hold the value for property DocPreferenceTypeCD.
        /// </summary>
        private string _docPreferenceTypeCD;

        /// <summary>
        ///     Rule that applies to the document privacy settings.
        /// </summary>
        /// <value>The doc preference type CD.</value>
        public virtual string DocPreferenceTypeCD
        {
            get => _docPreferenceTypeCD;
            set => _docPreferenceTypeCD = value;
        }


        /// <summary>
        ///     Private variable to hold the value for property DocumentID.
        /// </summary>
        public virtual int DocumentID { get; set; }

        /// <summary>
        ///     Private variable to hold the value for property Value.
        /// </summary>
        private string _value;

        /// <summary>
        ///     Value that applies to rule.
        /// </summary>
        /// <value>The value.</value>
        public virtual string Value
        {
            get => _value;
            set => _value = value;
        }

        #endregion Property Declarations
    }
}