﻿using System.Text;

namespace DomainModel
{
    public class DocStyle : EntityBase<int, DocStyle>
    {
        public virtual int DocumentID { get; set; }
        public virtual string StyleCD { get; set; }
        public virtual string Value { get; set; }

        #region ToString() Implementation

        /// <summary>
        ///     This methods returns a string representation of this class.
        /// </summary>
        /// <returns>A string representation of this class.</returns>
        public new virtual string ToString()
        {
            var sb = new StringBuilder(5);

            sb.AppendLine("------------DocStyle dump starts---------------");
            sb.AppendLine("DocumentID: " + DocumentID);
            sb.AppendLine("StyleCD: " + StyleCD);
            sb.AppendLine("Value: " + Value);
            sb.AppendLine("------------DocStyle dump ends---------------");

            return sb.ToString();
        }

        #endregion ToString() Implementation

        #region GetHashCode() implementation.

        /// <summary>
        ///     Serves as a hash function for a particular type.
        /// </summary>
        /// <returns>
        ///     A hash code for the current DocStyle.
        /// </returns>
        public override int GetHashCode()
        {
            return (GetType().FullName + DocumentID + StyleCD).GetHashCode();
        }

        #endregion GetHashCode() Implementation
    }
}