﻿namespace DomainModel
{
    public class ResumeCheckResultMsg
    {
        public const string Fifty =
            "A few quick changes will help you land more interviews. Click the links below to learn more.";

        public const string Hundred =
            "Congrats, you’ve reached resume perfection! Your 100% rating means you’re ready to land your dream job.";

        public const string Ninety =
            "Your resume scored a 90% percent. You’re oh-so-close to being perfect! To take your resume to the next level, check out the links below.";

        public const string Seventy =
            "Click the links below to boost your score and leave with an eye-catching, knockout resume.";
    }
}