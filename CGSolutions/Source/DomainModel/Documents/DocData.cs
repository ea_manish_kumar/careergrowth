﻿using System;
using System.Text;
using System.Web;
using Utilities;

namespace DomainModel
{
    [Serializable]
    public class DocData : EntityBase<int, DocData>
    {
        private Field objFieldFreeForm;

        #region ToString() Implementation

        public new virtual string ToString()
        {
            var sb = new StringBuilder(8);

            sb.AppendLine("------------DocData dump starts---------------");
            sb.AppendLine(base.ToString());
            sb.AppendLine("ParagraphID: " + (Paragraph != null ? Paragraph.Id.ToString() : "<NULL>"));
            sb.AppendLine("FieldCD: " + (FieldCD ?? "<NULL>"));
            sb.AppendLine("CharValue: " + (CharValue ?? "<NULL>"));
            sb.AppendLine("TimeStamp: " + TimeStamp);
            sb.AppendLine("------------DocData dump ends---------------");

            return sb.ToString();
        }

        #endregion ToString() Implementation

        #region Equals() implementation.

        /// <summary>
        ///     Determines whether the specified docdata is equal to the current docdata.
        /// </summary>
        /// <param name="obj">The docdata to compare with the current docdata.</param>
        /// <returns>
        ///     true if the specified docdata is equal to the current docdata; otherwise, false.
        /// </returns>
        /// <remarks>
        ///     Tests whether this and another object are equal in a way that will still pass
        ///     when proxy objects are being used.
        /// </remarks>
        public override bool Equals(object obj)
        {
            //Same reference check !
            if (ReferenceEquals(this, obj)) return true;

            //Type check !
            var other = obj as DocData;
            if (other == null) return false;

            if (Id == 0)
                //Do a per-property search
                return
                    CharValue == other.CharValue &&
                    FieldCD == other.FieldCD &&
                    TimeStamp == other.TimeStamp;
            return Id == other.Id;
        }

        #endregion Equals() Implementation

        #region GetHashCode() implementation.

        /// <summary>
        ///     Serves as a hash function for a particular type.
        /// </summary>
        /// <returns>
        ///     A hash code for the current SectionType.
        /// </returns>
        public override int GetHashCode()
        {
            return (GetType().FullName + Id).GetHashCode();
        }

        #endregion GetHashCode() Implementation

        #region Constructors

        public DocData()
        {
            TimeStamp = DateTime.Now;
        }

        public DocData(string FieldID)
        {
            FieldCD = FieldID;
            TimeStamp = DateTime.Now;
        }

        public DocData(Field objFieldFreeForm)
        {
            // TODO: Complete member initialization
            this.objFieldFreeForm = objFieldFreeForm;
        }

        /// <summary>
        ///     Create new docdata, provide all the values
        /// </summary>
        /// <param name="para"></param>
        /// <param name="fieldCd"></param>
        /// <param name="charValue"></param>
        public DocData(Paragraph para, string fieldCd, string charValue)
        {
            Paragraph = para;
            FieldCD = fieldCd;
            CharValue = charValue;
            TimeStamp = DateTime.Now;
        }

        #endregion

        #region Properties

        public virtual string FieldCD { get; set; }
        public virtual Paragraph Paragraph { get; set; }
        private string charvalue = string.Empty;

        public virtual string CharValue
        {
            get => charvalue;
            set
            {
                charvalue = HttpUtility.HtmlDecode(value);
                charvalue = CGUtility.SanitizeString(charvalue);
                value = charvalue;
            }
        }

        public virtual DateTime TimeStamp { get; set; }

        #endregion
    }
}