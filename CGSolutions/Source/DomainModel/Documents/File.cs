﻿using System;
using System.Text;

namespace DomainModel
{
    public class File : EntityBase<int, File>
    {
        #region ToString() Implementation

        /// <summary>
        ///     This methods returns a string representation of this class.
        /// </summary>
        /// <returns>A string representation of this class.</returns>
        public new virtual string ToString()
        {
            var sb = new StringBuilder(14);

            sb.Append("------------ File dump starts---------------\n");
            sb.AppendLine(base.ToString());
            sb.AppendLine("BinaryData: " + (BinaryData != null ? BinaryData.ToString() : "<NULL>"));
            sb.AppendLine("FileID: " + Id);
            sb.AppendLine("FileTypeCD: " + (FileTypeCD != null ? FileTypeCD : "<NULL>"));
            sb.AppendLine("TextData: " + (TextData != null ? TextData : "<NULL>"));
            sb.AppendLine("Timestamp: " + (Timestamp != null ? Timestamp.ToString() : "<NULL>"));
            sb.Append("------------ File dump ends---------------\n");

            return sb.ToString();
        }

        #endregion ToString() Implementation

        #region GetHashCode() implementation.

        /// <summary>
        ///     Serves as a hash function for a particular type.
        /// </summary>
        /// <returns>
        ///     A hash code for the current File.
        /// </returns>
        public override int GetHashCode()
        {
            return (GetType().FullName + Id + FileTypeCD).GetHashCode();
        }

        #endregion GetHashCode() Implementation


        #region Properties

        public virtual string FileTypeCD { get; set; }
        public virtual byte[] BinaryData { get; set; }
        public virtual string TextData { get; set; }
        public virtual DateTime Timestamp { get; set; }
        public virtual string Name { get; set; }
        public virtual int NumOfPages { get; set; }

        #endregion
    }
}