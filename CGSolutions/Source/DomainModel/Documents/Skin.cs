﻿namespace DomainModel
{
    public class Skin : EntityBase<int, Skin>
    {
        public virtual int SkinID { get; set; }
        public virtual string SkinCD { get; set; }
        public virtual string Description { get; set; }
    }
}