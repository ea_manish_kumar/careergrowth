﻿using System.Text;

namespace DomainModel
{
    public class ExperienceTip : EntityBase<int, ExperienceTip>
    {
        public virtual string OnetSocCode { get; set; }
        public virtual string JobTitle { get; set; }
        public virtual string ScaleName { get; set; }
        public virtual decimal TaskID { get; set; }
        public virtual string TaskType { get; set; }
        public virtual string Task { get; set; }
        public virtual decimal DataValue { get; set; }
        public virtual decimal ImportancePercentage { get; set; }

        #region ToString Implementation

        public new virtual string ToString()
        {
            var sb = new StringBuilder(14);

            sb.Append("------------ExperienceTip dump starts---------------\n");
            sb.AppendLine(base.ToString());
            sb.AppendLine("DataValue: " + DataValue);
            sb.AppendLine("ImportancePercentage: " + ImportancePercentage);
            sb.AppendLine("JobTitle: " + (JobTitle != null ? JobTitle : "<NULL>"));
            sb.AppendLine("OnetSocCode: " + (OnetSocCode != null ? OnetSocCode : "<NULL>"));
            sb.AppendLine("ScaleName: " + (ScaleName != null ? ScaleName : "<NULL>"));
            sb.AppendLine("Task: " + (Task != null ? Task : "<NULL>"));
            sb.AppendLine("TaskID: " + TaskID);
            sb.AppendLine("TaskType: " + (TaskType != null ? TaskType : "<NULL>"));
            sb.Append("------------ExperienceTip dump ends---------------\n");

            return sb.ToString();
        }

        #endregion
    }
}