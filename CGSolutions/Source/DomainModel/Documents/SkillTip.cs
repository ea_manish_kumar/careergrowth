﻿using System.Text;

namespace DomainModel
{
    public class SkillTip : EntityBase<int, SkillTip>
    {
        public virtual string OnetSocCode { get; set; }
        public virtual string JobTitle { get; set; }
        public virtual string ScaleName { get; set; }
        public virtual string ElementName { get; set; }
        public virtual decimal DataValue { get; set; }
        public virtual decimal ImportancePercentage { get; set; }

        #region ToString Implementation

        public new virtual string ToString()
        {
            var sb = new StringBuilder(14);

            sb.Append("------------ SkillTip dump starts---------------\n");
            sb.AppendLine(base.ToString());
            sb.AppendLine("DataValue: " + DataValue);
            sb.AppendLine("ElementName: " + (ElementName != null ? ElementName : "<NULL>"));
            sb.AppendLine("ImportancePercentage: " + ImportancePercentage);
            sb.AppendLine("JobTitle: " + (JobTitle != null ? JobTitle : "<NULL>"));
            sb.AppendLine("OnetSocCode: " + (OnetSocCode != null ? OnetSocCode : "<NULL>"));
            sb.AppendLine("ScaleName: " + (ScaleName != null ? ScaleName : "<NULL>"));
            sb.AppendLine("SkillsTipID: " + (this != null ? Id.ToString() : "<NULL>"));
            sb.Append("------------ SkillTip dump ends---------------\n");

            return sb.ToString();
        }

        #endregion
    }
}