﻿using System.Text;

namespace DomainModel
{
    public class SectionType : EntityBase<int, SectionType>
    {
        #region Equals() implementation.

        /// <summary>
        ///     Determines whether the specified section is equal to the current section.
        /// </summary>
        /// <param name="obj">The section to compare with the current section.</param>
        /// <returns>
        ///     true if the specified section is equal to the current section; otherwise, false.
        /// </returns>
        /// <remarks>
        ///     Tests whether this and another object are equal in a way that will still pass
        ///     when proxy objects are being used.
        /// </remarks>
        public override bool Equals(object obj)
        {
            //Same reference check !
            if (ReferenceEquals(this, obj)) return true;

            //Type check !
            var other = obj as Section;
            if (other == null) return false;

            if (Id == 0)
                //Do a per-property search
                return
                    SectionTypeCD == other.SectionTypeCD &&
                    Label == other.Label;
            return Id == other.Id;
        }

        #endregion Equals() Implementation

        #region GetHashCode() implementation.

        /// <summary>
        ///     Serves as a hash function for a particular type.
        /// </summary>
        /// <returns>
        ///     A hash code for the current SectionType.
        /// </returns>
        public override int GetHashCode()
        {
            return (GetType().FullName + Id).GetHashCode();
        }

        #endregion GetHashCode() Implementation

        #region ToString() implementation

        public new virtual string ToString()
        {
            var sb = new StringBuilder(14);

            sb.Append("------------ SectionType dump starts---------------\n");
            sb.AppendLine(base.ToString());
            sb.AppendLine("DocZoneTypeCD: " + (DocZoneTypeCD != null ? DocZoneTypeCD : "<NULL>"));
            sb.AppendLine("Label: " + (Label != null ? Label : "<NULL>"));
            sb.AppendLine("SectionTypeCD: " + (SectionTypeCD != null ? SectionTypeCD : "<NULL>"));
            sb.Append("------------ SectionType dump ends---------------\n");

            return sb.ToString();
        }

        #endregion


        #region Properties

        public virtual int SectionTypeID { get; set; }
        public virtual string SectionTypeCD { get; set; }
        public virtual string Label { get; set; }
        public virtual string DocZoneTypeCD { get; set; }

        #endregion
    }
}