﻿using System;
using System.Collections.Generic;
using System.Text;

namespace DomainModel
{
    public class Section : EntityBase<int, Section>
    {
        public Section()
        {
        }

        public Section(string name)
        {
            Label = name;
        }

        /// <summary>
        ///     Create new section provide all the values
        /// </summary>
        /// <param name="label">Label</param>
        /// <param name="docZoneTypCd">DocZoneTypeCD</param>
        /// <param name="secTypCd">SectionTypeCD</param>
        /// <param name="sortIndex">SortIndex</param>
        public Section(string label, string docZoneTypCd, string secTypCd, short sortIndex)
        {
            Label = label;
            DocZoneTypeCD = docZoneTypCd;
            SectionTypeCD = secTypCd;
            SortIndex = sortIndex;
        }

        #region Equals() implementation.

        /// <summary>
        ///     Determines whether the specified section is equal to the current section.
        /// </summary>
        /// <param name="obj">The section to compare with the current section.</param>
        /// <returns>
        ///     true if the specified section is equal to the current section; otherwise, false.
        /// </returns>
        /// <remarks>
        ///     Tests whether this and another object are equal in a way that will still pass
        ///     when proxy objects are being used.
        /// </remarks>
        public override bool Equals(object obj)
        {
            //Same reference check !
            if (ReferenceEquals(this, obj)) return true;

            //Type check !
            var other = obj as Section;
            if (other == null) return false;

            if (Id == 0)
                //Do a per-property search
                return
                    SectionTypeCD == other.SectionTypeCD &&
                    SortIndex == other.SortIndex &&
                    Label == other.Label; // &&
            //(DocumentID == other.DocumentID);
            return Id == other.Id;
        }

        #endregion Equals() Implementation

        #region ToString() implementation

        public new virtual string ToString()
        {
            var sb = new StringBuilder(14);

            sb.Append("------------ Section dump starts---------------\n");
            sb.AppendLine(base.ToString());
            sb.AppendLine("DocumentID: " + (Document != null ? Document.Id.ToString() : "<NULL>"));
            sb.AppendLine("DocZoneTypeCD: " + (DocZoneTypeCD ?? "<NULL>"));
            sb.AppendLine("Label: " + (Label ?? "<NULL>"));
            sb.AppendLine("SectionTypeCD: " + (SectionTypeCD ?? "<NULL>"));
            sb.AppendLine("SortIndex: " + SortIndex);
            sb.Append("------------ Section dump ends---------------\n");

            return sb.ToString();
        }

        #endregion

        #region GetHashCode() implementation.

        /// <summary>
        ///     Serves as a hash function for a particular section.
        /// </summary>
        /// <returns>
        ///     A hash code for the current EventCode.
        /// </returns>
        public override int GetHashCode()
        {
            return (GetType().FullName + Id).GetHashCode();
        }

        #endregion GetHashCode() Implementation

        /// <summary>
        ///     Adds a given Paragraph to the Paragraphs collection & associates Section to Paragraph
        /// </summary>
        /// <param name="para">The para.</param>
        public virtual void AddParagraph(Paragraph para)
        {
            para.Section = this;
            Paragraphs.Add(para);
        }

        /// <summary>
        ///     Removes a given Paragraph from the Paragraphs collection.
        /// </summary>
        /// <param name="para">The para.</param>
        public virtual void RemoveParagraph(Paragraph para)
        {
            if (!Paragraphs.Contains(para))
                throw new ArgumentNullException("The Paragraph Section does not exist in the collection");

            para.Section = null;
            Paragraphs.Remove(para);
        }

        /// <summary>
        ///     Creates a shallow copy of the Section object
        /// </summary>
        /// <param name="isShallowClone">
        ///     Set to True if a Shallow Clone is required that doesnt have
        ///     any downstream collections or associated objects
        /// </param>
        /// <returns></returns>
        public virtual Section GetClone(bool isShallowClone)
        {
            Section Clone = null;

            try
            {
                if (isShallowClone)
                {
                    Clone = new Section();
                    Clone.Id = Id;
                    // Clone.Index = this.Index;
                    // Clone.Name = this.Name;
                    Clone.DocZoneTypeCD = DocZoneTypeCD;
                    Clone.SectionTypeCD = SectionTypeCD;
                    //   Clone.TopPadding = this.TopPadding;
                }
                else
                {
                    Clone = GetClone(false, false);
                }
            }
            catch (Exception)
            {
                //Hashtable htParams = new Hashtable(1);
                //htParams.Add("isShallowClone", isShallowClone);
                //MethodInfo objMethod = (MethodInfo)MethodBase.GetCurrentMethod();
                //GlobalExceptionHandler.customExceptionHandler(ex, objMethod, htParams, ErrorSeverityType.Normal);
            }

            return Clone;
        }

        #region Properties

        /// <summary>
        /// Private variable to hold the value for property DocumentID.
        /// </summary>
        //private int _documentid;

        /// <summary>
        ///     The ID of the parent Document object, accesses the Document property internally
        ///     This property is ONLY used by WebORB serialization.
        ///     DO NOT USE this property for standard NHibernate operations
        /// </summary>
        /// <value>The document ID.</value>
        //public virtual int DocumentID
        //{
        //    get
        //    {
        //        if (!(_documentid > 0))
        //            if (Document != null)
        //                _documentid = Document.Id;

        //        return _documentid;
        //    }
        //    set
        //    {
        //        _documentid = value;
        //        Document D = new Document();
        //        D.Id = value;
        //        Document = D;
        //    }
        //}
        private Document _document;

        /// <summary>
        ///     Reference to the Parent Document for this Section object
        ///     This property CANNOT be set to NULL
        /// </summary>
        /// <value>The document.</value>
        public virtual Document Document
        {
            get => _document;
            set
            {
                if (value != null)
                    _document = value;
                //OnDocumentChanged();
            }
        }

        public virtual string SectionTypeCD { get; set; }
        public virtual string Label { get; set; }
        public virtual short SortIndex { get; set; }
        public virtual string DocZoneTypeCD { get; set; }

        private IList<Paragraph> _paragraphs = new List<Paragraph>();

        public virtual IList<Paragraph> Paragraphs
        {
            get
            {
                if (_paragraphs == null)
                    _paragraphs = new List<Paragraph>();

                return _paragraphs;
            }
            set => _paragraphs = value;
        }

        #endregion
    }
}