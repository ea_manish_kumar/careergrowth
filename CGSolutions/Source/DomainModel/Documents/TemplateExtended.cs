﻿using System.Text;

namespace DomainModel
{
    public class TemplateExtended : EntityBase<int, TemplateExtended>
    {
        public virtual int TemplateID { get; set; }
        public virtual string LabelForTemplateType { get; set; }
        public virtual string LabelForFinalize { get; set; }
        public virtual int SortIndexForTemplateType { get; set; }
        public virtual int SortIndexForFinalize { get; set; }
        public virtual string LabelFormat { get; set; }
        public virtual string DescriptionFormat { get; set; }
        public virtual string LabelFormatKey { get; set; }
        public virtual string LabelTemplateKey { get; set; }
        public virtual string LabelFinalizeKey { get; set; }
        public virtual string DescFormatKey { get; set; }

        public new virtual string ToString()
        {
            var sb = new StringBuilder(7);

            sb.AppendLine("------------Template Extended dump starts---------------");
            sb.AppendLine("TemplateID:" + TemplateID);
            sb.AppendLine("LabelForTemplateType: " + (LabelForTemplateType != null ? LabelForTemplateType : "<NULL>"));
            sb.AppendLine("LabelForFinalize: " + (LabelForFinalize != null ? LabelForFinalize : "<NULL>"));
            sb.AppendLine("SortIndexForTemplateType: " + SortIndexForTemplateType);
            sb.AppendLine("SortIndexForFinalize: " + SortIndexForFinalize);
            sb.AppendLine("LabelFormat: " + (LabelFormat != null ? LabelFormat : "<NULL>"));
            sb.AppendLine("DescriptionFormat: " + (DescriptionFormat != null ? DescriptionFormat : "<NULL>"));
            sb.AppendLine("LabelFormatKey: " + (LabelFormatKey != null ? LabelFormatKey : "<NULL>"));
            sb.AppendLine("LabelTemplateKey: " + (LabelTemplateKey != null ? LabelTemplateKey : "<NULL>"));
            sb.AppendLine("LabelFinalizeKey: " + (LabelFinalizeKey != null ? LabelFinalizeKey : "<NULL>"));
            sb.AppendLine("DescFormatKey: " + (DescFormatKey != null ? DescFormatKey : "<NULL>"));
            sb.AppendLine("------------Template dump ends---------------");

            return sb.ToString();
        }
    }
}