﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Reflection;
using System.Text;
using System.Text.RegularExpressions;
using System.Xml;
using CommonModules.Configuration;
using CommonModules.Exceptions;

namespace DomainModel
{
    public class Document : EntityBase<int, Document>
    {
        public Document()
        {
            Sections = new List<Section>();
        }

        #region Equals() implementation.

        /// <summary>
        ///     Determines whether the specified Document is equal to the current Document.
        /// </summary>
        /// <param name="obj">The Document to compare with the current Document.</param>
        /// <returns>
        ///     true if the specified Document is equal to the current Document; otherwise, false.
        /// </returns>
        /// <remarks>
        ///     Tests whether this and another object are equal in a way that will still pass
        ///     when proxy objects are being used.
        /// </remarks>
        public override bool Equals(object obj)
        {
            //Same reference check !
            if (ReferenceEquals(this, obj)) return true;

            //Type check !
            var other = obj as Document;
            if (other == null) return false;

            if (Id == 0)
                //Do a per-property search
                return
                    DocumentTypeCD == other.DocumentTypeCD &&
                    TemplateCD == other.TemplateCD &&
                    UserID == other.UserID &&
                    SkinCD == other.SkinCD &&
                    Name == other.Name &&
                    DocStatusTypeCD == other.DocStatusTypeCD &&
                    DateCreated == other.DateCreated &&
                    DateModified == other.DateModified &&
                    IsDefSec == other.IsDefSec;
            return Id == other.Id;
        }

        #endregion Equals() Implementation

        #region GetHashCode() implementation.

        public override int GetHashCode()
        {
            return base.GetHashCode();
        }

        #endregion GetHashCode() Implementation

        /// <summary>
        ///     This method associates Document to Section & add Section to Document's sections list
        /// </summary>
        /// <param name="section"></param>
        public virtual void AddSection(Section section)
        {
            Sections.Add(section);
            section.Document = this;
        }

        /// <summary>
        ///     This method removes Document to Section & add Section to Document's sections list
        /// </summary>
        /// <param name="para"></param>
        public virtual void RemoveSection(Section section)
        {
            if (!Sections.Contains(section))
                throw new ArgumentNullException("The Section does not exist in the collection");

            section.Document = null;
            Sections.Remove(section);
        }

        private string[] getOrderedItemList(string input)
        {
            var output = input;

            //xnField.InnerXml = System.Web.HttpUtility.HtmlEncode(dc.CharValue);
            //output = output.Replace("<LI>", "<li>").Replace("<UL>", "").Replace("</LI>", "</li>").Replace("</UL>", "").Replace("<ul>", "").Replace("</ul>", "");
            //output = output.Replace("<P>", "<p>").Replace("</P>", "</p>").Replace("</p>", "</li>").Replace("<p>", "<li>");
            //output = output.Replace("</li><li>", "|").Replace("<li>", "").Replace("</li>", "");
            //output = output.Replace("<P>", "<p>").Replace("</P>", "</p>");

            /// Strip some of the html tags.
            output = Regex.Replace(output, @"(\</?(DIV|UL)(.*?)/?\>)", string.Empty, RegexOptions.IgnoreCase);
            output = Regex.Replace(output, "<p>", "<li>", RegexOptions.IgnoreCase);
            output = Regex.Replace(output, "</p>", "</li>", RegexOptions.IgnoreCase);
            /// To extract array of text of 'li' tags, replace it with double pipe (||) separator.
            output = Regex.Replace(output, "</li>", string.Empty, RegexOptions.IgnoreCase);
            output = Regex.Replace(output, "<li>", "||", RegexOptions.IgnoreCase);

            string[] stringSeparators = {"||"};
            var result = output.Split(stringSeparators, StringSplitOptions.RemoveEmptyEntries);
            return result;
        }

        private string[] getUnOrderedItemList(string input)
        {
            string[] result;
            input = ReplaceHtmlTags(input).Trim();
            input = input.Replace("<p>", "").Replace("</p>", "<br>");
            input = (input + "<br>").Replace("<br><br>", "<br>").Replace("<ul>", string.Empty)
                .Replace("</ul>", string.Empty);
            string[] stringSeparators = {"<br>"};

            result = input.Split(stringSeparators, StringSplitOptions.None);

            return result;
        }

        private void CreateXmlForStyles(XmlDataDocument xdoc, XmlNode xnDocStyle, XmlNode xnStyle,
            XmlAttribute xaCommon)
        {
            var hashStyles = new Hashtable();

            hashStyles.Add("FTSZ", "12");
            hashStyles.Add("HDSZ", "16");
            hashStyles.Add("FTFC", "Palatino Linotype");
            hashStyles.Add("VMRG", "0");
            hashStyles.Add("HMRG", "15");
            hashStyles.Add("PGSZ", "Letter");
            hashStyles.Add("SSPC", "1");
            hashStyles.Add("PSPC", "1");
            hashStyles.Add("LNWT", "1");
            hashStyles.Add("PIND", "0");
            hashStyles.Add("LNSP", "1");

            foreach (string key in hashStyles.Keys)
            {
                xnStyle = xdoc.CreateNode(XmlNodeType.Element, "", "style", "");

                xaCommon = xdoc.CreateAttribute("cd");
                xaCommon.InnerText = key;

                xnStyle.Attributes.Append(xaCommon);
                xnStyle.InnerText = hashStyles[key].ToString();

                xnDocStyle.AppendChild(xnStyle);
            }
        }

        private string GetProcessedData(string sValue)
        {
            var sPatternStartULFind = @"((<(?!(/li|ul))[^<]+?>)|[^>\s])([\s]*)(<li>)";
            var sPatternStartULReplace = @"$1$4<ul>$5";
            var sPatternEndULFind = @"(</li>)([\s]*)((<(?!(li|/ul))[^<]+?>)|[^<\s])";
            var sPatternEndULReplace = @"$1</ul>$2$3";
            try
            {
                if (Regex.IsMatch(sValue, sPatternStartULFind, RegexOptions.IgnoreCase))
                {
                    var re = new Regex(sPatternStartULFind, RegexOptions.IgnoreCase);
                    sValue = re.Replace(sValue, sPatternStartULReplace);
                }

                if (Regex.IsMatch(sValue, sPatternEndULFind, RegexOptions.IgnoreCase))
                {
                    var re = new Regex(sPatternEndULFind, RegexOptions.IgnoreCase);
                    sValue = re.Replace(sValue, sPatternEndULReplace);
                }
            }
            catch (Exception ex)
            {
                var htParams = new Hashtable(1);
                htParams.Add("sValue", sValue);
                var objMethod = (MethodInfo) MethodBase.GetCurrentMethod();
                GlobalExceptionHandler.customExceptionHandler(ex, objMethod, htParams, ErrorSeverityType.Normal);
            }

            //return sValue.Replace("&", "&amp;");
            return sValue;
        }

        private string ReplaceHtmlTags(string input)
        {
            var tags = ConfigManager.GetConfig("HTML_TAGS_HANDLER");
            var startingTag = string.Empty;
            var closingTag = string.Empty;

            var sbOutput = new StringBuilder();

            if (!string.IsNullOrEmpty(tags) && tags.Length > 0)
            {
                var tagsArr = tags.Split(',');

                if (tagsArr == null || tagsArr.Length <= 0) return input;

                foreach (var tag in tagsArr)
                {
                    startingTag = "&lt;" + tag + "&gt;";
                    closingTag = "&lt;/" + tag + "&gt;";

                    if (sbOutput.Length > 0) input = sbOutput.ToString();

                    if (input.IndexOf(startingTag) > -1)
                    {
                        sbOutput.Clear();
                        var arr = input.Split(new[] {startingTag}, StringSplitOptions.None);

                        if (arr.Length > 0)
                            for (var index = 0; index < arr.Length; index++)
                                sbOutput.Append(ReplaceClosingTagByBreak(closingTag, arr[index]));
                    }
                    else if (input.IndexOf(closingTag) > -1)
                    {
                        sbOutput.Clear();
                        sbOutput.Append(ReplaceClosingTagByBreak(closingTag, input));
                    }
                }

                if (sbOutput.Length > 0)
                {
                    while (sbOutput.ToString().IndexOf("<br><br><br>") > -1) sbOutput.Replace("<br><br><br>", "<br>");
                }
                else
                {
                    var regBrkWithSpace = new Regex(@"(<br>)[ ]{2,}", RegexOptions.IgnoreCase);
                    sbOutput.Append(regBrkWithSpace.Replace(input, @"<br>"));

                    regBrkWithSpace = new Regex(@"(<br><br>)+", RegexOptions.IgnoreCase);
                    while (regBrkWithSpace.IsMatch(sbOutput.ToString()))
                        sbOutput = new StringBuilder(regBrkWithSpace.Replace(sbOutput.ToString(), "<br>"));
                }
            }

            return sbOutput.ToString();
        }

        private string ReplaceClosingTagByBreak(string closingTag, string input)
        {
            return input.Replace(closingTag, "<br>");
        }

        #region Properties

        public virtual string DocumentTypeCD { get; set; }
        public virtual string TemplateCD { get; set; }
        public virtual int UserID { get; set; }
        public virtual string SkinCD { get; set; }
        public virtual string Name { get; set; }
        public virtual string DocStatusTypeCD { get; set; }
        public virtual DateTime DateCreated { get; set; }
        public virtual DateTime? DateModified { get; set; }
        public virtual bool IsDefSec { get; set; }

        public virtual IList<Section> Sections { get; set; }
        public virtual IList<DocStyle> DocStyles { get; set; }
        public virtual IList<DocPreference> Preferences { get; set; }

        #endregion

        #region ToString() Implementation

        public new virtual string ToString()
        {
            var sb = new StringBuilder(14);

            sb.Append("------------Document dump starts---------------\n");
            sb.AppendLine(base.ToString());
            sb.AppendLine("DocumentTypeCD: " + (DocumentTypeCD ?? "<NULL>"));
            sb.AppendLine("TemplateCD: " + (TemplateCD ?? "<NULL>"));
            sb.AppendLine("UserID: " + UserID);
            sb.AppendLine("SkinCD: " + (SkinCD ?? "<NULL>"));
            sb.AppendLine("Name: " + (Name ?? "<NULL>"));
            sb.AppendLine("DocStatusTypeCD: " + (DocStatusTypeCD ?? "<NULL>"));
            sb.AppendLine("DateCreated: " + DateCreated);
            sb.AppendLine("DateModified: " + (DateModified.HasValue ? DateModified.Value.ToString() : "<NULL>"));
            sb.AppendLine("IsDefSec: " + IsDefSec);
            sb.Append("------------Document dump ends---------------\n");

            return sb.ToString();
        }

        /// <summary>
        ///     ToString with a flag to do deep dump
        /// </summary>
        /// <param name="shallowDump">if set to <c>true</c> a shallow dump is performed.</param>
        /// <returns></returns>
        public virtual string ToString(bool shallowDump)
        {
            var returnStr = string.Empty;
            if (shallowDump)
            {
                returnStr = ToString();
            }
            else
            {
                var sb = new StringBuilder();

                returnStr = ToString() + sb;
            }

            return returnStr;
        }

        #endregion ToString() Implementation
    }
}