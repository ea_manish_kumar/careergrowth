﻿using System;
using System.Collections.Generic;
using System.Text;

namespace DomainModel
{
    [Serializable]
    public class Paragraph : EntityBase<int, Paragraph>
    {
        #region Equals() implementation.

        /// <summary>
        ///     Determines whether the specified paragraph is equal to the current paragraph.
        /// </summary>
        /// <param name="obj">The paragraph to compare with the current paragraph.</param>
        /// <returns>
        ///     true if the specified paragraph is equal to the current paragraph; otherwise, false.
        /// </returns>
        /// <remarks>
        ///     Tests whether this and another object are equal in a way that will still pass
        ///     when proxy objects are being used.
        /// </remarks>
        public override bool Equals(object obj)
        {
            //Same reference check !
            if (ReferenceEquals(this, obj)) return true;

            //Type check !
            var other = obj as Paragraph;
            if (other == null) return false;

            if (Id == 0)
                //Do a per-property search
                return
                    //(SectionID == other.SectionID) &&
                    SortIndex == other.SortIndex;
            return Id == other.Id;
        }

        #endregion Equals() Implementation

        #region ToString() implementation

        public new virtual string ToString()
        {
            var sb = new StringBuilder(14);

            sb.Append("------------ Paragraph dump starts---------------\n");
            sb.AppendLine(base.ToString());
            sb.AppendLine("SectionID: " + (Section != null ? Section.Id.ToString() : "<NULL>"));
            sb.AppendLine("SortIndex: " + SortIndex);
            sb.Append("------------ Paragraph dump ends---------------\n");

            return sb.ToString();
        }

        #endregion

        #region GetHashCode() implementation.

        /// <summary>
        ///     Serves as a hash function for a paragraph.
        /// </summary>
        /// <returns>
        ///     A hash code for the current EventCode.
        /// </returns>
        public override int GetHashCode()
        {
            return (GetType().FullName + Id).GetHashCode();
        }

        #endregion GetHashCode() Implementation

        /// <summary>
        ///     Adds a given DocData to the DocDatas collection.
        /// </summary>
        /// <param name="docData">The doc data.</param>
        /// <docDatam name="docData"></docDatam>
        public virtual void AddDocData(DocData docData)
        {
            docData.Paragraph = this;
            DocData.Add(docData);
        }

        /// <summary>
        ///     Removes a given DocData from the DocData collection.
        /// </summary>
        /// <param name="docData">The doc data.</param>
        /// <docDatam name="docData"></docDatam>
        public virtual void RemoveDocData(DocData docData)
        {
            if (!DocData.Contains(docData))
                throw new ArgumentNullException("The DocData Paragraph does not exist in the collection");

            docData.Paragraph = null;
            DocData.Remove(docData);
        }

        #region Constructor

        public Paragraph()
        {
        }

        /// <summary>
        ///     Create new paragraph, provide all the values
        /// </summary>
        /// <param name="sortIndex"></param>
        public Paragraph(short sortIndex)
        {
            SortIndex = sortIndex;
        }

        #endregion


        #region Properties

        private Section _section;

        /// <summary>
        ///     Reference to the Parent Section for this Paragraph object
        ///     This property CANNOT be set to NULL
        /// </summary>
        /// <value>The section.</value>
        public virtual Section Section
        {
            get => _section;
            set
            {
                if (value != null)
                    _section = value;
            }
        }


        /// <summary>
        ///     Gets or sets the index.
        /// </summary>
        /// <value>The index.</value>
        public virtual short SortIndex { get; set; }

        private IList<DocData> _docData = new List<DocData>();

        /// <summary>
        ///     Gets or sets the doc data.
        /// </summary>
        /// <value>The doc data.</value>
        public virtual IList<DocData> DocData
        {
            get
            {
                if (_docData == null)
                    _docData = new List<DocData>();

                return _docData;
            }
            set => _docData = value;
        }

        #endregion
    }
}