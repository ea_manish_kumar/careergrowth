﻿namespace DomainModel.Ecom
{
    /// <summary>
    ///     Defines the available Gateways in the system
    /// </summary>
    public enum GatewayID : short
    {
        /// <summary>
        ///     SkipJack
        /// </summary>
        SkipJack = 1,

        /// <summary>
        ///     WireCard
        /// </summary>
        WireCard = 10
    }

    /// <summary>
    ///     Defines the Last SuccessfulStage of the Subscription.
    /// </summary>
    public enum LastSuccessfulStage : short
    {
        /// <summary>
        ///     preparing the order.
        /// </summary>
        OrderPreparation = 1,

        /// <summary>
        ///     gateway call is being made at this stage.
        /// </summary>
        GateWayCall = 2,

        /// <summary>
        ///     payment done and order completed.
        /// </summary>
        OrderFullfillment = 3,

        /// <summary>
        ///     User is updated at this stage.
        /// </summary>
        UserUpdated = 4
    }

    /// <summary>
    /// Defines the available Gateways in the system
    /// </summary>
}