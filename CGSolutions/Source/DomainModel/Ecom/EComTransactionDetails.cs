using System;
using System.Text;

namespace DomainModel.Ecom
{
    public class EComTransactionDetails
    {
        #region GetHashCode() implementation.

        public override int GetHashCode()
        {
            return (GetType().FullName + UserID).GetHashCode();
        }

        #endregion GetHashCode() Implementation

        #region Equals() implementation.

        /// <summary>
        ///     Determines whether the specified Order is equal to the current Order.
        /// </summary>
        /// <param name="obj">The Order to compare with the current Order.</param>
        /// <returns>
        ///     true if the specified Order is equal to the current Order; otherwise, false.
        /// </returns>
        /// <remarks>
        ///     Tests whether this and another object are equal in a way that will still pass
        ///     when proxy objects are being used.
        /// </remarks>
        public override bool Equals(object obj)
        {
            //Same reference check !
            if (ReferenceEquals(this, obj)) return true;

            //Type check !
            var other = obj as EComTransactionDetails;
            if (other == null) return false;

            //Do a per-property search
            return
                UserID == other.UserID &&
                ReferenceNumber == other.ReferenceNumber;
        }

        #endregion Equals() Implementation

        #region ToString() Implementation

        public new virtual string ToString()
        {
            var sb = new StringBuilder();

            sb.AppendLine("------------EcomOrderRequestDTO dump starts---------------");
            sb.AppendLine("TransactionID: " + TransactionID);
            sb.AppendLine("UserID: " + UserID);
            sb.AppendLine("ReferenceNumber: " + (ReferenceNumber ?? "<NULL>"));
            sb.AppendLine("oAuthToken: " + (oAuthToken ?? "<NULL>"));
            sb.AppendLine("EcomResponse: " + (EcomResponse ?? "<NULL>"));
            sb.AppendLine("IsSandBoxMode: " + IsSandBoxMode);
            sb.AppendLine("Request: " + (Request ?? "<NULL>"));
            sb.AppendLine("Response: " + (Response ?? "<NULL>"));
            sb.AppendLine("TimeStamp: " + (TimeStamp.HasValue ? TimeStamp.Value.ToString() : "<NULL>"));
            sb.AppendLine("------------EcomOrderRequestDTO dump ends---------------");

            return sb.ToString();
        }

        #endregion ToString() Implementation

        #region Property Declarations

        public virtual int TransactionID { get; set; }

        public virtual int UserID { get; set; }

        public virtual string ReferenceNumber { get; set; }

        public virtual string oAuthToken { get; set; }

        public virtual string EcomResponse { get; set; }

        public virtual string ClientCD { get; set; }

        public virtual bool IsSandBoxMode { get; set; }

        public virtual string Request { get; set; }

        public virtual string Response { get; set; }

        public virtual DateTime? TimeStamp { get; set; }

        #endregion
    }
}