using System;
using System.ComponentModel.DataAnnotations;
using System.Text;
using Newtonsoft.Json;

namespace DomainModel.Ecom
{
    public class Subscription
    {
        [JsonProperty(PropertyName = "id")] public int SubscriptionID { get; set; }

        [JsonProperty(PropertyName = "customer_id")]
        public int CustomerID { get; set; }

        [JsonProperty(PropertyName = "plan_id")]
        public short PlanID { get; set; }

        [Required]
        [StringLength(4)]
        [JsonProperty(PropertyName = "subscription_status_cd")]
        public string SubscriptionStatusCD { get; set; }

        [JsonProperty(PropertyName = "start_date")]
        public DateTime? StartDate { get; set; }

        [JsonProperty(PropertyName = "next_billing_date")]
        public DateTime? NextBillingDate { get; set; }

        [JsonProperty(PropertyName = "expiry_date")]
        public DateTime? ExpiryDate { get; set; }

        [Required]
        [JsonProperty(PropertyName = "last_billing_date")]
        public DateTime? LastBillingDate { get; set; }

        [Required]
        [JsonProperty(PropertyName = "master_transaction_id")]
        public int MasterTransactionID { get; set; }

        [JsonProperty(PropertyName = "renewal_count")]
        public short? RenewalCount { get; set; }

        [Required]
        [JsonProperty(PropertyName = "amount")]
        public decimal Amount { get; set; }

        [JsonProperty(PropertyName = "payment_id")]
        public int PaymentID { get; set; }

        [JsonProperty(PropertyName = "LinkSubscription")]
        public Subscription LinkedSubscription { get; set; }

        [JsonProperty(PropertyName = "credit_card")]
        public CreditCard CreditCard { get; set; }

        [JsonProperty(PropertyName = "payment_item_id")]
        public int PaymentItemID { get; set; }


        public new virtual string ToString()
        {
            var sb = new StringBuilder(10);

            sb.AppendLine("------------Customer dump starts---------------");
            sb.AppendLine("CustomerID: " + CustomerID);
            sb.AppendLine("SubscriptionID: " + SubscriptionID);
            sb.AppendLine("PlanID: " + PlanID);
            sb.AppendLine("SubscriptionStatusCD: " + (SubscriptionStatusCD ?? "<NULL>"));
            sb.AppendLine(
                "NextBillingDate: " + (NextBillingDate.HasValue ? NextBillingDate.Value.ToString() : "<NULL>"));
            sb.AppendLine("StartDate: " + (StartDate.HasValue ? StartDate.Value.ToString() : "<NULL>"));
            sb.AppendLine("ExpiryDate: " + (ExpiryDate.HasValue ? ExpiryDate.Value.ToString() : "<NULL>"));
            sb.AppendLine(
                "LastBillingDate: " + (LastBillingDate.HasValue ? LastBillingDate.Value.ToString() : "<NULL>"));
            sb.AppendLine("MasterTransactionID: " + MasterTransactionID);
            sb.AppendLine("RenewalCount: " + (RenewalCount.HasValue ? RenewalCount.Value.ToString() : "<NULL>"));
            sb.AppendLine("Amount: " + Amount);
            sb.AppendLine("PaymentID: " + PaymentID);
            sb.AppendLine("PaymentID: " + PaymentItemID);
            sb.AppendLine("------------Customer dump ends---------------");

            return sb.ToString();
        }
    }
}