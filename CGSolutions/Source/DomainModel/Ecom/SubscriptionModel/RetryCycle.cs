﻿namespace DomainModel.Ecom
{
    public class RetryCycle
    {
        public short RetryCycleID { get; set; }
        public string ClientCD { get; set; }
        public string GatewayCD { get; set; }
        public string DeclientCode { get; set; }
        public string Description { get; set; }
        public short RetryCount { get; set; }
        public string FrequencyCD { get; set; }
        public short FrequencyInterval { get; set; }
    }
}