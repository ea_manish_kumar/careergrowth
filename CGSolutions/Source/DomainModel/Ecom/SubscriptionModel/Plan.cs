using Newtonsoft.Json;

namespace DomainModel.Ecom
{
    public class Plan
    {
        [JsonProperty(PropertyName = "id")] public short Id { get; set; }

        [JsonIgnore] public string ClientCD { get; set; }

        [JsonProperty(PropertyName = "sku_id")]
        public string ClientSKUID { get; set; }

        [JsonProperty(PropertyName = "name")] public string Name { get; set; }

        [JsonProperty(PropertyName = "description")]
        public string Description { get; set; }

        [JsonProperty(PropertyName = "amount")]
        public decimal? Amount { get; set; }

        [JsonProperty(PropertyName = "frequency_cd")]
        public string FrequencyCD { get; set; }

        [JsonProperty(PropertyName = "frequency_interval")]
        public short? FrequencyInterval { get; set; }

        [JsonProperty(PropertyName = "cycles")]
        public short? Cycles { get; set; }

        [JsonProperty(PropertyName = "trial_amount")]
        public decimal? TrialAmount { get; set; }

        [JsonProperty(PropertyName = "trial_days")]
        public int? TrialDays { get; set; }

        [JsonProperty(PropertyName = "trial_split_amount")]
        public decimal? TrialSplitAmount { get; set; }

        [JsonProperty(PropertyName = "currency")]
        public string CurrencyCD { get; set; }
    }
}