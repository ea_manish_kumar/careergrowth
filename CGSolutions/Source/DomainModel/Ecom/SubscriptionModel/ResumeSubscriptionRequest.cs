﻿using System.ComponentModel.DataAnnotations;

namespace DomainModel.Ecom
{
    public class ResumeSubscriptionRequest
    {
        public CreditCard credit_card { get; set; }

        [Required(ErrorMessage = "Amount is required")]
        public decimal amount { get; set; }

        public string descriptor { get; set; }
        public string ip_address { get; set; }
    }
}