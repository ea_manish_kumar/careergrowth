﻿using System.ComponentModel.DataAnnotations;
using Newtonsoft.Json;

namespace DomainModel.Ecom
{
    public class DeactivateSubscriptionRequest
    {
        [Required(ErrorMessage = "Reason Code is required")]
        [JsonProperty(PropertyName = "reason_cd")]
        public string ReasonCode { get; set; }

        [JsonProperty(PropertyName = "reason_message")]
        public string ReasonDescription { get; set; }
    }
}