namespace DomainModel.Ecom
{
    public class SubscriptionTransactions
    {
        public virtual int TransactionID { get; set; }
        public virtual int SubscriptionID { get; set; }
        public virtual Transaction Transaction { get; set; }
        public virtual Subscription Subscription { get; set; }
        public virtual decimal? Amount { get; set; }

        #region NHibernate Composite Key Requirements

        public override bool Equals(object obj)
        {
            if (obj == null) return false;
            var t = obj as SubscriptionTransactions;
            if (t == null) return false;
            if (TransactionID == t.TransactionID
                && SubscriptionID == t.SubscriptionID)
                return true;

            return false;
        }

        public override int GetHashCode()
        {
            var hash = GetType().GetHashCode();
            hash = (hash * 397) ^ TransactionID.GetHashCode();
            hash = (hash * 397) ^ SubscriptionID.GetHashCode();

            return hash;
        }

        #endregion
    }
}