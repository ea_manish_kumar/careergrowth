﻿using System;
using System.Text;

namespace DomainModel.Ecom
{
    public class SubscriptionRequest
    {
        public int sku_id { get; set; }
        public int plan_id { get; set; }
        public DateTime? start_date { get; set; }
        public CreditCard credit_card { get; set; }
        public decimal amount { get; set; }
        public string descriptor { get; set; }
        public string cuid { get; set; }
        public string ip_address { get; set; }
        public string email { get; set; }
        public bool? charge_now { get; set; }

        public LinkedSubscriptionRequest linked_subscription { get; set; }

        public new virtual string ToString()
        {
            var sb = new StringBuilder();

            sb.AppendLine("------------Subscription Request dump starts---------------");
            sb.AppendLine("sku_id: " + sku_id);
            sb.AppendLine("plan_id: " + plan_id);
            sb.AppendLine("start_date: " + (start_date.HasValue ? start_date.Value.ToString() : "<NULL>"));
            sb.AppendLine("credit_card: " + (credit_card != null ? credit_card.ToString() : "<NULL>"));
            sb.AppendLine("amount: " + amount);
            sb.AppendLine("descriptor: " + descriptor ?? "<NULL>");
            sb.AppendLine("cuid: " + cuid ?? "<NULL>");
            sb.AppendLine("ip_address: " + ip_address ?? "<NULL>");
            sb.AppendLine("email: " + email ?? "<NULL>");
            sb.AppendLine("charge_now: " + (charge_now.HasValue ? charge_now.Value.ToString() : "<NULL>"));
            sb.AppendLine("linked_subscription: " +
                          (linked_subscription != null ? linked_subscription.ToString() : "<NULL>"));
            sb.AppendLine("------------Subscription Request dump ends---------------");

            return sb.ToString();
        }
    }
}