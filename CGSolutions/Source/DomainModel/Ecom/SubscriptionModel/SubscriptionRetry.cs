﻿using System;

namespace DomainModel.Ecom
{
    public class SubscriptionRetry
    {
        public int SubscriptionID { get; set; }
        public string DeclineCode { get; set; }
        public int FailCount { get; set; }
        public DateTime CreatedOn { get; set; }
        public DateTime ModifiedOn { get; set; }
    }
}