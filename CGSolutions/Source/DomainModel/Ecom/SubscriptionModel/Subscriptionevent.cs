using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using Newtonsoft.Json;

namespace DomainModel.Ecom
{
    public class SubscriptionEvent
    {
        [JsonProperty(PropertyName = "id")] public int SubscriptionEventID { get; set; }

        [JsonProperty(PropertyName = "subscription_id")]
        public int SubscriptionID { get; set; }

        [Required]
        [StringLength(4)]
        [JsonProperty(PropertyName = "event")]
        public string EventCD { get; set; }

        [StringLength(1024)]
        [JsonProperty(PropertyName = "description")]
        public string Descriptor { get; set; }

        [JsonProperty(PropertyName = "amount")]
        public decimal? Amount { get; set; }

        [JsonProperty(PropertyName = "created_on")]
        public DateTime? CreatedOn { get; set; }

        [JsonProperty(PropertyName = "subscription_event_property")]
        public IList<SubscriptionEventProperty> EventProperty { get; set; }
    }

    public class SubscriptionEventProperty
    {
        [JsonProperty(PropertyName = "key")] public string Key { get; set; }

        [JsonProperty(PropertyName = "value")] public string Value { get; set; }
    }
}