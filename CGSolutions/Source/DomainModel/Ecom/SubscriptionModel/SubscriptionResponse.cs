﻿using System.Collections.Generic;
using System.Text;

namespace DomainModel.Ecom
{
    public class SubscriptionResponse
    {
        private IList<Transaction> _transaction;
        public int payment_id;
        public int id { get; set; }
        public Customer customer { get; set; }
        public Subscription subscription { get; set; }

        public IList<Transaction> transaction
        {
            get
            {
                if (_transaction == null) _transaction = new List<Transaction>();
                return _transaction;
            }

            set => _transaction = value;
        }

        public string status { get; set; }
        public string mode { get; set; }
        public string fail_code { get; set; }
        public string fail_message { get; set; }


        public new virtual string ToString()
        {
            var sb = new StringBuilder();

            sb.AppendLine("------------Subscription Response dump starts---------------");
            sb.AppendLine("id: " + id);
            sb.AppendLine("customer: " + (customer != null ? customer.ToString() : "<NULL>"));
            sb.AppendLine("Subscription: " + (subscription != null ? subscription.ToString() : "<NULL>"));
            sb.AppendLine("status: " + (status ?? "<NULL>"));
            sb.AppendLine("mode: " + (mode ?? "<NULL>"));
            sb.AppendLine("payment_id: " + payment_id);
            sb.AppendLine("fail_code: " + (fail_code ?? "<NULL>"));
            sb.AppendLine("fail_message: " + (fail_message ?? "<NULL>"));
            sb.AppendLine("------------Subscription Response dump ends---------------");

            return sb.ToString();
        }
    }
}