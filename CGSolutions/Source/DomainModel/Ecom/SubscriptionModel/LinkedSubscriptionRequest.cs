﻿using System.Text;

namespace DomainModel.Ecom
{
    public class LinkedSubscriptionRequest
    {
        public int link_to_subscription_id { get; set; }
        public string link_type_cd { get; set; }

        public new virtual string ToString()
        {
            var sb = new StringBuilder(10);

            sb.AppendLine("------------LinkedSubscriptionRequest dump starts---------------");
            sb.AppendLine("link_to_subscription_id: " + link_to_subscription_id);
            sb.AppendLine("link_type_cd: " + link_type_cd ?? "<NULL>");
            sb.AppendLine("------------LinkedSubscriptionRequest dump ends---------------");

            return sb.ToString();
        }
    }
}