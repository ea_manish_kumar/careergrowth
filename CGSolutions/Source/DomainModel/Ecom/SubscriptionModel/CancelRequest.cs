﻿using System;
using Newtonsoft.Json;

namespace DomainModel.Ecom
{
    public class CancelRequest
    {
        [JsonProperty(PropertyName = "expiry_date")]
        public DateTime? Expirydate { get; set; }

        [JsonProperty(PropertyName = "mode")] //PHONE, UCAN, EMAIL, CHAT
        public string Mode { get; set; }

        [JsonProperty(PropertyName = "reason_cd")]
        public string ReasonCD { get; set; }

        [JsonProperty(PropertyName = "reason_message")]
        public string Reason_Message { get; set; } //Remarks Enterd by user
    }
}