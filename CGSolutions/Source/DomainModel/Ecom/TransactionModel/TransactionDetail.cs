namespace DomainModel.Ecom
{
    public class TransactionDetails
    {
        public int TransactionID { get; set; }
        public Transaction Transaction { get; set; }
        public string Request { get; set; }
        public string Response { get; set; }
    }
}