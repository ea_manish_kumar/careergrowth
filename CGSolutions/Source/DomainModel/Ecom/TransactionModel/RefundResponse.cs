﻿namespace DomainModel.Ecom
{
    public class RefundResponse
    {
        public Transaction transaction { get; set; }
        public string status { get; set; }
    }
}