using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace DomainModel.Ecom
{
    public class BusinessCase
    {
        public short BusinessCaseID { get; set; }
        public Client Client { get; set; }
        public string ClientCD { get; set; }
        public Gateway Gateway { get; set; }

        [StringLength(100)] public string Label { get; set; }

        [StringLength(100)] public string Identifier { get; set; }

        [StringLength(3)] public string CurrencyCD { get; set; }

        [StringLength(400)] public string Description { get; set; }

        [StringLength(100)] public string Criterion { get; set; }

        public IList<Transaction> Transaction { get; set; }
    }
}