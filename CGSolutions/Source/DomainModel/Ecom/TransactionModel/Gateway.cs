using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace DomainModel.Ecom
{
    public class Gateway
    {
        public virtual string GatewayCD { get; set; }

        [Required] [StringLength(20)] public virtual string Name { get; set; }

        [StringLength(20)] public virtual string Platform { get; set; }

        public virtual IList<BusinessCase> BusinessCase { get; set; }
    }
}