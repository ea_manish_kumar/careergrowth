﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;
using Newtonsoft.Json;

namespace DomainModel.Ecom
{
    public class TransactionRefund
    {
        [Required] private DateTime? _createdOn;

        [JsonProperty(PropertyName = "id")] public int TransactionID { get; set; }

        [Required]
        [StringLength(4)]
        [JsonProperty(PropertyName = "transaction_type")]
        public string TransactionTypeCD { get; set; }

        [StringLength(4)]
        [JsonProperty(PropertyName = "result")]
        public string ResultCD { get; set; }

        [Required]
        [JsonProperty(PropertyName = "amount")]
        public decimal Amount { get; set; }

        [StringLength(40)]
        [JsonProperty(PropertyName = "gateway_reference")]
        public string GatewayReference { get; set; }

        [StringLength(3)]
        [JsonProperty(PropertyName = "currency_cd")]
        public string CurrencyCD { get; set; }

        [JsonProperty(PropertyName = "created_on")]
        [Required]
        public DateTime CreatedOn
        {
            get =>
                _createdOn.HasValue
                    ? _createdOn.Value
                    : DateTime.Now;
            set => _createdOn = value;
        }


        [JsonProperty(PropertyName = "payment_item")]
        public IList<PaymentItem> PaymentItems { get; set; }

        [JsonProperty(PropertyName = "chargetransaction_id")]
        public int? chargeTransactionId { get; set; }

        #region ToString() Implementation

        /// <summary>
        ///     This methods returns a string representation of this class.
        /// </summary>
        /// <returns>A string representation of this class.</returns>
        public new virtual string ToString()
        {
            var sb = new StringBuilder(8);

            sb.Append("------------TransactionRefund dump starts---------------\n");
            sb.AppendLine(base.ToString());
            sb.Append("TransactionID: " + TransactionID + "\n");
            sb.AppendLine("TransactionTypeCD: " + TransactionTypeCD);
            sb.AppendLine("ResultCD: " + ResultCD);
            sb.AppendLine("CreatedOn:" + CreatedOn);
            sb.AppendLine("Amount:" + Amount);
            sb.Append("------------TransactionRefund dump ends---------------\n");

            return sb.ToString();
        }

        #endregion ToString() Implementation
    }
}