using System;

namespace DomainModel.Ecom
{
    public class Refund
    {
        public int id { get; set; }
        public string reason_cd { get; set; }
        public string reason_message { get; set; }
        public DateTime CreatedOn { get; set; }
        public decimal amount { get; set; }
    }
}