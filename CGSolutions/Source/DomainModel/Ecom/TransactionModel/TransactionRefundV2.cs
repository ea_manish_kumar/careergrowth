﻿using System;
using System.ComponentModel.DataAnnotations;
using System.Text;
using Newtonsoft.Json;

namespace DomainModel.Ecom
{
    public class TransactionRefundV2
    {
        [Required] private DateTime? _createdOn;

        [JsonProperty(PropertyName = "id")] public int TransactionID { get; set; }

        [StringLength(4)]
        [JsonProperty(PropertyName = "result")]
        public string ResultCD { get; set; }

        [Required]
        [StringLength(4)]
        [JsonProperty(PropertyName = "transaction_type")]
        public string TransactionTypeCD { get; set; }

        [StringLength(40)]
        [JsonProperty(PropertyName = "gateway_reference")]
        public string GatewayReference { get; set; }

        [StringLength(3)]
        [JsonProperty(PropertyName = "currency_cd")]
        public string CurrencyCode { get; set; }

        [Required]
        [JsonProperty(PropertyName = "amount")]
        public decimal Amount { get; set; }

        [JsonProperty(PropertyName = "created_on")]
        [Required]
        public DateTime CreatedOn
        {
            get =>
                _createdOn.HasValue
                    ? _createdOn.Value
                    : DateTime.Now;
            set => _createdOn = value;
        }

        [JsonProperty(PropertyName = "charge_transaction_id")]
        public int? chargeTransactionId { get; set; }

        [StringLength(4)]
        [JsonProperty(PropertyName = "gateway")]
        public string GateWay { get; set; }

        [JsonProperty(PropertyName = "credit_card")]
        public CreditCard CreditCard { get; set; }

        [StringLength(4)]
        [JsonProperty(PropertyName = "sub_type")]
        public string SubType { get; set; }

        #region ToString() Implementation

        /// <summary>
        ///     This methods returns a string representation of this class.
        /// </summary>
        /// <returns>A string representation of this class.</returns>
        public new virtual string ToString()
        {
            var sb = new StringBuilder(8);

            sb.Append("------------TransactionRefund dump starts---------------\n");
            sb.AppendLine(base.ToString());
            sb.Append("TransactionID: " + TransactionID + "\n");
            sb.AppendLine("TransactionTypeCD: " + TransactionTypeCD);
            sb.AppendLine("ResultCD: " + ResultCD);
            sb.AppendLine("CreatedOn:" + CreatedOn);
            sb.AppendLine("Amount:" + Amount);
            sb.Append("------------TransactionRefund dump ends---------------\n");

            return sb.ToString();
        }

        #endregion ToString() Implementation
    }
}