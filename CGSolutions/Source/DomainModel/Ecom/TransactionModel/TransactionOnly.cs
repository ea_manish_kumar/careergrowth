﻿using System;
using System.ComponentModel.DataAnnotations;
using Newtonsoft.Json;

namespace DomainModel.Ecom
{
    public class TransactionOnly
    {
        [Required] private DateTime? _createdOn;

        [JsonProperty(PropertyName = "id")] public int TransactionID { get; set; }

        [Required]
        [StringLength(4)]
        [JsonProperty(PropertyName = "transaction_type")]
        public string TransactionTypeCD { get; set; }

        [StringLength(4)]
        [JsonProperty(PropertyName = "result")]
        public string ResultCD { get; set; }

        [Required]
        [JsonProperty(PropertyName = "amount")]
        public decimal Amount { get; set; }

        [StringLength(40)]
        [JsonProperty(PropertyName = "gateway_reference")]
        public string GatewayReference { get; set; }

        [StringLength(3)]
        [JsonProperty(PropertyName = "currency_cd")]
        public string CurrencyCD { get; set; }

        [JsonProperty(PropertyName = "created_on")]
        [Required]
        public DateTime CreatedOn
        {
            get =>
                _createdOn.HasValue
                    ? _createdOn.Value
                    : DateTime.Now;
            set => _createdOn = value;
        }
    }
}