using System.ComponentModel.DataAnnotations;

namespace DomainModel.Ecom
{
    public class Failedtransaction
    {
        public virtual int TransactionID { get; set; }
        public virtual Transaction Transaction { get; set; }

        [Required] [StringLength(50)] public virtual string DeclineCode { get; set; }

        [StringLength(250)] public virtual string DeclineMessage { get; set; }
    }
}