﻿namespace DomainModel.Ecom
{
    public class EComTransactionExtendedDTO : Transaction
    {
        public EComTransactionExtendedDTO(Transaction transaction)
        {
            /// Set base class properties
            Amount = transaction.Amount;
            CreatedOn = transaction.CreatedOn;
            CurrencyCD = transaction.CurrencyCD;
            GatewayReference = transaction.GatewayReference;
            PaymentItems = transaction.PaymentItems;
            ResultCD = transaction.ResultCD;
            TransactionID = transaction.TransactionID;
            TransactionTypeCD = transaction.TransactionTypeCD;
        }

        public int SKUID { get; set; }
        public int UserID { get; set; }
        public int OrderID { get; set; }
        public string TransactionType { get; set; }
        public string ResultType { get; set; }
    }
}