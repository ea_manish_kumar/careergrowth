﻿using System.Collections.Generic;

namespace DomainModel.Ecom
{
    public class EComSubscriptionExtendedDTO : Subscription
    {
        public EComSubscriptionExtendedDTO(Subscription subscription)
        {
            /// Set base class properties
            Amount = subscription.Amount;
            CustomerID = subscription.CustomerID;
            ExpiryDate = subscription.ExpiryDate;
            LastBillingDate = subscription.LastBillingDate;
            MasterTransactionID = subscription.MasterTransactionID;
            NextBillingDate = subscription.NextBillingDate;
            PaymentID = subscription.PaymentID;
            PlanID = subscription.PlanID;
            RenewalCount = subscription.RenewalCount;
            StartDate = subscription.StartDate;
            SubscriptionID = subscription.SubscriptionID;
            SubscriptionStatusCD = subscription.SubscriptionStatusCD;
        }

        public int SKUID { get; set; }
        public int UserID { get; set; }
        public int OrderID { get; set; }

        public string SubscriptionType { get; set; }
        public string SubscriptionTypeCD { get; set; }
        public string ScheduleType { get; set; }
        public string ScheduleTypeCD { get; set; }
        public string EmailAddress { get; set; }
        public string SubscriptionSKUDescription { get; set; }
        public string BillingStatus { get; set; }
        public string ProductDescription { get; set; }
        public string CurrencySymbol { get; set; }

        public IList<EComTransactionExtendedDTO> TransactionsDTO { get; set; }
    }
}