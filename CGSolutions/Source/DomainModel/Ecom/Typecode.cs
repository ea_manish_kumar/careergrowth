using System.ComponentModel.DataAnnotations;

namespace DomainModel.Ecom
{
    public class TypeCode
    {
        public virtual string CodeGroup { get; set; }
        public virtual string TypeCD { get; set; }

        [StringLength(250)] public virtual string Description { get; set; }

        public virtual byte? SortIndex { get; set; }

        #region NHibernate Composite Key Requirements

        public override bool Equals(object obj)
        {
            if (obj == null) return false;
            var t = obj as TypeCode;
            if (t == null) return false;
            if (CodeGroup == t.CodeGroup
                && TypeCD == t.TypeCD)
                return true;

            return false;
        }

        public override int GetHashCode()
        {
            var hash = GetType().GetHashCode();
            hash = (hash * 397) ^ CodeGroup.GetHashCode();
            hash = (hash * 397) ^ TypeCD.GetHashCode();

            return hash;
        }

        #endregion
    }
}