﻿using System;

namespace DomainModel.Ecom
{
    public class SubscriptionNotification
    {
        public int ClientSubscriptionNotificationID { get; set; }
        public string ClientCD { get; set; }
        public int SubscriptionID { get; set; }
        public string NotificationTypeCD { get; set; }
        public short AttemptCount { get; set; }
        public DateTime LastAttemptedOn { get; set; }
        public string StatusCD { get; set; }
        public DateTime CreatedOn { get; set; }
    }
}