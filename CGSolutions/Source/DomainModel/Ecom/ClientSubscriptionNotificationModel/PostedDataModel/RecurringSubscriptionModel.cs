﻿namespace DomainModel.Ecom
{
    public class RecurringSubscriptionModel : BaseModel
    {
        private SubscriptionModel _subscription;

        public SubscriptionModel Subscription
        {
            get
            {
                if (_subscription == null) _subscription = new SubscriptionModel();
                return _subscription;
            }
            set => _subscription = value;
        }
    }
}