﻿using System;

namespace DomainModel.Ecom
{
    public class SubscriptionModel
    {
        public int SubscriptionId { get; set; }
        public int PlanId { get; set; }
        public int CustomerId { get; set; }
        public int PaymentId { get; set; }
        public string SubscriptionStatusCD { get; set; }
        public DateTime? StartDate { get; set; }
        public DateTime? LastBillingDate { get; set; }
        public DateTime? NextBillingDate { get; set; }
        public DateTime? ExpiryDate { get; set; }
        public DateTime? CreatedOn { get; set; }
        public DateTime? ModifiedOn { get; set; }
        public short? RenewalCount { get; set; }
        public decimal? Amount { get; set; }
    }
}