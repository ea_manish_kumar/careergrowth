﻿namespace DomainModel.Ecom
{
    public class ChainLinkedSubscriptionModel : BaseModel
    {
        private SubscriptionModel _activeSubscription;
        private SubscriptionModel _expiredSubscription;

        public SubscriptionModel ExpiredSubscription
        {
            get
            {
                if (_expiredSubscription == null) _expiredSubscription = new SubscriptionModel();
                return _expiredSubscription;
            }
            set => _expiredSubscription = value;
        }

        public SubscriptionModel ActiveSubscription
        {
            get
            {
                if (_activeSubscription == null) _activeSubscription = new SubscriptionModel();
                return _activeSubscription;
            }
            set => _activeSubscription = value;
        }
    }
}