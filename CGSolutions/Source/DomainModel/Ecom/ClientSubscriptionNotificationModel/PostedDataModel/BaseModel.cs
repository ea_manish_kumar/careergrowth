﻿namespace DomainModel.Ecom
{
    public abstract class BaseModel
    {
        public int SubscriptionNotificationId { get; set; }
        public string ClientCD { get; set; }
    }
}