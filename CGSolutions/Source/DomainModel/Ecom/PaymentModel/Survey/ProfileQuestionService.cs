﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using IBH.AppFramework.Exceptions;
using IBH.Services.Base;
using System.Reflection;
using IBH.DomainModel.Survey;
using IBH.Data.Survey;
using IBH.DomainModel.Content;
using IBH.AppFramework;

namespace IBH.Services.Survey
{
    public class ProfileQuestionService : ServiceBase
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="UserSurveyService"/> class.
        /// </summary>
        public ProfileQuestionService()
            : base()
        {

        }


        /// <summary>
        /// Initializes a new instance of the <see cref="UserSurveyService"/> class.
        /// </summary>
        /// <param name="isUnAuthenticated">if set to <c>true</c> [is un authenticated].</param>
        public ProfileQuestionService(bool isUnAuthenticated)
            : base(isUnAuthenticated)
        {

        }

        /// <summary>
        /// Initializes a new instance of the <see cref="UserSurveyService"/> class.
        /// </summary>
        /// <param name="partyId">The party id.</param>
        public ProfileQuestionService(int partyId)
            : base(partyId)
        {

        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="QuestionID"></param>
        /// <returns></returns>
        public ProfileQuestion GetProfileQuestionById(int QuestionID)
        {
            ProfileQuestion profileQuestion = null;
            try
            {
                profileQuestion = SurveyDataFactory.ProfileQuestionData.GetProfileQuestion(QuestionID);
            }
            catch (Exception ex)
            {
                Hashtable htParams = new Hashtable(1);
                htParams.Add("QuestionID", QuestionID);
                MethodInfo objMethod = (MethodInfo)MethodBase.GetCurrentMethod();
                GlobalExceptionHandler.customExceptionHandler(ex, objMethod, htParams, ErrorSeverityType.Normal);
            }
            return profileQuestion;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="PartyId"></param>
        /// <param name="QuestionID"></param>
        /// <returns></returns>
        public IList<UserProfileResponses> GetUserProfileResponses(int PartyId, int QuestionID)
        {
            IList<UserProfileResponses> lstuserProfileResponses = null;
            try
            {
                lstuserProfileResponses = SurveyDataFactory.UserProfileResponsesData.GetUserProfileResponses(PartyId, QuestionID);
            }
            catch (Exception ex)
            {
                Hashtable htParams = new Hashtable(2);
                htParams.Add("PartyId", PartyId);
                htParams.Add("QuestionID", QuestionID);
                MethodInfo objMethod = (MethodInfo)MethodBase.GetCurrentMethod();
                GlobalExceptionHandler.customExceptionHandler(ex, objMethod, htParams, ErrorSeverityType.Normal);
            }
            return lstuserProfileResponses;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="PartyId"></param>
        /// <param name="QuestionID"></param>
        /// <returns></returns>
        public UserProfileFreeForm GetUserProfileFreeForm(int PartyId, int QuestionID)
        {
            UserProfileFreeForm userprofilefreeform = null;
            try
            {
                //userprofilefreeform = SurveyDataFactory.UserProfileFreeFormData.GetUserProfileFreeForm(PartyId, QuestionID);
            }
            catch (Exception ex)
            {
                Hashtable htParams = new Hashtable(2);
                htParams.Add("PartyId", PartyId);
                htParams.Add("QuestionID", QuestionID);
                MethodInfo objMethod = (MethodInfo)MethodBase.GetCurrentMethod();
                GlobalExceptionHandler.customExceptionHandler(ex, objMethod, htParams, ErrorSeverityType.Normal);
            }
            return userprofilefreeform;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="PartyId"></param>
        /// <returns></returns>
        public IList<UserProfileFreeForm> GetUserProfileFreeForm(int PartyId)
        {
            IList<UserProfileFreeForm> lstuserprofilefreeform = null;
            try
            {
                //lstuserprofilefreeform = SurveyDataFactory.UserProfileFreeFormData.GetUserProfileFreeForm(PartyId);
            }
            catch (Exception ex)
            {
                Hashtable htParams = new Hashtable(2);
                htParams.Add("PartyId", PartyId);
                MethodInfo objMethod = (MethodInfo)MethodBase.GetCurrentMethod();
                GlobalExceptionHandler.customExceptionHandler(ex, objMethod, htParams, ErrorSeverityType.Normal);
            }
            return lstuserprofilefreeform;
        }
    }
}
