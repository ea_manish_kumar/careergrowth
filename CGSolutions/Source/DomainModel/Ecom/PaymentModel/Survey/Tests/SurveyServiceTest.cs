﻿/***************************************************************************************************
*** Module     : IBH.Services.Survey.UnitTests
*** Date       : 10/6/2009
*** Programmer : Subhasis Mittra (SM)
*** Description: Class for document subsystem services tests.
***               
*** Methods: 
***
*** History
*** Date       By  Comment
*** ---------  --- ----------------------------------------------------
*** 10/6/2009   SM  Created
***************************************************************************************************/
using IBH.Services.Survey;
using IBH.DomainModel.Survey;
using System.Collections.Generic;
using NUnit.Framework;
using IBH.Util.Test.Base;
using System.Linq;
using System;
using System.Diagnostics;
using IBH.Services.User;
using IBH.DomainModel.User;
using RandomGenerationFramework;
using IBH.DomainModel.Core;
using NHibernate;
using IBH.Data.Survey;
using IBH.Data.User;

namespace IBH.Services.Survey.UnitTests
{
    /// <summary>
    ///This is a test class for SurveyServiceTest and is intended
    ///to contain all SurveyServiceTest Unit Tests
    ///</summary>
    [TestFixture]
    public class SurveyServiceTest : TestBase
    {
        /// <summary>
        /// Tests the fixture set up.
        /// </summary>
        [TestFixtureSetUp]
        public void TestFixtureSetUp()
        {

        }

        /// <summary>
        /// Setups the context.
        /// </summary>
        [SetUp]
        public void SetupContext()
        {
        }

        /// <summary>
        ///A test for GetJobTitles
        ///</summary>
        [Test()]
        public void GetJobTitlesTest()
        {
            UserSurveyService target = new UserSurveyService();
            string Name = "Inc";
            IList<String> actual;
            actual = target.GetJobTitles(Name);
            Assert.NotNull(actual);
        }

        /// <summary>
        ///A test for GetSubIndustries
        ///</summary>
        [Test()]
        public void GetSubIndustriesTest()
        {
            UserSurveyService target = new UserSurveyService();
            int IndustryID = 263;
            IList<SurveyResponse> actual = null;
            actual = target.GetSubIndustries(IndustryID);
            foreach (SurveyResponse srChild in actual)
                Debug.WriteLine(srChild.Id);

            Assert.NotNull(actual);
        }

        [Test()]
        public void AddSurveyDataNormalTest()
        {
            SurveyDataNormal srv = new SurveyDataNormal();
            Person p = GetNewPersonInstance();
            SurveyQuestion q = GetRandomSurveyQuestionInstance(SurveyDataFactory.Session.GetISession());
            SurveyResponse srvyresp = GetRandomSurveyResponseInstance(SurveyDataFactory.Session.GetISession(), q.Id);

            UserDataFactory.PersonData.Save(p);
            
            UserSurveyService usrv = new UserSurveyService();
            usrv.AddSurveyDataNormal(srvyresp.Id, p.Id);

            SurveyDataNormal srvn = usrv.GetSurveyDataNormalByQuestionId(p.Id, q.Id);

            Assert.IsNotNull(srvn);
            Assert.AreEqual(srvn.PartyID, p.Id);
            Assert.AreEqual(srvn.SurveyResponse.Id, srvyresp.Id);
        }

        [Test()]
        public void AddSurveyDataFreeFormTest()
        {
            
            //int PartyID = 9871842;
            //int QuestionId = 125;
            //string surveyData = "Personnel Analyst";
            SurveyDataFreeForm srv = new SurveyDataFreeForm();
            Person P = GetNewPersonInstance();
            SurveyQuestion q = GetRandomFreeFormSurveyQuestionInstance(SurveyDataFactory.Session.GetISession());

            UserDataFactory.PersonData.Save(P);

            UserSurveyService usrv = new UserSurveyService();
            ListRandomGenerator<string> surveydatatype = new ListRandomGenerator<string>(new string[] { "Product Manager", "Project Manager", "Technical Manager" });
            string srvdatainsert = surveydatatype.GetRandom().ToString();
            srv = usrv.AddSurveyDataFreeForm(q.Id, P.Id, srvdatainsert);

            SurveyDataFreeForm srvn = usrv.GetSurveyDataFreeForm(P.Id, q.Id);

            Assert.IsNotNull(srvn);
            Assert.AreEqual(srvn.PartyID, P.Id);
            Assert.AreEqual(srvn.QuestionID, q.Id);
            Assert.AreEqual(srvn.Data, srvdatainsert);
           
        }

        [Test()]
        public void GetSurveyDataNormalByQuestionIdTest()
        {
            SurveyQuestion sq = new SurveyQuestion()
            {
                Id = 28

            };
            //IList<SurveyDataNormal> lst;
            SurveyDataFreeForm srv = new SurveyDataFreeForm();
           // int PartyID = 9871842;

            UserSurveyService usrv = new UserSurveyService();
            //lst = usrv.GetSurveyDataNormalByQuestionId(PartyID, sq);

            //Debug.WriteLine(lst[0].Id);


        }
        [Test()]
        public void GetSurveyDataFreeFormTest()
        {
            SurveyQuestion sq = new SurveyQuestion()
            {
                Id = 125

            };
           // IList<SurveyDataFreeForm> lst;
            SurveyDataFreeForm srv = new SurveyDataFreeForm();
            //int PartyID = 9871842;

            UserSurveyService usrv = new UserSurveyService();
           // lst = usrv.GetSurveyDataFreeForm(PartyID, sq);

            //Debug.WriteLine(lst[0].Id);


        }

        [Test()]
        public void getsurveybyquestionID()
        {
            UserSurveyService target = new UserSurveyService();
            IList<SurveyResponse> lstsurveyresponse=target.GetSurveyResponses(SurveyQuestionType.EducationLevel);
            
            foreach (SurveyResponse sr in lstsurveyresponse)
            {
                //Debug.WriteLine(sr.ToString());
                Debug.WriteLine(sr.Description);
            }
        }

        [Test()]
        public void GetSurveyDataByQuestionidPartyIDTest()
        {
            SurveyQuestion sq = new SurveyQuestion()
            {
                Id = 166

            };
            int PartyID = 9872969;
            UserSurveyService usrv = new UserSurveyService();
            //SurveyDataNormal surveydatanormal = usrv.GetSurveyDataNormalByQuestionId(PartyID, sq.Id);

//            Debug.WriteLine(surveydatanormal.ToString());
            SurveyResponse surveyresponse = usrv.GetSurveyResponseByPartyAndQuestionID(PartyID, sq.Id);
            Debug.WriteLine(surveyresponse.ToString());
        }

        #region Commented Code
        //[Test()]
        //public void TestsurevyDataNormalAndSurveyDataFreeForm()
        //{
        //    int isurveyId;
        //    IBH.DomainModel.Survey.Survey SessSurvey;
        //    PersonService pr = new PersonService();
        //    Person P = GetNewPersonInstance();
        //    IBHSessionService ibh = new IBHSessionService();

        //    IntegerRandomGenerator sessid = new IntegerRandomGenerator(8000, 9000);

        //    ISession session = IBH.Data.Survey.SurveyDataFactory.Session.GetISession();

        //    // SurveyResponse srvresponse = GetRandomSurveyResponseInstance(session);

        //    //SessSurvey = GetRandomSurveyInstance(session);

        //    //int itest = GetRandomSurveyResponseInstance(session).QuestionID;

        //    string sVisitTag = sessid.GetRandom().ToString();
        //    //UserDataFactory.PersonData.Save(P);

        //    Person oPersonfinal = null;

        //    oPersonfinal = pr.AddNewPerson(P, (int)ProductID.ResumeBuilder, true);
        //    string guid = ibh.CreateUserSession(oPersonfinal, (int)ProductID.ResumeBuilder, (int)PortalID.CDI, sVisitTag);

        //    SurveyDataNormal srvdtnormal = new SurveyDataNormal();


        //    UserSurveyService usrv = new UserSurveyService();
        //    // bool bflag = usrv.AddSurveyDataNormal((int)SurveyResponseID.Fashion, oPersonfinal.Id);
        //    //bool bflag = usrv.AddSurveyDataNormal(GetRandomSurveyResponseInstance(session).Id, oPersonfinal.Id);

        //    SurveyDataFreeForm srv = new SurveyDataFreeForm();

        //    //srv = usrv.AddSurveyDataFreeForm((int)SurveyQuestionType.JobTitle, oPersonfinal.Id,"Product Manager");
        //    srv = usrv.AddSurveyDataFreeForm(GetRandomFreeFormSurveyQuestionInstance(session).Id, oPersonfinal.Id, "Product Manager");

        //    SurveyQuestion sq = new SurveyQuestion()
        //    {
        //        Id = GetRandomSurveyQuestionInstance(session).Id

        //    };
        //    IList<SurveyDataNormal> lst;
        //    // lst = usrv.GetSurveyDataNormalByQuestionId(oPersonfinal.Id, sq);
        //    //if (lst.Count > 0)
        //    //{
        //    //    Debug.WriteLine(lst[0].Id);
        //    //}

        //    IList<SurveyDataFreeForm> lst1;
        //    SurveyQuestion sq1 = new SurveyQuestion()
        //    {
        //        Id = GetRandomFreeFormSurveyQuestionInstance(session).Id

        //    };
        //    lst1 = usrv.GetSurveyDataFreeForm(oPersonfinal.Id, sq1);
        //    //if (lst.Count > 0)
        //    //{
        //    //    Debug.WriteLine(lst1[0].Id);
        //    //}
        //}
        #endregion
    }


}
