﻿/***************************************************************************************************
*** Module     : IBH.Services.Survey.SurveyService
*** Date       : 10/6/2009
*** Programmer : Subhasis Mittra (SM)
*** Description: Class for SurveyService. 
***               
*** Methods: 
***
*** History
*** Date       By  Comment
*** ---------  --- ----------------------------------------------------
*** 10/6/2009   SM  Created
***************************************************************************************************/
using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using IBH.AppFramework.Exceptions;
using IBH.Services.Base;
using System.Reflection;
using IBH.DomainModel.Survey;
using IBH.Data.Survey;
using IBH.DomainModel.Content;
using IBH.AppFramework;
using IBH.Business.Logic.Survey;
using IBH.Data.Survey.DataTransfer;
using IBH.AppFramework.Caching;
using System.Text.RegularExpressions;
namespace IBH.Services.Survey
{
    /// <summary>
    /// Class for UserSurveyService
    /// </summary>
    /// <creation date="10/6/2009" author="SM"></creation>
    public class UserSurveyService : ServiceBase
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="UserSurveyService"/> class.
        /// </summary>
        public UserSurveyService()
            : base()
        {

        }


        /// <summary>
        /// Initializes a new instance of the <see cref="UserSurveyService"/> class.
        /// </summary>
        /// <param name="isUnAuthenticated">if set to <c>true</c> [is un authenticated].</param>
        public UserSurveyService(bool isUnAuthenticated)
            : base(isUnAuthenticated)
        {

        }

        /// <summary>
        /// Initializes a new instance of the <see cref="UserSurveyService"/> class.
        /// </summary>
        /// <param name="partyId">The party id.</param>
        public UserSurveyService(int partyId)
            : base(partyId)
        {

        }

        /// <summary>
        /// Gets the job titles.
        /// </summary>
        /// <param name="Name">The name.</param>
        /// <returns></returns>
        /// <creation date="10/6/2009" author="SM"></creation>
        //[IBH.AppFramework.WebORB.WebORBInvocationObserver]
        public IList<String> GetJobTitles(string Name)
        {
            IList<String> lstJobtitle = null;
            try
            {
                IList<SurveyResponse> lstSurveyResponse = SurveyDataFactory.SurveyResponseData.GetByQuestionAndText(SurveyQuestionType.JobTitle, Name);
                if (lstSurveyResponse != null && lstSurveyResponse.Count > 0)
                    // We can of course order the elements by the nHibernate criterion query : SM (10/6/2009) 
                    lstJobtitle = lstSurveyResponse
                        .OrderBy<SurveyResponse, String>((sr) => sr.Label)
                        .Select<SurveyResponse, String>((sr) => sr.Label)
                        //.OrderBy<SurveyResponse, String>((sr) => sr.Text.Description)
                        //.Select<SurveyResponse, String>((sr) => sr.Text.Description)
                                  .ToList<string>();
                //HD - lstJobtitle.ToList<string>();
            }
            catch (Exception ex)
            {
                Hashtable htParams = new Hashtable(1);
                htParams.Add("Name", Name);
                MethodInfo objMethod = (MethodInfo)MethodBase.GetCurrentMethod();
                GlobalExceptionHandler.customExceptionHandler(ex, objMethod, htParams, ErrorSeverityType.Normal);
            }
            return lstJobtitle;
        }

   

        /// <summary>
        /// Gets Sub-Indutries as per revised User Metadata structure/schema for I18N
        /// </summary>
        /// <param name="IndustryCD"></param>
        /// <returns></returns>
        public IList<Response> GetSubIndustriesI18N(string IndustryCD)
        {
            IList<Response> lstSubIndustries = null; string currentCulture = "en-US";//Default

            try
            {
                //NehaS.I18N.
                currentCulture = GetCultureFromSession();

                 IList<Response> subIndustries = SurveyDataFactory.ResponseData.GetChildResponse(IndustryCD, currentCulture);

                //Populate QuestionCD
                    foreach (Response res in subIndustries)
                    {
                        res.QuestionCD = IBH.DomainModel.Survey.Constant.USERQUESTION_SubIndustry;
                    }

                    if (ClientMode == IBHClientMode.FlexSTD)
                    {
                        lstSubIndustries = new List<Response>();

                        foreach (Response Res in subIndustries)
                        {
                            lstSubIndustries.Add(Res.GetClone(true, false));
                        }
                    }
               
            }
            catch (Exception ex)
            {
                Hashtable htParams = new Hashtable(1);
                htParams.Add("IndustryID", IndustryCD);
                MethodInfo objMethod = (MethodInfo)MethodBase.GetCurrentMethod();
                GlobalExceptionHandler.customExceptionHandler(ex, objMethod, htParams, ErrorSeverityType.Normal);
            }
            return lstSubIndustries;
        }


        /// <summary>
        /// Gets the sub industries.
        /// </summary>
        /// <param name="IndustryID">The industry ID.</param>
        /// <returns></returns>
        /// <creation date="10/7/2009" author="SM"></creation>
        //[IBH.AppFramework.WebORB.WebORBInvocationObserver]
        public IList<SurveyResponse> GetSubIndustries(int IndustryID)
        {
            IList<SurveyResponse> lstSubIndustries = null;
            
            try
            {
               
                SurveyResponse srParent = SurveyDataFactory.SurveyResponseData.GetById(IndustryID);

                //If its a Flex Call then perform custom cloning
                if (ClientMode == IBHClientMode.FlexSTD)
                {
                    //Create Clones to avoid WebORB Serialization issues
                    lstSubIndustries = new List<SurveyResponse>();
                    foreach (SurveyResponseRel Rel in srParent.Children)
                    {
                        lstSubIndustries.Add(Rel.ChildResponse.GetClone(true, false));
                    }
                }
                else
                {
                    lstSubIndustries = new List<SurveyResponse>();
                    foreach (SurveyResponseRel Rel in srParent.Children)
                    {
                        lstSubIndustries.Add(Rel.ChildResponse.GetClone(true, false));
                    }
                }
                lstSubIndustries = lstSubIndustries.OrderBy((SR) => SR.Index).ToList<SurveyResponse>();

                if (lstSubIndustries != null && lstSubIndustries.Count > 0)
                {
                    lstSubIndustries = lstSubIndustries.Where(x => x.QuestionID == (int)SurveyQuestionType.SubIndustry).ToList();
                }

            }
            catch (Exception ex)
            {
                Hashtable htParams = new Hashtable(1);
                htParams.Add("IndustryID", IndustryID);
                MethodInfo objMethod = (MethodInfo)MethodBase.GetCurrentMethod();
                GlobalExceptionHandler.customExceptionHandler(ex, objMethod, htParams, ErrorSeverityType.Normal);
            }
            return lstSubIndustries;
        }
        /// <summary>
        /// returns the data for the surveydatafreeform for the partid.
        /// </summary>
        /// <param name="PartyID"></param>
        /// <returns></returns>
        public IList<SurveyDataFreeForm> GetSurveyDataFreeFormByPartyID(int PartyID)
        {
            try
            {

                return SurveyDataFactory.SurveyDataFreeForm.GetSurveyDataFreeFormByPartyID(PartyID);
            }
            catch (Exception ex)
            {
                Hashtable htParams = new Hashtable(1);
                htParams.Add("PartyID", PartyID);
                MethodInfo objMethod = (MethodInfo)MethodBase.GetCurrentMethod();
                GlobalExceptionHandler.customExceptionHandler(ex, objMethod, htParams, ErrorSeverityType.Normal);
                return null;
            }
        }

        /// <summary>
        /// insert user's response to surveydatanormal table
        /// </summary>
        /// <param name="SurveyResponseId">SurveyResponseId</param>
        /// <param name="PartyId">PartyId</param>
        /// <returns></returns>
        public void AddSurveyDataNormal(int SurveyResponseId, int PartyId)
        {
            try
            {
                SurveyDataFactory.SurveyDataNormal.AddSurveyDataNormal(SurveyResponseId, PartyId);
            }
            catch (Exception ex)
            {
                Hashtable htParams = new Hashtable(2);
                htParams.Add("SurveyResponseId", SurveyResponseId);
                htParams.Add("PartyID", PartyID);
                MethodInfo objMethod = (MethodInfo)MethodBase.GetCurrentMethod();
                GlobalExceptionHandler.customExceptionHandler(ex, objMethod, htParams, ErrorSeverityType.Normal);
            }
        }


        /// <summary>
        /// Adds the survey data normal.
        /// </summary>
        /// <param name="SurveyResponseId">The survey response id.</param>
        /// <param name="PartyId">The party id.</param>
        /// <param name="UpdateExisting">if set to <c>true</c> [update existing].</param>
        public void AddSurveyDataNormal(int SurveyResponseId, int PartyId, bool UpdateExisting)
        {
            try
            {
                SurveyDataFactory.SurveyDataNormal.AddSurveyDataNormal(SurveyResponseId, PartyId, UpdateExisting);
            }
            catch (Exception ex)
            {
                Hashtable htParams = new Hashtable(2);
                htParams.Add("SurveyResponseId", SurveyResponseId);
                htParams.Add("PartyID", PartyID);
                MethodInfo objMethod = (MethodInfo)MethodBase.GetCurrentMethod();
                GlobalExceptionHandler.customExceptionHandler(ex, objMethod, htParams, ErrorSeverityType.Normal);
            }
        }

        public void AddSurveyDataNormal(string SurveyResponseId, int PartyId)
        {
            try
            {
                SurveyDataFactory.SurveyDataNormal.AddSurveyDataNormal(SurveyResponseId, PartyId);
            }
            catch (Exception ex)
            {
                Hashtable htParams = new Hashtable(2);
                htParams.Add("SurveyResponseId", SurveyResponseId);
                htParams.Add("PartyID", PartyID);
                MethodInfo objMethod = (MethodInfo)MethodBase.GetCurrentMethod();
                GlobalExceptionHandler.customExceptionHandler(ex, objMethod, htParams, ErrorSeverityType.Normal);
            }
        }

        /// <summary>
        /// Get Surveydata normal by question Id
        /// </summary>
        /// <param name="PartyID">The party ID.</param>
        /// <param name="srvQuestionId">SurveyQuestion</param>
        /// <returns></returns>
        public SurveyDataNormal GetSurveyDataNormalByQuestionId(int PartyID, int srvQuestionId)
        {
            SurveyDataNormal srvdatanormal = new SurveyDataNormal();

            try
            {
                srvdatanormal = SurveyDataFactory.SurveyDataNormal.GetSurveyDataNormalByQuestionId(PartyID, srvQuestionId);

            }
            catch (Exception ex)
            {
                Hashtable htParams = new Hashtable(2);
                htParams.Add("PartyID", PartyID);
                htParams.Add("srvQuestionId", srvQuestionId);
                MethodInfo objMethod = (MethodInfo)MethodBase.GetCurrentMethod();
                GlobalExceptionHandler.customExceptionHandler(ex, objMethod, htParams, ErrorSeverityType.Normal);
            }
            return srvdatanormal;
        }

        
        public PersonResponse GetPersonResponseByResponseGroup(int PartyID, string ResponseGroupCD, string CultureCD)
        {
            PersonResponse personResponse = new PersonResponse();

            try
            {
                personResponse = SurveyDataFactory.PersonResponseData.GetPersonResponseByResponseGroup(PartyID, ResponseGroupCD, CultureCD);

            }
            catch (Exception ex)
            {
                Hashtable htParams = new Hashtable(2);
                htParams.Add("PartyID", PartyID);
                htParams.Add("ResponseGroupCD", ResponseGroupCD);
                htParams.Add("CultureCD", CultureCD);
                MethodInfo objMethod = (MethodInfo)MethodBase.GetCurrentMethod();
                GlobalExceptionHandler.customExceptionHandler(ex, objMethod, htParams, ErrorSeverityType.Normal);
            }
            return personResponse;
        }



        /// <summary>
        /// Get Surveydata normal by questions
        /// </summary>
        /// <param name="PartyID"></param>
        /// <param name="Questions"></param>
        /// <returns></returns>
        public IList<SurveyDataNormal> GetSurveyDataNormal(int PartyID, int[] Questions)
        {
            SurveyDataNormal srvdatanormal = new SurveyDataNormal();
            IList<SurveyDataNormal> lstSDN = new List<SurveyDataNormal>();

            try
            {
                foreach (int QuestionID in Questions)
                {
                    srvdatanormal = SurveyDataFactory.SurveyDataNormal.GetSurveyDataNormalByQuestionId(PartyID, QuestionID);
                    if (srvdatanormal != null)
                        lstSDN.Add(srvdatanormal);
                }
            }
            catch (Exception ex)
            {
                Hashtable htParams = new Hashtable(2);
                htParams.Add("PartyID", PartyID);
                htParams.Add("Questions", Questions);
                MethodInfo objMethod = (MethodInfo)MethodBase.GetCurrentMethod();
                GlobalExceptionHandler.customExceptionHandler(ex, objMethod, htParams, ErrorSeverityType.Normal);
            }
            return lstSDN;
        }

        /// <summary>
        /// Get the value of surveyResponse using PartyId and QuestionID
        /// </summary>
        /// <param name="PartyID"></param>
        /// <param name="questionID"></param>
        /// <returns></returns>
        public SurveyResponse GetSurveyResponseByPartyAndQuestionID(int PartyID, int questionID)
        {
            SurveyResponse surveyresponse = new SurveyResponse();
            try
            {
                // Get the ResponseId from surveydatanormal Table
                SurveyDataNormal surveydatanormal = GetSurveyDataNormalByQuestionId(PartyID, questionID);
                // Get Response data and return it 
                // add a check for surveydatanormal is null 
                if (surveydatanormal != null)
                    surveyresponse = SurveyDataFactory.SurveyResponseData.GetById(surveydatanormal.SurveyResponse.Id);
            }
            catch (Exception ex)
            {
                Hashtable htParams = new Hashtable(2);
                htParams.Add("PartyID", PartyID);
                htParams.Add("questionID", questionID);
                MethodInfo objMethod = (MethodInfo)MethodBase.GetCurrentMethod();
                GlobalExceptionHandler.customExceptionHandler(ex, objMethod, htParams, ErrorSeverityType.Normal);
            }
            return surveyresponse;
        }

        /// <summary>
        /// Get the list of surveyResponse using PartyId and QuestionID
        /// </summary>
        /// <param name="PartyID"></param>
        /// <param name="questionID"></param>
        /// <returns></returns>
        public IList<SurveyResponse> GetSurveyResponseListByPartyAndQuestionID(int PartyID, int questionID)
        {
            IList<SurveyResponse> lstsurveyresponse = new List<SurveyResponse>();
            try
            {
                // Get the ResponseId from surveydatanormal Table
                IList<SurveyDataNormal> lstsurveydatanormal = SurveyDataFactory.SurveyDataNormal.GetSurveyDataNormalListByQuestionId(PartyID, questionID);
                // Get Response data and return it 
                // add a check for surveydatanormal is null 

                foreach (SurveyDataNormal sdn in lstsurveydatanormal)
                {
                    lstsurveyresponse.Add(SurveyDataFactory.SurveyResponseData.GetById(sdn.SurveyResponse.Id));
                }
            }
            catch (Exception ex)
            {
                Hashtable htParams = new Hashtable(2);
                htParams.Add("PartyID", PartyID);
                htParams.Add("questionID", questionID);
                MethodInfo objMethod = (MethodInfo)MethodBase.GetCurrentMethod();
                GlobalExceptionHandler.customExceptionHandler(ex, objMethod, htParams, ErrorSeverityType.Normal);
            }
            return lstsurveyresponse;
        }


        /// <summary>
        /// insert Survey Response Free form data to the Surveydatafreeform table
        /// </summary>
        /// <param name="QuestionID">QuestionID</param>
        /// <param name="PartyId">PartyId</param>
        /// <param name="surveydata">surveydata</param>
        /// <returns></returns>
        public SurveyDataFreeForm AddSurveyDataFreeForm(int QuestionID, int PartyId, string surveydata)
        {
            try
            {
                SurveyDataFreeForm survey = new SurveyDataFreeForm()
                {
                    PartyID = PartyId,
                    Data = surveydata,
                    QuestionID = QuestionID,
                    Timestamp = DateTime.Now

                };
                SurveyDataFactory.SurveyDataFreeForm.SaveOrUpdate(survey, true);
                return survey;
            }
            catch (Exception ex)
            {
                Hashtable htParams = new Hashtable(3);
                htParams.Add("QuestionID", QuestionID);
                htParams.Add("PartyID", PartyID);
                htParams.Add("surveydata", surveydata);
                MethodInfo objMethod = (MethodInfo)MethodBase.GetCurrentMethod();
                GlobalExceptionHandler.customExceptionHandler(ex, objMethod, htParams, ErrorSeverityType.Normal);
                return null;
            }
        }


        /// <summary>
        /// Get Survey DataFree form
        /// </summary>
        /// <param name="PartyID">PartyID</param>
        /// <param name="srvQuestionId">srvQuestionId</param>
        /// <returns></returns>
        public SurveyDataFreeForm GetSurveyDataFreeForm(int PartyID, int srvQuestionId)
        {
            SurveyDataFreeForm srvfreeform = new SurveyDataFreeForm();
            try
            {
                srvfreeform = SurveyDataFactory.SurveyDataFreeForm.GetSurveyDataFreeForm(PartyID, srvQuestionId);

            }
            catch (Exception ex)
            {
                Hashtable htParams = new Hashtable(2);
                htParams.Add("PartyID", PartyID);
                htParams.Add("srvQuestionId", srvQuestionId);
                MethodInfo objMethod = (MethodInfo)MethodBase.GetCurrentMethod();
                GlobalExceptionHandler.customExceptionHandler(ex, objMethod, htParams, ErrorSeverityType.Normal);
                return null;
            }
            return srvfreeform;
        }


        /// <summary>
        /// Gets all SurveyDataNormal by partyid and SurveyResponse
        /// </summary>
        /// <param name="PartyID">The party ID.</param>
        /// <param name="srvResp">The SurveyResponse.</param>
        /// <returns></returns>
        public SurveyDataNormal GetSurveyDataNormal(int PartyID, SurveyResponse srvResp)
        {
            SurveyDataNormal srvdatanormal = new SurveyDataNormal();

            try
            {
                srvdatanormal = SurveyDataFactory.SurveyDataNormal.GetSurveyDataNormal(PartyID, srvResp);

            }
            catch (Exception ex)
            {
                Hashtable htParams = new Hashtable(2);
                htParams.Add("PartyID", PartyID);
                htParams.Add("SurveyResponse", srvResp.ToString());
                MethodInfo objMethod = (MethodInfo)MethodBase.GetCurrentMethod();
                GlobalExceptionHandler.customExceptionHandler(ex, objMethod, htParams, ErrorSeverityType.Normal);
            }
            return srvdatanormal;
        }

        /// <summary>
        /// GetSurveyResponses
        /// </summary>
        /// <param name="QuestionType"></param>
        /// <returns></returns>
        public IList<SurveyResponse> GetSurveyResponses(SurveyQuestionType QuestionType)
        {
            IList<SurveyResponse> lstsurveyResponse = null;
            try
            {
                lstsurveyResponse = SurveyDataFactory.SurveyResponseData.GetSurveyResponses(QuestionType);
            }
            catch (Exception ex)
            {
                Hashtable htParams = new Hashtable(1);
                htParams.Add("QuestionType", QuestionType.ToString());
                MethodInfo objMethod = (MethodInfo)MethodBase.GetCurrentMethod();
                GlobalExceptionHandler.customExceptionHandler(ex, objMethod, htParams, ErrorSeverityType.Normal);
            }
            return lstsurveyResponse;
        }

        /// <summary>
        /// GetSurveyResponses
        /// </summary>
        /// <param name="QuestionID"></param>
        /// <returns></returns>
        public IList<SurveyResponse> GetSurveyResponses(int QuestionID)
        {
            IList<SurveyResponse> lstsurveyResponse = null;
            try
            {
                lstsurveyResponse = SurveyDataFactory.SurveyResponseData.GetSurveyResponses(QuestionID);
            }
            catch (Exception ex)
            {
                Hashtable htParams = new Hashtable(1);
                htParams.Add("QuestionID", QuestionID.ToString());
                MethodInfo objMethod = (MethodInfo)MethodBase.GetCurrentMethod();
                GlobalExceptionHandler.customExceptionHandler(ex, objMethod, htParams, ErrorSeverityType.Normal);
            }
            return lstsurveyResponse;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="PartyID"></param>
        /// <returns></returns>
        public string GetUserCareerLevl(int PartyID)
        {
            string sCareerLevel = string.Empty;
            SurveyDataNormal surveyDataNormal = null;
            try
            {
                surveyDataNormal = GetSurveyDataNormalByQuestionId(PartyID, Constant.CareerLevelQuestionID);
                if (surveyDataNormal != null)
                {
                    switch (surveyDataNormal.SurveyResponse.Id)
                    {
                        case (int)CareerLevelSurveyResponses.EntryLevel:
                            sCareerLevel = CareerLevel.EntryLevel;
                            break;
                        case (int)CareerLevelSurveyResponses.Student:
                            sCareerLevel = CareerLevel.Student;
                            break;
                        case (int)CareerLevelSurveyResponses.Experienced:
                            sCareerLevel = CareerLevel.Experienced;
                            break;
                        case (int)CareerLevelSurveyResponses.Manager:
                            sCareerLevel = CareerLevel.Manager;
                            break;
                        case (int)CareerLevelSurveyResponses.Executive:
                            sCareerLevel = CareerLevel.Executive;
                            break;
                        case (int)CareerLevelSurveyResponses.SeniorExecutive:
                            sCareerLevel = CareerLevel.SeniorExecutive;
                            break;
                        default:
                            sCareerLevel = "";
                            break;
                    }
                }
            }
            catch (Exception ex)
            {
                Hashtable htParams = new Hashtable(1);
                htParams.Add("PartyID", PartyID.ToString());
                MethodInfo objMethod = (MethodInfo)MethodBase.GetCurrentMethod();
                GlobalExceptionHandler.customExceptionHandler(ex, objMethod, htParams, ErrorSeverityType.Normal);
            }
            return sCareerLevel;
        }

        /// <summary>
        /// Gets the survey response for the Parent response Id.
        /// </summary>
        /// <param name="ParentResponseID">Parent response Id.</param>
        /// <returns></returns>
        /// <creation date="04/25/2012" author="Promothash Boruah"></creation>
        public IList<SurveyResponse> GetSurveyResponsesByParentID(int ParentResponseID)
        {

            IList<SurveyResponse> lstsurveyResponse = null;
            try
            {
                lstsurveyResponse = SurveyDataFactory.SurveyResponseData.GetSurveyResponsesByParentID(ParentResponseID);
            }
            catch (Exception ex)
            {
                Hashtable htParams = new Hashtable(1);
                htParams.Add("ResponseID", ParentResponseID.ToString());
                MethodInfo objMethod = (MethodInfo)MethodBase.GetCurrentMethod();
                GlobalExceptionHandler.customExceptionHandler(ex, objMethod, htParams, ErrorSeverityType.Normal);
            }
            return lstsurveyResponse;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="Name"></param>
        /// <param name="maxCount"></param>
        /// <returns></returns>
        public IList<SurveyResponse> GetSuggestedJobTitles(string Name, int maxCount)
        {
            IList<SurveyResponse> lstSurveyResponse = null;
            try
            {
                lstSurveyResponse = SurveyDataFactory.SurveyResponseData.GetByQuestionAndText(SurveyQuestionType.JobTitle, Name, maxCount);

            }
            catch (Exception ex)
            {
                Hashtable htParams = new Hashtable(1);
                htParams.Add("Name", Name);
                htParams.Add("maxCount", maxCount);
                MethodInfo objMethod = (MethodInfo)MethodBase.GetCurrentMethod();
                GlobalExceptionHandler.customExceptionHandler(ex, objMethod, htParams, ErrorSeverityType.Normal);
            }
            return lstSurveyResponse;
        }

        /// <summary>
        /// Get the Highest education Degree Achieved
        /// </summary>
        /// <param name="iBHClientMode"></param>
        /// <date>06-26-2012</date>
        /// <author>Promothash</author>
        /// <returns></returns>
        public IList<SurveyResponse> GetHEDA(IBHClientMode iBHClientMode)
        {
            IList<SurveyResponse> lstSurveyResponse = null;
            try
            {
                lstSurveyResponse = SurveyDataFactory.SurveyResponseData.GetSurveyResponses((int)IBH.DomainModel.Survey.SurveyQuestionType.HEDA);
                SurveyLogic surveyLogic = new SurveyLogic();
                //get the HEDA for the UI dropdown
                lstSurveyResponse = surveyLogic.GetHEDAForUI(lstSurveyResponse, iBHClientMode);
            }
            catch (Exception ex)
            {
                Hashtable htParams = new Hashtable();
                htParams.Add("SurveyResponse", lstSurveyResponse.ToString());
                MethodInfo objMethod = (MethodInfo)MethodBase.GetCurrentMethod();
                GlobalExceptionHandler.customExceptionHandler(ex, objMethod, htParams, ErrorSeverityType.High);
            }

            return lstSurveyResponse;
        }

        /// <summary>
        /// Get the Highest education Degree Achieved
        /// </summary>
        /// <param name="iBHClientMode"></param>
        /// <author>NehaS</author>
        /// <returns></returns>
        public IList<Response> GetHEDAI18N(IBHClientMode iBHClientMode)
        {
            IList<Response> lstEducationResponse = null;
            string currentCulture = "en-US";
            currentCulture = GetCultureFromSession();
            try
            {
                lstEducationResponse = SurveyDataFactory.ResponseData.GetResponses(Constant.USERQUESTION_EducationLevel, currentCulture,false);

                //Populate QuestionCD, it's not coming via Named Query
                foreach (Response res in lstEducationResponse)
                {
                    res.QuestionCD = Constant.USERQUESTION_EducationLevel;
                }
             
            }
            catch (Exception ex)
            {
                Hashtable htParams = new Hashtable();
                htParams.Add("Response", lstEducationResponse.ToString());
                MethodInfo objMethod = (MethodInfo)MethodBase.GetCurrentMethod();
                GlobalExceptionHandler.customExceptionHandler(ex, objMethod, htParams, ErrorSeverityType.High);
            }

            return lstEducationResponse;
        }


        /// <summary>
        /// 
        /// </summary>
        /// <param name="jobtitle"></param>
        /// <returns></returns>
        public IList<JobTitleDTO> GetSuggestedRBJobTitles(string jobtitle)
        {
            IList<JobTitleDTO> lstSurveyResponse = null;
            try
            {

                if (IBHCacheManager.IsInCache("JobTitleDTO"))
                {
                    lstSurveyResponse = (List<JobTitleDTO>)IBHCacheManager.GetFromCache("JobTitleDTO");
                    if (lstSurveyResponse == null || lstSurveyResponse.Count == 0)
                    {
                        lstSurveyResponse = SurveyDataFactory.SurveyResponseData.GetSuggestedRBJobTitles(jobtitle);
                    }
                }
                else
                {
                    lstSurveyResponse = SurveyDataFactory.SurveyResponseData.GetSuggestedRBJobTitles(jobtitle);
                }
                if (lstSurveyResponse != null && lstSurveyResponse.Count > 0)
                {
                    IBHCacheManager.SaveInCache("JobTitleDTO", lstSurveyResponse, 60, true);
                    //lstSurveyResponse = lstSurveyResponse.Where(p => p.JobTitle.ToUpper().StartsWith(jobtitle.ToUpper())).Take(12).ToList();
                   Regex regEx = new Regex(jobtitle, System.Text.RegularExpressions.RegexOptions.IgnoreCase);                
                   lstSurveyResponse = lstSurveyResponse.Where(item => regEx.IsMatch(item.JobTitle)).Take(12).ToList(); 
                }

                lstSurveyResponse = GetListCloneForFlex<JobTitleDTO, int>(lstSurveyResponse);

            }
            catch (Exception ex)
            {
                Hashtable htParams = new Hashtable(1);
                htParams.Add("Name", jobtitle);
                MethodInfo objMethod = (MethodInfo)MethodBase.GetCurrentMethod();
                GlobalExceptionHandler.customExceptionHandler(ex, objMethod, htParams, ErrorSeverityType.Normal);
            }
            return lstSurveyResponse;
        }

        /// <summary>
        /// Get Job Titles for auto suggest drop down for RB
        /// </summary>
        /// <param name="jobtitle"></param>
        /// <param name="partyId"></param>
        /// <returns></returns>
        public IList<JobTitleDTO> GetAutoSuggestedJobTitles(string jobtitle, int partyId)
        {
            SurveyDataNormal SurveyDataNormal165 = null;
            SurveyDataNormal SurveyDataNormal166 = null;
            IList<JobTitleDTO> lstSurveyResponse = null;
            try
            {

                if (IBHCacheManager.IsInCache("JobTitleDTO"))
                {
                    lstSurveyResponse = (List<JobTitleDTO>)IBHCacheManager.GetFromCache("JobTitleDTO");
                    SurveyDataNormal166 = GetSurveyDataNormalByQuestionId(partyId, (int)SurveyQuestionType.SubIndustry);
                    SurveyDataNormal165 = GetSurveyDataNormalByQuestionId(partyId, (int)SurveyQuestionType.Industry);

                    if (lstSurveyResponse == null || lstSurveyResponse.Count == 0)
                    {
                        lstSurveyResponse = SurveyDataFactory.SurveyResponseData.GetAutoSuggestedJobTitles(partyId);
                    }
                    else if (lstSurveyResponse != null && lstSurveyResponse.Count > 0)
                    {
                        if (SurveyDataNormal166 != null && SurveyDataNormal166.SurveyResponse != null)
                        {
                            if (SurveyDataNormal166.SurveyResponse.Id != lstSurveyResponse[0].Id)
                            {
                            lstSurveyResponse = SurveyDataFactory.SurveyResponseData.GetAutoSuggestedJobTitles(partyId);
                        }
                        }

                        else if (SurveyDataNormal165 != null && SurveyDataNormal165.SurveyResponse != null)
                        {
                            if (SurveyDataNormal165 != null && SurveyDataNormal165.SurveyResponse.Id != lstSurveyResponse[0].Id)
                            {
                            lstSurveyResponse = SurveyDataFactory.SurveyResponseData.GetAutoSuggestedJobTitles(partyId);
                        }
                    }
                }
                }
                else
                {
                    lstSurveyResponse = SurveyDataFactory.SurveyResponseData.GetAutoSuggestedJobTitles(partyId);
                }

                if (lstSurveyResponse != null && lstSurveyResponse.Count > 0)
                {
                    IBHCacheManager.SaveInCache("JobTitleDTO", lstSurveyResponse, 60, true);
                    //lstSurveyResponse = lstSurveyResponse.Where(p => p.JobTitle.ToUpper().StartsWith(jobtitle.ToUpper())).Take(12).ToList();
                    Regex regEx = new Regex(jobtitle, System.Text.RegularExpressions.RegexOptions.IgnoreCase);
                    
                    lstSurveyResponse = lstSurveyResponse.Where(item => regEx.IsMatch(item.JobTitle)).Take(12).ToList();
                }

                lstSurveyResponse = GetListCloneForFlex<JobTitleDTO, int>(lstSurveyResponse);


            }
            catch (Exception ex)
            {
                Hashtable htParams = new Hashtable(1);
                htParams.Add("Name", jobtitle);
                MethodInfo objMethod = (MethodInfo)MethodBase.GetCurrentMethod();
                GlobalExceptionHandler.customExceptionHandler(ex, objMethod, htParams, ErrorSeverityType.Normal);
            }
            return lstSurveyResponse;
        }

        //public void SyncResponsePreference(Response Response)
        //{
        //    Response res  = SurveyDataFactory.ResponseData.GetById(Response.Id);
        //    if (res != null && res.Id != string.Empty)
        //    {
        //        Response.Id = res.Id;
        //        Response.ResponseGroupCD = res.ResponseGroupCD;
        //        Response.QuestionCD = res.QuestionCD;
        //        Response.Definition = res.Definition;
        //        Response.SortIndex = res.SortIndex;
        //        Response.Children = SurveyDataFactory.ResponseData.GetById(Response.Id);
        //    }
        //}

    }
}
