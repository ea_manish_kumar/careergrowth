using System.ComponentModel.DataAnnotations;
using Newtonsoft.Json;

namespace DomainModel.Ecom
{
    public class PaymentItem
    {
        [JsonProperty(PropertyName = "id")] public int PaymentItemID { get; set; }

        [JsonProperty(PropertyName = "sku_id")]
        public string ClientSKUID { get; set; }

        [StringLength(1024)]
        [JsonProperty(PropertyName = "descriptor")]
        public string Descriptor { get; set; }

        [Required]
        [JsonProperty(PropertyName = "price")]
        public decimal Price { get; set; }

        [Required]
        [JsonProperty(PropertyName = "quantity")]
        public byte Qty { get; set; }

        [Required]
        [JsonProperty(PropertyName = "amount")]
        public decimal Amount { get; set; }

        [JsonProperty(PropertyName = "status")]
        public string Status { get; set; }

        public string item_type_cd { get; set; }
    }
}