﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using Newtonsoft.Json;

namespace DomainModel.Ecom
{
    public class PaymentItemWithTransactions
    {
        [JsonProperty(PropertyName = "id")] public int PaymentItemID { get; set; }

        [JsonIgnore] public int PaymentID { get; set; }

        [JsonProperty(PropertyName = "sku_id")]
        public string ClientSKUID { get; set; }

        [StringLength(1024)]
        [JsonProperty(PropertyName = "descriptor")]
        public string Descriptor { get; set; }

        [Required]
        [JsonProperty(PropertyName = "price")]
        public decimal Price { get; set; }

        [Required]
        [JsonProperty(PropertyName = "quantity")]
        public byte Qty { get; set; }

        [Required]
        [JsonProperty(PropertyName = "amount")]
        public decimal Amount { get; set; }

        [StringLength(4)]
        [JsonProperty(PropertyName = "status")]
        public string Status { get; set; }

        [JsonProperty(PropertyName = "transactions")]
        public IList<TransactionOnly> Transaction { get; set; }

        [JsonProperty(PropertyName = "item_type_cd")]
        public string PaymentItemTypeCD { get; set; }
    }
}