﻿using System.Collections.Generic;

namespace DomainModel.Ecom
{
    public class PaymentRequest
    {
        public string email { get; set; }
        public IList<PaymentItem> payment { get; set; }
        public CreditCard credit_card { get; set; }
        public decimal amount { get; set; }
        public string currency { get; set; }
        public string cuid { get; set; }
        public string ip_address { get; set; }
    }
}