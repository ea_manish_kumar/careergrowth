using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace DomainModel.Ecom
{
    public class Payment
    {
        private DateTime? _createdOn;

        public Payment()
        {
            PaymentItem = new List<PaymentItem>();
        }

        public int PaymentID { get; set; }
        public int CustomerID { get; set; }
        public ClientRequest ClientRequest { get; set; }

        [StringLength(4)] public string PaymentStatusCD { get; set; }

        [Required] public decimal Amount { get; set; }

        [Required] [StringLength(3)] public string CurrencyCD { get; set; }

        [Required]
        public DateTime CreatedOn
        {
            get =>
                _createdOn.HasValue
                    ? _createdOn.Value
                    : DateTime.Now;
            set => _createdOn = value;
        }

        public IList<PaymentItem> PaymentItem { get; set; }

        public string IPAddress { get; set; }
    }
}