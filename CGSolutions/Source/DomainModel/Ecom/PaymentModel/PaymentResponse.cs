﻿using System.Collections.Generic;
using System.Text;

namespace DomainModel.Ecom
{
    public class PaymentResponse
    {
        public int id { get; set; }
        public IList<PaymentItem> payment { get; set; }
        public Customer customer { get; set; }
        public IList<Transaction> transaction { get; set; }
        public string status { get; set; }
        public string mode { get; set; }
        public string fail_code { get; set; }
        public string fail_message { get; set; }

        public new virtual string ToString()
        {
            var sb = new StringBuilder(10);

            sb.AppendLine("------------Payment Response dump starts---------------");
            sb.AppendLine("id: " + id);
            sb.AppendLine("customer: " + (customer != null ? customer.ToString() : "<NULL>"));
            sb.AppendLine("status: " + (status != null ? status : "<NULL>"));
            sb.AppendLine("mode: " + (mode != null ? mode : "<NULL>"));
            sb.AppendLine("fail_code: " + (fail_code != null ? fail_code : "<NULL>"));
            sb.AppendLine("fail_message: " + (fail_message != null ? fail_message : "<NULL>"));
            sb.AppendLine("------------PaymentResponse dump ends---------------");
            return sb.ToString();
        }
    }
}