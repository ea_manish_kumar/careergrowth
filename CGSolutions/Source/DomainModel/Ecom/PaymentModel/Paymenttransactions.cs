namespace DomainModel.Ecom
{
    public class PaymentItemTransactions
    {
        public virtual int PaymentID { get; set; }
        public virtual int TransactionID { get; set; }
        public virtual PaymentItem PaymentItem { get; set; }

        #region NHibernate Composite Key Requirements

        public override bool Equals(object obj)
        {
            if (obj == null) return false;
            var t = obj as PaymentItemTransactions;
            if (t == null) return false;
            if (PaymentID == t.PaymentID
                && TransactionID == t.TransactionID)
                return true;

            return false;
        }

        public override int GetHashCode()
        {
            var hash = GetType().GetHashCode();
            hash = (hash * 397) ^ PaymentID.GetHashCode();
            hash = (hash * 397) ^ TransactionID.GetHashCode();

            return hash;
        }

        #endregion
    }
}