using System;
using System.ComponentModel.DataAnnotations;

namespace DomainModel.Ecom
{
    public class ClientCreditCardType
    {
        public virtual string ClientCD { get; set; }
        public virtual string CardTypeCD { get; set; }
        public virtual Client Client { get; set; }
        public virtual byte? SortIndex { get; set; }

        [Required] public virtual bool IsActive { get; set; }

        [Required] public virtual DateTime CreatedOn { get; set; }

        #region NHibernate Composite Key Requirements

        public override bool Equals(object obj)
        {
            if (obj == null) return false;
            var t = obj as ClientCreditCardType;
            if (t == null) return false;
            if (ClientCD == t.ClientCD
                && CardTypeCD == t.CardTypeCD)
                return true;

            return false;
        }

        public override int GetHashCode()
        {
            var hash = GetType().GetHashCode();
            hash = (hash * 397) ^ ClientCD.GetHashCode();
            hash = (hash * 397) ^ CardTypeCD.GetHashCode();

            return hash;
        }

        #endregion
    }
}