using System;
using System.ComponentModel.DataAnnotations;

namespace DomainModel.Ecom
{
    public class ClientRequest
    {
        public virtual int Id { get; set; }
        public virtual string ClientCD { get; set; }
        public virtual string RequestData { get; set; }

        [Required] public virtual DateTime CreatedOn { get; set; }

        [StringLength(100)] public virtual string ClientRequestReference { get; set; }
    }
}