using System;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace DomainModel.Ecom
{
    public class Client
    {
        [Key] public string ClientCD { get; set; }

        [Required] [StringLength(50)] public string Name { get; set; }

        [Required] public DateTime CreatedOn { get; set; }

        [Required] public DateTime ModifiedOn { get; set; }

        public new virtual string ToString()
        {
            var sb = new StringBuilder(10);

            sb.AppendLine("------------Customer dump starts---------------");
            sb.AppendLine("Name: " + Name ?? "<NULL>");
            sb.AppendLine("ClientCD: " + (ClientCD ?? "<NULL>"));
            sb.AppendLine("CreatedOn: " + CreatedOn);
            sb.AppendLine("ModifiedOn: " + ModifiedOn);
            sb.AppendLine("------------Customer dump ends---------------");

            return sb.ToString();
        }
    }
}