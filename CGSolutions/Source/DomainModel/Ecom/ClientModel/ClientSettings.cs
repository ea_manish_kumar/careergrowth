﻿using System.Collections.Generic;

namespace DomainModel.Ecom
{
    public class ClientSettings
    {
        public List<Settings> settings { get; set; }
    }

    public class Settings
    {
        public string Key { get; set; }
        public string Value { get; set; }
    }
}