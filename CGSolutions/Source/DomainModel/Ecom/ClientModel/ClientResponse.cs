using System;
using System.ComponentModel.DataAnnotations;

namespace DomainModel.Ecom
{
    public class ClientResponse
    {
        public virtual int Id { get; set; }
        public virtual string ResponseData { get; set; }

        [Required] public virtual DateTime CreatedOn { get; set; }
    }
}