﻿namespace DomainModel.Ecom
{
    public class ClientAdmin
    {
        public virtual int Id { get; set; }
        public virtual string ClientCD { get; set; }
        public virtual string AdminCD { get; set; }
    }
}