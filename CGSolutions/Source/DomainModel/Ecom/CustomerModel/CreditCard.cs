using System;
using System.ComponentModel.DataAnnotations;
using System.Text;
using Newtonsoft.Json;

namespace DomainModel.Ecom
{
    public class CreditCard
    {
        private DateTime? _createdOn;
        private DateTime? _modifiedOn;

        [JsonProperty(PropertyName = "id")] public int CreditCardID { get; set; }

        public int CustomerID { get; set; }

        [Required]
        [StringLength(150)]
        [JsonProperty(PropertyName = "cardholder_name")]
        public string CardHolderName { get; set; }

        [Required]
        [StringLength(4)]
        [JsonProperty(PropertyName = "type")]
        public string CardTypeCD { get; set; }

        [Required]
        [StringLength(20)]
        [JsonProperty(PropertyName = "number")]
        public string CardNum { get; set; }

        [Required]
        [JsonProperty(PropertyName = "expire_year")]
        public short ExpiryYear { get; set; }

        [Required]
        [JsonProperty(PropertyName = "expire_month")]
        public byte ExpiryMonth { get; set; }

        [JsonProperty(PropertyName = "cvv")] public short CVV { get; set; }

        [StringLength(10)]
        [JsonProperty(PropertyName = "bin")]
        public string BIN { get; set; }

        [JsonProperty(PropertyName = "created_on")]
        public DateTime CreatedOn
        {
            get =>
                _createdOn.HasValue
                    ? _createdOn.Value
                    : DateTime.Now;
            set => _createdOn = value;
        }

        public DateTime ModifiedOn
        {
            get =>
                _modifiedOn.HasValue
                    ? _modifiedOn.Value
                    : DateTime.Now;
            set => _modifiedOn = value;
        }


        public new virtual string ToString()
        {
            var sb = new StringBuilder();

            sb.AppendLine("------------CreditCard dump starts---------------");
            sb.AppendLine("CreditCardID: " + CreditCardID);
            sb.AppendLine("CustomerID: " + CustomerID);
            sb.AppendLine("CardHolderName: " + (CardHolderName ?? "<NULL>"));
            sb.AppendLine("CardTypeCD: " + (CardTypeCD ?? "<NULL>"));
            sb.AppendLine("CardNum: " + (CardNum ?? "<NULL>"));
            sb.AppendLine("ExpiryYear: " + ExpiryYear);
            sb.AppendLine("ExpiryMonth: " + ExpiryMonth);
            sb.AppendLine("CVV: " + CVV);
            sb.AppendLine("BIN: " + (BIN ?? "<NULL>"));
            sb.AppendLine("CreatedOn: " + CreatedOn);
            sb.AppendLine("ModifiedOn: " + ModifiedOn);
            sb.AppendLine("------------CreditCard dump ends---------------");

            return sb.ToString();
        }
    }
}