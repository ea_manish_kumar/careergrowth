﻿using System.Text;

namespace DomainModel.Ecom
{
    public class CustomerRequest
    {
        public string email { get; set; }
        public string cuid { get; set; }

        public new virtual string ToString()
        {
            var sb = new StringBuilder(10);

            sb.AppendLine("------------CustomerRequest dump starts---------------");
            sb.AppendLine("email: " + (email ?? "<NULL>"));
            sb.AppendLine("cuid: " + (cuid ?? "<NULL>"));
            sb.AppendLine("------------CustomerRequest dump ends---------------");

            return sb.ToString();
        }
    }
}