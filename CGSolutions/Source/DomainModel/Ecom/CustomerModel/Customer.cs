using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;
using Newtonsoft.Json;

namespace DomainModel.Ecom
{
    public class Customer
    {
        [JsonProperty(PropertyName = "id")] public int CustomerID { get; set; }

        [JsonIgnore] public string ClientCD { get; set; }

        [JsonIgnore] public Client Client { get; set; }

        [Required]
        [StringLength(256)]
        [JsonProperty(PropertyName = "email")]
        public string Emailaddress { get; set; }

        [Required]
        [JsonProperty(PropertyName = "primary_card_id")]
        public int PrimaryCreditCardID { get; set; }

        [Required]
        [JsonProperty(PropertyName = "created_on")]
        public DateTime CreatedOn { get; set; }

        [Required]
        [JsonProperty(PropertyName = "modified_on")]
        public DateTime ModifiedOn { get; set; }

        public IList<CreditCard> CreditCard { get; set; }

        public new virtual string ToString()
        {
            var sb = new StringBuilder(10);

            sb.AppendLine("------------Customer dump starts---------------");
            sb.AppendLine("CustomerID: " + CustomerID);
            sb.AppendLine("ClientCD: " + (ClientCD ?? "<NULL>"));
            sb.AppendLine("Client: " + (Client != null ? Client.ToString() : "<NULL>"));
            sb.AppendLine("Emailaddress: " + (Emailaddress ?? "<NULL>"));
            sb.AppendLine("PrimaryCreditCardID: " + PrimaryCreditCardID);
            sb.AppendLine("CreatedOn: " + CreatedOn);
            sb.AppendLine("ModifiedOn: " + ModifiedOn);
            sb.AppendLine("------------Customer dump ends---------------");

            return sb.ToString();
        }
    }
}