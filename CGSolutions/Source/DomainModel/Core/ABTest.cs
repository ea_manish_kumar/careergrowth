using System;
using System.Collections.Generic;
using System.Text;

namespace DomainModel.Tests
{
    [Serializable]
    public class ABTest : EntityBase<int, ABTest>
    {
        #region Constructors

        /// <summary>
        ///     Class constructor...
        /// </summary>
        public ABTest()
        {
            //TODO:Write constructor stuff here..
            _ABtestcases = new List<ABTestCase>();
        }

        #endregion


        #region ToString() Implementation

        /// <summary>
        ///     This methods returns a string representation of this class.
        /// </summary>
        /// <returns>A string representation of this class.</returns>
        public new virtual string ToString()
        {
            var sb = new StringBuilder(6);

            sb.AppendLine("------------ABTest dump starts---------------");
            sb.AppendLine("ABTestID: " + Id);
            sb.AppendLine("Name: " + (Name != null ? Name : "<NULL>"));
            sb.AppendLine("Description: " + (Description != null ? Description : "<NULL>"));
            sb.AppendLine("IsActive: " + (IsActive == false ? IsActive.ToString() : "true"));
            sb.AppendLine("StartDate: " + (StartDate != null ? StartDate.ToString() : "<NULL>"));
            sb.AppendLine("------------ABTest dump ends---------------");

            return sb.ToString();
        }

        #endregion ToString() Implementation

        #region GetClone() Implementation

        /// <summary>
        ///     This methods returns a cloned instance of this class.
        /// </summary>
        /// <returns>A string cloned instance of this class.</returns>
        public virtual ABTest GetClone()
        {
            var clone = new ABTest();

            clone.Id = Id;
            clone.Name = Name;
            clone.Description = Description;
            clone.IsActive = IsActive;
            clone.StartDate = StartDate;

            foreach (var objABTestCase in ABTestCases)
            {
                var CloneABTestCase = objABTestCase.GetClone();
                CloneABTestCase.ParentABTest = clone;
                clone.ABTestCases.Add(CloneABTestCase);
            }

            foreach (var objABTestData in ABTestDatas)
            {
                var CloneABTestData = objABTestData.GetClone();
                CloneABTestData.ParentABTest = clone;
                clone.ABTestDatas.Add(CloneABTestData);
            }

            return clone;
        }

        #endregion GetClone() Implementation

        #region GetHashCode() implementation.

        /// <summary>
        ///     Serves as a hash function for a particular type.
        /// </summary>
        /// <returns>
        ///     A hash code for the current ABTest.
        /// </returns>
        public override int GetHashCode()
        {
            return (GetType().FullName + Id).GetHashCode();
        }

        #endregion GetHashCode() Implementation

        #region Equals() implementation.

        /// <summary>
        ///     Determines whether the specified ABTest is equal to the current ABTest.
        /// </summary>
        /// <param name="obj">The ABTest to compare with the current ABTest.</param>
        /// <returns>
        ///     true if the specified ABTest is equal to the current ABTest; otherwise, false.
        /// </returns>
        /// <remarks>
        ///     Tests whether this and another object are equal in a way that will still pass
        ///     when proxy objects are being used.
        /// </remarks>
        public override bool Equals(object obj)
        {
            //Same reference check !
            if (ReferenceEquals(this, obj)) return true;

            //Type check !
            var other = obj as ABTest;
            if (other == null) return false;

            if (Id == 0)
                //Do a per-property search
                return
                    Name == other.Name &&
                    Description == other.Description &&
                    IsActive == other.IsActive &&
                    StartDate == other.StartDate &&
                    ABTestCases.Count == other.ABTestCases.Count &&
                    ABTestDatas.Count == other.ABTestDatas.Count;
            return Id == other.Id;
        }

        #endregion Equals() Implementation

        #region Property Declarations

        //private int _Id;

        ///// <summary>
        ///// Id of ABTest.
        ///// </summary>
        //public override int Id
        //{
        //    get
        //    {
        //        return this._Id;
        //    }
        //    set
        //    {
        //        this._Id = value;
        //    }
        //}

        /// <summary>
        ///     Private variable to hold the value for property Name.
        /// </summary>
        private string _name;

        /// <summary>
        ///     Name of ABTest.
        /// </summary>
        public virtual string Name
        {
            get => _name;
            set => _name = value;
        }

        /// <summary>
        ///     Private variable to hold the value for property Description.
        /// </summary>
        private string _description;

        /// <summary>
        ///     Decription Of ABTest.
        /// </summary>
        public virtual string Description
        {
            get => _description;
            set => _description = value;
        }

        /// <summary>
        ///     Private variable to hold the value for property IsActive.
        /// </summary>
        private bool _isActive;

        /// <summary>
        ///     is Test Active.
        /// </summary>
        public virtual bool IsActive
        {
            get => _isActive;
            set => _isActive = value;
        }

        /// <summary>
        ///     Private variable to hold the value for property StartDate.
        /// </summary>
        private DateTime? _startDate;

        /// <summary>
        ///     ABTest Start Date
        /// </summary>
        public virtual DateTime? StartDate
        {
            get => _startDate;
            set => _startDate = value;
        }


        private IList<ABTestCase> _ABtestcases;

        /// <summary>
        ///     ABTestCases collection
        /// </summary>
        public virtual IList<ABTestCase> ABTestCases
        {
            get => _ABtestcases;
            set => _ABtestcases = value;
        }

        private IList<ABTestData> _ABtestdatas;

        /// <summary>
        ///     Abtest DataCollections
        /// </summary>
        public virtual IList<ABTestData> ABTestDatas
        {
            get => _ABtestdatas;
            set => _ABtestdatas = value;
        }

        #endregion Property Declarations

        #region ABTestCases

        /// <summary>
        ///     Removes a ABtestcase at the given index
        /// </summary>
        /// <param name="ABtestcaseIndex">The index of the ABtestcase to remove</param>
        public virtual void RemoveABTestCase(int ABtestcaseIndex)
        {
            if (ABTestCases.Count < ABtestcaseIndex)
                throw new IndexOutOfRangeException("The provided index is higher than collection count");

            var ABtestcase = ABTestCases[ABtestcaseIndex];
            RemoveABTestCase(ABtestcase);
        }

        /// <summary>
        ///     Removes a given ABtestcase from the ABTestCases collection.
        /// </summary>
        /// <param name="ABtestcase">The ABtestcase to remove</param>
        public virtual void RemoveABTestCase(ABTestCase ABtestcase)
        {
            if (!ABTestCases.Contains(ABtestcase))
                throw new ArgumentNullException("The provided ABTestCase does not exist in the collection");

            ABTestCases.Remove(ABtestcase);
            ABtestcase.ParentABTest = null;
        }

        /// <summary>
        ///     Adds a given ABtestcase to the Document.ABTestCases collection.
        /// </summary>
        /// <param name="ABtestcase"></param>
        public virtual void AddABTestCase(ABTestCase ABtestcase)
        {
            ABtestcase.ParentABTest = this;
            ABTestCases.Add(ABtestcase);
        }

        #endregion


        #region ABTestData

        /// <summary>
        ///     Removes a ABtestData at the given index
        /// </summary>
        /// <param name="ABtestdataIndex">The index of the ABtestData to remove</param>
        public virtual void RemoveABTestData(int ABtestdataIndex)
        {
            if (ABTestDatas.Count < ABtestdataIndex)
                throw new IndexOutOfRangeException("The provided index is higher than collection count");

            var ABtestData = ABTestDatas[ABtestdataIndex];
            RemoveABTestData(ABtestData);
        }

        /// <summary>
        ///     Removes a given ABtestData from the ABTestDatas collection.
        /// </summary>
        /// <param name="ABtestData">The ABtestData to remove</param>
        public virtual void RemoveABTestData(ABTestData ABtestData)
        {
            if (!ABTestDatas.Contains(ABtestData))
                throw new ArgumentNullException("The provided ABTestData does not exist in the collection");

            ABTestDatas.Remove(ABtestData);
            ABtestData.ParentABTest = null;
        }

        /// <summary>
        ///     Adds a given ABtestData to the Document.ABTestDatas collection.
        /// </summary>
        /// <param name="ABtestData"></param>
        public virtual void AddABTestData(ABTestData ABtestData)
        {
            ABtestData.ParentABTest = this;
            ABTestDatas.Add(ABtestData);
        }

        #endregion
    }
}