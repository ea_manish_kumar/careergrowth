﻿using System;
using System.Text;

namespace DomainModel.Core
{
    [Serializable]
    public class Culture : EntityBase<int, Culture>
    {
        #region Constructors

        #endregion

        #region property declarations

        public virtual string Label { get; set; }
        public virtual string LanguageCD { get; set; }
        public virtual string CountryCD { get; set; }
        public virtual string CultureCD { get; set; }

        #endregion

        #region Methods

        //#region Equals() implementation.
        //public override bool Equals(object obj)
        //{
        //    //Same reference check !
        //    if (object.ReferenceEquals(this, obj))
        //        return true;

        //    //Type check !
        //    Culture other = obj as Culture;
        //    if (other == null) return false;

        //    if (Id == 0)
        //        return
        //            (Label == other.Label) &&
        //            (LanguageCD == other.LanguageCD) &&
        //            (CountryCD == other.CountryCD) &&
        //            (CountryCD == other.CountryCD);
        //    else
        //        return Id == other.Id;

        //}
        //#endregion

        #region ToString() Implementation

        public new virtual string ToString()
        {
            var sb = new StringBuilder(11);
            sb.AppendLine("------------Culture dump starts---------------");
            sb.AppendLine("CultureID: " + Id);
            sb.AppendLine("CultureCD: " + (CultureCD != null ? CultureCD : "<NULL>"));
            sb.AppendLine("Label: " + (Label != null ? Label : "<NULL>"));
            sb.AppendLine("LanguageCD: " + (LanguageCD != null ? LanguageCD : "<NULL>"));
            sb.AppendLine("CountryCD: " + (CountryCD != null ? CountryCD : "<NULL>"));
            sb.AppendLine("------------Culture dump ends---------------");

            return sb.ToString();
        }

        #endregion


        #region GetClone() Implementation

        public virtual Culture GetClone()
        {
            var clone = new Culture();

            clone.Id = Id;
            clone.CultureCD = CultureCD;
            clone.Label = Label;
            clone.LanguageCD = LanguageCD;
            clone.CountryCD = CountryCD;
            return clone;
        }

        #endregion

        #region GetHashCode() implementation.

        /// <summary>
        ///     Serves as a hash function for a particular type.
        /// </summary>
        /// <returns>A hash code for the current Culture.</returns>
        public override int GetHashCode()
        {
            return (GetType().FullName + Id).GetHashCode();
        }

        #endregion GetHashCode() Implementation

        #endregion
    }
}