﻿using System;
using System.Text;

namespace DomainModel.Core
{
    [Serializable]
    public class SessionData : EntityBase<Guid, SessionData>
    {
        #region ToString() Implementation

        /// <summary>
        ///     This methods returns a string representation of this class.
        /// </summary>
        /// <returns>A string representation of this class.</returns>
        public new virtual string ToString()
        {
            var sb = new StringBuilder(5);

            sb.AppendLine("------------SessionData dump starts---------------");
            sb.AppendLine("Id: " + Id);
            sb.AppendLine("DataTypeCD: " + (DataTypeCD != null ? DataTypeCD : "<NULL>"));
            sb.AppendLine("Data: " + (Data != null ? Data : "<NULL>"));
            sb.AppendLine("------------SessionData dump ends---------------");
            sb.AppendLine("Session: " + (SessionID != null ? SessionID.ToString() : "<NULL>"));

            return sb.ToString();
        }

        #endregion ToString() Implementation

        #region GetClone() Implementation

        /// <summary>
        ///     This methods returns a cloned instance of this class.
        /// </summary>
        /// <returns>A string cloned instance of this class.</returns>
        public virtual SessionData GetClone()
        {
            var clone = new SessionData();

            clone.Id = Id;
            clone.DataTypeCD = DataTypeCD;
            clone.Data = Data;
            clone.SessionID = SessionID;
            return clone;
        }

        #endregion GetClone() Implementation

        #region GetHashCode() implementation.

        /// <summary>
        ///     Serves as a hash function for a particular type.
        /// </summary>
        /// <returns>
        ///     A hash code for the current SessionData.
        /// </returns>
        public override int GetHashCode()
        {
            return (GetType().FullName + Id + DataTypeCD).GetHashCode();
        }

        #endregion GetHashCode() Implementation

        #region Equals() implementation.

        /// <summary>
        ///     Determines whether the specified SessionData is equal to the current SessionData.
        /// </summary>
        /// <param name="obj">The SessionData to compare with the current SessionData.</param>
        /// <returns>
        ///     true if the specified SessionData is equal to the current SessionData; otherwise, false.
        /// </returns>
        /// <remarks>
        ///     Tests whether this and another object are equal in a way that will still pass
        ///     when proxy objects are being used.
        /// </remarks>
        public override bool Equals(object obj)
        {
            //Same reference check !
            if (ReferenceEquals(this, obj)) return true;

            //Type check !
            var other = obj as SessionData;
            if (other == null) return false;

            if (string.IsNullOrEmpty(DataTypeCD) && string.IsNullOrEmpty(other.DataTypeCD))
                //Do a per-property search
                return
                    Data == other.Data &&
                    DataTypeCD == other.DataTypeCD &&
                    SessionID == other.SessionID &&
                    Data == other.Data;
            return Id == other.Id && DataTypeCD == other.DataTypeCD;
        }

        #endregion Equals() Implementation

        #region Constructors

        /// <summary>
        ///     Class constructor...
        /// </summary>
        public SessionData()
        {
        }


        /// <summary>
        ///     The parameterized constructor.
        /// </summary>
        public SessionData(Guid id, string dataTypeCD, string data)
        {
            Id = id;
            DataTypeCD = dataTypeCD;
            Data = data;
        }

        #endregion

        #region Property Declarations

        /// <summary>
        ///     Private variable to hold the value for property DataTypeCD.
        /// </summary>
        private string _dataTypeCD;

        /// <summary>
        ///     Data Type of session data.
        /// </summary>
        public virtual string DataTypeCD
        {
            get => _dataTypeCD;
            set => _dataTypeCD = value;
        }

        /// <summary>
        ///     Private variable to hold the value for property Data.
        /// </summary>
        private string _data;

        /// <summary>
        ///     value of Session data.
        /// </summary>
        public virtual string Data
        {
            get => _data;
            set => _data = value;
        }

        private Guid _sessionID;

        public virtual Guid SessionID
        {
            get => _sessionID;
            set => _sessionID = value;
        }

        #endregion Property Declarations
    }
}