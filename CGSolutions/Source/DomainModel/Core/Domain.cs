﻿using System.Text;

namespace DomainModel.Core
{
    public class Domain : EntityBase<int, Domain>
    {
        #region ToString() Implementation

        /// <summary>
        ///     This methods returns a string representation of this class.
        /// </summary>
        /// <returns>A string representation of this class.</returns>
        public new virtual string ToString()
        {
            var sb = new StringBuilder();
            sb.AppendLine("------------Domain dump starts---------------");
            sb.AppendLine("DomainID: " + Id);
            sb.AppendLine("DomainCD: " + DomainCD);
            sb.AppendLine("Description: " + Description);
            sb.AppendLine("PortalID: " + Portal.Id);
            sb.AppendLine("------------Domain dump ends---------------");
            return sb.ToString();
        }

        #endregion ToString() Implementation

        # region property declarations

        public virtual string DomainCD { get; set; }
        public virtual string Description { get; set; }
        public virtual Portal Portal { get; set; }

        public virtual string ClientCD { get; set; }

        #endregion
    }
}