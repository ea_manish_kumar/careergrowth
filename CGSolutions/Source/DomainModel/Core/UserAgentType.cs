﻿using System;
using System.Text;

namespace DomainModel.Core
{
    [Serializable]
    public class UserAgentType : EntityBase<int, UserAgentType>
    {
        #region Constructors

        #endregion

        #region ToString() Implementation

        /// <summary>
        ///     This methods returns a string representation of this class.
        /// </summary>
        /// <returns>A string representation of this class.</returns>
        public new virtual string ToString()
        {
            var sb = new StringBuilder(7);

            sb.AppendLine("------------UserAgentType dump starts---------------");
            sb.AppendLine("UserAgentTypeID: " + UserAgentTypeID);
            sb.AppendLine("Name: " + (BrowserName != null ? BrowserName : "<NULL>"));

            sb.AppendLine("------------UserAgentType dump ends---------------");

            return sb.ToString();
        }

        #endregion ToString() Implementation

        #region GetClone() Implementation

        /// <summary>
        ///     This methods returns a cloned instance of this class.
        /// </summary>
        /// <returns>A string cloned instance of this class.</returns>
        public virtual UserAgentType GetClone()
        {
            var clone = new UserAgentType();

            clone.UserAgentTypeID = UserAgentTypeID;
            clone.BrowserName = BrowserName;
            return clone;
        }

        #endregion GetClone() Implementation

        #region GetHashCode() implementation.

        /// <summary>
        ///     Serves as a hash function for a particular type.
        /// </summary>
        /// <returns>
        ///     A hash code for the current UserAgentType.
        /// </returns>
        public override int GetHashCode()
        {
            return GetType().FullName.GetHashCode();
        }

        #endregion GetHashCode() Implementation

        #region Equals() implementation.

        /// <summary>
        ///     Determines whether the specified UserAgentType is equal to the current UserAgentType.
        /// </summary>
        /// <param name="obj">The UserAgentType to compare with the current UserAgentType.</param>
        /// <returns>
        ///     true if the specified UserAgentType is equal to the current UserAgentType; otherwise, false.
        /// </returns>
        /// <remarks>
        ///     Tests whether this and another object are equal in a way that will still pass
        ///     when proxy objects are being used.
        /// </remarks>
        public override bool Equals(object obj)
        {
            //Same reference check !
            //if (object.ReferenceEquals(this, obj)) return true;

            ////Type check !
            //CardTypes other = obj as CardTypes;
            //if (other == null) return false;

            //if ((Id == 0))
            //    //Do a per-property search
            //    return
            //    (CardType == other.CardType);
            //else
            //    return Id == other.Id;

            return true;
        }

        #endregion Equals() Implementation

        #region Property Declarations

        public virtual int UserAgentTypeID { get; set; }
        public virtual string BrowserName { get; set; }
        public virtual string BrowserVersion { get; set; }
        public virtual string EngineName { get; set; }
        public virtual string EngineVersion { get; set; }
        public virtual string OSName { get; set; }
        public virtual string OSVersion { get; set; }
        public virtual string DeviceModel { get; set; }
        public virtual string DeviceType { get; set; }
        public virtual string DTDeviceType { get; set; }

        #endregion Property Declarations
    }
}