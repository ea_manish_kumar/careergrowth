﻿using System;
using System.Text;

namespace DomainModel.Tests
{
    [Serializable]
    public class ABTestDataSession : EntityBase<int, ABTestDataSession>
    {
        #region ToString() Implementation

        /// <summary>
        ///     This methods returns a string representation of this class.
        /// </summary>
        /// <returns>A string representation of this class.</returns>
        public new virtual string ToString()
        {
            var sb = new StringBuilder(8);

            sb.AppendLine("------------ABTestDataSession dump starts---------------");
            sb.AppendLine("ABTestDataSessionID: " + Id);
            sb.AppendLine("ABTestID: " + ABTestID);
            sb.AppendLine("CaseIndex: " + (CaseIndex != null ? CaseIndex.ToString() : "<NULL>"));
            sb.AppendLine("SessionID: " + (SessionID != null ? SessionID.ToString() : "<NULL>"));
            sb.AppendLine("Data: " + (Data != null ? Data : "<NULL>"));
            sb.AppendLine("TimeStamp: " + (TimeStamp != null ? TimeStamp.ToString() : "<NULL>"));
            sb.AppendLine("------------ABTestDataSession dump ends---------------");

            return sb.ToString();
        }

        #endregion ToString() Implementation

        #region GetClone() Implementation

        /// <summary>
        ///     This methods returns a cloned instance of this class.
        /// </summary>
        /// <returns>A string cloned instance of this class.</returns>
        public virtual ABTestDataSession GetClone()
        {
            var clone = new ABTestDataSession();

            clone.Id = Id;
            clone.ABTestID = ABTestID;
            clone.CaseIndex = CaseIndex;
            clone.SessionID = SessionID;
            clone.Data = Data;
            clone.TimeStamp = TimeStamp;

            return clone;
        }

        #endregion GetClone() Implementation

        #region GetHashCode() implementation.

        /// <summary>
        ///     Serves as a hash function for a particular type.
        /// </summary>
        /// <returns>
        ///     A hash code for the current ABTestData.
        /// </returns>
        public override int GetHashCode()
        {
            return (GetType().FullName + Id).GetHashCode();
        }

        #endregion GetHashCode() Implementation

        #region Equals() implementation.

        /// <summary>
        ///     Determines whether the specified ABTestData is equal to the current ABTestData.
        /// </summary>
        /// <param name="obj">The ABTestData to compare with the current ABTestData.</param>
        /// <returns>
        ///     true if the specified ABTestData is equal to the current ABTestData; otherwise, false.
        /// </returns>
        /// <remarks>
        ///     Tests whether this and another object are equal in a way that will still pass
        ///     when proxy objects are being used.
        /// </remarks>
        public override bool Equals(object obj)
        {
            //Same reference check !
            if (ReferenceEquals(this, obj)) return true;

            //Type check !
            var other = obj as ABTestDataSession;
            if (other == null) return false;

            if (Id == 0)
                //Do a per-property search
                return
                    ABTestID == other.ABTestID &&
                    CaseIndex == other.CaseIndex &&
                    SessionID == other.SessionID &&
                    Data == other.Data &&
                    TimeStamp == other.TimeStamp;
            return Id == other.Id;
        }

        #endregion Equals() Implementation

        #region Constructors

        /// <summary>
        ///     Class constructor...
        /// </summary>
        public ABTestDataSession()
        {
            //TODO:Write constructor stuff here..
        }


        /// <summary>
        ///     The parameterized constructor.
        /// </summary>
        public ABTestDataSession(int iAbTestID, int? iCaseIndex, string sSessionID, string sData)
        {
            ABTestID = iAbTestID;
            CaseIndex = iCaseIndex;
            SessionID = new Guid(sSessionID);
            Data = sData;
            TimeStamp = DateTime.Now;
        }

        #endregion

        #region Property Declarations

        /// <summary>
        ///     Private variable to hold the value for property ABTestID.
        /// </summary>
        private int _aBTestID;

        /// <summary>
        ///     Id of ABTest.
        /// </summary>
        public virtual int ABTestID
        {
            get => _aBTestID;
            set => _aBTestID = value;
        }

        /// <summary>
        ///     Private variable to hold the value for property CaseIndex.
        /// </summary>
        private int? _caseIndex;

        /// <summary>
        ///     Case index.
        /// </summary>
        public virtual int? CaseIndex
        {
            get => _caseIndex;
            set => _caseIndex = value;
        }

        /// <summary>
        ///     Private variable to hold the value for property UserID.
        /// </summary>
        private Guid _SessionID;

        /// <summary>
        ///     UserID of User.
        /// </summary>
        public virtual Guid SessionID
        {
            get => _SessionID;
            set => _SessionID = value;
        }

        /// <summary>
        ///     Private variable to hold the value for property Data.
        /// </summary>
        private string _data;

        /// <summary>
        ///     Data in ABTest.
        /// </summary>
        public virtual string Data
        {
            get => _data;
            set => _data = value;
        }

        /// <summary>
        ///     Private variable to hold the value for property TimeStamp.
        /// </summary>
        private DateTime? _timeStamp;

        /// <summary>
        ///     TimeStamp.
        /// </summary>
        public virtual DateTime? TimeStamp
        {
            get => _timeStamp;
            set => _timeStamp = value;
        }

        #endregion Property Declarations
    }
}