﻿using System;
using System.Text;

namespace DomainModel.Core
{
    [Serializable]
    public class SessionExtended : EntityBase<int, SessionExtended>
    {
        #region ToString() Implementation

        /// <summary>
        ///     This methods returns a string representation of this class.
        /// </summary>
        /// <returns>A string representation of this class.</returns>
        public new virtual string ToString()
        {
            var sb = new StringBuilder(5);

            sb.AppendLine("------------SessionExtended dump starts---------------");
            sb.AppendLine("Id: " + Id);
            sb.AppendLine("SessionID: " + SessionID);
            sb.AppendLine("BrowserID: " + BrowserID);
            sb.AppendLine("CreatedOn: " + CreatedOn);
            sb.AppendLine("------------SessionExtended dump ends---------------");
            return sb.ToString();
        }

        #endregion ToString() Implementation

        #region GetClone() Implementation

        /// <summary>
        ///     This methods returns a cloned instance of this class.
        /// </summary>
        /// <returns>A string cloned instance of this class.</returns>
        public virtual SessionExtended GetClone()
        {
            var clone = new SessionExtended();

            clone.Id = Id;
            clone.SessionID = SessionID;
            clone.BrowserID = BrowserID;
            clone.CreatedOn = CreatedOn;
            return clone;
        }

        #endregion GetClone() Implementation

        #region GetHashCode() implementation.

        /// <summary>
        ///     Serves as a hash function for a particular type.
        /// </summary>
        /// <returns>
        ///     A hash code for the current SessionData.
        /// </returns>
        public override int GetHashCode()
        {
            return (GetType().FullName + SessionID + BrowserID).GetHashCode();
        }

        #endregion GetHashCode() Implementation

        #region Equals() implementation.

        /// <summary>
        ///     Determines whether the specified SessionData is equal to the current SessionData.
        /// </summary>
        /// <param name="obj">The SessionData to compare with the current SessionData.</param>
        /// <returns>
        ///     true if the specified SessionData is equal to the current SessionData; otherwise, false.
        /// </returns>
        /// <remarks>
        ///     Tests whether this and another object are equal in a way that will still pass
        ///     when proxy objects are being used.
        /// </remarks>
        public override bool Equals(object obj)
        {
            //Same reference check !
            if (ReferenceEquals(this, obj)) return true;

            //Type check !
            var other = obj as SessionExtended;
            if (other == null) return false;
            return SessionID == other.SessionID && BrowserID == other.BrowserID;
        }

        #endregion Equals() Implementation

        #region Constructors

        /// <summary>
        ///     Class constructor...
        /// </summary>
        public SessionExtended()
        {
        }


        /// <summary>
        ///     The parameterized constructor.
        /// </summary>
        public SessionExtended(Guid sessionID, Guid browserID)
        {
            SessionID = sessionID;
            BrowserID = browserID;
        }

        #endregion

        #region Property Declarations

        public virtual Guid SessionID { get; set; }

        public virtual Guid BrowserID { get; set; }

        public virtual DateTime CreatedOn { get; set; }

        #endregion Property Declarations
    }
}