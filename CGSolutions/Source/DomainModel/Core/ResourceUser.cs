using System;
using System.Text;

namespace DomainModel.Core
{
    [Serializable]
    public class ResourceUser : EntityBase<Guid, ResourceUser>
    {
        #region Constructors

        #endregion

        #region Property Declarations

        public virtual int UserID { get; set; }

        #endregion Property Declarations

        #region ToString() Implementation

        /// <summary>
        ///     This methods returns a string representation of this class.
        /// </summary>
        /// <returns>A string representation of this class.</returns>
        public new virtual string ToString()
        {
            var sb = new StringBuilder(9);

            sb.AppendLine("------------UserSession dump starts---------------");
            sb.AppendLine("Id: " + Id);
            sb.AppendLine("UserID: " + UserID);
            sb.AppendLine("------------UserSession dump ends---------------");

            return sb.ToString();
        }

        #endregion ToString() Implementation

        #region GetClone() Implementation

        /// <summary>
        ///     This methods returns a cloned instance of this class.
        /// </summary>
        /// <returns>A string cloned instance of this class.</returns>
        public virtual ResourceUser GetClone()
        {
            var clone = new ResourceUser();

            clone.Id = Id;
            clone.UserID = UserID;

            return clone;
        }

        #endregion GetClone() Implementation

        #region GetHashCode() implementation.

        /// <summary>
        ///     Serves as a hash function for a particular type.
        /// </summary>
        /// <returns>
        ///     A hash code for the current HitLog.
        /// </returns>
        public override int GetHashCode()
        {
            return (GetType().FullName + Id).GetHashCode();
        }

        #endregion GetHashCode() Implementation

        #region Equals() implementation.

        /// <summary>
        ///     Determines whether the specified HitLog is equal to the current HitLog.
        /// </summary>
        /// <param name="obj">The HitLog to compare with the current HitLog.</param>
        /// <returns>
        ///     true if the specified HitLog is equal to the current HitLog; otherwise, false.
        /// </returns>
        /// <remarks>
        ///     Tests whether this and another object are equal in a way that will still pass
        ///     when proxy objects are being used.
        /// </remarks>
        public override bool Equals(object obj)
        {
            //Same reference check !
            if (ReferenceEquals(this, obj)) return true;

            //Type check !
            var other = obj as ResourceUser;
            if (other == null) return false;

            if (Id.ToString() == "")
                //Do a per-property search
                return
                    Id == other.Id &&
                    UserID == other.UserID;
            return Id == other.Id;
        }

        #endregion Equals() Implementation
    }
}