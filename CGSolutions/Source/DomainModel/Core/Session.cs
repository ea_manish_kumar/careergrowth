using System;
using System.Collections.Generic;
using System.Text;

namespace DomainModel.Core
{
    [Serializable]
    public class Session : EntityBase<Guid, Session>
    {
        #region Constructors

        #endregion

        #region ToString() Implementation

        /// <summary>
        ///     This methods returns a string representation of this class.
        /// </summary>
        /// <returns>A string representation of this class.</returns>
        public new virtual string ToString()
        {
            var sb = new StringBuilder(9);

            sb.AppendLine("------------ Session dump starts---------------");
            sb.AppendLine("Id: " + Id);
            sb.AppendLine("DomainCode: " + (DomainCD ?? "<NULL>"));
            //sb.AppendLine("VisitType: " + (this.VisitType ?? "<NULL>"));
            sb.AppendLine("Requested_URL: " + (Requested_URL ?? "<NULL>"));
            sb.AppendLine("IPAddress: " + (IPAddress ?? "<NULL>"));
            sb.AppendLine("Source: " + (Source ?? "<NULL>"));
            sb.AppendLine("Campaign: " + (Campaign ?? "<NULL>"));
            //sb.AppendLine("IsAuth: " + this.IsAuth.ToString());
            sb.AppendLine("Medium: " + (Medium ?? "<NULL>"));
            sb.AppendLine("URL_Referrer: " + (URL_Referrer ?? "<NULL>"));
            sb.AppendLine("UserAgent: " + (UserAgent ?? "<NULL>"));
            sb.AppendLine("UserAgentTypeID: " + UserAgentTypeID);
            sb.AppendLine("Utm_Source: " + (Utm_Source ?? "<NULL>"));
            sb.AppendLine("Utm_Term: " + (Utm_Term ?? "<NULL>"));
            sb.AppendLine("Utm_Content: " + (Utm_Content ?? "<NULL>"));
            sb.AppendLine("Utm_Keyword: " + (Utm_Keyword ?? "<NULL>"));
            sb.AppendLine("CreatedOn: " + (CreatedOn.HasValue ? CreatedOn.Value.ToString() : "<NULL>"));
            //sb.AppendLine("ExpiredOn: " + (this.ExpiredOn.HasValue ? this.ExpiredOn.Value.ToString() : "<NULL>"));
            sb.AppendLine("------------ Session dump ends---------------");

            return sb.ToString();
        }

        #endregion ToString() Implementation

        #region GetClone() Implementation

        /// <summary>
        ///     This methods returns a cloned instance of this class.
        /// </summary>
        /// <returns>A string cloned instance of this class.</returns>
        public virtual Session GetClone()
        {
            //return this.MemberwiseClone();    Not preferred as it inclludes boxing and unboxing.

            var clone = new Session();

            clone.Id = Id;
            clone.Campaign = Campaign;
            clone.CreatedOn = CreatedOn;
            clone.DomainCD = DomainCD;
            //clone.ExpiredOn = this.ExpiredOn;
            clone.IPAddress = IPAddress;
            //clone.IsAuth = this.IsAuth;
            clone.Medium = Medium;
            clone.Requested_URL = Requested_URL;
            clone.SessionDatas = SessionDatas;
            clone.Source = Source;
            clone.URL_Referrer = URL_Referrer;
            clone.UserAgent = UserAgent;
            clone.UserAgentTypeID = UserAgentTypeID;
            clone.Utm_Content = Utm_Content;
            clone.Utm_Keyword = Utm_Keyword;
            clone.Utm_Source = Utm_Source;
            clone.Utm_Term = Utm_Term;
            //clone.VisitType = this.VisitType;

            return clone;
        }

        #endregion GetClone() Implementation

        #region GetHashCode() implementation.

        /// <summary>
        ///     Serves as a hash function for a particular type.
        /// </summary>
        /// <returns>
        ///     A hash code for the current HitLog.
        /// </returns>
        public override int GetHashCode()
        {
            return (GetType().FullName + Id).GetHashCode();
        }

        #endregion GetHashCode() Implementation

        #region Equals() implementation.

        /// <summary>
        ///     Determines whether the specified HitLog is equal to the current HitLog.
        /// </summary>
        /// <param name="obj">The HitLog to compare with the current HitLog.</param>
        /// <returns>
        ///     true if the specified HitLog is equal to the current HitLog; otherwise, false.
        /// </returns>
        /// <remarks>
        ///     Tests whether this and another object are equal in a way that will still pass
        ///     when proxy objects are being used.
        /// </remarks>
        public override bool Equals(object obj)
        {
            //Same reference check !
            if (ReferenceEquals(this, obj)) return true;

            //Type check !
            var other = obj as Session;
            if (other == null) return false;

            if (Id.ToString() == "")
                //Do a per-property search
                return
                    Id == other.Id &&
                    UserAgent == other.UserAgent &&
                    IPAddress == other.IPAddress &&
                    CreatedOn == other.CreatedOn &&
                    DomainCD == other.DomainCD; //&&
            //(VisitType == other.VisitType);
            return Id == other.Id;
        }

        #endregion Equals() Implementation

        #region Property Declarations

        public virtual string Campaign { get; set; }
        public virtual string Source { get; set; }
        public virtual string Medium { get; set; }
        public virtual string Utm_Source { get; set; }
        public virtual string Utm_Term { get; set; }
        public virtual string Utm_Content { get; set; }
        public virtual string Utm_Keyword { get; set; }
        public virtual string Requested_URL { get; set; }
        public virtual string URL_Referrer { get; set; }
        public virtual string UserAgent { get; set; }

        public virtual string IPAddress { get; set; }

        //public virtual string VisitType { get; set; }
        public virtual DateTime? CreatedOn { get; set; }

        //public virtual DateTime? ExpiredOn { get; set; }
        /// <summary>
        ///     Defines if the user's session is authenticated or not
        /// </summary>
        //public virtual bool IsAuth { get; set; }
        public virtual int UserAgentTypeID { get; set; }

        public virtual string DomainCD { get; set; }
        private IList<SessionData> _sessiondatas;

        public virtual IList<SessionData> SessionDatas
        {
            get => _sessiondatas;
            set => _sessiondatas = value;
        }

        #endregion Property Declarations
    }
}