﻿using System;
using System.Text;

namespace DomainModel.Tests
{
    [Serializable]
    public class ABTestAdminNote : EntityBase<int, ABTestAdminNote>
    {
        #region Constructors

        #endregion


        #region ToString() Implementation

        /// <summary>
        ///     This methods returns a string representation of this class.
        /// </summary>
        /// <returns>A string representation of this class.</returns>
        public new virtual string ToString()
        {
            var sb = new StringBuilder(6);

            sb.AppendLine("------------AdminNotes dump starts---------------");
            sb.AppendLine("ABTestAdminNoteID: " + Id);
            sb.AppendLine("AdminUserID: " + AdminUserID);
            sb.AppendLine("------------AdminNotes dump ends---------------");

            return sb.ToString();
        }

        #endregion ToString() Implementation

        #region GetClone() Implementation

        /// <summary>
        ///     This methods returns a cloned instance of this class.
        /// </summary>
        /// <returns>A string cloned instance of this class.</returns>
        public virtual ABTestAdminNote GetClone()
        {
            var clone = new ABTestAdminNote();
            clone.Id = Id;
            clone.AdminUserID = AdminUserID;

            return clone;
        }

        #endregion GetClone() Implementation

        #region GetHashCode() implementation.

        /// <summary>
        ///     Serves as a hash function for a particular type.
        /// </summary>
        /// <returns>
        ///     A hash code for the current AdminNotes.
        /// </returns>
        public override int GetHashCode()
        {
            return (GetType().FullName + Id).GetHashCode();
        }

        #endregion GetHashCode() Implementation

        #region Equals() implementation.

        /// <summary>
        ///     Determines whether the specified AdminNotes is equal to the current AdminNotes.
        /// </summary>
        /// <param name="obj">The AdminNotes to compare with the current AdminNotes.</param>
        /// <returns>
        ///     true if the specified AdminNotes is equal to the current AdminNotes; otherwise, false.
        /// </returns>
        /// <remarks>
        ///     Tests whether this and another object are equal in a way that will still pass
        ///     when proxy objects are being used.
        /// </remarks>
        public override bool Equals(object obj)
        {
            //Same reference check !
            if (ReferenceEquals(this, obj)) return true;

            //Type check !
            var other = obj as ABTestAdminNote;
            if (other == null) return false;

            if (Id == 0)
                //Do a per-property search
                return
                    AdminUserID == other.AdminUserID &&
                    ABTestID == other.ABTestID &&
                    Note == other.Note;
            return Id == other.Id;
        }

        #endregion Equals() Implementation

        #region Property Declarations

        //private int _Id;

        ///// <summary>
        ///// Id of AdminNotes.
        ///// </summary>
        //public virtual int Id
        //{
        //    get
        //    {
        //        return this._Id;
        //    }
        //    set
        //    {
        //        this._Id = value;
        //    }
        //}        

        /// <summary>
        ///     Private variable to hold the value for AdminUserID.
        /// </summary>
        private int _adminUserID;

        /// <summary>
        ///     AdminUserID.
        /// </summary>
        public virtual int AdminUserID
        {
            get => _adminUserID;
            set => _adminUserID = value;
        }

        /// <summary>
        ///     Private variable to hold the value for ABTestID.
        /// </summary>
        private int _abTestID;

        /// <summary>
        ///     Name of AdminNotes.
        /// </summary>
        public virtual int ABTestID
        {
            get => _abTestID;
            set => _abTestID = value;
        }

        /// <summary>
        ///     Private variable to hold the value for Timestamp.
        /// </summary>
        private DateTime _timestamp;

        /// <summary>
        ///     Name of AdminNotes.
        /// </summary>
        public virtual DateTime Timestamp
        {
            get => _timestamp;
            set => _timestamp = value;
        }

        /// <summary>
        ///     Private variable to hold the value for property Note.
        /// </summary>
        private string _note;

        /// <summary>
        ///     Decription Of AdminNotes.
        /// </summary>
        public virtual string Note
        {
            get => _note;
            set => _note = value;
        }

        #endregion Property Declarations
    }
}