using System;
using System.Text;

namespace DomainModel.Tests
{
    [Serializable]
    public class ABTestData : EntityBase<int, ABTestData>
    {
        #region ToString() Implementation

        /// <summary>
        ///     This methods returns a string representation of this class.
        /// </summary>
        /// <returns>A string representation of this class.</returns>
        public new virtual string ToString()
        {
            var sb = new StringBuilder(8);

            sb.AppendLine("------------ABTestData dump starts---------------");
            sb.AppendLine("ABTestDataID: " + Id);
            sb.AppendLine("ABTestID: " + ABTestID);
            sb.AppendLine("CaseIndex: " + (CaseIndex != null ? CaseIndex.ToString() : "<NULL>"));
            sb.AppendLine("UserID: " + (UserID != null ? UserID.ToString() : "<NULL>"));
            sb.AppendLine("Data: " + (Data != null ? Data : "<NULL>"));
            sb.AppendLine("TimeStamp: " + (TimeStamp != null ? TimeStamp.ToString() : "<NULL>"));
            sb.AppendLine("------------ABTestData dump ends---------------");

            return sb.ToString();
        }

        #endregion ToString() Implementation

        #region GetClone() Implementation

        /// <summary>
        ///     This methods returns a cloned instance of this class.
        /// </summary>
        /// <returns>A string cloned instance of this class.</returns>
        public virtual ABTestData GetClone()
        {
            var clone = new ABTestData();

            clone.Id = Id;
            clone.ABTestID = ABTestID;
            clone.CaseIndex = CaseIndex;
            clone.UserID = UserID;
            clone.Data = Data;
            clone.TimeStamp = TimeStamp;

            return clone;
        }

        #endregion GetClone() Implementation

        #region GetHashCode() implementation.

        /// <summary>
        ///     Serves as a hash function for a particular type.
        /// </summary>
        /// <returns>
        ///     A hash code for the current ABTestData.
        /// </returns>
        public override int GetHashCode()
        {
            return (GetType().FullName + Id).GetHashCode();
        }

        #endregion GetHashCode() Implementation

        #region Equals() implementation.

        /// <summary>
        ///     Determines whether the specified ABTestData is equal to the current ABTestData.
        /// </summary>
        /// <param name="obj">The ABTestData to compare with the current ABTestData.</param>
        /// <returns>
        ///     true if the specified ABTestData is equal to the current ABTestData; otherwise, false.
        /// </returns>
        /// <remarks>
        ///     Tests whether this and another object are equal in a way that will still pass
        ///     when proxy objects are being used.
        /// </remarks>
        public override bool Equals(object obj)
        {
            //Same reference check !
            if (ReferenceEquals(this, obj)) return true;

            //Type check !
            var other = obj as ABTestData;
            if (other == null) return false;

            if (Id == 0)
                //Do a per-property search
                return
                    ABTestID == other.ABTestID &&
                    CaseIndex == other.CaseIndex &&
                    UserID == other.UserID &&
                    Data == other.Data &&
                    TimeStamp == other.TimeStamp;
            return Id == other.Id;
        }

        #endregion Equals() Implementation

        #region Constructors

        /// <summary>
        ///     Class constructor...
        /// </summary>
        public ABTestData()
        {
            //TODO:Write constructor stuff here..
        }


        /// <summary>
        ///     The parameterized constructor.
        /// </summary>
        public ABTestData(int aBTestDataID, int aBTestID, int? caseIndex, int? UserID, string data, DateTime? timeStamp)
        {
            Id = aBTestDataID;
            ABTestID = aBTestID;
            CaseIndex = caseIndex;
            this.UserID = UserID;
            Data = data;
            TimeStamp = timeStamp;
        }

        #endregion

        #region Property Declarations

        /// <summary>
        ///     Private variable to hold the value for property ABTestID.
        /// </summary>
        private int _aBTestID;

        /// <summary>
        ///     Id of ABTest.
        /// </summary>
        public virtual int ABTestID
        {
            get => _aBTestID;
            set => _aBTestID = value;
        }

        /// <summary>
        ///     Private variable to hold the value for property CaseIndex.
        /// </summary>
        private int? _caseIndex;

        /// <summary>
        ///     Case index.
        /// </summary>
        public virtual int? CaseIndex
        {
            get => _caseIndex;
            set => _caseIndex = value;
        }

        /// <summary>
        ///     Private variable to hold the value for property UserID.
        /// </summary>
        private int? _UserID;

        /// <summary>
        ///     UserID of User.
        /// </summary>
        public virtual int? UserID
        {
            get => _UserID;
            set => _UserID = value;
        }

        /// <summary>
        ///     Private variable to hold the value for property Data.
        /// </summary>
        private string _data;

        /// <summary>
        ///     Data in ABTest.
        /// </summary>
        public virtual string Data
        {
            get => _data;
            set => _data = value;
        }

        /// <summary>
        ///     Private variable to hold the value for property TimeStamp.
        /// </summary>
        private DateTime? _timeStamp;

        /// <summary>
        ///     TimeStamp.
        /// </summary>
        public virtual DateTime? TimeStamp
        {
            get => _timeStamp;
            set => _timeStamp = value;
        }


        /// <summary>
        ///     Private variable to hold value for property ParentABTests
        /// </summary>
        private ABTest _parentABTest;

        /// <summary>
        ///     This property will be used to get/set value of ParentABTest (ParentABTest>)
        /// </summary>
        public virtual ABTest ParentABTest
        {
            get => _parentABTest;
            set => _parentABTest = value;
        }

        #endregion Property Declarations
    }
}