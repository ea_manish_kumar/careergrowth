using System;
using System.Collections.Generic;
using System.Text;

namespace DomainModel.Tests
{
    [Serializable]
    public class ABTestCase : EntityBase<int, ABTestData>
    {
        #region ToString() Implementation

        /// <summary>
        ///     This methods returns a string representation of this class.
        /// </summary>
        /// <returns>A string representation of this class.</returns>
        public new virtual string ToString()
        {
            var sb = new StringBuilder(6);

            sb.AppendLine("------------ABTestCase dump starts---------------");
            sb.AppendLine("ABTestID: " + ParentABTest.Id);
            sb.AppendLine("CaseIndex: " + CaseIndex);
            sb.AppendLine("Label: " + (Label != null ? Label : "<NULL>"));
            sb.AppendLine("Weight: " + (Weight != null ? Weight.ToString() : "<NULL>"));
            sb.AppendLine("------------ABTestCase dump ends---------------");

            return sb.ToString();
        }

        #endregion ToString() Implementation

        #region GetClone() Implementation

        /// <summary>
        ///     This methods returns a cloned instance of this class.
        /// </summary>
        /// <returns>A string cloned instance of this class.</returns>
        public virtual ABTestCase GetClone()
        {
            var clone = new ABTestCase();

            //clone.Id = this.Id;   //Parent object, don't clone
            clone.CaseIndex = CaseIndex;
            clone.Label = Label;
            clone.Weight = Weight;

            foreach (var data in ABTestDatas)
            {
                //TODO: complete this
            }

            return clone;
        }

        #endregion GetClone() Implementation


        #region GetHashCode() implementation.

        /// <summary>
        ///     Serves as a hash function for a particular type.
        /// </summary>
        /// <returns>
        ///     A hash code for the current ABTestCase.
        /// </returns>
        public override int GetHashCode()
        {
            return (GetType().FullName + ParentABTest.Id + CaseIndex).GetHashCode();
        }

        #endregion GetHashCode() Implementation

        #region Equals() implementation.

        /// <summary>
        ///     Determines whether the specified ABTestCase is equal to the current ABTestCase.
        /// </summary>
        /// <param name="obj">The ABTestCase to compare with the current ABTestCase.</param>
        /// <returns>
        ///     true if the specified ABTestCase is equal to the current ABTestCase; otherwise, false.
        /// </returns>
        /// <remarks>
        ///     Tests whether this and another object are equal in a way that will still pass
        ///     when proxy objects are being used.
        /// </remarks>
        public override bool Equals(object obj)
        {
            //Same reference check !
            if (ReferenceEquals(this, obj)) return true;

            //Type check !
            var other = obj as ABTestCase;
            if (other == null) return false;

            if ((ParentABTest.Id == 0 && other.ParentABTest.Id == 0) | (CaseIndex == 0 && other.CaseIndex == 0))
                //Do a per-property search
                return
                    Label == other.Label &&
                    Weight == other.Weight &&
                    ABTestDatas.Count == other.ABTestDatas.Count;
            return ParentABTest.Id == other.ParentABTest.Id && CaseIndex == other.CaseIndex;
        }

        #endregion Equals() Implementation

        #region Constructors

        /// <summary>
        ///     Class constructor...
        /// </summary>
        public ABTestCase()
        {
            _ABtestdatas = new List<ABTestData>();
        }

        /// <summary>
        ///     The parameterized constructor.
        /// </summary>
        public ABTestCase(int aBTestID, int caseIndex, string label, short? weight)
        {
            //this.Id = aBTestID; 
            ABtestID = aBTestID;
            CaseIndex = caseIndex;
            Label = label;
            Weight = weight;
        }

        #endregion

        #region Property Declarations

        /// <summary>
        ///     Acccess to Id column of base class prohibited in case of an entity that has composite key
        /// </summary>
        public new virtual int Id
        {
            get => throw new InvalidOperationException("Id column not applicable in this case");
            set => throw new InvalidOperationException("Id column not applicable in this case");
        }

        /// <summary>
        ///     Private variable to hold the value for property CaseIndex.
        /// </summary>
        private int _ABtestID;

        /// <summary>
        ///     Case Index.
        /// </summary>
        public virtual int ABtestID
        {
            get => ParentABTest.Id;
            set => _ABtestID = ParentABTest.Id;
        }

        /// <summary>
        ///     Private variable to hold the value for property CaseIndex.
        /// </summary>
        private int _caseIndex;

        /// <summary>
        ///     Case Index.
        /// </summary>
        public virtual int CaseIndex
        {
            get => _caseIndex;
            set => _caseIndex = value;
        }

        /// <summary>
        ///     Private variable to hold the value for property Label.
        /// </summary>
        private string _label;

        /// <summary>
        ///     Description of AB Test case.
        /// </summary>
        public virtual string Label
        {
            get => _label;
            set => _label = value;
        }

        /// <summary>
        ///     Private variable to hold the value for property Weight.
        /// </summary>
        private short? _weight;

        /// <summary>
        ///     Weight Given.
        /// </summary>
        public virtual short? Weight
        {
            get => _weight;
            set => _weight = value;
        }

        /// <summary>
        ///     Private variable to hold value for property ParentABTests
        /// </summary>
        private ABTest _parentABTest;

        /// <summary>
        ///     This property will be used to get/set value of ParentABTest (ParentABTest>)
        /// </summary>
        public virtual ABTest ParentABTest
        {
            get => _parentABTest;
            set => _parentABTest = value;
        }


        private IList<ABTestData> _ABtestdatas;

        /// <summary>
        ///     ABTestCases collection
        /// </summary>
        public virtual IList<ABTestData> ABTestDatas
        {
            get => _ABtestdatas;
            set => _ABtestdatas = value;
        }

        #endregion Property Declarations
    }
}