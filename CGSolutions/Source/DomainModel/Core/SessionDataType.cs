﻿using System;
using System.Text;

namespace DomainModel.Core
{
    [Serializable]
    public class SessionDataType : EntityBase<string, SessionDataType>
    {
        #region ToString() Implementation

        /// <summary>
        ///     This methods returns a string representation of this class.
        /// </summary>
        /// <returns>A string representation of this class.</returns>
        public new virtual string ToString()
        {
            var sb = new StringBuilder(5);

            sb.AppendLine("------------SessionDataType dump starts---------------");
            sb.AppendLine("Id: " + (Id != null ? Id : "<NULL>"));
            sb.AppendLine("Description: " + (Description != null ? Description : "<NULL>"));
            sb.AppendLine("Persist: " + (Persist != null ? Persist.ToString() : "<NULL>"));
            sb.AppendLine("------------SessionDataType dump ends---------------");

            return sb.ToString();
        }

        #endregion ToString() Implementation

        #region GetClone() Implementation

        /// <summary>
        ///     This methods returns a cloned instance of this class.
        /// </summary>
        /// <returns>A string cloned instance of this class.</returns>
        public virtual SessionDataType GetClone()
        {
            var clone = new SessionDataType();

            clone.Id = Id;
            clone.Description = Description;
            clone.Persist = Persist;

            return clone;
        }

        #endregion GetClone() Implementation

        #region GetHashCode() implementation.

        /// <summary>
        ///     Serves as a hash function for a particular type.
        /// </summary>
        /// <returns>
        ///     A hash code for the current SessionDataType.
        /// </returns>
        public override int GetHashCode()
        {
            return (GetType().FullName + Id).GetHashCode();
        }

        #endregion GetHashCode() Implementation

        #region Equals() implementation.

        /// <summary>
        ///     Determines whether the specified SessionDataType is equal to the current SessionDataType.
        /// </summary>
        /// <param name="obj">The SessionDataType to compare with the current SessionDataType.</param>
        /// <returns>
        ///     true if the specified SessionDataType is equal to the current SessionDataType; otherwise, false.
        /// </returns>
        /// <remarks>
        ///     Tests whether this and another object are equal in a way that will still pass
        ///     when proxy objects are being used.
        /// </remarks>
        public override bool Equals(object obj)
        {
            //Same reference check !
            if (ReferenceEquals(this, obj)) return true;

            //Type check !
            var other = obj as SessionDataType;
            if (other == null) return false;

            if (string.IsNullOrEmpty(Id))
                //Do a per-property search
                return
                    Description == other.Description &&
                    Persist == other.Persist;
            return Id == other.Id;
        }

        #endregion Equals() Implementation

        #region Constructors

        /// <summary>
        ///     Class constructor...
        /// </summary>
        public SessionDataType()
        {
            //TODO:Write constructor stuff here..
        }


        /// <summary>
        ///     The parameterized constructor.
        /// </summary>
        public SessionDataType(string id, string description, bool? persist)
        {
            Id = id;
            Description = description;
            Persist = persist;
        }

        #endregion

        #region Property Declarations

        ///// <summary>
        ///// Private variable to hold the value for property Id.
        ///// </summary>
        //private string _id;

        ///// <summary>
        ///// Primary Key.
        ///// </summary>
        //public virtual string Id
        //{
        //    get
        //    {
        //        return this._id;
        //    }
        //    set
        //    {
        //        this._id = value;
        //    }
        //}

        /// <summary>
        ///     Private variable to hold the value for property Description.
        /// </summary>
        private string _description;

        /// <summary>
        ///     Description of Data Type.
        /// </summary>
        public virtual string Description
        {
            get => _description;
            set => _description = value;
        }

        /// <summary>
        ///     Private variable to hold the value for property Persist.
        /// </summary>
        private bool? _persist;

        /// <summary>
        ///     Persists yes/no.
        /// </summary>
        public virtual bool? Persist
        {
            get => _persist;
            set => _persist = value;
        }

        #endregion Property Declarations
    }
}