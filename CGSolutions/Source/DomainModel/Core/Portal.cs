﻿using System;
using System.Text;

namespace DomainModel.Core
{
    [Serializable]
    public class Portal : EntityBase<int, Portal>
    {
        #region ToString() Implementation

        /// <summary>
        /// This methods returns a string representation of this class.
        /// </summary>
        /// <returns>A string representation of this class.</returns>
        public new virtual string ToString()
        {
            var sb = new StringBuilder(8);

            sb.AppendLine("------------Portal dump starts---------------");
            sb.AppendLine("Id: " + Id);
            sb.AppendLine("Description: " + (Description != null ? Description : "<NULL>"));
            sb.AppendLine("StartDate: " + (StartDate != null ? StartDate.ToString() : "<NULL>"));
            sb.AppendLine("Status: " + (Status != null ? Status : "<NULL>"));
            sb.AppendLine("PortalCD: " + (PortalCD != null ? PortalCD : "<NULL>"));
            sb.AppendLine("CultureID: " + CultureID);
            sb.AppendLine("------------Portal dump ends---------------");

            return sb.ToString();
        }

        #endregion ToString() Implementation


        #region GetHashCode() implementation.

        /// <summary>
        /// Serves as a hash function for a particular type.
        /// </summary>
        /// <returns>A hash code for the current Culture.</returns>
        public override int GetHashCode()
        {
            return (GetType().FullName + Id).GetHashCode();
        }

        #endregion GetHashCode() Implementation

        #region Constructors

        /// <summary>
		/// Class constructor...
		/// </summary>
		public Portal()
		{
			//TODO:Write constructor stuff here..
		}


        /// <summary>
		/// The parameterized constructor.
		/// </summary>
        /// 
        public Portal(int PortalID, string Description, DateTime StartDate, string Status, string PortalCD, string ClientCD, int CultureID, string Domain)
		{
            Id = Id;
            this.Description = Description;
            this.StartDate = StartDate;
            this.Status = Status;
            this.PortalCD = PortalCD;
            this.CultureID = CultureID;
		}

        #endregion


        #region Property Declarations

        public virtual string Description { get; set; }

        public virtual DateTime? StartDate { get; set; }

        public virtual string Status { get; set; }

        public virtual string PortalCD { get; set; }

        public virtual int CultureID { get; set; }

        #endregion
    }
}
