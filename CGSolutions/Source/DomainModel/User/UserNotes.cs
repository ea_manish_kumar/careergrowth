﻿using System;
using System.Text;

namespace DomainModel.User
{
    [Serializable]
    public class UserNotesDTO
    {
        #region Constructors

        #endregion

        #region ToString() Implementation

        /// <summary>
        ///     This methods returns a string representation of this class.
        /// </summary>
        /// <returns>A string representation of this class.</returns>
        public override string ToString()
        {
            var sb = new StringBuilder(6);

            sb.AppendLine("------------UserNotesDTO dump starts---------------");
            sb.AppendLine("UserID: " + UserID);
            sb.AppendLine("AdminUserID: " + AdminUserID);
            sb.AppendLine("Notes: " + (Notes != null ? Notes : "<NULL>"));
            sb.AppendLine("TimeStamp: " + (TimeStamp != null ? TimeStamp.ToString() : "<NULL>"));

            sb.AppendLine("------------UserNotesDTO dump ends---------------");

            return sb.ToString();
        }

        #endregion ToString() Implementation

        #region GetClone() Implementation

        /// <summary>
        ///     This methods returns a cloned instance of this class.
        /// </summary>
        /// <returns>A string cloned instance of this class.</returns>
        public virtual UserNotesDTO GetClone()
        {
            var clone = new UserNotesDTO();

            clone.UserID = UserID;
            clone.AdminUserID = AdminUserID;
            clone.Notes = Notes;
            clone.TimeStamp = TimeStamp;

            return clone;
        }

        #endregion GetClone() Implementation

        #region Equals() implementation.

        /// <summary>
        ///     Determines whether the specified SKUOrderDetailDTO is equal to the current SKUOrderDetailDTO.
        /// </summary>
        /// <param name="obj">The SKUOrderDetailDTO to compare with the current SKUOrderDetailDTO.</param>
        /// <returns>
        ///     true if the specified SKUOrderDetailDTO is equal to the current SKUOrderDetailDTO; otherwise, false.
        /// </returns>
        /// <remarks>
        ///     Tests whether this and another object are equal in a way that will still pass
        ///     when proxy objects are being used.
        /// </remarks>
        public override bool Equals(object obj)
        {
            return true;
        }

        #endregion

        #region GetHashCode() implementation.

        /// <summary>
        ///     Serves as a hash function for a particular type.
        /// </summary>
        /// <returns>
        ///     A hash code for the current SKUOrderDetailDTO.
        /// </returns>
        public override int GetHashCode()
        {
            return GetType().FullName.GetHashCode();
        }

        #endregion GetHashCode() Implementation

        #region Property Declarations

        public virtual int Id { get; set; }
        public virtual int UserID { get; set; }
        public virtual int AdminUserID { get; set; }
        public virtual string Notes { get; set; }
        public virtual DateTime TimeStamp { get; set; }

        #endregion Property Declarations
    }
}