﻿using System;
using System.Text;

namespace DomainModel.User
{
    public class ProgressHistory : EntityBase<int, ProgressHistory>
    {
        #region Constructors

        #endregion

        #region ToString() Implementation

        /// <summary>
        ///     This methods returns a string representation of this class.
        /// </summary>
        /// <returns>A string representation of this class.</returns>
        public new virtual string ToString()
        {
            var sb = new StringBuilder(8);

            sb.AppendLine("------------ProgressHistory dump starts---------------");
            sb.AppendLine(base.ToString());
            sb.AppendLine("ID: " + Id);
            sb.AppendLine("PreferenceCD: " + PreferenceCD);
            sb.AppendLine("Value: " + Value);
            sb.AppendLine("TimeStamp: " + (TimeStamp != null ? TimeStamp.ToString() : "<NULL>"));
            sb.AppendLine("------------ProgressHistory dump ends---------------");

            return sb.ToString();
        }

        #endregion ToString() Implementation

        #region Property Declarations

        public virtual string PreferenceCD { get; set; }
        public virtual string Value { get; set; }
        public virtual DateTime TimeStamp { get; set; }

        #endregion Property Declarations
    }
}