﻿using System;
using System.Text;

namespace DomainModel.User
{
    public class SharedAuth
    {
        #region ToString() Implementation

        /// <summary>
        ///     This methods returns a string representation of this class.
        /// </summary>
        /// <returns>A string representation of this class.</returns>
        public new virtual string ToString()
        {
            var sb = new StringBuilder(8);

            sb.Append("------------SharedAuth dump starts---------------\n");
            sb.AppendLine(base.ToString());
            sb.Append("UserID: " + UserID + "\n");
            sb.Append("AuthProviderCD: " + AuthProviderCD + "\n");
            sb.Append("PUID: " + PUID + "\n");
            sb.Append("AccessToken: " + AccessToken + "\n");
            sb.Append("------------SharedAuth dump ends---------------\n");

            return sb.ToString();
        }

        #endregion ToString() Implementation

        #region GetClone() Implementation

        /// <summary>
        ///     This methods returns a cloned instance of this class.
        /// </summary>
        /// <returns>A string cloned instance of this class.</returns>
        public virtual SharedAuth GetClone()
        {
            var clone = new SharedAuth();

            clone.UserID = UserID;
            clone.AuthProviderCD = AuthProviderCD;
            clone.PUID = PUID;
            clone.AccessToken = AccessToken;
            clone.CreatedOn = CreatedOn;
            clone.IsPrimary = IsPrimary;

            return clone;
        }

        #endregion GetClone() Implementation

        #region GetHashCode() implementation.

        /// <summary>
        ///     Serves as a hash function for a particular type.
        /// </summary>
        /// <returns>
        ///     A hash code for the current HitLog.
        /// </returns>
        public override int GetHashCode()
        {
            return (GetType().FullName + UserID + AuthProviderCD).GetHashCode();
        }

        #endregion GetHashCode() Implementation

        #region Equals() implementation.

        /// <summary>
        ///     Determines whether the specified HitLog is equal to the current HitLog.
        /// </summary>
        /// <param name="obj">The HitLog to compare with the current HitLog.</param>
        /// <returns>
        ///     true if the specified HitLog is equal to the current HitLog; otherwise, false.
        /// </returns>
        /// <remarks>
        ///     Tests whether this and another object are equal in a way that will still pass
        ///     when proxy objects are being used.
        /// </remarks>
        public override bool Equals(object obj)
        {
            //Same reference check !
            if (ReferenceEquals(this, obj)) return true;

            //Type check !
            var other = obj as SharedAuth;
            if (other == null) return false;

            if (UserID <= 0)
                //Do a per-property search
                return
                    UserID == other.UserID &&
                    AuthProviderCD == other.AuthProviderCD &&
                    PUID == other.PUID &&
                    AccessToken == other.AccessToken;
            return UserID == other.UserID;
        }

        #endregion Equals() Implementation

        #region Property Declarations

        public virtual int UserID { get; set; }
        public virtual string AuthProviderCD { get; set; }
        public virtual string PUID { get; set; }
        public virtual string AccessToken { get; set; }
        public virtual DateTime CreatedOn { get; set; }
        public virtual bool IsPrimary { get; set; }

        #endregion Property Declarations
    }
}