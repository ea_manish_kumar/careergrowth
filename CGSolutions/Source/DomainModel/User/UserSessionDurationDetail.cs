using System;
using System.Text;

namespace DomainModel.User
{
    [Serializable]
    public class UserSessionDurationDetail
    {
        #region ToString() Implementation

        /// <summary>
        ///     This methods returns a string representation of this class.
        /// </summary>
        /// <returns>A string representation of this class.</returns>
        public new virtual string ToString()
        {
            var sb = new StringBuilder(4);

            sb.AppendLine("------------UserSessionDurationDetail dump starts---------------");
            sb.AppendLine("LoginDate: " + LoginDate);
            sb.AppendLine("Duration: " + Duration);
            sb.AppendLine("------------UserSessionDurationDetail dump ends---------------");

            return sb.ToString();
        }

        #endregion ToString() Implementation

        #region GetClone() Implementation

        /// <summary>
        ///     This methods returns a cloned instance of this class.
        /// </summary>
        /// <returns>A string cloned instance of this class.</returns>
        public virtual UserSessionDurationDetail GetClone()
        {
            var clone = new UserSessionDurationDetail();

            clone.LoginDate = LoginDate;
            clone.Duration = Duration;

            return clone;
        }

        #endregion GetClone() Implementation

        #region GetHashCode() implementation.

        /// <summary>
        ///     Serves as a hash function for a particular type.
        /// </summary>
        /// <returns>
        ///     A hash code for the current UserSessionDurationDetail.
        /// </returns>
        public override int GetHashCode()
        {
            return (GetType().FullName + LoginDate).GetHashCode();
        }

        #endregion GetHashCode() Implementation

        #region Equals() implementation.

        /// <summary>
        ///     Determines whether the specified UserSessionDurationDetail is equal to the current UserSessionDurationDetail.
        /// </summary>
        /// <param name="obj">The UserSessionDurationDetail to compare with the current UserSessionDurationDetail.</param>
        /// <returns>
        ///     true if the specified UserSessionDurationDetail is equal to the current UserSessionDurationDetail; otherwise,
        ///     false.
        /// </returns>
        /// <remarks>
        ///     Tests whether this and another object are equal in a way that will still pass
        ///     when proxy objects are being used.
        /// </remarks>
        public override bool Equals(object obj)
        {
            //Same reference check !
            if (ReferenceEquals(this, obj)) return true;

            //Type check !
            var other = obj as UserSessionDurationDetail;
            if (other == null) return false;
            return true;

            //if ((LoginDate<UNDEFINED>))
            //    //Do a per-property search
            //    return 
            //    (Duration == other.Duration);
            //else
            //    return LoginDate == other.LoginDate;
        }

        #endregion Equals() Implementation

        #region Constructors

        /// <summary>
        ///     Class constructor...
        /// </summary>
        public UserSessionDurationDetail()
        {
            //TODO:Write constructor stuff here..
        }


        /// <summary>
        ///     The parameterized constructor.
        /// </summary>
        public UserSessionDurationDetail(DateTime loginDate, int duration)
        {
            LoginDate = loginDate;
            Duration = duration;
        }

        #endregion

        #region Property Declarations

        /// <summary>
        ///     Private variable to hold the value for property LoginDate.
        /// </summary>
        private DateTime _loginDate;

        /// <summary>
        ///     Login Date.
        /// </summary>
        public virtual DateTime LoginDate
        {
            get => _loginDate;
            set => _loginDate = value;
        }

        /// <summary>
        ///     Private variable to hold the value for property Duration.
        /// </summary>
        private int _duration;

        /// <summary>
        ///     Duration in minutes.
        /// </summary>
        public virtual int Duration
        {
            get => _duration;
            set => _duration = value;
        }

        #endregion Property Declarations
    }
}