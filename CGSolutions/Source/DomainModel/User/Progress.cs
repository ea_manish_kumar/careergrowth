﻿using System.Text;

namespace DomainModel.User
{
    public class Progress : EntityBase<int, Progress>
    {
        #region Constructors

        #endregion

        #region ToString() Implementation

        /// <summary>
        ///     This methods returns a string representation of this class.
        /// </summary>
        /// <returns>A string representation of this class.</returns>
        public new virtual string ToString()
        {
            var sb = new StringBuilder(8);

            sb.AppendLine("------------Progress dump starts---------------");
            sb.AppendLine(base.ToString());
            sb.AppendLine("ID: " + Id);
            sb.AppendLine("ProgressCD: " + (ProgressCD ?? "<NULL>"));
            sb.AppendLine("Definition: " + (Definition ?? "<NULL>"));
            sb.AppendLine("StageIndex: " + (StageIndex.HasValue ? StageIndex.Value.ToString() : "<NULL>"));
            sb.AppendLine("DocumentTypeCD: " + (DocumentTypeCD ?? "<NULL>"));
            sb.AppendLine("------------Progress dump ends---------------");

            return sb.ToString();
        }

        #endregion ToString() Implementation

        #region Property Declarations

        public virtual string ProgressCD { get; set; }
        public virtual short? StageIndex { get; set; }
        public virtual string Definition { get; set; }
        public virtual string DocumentTypeCD { get; set; }

        #endregion Property Declarations
    }
}