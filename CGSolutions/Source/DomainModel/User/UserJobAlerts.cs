﻿namespace DomainModel.User
{
    public class UserJobAlerts : EntityBase<int, UserJobAlerts>
    {
        public virtual int ID
        {
            get => base.Id;
            set => base.Id = value;
        }

        public virtual int UserID { get; set; }

        public virtual string SubscriberID { get; set; }

        public virtual int SubscriptionID { get; set; }
    }

    public class JobAlertSubscription
    {
        public int SubsciptionID { get; set; }
        public string JobTitle { get; set; }
        public string JobLocation { get; set; }
        public string Frequency { get; set; }
        public string UserSubscriptionStatus { get; set; }
        public string DocumentStatus { get; set; }
    }
}