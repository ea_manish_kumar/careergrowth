﻿using System.Text;

namespace DomainModel.User
{
    public class UserAgentTypeDTO
    {
        #region ToString() Implementation

        public override string ToString()
        {
            var sb = new StringBuilder(6);

            sb.AppendLine("------------UserAgentTypeDTO dump starts---------------");
            sb.AppendLine("BrowserName: " + (BrowserName ?? "<NULL>"));
            sb.AppendLine("BrowserVersion: " + (BrowserVersion ?? "<NULL>"));
            sb.AppendLine("EngineName: " + (EngineName ?? "<NULL>"));
            sb.AppendLine("EngineVersion: " + (EngineVersion ?? "<NULL>"));
            sb.AppendLine("OSName: " + (OSName ?? "<NULL>"));
            sb.AppendLine("OSVersion: " + (OSVersion ?? "<NULL>"));
            sb.AppendLine("DeviceModel: " + (DeviceModel ?? "<NULL>"));
            sb.AppendLine("DeviceType: " + (DeviceType ?? "<NULL>"));
            sb.AppendLine("DTDeviceType: " + (DTDeviceType ?? "<NULL>"));
            sb.AppendLine("------------UserAgentTypeDTO dump ends---------------");
            return sb.ToString();
        }

        #endregion ToString() Implementation

        #region Property Declarations

        public virtual string BrowserName { get; set; }
        public virtual string BrowserVersion { get; set; }
        public virtual string EngineName { get; set; }
        public virtual string EngineVersion { get; set; }
        public virtual string OSName { get; set; }
        public virtual string OSVersion { get; set; }
        public virtual string DeviceModel { get; set; }
        public virtual string DeviceType { get; set; }
        public virtual string DTDeviceType { get; set; }
        public virtual string UserAgent { get; set; }
        public virtual bool IsSmallScreen { get; set; }
        public virtual bool IsTablet { get; set; }

        #endregion Property Declarations
    }
}