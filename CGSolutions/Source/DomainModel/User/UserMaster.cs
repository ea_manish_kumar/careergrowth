﻿using System;
using System.Text;

namespace DomainModel.User
{
    public class UserMaster : EntityBase<int, UserMaster>
    {
        public UserMaster()
        {
            RoleID = 0;
            CreationDate = DateTime.Now;
            LastLoginDate = DateTime.Now;
            IsActive = true;
        }

        public UserMaster(string userName, string password, short Role)
        {
            UserName = userName;
            HashedPwd = password;
            RoleID = Role;
            CreationDate = DateTime.Now;
            LastLoginDate = DateTime.Now;
            IsActive = true;
        }


        public virtual int ID
        {
            get => base.Id;
            set => base.Id = value;
        }

        public virtual short RoleID { get; set; }

        public virtual int? PortalID { get; set; }

        public virtual string FirstName { get; set; }

        public virtual string LastName { get; set; }

        public virtual string UserName { get; set; }

        public virtual string HashedPwd { get; set; }

        public virtual DateTime CreationDate { get; set; }

        public virtual DateTime LastLoginDate { get; set; }

        public virtual bool IsActive { get; set; }

        public virtual short UserLevelID { get; set; }

        public virtual string ProgressCD { get; set; }

        public virtual bool ResetPwd { get; set; }

        public virtual string DomainCD { get; set; }

        #region ToString() Implementation

        public new virtual string ToString()
        {
            var sb = new StringBuilder(8);

            sb.AppendLine("------------User dump starts---------------\n");
            sb.AppendLine("UserID: " + ID);
            sb.AppendLine("ProgressCD: " + (ProgressCD ?? "<NULL>"));
            sb.AppendLine("FirstName: " + (FirstName ?? "<NULL>"));
            sb.AppendLine("LastName: " + (LastName ?? "<NULL>"));
            sb.AppendLine("UserName: " + (UserName ?? "<NULL>"));
            sb.AppendLine("UserLevelID: " + UserLevelID);
            sb.AppendLine("RoleID: " + RoleID);
            sb.AppendLine("DomainCD: " + (DomainCD ?? "<NULL>"));
            sb.AppendLine("CreationDate: " + CreationDate);
            sb.AppendLine("LastLoginDate: " + LastLoginDate);
            sb.AppendLine("IsActive: " + IsActive);
            sb.AppendLine("ResetPwd: " + ResetPwd);
            sb.AppendLine("PortalID: " + (PortalID.HasValue ? PortalID.ToString() : "<NULL>"));
            sb.AppendLine("------------User dump ends---------------\n");

            return sb.ToString();
        }

        #endregion ToString() Implementation
    }
}