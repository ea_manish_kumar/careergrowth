﻿using System;
using System.Text;

namespace DomainModel.User
{
    public class UserAuth : EntityBase<int, UserData>
    {
        #region Constructors

        #endregion

        #region ToString() Implementation

        /// <summary>
        ///     This methods returns a string representation of this class.
        /// </summary>
        /// <returns>A string representation of this class.</returns>
        public new virtual string ToString()
        {
            var sb = new StringBuilder(8);

            sb.AppendLine("------------UserAuth dump starts---------------");
            sb.AppendLine(base.ToString());
            sb.AppendLine("Id: " + Id);
            sb.AppendLine("UserID: " + UserID);
            sb.AppendLine("AuthTypeCD: " + (AuthTypeCD ?? "<NULL>"));
            sb.AppendLine("IsSignedOut: " + (IsSignedOut.HasValue ? IsSignedOut.Value.ToString() : "<NULL>"));
            sb.AppendLine("ExpiredOn: " + (ExpiredOn.HasValue ? ExpiredOn.Value.ToString() : "<NULL>"));
            sb.AppendLine("CreatedOn: " + CreatedOn);
            sb.AppendLine("ModifiedOn: " + ModifiedOn);
            sb.AppendLine("------------UserAuth dump ends---------------");

            return sb.ToString();
        }

        #endregion ToString() Implementation

        #region Property Declarations

        public virtual int UserID { get; set; }
        public virtual string AuthTypeCD { get; set; }
        public virtual bool? IsSignedOut { get; set; }
        public virtual DateTime CreatedOn { get; set; }
        public virtual DateTime ModifiedOn { get; set; }
        public virtual DateTime? ExpiredOn { get; set; }

        #endregion Property Declarations
    }
}