﻿using System;
using System.Text;

namespace DomainModel.User
{
    public class UserOptin : EntityBase<string, UserOptin>
    {
        #region Constructors

        #endregion

        #region ToString() Implementation

        public new virtual string ToString()
        {
            var sb = new StringBuilder(8);

            sb.Append("------------UserOptin dump starts---------------\n");
            sb.AppendLine(base.ToString());
            sb.Append("UserID: " + UserID + "\n");
            sb.Append("OptinID: " + OptinID + "\n");
            sb.Append("Response: " + Response + "\n");
            sb.Append("TimeStamp: " + TimeStamp + "\n");
            sb.Append("------------UserOptin dump ends---------------\n");

            return sb.ToString();
        }

        #endregion ToString() Implementation

        #region Property Declarations

        /// <summary>
        ///     Gets or sets the id.
        /// </summary>
        /// <value>The id.</value>
        public override string Id => UserID + "~" + OptinID;

        /// <summary>
        ///     Private variable to hold the value for property UserID.
        /// </summary>
        private int _UserID;

        /// <summary>
        ///     The UserID
        /// </summary>
        /// <value>The party ID.</value>
        public virtual int UserID
        {
            get => _UserID;
            set => _UserID = value;
        }

        /// <summary>
        ///     Private variable to hold the value for property Response.
        /// </summary>
        private short _response;

        /// <summary>
        ///     The Response value(0/1)
        /// </summary>
        /// <value>The response.</value>
        public virtual short Response
        {
            get => _response;
            set => _response = value;
        }

        /// <summary>
        ///     Private variable to hold the value for property OptinID.
        /// </summary>
        private short _optinID;

        /// <summary>
        ///     The ID of the Optin record
        /// </summary>
        /// <value>The optin ID.</value>
        public virtual short OptinID
        {
            get => _optinID;
            set => _optinID = value;
        }

        /// <summary>
        ///     Private variable to hold the value for property TimeStamp.
        /// </summary>
        private DateTime _timestamp;

        /// <summary>
        ///     The TimeStamp for when this value was set
        /// </summary>
        /// <value>The time stamp.</value>
        public virtual DateTime TimeStamp
        {
            get => _timestamp;
            set => _timestamp = value;
        }

        #endregion Property Declarations
    }
}