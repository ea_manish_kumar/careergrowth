﻿using System;

namespace DomainModel.User
{
    public class UserExtended : EntityBase<int, UserExtended>
    {
        #region Constructors

        #endregion

        #region Property Declarations

        /// <summary>
        ///     Gets or sets the id.
        /// </summary>
        /// <value>The id.</value>
        public virtual int ID
        {
            get => base.Id;
            set => base.Id = value;
        }

        /// <summary>
        ///     Private variable to hold the value for property UserID.
        /// </summary>
        private int _UserID;

        /// <summary>
        ///     The UserID
        /// </summary>
        /// <value>The party ID.</value>
        public virtual int UserID
        {
            get => _UserID;
            set => _UserID = value;
        }

        /// <summary>
        ///     Private variable to hold the value for property Response.
        /// </summary>
        private bool _isExprNotified;

        /// <summary>
        ///     The Response value(0/1)
        /// </summary>
        /// <value>The response.</value>
        public virtual bool IsExprNotified
        {
            get => _isExprNotified;
            set => _isExprNotified = value;
        }


        /// <summary>
        ///     Private variable to hold the value for property TimeStamp.
        /// </summary>
        private DateTime _timestamp;

        /// <summary>
        ///     The TimeStamp for when this value was set
        /// </summary>
        /// <value>The time stamp.</value>
        public virtual DateTime TimeStamp
        {
            get => _timestamp;
            set => _timestamp = value;
        }

        #endregion Property Declarations
    }
}