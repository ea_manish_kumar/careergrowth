﻿using System;
using System.Text;

namespace DomainModel.User
{
    [Serializable]
    public class UserSearchDetailDTO
    {
        #region Constructors

        #endregion

        #region ToString() Implementation

        /// <summary>
        ///     This methods returns a string representation of this class.
        /// </summary>
        /// <returns>A string representation of this class.</returns>
        public override string ToString()
        {
            var sb = new StringBuilder(6);

            sb.AppendLine("------------UserSearchDetailDTO dump starts---------------");
            sb.AppendLine("SubStatus: " + (SubStatus ?? "<NULL>"));
            sb.AppendLine("BillingStatusTypeCD: " + (BillingStatusTypeCD ?? "<NULL>"));
            sb.AppendLine("UserID: " + UserID);
            sb.AppendLine("FirstName: " + (FirstName ?? "<NULL>"));
            sb.AppendLine("LastName: " + (LastName ?? "<NULL>"));
            sb.AppendLine("Email: " + (UserName ?? "<NULL>"));
            sb.AppendLine("CustomerID: " + CustomerID);
            sb.AppendLine("LastLoginDate: " + LastLoginDate);
            sb.AppendLine("BillingSubscriptionID: " +
                          (BillingSubscriptionID.HasValue ? BillingSubscriptionID.ToString() : "<NULL>"));
            sb.AppendLine("------------UserSearchDetailDTO dump ends---------------");

            return sb.ToString();
        }

        #endregion ToString() Implementation

        #region GetClone() Implementation

        /// <summary>
        ///     This methods returns a cloned instance of this class.
        /// </summary>
        /// <returns>A string cloned instance of this class.</returns>
        public virtual UserSearchDetailDTO GetClone()
        {
            var clone = new UserSearchDetailDTO();

            clone.SubStatus = SubStatus;
            clone.BillingStatusTypeCD = BillingStatusTypeCD;
            clone.FirstName = FirstName;
            clone.LastName = LastName;
            clone.UserName = UserName;
            clone.UserID = UserID;
            clone.CustomerID = CustomerID;
            clone.LastLoginDate = LastLoginDate;
            clone.BillingSubscriptionID = BillingSubscriptionID;

            return clone;
        }

        #endregion GetClone() Implementation

        #region Equals() implementation.

        /// <summary>
        ///     Determines whether the specified SKUOrderDetailDTO is equal to the current SKUOrderDetailDTO.
        /// </summary>
        /// <param name="obj">The SKUOrderDetailDTO to compare with the current SKUOrderDetailDTO.</param>
        /// <returns>
        ///     true if the specified SKUOrderDetailDTO is equal to the current SKUOrderDetailDTO; otherwise, false.
        /// </returns>
        /// <remarks>
        ///     Tests whether this and another object are equal in a way that will still pass
        ///     when proxy objects are being used.
        /// </remarks>
        public override bool Equals(object obj)
        {
            return true;
        }

        #endregion

        #region GetHashCode() implementation.

        /// <summary>
        ///     Serves as a hash function for a particular type.
        /// </summary>
        /// <returns>
        ///     A hash code for the current SKUOrderDetailDTO.
        /// </returns>
        public override int GetHashCode()
        {
            return GetType().FullName.GetHashCode();
        }

        #endregion GetHashCode() Implementation

        #region Property Declarations

        public virtual string SubStatus { get; set; }
        public virtual string BillingStatusTypeCD { get; set; }

        public virtual int UserID { get; set; }

        //public virtual string UserID { get; set; }
        //public virtual string OrderID { get; set; }
        public virtual string FirstName { get; set; }
        public virtual string LastName { get; set; }
        public virtual string UserName { get; set; }
        public virtual DateTime LastLoginDate { get; set; }
        public virtual int? BillingSubscriptionID { get; set; }
        public virtual int CustomerID { get; set; }

        #endregion Property Declarations
    }
}