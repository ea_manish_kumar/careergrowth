﻿namespace DomainModel.User
{
    public class Address : EntityBase<int, Address>
    {
        #region ToString() Implementation

        public new virtual string ToString()
        {
            return string.Empty;
        }

        #endregion ToString() Implementation

        #region GetClone() Implementation

        /// <summary>
        ///     This methods returns a cloned instance of this class.
        /// </summary>
        /// <returns>A string cloned instance of this class.</returns>
        public virtual Address GetClone()
        {
            var clone = new Address();

            clone.Id = Id;
            //clone.UserID = this.UserID;
            //clone.AddressTypeCD = this.AddressTypeCD;
            clone.StateProvince = StateProvince;
            clone.CountryCD = CountryCD; //Ref only
            clone.Address1 = Address1;
            clone.Address2 = Address2;
            clone.City = City;
            clone.PostalCode = PostalCode;
            clone.PhoneNumber = PhoneNumber;
            clone.AddressTypeCD = AddressTypeCD;
            return clone;
        }

        #endregion GetClone() Implementation

        #region GetHashCode() implementation.

        public override int GetHashCode()
        {
            return (GetType().FullName + Id).GetHashCode();
        }

        #endregion GetHashCode() Implementation

        #region Equals() implementation.

        /// <summary>
        ///     Determines whether the specified Address is equal to the current Address.
        /// </summary>
        /// <param name="obj">The Address to compare with the current Address.</param>
        /// <returns>
        ///     true if the specified Address is equal to the current Address; otherwise, false.
        /// </returns>
        /// <remarks>
        ///     Tests whether this and another object are equal in a way that will still pass
        ///     when proxy objects are being used.
        /// </remarks>
        public override bool Equals(object obj)
        {
            //Same reference check !
            if (ReferenceEquals(this, obj)) return true;

            //Type check !
            var other = obj as Address;
            if (other == null) return false;

            if (
                    Id == 0 && other.Id ==
                    0 /*| (UserID == 0 && other.UserID == 0) | (String.IsNullOrEmpty(AddressTypeCD) && String.IsNullOrEmpty(other.AddressTypeCD))*/
                )
                //Do a per-property search
                return
                    StateProvince == other.StateProvince &&
                    CountryCD == other.CountryCD &&
                    Address1 == other.Address1 &&
                    Address2 == other.Address2 &&
                    City == other.City &&
                    PostalCode == other.PostalCode;
            return Id == other.Id /*&& UserID == other.UserID && AddressTypeCD == other.AddressTypeCD*/;
        }

        #endregion Equals() Implementation

        #region Constructors

        /// <summary>
        ///     Class constructor...
        /// </summary>
        public Address()
        {
        }

        public Address(int id, string stateProvince, string country, string address1, string address2, string city,
            string postalCode)
        {
            Id = id;

            StateProvince = stateProvince;
            CountryCD = country;
            Address1 = address1;
            Address2 = address2;
            City = city;
            PostalCode = postalCode;
        }

        public Address(int id, string stateProvince, string country, string address1, string address2, string city,
            string postalCode, string phoneNumber)
        {
            Id = id;

            StateProvince = stateProvince;
            CountryCD = country;
            Address1 = address1;
            Address2 = address2;
            City = city;
            PostalCode = postalCode;
            PhoneNumber = phoneNumber;
        }

        #endregion

        #region Property Declarations

        public virtual int UserID { get; set; }

        public virtual string StateProvince { get; set; }

        public virtual string CountryCD { get; set; }

        public virtual string Address1 { get; set; }

        public virtual string Address2 { get; set; }

        public virtual string City { get; set; }

        public virtual string PostalCode { get; set; }

        public virtual string PhoneNumber { get; set; }

        public virtual string AddressTypeCD { get; set; } = "HOME";

        #endregion Property Declarations
    }
}