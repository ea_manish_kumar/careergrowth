﻿using System;
using System.Text;

namespace DomainModel.User
{
    public class UserAuthSession : EntityBase<int, UserData>
    {
        #region Constructors

        #endregion

        #region ToString() Implementation

        /// <summary>
        ///     This methods returns a string representation of this class.
        /// </summary>
        /// <returns>A string representation of this class.</returns>
        public new virtual string ToString()
        {
            var sb = new StringBuilder(8);

            sb.AppendLine("------------UserAuthSession dump starts---------------");
            sb.AppendLine(base.ToString());
            sb.AppendLine("Id: " + Id);
            sb.AppendLine("UserAuthID: " + UserAuthID);
            sb.AppendLine("SessionID: " + SessionID);
            sb.AppendLine("CreatedOn: " + CreatedOn);
            sb.AppendLine("------------UserAuthSession dump ends---------------");

            return sb.ToString();
        }

        #endregion ToString() Implementation

        #region Property Declarations

        public virtual int UserAuthID { get; set; }
        public virtual Guid SessionID { get; set; }
        public virtual DateTime CreatedOn { get; set; }

        #endregion Property Declarations
    }
}