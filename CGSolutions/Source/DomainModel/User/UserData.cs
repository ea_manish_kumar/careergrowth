﻿using System.Text;

namespace DomainModel.User
{
    public class UserData : EntityBase<string, UserData>
    {
        #region ToString() Implementation

        /// <summary>
        ///     This methods returns a string representation of this class.
        /// </summary>
        /// <returns>A string representation of this class.</returns>
        public new virtual string ToString()
        {
            var sb = new StringBuilder(8);

            sb.Append("------------UserData dump starts---------------\n");
            sb.AppendLine(base.ToString());
            sb.Append("UserID: " + Id + "\n");
            sb.Append("DataTypeCD: " + DataTypeCD + "\n");
            sb.Append("Data: " + Data + "\n");
            sb.Append("------------UserData dump ends---------------\n");

            return sb.ToString();
        }

        #endregion ToString() Implementation

        #region Constructors

        /// <summary>
        ///     Default constructor
        /// </summary>
        public UserData()
        {
        }

        /// <summary>
        ///     Initialize UserData with data
        /// </summary>
        /// <param name="userID"></param>
        /// <param name="dataTypeCD"></param>
        /// <param name="value"></param>
        public UserData(int userID, string dataTypeCD, string value)
        {
            UserID = userID;
            DataTypeCD = dataTypeCD;
            Data = value;
        }

        #endregion

        #region Property Declarations

        public virtual int UserID { get; set; }
        public virtual string DataTypeCD { get; set; }
        public virtual string Data { get; set; }

        #endregion Property Declarations
    }
}