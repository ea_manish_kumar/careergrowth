﻿using App.Data;
using App.DomainModel;
using System;
using System.Threading.Tasks;

namespace App.Services
{
    public class UserService : IUserService
    {
        private readonly IUserDB UserDB;

        public UserService(IUserDB userDB)
        {
            UserDB = userDB;
        }
        
        public async Task<bool> UserExists(string username)
        {
            return await UserDB.UserExists(username);
        }

        public async Task<User> Get(string username)
        {
            return await UserDB.Get(username);
        }

        public async Task<User> Get(int id)
        {
            return await UserDB.Get(id);
        }

        public async Task<short> GetRoleId(string role)
        {
            return await UserDB.GetRoleId(role);
        }

        public async Task Save(User user)
        {
            await UserDB.Save(user);
        }

        public async Task Update(User user)
        {
            await UserDB.Update(user);
        }

        public async Task ExpireUserLogin(int userLoginSessionId, string userLoginStamp)
        {
            await UserDB.ExpireUserLogin(userLoginSessionId, userLoginStamp);
        }

        public async Task<UserLogin> SaveUserLogin(int userId, UserLoginType loginType, TimeSpan expiryTime)
        {
            return await UserDB.SaveUserLogin(userId, loginType, expiryTime);
        }

        public async Task SaveUserLoginSession(int userLoginId, int sessionId)
        {
            await UserDB.SaveUserLoginSession(userLoginId, sessionId);
        }

        public async Task<UserToken> GenerateUserToken(int userId, UserTokenType tokenType, TimeSpan expiryTime, Guid uid, string hashedToken)
        {
            return await UserDB.GenerateUserToken(userId, tokenType, expiryTime, uid, hashedToken);
        }

        public async Task<UserToken> GetUserToken(Guid uid, string hashedToken)
        {
            return await UserDB.GetUserToken(uid, hashedToken);
        }

        public async Task SaveUserToken(UserToken userToken)
        {
            await UserDB.SaveUserToken(userToken);
        }

        public async Task<short> GetEventId(string eventName)
        {
            return await UserDB.GetEventId(eventName);
        }
        public async Task<UserEventData> GetUserEvent(int userId)
        {
            return await UserDB.GetUserEvent(userId);
        }
        public async Task SaveUserEvent(UserEventData userEventData)
        {
            await UserDB.SaveUserEvent(userEventData);
        }
    }
}