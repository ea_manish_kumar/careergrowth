﻿using App.DomainModel;
using App.Utility.Constants;
using System;
using System.Threading.Tasks;

namespace App.Services
{
    public interface IUserService
    {
        Task<bool> UserExists(string username);
        Task<User> Get(string username);
        Task<User> Get(int id);
        Task<short> GetRoleId(string role);
        Task Save(User user);
        Task Update(User user);
        Task ExpireUserLogin(int userLoginSessionId, string userLoginStamp);
        Task<UserLogin> SaveUserLogin(int userId, UserLoginType loginType, TimeSpan expiryTime);
        Task SaveUserLoginSession(int userLoginId, int sessionId);
        Task<UserToken> GenerateUserToken(int userId, UserTokenType tokenType, TimeSpan expiryTime, Guid uid, string hashedToken);
        Task<UserToken> GetUserToken(Guid uid, string hashedToken);
        Task SaveUserToken(UserToken userToken);
        Task<short> GetEventId(string eventName);
        Task<UserEventData> GetUserEvent(int userId);
        Task SaveUserEvent(UserEventData userEventData);
    }
}