﻿using App.DomainModel;
using App.Utility.Constants;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace App.Services
{
    public interface IBillingService
    {
        Task<bool> IsValidExportRequest(int userId, int skinId);
        Task<IList<Subscription>> GetAllSubscriptionsByUserId(int userId);
        Task<Subscription> CreateNewSubscription();
        Task<Transaction> AddBillingTransaction();
        Task<IList<PlanType>> GetAllPlanTypes();
        Task<IList<Plan>> GetAllPlansAvailableForThisPortal();
        Task<Plan> GetPlanById(int planId);
        Task<IList<PlanTypeSkin>> GetAllPlanTypeSkins();
        Task<IList<PlanTypeSkin>> GetAllPlanTypeBySkinId(int skinId);
        Task<Order> GetOrder(int orderId);
        Task<Order> GetOrder(int userId, string statusCD);
        Task<IList<OrderItem>> GetOrderItems(int orderId);
        Task<Order> UpdateOrder();
        Task<Order> PlaceOrder(int userId, int planId, int sessionId);
    }
}