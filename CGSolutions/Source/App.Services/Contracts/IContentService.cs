﻿using App.Utility.Constants;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace App.Services
{
    public interface IContentService
    {
        Task<IList<string>> GetJobTitles(string query);

        Task<IList<string>> GetExamples(ContentType contentType, string title);

        Task<IList<string>> GetGenericExamples(ContentType contentType);
    }
}