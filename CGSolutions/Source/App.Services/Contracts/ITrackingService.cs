﻿using App.DomainModel;
using System;
using System.Threading.Tasks;

namespace App.Services
{
    public interface ITrackingService
    {
        Task SaveSession(Session session);
        Task<int?> GetSessionBrowserId(Guid sessionBrowserUID);
        Task<int?> GetSessionId(Guid sessionUID);
        Task SavePageView(string page, Guid sessionUID, int? userId);
        Task<int?> GetReferrerId(string code);
    }
}