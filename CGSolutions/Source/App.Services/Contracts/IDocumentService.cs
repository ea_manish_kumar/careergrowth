﻿using App.DomainModel;
using App.Model;
using App.Model.Documents;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace App.Services
{
    public interface IDocumentService
    {
        Task<IEnumerable<SectionType>> GetSectionTypes();

        Task<IEnumerable<Field>> GetFields();

        Task<Document> GetDocument(int documentId);

        Task SaveDocument(Document document);

        Task<ResponseCode> UpdateSkin(int documentId, string skinCD);

        Task<int> GetDocumentCount(string docTypeCD);

        Task<SectionDTO> GetSection(int documentId, string sectionType);

        Task<ResponseData<SectionDTO>> SaveSection(SectionDTO section);

        Task<ResponseData<SectionDTO>> SortSubSections(SectionDTO section);
    }
}
