﻿using App.Data;
using App.DomainModel;
using System;
using System.Threading.Tasks;

namespace App.Services
{
    public class TrackingService : ITrackingService
    {
        readonly ITrackingDB TrackingDB;

        public TrackingService(ITrackingDB trackingDB)
        {
            TrackingDB = trackingDB;
        }

        public async Task SaveSession(Session session)
        {
            await TrackingDB.SaveSession(session);
        }

        public async Task<int?> GetSessionBrowserId(Guid sessionBrowserUID)
        {
            return await TrackingDB.GetSessionBrowserId(sessionBrowserUID);
        }

        public async Task<int?> GetSessionId(Guid sessionUID)
        {
            return await TrackingDB.GetSessionId(sessionUID);
        }

        public async Task SavePageView(string page, Guid sessionUID, int? userId)
        {
            await TrackingDB.SavePageView(page, sessionUID, userId);
        }

        public async Task<int?> GetReferrerId(string code)
        {
            return await TrackingDB.GetReferrerId(code);
        }
    }
}