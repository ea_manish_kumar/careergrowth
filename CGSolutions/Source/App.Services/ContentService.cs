﻿using App.Data;
using App.Utility.Constants;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace App.Services
{
    public class ContentService : IContentService
    {
        private readonly IContentDB ContentDB;

        public ContentService(IContentDB contentDB)
        {
            ContentDB = contentDB;
        }

        public async Task<IList<string>> GetExamples(ContentType contentType, string title)
        {
            return await ContentDB.GetExamples(contentType, title);
        }

        public async Task<IList<string>> GetGenericExamples(ContentType contentType)
        {
            return await ContentDB.GetGenericExamples(contentType);
        }

        public async Task<IList<string>> GetJobTitles(string query)
        {
            return await ContentDB.GetJobTitles(query);
        }
    }
}