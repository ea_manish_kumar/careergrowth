﻿using App.Data;
using App.DomainModel;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace App.Services
{
    public class BillingService : IBillingService
    {
        private readonly IBillingDB BillingDB;

        public BillingService(IBillingDB billingDB)
        {
            BillingDB = billingDB;
        }

        public async Task<Transaction> AddBillingTransaction()
        {
            return await BillingDB.AddBillingTransaction();
        }

        public async Task<Subscription> CreateNewSubscription()
        {
            return await BillingDB.CreateNewSubscription();
        }

        public async Task<IList<Plan>> GetAllPlansAvailableForThisPortal()
        {
            return await BillingDB.GetAllPlansAvailableForThisPortal();
        }

        public async Task<IList<PlanTypeSkin>> GetAllPlanTypeBySkinId(int skinId)
        {
            return await BillingDB.GetAllPlanTypeBySkinId(skinId);
        }

        public async Task<IList<PlanType>> GetAllPlanTypes()
        {
            return await BillingDB.GetAllPlanTypes();
        }

        public async Task<IList<PlanTypeSkin>> GetAllPlanTypeSkins()
        {
            return await BillingDB.GetAllPlanTypeSkins();
        }

        public async Task<IList<Subscription>> GetAllSubscriptionsByUserId(int userId)
        {
            return await BillingDB.GetAllSubscriptionsByUserId(userId);
        }

        public async Task<Order> GetOrder(int orderId)
        {
            return await BillingDB.GetOrder(orderId);
        }

        public async Task<Order> GetOrder(int userId, string statusCD)
        {
            return await BillingDB.GetOrder(userId, statusCD);
        }

        public async Task<IList<OrderItem>> GetOrderItems(int orderId)
        {
            return await BillingDB.GetOrderItems(orderId);
        }

        public async Task<Plan> GetPlanById(int planId)
        {
            return await BillingDB.GetPlanById(planId);
        }

        public async Task<bool> IsValidExportRequest(int userId, int skinId)
        {
            return await BillingDB.IsValidExportRequest(userId, skinId);
        }

        public async Task<Order> PlaceOrder(int userId, int planId, int sessionId)
        {
            return await BillingDB.PlaceOrder(userId, planId, sessionId);
        }

        public async Task<Order> UpdateOrder()
        {
            return await BillingDB.UpdateOrder();
        }
    }
}