﻿using App.Data;
using App.DomainModel;
using App.Model;
using App.Model.Documents;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace App.Services
{
    public class DocumentService : IDocumentService
    {
        private readonly IDocumentDB DocumentDB;

        public DocumentService(IDocumentDB documentDB)
        {
            DocumentDB = documentDB;
        }

        public Task<IEnumerable<SectionType>> GetSectionTypes()
        {
            return DocumentDB.GetSectionTypes();
        }

        public Task<IEnumerable<Field>> GetFields()
        {
            return DocumentDB.GetFields();
        }

        public Task<Document> GetDocument(int documentId)
        {
            return DocumentDB.GetDocument(documentId);
        }

        public Task SaveDocument(Document document)
        {
            return DocumentDB.SaveDocument(document);
        }

        public Task<ResponseCode> UpdateSkin(int documentId, string skinCD)
        {
            return DocumentDB.UpdateSkin(documentId, skinCD);
        }

        public Task<int> GetDocumentCount(string docTypeCD)
        {
            return DocumentDB.GetDocumentCount(docTypeCD);
        }

        public Task<SectionDTO> GetSection(int documentId, string sectionType)
        {
            return DocumentDB.GetSection(documentId, sectionType);
        }

        public Task<ResponseData<SectionDTO>> SaveSection(SectionDTO section)
        {
            return DocumentDB.SaveSection(section);
        }

        public Task<ResponseData<SectionDTO>> SortSubSections(SectionDTO section)
        {
            return DocumentDB.SortSubSections(section);
        }
    }
}