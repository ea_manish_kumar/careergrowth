﻿using App.DomainModel;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace App.Data
{
    public interface IBillingDB
    {
        Task<bool> IsValidExportRequest(int userId, int skinId);
        Task<IList<Subscription>> GetAllSubscriptionsByUserId(int userId);
        Task<Subscription> CreateNewSubscription();
        Task<Transaction> AddBillingTransaction();
        Task<IList<PlanType>> GetAllPlanTypes();
        Task<IList<Plan>> GetAllPlansAvailableForThisPortal();
        Task<Plan> GetPlanById(int planId);
        Task<IList<PlanTypeSkin>> GetAllPlanTypeSkins();
        Task<IList<PlanTypeSkin>> GetAllPlanTypeBySkinId(int skinId);
        Task<Order> GetOrder(int orderId);
        Task<Order> GetOrder(int userId, string statusCD);
        Task<IList<OrderItem>> GetOrderItems(int orderId);
        Task<Order> UpdateOrder();
        Task<Order> PlaceOrder(int userId, int planId, int sessionId);
    }
}