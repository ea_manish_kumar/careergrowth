﻿using App.Utility.Constants;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace App.Data
{
    public interface IContentDB
    {
        Task<IList<string>> GetJobTitles(string query);
        Task<IList<string>> GetExamples(ContentType contentType, string title);
        Task<IList<string>> GetGenericExamples(ContentType contentType);
    }
}