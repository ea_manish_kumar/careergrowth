﻿using App.Utility;
using App.Utility.Extensions;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Threading.Tasks;

namespace App.Data.EF.Extensions
{
    public static class DbContextExtensions
    {
        public static async Task<IEnumerable<T>> FromCache<T>(this IDbSet<T> dbSet)
               where T : class
        {
            return await dbSet.AsQueryable().FromCache();
        }

        public static async Task<IEnumerable<T>> FromCache<T>(this IQueryable<T> dbSet)
            where T : class
        {
            string key = "EF_Entities_" + typeof(T).Name;
            var cacheData = CacheUtility.Get<IEnumerable<T>>(key);
            if (cacheData.IsNullOrEmpty())
            {
                cacheData = await dbSet.AsNoTracking().ToListAsync();
                CacheUtility.Set(key, cacheData);
            }
            return cacheData;
        }

        public static async Task<T> FromCache<T>(this IDbSet<T> dbSet, Func<T, bool> predicate)
               where T : class
        {
            var cacheData = await dbSet.FromCache();
            var item = cacheData.FirstOrDefault(predicate);
            if (item == default)
            {
                var refreshData = dbSet.AsNoTracking().ToList();
                item = refreshData.FirstOrDefault(predicate);
                if (item != null)
                {
                    string key = "EF_Entities_" + typeof(T).Name;
                    CacheUtility.Set(key, refreshData);
                    return item;
                }
            }
            return default;
        }
    }
}