﻿using App.DomainModel;
using App.Model.Filters;
using System.Data.Entity;
using System.Linq;

namespace App.Data.EF.Queries
{
    internal static class UserQueries
    {
        internal static IQueryable<Document> UserQuery(this IDbSet<Document> dbSet, UserFilter userFilter)
        {
            return dbSet.Where(d => d.UserId.Equals(userFilter.UserId));
        }
        internal static IQueryable<Section> UserQuery(this IDbSet<Section> dbSet, UserFilter userFilter)
        {
            return dbSet.Include(s => s.Document).Where(s => s.Document.UserId.Equals(userFilter.UserId));
        }
    }
}