﻿using App.DomainModel;
using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.ModelConfiguration;

namespace App.Data.EF.Mapping
{
    public class SessionEntityTypeConfiguration : EntityTypeConfiguration<Session>
    {
        public SessionEntityTypeConfiguration()
        {
            ToTable("Sessions");
            HasKey(s => s.Id).Property(t => t.Id).HasColumnName("Id").HasColumnType("int").HasDatabaseGeneratedOption(DatabaseGeneratedOption.Identity);
            Property(s => s.UID).HasColumnName("UID").HasColumnType("uniqueidentifier").IsRequired();

            Property(s => s.SessionBrowserId).HasColumnName("SessionBrowserId").HasColumnType("int").IsRequired();
            Property(s => s.ReferrerId).HasColumnName("ReferrerId").HasColumnType("int").IsOptional();
            Property(s => s.Campaign).HasColumnName("Campaign").HasColumnType("varchar").HasMaxLength(64).IsOptional();
            Property(s => s.Source).HasColumnName("Source").HasColumnType("varchar").HasMaxLength(64).IsOptional();
            Property(s => s.Medium).HasColumnName("Medium").HasColumnType("varchar").HasMaxLength(64).IsOptional();
            Property(s => s.RequestedUrl).HasColumnName("RequestedUrl").HasColumnType("nvarchar").IsOptional();
            Property(s => s.UrlReferrer).HasColumnName("UrlReferrer").HasColumnType("nvarchar").IsOptional();
            Property(s => s.UserAgent).HasColumnName("UserAgent").HasColumnType("nvarchar").IsOptional();
            Property(s => s.IPAddress).HasColumnName("IPAddress").HasColumnType("varchar").HasMaxLength(128).IsOptional();
            Property(s => s.CreatedOnUtc).HasColumnName("CreatedOnUtc").HasColumnType("datetime2").IsOptional();
            
            HasIndex(s => s.UID).IsUnique();
            HasRequired(s => s.SessionBrowser).WithMany().HasForeignKey(s => s.SessionBrowserId);
            HasOptional(s => s.Referrer).WithMany().HasForeignKey(s => s.ReferrerId);
        }
    }
}