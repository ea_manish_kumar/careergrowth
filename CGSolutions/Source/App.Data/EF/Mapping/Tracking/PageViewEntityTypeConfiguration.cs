﻿using App.DomainModel;
using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.ModelConfiguration;

namespace App.Data.EF.Mapping
{
    public class PageViewEntityTypeConfiguration : EntityTypeConfiguration<PageView>
    {
        public PageViewEntityTypeConfiguration()
        {
            ToTable("PageViews");
            HasKey(s => s.Id).Property(t => t.Id).HasColumnName("Id").HasColumnType("bigint").HasDatabaseGeneratedOption(DatabaseGeneratedOption.Identity);
            Property(s => s.PageId).HasColumnName("PageId").HasColumnType("smallint").IsRequired();
            Property(s => s.UserId).HasColumnName("UserId").HasColumnType("int").IsOptional();

            HasRequired(s => s.Page).WithMany().HasForeignKey(s => s.PageId);
        }
    }
}