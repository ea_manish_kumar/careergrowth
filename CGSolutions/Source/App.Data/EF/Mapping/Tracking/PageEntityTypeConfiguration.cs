﻿using App.DomainModel;

namespace App.Data.EF.Mapping
{
    public class PageEntityTypeConfiguration : BaseEntityMasterTypeConfiguration<Page>
    {
        public PageEntityTypeConfiguration()
        {
            ToTable("Pages");
            Property(s => s.SortIndex).HasColumnName("SortIndex").HasColumnType("tinyint").IsOptional();
        }
    }
}