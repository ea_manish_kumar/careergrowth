﻿using App.DomainModel;
using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.ModelConfiguration;

namespace App.Data.EF.Mapping
{
    public class ReferrerEntityTypeConfiguration : EntityTypeConfiguration<Referrer>
    {
        public ReferrerEntityTypeConfiguration()
        {
            ToTable("Referrers");
            HasKey(s => s.Id).Property(t => t.Id).HasColumnName("Id").HasColumnType("int").HasDatabaseGeneratedOption(DatabaseGeneratedOption.Identity);

            Property(s => s.Code).HasColumnName("Code").HasColumnType("varchar").HasMaxLength(16).IsRequired();
            Property(s => s.Name).HasColumnName("Name").HasColumnType("varchar").HasMaxLength(32).IsRequired();
            Property(s => s.Description).HasColumnName("Description").HasColumnType("varchar").HasMaxLength(64).IsOptional();

            HasIndex(s => s.Code).IsUnique();
        }
    }
}