﻿using App.DomainModel;
using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.ModelConfiguration;

namespace App.Data.EF.Mapping
{
    public class SessionBrowserEntityTypeConfiguration : EntityTypeConfiguration<SessionBrowser>
    {
        public SessionBrowserEntityTypeConfiguration()
        {
            ToTable("SessionBrowsers");
            HasKey(s => s.Id).Property(t => t.Id).HasColumnName("Id").HasColumnType("int").HasDatabaseGeneratedOption(DatabaseGeneratedOption.Identity);

            Property(s => s.UID).HasColumnName("UID").HasColumnType("uniqueidentifier").IsRequired();

            HasIndex(s => s.UID).IsUnique();
        }
    }
}