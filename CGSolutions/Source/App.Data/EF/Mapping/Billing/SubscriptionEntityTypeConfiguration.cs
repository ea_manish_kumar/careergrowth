﻿using App.DomainModel;
using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.ModelConfiguration;

namespace App.Data.EF.Mapping
{
    public class SubscriptionEntityTypeConfiguration : EntityTypeConfiguration<Subscription>
    {
        public SubscriptionEntityTypeConfiguration()
        {
            ToTable("Subscriptions");
            HasKey(s => s.Id).Property(t => t.Id).HasColumnName("SubscriptionId").HasColumnType("int").HasDatabaseGeneratedOption(DatabaseGeneratedOption.Identity);
            Property(s => s.PlanTypeID).HasColumnName("PlanTypeId").HasColumnType("smallint").IsOptional();
            Property(s => s.StartDate).HasColumnName("StartDate").HasColumnType("datetime2").IsOptional();
            Property(s => s.ExpireDate).HasColumnName("ExpireDate").HasColumnType("datetime2").IsOptional();
            Property(s => s.OrderID).HasColumnName("OrderID").HasColumnType("int").IsOptional();
            Property(s => s.UserID).HasColumnName("UserID").HasColumnType("int").IsOptional();
            Property(s => s.CreatedOnUtc).HasColumnName("CreatedOnUtc").HasColumnType("datetime2").IsOptional();
            Property(s => s.ModifiedOnUtc).HasColumnName("ModifiedOnUtc").HasColumnType("datetime2").IsOptional();
            Property(s => s.StatusCD).HasColumnName("StatusCD").HasColumnType("varchar").HasMaxLength(10).IsOptional();

            HasOptional(u => u.User).WithMany().HasForeignKey(u => u.UserID);
            HasOptional(u => u.PlanType).WithMany().HasForeignKey(u => u.PlanTypeID);
            HasOptional(u => u.Order).WithMany().HasForeignKey(u => u.OrderID);
        }
    }
}