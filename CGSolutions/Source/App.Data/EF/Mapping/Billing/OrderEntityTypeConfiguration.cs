﻿using App.DomainModel;
using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.ModelConfiguration;

namespace App.Data.EF.Mapping
{
    public class OrderEntityTypeConfiguration : EntityTypeConfiguration<Order>
    {
        public OrderEntityTypeConfiguration()
        {
            ToTable("Orders");
            HasKey(s => s.Id).Property(t => t.Id).HasColumnName("OrderId").HasColumnType("int").HasDatabaseGeneratedOption(DatabaseGeneratedOption.Identity);
            Property(s => s.UserID).HasColumnName("UserID").HasColumnType("int").IsOptional();
            Property(s => s.SessionID).HasColumnName("SessionID").HasColumnType("int").IsRequired();
            Property(s => s.StatusCD).HasColumnName("StatusCD").HasColumnType("varchar").HasMaxLength(10).IsOptional();
            Property(s => s.CreatedOnUtc).HasColumnName("CreatedOnUtc").HasColumnType("datetime2").IsOptional();
            Property(s => s.ModifiedOnUtc).HasColumnName("ModifiedOnUtc").HasColumnType("datetime2").IsOptional();
            Property(s => s.Amount).HasColumnName("Amount").HasColumnType("decimal").IsOptional();

            HasOptional(u => u.User).WithMany().HasForeignKey(u => u.UserID);
        }
    }
}