﻿using App.DomainModel;
using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.ModelConfiguration;

namespace App.Data.EF.Mapping
{
    public class OrderItemEntityTypeConfiguration : EntityTypeConfiguration<OrderItem>
    {
        public OrderItemEntityTypeConfiguration()
        {
            ToTable("OrderItems");
            HasKey(s => s.Id).Property(t => t.Id).HasColumnName("OrderItemId").HasColumnType("int").HasDatabaseGeneratedOption(DatabaseGeneratedOption.Identity);
            Property(s => s.OrderID).HasColumnName("OrderID").HasColumnType("int").IsRequired();
            Property(s => s.PlanID).HasColumnName("PlanID").HasColumnType("int").IsRequired();
            Property(s => s.CreatedOnUtc).HasColumnName("CreatedOnUtc").HasColumnType("datetime2").IsOptional();
            Property(s => s.Price).HasColumnName("Price").HasColumnType("decimal").IsOptional();
        }
    }
}