﻿using App.DomainModel;
using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.ModelConfiguration;

namespace App.Data.EF.Mapping
{
    public class TransactionEntityTypeConfiguration : EntityTypeConfiguration<Transaction>
    {
        public TransactionEntityTypeConfiguration()
        {
            ToTable("Transactions");
            HasKey(s => s.Id).Property(t => t.Id).HasColumnName("TransactionId").HasColumnType("int").HasDatabaseGeneratedOption(DatabaseGeneratedOption.Identity);
            Property(s => s.StatusCD).HasColumnName("StatusCD").HasColumnType("varchar").HasMaxLength(10).IsOptional();
            Property(s => s.SessionID).HasColumnName("SessionID").HasColumnType("int").IsRequired();
            Property(s => s.Amount).HasColumnName("Amount").HasColumnType("decimal").IsOptional();
            Property(s => s.OrderID).HasColumnName("OrderID").HasColumnType("int").IsOptional();
            Property(s => s.UserID).HasColumnName("UserID").HasColumnType("int").IsOptional();
            Property(s => s.IPAddress).HasColumnName("IPAddress").HasColumnType("varchar").HasMaxLength(50).IsOptional();
            Property(s => s.VendorReferenceCD).HasColumnName("VendorReferenceCD").HasColumnType("varchar").HasMaxLength(20).IsOptional();
            Property(s => s.VendorCD).HasColumnName("VendorCD").HasColumnType("varchar").HasMaxLength(20).IsOptional();
            Property(s => s.CreatedOnUtc).HasColumnName("CreatedOnUtc").HasColumnType("datetime2").IsOptional();
            Property(s => s.ModifiedOnUtc).HasColumnName("ModifiedOnUtc").HasColumnType("datetime2").IsOptional();

            HasOptional(u => u.User).WithMany().HasForeignKey(u => u.UserID);
            HasOptional(u => u.Order).WithMany().HasForeignKey(u => u.OrderID);
        }
    }
}