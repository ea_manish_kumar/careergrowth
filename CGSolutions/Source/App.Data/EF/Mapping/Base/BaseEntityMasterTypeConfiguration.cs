﻿using App.DomainModel;
using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.ModelConfiguration;

namespace App.Data.EF.Mapping
{
    public class BaseEntityMasterTypeConfiguration<T> : EntityTypeConfiguration<T>
        where T : BaseEntityMaster
    {
        public BaseEntityMasterTypeConfiguration()
        {
            HasKey(s => s.Id).Property(t => t.Id).HasColumnName("Id").HasColumnType("smallint").HasDatabaseGeneratedOption(DatabaseGeneratedOption.Identity);
            Property(s => s.Name).HasColumnName("Name").HasColumnType("varchar").HasMaxLength(8).IsRequired();
            Property(s => s.Label).HasColumnName("Label").HasColumnType("varchar").HasMaxLength(64).IsRequired();

            HasIndex(s => s.Name).IsUnique();
        }
    }
}