﻿using App.DomainModel;
using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.ModelConfiguration;

namespace App.Data.EF.Mapping
{
    public class DocumentDataEntityTypeConfiguration : EntityTypeConfiguration<DocumentData>
    {
        public DocumentDataEntityTypeConfiguration()
        {
            ToTable("DocumentDatas");
            HasKey(s => s.Id).Property(t => t.Id).HasColumnName("Id").HasColumnType("int").HasDatabaseGeneratedOption(DatabaseGeneratedOption.Identity);
            Property(s => s.SubSectionId).HasColumnName("SubSectionId").HasColumnType("int").IsRequired();
            Property(s => s.FieldId).HasColumnName("FieldId").HasColumnType("smallint").IsRequired();

            HasRequired(s => s.SubSection).WithMany(s => s.DocumentDatas).HasForeignKey(s => s.SubSectionId);
            HasRequired(s => s.Field).WithMany().HasForeignKey(s => s.FieldId);
        }
    }
}