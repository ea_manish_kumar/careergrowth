﻿using App.DomainModel;
using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.ModelConfiguration;

namespace App.Data.EF.Mapping
{
    public class DocumentEntityTypeConfiguration : EntityTypeConfiguration<Document>
    {
        public DocumentEntityTypeConfiguration()
        {
            ToTable("Documents");
            HasKey(s => s.Id).Property(t => t.Id).HasColumnName("Id").HasColumnType("int").HasDatabaseGeneratedOption(DatabaseGeneratedOption.Identity);
            Property(s => s.UserId).HasColumnName("UserId").HasColumnType("int").IsRequired();
            Property(s => s.DocName).HasColumnName("DocName").HasColumnType("nvarchar").HasMaxLength(256).IsOptional();
            Property(s => s.DocTypeCD).HasColumnName("DocTypeCD").HasColumnType("varchar").HasMaxLength(8).IsRequired();
            Property(s => s.SkinCD).HasColumnName("SkinCD").HasColumnType("varchar").HasMaxLength(8).IsRequired();
            Property(s => s.CreatedOnUtc).HasColumnName("CreatedOnUtc").HasColumnType("datetime2").IsRequired();
            Property(s => s.ModifiedOnUtc).HasColumnName("ModifiedOnUtc").HasColumnType("datetime2").IsOptional();

            HasIndex(s => s.UserId);
        }
    }
}