﻿using App.DomainModel;
using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.ModelConfiguration;

namespace App.Data.EF.Mapping
{
    public class SectionEntityTypeConfiguration : EntityTypeConfiguration<Section>
    {
        public SectionEntityTypeConfiguration()
        {
            ToTable("Sections");
            HasKey(s => s.Id).Property(t => t.Id).HasColumnName("Id").HasColumnType("int").HasDatabaseGeneratedOption(DatabaseGeneratedOption.Identity);
            Property(s => s.Label).HasColumnName("Label").HasColumnType("varchar").HasMaxLength(64).IsRequired();
            Property(s => s.DocumentId).HasColumnName("DocumentId").HasColumnType("int").IsRequired();
            Property(s => s.SectionTypeId).HasColumnName("SectionTypeId").HasColumnType("smallint").IsRequired();
            Property(s => s.SortIndex).HasColumnName("SortIndex").HasColumnType("smallint").IsRequired();

            HasRequired(s => s.SectionType).WithMany().HasForeignKey(s => s.SectionTypeId);
            HasRequired(s => s.Document).WithMany(d => d.Sections).HasForeignKey(s => s.DocumentId);
        }
    }
}