﻿using App.DomainModel;
using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.ModelConfiguration;

namespace App.Data.EF.Mapping
{
    public class SubSectionEntityTypeConfiguration : EntityTypeConfiguration<SubSection>
    {
        public SubSectionEntityTypeConfiguration()
        {
            ToTable("SubSections");
            HasKey(s => s.Id).Property(t => t.Id).HasColumnName("Id").HasColumnType("int").HasDatabaseGeneratedOption(DatabaseGeneratedOption.Identity);
            Property(s => s.SectionId).HasColumnName("SectionId").HasColumnType("int").IsRequired();
            Property(s => s.SortIndex).HasColumnName("SortIndex").HasColumnType("smallint").IsRequired();

            HasRequired(s => s.Section).WithMany(s => s.SubSections).HasForeignKey(s => s.SectionId);
        }
    }
}