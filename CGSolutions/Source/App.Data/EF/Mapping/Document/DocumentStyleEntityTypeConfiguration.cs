﻿using App.DomainModel;
using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.ModelConfiguration;

namespace App.Data.EF.Mapping
{
    public class DocumentStyleEntityTypeConfiguration : EntityTypeConfiguration<DocumentStyle>
    {
        public DocumentStyleEntityTypeConfiguration()
        {
            ToTable("DocumentStyles");
            HasKey(s => s.Id).Property(t => t.Id).HasColumnName("Id").HasColumnType("int").HasDatabaseGeneratedOption(DatabaseGeneratedOption.Identity);
            Property(s => s.DocumentId).HasColumnName("DocumentId").HasColumnType("int").IsRequired();
            Property(s => s.StyleId).HasColumnName("StyleId").HasColumnType("smallint").IsRequired();

            HasIndex(s => new { s.DocumentId, s.StyleId });

            HasRequired(s => s.Document).WithMany(d => d.DocumentStyles).HasForeignKey(s => s.DocumentId);
            HasRequired(s => s.Style).WithMany().HasForeignKey(s => s.StyleId);
        }
    }
}