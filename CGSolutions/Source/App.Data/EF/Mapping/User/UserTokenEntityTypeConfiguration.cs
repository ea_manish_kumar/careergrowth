﻿using App.DomainModel;
using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.ModelConfiguration;

namespace App.Data.EF.Mapping
{
    public class UserTokenEntityTypeConfiguration : EntityTypeConfiguration<UserToken>
    {
        public UserTokenEntityTypeConfiguration()
        {
            ToTable("UserTokens");
            HasKey(s => s.Id).Property(t => t.Id).HasColumnName("Id").HasColumnType("int").HasDatabaseGeneratedOption(DatabaseGeneratedOption.Identity);
            Property(s => s.UID).HasColumnName("UID").HasColumnType("uniqueidentifier").IsRequired();
            Property(s => s.UserId).HasColumnName("UserId").HasColumnType("int").IsRequired();
            Property(s => s.TokenType).HasColumnName("TokenType").HasColumnType("tinyint").IsRequired();
            Property(s => s.CreatedOnUtc).HasColumnName("CreatedOnUtc").HasColumnType("datetime2").IsRequired();
            Property(s => s.HashedToken).HasColumnName("HashedToken").HasColumnType("nvarchar").HasMaxLength(512).IsRequired();
            Property(s => s.CreatedOnUtc).HasColumnName("CreatedOnUtc").HasColumnType("datetime2").IsRequired();
            Property(s => s.ExpiryTime).HasColumnName("ExpiryTime").HasColumnType("time").IsRequired();
            Property(s => s.VerifiedOnUtc).HasColumnName("VerifiedOnUtc").HasColumnType("datetime2").IsOptional();

            HasIndex(s => s.UID).IsUnique();
            HasRequired(s => s.User).WithMany().HasForeignKey(s => s.UserId);
        }
    }
}