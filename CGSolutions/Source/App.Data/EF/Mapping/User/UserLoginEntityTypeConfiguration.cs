﻿using App.DomainModel;
using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.ModelConfiguration;

namespace App.Data.EF.Mapping
{
    public class UserLoginEntityTypeConfiguration : EntityTypeConfiguration<UserLogin>
    {
        public UserLoginEntityTypeConfiguration()
        {
            ToTable("UserLogins");
            HasKey(u => u.Id).Property(t => t.Id).HasColumnName("Id").HasColumnType("int").HasDatabaseGeneratedOption(DatabaseGeneratedOption.Identity);

            Property(s => s.UniqueStamp).HasColumnName("UniqueStamp").HasColumnType("nvarchar").HasMaxLength(64).IsRequired();
            Property(u => u.UserId).HasColumnName("UserId").HasColumnType("int").IsRequired();
            Property(u => u.LoginType).HasColumnName("LoginType").HasColumnType("tinyint").IsRequired();
            Property(u => u.CreatedOnUtc).HasColumnName("CreatedOnUtc").HasColumnType("datetime2").IsRequired();
            Property(u => u.ExpiredOnUtc).HasColumnName("ExpiredOnUtc").HasColumnType("datetime2").IsRequired();

            HasRequired(u => u.User).WithMany().HasForeignKey(u => u.UserId);
        }
    }
}