﻿using App.DomainModel;
using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.ModelConfiguration;

namespace App.Data.EF.Mapping
{
    public class UserLoginSessionEntityTypeConfiguration : EntityTypeConfiguration<UserLoginSession>
    {
        public UserLoginSessionEntityTypeConfiguration()
        {
            ToTable("UserLoginSessions");
            HasKey(s => s.Id).Property(t => t.Id).HasColumnName("Id").HasColumnType("int").HasDatabaseGeneratedOption(DatabaseGeneratedOption.Identity);
            
            Property(s => s.UserLoginId).HasColumnName("UserLoginId").HasColumnType("int").IsRequired();
            Property(s => s.SessionId).HasColumnName("SessionId").HasColumnType("int").IsRequired();
            Property(s => s.CreatedOnUtc).HasColumnName("CreatedOnUtc").HasColumnType("datetime2").IsRequired();

            HasIndex(s => new { s.SessionId, s.UserLoginId }).IsUnique();

            HasRequired(u => u.UserLogin).WithMany().HasForeignKey(u => u.UserLoginId);
        }
    }
}