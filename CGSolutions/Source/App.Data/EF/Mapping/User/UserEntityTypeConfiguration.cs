﻿using App.DomainModel;
using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.ModelConfiguration;

namespace App.Data.EF.Mapping
{
    public class UserEntityTypeConfiguration : EntityTypeConfiguration<User>
    {
        public UserEntityTypeConfiguration()
        {
            ToTable("Users");
            HasKey(s => s.Id).Property(t => t.Id).HasColumnName("Id").HasColumnType("int").HasDatabaseGeneratedOption(DatabaseGeneratedOption.Identity);
            Property(s => s.UserName).HasColumnName("UserName").HasColumnType("nvarchar").HasMaxLength(256).IsRequired();
            Property(s => s.RoleId).HasColumnName("RoleId").HasColumnType("smallint").IsRequired();
            Property(s => s.Email).HasColumnName("Email").HasColumnType("nvarchar").HasMaxLength(256).IsRequired();
            Property(s => s.Name).HasColumnName("Name").HasColumnType("varchar").HasMaxLength(128).IsOptional(); 
            Property(s => s.HashedPassword).HasColumnName("HashedPassword").HasColumnType("nvarchar").IsRequired();
            Property(s => s.SecurityStamp).HasColumnName("SecurityStamp").HasColumnType("nvarchar").HasMaxLength(64).IsRequired();
            Property(s => s.CreatedOnUtc).HasColumnName("CreatedOnUtc").HasColumnType("datetime2").IsRequired();
            Property(s => s.LockoutEndDateUTC).HasColumnName("LockoutEndDateUTC").HasColumnType("datetime2").IsOptional();
            Property(s => s.AccessFailedCount).HasColumnName("AccessFailedCount").HasColumnType("int").IsOptional();
            Property(s => s.IsActive).HasColumnName("IsActive").HasColumnType("bit").IsRequired();

            HasIndex(s => s.UserName).IsUnique();
            HasRequired(u => u.Role).WithMany().HasForeignKey(u => u.RoleId);
        }
    }
}