﻿using App.DomainModel;
using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.ModelConfiguration;

namespace App.Data.EF.Mapping
{
    public class UserEventDataEntityTypeConfiguration : EntityTypeConfiguration<UserEventData>
    {
        public UserEventDataEntityTypeConfiguration()
        {
            ToTable("UserEventData");
            HasKey(s => s.Id).Property(t => t.Id).HasColumnName("Id").HasColumnType("int").HasDatabaseGeneratedOption(DatabaseGeneratedOption.Identity);
            Property(s => s.UserId).HasColumnName("UserId").HasColumnType("int").IsRequired();
            Property(s => s.EventId).HasColumnName("EventId").HasColumnType("smallint").IsRequired();
            Property(s => s.EventData).HasColumnName("EventData").HasColumnType("nvarchar").IsRequired();
            Property(s => s.CreatedOnUtc).HasColumnName("CreatedOnUtc").HasColumnType("datetime2").IsRequired();
            Property(s => s.ModifiedOnUtc).HasColumnName("ModifiedOnUtc").HasColumnType("datetime2").IsOptional();

            HasRequired(u => u.User).WithMany().HasForeignKey(u => u.UserId);
            HasRequired(e => e.Event).WithMany().HasForeignKey(e => e.EventId);
        }
    }
}