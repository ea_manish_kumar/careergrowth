﻿using App.DomainModel;
using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.ModelConfiguration;

namespace App.Data.EF.Mapping
{
    public class ContentEntityTypeConfiguration : EntityTypeConfiguration<Content>
    {
        public ContentEntityTypeConfiguration()
        {
            ToTable("Content");
            HasKey(s => s.Id).Property(t => t.Id).HasColumnName("Id").HasColumnType("int").HasDatabaseGeneratedOption(DatabaseGeneratedOption.Identity);
            Property(s => s.OnetSocCode).HasColumnName("OnetSoc_Code").HasColumnType("varchar").HasMaxLength(10).IsRequired();
            Property(s => s.ContentType).HasColumnName("ContentType").HasColumnType("smallint").IsRequired();
            Property(s => s.OccupationId).HasColumnName("OccupationId").HasColumnType("int").IsOptional();
            Property(s => s.Example).HasColumnName("Example").HasColumnType("nvarchar").IsOptional();
            Property(s => s.SourceType).HasColumnName("SourceType").HasColumnType("smallint").IsOptional();

            HasOptional(s => s.Occupation).WithMany().HasForeignKey(s => s.OccupationId);
        }
    }
}