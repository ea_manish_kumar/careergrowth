﻿using App.DomainModel;
using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.ModelConfiguration;

namespace App.Data.EF.Mapping
{
    public class OccupationEntityTypeConfiguration : EntityTypeConfiguration<Occupation>
    {
        public OccupationEntityTypeConfiguration()
        {
            ToTable("Occupations");
            HasKey(s => s.Id).Property(t => t.Id).HasColumnName("Id").HasColumnType("int").HasDatabaseGeneratedOption(DatabaseGeneratedOption.Identity);
            Property(s => s.OnetSocCode).HasColumnName("OnetSoc_Code").HasColumnType("varchar").HasMaxLength(10).IsRequired();
            Property(s => s.Title).HasColumnName("Title").HasColumnType("nvarchar").HasMaxLength(150).IsRequired();
            Property(s => s.Description).HasColumnName("Description").HasColumnType("nvarchar").HasMaxLength(1000).IsOptional();
        }
    }
}