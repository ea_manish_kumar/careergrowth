﻿using App.DomainModel;

namespace App.Data.EF.Mapping
{
    public class UserRoleEntityTypeConfiguration : BaseEntityMasterTypeConfiguration<UserRole>
    {
        public UserRoleEntityTypeConfiguration()
        {
            ToTable("UserRoles");
        }
    }
}