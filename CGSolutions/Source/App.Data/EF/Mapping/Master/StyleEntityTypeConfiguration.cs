﻿using App.DomainModel;

namespace App.Data.EF.Mapping
{
    public class StyleEntityTypeConfiguration : BaseEntityMasterTypeConfiguration<Style>
    {
        public StyleEntityTypeConfiguration()
        {
            ToTable("Styles");
        }
    }
}