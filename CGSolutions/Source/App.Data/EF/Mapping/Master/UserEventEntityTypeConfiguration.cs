﻿using App.DomainModel;

namespace App.Data.EF.Mapping
{
    public class UserEventEntityTypeConfiguration : BaseEntityMasterTypeConfiguration<UserEvent>
    {
        public UserEventEntityTypeConfiguration()
        {
            ToTable("UserEvents");
        }
    }
}