﻿using App.DomainModel;
using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.ModelConfiguration;

namespace App.Data.EF.Mapping
{
    public class FeatureEntityTypeConfiguration : EntityTypeConfiguration<Feature>
    {
        public FeatureEntityTypeConfiguration()
        {
            ToTable("Features");
            HasKey(s => s.Id).Property(t => t.Id).HasColumnName("FeatureId").HasColumnType("smallint").HasDatabaseGeneratedOption(DatabaseGeneratedOption.Identity);
        }
    }
}