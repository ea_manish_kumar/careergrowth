﻿using App.DomainModel;
using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.ModelConfiguration;

namespace App.Data.EF.Mapping
{
    public class PlanEntityTypeConfiguration : EntityTypeConfiguration<Plan>
    {
        public PlanEntityTypeConfiguration()
        {
            ToTable("Plans");
            HasKey(s => s.Id).Property(t => t.Id).HasColumnName("PlanId").HasColumnType("int").HasDatabaseGeneratedOption(DatabaseGeneratedOption.Identity);
            Property(s => s.PlanTypeID).HasColumnName("PlanTypeID").HasColumnType("smallint").IsRequired();
            Property(s => s.CurrencyCD).HasColumnName("CurrencyCD").HasColumnType("varchar").HasMaxLength(10).IsRequired();
            Property(s => s.CountryCD).HasColumnName("CountryCD").HasColumnType("varchar").HasMaxLength(10).IsRequired();
            Property(s => s.Label).HasColumnName("Label").HasColumnType("varchar").HasMaxLength(50).IsOptional();
            Property(s => s.ValidityDays).HasColumnName("ValidityDays").HasColumnType("int").IsOptional();
            Property(s => s.UnitPrice).HasColumnName("UnitPrice").HasColumnType("decimal").IsOptional();

            HasRequired(p => p.PlanType).WithMany().HasForeignKey(p => p.PlanTypeID);
        }
    }
}