﻿using App.DomainModel;

namespace App.Data.EF.Mapping
{
    public class PlanTypeEntityTypeConfiguration : BaseEntityMasterTypeConfiguration<PlanType>
    {
        public PlanTypeEntityTypeConfiguration()
        {
            ToTable("PlanType");
            Property(s => s.WaterMarkEnabled).HasColumnName("WaterMarkEnabled").HasColumnType("bit").IsOptional();
        }
    }
}