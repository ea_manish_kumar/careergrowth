﻿using App.DomainModel;

namespace App.Data.EF.Mapping
{
    public class FieldEntityTypeConfiguration : BaseEntityMasterTypeConfiguration<Field>
    {
        public FieldEntityTypeConfiguration()
        {
            ToTable("Fields");
        }
    }
}