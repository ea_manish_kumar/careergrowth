﻿using App.DomainModel;

namespace App.Data.EF.Mapping
{
    public class SkinEntityTypeConfiguration : BaseEntityMasterTypeConfiguration<Skin>
    {
        public SkinEntityTypeConfiguration()
        {
            ToTable("Skins");
        }
    }
}