﻿using App.DomainModel;
using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.ModelConfiguration;

namespace App.Data.EF.Mapping
{
    public class PlanTypeSkinEntityTypeConfiguration : EntityTypeConfiguration<PlanTypeSkin>
    {
        public PlanTypeSkinEntityTypeConfiguration()
        {
            ToTable("PlanTypeSkin");
            HasKey(s => s.Id).Property(t => t.Id).HasColumnName("PlanTypeSkinId").HasColumnType("int").HasDatabaseGeneratedOption(DatabaseGeneratedOption.Identity);
            Property(s => s.PlanTypeID).HasColumnName("PlanTypeId").HasColumnType("smallint").IsRequired();
            Property(s => s.SkinID).HasColumnName("SkinID").HasColumnType("smallint").IsRequired();
            Property(s => s.WaterMarkEnabled).HasColumnName("WaterMarkEnabled").HasColumnType("bit").IsOptional();

            HasRequired(p => p.PlanType).WithMany().HasForeignKey(p => p.PlanTypeID);
            HasRequired(p => p.Skin).WithMany().HasForeignKey(p => p.SkinID);
        }
    }
}