﻿using App.DomainModel;
using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.ModelConfiguration;

namespace App.Data.EF.Mapping
{
    public class PlanTypeFeatureEntityTypeConfiguration : EntityTypeConfiguration<PlanTypeFeature>
    {
        public PlanTypeFeatureEntityTypeConfiguration()
        {
            ToTable("PlanTypeFeatures");
            HasKey(s => s.Id).Property(t => t.Id).HasColumnName("PlanTypeFeatureID").HasColumnType("int").HasDatabaseGeneratedOption(DatabaseGeneratedOption.Identity);
            Property(s => s.PlanTypeID).HasColumnName("PlanTypeID").HasColumnType("smallint").IsRequired();
            Property(s => s.FeatureID).HasColumnName("FeatureID").HasColumnType("smallint").IsRequired();

            HasRequired(u => u.PlanType).WithMany().HasForeignKey(u => u.PlanTypeID);
            HasRequired(u => u.Feature).WithMany().HasForeignKey(u => u.FeatureID);
        }
    }
}