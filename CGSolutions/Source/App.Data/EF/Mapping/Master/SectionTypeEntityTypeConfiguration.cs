﻿using App.DomainModel;

namespace App.Data.EF.Mapping
{
    public class SectionTypeEntityTypeConfiguration : BaseEntityMasterTypeConfiguration<SectionType>
    {
        public SectionTypeEntityTypeConfiguration()
        {

        }
    }
}