﻿using App.Data.EF.Configuration;
using App.DomainModel;
using System.Data.Entity;

namespace App.Data.EF
{
    public class ApplicationDbContext : DbContext
    {
        public ApplicationDbContext() : base("ConnStringEF")
        {
            Configuration.LazyLoadingEnabled = false;
            Configuration.ProxyCreationEnabled = false;
            Configuration.AutoDetectChangesEnabled = true;
            Configuration.ValidateOnSaveEnabled = false;
            Database.SetInitializer(new MigrateDatabaseToLatestVersion<ApplicationDbContext, ApplicationDbConfiguration>());
            //Database.SetInitializer<RequestTrackingDbContext>(null);
            //Database.Log = message => System.Diagnostics.Trace.WriteLine(message);
        }

        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            modelBuilder.Conventions.Remove<System.Data.Entity.ModelConfiguration.Conventions.PluralizingTableNameConvention>();

            modelBuilder.Ignore<BaseEntity>();
            modelBuilder.Ignore<BaseEntityLong>();
            modelBuilder.Ignore<BaseEntityGuid>();
            modelBuilder.Ignore<BaseEntityMaster>();
            
            modelBuilder.Configurations.AddFromAssembly(System.Reflection.Assembly.GetExecutingAssembly());
            modelBuilder.HasDefaultSchema("EF");

            base.OnModelCreating(modelBuilder);
        }
    }
}
