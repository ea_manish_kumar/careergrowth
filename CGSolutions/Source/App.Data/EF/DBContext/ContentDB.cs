﻿using App.Data.EF.Extensions;
using App.DomainModel;
using App.Utility.Constants;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Threading.Tasks;

namespace App.Data.EF
{
    public class ContentDB : IContentDB
    {
        private readonly ApplicationDbContext ApplicationDbContext;

        public ContentDB(ApplicationDbContext applicationDbContext)
        {
            ApplicationDbContext = applicationDbContext;
        }

        public async Task<IList<string>> GetExamples(ContentType contentType, string title)
        {
            var examplesQuery = ApplicationDbContext.Set<Content>().Include(c => c.Occupation)
                        .Where(x => x.ContentType == contentType && x.Occupation.Title.Equals(title));
            return await examplesQuery.Select(x => x.Example).Take(50).ToListAsync();
        }

        public async Task<IList<string>> GetGenericExamples(ContentType contentType)
        {
            var examplesQuery = ApplicationDbContext.Set<Content>()
                        .Where(x => x.ContentType == contentType);
            return await examplesQuery.Select(x => x.Example).Take(50).ToListAsync();
        }

        public async Task<IList<string>> GetJobTitles(string query)
        {
            var examplesCache = await ApplicationDbContext.Set<JobTitle>().FromCache();
            return examplesCache.Where(x => x.Title.Contains(query)).Select(x => x.Title).Take(50).ToList();
        }
    }
}