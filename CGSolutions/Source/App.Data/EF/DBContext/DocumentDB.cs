﻿using App.Data.EF.Extensions;
using App.Data.EF.Queries;
using App.DomainModel;
using App.Model;
using App.Model.Documents;
using App.Model.Filters;
using App.Utility;
using App.Utility.Extensions;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Threading.Tasks;

namespace App.Data.EF
{
    public class DocumentDB : IDocumentDB
    {
        private readonly ApplicationDbContext ApplicationDbContext;
        readonly UserFilter UserFilter;

        public DocumentDB(ApplicationDbContext applicationDbContext, UserFilter userFilter)
        {
            ApplicationDbContext = applicationDbContext;
            UserFilter = userFilter;
        }

        public async Task<IEnumerable<SectionType>> GetSectionTypes()
        {
            return await ApplicationDbContext.Set<SectionType>().FromCache();
        }

        public async Task<IEnumerable<Field>> GetFields()
        {
            return await ApplicationDbContext.Set<Field>().FromCache();
        }

        public async Task<Document> GetDocument(int documentId)
        {
            return await ApplicationDbContext.Set<Document>().UserQuery(UserFilter).Where(d => d.Id.Equals(documentId)).FirstOrDefaultAsync();
        }

        public async Task SaveDocument(Document document)
        {
            document.CreatedOnUtc = DateTime.UtcNow;
            document.UserId = UserFilter.UserId;
            ApplicationDbContext.Set<Document>().Add(document);
            await ApplicationDbContext.SaveChangesAsync();
        }

        public async Task<ResponseCode> UpdateSkin(int documentId, string skinCD)
        {
            var documentExits = await ApplicationDbContext.Set<Document>().UserQuery(UserFilter).AnyAsync(d => d.Id.Equals(documentId));
            if (documentExits)
            {
                Document document = new Document { Id = documentId, SkinCD = skinCD, ModifiedOnUtc = DateTime.UtcNow };
                ApplicationDbContext.Set<Document>().Attach(document);
                ApplicationDbContext.Entry(document).Property(ts => ts.SkinCD).IsModified = true;
                ApplicationDbContext.Entry(document).Property(ts => ts.ModifiedOnUtc).IsModified = true;

                await ApplicationDbContext.SaveChangesAsync();
                ApplicationDbContext.Entry(document).State = EntityState.Detached;

                return ResponseCode.Ok;
            }
            else
                return ResponseCode.NotExists;
        }

        public async Task<int> GetDocumentCount(string docTypeCD)
        {
            return await ApplicationDbContext.Set<Document>().UserQuery(UserFilter).CountAsync(d => d.DocTypeCD.Equals(docTypeCD));
        }

        public async Task<SectionDTO> GetSection(int documentId, string sectionType)
        {
            var sectionTypes = await GetSectionTypes();
            if (sectionTypes.IsNotNullOrEmpty())
            {
                var secType = sectionTypes.FirstOrDefault(s => Strings.Equals(s.Name, sectionType));
                if (secType != null)
                {
                    var dbSection = await ApplicationDbContext.Set<Section>().UserQuery(UserFilter)
                                    .Include(s => s.SubSections.Select(sb => sb.DocumentDatas))
                                    .Where(s => s.DocumentId.Equals(documentId) && s.SectionTypeId.Equals(secType.Id))
                                    .FirstOrDefaultAsync();
                    if (dbSection != null)
                    {
                        var fields = await GetFields();
                        var section = new SectionDTO();
                        section.Initialize(dbSection, sectionTypes, fields);
                        return section;
                    }
                }
            }
            return null;
        }

        public async Task<ResponseData<SectionDTO>> SaveSection(SectionDTO section)
        {
            if (section == null)
                return new ResponseData<SectionDTO> { ResponseCode = ResponseCode.BadRequest };

            var sectionTypes = await GetSectionTypes();
            var fields = await GetFields();
            section.Initialize(sectionTypes, fields);

            if (!section.IsValid())
                return new ResponseData<SectionDTO> { ResponseCode = ResponseCode.BadRequest };

            if (section.Id > 0)
            {
                var dbSection = await ApplicationDbContext.Set<Section>().UserQuery(UserFilter)
                                .Include(s => s.SubSections.Select(sb => sb.DocumentDatas))
                                .Where(s => s.Id.Equals(section.Id) && s.DocumentId.Equals(section.DocumentId))
                                .FirstOrDefaultAsync();
                if (dbSection != null)
                {
                    Merge(dbSection, section);
                    dbSection.Document.ModifiedOnUtc = DateTime.UtcNow;
                    await ApplicationDbContext.SaveChangesAsync();
                    var savedSection = new SectionDTO();
                    savedSection.Initialize(dbSection, sectionTypes, fields);
                    return new ResponseData<SectionDTO> { Data = savedSection, ResponseCode = ResponseCode.Ok };
                }
                else
                    return new ResponseData<SectionDTO> { ResponseCode = ResponseCode.NotExists };
            }
            else
            {
                var document = await ApplicationDbContext.Set<Document>().UserQuery(UserFilter).FirstOrDefaultAsync(d => d.Id.Equals(section.DocumentId));
                if (document != null)
                {
                    var sectionEntity = section.ToSection();
                    ApplicationDbContext.Set<Section>().Add(sectionEntity);
                    document.ModifiedOnUtc = DateTime.UtcNow;
                    await ApplicationDbContext.SaveChangesAsync();
                    var savedSection = new SectionDTO();
                    savedSection.Initialize(sectionEntity, sectionTypes, fields);
                    return new ResponseData<SectionDTO> { Data = savedSection, ResponseCode = ResponseCode.Ok };
                }
                else
                    return new ResponseData<SectionDTO> { ResponseCode = ResponseCode.NotExists };
            }
        }

        public async Task<ResponseData<SectionDTO>> SortSubSections(SectionDTO section)
        {
            if (section == null || !section.IsValidForSubSectionSorting())
                return new ResponseData<SectionDTO> { ResponseCode = ResponseCode.BadRequest };

            //var subSectionIDs = section.SubSections.Select(sb => sb.Id);
            var dbSection = await ApplicationDbContext.Set<Section>().UserQuery(UserFilter)
                            .Include(s => s.SubSections)
                            .Where(s => s.Id.Equals(section.Id) && s.DocumentId.Equals(section.DocumentId))
                            .FirstOrDefaultAsync();
            MergeSorting(dbSection, section);
            dbSection.Document.ModifiedOnUtc = DateTime.UtcNow;
            await ApplicationDbContext.SaveChangesAsync();
            var savedSection = new SectionDTO();
            savedSection.Initialize(dbSection);
            return new ResponseData<SectionDTO> { Data = savedSection, ResponseCode = ResponseCode.Ok };
        }

        #region Private Methods
        private void Merge(Section dbSection, SectionDTO section)
        {
            if (dbSection != null && section != null && section.SubSections.IsNotNullOrEmpty())
            {
                foreach (var subSection in section.SubSections)
                {
                    if (subSection.Id > 0)
                    {
                        if (subSection.IsDeleted || subSection.IsEmpty)
                        {
                            var subSectionToBeDeleted = dbSection.SubSections.FirstOrDefault(sb => sb.Id.Equals(subSection.Id));
                            if (subSectionToBeDeleted != null)
                                dbSection.SubSections.Remove(subSectionToBeDeleted);
                        }
                        else
                        {
                            var dbSubSection = dbSection.SubSections.FirstOrDefault(sb => sb.Id.Equals(subSection.Id));
                            if (dbSubSection != null)
                                Merge(dbSubSection, subSection);
                        }
                    }
                    else if (!subSection.IsEmpty)
                    {
                        var newSubSection = subSection.ToSubSection();
                        newSubSection.SortIndex = Convert.ToInt16(dbSection.SubSections.Count);
                        dbSection.SubSections.Add(newSubSection);
                    }
                }
            }
        }
        private void MergeSorting(Section dbSection, SectionDTO section)
        {
            if (dbSection != null && dbSection.SubSections.IsNotNullOrEmpty() && section != null && section.SubSections.IsNotNullOrEmpty())
            {
                foreach (var subSection in section.SubSections)
                {
                    var dbSubSection = dbSection.SubSections.FirstOrDefault(sb => sb.Id.Equals(subSection.Id));
                    if (dbSubSection != null && !dbSubSection.SortIndex.Equals(subSection.SortIndex))
                        dbSubSection.SortIndex = subSection.SortIndex;
                }
            }
        }
        private void Merge(SubSection dbSubSection, SubSectionDTO subSection)
        {
            if (dbSubSection != null && subSection != null)
            {
                foreach (var documentData in subSection.DocumentDatas)
                {
                    if (documentData.Id > 0)
                    {
                        var dbDocumentData = dbSubSection.DocumentDatas.FirstOrDefault(sb => sb.Id.Equals(documentData.Id));
                        if (dbDocumentData != null && !Strings.Equals(dbDocumentData.Data, documentData.Value))
                            dbDocumentData.Data = documentData.Value;
                    }
                    else if (Strings.IsNotNullOrEmpty(documentData.Value))
                        dbSubSection.DocumentDatas.Add(new DocumentData { FieldId = documentData.FieldId, SubSectionId = dbSubSection.Id });
                }
            }
        }
        #endregion
    }
}