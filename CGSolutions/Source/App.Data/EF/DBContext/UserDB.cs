﻿using App.Data.EF.Extensions;
using App.DomainModel;
using App.Utility.Extensions;
using System;
using System.Data.Entity;
using System.Linq;
using System.Threading.Tasks;

namespace App.Data.EF
{
    public class UserDB : IUserDB
    {
        private readonly ApplicationDbContext ApplicationDbContext;

        public UserDB(ApplicationDbContext applicationDbContext)
        {
            ApplicationDbContext = applicationDbContext;
        }


        public async Task<bool> UserExists(string username)
        {
            return await ApplicationDbContext.Set<User>().AnyAsync(u => u.UserName.Equals(username));
        }

        public async Task<User> Get(string username)
        {
            return await ApplicationDbContext.Set<User>().FirstOrDefaultAsync(u => u.UserName.Equals(username));
        }

        public async Task<User> Get(int id)
        {
            return await ApplicationDbContext.Set<User>().FindAsync(id);
        }

        public async Task<short> GetRoleId(string role)
        {
            var cachedRoles = await ApplicationDbContext.Set<UserRole>().FromCache();
            if (cachedRoles.IsNotNullOrEmpty())
                return cachedRoles.Where(r => string.Equals(role, r.Name, StringComparison.OrdinalIgnoreCase)).Select(r => r.Id).FirstOrDefault();
            return default;
        }

        public async Task Save(User user)
        {
            user.CreatedOnUtc = DateTime.UtcNow;
            user.IsActive = true;
            ApplicationDbContext.Entry(user).State = EntityState.Added;
            await ApplicationDbContext.SaveChangesAsync();
        }

        public async Task Update(User user)
        {
            ApplicationDbContext.Entry(user).State = EntityState.Modified;
            await ApplicationDbContext.SaveChangesAsync();
        }

        public async Task ExpireUserLogin(int userLoginId, string userLoginStamp)
        {
            var userLogin = await ApplicationDbContext.Set<UserLogin>().FindAsync(userLoginId);
            if (userLogin != null && string.Equals(userLoginStamp, userLogin.UniqueStamp, StringComparison.Ordinal))
            {
                userLogin.ExpiredOnUtc = DateTime.UtcNow;
                await ApplicationDbContext.SaveChangesAsync();
            }
        }
        public async Task<UserLogin> SaveUserLogin(int userId, UserLoginType loginType, TimeSpan expiryTime)
        {
            var userLogin = new UserLogin
            {
                UserId = userId,
                LoginType = loginType,
                UniqueStamp = Guid.NewGuid().ToString(),
                CreatedOnUtc = DateTime.UtcNow,
                ExpiredOnUtc = DateTime.UtcNow.Add(expiryTime)
            };
            ApplicationDbContext.Set<UserLogin>().Add(userLogin);
            await ApplicationDbContext.SaveChangesAsync();
            return userLogin;
        }
        public async Task SaveUserLoginSession(int userLoginId, int sessionId)
        {
            var userSession = new UserLoginSession { UserLoginId = userLoginId, SessionId = sessionId, CreatedOnUtc = DateTime.UtcNow };
            ApplicationDbContext.Set<UserLoginSession>().Add(userSession);
            await ApplicationDbContext.SaveChangesAsync();
        }
        public async Task<bool> ValidateAndExtendUserLogin(int userLoginId, TimeSpan extendTimeSpan)
        {
            var currentUtc = DateTime.UtcNow;
            var userLogin = await ApplicationDbContext.Set<UserLogin>().FindAsync(userLoginId);
            if (userLogin != null && currentUtc < userLogin.ExpiredOnUtc)
            {
                var issuedUtc = userLogin.ExpiredOnUtc.Add(extendTimeSpan.Negate());
                var timeElapsed = currentUtc.Subtract(issuedUtc);
                var timeRemaining = userLogin.ExpiredOnUtc.Subtract(currentUtc);
                if (timeElapsed > timeRemaining)
                {
                    userLogin.ExpiredOnUtc = currentUtc.Add(extendTimeSpan);
                    await ApplicationDbContext.SaveChangesAsync();
                }
                return true;
            }
            return false;
        }

        public async Task<UserToken> GenerateUserToken(int userId, UserTokenType tokenType, TimeSpan expiryTime, Guid uid, string hashedToken)
        {
            var userToken = new UserToken
            {
                UserId = userId,
                UID = uid,
                TokenType = tokenType,
                HashedToken = hashedToken,
                ExpiryTime = expiryTime,
                CreatedOnUtc = DateTime.UtcNow
            };
            ApplicationDbContext.Set<UserToken>().Add(userToken);
            await ApplicationDbContext.SaveChangesAsync();
            return userToken;
        }

        public async Task<UserToken> GetUserToken(Guid uid, string hashedToken)
        {
            return await ApplicationDbContext.Set<UserToken>().Include(u => u.User).FirstOrDefaultAsync(u => u.UID == uid && u.HashedToken.Equals(hashedToken) && u.VerifiedOnUtc == null);
        }

        public async Task SaveUserToken(UserToken userToken)
        {
            ApplicationDbContext.Entry(userToken).State = EntityState.Modified;
            ApplicationDbContext.Entry(userToken.User).State = EntityState.Modified;
            await ApplicationDbContext.SaveChangesAsync();
        }
        public async Task<short> GetEventId(string eventName)
        {
            var cachedEvents = await ApplicationDbContext.Set<UserEvent>().FromCache();
            if (cachedEvents.IsNotNullOrEmpty())
                return cachedEvents.FirstOrDefault(x => string.Equals(eventName, x.Name, StringComparison.OrdinalIgnoreCase)).Id;
            return default;
        }

        public async Task<UserEventData> GetUserEvent(int userId)
        {
            return await ApplicationDbContext.Set<UserEventData>().FirstOrDefaultAsync(u => u.UserId == userId);
        }

        public async Task SaveUserEvent(UserEventData userEventData)
        {
            ApplicationDbContext.Set<UserEventData>().Add(userEventData);
            await ApplicationDbContext.SaveChangesAsync();
        }
    }
}