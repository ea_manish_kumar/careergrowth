﻿using App.DomainModel;
using CommonModules;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Threading.Tasks;

namespace App.Data.EF
{
    public class BillingDB : IBillingDB
    {
        private readonly ApplicationDbContext ApplicationDbContext;

        public BillingDB(ApplicationDbContext applicationDbContext)
        {
            ApplicationDbContext = applicationDbContext;
        }

        //TODO: Check this function's use case
        public async Task<Transaction> AddBillingTransaction()
        {
            Transaction transaction = new Transaction();
            transaction.CreatedOnUtc = DateTime.UtcNow;
            ApplicationDbContext.Set<Transaction>().Add(transaction);
            await ApplicationDbContext.SaveChangesAsync();
            return transaction;
        }

        //TODO: Check this function's use case
        public async Task<Subscription> CreateNewSubscription()
        {
            Subscription subscription = new Subscription();
            subscription.CreatedOnUtc = DateTime.UtcNow;
            ApplicationDbContext.Set<Subscription>().Add(subscription);
            await ApplicationDbContext.SaveChangesAsync();
            return subscription;
        }

        //TODO: Discuss the use case of Portal, No Portal functionality as of now
        public async Task<IList<Plan>> GetAllPlansAvailableForThisPortal()
        {
            return await ApplicationDbContext.Set<Plan>().ToListAsync();
        }

        public async Task<IList<PlanTypeSkin>> GetAllPlanTypeBySkinId(int skinId)
        {
            return await ApplicationDbContext.Set<PlanTypeSkin>().Where(x => x.SkinID == skinId).ToListAsync();
        }

        public async Task<IList<PlanType>> GetAllPlanTypes()
        {
            return await ApplicationDbContext.Set<PlanType>().ToListAsync();
        }

        public async Task<IList<PlanTypeSkin>> GetAllPlanTypeSkins()
        {
            return await ApplicationDbContext.Set<PlanTypeSkin>().ToListAsync();
        }

        public async Task<IList<Subscription>> GetAllSubscriptionsByUserId(int userId)
        {
           return await ApplicationDbContext.Set<Subscription>().Where(x => x.UserID == userId).ToListAsync();
        }

        public async Task<Order> GetOrder(int orderId)
        {
            return await ApplicationDbContext.Set<Order>().FirstOrDefaultAsync(x => x.Id == orderId);
        }

        public async Task<Order> GetOrder(int userId, string statusCD)
        {
            return await ApplicationDbContext.Set<Order>().FirstOrDefaultAsync(x => x.UserID == userId && 
                                                                        x.StatusCD.Equals(statusCD, StringComparison.InvariantCultureIgnoreCase));
        }

        public async Task<IList<OrderItem>> GetOrderItems(int orderId)
        {
            return await ApplicationDbContext.Set<OrderItem>().Where(x => x.OrderID == orderId).ToListAsync();
        }

        public async Task<Plan> GetPlanById(int planId)
        {
            return await ApplicationDbContext.Set<Plan>().FirstOrDefaultAsync(x => x.PlanTypeID == planId);
        }

        public async Task<bool> IsValidExportRequest(int userId, int skinId)
        {
            var subscriptions = await GetAllSubscriptionsByUserId(userId);
            bool isValidRequest = false;
            if (subscriptions.IsAny(x => x.ExpireDate >= DateTime.Now))
            {
                var planTypeSkins = await GetAllPlanTypeSkins();
                var activeSubscription = subscriptions.Where(x => x.ExpireDate >= DateTime.Now)
                    .OrderByDescending(x => x.ExpireDate).First();
                 isValidRequest = planTypeSkins.Any(x => x.PlanTypeID == activeSubscription.PlanTypeID && x.SkinID == skinId);
            }

            return isValidRequest;
        }

        public async Task<Order> PlaceOrder(int userId, int planId, int sessionId)
        {
            Order order = null;
            var plan = GetPlanById(planId);
            if (plan != null)
            {
                var freshOrder = true;
                order = await GetOrder(userId, OrderStatusTypeCD.Pending);
                if (order == null)
                    order = new Order();
                else
                    freshOrder = false;
                order.ModifiedOnUtc = DateTime.UtcNow;
                order.StatusCD = OrderStatusTypeCD.Pending;
                order.UserID = userId;
                order.SessionID = sessionId;
                order.Amount = plan.Result.UnitPrice;

                //if (order.Id > 0)
                //{
                //    OrderItem orderItem = null;
                //    IList<OrderItem> orderitems = null;
                //    if (!freshOrder)
                //        orderitems = GetOrderItems(order.Id);
                //    if (orderitems.IsAny())
                //        orderItem = orderitems.OrderByDescending(o => o.CreationDate).First();
                //    else
                //        orderItem = new OrderItem();
                //    orderItem.Order = order;
                //    orderItem.SKU = sku;
                //    orderItem.Quantity = 1;
                //    orderItem.Descriptor = sku.Description;
                //    orderItem.UnitPrice = sku.UnitPrice;
                //    orderItem.CreatedOn = DateTime.Now;
                //    orderItem.ModifiedOn = DateTime.Now;
                //    orderItem.StatusCD = BillingStatusTypeCD.Pending;
                //    BillingFactory.OrderSKUDAL.SaveOrUpdate(orderItem, true);
                //}

                ApplicationDbContext.Set<Order>().Add(order);
                await ApplicationDbContext.SaveChangesAsync();
            }
            return order;
        }
        
        //TODO: The update details would actually flow from order controller
        public async Task<Order> UpdateOrder()
        {
            Order order = new Order();
            ApplicationDbContext.Set<Order>().Add(order);
            await ApplicationDbContext.SaveChangesAsync();
            return order;
        }
    }
}