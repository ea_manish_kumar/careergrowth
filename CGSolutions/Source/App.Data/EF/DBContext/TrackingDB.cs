﻿using App.DomainModel;
using App.Data.EF.Extensions;
using App.Utility.Extensions;
using System;
using System.Data.Entity;
using System.Linq;
using System.Threading.Tasks;

namespace App.Data.EF
{
    public class TrackingDB : ITrackingDB
    {
        readonly ApplicationDbContext ApplicationDbContext;

        public TrackingDB(ApplicationDbContext applicationDbContext)
        {
            ApplicationDbContext = applicationDbContext;
        }

        public async Task SaveSession(Session session)
        {
            session.CreatedOnUtc = DateTime.UtcNow;
            ApplicationDbContext.Set<Session>().Add(session);
            await ApplicationDbContext.SaveChangesAsync();
        }

        public async Task<int?> GetSessionBrowserId(Guid sessionBrowserUID)
        {
            var id = await ApplicationDbContext.Set<SessionBrowser>().Where(bs => bs.UID == sessionBrowserUID).Select(bs => bs.Id).FirstOrDefaultAsync();
            if (id > 0)
                return id;
            else
                return null;
        }

        public async Task<int?> GetSessionId(Guid sessionUID)
        {
            return await ApplicationDbContext.Set<Session>().Where(bs => bs.UID == sessionUID).Select(bs => bs.Id).FirstOrDefaultAsync();
        }

        public async Task SavePageView(string page, Guid sessionUID, int? userId)
        {
            var pageId = await GetPageId(page);
            var sessionId = await GetSessionId(sessionUID);
            if (pageId.HasValue && sessionId.HasValue)
            {
                var pageView = new PageView
                {
                    PageId = pageId.Value,
                    SessionId = sessionId.Value,
                    UserId = userId,
                    CreatedOnUtc = DateTime.UtcNow
                };
                ApplicationDbContext.Set<PageView>().Add(pageView);
                await ApplicationDbContext.SaveChangesAsync();
            }
        }

        public async Task<int?> GetReferrerId(string code)
        {
            var cachedReferrer = await ApplicationDbContext.Set<Referrer>().FromCache(r => string.Equals(code, r.Code, StringComparison.OrdinalIgnoreCase));
            return cachedReferrer?.Id;
        }

        private async Task<short?> GetPageId(string page)
        {
            var cachedPages = await ApplicationDbContext.Set<Page>().FromCache();
            var cachedPage = cachedPages.FirstOrDefault(p => string.Equals(p.Name, page, StringComparison.OrdinalIgnoreCase));
            return cachedPage?.Id;
        }
    }
}