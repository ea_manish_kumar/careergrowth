﻿using App.DomainModel;
using App.Utility.Constants;
using System;
using System.Collections.Generic;

namespace App.Data.EF.Configuration
{
    internal static class MasterData
    {
        public static IEnumerable<UserRole> GetUserRoles()
        {
            return new List<UserRole>
            {
                new UserRole { Name = UserRoles.Admin, Label = "Site Administrator" },
                new UserRole { Name = UserRoles.User, Label = "Site User" },
            };
        }

        public static IEnumerable<Page> GetPages()
        {
            return new List<Page>
            {
                new Page { Name = Pages.Home, Label = "Home Page", SortIndex = 1 },
                new Page { Name = Pages.Journey, Label = "User Journey", SortIndex = 2 },
                new Page { Name = Pages.Contact, Label = "User Details", SortIndex = 3 },
                new Page { Name = Pages.Template, Label = "Format Selected", SortIndex = 4 },
                new Page { Name = Pages.Academics, Label = "Academics", SortIndex = 5 },
                new Page { Name = Pages.AcademicList, Label = "Academics Details", SortIndex = 6 },
                new Page { Name = Pages.Experience, Label = "Work Experience", SortIndex = 7 },
                new Page { Name = Pages.ExperienceList, Label = "Experience Details", SortIndex = 8 },
                new Page { Name = Pages.Competencies, Label = "Competencies", SortIndex = 9 },
                new Page { Name = Pages.Summary, Label = "Summary", SortIndex = 10 },
                new Page { Name = Pages.Preview, Label = "Preview", SortIndex = 11 },
                new Page { Name = Pages.Dashboard, Label = "Dashboard", SortIndex = 12 },
                new Page { Name = Pages.CustomSection, Label = "Custom Section", SortIndex = 13 },
                new Page { Name = Pages.Plans, Label = "Plans", SortIndex = 14 },
                new Page { Name = Pages.Checkout, Label = "Checkout", SortIndex = 15 },
                new Page { Name = Pages.Premium, Label = "Premium User", SortIndex = 16 },
            };
        }


        public static IEnumerable<DomainModel.SectionType> GetSectionTypes()
        {
            return new List<DomainModel.SectionType>
            {
                new DomainModel.SectionType { Name = Utility.Constants.SectionTypes.Accomplishments, Label = "Accomplishments"},
                new DomainModel.SectionType { Name = Utility.Constants.SectionTypes.AdditionalInformation, Label = "Additional Information"},
                new DomainModel.SectionType { Name = Utility.Constants.SectionTypes.Affiliations, Label = "Affiliations"},
                new DomainModel.SectionType { Name = Utility.Constants.SectionTypes.Awards, Label = "Awards"},
                new DomainModel.SectionType { Name = Utility.Constants.SectionTypes.Certifications, Label = "Certifications"},
                new DomainModel.SectionType { Name = Utility.Constants.SectionTypes.Contact, Label = "Contact"},
                new DomainModel.SectionType { Name = Utility.Constants.SectionTypes.Dissertation, Label = "Dissertation"},
                new DomainModel.SectionType { Name = Utility.Constants.SectionTypes.Education, Label = "Education"},
                new DomainModel.SectionType { Name = Utility.Constants.SectionTypes.ExtraCurricular, Label = "Extra Curricular"},
                new DomainModel.SectionType { Name = Utility.Constants.SectionTypes.Experience, Label = "Experience"},
                new DomainModel.SectionType { Name = Utility.Constants.SectionTypes.Skills, Label = "Skills"},
                new DomainModel.SectionType { Name = Utility.Constants.SectionTypes.Interests, Label = "Interests"},
                new DomainModel.SectionType { Name = Utility.Constants.SectionTypes.Language, Label = "Language"},
                new DomainModel.SectionType { Name = Utility.Constants.SectionTypes.Name, Label = "Name"},
                new DomainModel.SectionType { Name = Utility.Constants.SectionTypes.Other, Label = "Other"},
                new DomainModel.SectionType { Name = Utility.Constants.SectionTypes.PersonalInformation, Label = "Personal Information"},
                new DomainModel.SectionType { Name = Utility.Constants.SectionTypes.Presentations, Label = "Presentations"},
                new DomainModel.SectionType { Name = Utility.Constants.SectionTypes.Portfolio, Label = "Portfolio"},
                new DomainModel.SectionType { Name = Utility.Constants.SectionTypes.Publication, Label = "Publication"},
                new DomainModel.SectionType { Name = Utility.Constants.SectionTypes.References, Label = "References"},
                new DomainModel.SectionType { Name = Utility.Constants.SectionTypes.Summary, Label = "Summary"},
                new DomainModel.SectionType { Name = Utility.Constants.SectionTypes.Training, Label = "Training"},
                new DomainModel.SectionType { Name = Utility.Constants.SectionTypes.Volunteer, Label = "Volunteer"},
                new DomainModel.SectionType { Name = Utility.Constants.SectionTypes.CommunityService, Label = "Community Service"},
            };
        }

        //TODO: Master data for the Fields and description to be updated
        public static IEnumerable<Field> GetFields()
        {
            return new List<Field>
            {
                new Field { Name = DocumentFieldTypes.Name, Label = "Name" },
                new Field { Name = DocumentFieldTypes.Email, Label = "Email" },
                new Field { Name = DocumentFieldTypes.Address, Label = "Address" },
                new Field { Name = DocumentFieldTypes.Description, Label = "Description"},
                //new Field { Name = "ALRT", Label = "Send me job alert emails based on my desired career."},
                new Field { Name = DocumentFieldTypes.AlternatePhone, Label = "Alternate Phone"},
                new Field { Name = DocumentFieldTypes.CertificationName, Label = "Certification Name"},
                new Field { Name = DocumentFieldTypes.CGPA, Label = "CGPA"},
                new Field { Name = DocumentFieldTypes.City, Label = "City"},
                new Field { Name = DocumentFieldTypes.State, Label = "State"},
                new Field { Name = DocumentFieldTypes.ZipCode, Label = "Zip Code"},
                new Field { Name = DocumentFieldTypes.CompanyLocation, Label = "Company Location"},
                new Field { Name = DocumentFieldTypes.CompanyName, Label = "Company Name"},
                new Field { Name = DocumentFieldTypes.CellPhone, Label = "Cell Phone"},
                new Field { Name = DocumentFieldTypes.CareerField, Label = "Career Field"},
                new Field { Name = DocumentFieldTypes.CareerLevel, Label = "Career Level"},
                new Field { Name = DocumentFieldTypes.StartMonth, Label = "Start Month"},
                new Field { Name = DocumentFieldTypes.StartYear, Label = "Start Year"},
                new Field { Name = DocumentFieldTypes.EndMonth, Label = "End Month"},
                new Field { Name = DocumentFieldTypes.EndYear, Label = "End Year"},
                new Field { Name = DocumentFieldTypes.Degree, Label = "Most recent degree"},
                new Field { Name = DocumentFieldTypes.DegreeSpecialization, Label = "Degree Specialization"},
                //new Field { Name = DocumentFieldType., Label = "I am currently"},
                new Field { Name = DocumentFieldTypes.EmploymentType, Label = "Employment Type"},
                new Field { Name = DocumentFieldTypes.FirstName, Label = "First Name"},
                new Field { Name = DocumentFieldTypes.From, Label = "From"},
                new Field { Name = DocumentFieldTypes.Headline, Label = "Headline"},
                new Field { Name = DocumentFieldTypes.HighestEducation, Label = "Highest level of education"},
                new Field { Name = DocumentFieldTypes.HomePhone, Label = "Home Phone"},
                new Field { Name = DocumentFieldTypes.InstitutionName, Label = "Institution Name"},
                new Field { Name = DocumentFieldTypes.JobTitle, Label = "Desired Job Title"},
                new Field { Name = DocumentFieldTypes.JobType, Label = "Job Type"},
                new Field { Name = DocumentFieldTypes.LastName, Label = "Last Name"},
                //new Field { Name = DocumentFieldType., Label = "Where would you like to work?"},
                new Field { Name = DocumentFieldTypes.ProficiencyLevel, Label = "Proficiency level"},
                //new Field { Name = DocumentFieldType., Label = "Resume Title"},
                new Field { Name = DocumentFieldTypes.SchoolName, Label = "School Name"},
                new Field { Name = DocumentFieldTypes.SchoolLocation, Label = "School Location"},
                new Field { Name = DocumentFieldTypes.SkillName, Label = "Skill name"},
                new Field { Name = DocumentFieldTypes.SkillLevel, Label = "Skill level"},
                new Field { Name = DocumentFieldTypes.PreferredSpeciality, Label = "Preferred Speciality"},
                new Field { Name = DocumentFieldTypes.FieldOfStudy, Label = "Field(s) of study"},
                //new Field { Name = DocumentFieldType., Label = "Summary"},
                new Field { Name = DocumentFieldTypes.University, Label = "University"},
                new Field { Name = DocumentFieldTypes.WebsiteOrPortfolio, Label = "Your website or portfolio"},
            };
        }

        public static IEnumerable<UserEvent> GetEvents()
        {
            return new List<UserEvent>
            {
                new UserEvent { Name = EventType.Navigation, Label = "Navigation" },
                new UserEvent { Name = EventType.DownloadAction, Label = "Download" },
                new UserEvent { Name = EventType.PrintAction, Label = "Print" },
                new UserEvent { Name = EventType.EmailAction, Label = "Email" },
                new UserEvent { Name = EventType.PaymentAction, Label = "Payment" },
                new UserEvent { Name = EventType.WelcomeBackFlow, Label = "Welcome Back" },
                new UserEvent { Name = EventType.Email_Guid, Label = "Email GUID" },
                new UserEvent { Name = EventType.PasswordEmailGuid, Label = "Password GUID" }
            };
        }

        public static IEnumerable<PlanType> GetPlanTypes()
        {
            return new List<PlanType>
            { 
                new PlanType { Name = "FREE", WaterMarkEnabled = true, Label = "Free Plan" },
                new PlanType { Name = "STANDARD", WaterMarkEnabled = false, Label = "7 Days Access" },
                new PlanType { Name = "PROF", WaterMarkEnabled = false, Label = "30 Days Access" },
                new PlanType { Name = "PREMIUM", WaterMarkEnabled = false, Label = "90 Days Access" }
            };
        }

        public static IEnumerable<Plan> GetPlans()
        {
            return new List<Plan>
            {
                new Plan { PlanTypeID = 1, Name="Free", CurrencyCD = "INR", CountryCD = "IN", ValidityDays = 2, UnitPrice = 0, Label = "2 Days Free Access" },
                new Plan { PlanTypeID = 2, Name="Standard", CurrencyCD = "INR", CountryCD = "IN", ValidityDays = 7, UnitPrice = 199, Label = "7 Days Standard Access"},
                new Plan { PlanTypeID = 3, Name="Prof", CurrencyCD = "INR", CountryCD = "IN", ValidityDays = 30, UnitPrice = 399, Label = "30 Days Professional Access"},
                new Plan { PlanTypeID = 4, Name="Premium", CurrencyCD = "INR", CountryCD = "IN", ValidityDays = 90, UnitPrice = 599, Label = "90 Days Premium Access"},
                new Plan { PlanTypeID = 1, Name="Free", CurrencyCD = "USD", CountryCD = "US", ValidityDays = 2, UnitPrice = 0, Label = "2 Days Free Access" },
                new Plan { PlanTypeID = 2, Name="Standard", CurrencyCD = "USD", CountryCD = "US", ValidityDays = 7, UnitPrice = 1.99M, Label = "7 Days Standard Access"},
                new Plan { PlanTypeID = 3, Name="Prof", CurrencyCD = "USD", CountryCD = "US", ValidityDays = 30, UnitPrice = 4.99M, Label = "30 Days Professional Access"},
                new Plan { PlanTypeID = 4, Name="Premium", CurrencyCD = "USD", CountryCD = "US", ValidityDays = 90, UnitPrice = 7.99M, Label = "90 Days Premium Access"}
            };
        }

        public static IEnumerable<Feature> GetFeatures()
        {
            return new List<Feature>
            {
                new Feature { Name = "Download", Label = "Download Resume" },
                new Feature { Name = "Print", Label = "Print Resume" },
                new Feature { Name = "Email",  Label = "Email Resume" },
                new Feature { Name = "ResumeCheck", Label = "Resume Check" }
            };
        }

        public static IEnumerable<PlanTypeFeature> GetPlanFeatures()
        {
            return new List<PlanTypeFeature>
            {
                new PlanTypeFeature { PlanTypeID = 2, FeatureID = 1 },
                new PlanTypeFeature { PlanTypeID = 2, FeatureID = 2 },
                new PlanTypeFeature { PlanTypeID = 2, FeatureID = 3 },
                new PlanTypeFeature { PlanTypeID = 2, FeatureID = 4 },

                new PlanTypeFeature { PlanTypeID = 3, FeatureID = 1 },
                new PlanTypeFeature { PlanTypeID = 3, FeatureID = 2 },
                new PlanTypeFeature { PlanTypeID = 3, FeatureID = 3 },
                new PlanTypeFeature { PlanTypeID = 3, FeatureID = 4 },

                new PlanTypeFeature { PlanTypeID = 4, FeatureID = 1 },
                new PlanTypeFeature { PlanTypeID = 4, FeatureID = 2 },
                new PlanTypeFeature { PlanTypeID = 4, FeatureID = 3 },
                new PlanTypeFeature { PlanTypeID = 4, FeatureID = 4 }
            };
        }
    }
}