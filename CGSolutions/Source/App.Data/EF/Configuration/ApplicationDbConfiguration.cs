﻿using App.DomainModel;
using App.Utility.Extensions;
using System.Collections.Generic;
using System.Data.Entity.Migrations;
using System.Linq;

namespace App.Data.EF.Configuration
{
    internal sealed class ApplicationDbConfiguration : DbMigrationsConfiguration<ApplicationDbContext>
    {
        private ApplicationDbContext ApplicationDbContext;

        public ApplicationDbConfiguration()
        {
            AutomaticMigrationsEnabled = false;
            AutomaticMigrationDataLossAllowed = false;
        }

        protected override void Seed(ApplicationDbContext context)
        {
            ApplicationDbContext = context;

            SeedData(MasterData.GetUserRoles());
            SeedData(MasterData.GetPages());
            SeedData(MasterData.GetFields());
            SeedData(MasterData.GetSectionTypes());
            SeedData(MasterData.GetEvents());
            SeedData(MasterData.GetPlanTypes());
            SeedData(MasterData.GetPlans());
            SeedData(MasterData.GetFeatures());
            SeedData(MasterData.GetPlanFeatures());

            base.Seed(context);
        }

        void SeedData<T>(IEnumerable<T> seedEntities)
            where T : class, ISeedEntity
        {
            if (seedEntities.IsNotNullOrEmpty())
            {
                var dbEntities = ApplicationDbContext.Set<T>().ToList();
                foreach (var seedEntity in seedEntities)
                {
                    var dbEntity = dbEntities.FirstOrDefault(db => db.IdentifierEquals(seedEntity));
                    if (dbEntity == null)
                        ApplicationDbContext.Set<T>().Add(seedEntity);
                    else if (!dbEntity.ValueEquals(seedEntity))
                        dbEntity.Merge(seedEntity);
                }
                ApplicationDbContext.SaveChanges();
            }
        }
    }
}