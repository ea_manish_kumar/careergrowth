﻿namespace App.Data.EF.Configuration
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class UserEventDataMigration : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "EF.UserEvents",
                c => new
                    {
                        Id = c.Short(nullable: false, identity: true),
                        Name = c.String(nullable: false, maxLength: 8, unicode: false),
                        Label = c.String(nullable: false, maxLength: 64, unicode: false),
                    })
                .PrimaryKey(t => t.Id)
                .Index(t => t.Name, unique: true);
            
            CreateTable(
                "EF.UserEventData",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        UserId = c.Int(nullable: false),
                        EventId = c.Short(nullable: false),
                        EventData = c.String(nullable: false, maxLength: 4000),
                        CreatedOnUtc = c.DateTime(nullable: false, precision: 7, storeType: "datetime2"),
                        ModifiedOnUtc = c.DateTime(precision: 7, storeType: "datetime2"),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("EF.UserEvents", t => t.EventId, cascadeDelete: true)
                .ForeignKey("EF.Users", t => t.UserId, cascadeDelete: true)
                .Index(t => t.UserId)
                .Index(t => t.EventId);
            
        }
        
        public override void Down()
        {
            DropForeignKey("EF.UserEventData", "UserId", "EF.Users");
            DropForeignKey("EF.UserEventData", "EventId", "EF.UserEvents");
            DropIndex("EF.UserEventData", new[] { "EventId" });
            DropIndex("EF.UserEventData", new[] { "UserId" });
            DropIndex("EF.UserEvents", new[] { "Name" });
            DropTable("EF.UserEventData");
            DropTable("EF.UserEvents");
        }
    }
}
