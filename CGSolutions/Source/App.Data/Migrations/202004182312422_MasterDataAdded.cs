﻿namespace App.Data.EF.Configuration
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class MasterDataAdded : DbMigration
    {
        public override void Up()
        {
            AddColumn("EF.Plans", "Name", c => c.String());
            DropColumn("EF.Features", "FeatureCD");
            DropColumn("EF.PlanType", "PlanTypeCD");
        }
        
        public override void Down()
        {
            AddColumn("EF.PlanType", "PlanTypeCD", c => c.String(maxLength: 50, unicode: false));
            AddColumn("EF.Features", "FeatureCD", c => c.String(nullable: false, maxLength: 10, unicode: false));
            DropColumn("EF.Plans", "Name");
        }
    }
}
