﻿namespace App.Data.EF.Configuration
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class CompleteDB : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "EF.PlanTypeFeatures",
                c => new
                    {
                        PlanTypeFeatureID = c.Int(nullable: false, identity: true),
                        PlanTypeID = c.Short(nullable: false),
                        FeatureID = c.Short(nullable: false),
                    })
                .PrimaryKey(t => t.PlanTypeFeatureID)
                .ForeignKey("EF.Features", t => t.FeatureID, cascadeDelete: true)
                .ForeignKey("EF.PlanType", t => t.PlanTypeID, cascadeDelete: true)
                .Index(t => t.PlanTypeID)
                .Index(t => t.FeatureID);
            
            CreateTable(
                "EF.Features",
                c => new
                    {
                        FeatureId = c.Short(nullable: false, identity: true),
                        FeatureCD = c.String(nullable: false, maxLength: 10, unicode: false),
                        Name = c.String(),
                        Label = c.String(),
                    })
                .PrimaryKey(t => t.FeatureId);
            
            CreateTable(
                "EF.PlanType",
                c => new
                    {
                        Id = c.Short(nullable: false, identity: true),
                        PlanTypeCD = c.String(maxLength: 50, unicode: false),
                        WaterMarkEnabled = c.Boolean(),
                        Name = c.String(nullable: false, maxLength: 8, unicode: false),
                        Label = c.String(nullable: false, maxLength: 64, unicode: false),
                    })
                .PrimaryKey(t => t.Id)
                .Index(t => t.Name, unique: true);
            
            CreateTable(
                "EF.Transactions",
                c => new
                    {
                        TransactionId = c.Int(nullable: false, identity: true),
                        UserID = c.Int(),
                        OrderID = c.Int(),
                        StatusCD = c.String(maxLength: 10, unicode: false),
                        VendorCD = c.String(maxLength: 20, unicode: false),
                        VendorReferenceCD = c.String(maxLength: 20, unicode: false),
                        Amount = c.Decimal(precision: 18, scale: 2),
                        IPAddress = c.String(maxLength: 50, unicode: false),
                        SessionID = c.Int(nullable: false),
                        CreatedOnUtc = c.DateTime(precision: 7, storeType: "datetime2"),
                        ModifiedOnUtc = c.DateTime(precision: 7, storeType: "datetime2"),
                    })
                .PrimaryKey(t => t.TransactionId)
                .ForeignKey("EF.Orders", t => t.OrderID)
                .ForeignKey("EF.Users", t => t.UserID)
                .Index(t => t.UserID)
                .Index(t => t.OrderID);
            
            CreateTable(
                "EF.Orders",
                c => new
                    {
                        OrderId = c.Int(nullable: false, identity: true),
                        UserID = c.Int(),
                        Amount = c.Decimal(precision: 18, scale: 2),
                        StatusCD = c.String(maxLength: 10, unicode: false),
                        SessionID = c.Int(nullable: false),
                        CreatedOnUtc = c.DateTime(precision: 7, storeType: "datetime2"),
                        ModifiedOnUtc = c.DateTime(precision: 7, storeType: "datetime2"),
                    })
                .PrimaryKey(t => t.OrderId)
                .ForeignKey("EF.Users", t => t.UserID)
                .Index(t => t.UserID);
            
            CreateTable(
                "EF.Users",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        UserName = c.String(nullable: false, maxLength: 256),
                        Email = c.String(nullable: false, maxLength: 256),
                        RoleId = c.Short(nullable: false),
                        Name = c.String(maxLength: 128, unicode: false),
                        SecurityStamp = c.String(nullable: false, maxLength: 64),
                        HashedPassword = c.String(nullable: false, maxLength: 4000),
                        CreatedOnUtc = c.DateTime(nullable: false, precision: 7, storeType: "datetime2"),
                        LockoutEndDateUTC = c.DateTime(precision: 7, storeType: "datetime2"),
                        AccessFailedCount = c.Int(),
                        IsActive = c.Boolean(nullable: false),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("EF.UserRoles", t => t.RoleId, cascadeDelete: true)
                .Index(t => t.UserName, unique: true)
                .Index(t => t.RoleId);
            
            CreateTable(
                "EF.UserRoles",
                c => new
                    {
                        Id = c.Short(nullable: false, identity: true),
                        Name = c.String(nullable: false, maxLength: 8, unicode: false),
                        Label = c.String(nullable: false, maxLength: 64, unicode: false),
                    })
                .PrimaryKey(t => t.Id)
                .Index(t => t.Name, unique: true);
            
            CreateTable(
                "EF.Subscriptions",
                c => new
                    {
                        SubscriptionId = c.Int(nullable: false, identity: true),
                        UserID = c.Int(),
                        OrderID = c.Int(),
                        PlanTypeId = c.Short(),
                        StartDate = c.DateTime(precision: 7, storeType: "datetime2"),
                        ExpireDate = c.DateTime(precision: 7, storeType: "datetime2"),
                        StatusCD = c.String(maxLength: 10, unicode: false),
                        CreatedOnUtc = c.DateTime(precision: 7, storeType: "datetime2"),
                        ModifiedOnUtc = c.DateTime(precision: 7, storeType: "datetime2"),
                    })
                .PrimaryKey(t => t.SubscriptionId)
                .ForeignKey("EF.Orders", t => t.OrderID)
                .ForeignKey("EF.PlanType", t => t.PlanTypeId)
                .ForeignKey("EF.Users", t => t.UserID)
                .Index(t => t.UserID)
                .Index(t => t.OrderID)
                .Index(t => t.PlanTypeId);
            
            CreateTable(
                "EF.PlanTypeSkin",
                c => new
                    {
                        PlanTypeSkinId = c.Int(nullable: false, identity: true),
                        PlanTypeId = c.Short(nullable: false),
                        SkinID = c.Short(nullable: false),
                        WaterMarkEnabled = c.Boolean(),
                    })
                .PrimaryKey(t => t.PlanTypeSkinId)
                .ForeignKey("EF.PlanType", t => t.PlanTypeId, cascadeDelete: true)
                .ForeignKey("EF.Skins", t => t.SkinID, cascadeDelete: true)
                .Index(t => t.PlanTypeId)
                .Index(t => t.SkinID);
            
            CreateTable(
                "EF.Skins",
                c => new
                    {
                        Id = c.Short(nullable: false, identity: true),
                        Name = c.String(nullable: false, maxLength: 8, unicode: false),
                        Label = c.String(nullable: false, maxLength: 64, unicode: false),
                    })
                .PrimaryKey(t => t.Id)
                .Index(t => t.Name, unique: true);
            
            CreateTable(
                "EF.Plans",
                c => new
                    {
                        PlanId = c.Int(nullable: false, identity: true),
                        PlanTypeID = c.Short(nullable: false),
                        Label = c.String(maxLength: 50, unicode: false),
                        CurrencyCD = c.String(nullable: false, maxLength: 10, unicode: false),
                        CountryCD = c.String(nullable: false, maxLength: 10, unicode: false),
                        UnitPrice = c.Decimal(precision: 18, scale: 2),
                        ValidityDays = c.Int(),
                    })
                .PrimaryKey(t => t.PlanId)
                .ForeignKey("EF.PlanType", t => t.PlanTypeID, cascadeDelete: true)
                .Index(t => t.PlanTypeID);
            
            CreateTable(
                "EF.OrderItems",
                c => new
                    {
                        OrderItemId = c.Int(nullable: false, identity: true),
                        OrderID = c.Int(nullable: false),
                        PlanID = c.Int(nullable: false),
                        Price = c.Decimal(precision: 18, scale: 2),
                        CreatedOnUtc = c.DateTime(precision: 7, storeType: "datetime2"),
                    })
                .PrimaryKey(t => t.OrderItemId);
            
            CreateTable(
                "EF.Content",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        OccupationId = c.Int(),
                        OnetSoc_Code = c.String(nullable: false, maxLength: 10, unicode: false),
                        ContentType = c.Short(nullable: false),
                        Example = c.String(maxLength: 4000),
                        SourceType = c.Short(),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("EF.Occupations", t => t.OccupationId)
                .Index(t => t.OccupationId);
            
            CreateTable(
                "EF.Occupations",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        OnetSoc_Code = c.String(nullable: false, maxLength: 10, unicode: false),
                        Title = c.String(nullable: false, maxLength: 150),
                        Description = c.String(maxLength: 1000),
                    })
                .PrimaryKey(t => t.Id);
            
            CreateTable(
                "EF.JobTitle",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        OnetSoc_Code = c.String(nullable: false, maxLength: 10, unicode: false),
                        Title = c.String(nullable: false, maxLength: 150),
                        OccupationId = c.Int(),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("EF.Occupations", t => t.OccupationId)
                .Index(t => t.OccupationId);
            
            CreateTable(
                "EF.DocumentDatas",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        SubSectionId = c.Int(nullable: false),
                        FieldId = c.Short(nullable: false),
                        Data = c.String(),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("EF.Fields", t => t.FieldId, cascadeDelete: true)
                .ForeignKey("EF.SubSections", t => t.SubSectionId, cascadeDelete: true)
                .Index(t => t.SubSectionId)
                .Index(t => t.FieldId);
            
            CreateTable(
                "EF.Fields",
                c => new
                    {
                        Id = c.Short(nullable: false, identity: true),
                        Name = c.String(nullable: false, maxLength: 8, unicode: false),
                        Label = c.String(nullable: false, maxLength: 64, unicode: false),
                    })
                .PrimaryKey(t => t.Id)
                .Index(t => t.Name, unique: true);
            
            CreateTable(
                "EF.SubSections",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        SectionId = c.Int(nullable: false),
                        SortIndex = c.Short(nullable: false),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("EF.Sections", t => t.SectionId, cascadeDelete: true)
                .Index(t => t.SectionId);
            
            CreateTable(
                "EF.Sections",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Label = c.String(nullable: false, maxLength: 64, unicode: false),
                        DocumentId = c.Int(nullable: false),
                        SectionTypeId = c.Short(nullable: false),
                        SortIndex = c.Short(nullable: false),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("EF.Documents", t => t.DocumentId, cascadeDelete: true)
                .ForeignKey("EF.SectionType", t => t.SectionTypeId, cascadeDelete: true)
                .Index(t => t.DocumentId)
                .Index(t => t.SectionTypeId);
            
            CreateTable(
                "EF.Documents",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        UserId = c.Int(nullable: false),
                        DocName = c.String(maxLength: 256),
                        DocTypeCD = c.String(nullable: false, maxLength: 8, unicode: false),
                        SkinCD = c.String(nullable: false, maxLength: 8, unicode: false),
                        CreatedOnUtc = c.DateTime(nullable: false, precision: 7, storeType: "datetime2"),
                        ModifiedOnUtc = c.DateTime(precision: 7, storeType: "datetime2"),
                    })
                .PrimaryKey(t => t.Id)
                .Index(t => t.UserId);
            
            CreateTable(
                "EF.DocumentStyles",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        StyleId = c.Short(nullable: false),
                        DocumentId = c.Int(nullable: false),
                        Value = c.String(),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("EF.Documents", t => t.DocumentId, cascadeDelete: true)
                .ForeignKey("EF.Styles", t => t.StyleId, cascadeDelete: true)
                .Index(t => new { t.DocumentId, t.StyleId });
            
            CreateTable(
                "EF.Styles",
                c => new
                    {
                        Id = c.Short(nullable: false, identity: true),
                        Name = c.String(nullable: false, maxLength: 8, unicode: false),
                        Label = c.String(nullable: false, maxLength: 64, unicode: false),
                    })
                .PrimaryKey(t => t.Id)
                .Index(t => t.Name, unique: true);
            
            CreateTable(
                "EF.SectionType",
                c => new
                    {
                        Id = c.Short(nullable: false, identity: true),
                        Name = c.String(nullable: false, maxLength: 8, unicode: false),
                        Label = c.String(nullable: false, maxLength: 64, unicode: false),
                    })
                .PrimaryKey(t => t.Id)
                .Index(t => t.Name, unique: true);
            
            CreateTable(
                "EF.Referrers",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Code = c.String(nullable: false, maxLength: 16, unicode: false),
                        Name = c.String(nullable: false, maxLength: 32, unicode: false),
                        Description = c.String(maxLength: 64, unicode: false),
                    })
                .PrimaryKey(t => t.Id)
                .Index(t => t.Code, unique: true);
            
            CreateTable(
                "EF.SessionBrowsers",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        UID = c.Guid(nullable: false),
                    })
                .PrimaryKey(t => t.Id)
                .Index(t => t.UID, unique: true);
            
            CreateTable(
                "EF.Sessions",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        RequestedUrl = c.String(maxLength: 4000),
                        UrlReferrer = c.String(maxLength: 4000),
                        UserAgent = c.String(maxLength: 4000),
                        IPAddress = c.String(maxLength: 128, unicode: false),
                        Source = c.String(maxLength: 64, unicode: false),
                        Medium = c.String(maxLength: 64, unicode: false),
                        Campaign = c.String(maxLength: 64, unicode: false),
                        CreatedOnUtc = c.DateTime(precision: 7, storeType: "datetime2"),
                        SessionBrowserId = c.Int(nullable: false),
                        ReferrerId = c.Int(),
                        UID = c.Guid(nullable: false),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("EF.Referrers", t => t.ReferrerId)
                .ForeignKey("EF.SessionBrowsers", t => t.SessionBrowserId, cascadeDelete: true)
                .Index(t => t.SessionBrowserId)
                .Index(t => t.ReferrerId)
                .Index(t => t.UID, unique: true);
            
            CreateTable(
                "EF.UserLogins",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        UniqueStamp = c.String(nullable: false, maxLength: 64),
                        UserId = c.Int(nullable: false),
                        LoginType = c.Byte(nullable: false),
                        CreatedOnUtc = c.DateTime(nullable: false, precision: 7, storeType: "datetime2"),
                        ExpiredOnUtc = c.DateTime(nullable: false, precision: 7, storeType: "datetime2"),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("EF.Users", t => t.UserId, cascadeDelete: true)
                .Index(t => t.UserId);
            
            CreateTable(
                "EF.UserLoginSessions",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        UserLoginId = c.Int(nullable: false),
                        SessionId = c.Int(nullable: false),
                        CreatedOnUtc = c.DateTime(nullable: false, precision: 7, storeType: "datetime2"),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("EF.UserLogins", t => t.UserLoginId, cascadeDelete: true)
                .Index(t => new { t.SessionId, t.UserLoginId }, unique: true);
            
            CreateTable(
                "EF.Pages",
                c => new
                    {
                        Id = c.Short(nullable: false, identity: true),
                        SortIndex = c.Byte(),
                        Name = c.String(nullable: false, maxLength: 8, unicode: false),
                        Label = c.String(nullable: false, maxLength: 64, unicode: false),
                    })
                .PrimaryKey(t => t.Id)
                .Index(t => t.Name, unique: true);
            
            CreateTable(
                "EF.PageViews",
                c => new
                    {
                        Id = c.Long(nullable: false, identity: true),
                        PageId = c.Short(nullable: false),
                        SessionId = c.Int(nullable: false),
                        UserId = c.Int(),
                        CreatedOnUtc = c.DateTime(nullable: false),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("EF.Pages", t => t.PageId, cascadeDelete: true)
                .Index(t => t.PageId);
            
            CreateTable(
                "EF.UserTokens",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        UserId = c.Int(nullable: false),
                        TokenType = c.Byte(nullable: false),
                        HashedToken = c.String(nullable: false, maxLength: 512),
                        CreatedOnUtc = c.DateTime(nullable: false, precision: 7, storeType: "datetime2"),
                        ExpiryTime = c.Time(nullable: false, precision: 7),
                        VerifiedOnUtc = c.DateTime(precision: 7, storeType: "datetime2"),
                        UID = c.Guid(nullable: false),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("EF.Users", t => t.UserId, cascadeDelete: true)
                .Index(t => t.UserId)
                .Index(t => t.UID, unique: true);
            
        }
        
        public override void Down()
        {
            DropForeignKey("EF.UserTokens", "UserId", "EF.Users");
            DropForeignKey("EF.PageViews", "PageId", "EF.Pages");
            DropForeignKey("EF.UserLoginSessions", "UserLoginId", "EF.UserLogins");
            DropForeignKey("EF.UserLogins", "UserId", "EF.Users");
            DropForeignKey("EF.Sessions", "SessionBrowserId", "EF.SessionBrowsers");
            DropForeignKey("EF.Sessions", "ReferrerId", "EF.Referrers");
            DropForeignKey("EF.DocumentDatas", "SubSectionId", "EF.SubSections");
            DropForeignKey("EF.SubSections", "SectionId", "EF.Sections");
            DropForeignKey("EF.Sections", "SectionTypeId", "EF.SectionType");
            DropForeignKey("EF.Sections", "DocumentId", "EF.Documents");
            DropForeignKey("EF.DocumentStyles", "StyleId", "EF.Styles");
            DropForeignKey("EF.DocumentStyles", "DocumentId", "EF.Documents");
            DropForeignKey("EF.DocumentDatas", "FieldId", "EF.Fields");
            DropForeignKey("EF.JobTitle", "OccupationId", "EF.Occupations");
            DropForeignKey("EF.Content", "OccupationId", "EF.Occupations");
            DropForeignKey("EF.Plans", "PlanTypeID", "EF.PlanType");
            DropForeignKey("EF.PlanTypeSkin", "SkinID", "EF.Skins");
            DropForeignKey("EF.PlanTypeSkin", "PlanTypeId", "EF.PlanType");
            DropForeignKey("EF.Subscriptions", "UserID", "EF.Users");
            DropForeignKey("EF.Subscriptions", "PlanTypeId", "EF.PlanType");
            DropForeignKey("EF.Subscriptions", "OrderID", "EF.Orders");
            DropForeignKey("EF.Transactions", "UserID", "EF.Users");
            DropForeignKey("EF.Transactions", "OrderID", "EF.Orders");
            DropForeignKey("EF.Orders", "UserID", "EF.Users");
            DropForeignKey("EF.Users", "RoleId", "EF.UserRoles");
            DropForeignKey("EF.PlanTypeFeatures", "PlanTypeID", "EF.PlanType");
            DropForeignKey("EF.PlanTypeFeatures", "FeatureID", "EF.Features");
            DropIndex("EF.UserTokens", new[] { "UID" });
            DropIndex("EF.UserTokens", new[] { "UserId" });
            DropIndex("EF.PageViews", new[] { "PageId" });
            DropIndex("EF.Pages", new[] { "Name" });
            DropIndex("EF.UserLoginSessions", new[] { "SessionId", "UserLoginId" });
            DropIndex("EF.UserLogins", new[] { "UserId" });
            DropIndex("EF.Sessions", new[] { "UID" });
            DropIndex("EF.Sessions", new[] { "ReferrerId" });
            DropIndex("EF.Sessions", new[] { "SessionBrowserId" });
            DropIndex("EF.SessionBrowsers", new[] { "UID" });
            DropIndex("EF.Referrers", new[] { "Code" });
            DropIndex("EF.SectionType", new[] { "Name" });
            DropIndex("EF.Styles", new[] { "Name" });
            DropIndex("EF.DocumentStyles", new[] { "DocumentId", "StyleId" });
            DropIndex("EF.Documents", new[] { "UserId" });
            DropIndex("EF.Sections", new[] { "SectionTypeId" });
            DropIndex("EF.Sections", new[] { "DocumentId" });
            DropIndex("EF.SubSections", new[] { "SectionId" });
            DropIndex("EF.Fields", new[] { "Name" });
            DropIndex("EF.DocumentDatas", new[] { "FieldId" });
            DropIndex("EF.DocumentDatas", new[] { "SubSectionId" });
            DropIndex("EF.JobTitle", new[] { "OccupationId" });
            DropIndex("EF.Content", new[] { "OccupationId" });
            DropIndex("EF.Plans", new[] { "PlanTypeID" });
            DropIndex("EF.Skins", new[] { "Name" });
            DropIndex("EF.PlanTypeSkin", new[] { "SkinID" });
            DropIndex("EF.PlanTypeSkin", new[] { "PlanTypeId" });
            DropIndex("EF.Subscriptions", new[] { "PlanTypeId" });
            DropIndex("EF.Subscriptions", new[] { "OrderID" });
            DropIndex("EF.Subscriptions", new[] { "UserID" });
            DropIndex("EF.UserRoles", new[] { "Name" });
            DropIndex("EF.Users", new[] { "RoleId" });
            DropIndex("EF.Users", new[] { "UserName" });
            DropIndex("EF.Orders", new[] { "UserID" });
            DropIndex("EF.Transactions", new[] { "OrderID" });
            DropIndex("EF.Transactions", new[] { "UserID" });
            DropIndex("EF.PlanType", new[] { "Name" });
            DropIndex("EF.PlanTypeFeatures", new[] { "FeatureID" });
            DropIndex("EF.PlanTypeFeatures", new[] { "PlanTypeID" });
            DropTable("EF.UserTokens");
            DropTable("EF.PageViews");
            DropTable("EF.Pages");
            DropTable("EF.UserLoginSessions");
            DropTable("EF.UserLogins");
            DropTable("EF.Sessions");
            DropTable("EF.SessionBrowsers");
            DropTable("EF.Referrers");
            DropTable("EF.SectionType");
            DropTable("EF.Styles");
            DropTable("EF.DocumentStyles");
            DropTable("EF.Documents");
            DropTable("EF.Sections");
            DropTable("EF.SubSections");
            DropTable("EF.Fields");
            DropTable("EF.DocumentDatas");
            DropTable("EF.JobTitle");
            DropTable("EF.Occupations");
            DropTable("EF.Content");
            DropTable("EF.OrderItems");
            DropTable("EF.Plans");
            DropTable("EF.Skins");
            DropTable("EF.PlanTypeSkin");
            DropTable("EF.Subscriptions");
            DropTable("EF.UserRoles");
            DropTable("EF.Users");
            DropTable("EF.Orders");
            DropTable("EF.Transactions");
            DropTable("EF.PlanType");
            DropTable("EF.Features");
            DropTable("EF.PlanTypeFeatures");
        }
    }
}
