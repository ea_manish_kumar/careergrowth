﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Drawing;
using System.Drawing.Drawing2D;
using System.Drawing.Imaging;
using System.Linq;
using System.Reflection;
using System.Resources;
using System.Text;
using System.Text.RegularExpressions;
using System.Web;
using System.Xml;
using BusinessLogic.Content;
using CommonModules.Exceptions;
using DataAccess.Documents;
using DataAccess.User;
using DomainModel;
using DomainModel.Content;
using DomainModel.User;
using Resources;
using Utilities.Constants;
using Utilities.Constants.Documents;
using Utilities.ImageExport;
using MSIO = System.IO;

namespace BusinessLogicProcess.Documents
{
    public class DocumentsProcess
    {
        private static readonly Dictionary<string, XmlDocument> skinDocuments = new Dictionary<string, XmlDocument>();
        private static readonly Dictionary<string, string> skinCDs = new Dictionary<string, string>();
        private static readonly ResourceManager resourceMgr = Resource.ResourceManager;

        private static IList<SkinStyle> lstSkinStyles;

        static DocumentsProcess()
        {
            LoadAllSkins();
            lstSkinStyles = LoadAllSkinStyles();
        }

        public static IList<SkinStyle> GetAllSkinStyles
        {
            get
            {
                if (lstSkinStyles == null)
                    lstSkinStyles = LoadAllSkinStyles();
                return lstSkinStyles;
            }
        }

        private static IList<SkinStyle> LoadAllSkinStyles()
        {
            IList<SkinStyle> lstSkinStyles = null;
            try
            {
                lstSkinStyles = DocumentFactory.SkinStyleDAL.GetAll(3000);
            }
            catch (Exception ex)
            {
                var htParams = new Hashtable(1);
                htParams.Add("SkinStylesList",
                    lstSkinStyles != null && lstSkinStyles.Count > 0 ? lstSkinStyles.ToString() : "<NULL>");
                var objMethod = (MethodInfo) MethodBase.GetCurrentMethod();
                GlobalExceptionHandler.customExceptionHandler(ex, objMethod, htParams, SubSystem.Content,
                    Application.WebApp, ErrorSeverityType.High, null, true);
            }

            return lstSkinStyles;
        }

        private static void LoadAllSkins()
        {
            var skinsFolder = HttpContext.Current.Server.MapPath("~/resources/skinsb/");
            foreach (var skinCD in skinCDs)
            {
                var skinFile = skinsFolder + skinCD.Key + ".htm";
                if (!skinDocuments.ContainsKey(skinCD.Key.ToUpper()))
                {
                    var skinHTML = new XmlDocument();
                    skinHTML.PreserveWhitespace = true;
                    skinHTML.Load(skinFile);
                    skinDocuments.Add(skinCD.Key.ToUpper(), skinHTML);
                }
            }
        }
        
        /// <summary>
        ///     Returns file object with latest data
        /// </summary>
        /// <param name="pageCount"></param>
        /// <param name="bytStrm"></param>
        /// <param name="fileTypeCd"></param>
        /// <param name="oldFile"></param>
        /// <returns></returns>
        private File GetFile(int pageCount, byte[] bytStrm, string fileTypeCd, File oldFile)
        {
            if (oldFile == null)
                oldFile = new File();

            oldFile.FileTypeCD = fileTypeCd;
            oldFile.BinaryData = bytStrm;
            oldFile.NumOfPages = pageCount;
            return oldFile;
        }

        public List<DocStyle> AddDefaultDocStyles(string skinCd, string styleTypeCode, int documentId)
        {
            var lstDocStyles = new List<DocStyle>();
            AddDocStyles(skinCd, lstDocStyles, styleTypeCode, documentId);
            return lstDocStyles;
        }

        private void AddDocStyles(string skinCd, IList<DocStyle> docStyles, string styleTypeCode, int documentId)
        {
            DocStyle docStyle = null;
            IList<SkinStyle> lstSkinStyles = null;

            try
            {
                lstSkinStyles = GetAllSkinStyles.Where(s => s.SkinCD == skinCd && s.StyleGroupvalue == styleTypeCode)
                    .ToList();
                if (lstSkinStyles != null && lstSkinStyles.Count > 0)
                    foreach (var style in lstSkinStyles)
                    {
                        docStyle = new DocStyle();
                        docStyle.DocumentID = documentId;
                        docStyle.StyleCD = style.StyleCD;
                        docStyle.Value = style.Value;
                        docStyles.Add(docStyle);
                    }
            }
            catch (Exception ex)
            {
                var htParams = new Hashtable(1);
                htParams.Add("SkinCD", skinCd);
                var objMethod = (MethodInfo) MethodBase.GetCurrentMethod();
                GlobalExceptionHandler.customExceptionHandler(ex, objMethod, htParams, ErrorSeverityType.Normal);
            }
        }
        
        public void SortExperienceReverseChronological(Section experienceSection)
        {
            var allStartDates = new Dictionary<int, DateTime>();
            var allEndDates = new Dictionary<int, DateTime>();

            // get all experience start and end dates
            GetAllExperienceStartNEndDates(experienceSection, ref allStartDates, ref allEndDates);
            SetExperienceParaOrdering(experienceSection, allStartDates, allEndDates);
        }

        private static void SetExperienceParaOrdering(Section experienceSection,
            Dictionary<int, DateTime> allStartDates, Dictionary<int, DateTime> allEndDates)
        {
            short counter = 0;
            var prevDate = new DateTime();
            foreach (var endDate in allEndDates)
            {
                if (prevDate != endDate.Value)
                    prevDate = endDate.Value;
                else
                    continue;
                // Get Items for Which End Date is Same
                var sameEndDates = allEndDates.Where(item => item.Value == endDate.Value)
                    .ToDictionary(item => item.Key, item => item.Value);
                if (sameEndDates.Count > 1)
                {
                    foreach (var item in allStartDates)
                        // Check if the Key in [lst] is Same as in [item]
                        if (sameEndDates.ContainsKey(item.Key))
                            // set the Paragraphs SortIndex
                            if (experienceSection.Paragraphs.FirstOrDefault(p => p.Id == item.Key) != null)
                            {
                                experienceSection.Paragraphs.FirstOrDefault(p => p.Id == item.Key).SortIndex = counter;
                                counter++;
                            }
                }
                else
                {
                    if (experienceSection.Paragraphs.FirstOrDefault(p => p.Id == endDate.Key) != null)
                    {
                        experienceSection.Paragraphs.FirstOrDefault(p => p.Id == endDate.Key).SortIndex = counter;
                        counter++;
                    }
                }
            }

            foreach (var para in experienceSection.Paragraphs)
                if (!allEndDates.ContainsKey(para.Id))
                {
                    para.SortIndex = counter;
                    counter++;
                }

            DocumentFactory.SectionDAL.Save(experienceSection, true);
        }

        private void GetAllExperienceStartNEndDates(Section experienceSection,
            ref Dictionary<int, DateTime> allStartDates, ref Dictionary<int, DateTime> allEndDates)
        {
            foreach (var para in experienceSection.Paragraphs)
            {
                var docdata = para.DocData.FirstOrDefault(dd => dd.FieldCD == FieldTypeCD.JobStartDate);
                if (docdata != null && !string.IsNullOrWhiteSpace(docdata.CharValue))
                {
                    var dtStartDate = ConvertCharValueToDateTime(docdata.CharValue);
                    allStartDates.Add(para.Id, dtStartDate);
                }

                docdata = para.DocData.FirstOrDefault(dd => dd.FieldCD == FieldTypeCD.JobEndDate);
                if (docdata != null && docdata.CharValue != null && !string.IsNullOrWhiteSpace(docdata.CharValue))
                {
                    if (docdata.CharValue.ToUpper() == Resource.Present.ToUpper() ||
                        docdata.CharValue.ToUpper() == Resource.Current.ToUpper())
                    {
                        allEndDates.Add(para.Id, DateTime.Now);
                    }
                    else
                    {
                        var dtEndDate = ConvertCharValueToDateTime(docdata.CharValue);
                        allEndDates.Add(para.Id, dtEndDate);
                    }
                }
            }

            allStartDates = (from startDate in allStartDates orderby startDate.Value descending select startDate)
                .ToDictionary(pair => pair.Key, pair => pair.Value);
            allEndDates = (from endDate in allEndDates orderby endDate.Value descending select endDate)
                .ToDictionary(pair => pair.Key, pair => pair.Value);
        }

        private DateTime ConvertCharValueToDateTime(string charValue)
        {
            var dt = new DateTime(1900, 1, 1);
            var month = -1;
            var year = -1;

            if (!string.IsNullOrEmpty(charValue))
            {
                var strDateParts = charValue.Split('/');
                if (strDateParts.Length == 2)
                    if (int.TryParse(strDateParts[0], out month))
                        if (int.TryParse(strDateParts[1], out year))
                            if (month >= 1 && month <= 12 && year >= 1799)
                                // Prepare Date As Per CharValue, As we store Only Month/Year, HardCoded DatePart to 01
                                dt = new DateTime(year, month, 01);
            }

            return dt;
        }

        public void SortEducationReverseChronological(Section educationSection)
        {
            var allEndDates = new Dictionary<int, DateTime>();

            allEndDates = GetAllEducationEndDates(educationSection);

            short counter = 0;

            // set index order for paragraphs having end dates
            foreach (var endDate in allEndDates)
            {
                educationSection.Paragraphs.FirstOrDefault(x => x.Id == endDate.Key).SortIndex = counter;
                counter++;
            }

            // set index order for remaining paragraphs 
            foreach (var para in educationSection.Paragraphs)
                if (!allEndDates.ContainsKey(para.Id))
                {
                    para.SortIndex = counter;
                    counter++;
                }

            DocumentFactory.SectionDAL.Save(educationSection, true);
        }

        private Dictionary<int, DateTime> GetAllEducationEndDates(Section educationSection)
        {
            var lstEndDates = new Dictionary<int, DateTime>();
            foreach (var para in educationSection.Paragraphs)
            {
                var docdata = para.DocData.FirstOrDefault(dd => dd.FieldCD == FieldTypeCD.GraduationYear);
                if (docdata != null && docdata.CharValue != null && !string.IsNullOrWhiteSpace(docdata.CharValue))
                {
                    if (docdata.CharValue.ToUpper() == Resource.Present.ToUpper() ||
                        docdata.CharValue.ToUpper() == Resource.Current.ToUpper())
                    {
                        lstEndDates.Add(para.Id, DateTime.Now);
                    }
                    else
                    {
                        var dtEndDate = ConvertCharValueToDateTime(docdata.CharValue);
                        lstEndDates.Add(para.Id, dtEndDate);
                    }
                }
            }

            lstEndDates = (from endDate in lstEndDates orderby endDate.Value descending select endDate)
                .ToDictionary(pair => pair.Key, pair => pair.Value);
            return lstEndDates;
        }

        public IList<SkinStyle> UpdateDocStyleToDefaultSettings(int documentID, string skinCD,
            string styleTypeCd = Styles.Default)
        {
            DocStyle docStyle = null;
            Document document = null;
            IList<SkinStyle> lstSkinStyles = null;

            try
            {
                document = DocumentFactory.DocumentDAL.GetById(documentID);

                if (document != null)
                {
                    lstSkinStyles = GetAllSkinStyles.Where(s => s.SkinCD == skinCD && s.StyleGroupvalue == styleTypeCd)
                        .ToList();
                    if (lstSkinStyles != null && lstSkinStyles.Count > 0)
                        foreach (var style in lstSkinStyles)
                        {
                            docStyle = document.DocStyles.FirstOrDefault(item => item.StyleCD == style.StyleCD);
                            if (docStyle != default(DocStyle))
                            {
                                docStyle.Value = style.Value;
                            }
                            else
                            {
                                docStyle = new DocStyle();
                                docStyle.StyleCD = style.StyleCD;
                                docStyle.Value = style.Value;
                                document.DocStyles.Add(docStyle);
                            }
                        }

                    if (!string.IsNullOrEmpty(skinCD))
                        document.SkinCD = skinCD;
                    document.DateModified = DateTime.Now;
                    DocumentFactory.DocumentDAL.SaveOrUpdate(document, true);
                }
            }
            catch (Exception ex)
            {
                var htErrorArgs = new Hashtable(2);
                htErrorArgs.Add("Document", document != null ? document.ToString() : "Doc is NULL");
                htErrorArgs.Add("SkinCD", skinCD);
                var objMethod = (MethodInfo) MethodBase.GetCurrentMethod();
                GlobalExceptionHandler.customExceptionHandler(ex, objMethod, htErrorArgs, ErrorSeverityType.Normal);
            }

            return lstSkinStyles;
        }
        
        /// <summary>
        /// This method will get section by section type from a document
        /// </summary>
        /// <param name="document"></param>
        /// <param name="sectioncode"></param>
        /// <returns>Section</returns>
        public Section GetSectionByCode(Document document, string sectioncode)
        {
            Section section = null;

            try
            {
                if (document != null && document.Sections.Count > 0)
                {
                    section = document.Sections.FirstOrDefault(sec => sec.SectionTypeCD.Equals(sectioncode));

                    if (section != null && section != default(Section)) return section;
                }
            }
            catch (Exception ex)
            {
                var htParams = new Hashtable(2);
                htParams.Add("SectionCD", sectioncode);
                htParams.Add("Document", document);
                var objMethod = (MethodInfo) MethodBase.GetCurrentMethod();
                GlobalExceptionHandler.customExceptionHandler(ex, objMethod, htParams, ErrorSeverityType.Normal);
            }

            return section;
        }
       
        /// <summary>
        ///     GetAttachmentStream
        /// </summary>
        /// <param name="sFileFormatType"></param>
        /// <param name="dmFile"></param>
        /// <returns></returns>
        public MSIO.Stream GetAttachmentStream(string sFileFormatType, File dmFile)
        {
            MSIO.Stream fileStream = null;

            try
            {
                if (sFileFormatType == FileTypeCD.Text || sFileFormatType == FileTypeCD.HTM ||
                    sFileFormatType == FileTypeCD.HTML)
                {
                    var buffer = new byte[dmFile.TextData.Length];
                    var encoding = new ASCIIEncoding();
                    buffer = encoding.GetBytes(dmFile.TextData);
                    fileStream = new MSIO.MemoryStream(buffer);
                }
                else
                {
                    fileStream = new MSIO.MemoryStream(dmFile.BinaryData);
                }
            }

            catch (Exception e)
            {
                var htErrorArgs = new Hashtable(3);
                htErrorArgs.Add("FileID", dmFile.Id.ToString());
                htErrorArgs.Add("sFileFormatType", sFileFormatType);
                var objMethod = (MethodInfo) MethodBase.GetCurrentMethod();
                GlobalExceptionHandler.customExceptionHandler(e, objMethod, htErrorArgs, ErrorSeverityType.Normal);
            }

            return fileStream;
        }

        private int selectStyleCodeFromList(List<DocStyle> docStyles, string styleTypeCD)
        {
            var value = 0;
            if (docStyles != null && docStyles.Count > 0)
            {
                var dmDocStyle = docStyles.Where(ds => ds.StyleCD == styleTypeCD).FirstOrDefault();
                if (dmDocStyle != null) int.TryParse(dmDocStyle.Value, out value);
            }

            return value;
        }
    }
}