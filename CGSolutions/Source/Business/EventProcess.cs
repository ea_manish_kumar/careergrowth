﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Reflection;
using CommonModules.Exceptions;
using DataAccess.Event;
using DomainModel;
using DomainModel.Event;

namespace BusinessLogicProcess.Event
{
    /// <summary>
    ///
    /// </summary>
    public class EventProcess
    {
        /// <summary>
        /// Creates the document event.
        /// </summary>
        /// <param name="objDoc">The obj doc.</param>
        /// <param name="EventCodeType">Type of the event code.</param>
        /// <returns></returns>
        private static Guid CreateDocumentEvent(Document objDoc, DocumentEventCodeType EventCodeType)
        {
            var objDocumentEvent = new DocumentEvent();
            EventCode eventCode = null;
            try
            {
                eventCode = EventDataFactory.EventCodeData.GetById((int)EventCodeType);

                if (objDoc != null)
                {
                    objDocumentEvent.Documents.Add(objDoc);
                    objDocumentEvent.UserID = objDoc.UserID;
                    objDocumentEvent.EventCode = eventCode;
                    objDocumentEvent.Timestamp = DateTime.Now;
                    EventDataFactory.DocumentEventData.Merge("DocumentEvent", objDocumentEvent);                    
                }
            }
            catch (Exception e)
            {
                var ht = new Hashtable(3);
                ht.Add("Document", objDoc.ToString());
                ht.Add("DocumentEvent", objDocumentEvent.ToString());
                ht.Add("EventCode", eventCode.ToString());
                var objMethod = (MethodInfo)MethodBase.GetCurrentMethod();
                GlobalExceptionHandler.customExceptionHandler(e, objMethod, ht, ErrorSeverityType.High);
            }
            return objDocumentEvent.Id;
        }

        /// <summary>
        /// Updates the event data.
        /// </summary>
        /// <param name="datalist">The datalist.</param>
        /// <param name="typeCD">The type CD.</param>
        /// <param name="documenteventID">The documentevent ID.</param>
        /// <param name="SkipTypeCode">if set to <c>true</c> [skip type code].</param>
        private static void UpdateEventData(IList<EventData> datalist, string typeCD, Guid documenteventID, bool SkipTypeCode)
        {
            if (datalist != null)
                foreach (var data in datalist)
                {
                    if (!SkipTypeCode) data.DataTypeCD = typeCD;
                    data.Id = documenteventID;
                }
        }

        /// <summary>
        /// Creates the document event.
        /// </summary>
        /// <param name="objDoc">The obj doc.</param>
        /// <param name="EventCodeType">Type of the event code.</param>
        /// <param name="eventDataDetails">The event data details.</param>
        /// <returns></returns>
        public static Guid CreateDocumentEvent(Document objDoc, DocumentEventCodeType EventCodeType, string eventDataDetails)
        {
            try
            {
                var ed = new EventData
                {
                    EventDataDetails = eventDataDetails
                };
                IList<EventData> datalist = new List<EventData>();
                datalist.Add(ed);
                var objDocumentEvent = new DocumentEvent();
                objDocumentEvent = CreateDocumentEvent(objDoc, EventCodeType, datalist);
                return objDocumentEvent.Id;
            }
            catch (Exception e)
            {
                var ht = new Hashtable(3);
                ht.Add("Document", objDoc.ToString());
                ht.Add("eventDataDetails", eventDataDetails);
                ht.Add("EventCode", EventCodeType.ToString());
                var objMethod = (MethodInfo)MethodBase.GetCurrentMethod();
                GlobalExceptionHandler.customExceptionHandler(e, objMethod, ht, ErrorSeverityType.High);
                return Guid.Empty;
            }
        }

        /// <summary>
        /// Creates the document event.
        /// </summary>
        /// <param name="objDoc">The obj doc.</param>
        /// <param name="EventCodeType">Type of the event code.</param>
        /// <param name="eventData">The event data.</param>
        /// <returns></returns>
        public static DocumentEvent CreateDocumentEvent(Document objDoc, DocumentEventCodeType EventCodeType, IList<EventData> eventData)
        {
            var objDocumentEvent = new DocumentEvent();
            var eventCode = new EventCode();
            try
            {                
                eventCode = EventDataFactory.EventCodeData.GetById((int)EventCodeType);
                if (objDoc != null && eventData != null)
                {
                    objDocumentEvent.Documents.Add(objDoc);
                    objDocumentEvent.UserID = objDoc.UserID;
                    objDocumentEvent.EventCode = eventCode;
                    objDocumentEvent.Timestamp = DateTime.Now;
                    EventDataFactory.DocumentEventData.Merge("DocumentEvent", objDocumentEvent);
                    switch (eventCode.EventCodeType)
                    {
                        case EventCodeCD.DocumentCreated:
                            UpdateEventData(eventData, EventDataTypeCD.DoumentName, objDocumentEvent.Id, false);
                            break;
                        case EventCodeCD.DocumentRenamed:
                            UpdateEventData(eventData, EventDataTypeCD.DoumentName, objDocumentEvent.Id, false);
                            break;
                        case EventCodeCD.DocumentCopied:
                            UpdateEventData(eventData, EventDataTypeCD.DoumentNewName, objDocumentEvent.Id, false);
                            break;
                        case EventCodeCD.DocumentDownloaded:
                            UpdateEventData(eventData, EventDataTypeCD.DoumentFormat, objDocumentEvent.Id, false);
                            break;
                        case EventCodeCD.DocumentEmailed:
                            UpdateEventData(eventData, EventDataTypeCD.DoumentFormat, objDocumentEvent.Id, false);
                            break;
                        case EventCodeCD.SharedResourceCreated:
                            UpdateEventData(eventData, EventDataTypeCD.Document, objDocumentEvent.Id, false);
                            break;
                        case EventCodeCD.UserContacted:
                            UpdateEventData(eventData, EventDataTypeCD.Document, objDocumentEvent.Id, true);
                            break;
                        case EventCodeCD.DocumentUpdated:
                            UpdateEventData(eventData, EventDataTypeCD.Document, objDocumentEvent.Id, false);
                            break;
                        case EventCodeCD.DocumentDeleted:
                            UpdateEventData(eventData, EventDataTypeCD.Document, objDocumentEvent.Id, false);
                            break;
                        case EventCodeCD.TexttunerClick:
                            UpdateEventData(eventData, EventDataTypeCD.Document, objDocumentEvent.Id, false);
                            break;
                        case EventCodeCD.SharedDocumentCopied:
                            UpdateEventData(eventData, EventDataTypeCD.DoumentNewName, objDocumentEvent.Id, true);
                            break;
                        default:
                            UpdateEventData(eventData, EventDataTypeCD.DoumentName, objDocumentEvent.Id, false);
                            break;
                    }
                    foreach (var data in eventData) objDocumentEvent.AddEventData(data);
                    EventDataFactory.DocumentEventData.Merge("DocumentEvent", objDocumentEvent);
                }
            }
            catch (Exception e)
            {
                var ht = new Hashtable(2);
                ht.Add("DocumentEvent", objDocumentEvent.ToString());
                ht.Add("EventCode", eventCode.ToString());
                var objMethod = (MethodInfo)MethodBase.GetCurrentMethod();
                GlobalExceptionHandler.customExceptionHandler(e, objMethod, ht, ErrorSeverityType.High);
            }
            return objDocumentEvent;
        }

        /// <summary>
        /// Creates the event data.
        /// </summary>
        /// <param name="lstEventdata">The LST eventdata.</param>
        ///
        [Obsolete("This method is no longer to be used. Add Data to UserEvent.AddEventData() instead")]
        public static void CreateEventData(IList<EventData> lstEventdata)
        {
            try
            {
                if (lstEventdata != null)
                    foreach (var ev in lstEventdata) EventDataFactory.EventDataData.Save(ev, true);
            }
            catch (Exception ex)
            {
                var ht = new Hashtable(2);
                ht.Add("lstEventdata", lstEventdata);
                var objMethod = (MethodInfo)MethodBase.GetCurrentMethod();
                GlobalExceptionHandler.customExceptionHandler(ex, objMethod, ht, ErrorSeverityType.High);
            }
        }

        /// <summary>
        /// Creates the user event.
        /// </summary>
        /// <param name="EventCode">The event code.</param>
        /// <param name="lstEventdata">The LST eventdata.</param>
        public static void CreateUserEvent(EventCode EventCode, IList<EventData> lstEventdata)
        {
            UserEvent Ue = null;
            try
            {
                if (lstEventdata != null)
                {
                    Ue = new UserEvent
                    {
                        EventCode = EventCode,
                        Timestamp = DateTime.Now
                    };
                    foreach (var ed in lstEventdata) Ue.AddEventData(ed);
                    //EventDataFactory.UserEventData.Save(Ue);
                    EventDataFactory.UserEventData.Merge("UserEvent", Ue);
                }
            }
            catch (Exception ex)
            {
                var ht = new Hashtable(2);
                ht.Add("EventCode", EventCode);
                ht.Add("IList<EventData>", lstEventdata);
                var objMethod = (MethodInfo)MethodBase.GetCurrentMethod();
                GlobalExceptionHandler.customExceptionHandler(ex, objMethod, ht, ErrorSeverityType.Low);
            }
        }

        #region Process Event Log

        ///// <summary>
        ///// Log Event
        ///// </summary>
        ///// <param name="documentID"></param>
        ///// <param name="eventCodeID"></param>
        ///// <param name="eventData"></param>
        ///// <returns></returns>
        //public static Guid LogEvent(int documentID, int eventCodeID, IList<EventData> eventData)
        //{
        //    //Log DocumentEvent
        //    DocumentEvent objDocumentEvent = LogDocumentEvent(documentID, eventCodeID, eventData);
        //    return objDocumentEvent.Id;
        //}

        ///// <summary>
        ///// Log Document Event
        ///// </summary>
        ///// <param name="documentID"></param>
        ///// <param name="eventCodeTypeID"></param>
        ///// <param name="eventData"></param>
        ///// <returns></returns>
        //private static DocumentEvent LogDocumentEvent(int documentID, int eventCodeID, IList<EventData> eventData)
        //{
        //    DocumentEvent objDocumentEvent = new DocumentEvent();
        //    EventCode eventCode = null;
        //    Document document = null;

        //    try
        //    {
        //        //Get Document from DocumentID
        //        document = DocumentDAL.GetById(documentID);

        //        //Get EventCode from EventCodeID
        //        eventCode = EventDataFactory.EventCodeData.GetById(eventCodeID);

        //        //If document not null then fill data for UserEvent/DocumentEvent
        //        if (document != null && eventData != null && eventCode != null)
        //        {
        //            objDocumentEvent.Documents.Add(document);
        //            objDocumentEvent.UserID = (int)document.UserID;
        //            objDocumentEvent.EventCode = eventCode;
        //            objDocumentEvent.Timestamp = DateTime.Now;

        //            //Save in UserEvent and EventDocument tables
        //            //EventDataFactory.DocumentEventData.Save(objDocumentEvent, true);
        //            EventDataFactory.DocumentEventData.Merge("DocumentEvent", objDocumentEvent);

        //            //If eventData not null then fill data for EventData
        //            //if (eventData != null)//Sachin: Not reqd.
        //            //{
        //            foreach (EventData data in eventData)
        //            {
        //                data.Id = objDocumentEvent.Id;
        //                objDocumentEvent.AddEventData(data);
        //            }

        //            //Save in EventData table
        //            foreach (EventData ed in objDocumentEvent.EventDataCollection)
        //            {
        //                EventDataFactory.EventDataData.Save(ed, true);
        //            }

        //        }
        //    }
        //    catch (Exception e)
        //    {
        //        Hashtable ht = new Hashtable(3);
        //        ht.Add("Document", document.ToString());
        //        ht.Add("DocumentEvent", objDocumentEvent.ToString());
        //        ht.Add("EventCode", eventCode.ToString());
        //        MethodInfo objMethod = (MethodInfo)MethodBase.GetCurrentMethod();
        //        GlobalExceptionHandler.customExceptionHandler(e, objMethod, ht, ErrorSeverityType.High);
        //    }
        //    return objDocumentEvent;
        //}

        #endregion Process Event Log
    }
}
