﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Reflection;
using CommonModules.Exceptions;
using DataAccess.Billing;
using DomainModel.Billing;

namespace BusinessLogic.Billing
{
    public class BillingProcess
    {
        public IList<Subscription> GetAllSubscriptionsByUserId(int userId)
        {
            IList<Subscription> subscriptions = null;
            try
            {
                subscriptions = BillingFactory.SubscriptionDAL.GetAllSubscriptionsByUserId(userId);
            }
            catch (Exception ex)
            {
                var parameters = new Hashtable(1);
                parameters.Add("UserId", userId);
                var method = (MethodInfo) MethodBase.GetCurrentMethod();
                GlobalExceptionHandler.customExceptionHandler(ex, method, parameters, SubSystem.Billing,
                    Application.WebApp, ErrorSeverityType.High, null, true);
            }

            return subscriptions;
        }

        public Subscription CreateNewSubscription()
        {
            try
            {
                var subscription = new Subscription();
                BillingFactory.SubscriptionDAL.SaveOrUpdate(subscription, true);
                return subscription;
            }
            catch
            {
                return null;
            }
        }

        public Transaction AddBillingTransaction()
        {
            try
            {
                var transaction = new Transaction();
                BillingFactory.TransactionDAL.SaveOrUpdate(transaction, true);
                return transaction;
            }
            catch
            {
                return null;
            }
        }

        public IList<PlanType> GetAllPlanTypes()
        {
            IList<PlanType> planTypes = null;
            try
            {
                planTypes = BillingFactory.PlanTypeDAL.GetAll();
            }
            catch (Exception ex)
            {
                var parameters = new Hashtable();
                var method = (MethodInfo) MethodBase.GetCurrentMethod();
                GlobalExceptionHandler.customExceptionHandler(ex, method, parameters, SubSystem.Billing,
                    Application.WebApp, ErrorSeverityType.High, null, true);
            }

            return planTypes;
        }

        public IList<Plan> GetAllPlansAvailableForThisPortal()
        {
            IList<Plan> plans = null;
            try
            {
                plans = BillingFactory.PlanDAL.GetAll();
            }
            catch (Exception ex)
            {
                var parameters = new Hashtable();
                var method = (MethodInfo) MethodBase.GetCurrentMethod();
                GlobalExceptionHandler.customExceptionHandler(ex, method, parameters, SubSystem.Billing,
                    Application.WebApp, ErrorSeverityType.High, null, true);
            }

            return plans;
        }

        public Plan GetPlanById(int planId)
        {
            try
            {
                return BillingFactory.PlanDAL.GetById(planId);
            }
            catch (Exception ex)
            {
                var parameters = new Hashtable();
                var method = (MethodInfo) MethodBase.GetCurrentMethod();
                GlobalExceptionHandler.customExceptionHandler(ex, method, parameters, SubSystem.Billing,
                    Application.WebApp, ErrorSeverityType.High, null, true);
            }

            return null;
        }

        public IList<PlanTypeSkin> GetAllPlanTypeSkins()
        {
            IList<PlanTypeSkin> planTypeSkins = null;
            try
            {
                planTypeSkins = BillingFactory.PlanTypeSkinDAL.GetAll();
            }
            catch (Exception ex)
            {
                var parameters = new Hashtable();
                var method = (MethodInfo) MethodBase.GetCurrentMethod();
                GlobalExceptionHandler.customExceptionHandler(ex, method, parameters, SubSystem.Billing,
                    Application.WebApp, ErrorSeverityType.High, null, true);
            }

            return planTypeSkins;
        }

        public Order GetOrder(int orderId)
        {
            Order order = null;
            try
            {
                order = BillingFactory.OrderDAL.GetById(orderId);
            }
            catch (Exception ex)
            {
                var parameters = new Hashtable(1);
                parameters.Add("OrderId", orderId);
                var method = (MethodInfo) MethodBase.GetCurrentMethod();
                GlobalExceptionHandler.customExceptionHandler(ex, method, parameters, SubSystem.Billing,
                    Application.WebApp, ErrorSeverityType.High, null, true);
            }

            return order;
        }

        public void SaveOrUpdateOrder(Order order)
        {
            try
            {
                BillingFactory.OrderDAL.SaveOrUpdate(order, true);
            }
            catch (Exception ex)
            {
                var parameters = new Hashtable(1);
                parameters.Add("Order", order);
                var method = (MethodInfo) MethodBase.GetCurrentMethod();
                GlobalExceptionHandler.customExceptionHandler(ex, method, parameters, SubSystem.Billing,
                    Application.WebApp, ErrorSeverityType.High, null, true);
            }
        }

        public Order GetOrder(int userId, string statusCD)
        {
            try
            {
                return BillingFactory.OrderDAL.GetOrder(userId, statusCD);
            }
            catch (Exception ex)
            {
                var parameters = new Hashtable(2);
                parameters.Add("UserId", userId);
                parameters.Add("StatusCD", statusCD);
                var method = (MethodInfo) MethodBase.GetCurrentMethod();
                GlobalExceptionHandler.customExceptionHandler(ex, method, parameters, SubSystem.Billing,
                    Application.WebApp, ErrorSeverityType.High, null, true);
            }

            return null;
        }

        public IList<OrderItem> GetOrderItems(int orderId)
        {
            IList<OrderItem> orderItems = null;
            try
            {
                orderItems = BillingFactory.OrderItemDAL.GetAll();
            }
            catch (Exception ex)
            {
                var parameters = new Hashtable();
                var method = (MethodInfo) MethodBase.GetCurrentMethod();
                GlobalExceptionHandler.customExceptionHandler(ex, method, parameters, SubSystem.Billing,
                    Application.WebApp, ErrorSeverityType.High, null, true);
            }

            return orderItems;
        }
    }
}