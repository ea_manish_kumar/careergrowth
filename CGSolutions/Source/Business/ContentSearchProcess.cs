﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Text;
using CommonModules.Exceptions;
using DataAccess.Content;
using DomainModel.Content;
using Utilities;
using Utilities.Constants;
using Utilities.Constants.Documents;

namespace BusinessLogic.Content
{
    public class ContentSearchProcess
    {
        #region Constructor

        static ContentSearchProcess()
        {
            //For Cover Letter Builder
            WritingAssistanceData = PopulateWritingAssistanceData();
            SectionTipsContent = GetAllTips();
        }

        #endregion

        #region Variables

        private static IList<WritingAssistance> WritingAssistanceData;
        private static IList<Tips> SectionTipsContent;

        #endregion

        #region Properties

        private static IList<WritingAssistance> WritingAssistanceExamples
        {
            get
            {
                if (WritingAssistanceData == null)
                    WritingAssistanceData = PopulateWritingAssistanceData();
                return WritingAssistanceData;
            }
        }

        private static IList<Tips> SectionTips
        {
            get
            {
                if (SectionTipsContent == null) SectionTipsContent = GetAllTips();
                return SectionTipsContent;
            }
        }

        #endregion

        #region Private Methods

        private static IList<WritingAssistance> PopulateWritingAssistanceData()
        {
            try
            {
                return ContentFactory.WritingAssistanceDAL.GetAll();
            }
            catch (Exception ex)
            {
                var objMethod = (MethodInfo) MethodBase.GetCurrentMethod();
                GlobalExceptionHandler.customExceptionHandler(ex, objMethod, null, SubSystem.Content,
                    Application.WebApp, ErrorSeverityType.High, null, true);
            }

            return null;
        }

        private static IList<Tips> GetAllTips()
        {
            try
            {
                return ContentFactory.TipsDAL.GetAll();
            }
            catch (Exception ex)
            {
                var objMethod = (MethodInfo) MethodBase.GetCurrentMethod();
                GlobalExceptionHandler.customExceptionHandler(ex, objMethod, null, SubSystem.Content,
                    Application.WebApp, ErrorSeverityType.High, null, true);
            }

            return null;
        }

        #endregion

        #region Methods

        public static IList<string> GetJobTitles(string jobTitleStartingWith)
        {
            try
            {
                return ContentFactory.JobTitleDAL.GetJobTitles(jobTitleStartingWith);
            }
            catch (Exception ex)
            {
                var htParams = new Hashtable(1);
                var objMethod = (MethodInfo) MethodBase.GetCurrentMethod();
                GlobalExceptionHandler.customExceptionHandler(ex, objMethod, htParams, "DATABAS", "CG",
                    ErrorSeverityType.Normal, ex.Message, true);
            }

            return null;
        }

        public static IList<string> GetExamples(int sectionId, string jobTitle)
        {
            return ContentFactory.ContentDAL.GetExamples(sectionId, jobTitle);
        }

        public static IList<string> GetGenericExamples(int sectionId)
        {
            return ContentFactory.ContentDAL.GetGenericExamples(sectionId);
        }

        #endregion

        #region CoverLetter Data

        public static List<string> GetWritingAssistanceExamples(string templateCategoryCD, string templateCD,
            string sectionTypeCD)
        {
            try
            {
                var writingAssitanceData = FilterWritingAssistanceExamples(templateCD, sectionTypeCD,
                    WritingAssistanceTypeCodes.EXAMPLES, string.Empty);
                if (writingAssitanceData.NotNullOrEmpty())
                    return writingAssitanceData.ConvertAll(item => item.LocalizationKey);

                if (templateCategoryCD == LetterTypes.CoverLetter)
                {
                    // If not found, fetch writing assistance for default templateCd, default template is empty in table
                    writingAssitanceData = FilterWritingAssistanceExamples(string.Empty, sectionTypeCD,
                        WritingAssistanceTypeCodes.EXAMPLES, string.Empty);
                    if (writingAssitanceData != null)
                        return writingAssitanceData.ConvertAll(item => item.LocalizationKey);
                }
            }
            catch (Exception ex)
            {
                var ht = new Hashtable(3);
                ht.Add("TemplateCategoryCD", templateCategoryCD);
                ht.Add("TemplateCD", templateCD);
                ht.Add("SectionTypeCD", sectionTypeCD);
                var objMethod = (MethodInfo) MethodBase.GetCurrentMethod();
                GlobalExceptionHandler.customExceptionHandler(ex, objMethod, ht, ErrorSeverityType.Critical);
            }

            return null;
        }

        public static List<WritingAssistance> FilterWritingAssistanceExamples(string templateCd, string sectionTypeCd,
            string writingAssitanceTypeCd, string templateFormatCd)
        {
            if (string.IsNullOrEmpty(templateCd))
                templateCd = null;

            var examples = WritingAssistanceExamples.Where(x =>
                x.TemplateCD == templateCd && x.WritingAssistanceTypeCD == writingAssitanceTypeCd &&
                x.Status == StatusCodes.ACTIVE);

            if (!string.IsNullOrEmpty(sectionTypeCd))
                examples = examples.Where(x => x.SectionTypeCD == sectionTypeCd);

            if (!string.IsNullOrEmpty(templateFormatCd))
                examples = examples.Where(x => x.TemplateFormatCD == templateFormatCd);

            if (examples != null && examples.Any())
            {
                examples = examples.OrderBy(z => z.SortIndex);
                return examples.ToList();
            }

            return null;
        }

        public List<WritingAssistance> GetWritingAssistance(string sectionTypeCD, string[] writingAssistanceTypes)
        {
            var writingAssistanceDataList = new List<WritingAssistance>();
            try
            {
                foreach (var writingAssistanceType in writingAssistanceTypes)
                {
                    List<WritingAssistance> writingAssistanceData = null;
                    if (!string.IsNullOrEmpty(sectionTypeCD))
                        writingAssistanceData = FilterWritingAssistanceExamples(string.Empty, sectionTypeCD,
                            writingAssistanceType, string.Empty);
                    if (writingAssistanceData.NotNullOrEmpty())
                        writingAssistanceDataList.AddRange(writingAssistanceData);
                }
            }
            catch (Exception ex)
            {
                var ht = new Hashtable(1);
                ht.Add("SectionTypeCD", sectionTypeCD);
                var objMethod = (MethodInfo) MethodBase.GetCurrentMethod();
                GlobalExceptionHandler.customExceptionHandler(ex, objMethod, ht, ErrorSeverityType.Critical);
            }

            return writingAssistanceDataList;
        }

        public string GetWritingAssistance(string templateCD, string templateFormatCD, string writingAssistanceTypeCD)
        {
            var writingAssistanceSections = new StringBuilder(string.Empty);
            try
            {
                List<WritingAssistance> writingAssistanceData = null;
                if (!string.IsNullOrEmpty(templateCD))
                    writingAssistanceData = FilterWritingAssistanceExamples(templateCD, string.Empty,
                        writingAssistanceTypeCD, templateFormatCD);
                if (writingAssistanceData.NotNullOrEmpty())
                {
                    writingAssistanceSections.Append(string.Join(",",
                        writingAssistanceData.ConvertAll(item => item.SectionTypeCD).ToArray()));
                    writingAssistanceSections = writingAssistanceSections.Replace("," + LetterSectionTypeCD.Contact, "")
                        .Replace(LetterSectionTypeCD.Contact + ",", "");
                    writingAssistanceSections = writingAssistanceSections.Replace("," + LetterSectionTypeCD.Name, "")
                        .Replace(LetterSectionTypeCD.Name + ",", "");
                    writingAssistanceSections = writingAssistanceSections
                        .Replace("," + LetterSectionTypeCD.RecipientContact, "")
                        .Replace(LetterSectionTypeCD.RecipientContact + ",", "");
                    writingAssistanceSections = writingAssistanceSections.Replace("," + LetterSectionTypeCD.Date, "")
                        .Replace(LetterSectionTypeCD.Date + ",", "");
                }
            }
            catch (Exception ex)
            {
                var ht = new Hashtable(3);
                ht.Add("TemplateCD", templateCD);
                ht.Add("TemplateFormatCD", templateFormatCD);
                ht.Add("WritingAssistanceTypeCD", writingAssistanceTypeCD);
                var objMethod = (MethodInfo) MethodBase.GetCurrentMethod();
                GlobalExceptionHandler.customExceptionHandler(ex, objMethod, ht, ErrorSeverityType.Critical);
            }

            return writingAssistanceSections.ToString();
        }

        #endregion
    }
}