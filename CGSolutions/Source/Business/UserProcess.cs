﻿using System;
using System.Collections;
using System.Reflection;
using CommonModules.Exceptions;
using DataAccess.User;
using DomainModel.User;
using Utilities.HashingUtility;

namespace BusinessLogicProcess.User
{
    public class UserProcess
    {
        public UserMaster ValidateUser(string email, string sPassword, int portalId)
        {
            var user = UserFactory.UserDAL.GetUserByEmail(email, portalId);
            if (user != null)
                if (HashingUtility.ValidatePassword(sPassword, user.HashedPwd))
                    return user;

            return null;
        }

        public bool ValidatePassword(string hashedPassword, string password)
        {
            var isAuthenticateduser = false;

            if (!string.IsNullOrEmpty(hashedPassword))
                if (HashingUtility.ValidatePassword(password, hashedPassword))
                    isAuthenticateduser = true;
            return isAuthenticateduser;
        }

        public void UpdateUserToPremium(int iUserID)
        {
            UserMaster user = null;

            try
            {
                if (iUserID <= 0)
                    return;

                user = UserFactory.UserDAL.GetById(iUserID);
                if (user != null)
                {
                    user.UserLevelID = 1;
                    UserFactory.UserDAL.SaveOrUpdate(user, true);
                }
            }
            catch (Exception ex)
            {
                var htParams = new Hashtable(1);
                htParams.Add("UserID", iUserID);
                var objMethod = (MethodInfo) MethodBase.GetCurrentMethod();
                GlobalExceptionHandler.customExceptionHandler(ex, objMethod, htParams, ErrorSeverityType.Normal);
            }
        }

        public void UpdateUserToBasic(int iUserID)
        {
            UserMaster user = null;

            try
            {
                if (iUserID <= 0)
                    return;

                user = UserFactory.UserDAL.GetById(iUserID);
                if (user != null)
                {
                    user.UserLevelID = 0;
                    UserFactory.UserDAL.SaveOrUpdate(user, true);
                }
            }
            catch (Exception ex)
            {
                var htParams = new Hashtable(1);
                htParams.Add("UserID", iUserID);
                var objMethod = (MethodInfo) MethodBase.GetCurrentMethod();
                GlobalExceptionHandler.customExceptionHandler(ex, objMethod, htParams, ErrorSeverityType.Normal);
            }
        }
    }
}