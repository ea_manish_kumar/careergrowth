﻿namespace CommonModules.Caching
{
    /// <summary>
    ///     CGCacheItemPriority.
    /// </summary>
    public enum CustomCacheItemPriority
    {
        /// <summary>
        ///     None
        /// </summary>
        None = 0,

        /// <summary>
        ///     Low
        /// </summary>
        Low = 1,

        /// <summary>
        ///     Normal
        /// </summary>
        Normal = 2,

        /// <summary>
        ///     High
        /// </summary>
        High = 3,

        /// <summary>
        ///     Not Removable
        /// </summary>
        NotRemovable = 4
    }
}