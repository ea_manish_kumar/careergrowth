using System;
using System.Reflection;
using System.Runtime.Remoting.Messaging;
using System.Web;
using CommonModules.Exceptions;

namespace CommonModules.Caching
{
    /// <summary>
    ///     Provides access to the current CallContext, HttpRequest and few properties that persist data using the
    ///     CallContext.Items cache.
    /// </summary>
    public class ContextCacher
    {
        private const string _sessionContextKey = "NHibernateSession-ContextKey";

        #region Public Properties

        /// <summary>
        ///     Gets the Current HTTPRequest, either from the CallContext
        ///     or the WebORB ThreadContext
        /// </summary>
        /// <value>The current request.</value>
        public static HttpRequest CurrentRequest
        {
            get
            {
                if (IsWebContext)
                    return HttpContext.Current.Request;
                //else if (Weborb.Util.ThreadContext.currentHttpContext() != null)
                //    return Weborb.Util.ThreadContext.currentHttpContext().Request;

                return null;
            }
        }

        /// <summary>
        ///     UserID of the authenticated user for the current http request
        /// </summary>
        /// <value>The party ID.</value>
        public static int? UserID
        {
            get
            {
                //Got rid of -1 as default.
                int? _UserID = null;

                try
                {
                    if (GetDataFromContext("UserID") != null)
                        _UserID = GetDataFromContext("UserID").ToString().ToNullableInt32();
                }
                catch
                {
                }

                return _UserID;
            }
            set => AddDataToContext("UserID", value);
        }


        /// <summary>
        ///     Returns a boolean indicating whether the current user session is a "backdoor" Session
        /// </summary>
        /// <value>
        ///     <c>true</c> if this instance is backdoor session; otherwise, <c>false</c>.
        /// </value>
        public static bool isBackdoorSession
        {
            get
            {
                var retval = false;

                try
                {
                    if (GetDataFromContext("isBackdoorSession") != null)
                        retval = (bool) GetDataFromContext("isBackdoorSession");
                }
                catch
                {
                }

                return retval;
            }
            set
            {
                AddDataToContext("isBackdoorSession", value);

                //If it is a backdoor session, then it is automatically an UnAuthenticated Request
                if (value)
                    isUnAuthenticatedRequest = true;
            }
        }

        /// <summary>
        ///     Returns a boolean indicating whether the current Request is an UnAuthenticated one
        /// </summary>
        /// <value>
        ///     <c>true</c> if this instance is un authenticated request; otherwise, <c>false</c>.
        /// </value>
        public static bool isUnAuthenticatedRequest
        {
            get
            {
                var retval = false;

                try
                {
                    if (GetDataFromContext("isUnAuthenticatedRequest") != null)
                        retval = (bool) GetDataFromContext("isUnAuthenticatedRequest");
                }
                catch
                {
                }

                return retval;
            }
            set => AddDataToContext("isUnAuthenticatedRequest", value);
        }

        /// <summary>
        ///     Returns a boolean indicating whether the current Request has been authenticated
        /// </summary>
        /// <value><c>true</c> if [request authenticated]; otherwise, <c>false</c>.</value>
        public static bool RequestAuthenticated
        {
            get
            {
                var retval = false;

                try
                {
                    if (GetDataFromContext("RequestAuthenticated") != null)
                        retval = (bool) GetDataFromContext("RequestAuthenticated");
                }
                catch
                {
                }

                return retval;
            }
            set => AddDataToContext("RequestAuthenticated", value);
        }

        /// <summary>
        ///     Returns the SessionGUID for the current Request
        /// </summary>
        /// <value>The session GUID.</value>
        public static string SessionGUID
        {
            get
            {
                var retval = string.Empty;

                try
                {
                    if (GetDataFromContext("SessionGUID") != null)
                        retval = (string) GetDataFromContext("SessionGUID");
                }
                catch
                {
                }

                return retval;
            }
            set => AddDataToContext("SessionGUID", value);
        }

        /// <summary>
        ///     Gets or sets the NHSession object in the CallContext.
        /// </summary>
        /// <value>The NH session object .</value>
        public static object CurrentNHSession
        {
            get => GetDataFromContext(_sessionContextKey);
            set => AddDataToContext(_sessionContextKey, value);
        }

        /// <summary>
        ///     Gets a value indicating whether this instance is web context.
        /// </summary>
        /// <value>
        ///     <c>true</c> if this instance is web context; otherwise, <c>false</c>.
        /// </value>
        public static bool IsWebContext => HttpContext.Current != null;

        #endregion

        #region Public Methods

        /// <summary>
        ///     Allows the application to add some data to CallContext's Items collection.
        /// </summary>
        /// <param name="key">The key with which to add. Any previous data will be removed.</param>
        /// <param name="data">The data to add.</param>
        public static void AddDataToContext(string key, object data)
        {
            if (string.IsNullOrEmpty(key)) return;

            if (IsWebContext)
            {
                if (HttpContext.Current.Items.Contains(key))
                    HttpContext.Current.Items.Remove(key);

                HttpContext.Current.Items[key] = data;
            }
            else
            {
                CallContext.FreeNamedDataSlot(key);
                CallContext.SetData(key, data);
            }
        }

        /// <summary>
        ///     Gets the data matching the provided key from CallContext's Items collection.
        /// </summary>
        /// <param name="key">The key.</param>
        /// <returns></returns>
        public static object GetDataFromContext(string key)
        {
            if (IsWebContext)
                return HttpContext.Current.Items[key];
            return CallContext.GetData(key);
        }

        /// <summary>
        ///     This method removes an existing NHibernate Session object from the HTTPContext for a WebORB request.
        ///     This method is called for all WebORB requests to ensure that each method invocation
        ///     initiated by WebORB is processed with a new NHibernate Session
        /// </summary>
        public static void RemoveCurrentNHSession()
        {
            try
            {
                //Check if a valid NHSession exists in the CallContext
                if (CurrentNHSession != null)
                {
                    if (IsWebContext)
                        HttpContext.Current.Items[_sessionContextKey] = null;
                    else
                        //Remove the NHSession from the CallContext
                        CallContext.SetData(_sessionContextKey, null);
                }
            }
            catch (Exception ex)
            {
                //Log the error and dont throw it
                var objMethod = (MethodInfo) MethodBase.GetCurrentMethod();
                GlobalExceptionHandler.customExceptionHandler(ex, objMethod, null, SubSystem.Framework,
                    Application.WebApp, ErrorSeverityType.High,
                    "Error while removing NHibernate Session from HTTPContext for WebORB handling", true);
            }
        }

        #endregion
    }
}