﻿using System;
using System.IO;
using System.Reflection;
using System.Diagnostics;
using Microsoft.Practices.EnterpriseLibrary.Caching;
using Microsoft.Practices.EnterpriseLibrary.Caching.Expirations;
using CommonModules.Logging;

namespace CommonModules.Caching
{

    /// <summary>
    /// Provides functionality to read and write from cache
    /// </summary>
    /// <creation date="11/5/2009" author="VJ"></creation>
    /// `
    public class RB3CacheManager
    {
        private static int DEFAULT_TIME_TO_CACHE = 60; // 60 minutes

        #region KeyPrefix declarations
        // NOTE: All key name prefixes need to be declared here, so that we end up with a central repository of all keys being used in the system.
        //private string FaxAccountUsageDetails_key = "FaxAccountUsageDetails";

        /// <summary>
        /// Cached instances of dynamically compiled web services proxies.
        /// </summary>
        public static string CachedWebServiceKey = "CachedWebServiceKey";
        
        /// <summary>
        /// The key with which the compiled XslCompiledTransform is cached
        /// </summary>
        public static string DocExportXslCompiledTransformKey = "DocExportXslCompiledTransform";

        /// <summary>
        /// The key with which the file BannedRemoteAddress.xml is cached
        /// </summary>
        public const string BannedUserAgentXmlFileNameCacheKey = "BannedUserAgent";

        /// <summary>
        /// The key with which the file BannedUserAgent.xml is cached
        /// </summary>
        public const string BannedRemoteAddressXmlFileNameCacheKey = "BannedRemoteAddress";


        #endregion KeyPrefix declarations

        #region Helper Methods
        /// <summary>
        /// Gets the RB3 cache.
        /// </summary>
        /// <returns></returns>
        /// <creation date="9/18/2009" author="VJ"></creation>
        private static CacheManager GetRB3Cache
        {
            get
            {
                return (CacheManager)CacheFactory.GetCacheManager("RB3CacheManager");
            }
        }

        /// <summary>
        /// Provides status about #items in the cache.
        /// </summary>
        /// <returns>status about #items in the cache.</returns>
        private static string WriteCacheStatus()
        {
            CacheManager objCacheManager = GetRB3Cache;
            return "# Items in cache - " + objCacheManager.Count.ToString() + "\n ";
        }

        #endregion  Helper Methods

        #region Public methods

        /// <summary>
        /// Clears cache of everything
        /// </summary>
        public static void ClearCache()
        {
            CacheManager cacheManager = GetRB3Cache;
            cacheManager.Flush();
            MethodInfo method = (MethodInfo)MethodBase.GetCurrentMethod();
            Log4NetManager.LogInformation(method, "RB3CacheManager::ClearCache() called. Cache status is-" + WriteCacheStatus());
        }

        /// <summary>
        /// Gets an object from cache
        /// </summary>
        /// <param name="key">The key with which the item was stored.</param>
        /// <returns>Cached object or null</returns>
        /// <creation date="11/5/2009" author="VJ"></creation>
        public static object GetFromCache(string key)
        {
            CacheManager cacheManager = GetRB3Cache;
            return cacheManager.GetData(key);
        }

        /// <summary>
        /// Removes a given item from cache
        /// </summary>
        /// <param name="key">The key with which the item was stored.</param>
        public static void RemoveFromCache(string key)
        {
            CacheManager cacheManager = GetRB3Cache;
            cacheManager.Remove(key);
        }

        /// <summary>
        /// This method can be used to check if an item matching the provided keyname already exists in cache.
        /// </summary>
        /// <param name="key">The key matching which the check need be perfomed.</param>
        /// <returns>true if found, else false.</returns>
        public static bool IsInCache(string key)
        {
            CacheManager cacheManager = GetRB3Cache;
            return cacheManager.Contains(key);
        }

        #endregion Public methods

        #region Cache with file dependency

        /// <summary>
        /// Saves a fiven file in cache and attaches FileDependency to it.
        /// </summary>
        /// <param name="key">The key to save the object with.</param>
        /// <param name="filePath">The path to the file to be cached.</param>
        public static void SaveFileInCache(string key, string filePath)
        {
            FileDependency dep = new FileDependency(filePath);
            string value = File.ReadAllText(filePath);
            SaveInCache(key, value, dep);
        }

        /// <summary>
        /// Saves a given object in cache and attaches a FileDependency for a provided file path to it.
        /// </summary>
        /// <param name="key">The key to save the object with.</param>
        /// <param name="value">The object to save </param>
        /// <param name="filePath">The path to the file upon which the object being cached depends.</param>
        public static void SaveInCache(string key, object value, string filePath)
        {
            FileDependency dep = new FileDependency(filePath);            
            SaveInCache(key, value, dep);
        }

        /// <summary>
        /// Saves the in cache with a file dependency attached..
        /// </summary>
        /// <param name="key">The key to save the object with.</param>
        /// <param name="value">The object to save.</param>
        /// <param name="dep">the file depenndency object</param>
        /// <creation date="11/5/2009" author="VJ"></creation>
        private static void SaveInCache(string key, object value, FileDependency dep)
        {
            LoggedCacheRefreshAction refreshAction = new LoggedCacheRefreshAction();    //any changes to the file will be logged
            SlidingTime sldtime = new SlidingTime(TimeSpan.FromMinutes(DEFAULT_TIME_TO_CACHE));
            ICacheItemExpiration[] expirations = new ICacheItemExpiration[] { dep, sldtime };

            CacheManager cacheManager = GetRB3Cache;
            cacheManager.Add(key, value, CacheItemPriority.Normal, refreshAction, expirations);
        }

        #endregion Cache with file dependency

        #region SaveInCache

        /// <summary>
        /// Saves the in cache.
        /// </summary>
        /// <param name="key">The key.</param>
        /// <param name="value">The value.</param>
        public static void SaveInCache(string key, object value)
        {
            SaveInCache(key, value, DEFAULT_TIME_TO_CACHE);
        }

        /// <summary>
        /// Saves a provided object in cache with expiration time and sliding flag.
        /// </summary>
        /// <param name="key">The key to save the object with.</param>
        /// <param name="value">The object to save.</param>
        /// <param name="minutesToExpiration">the minutes after which the cached item will expire.</param>
        public static void SaveInCache(string key, object value, int minutesToExpiration)
        {
            SaveInCache(key, value, minutesToExpiration, false);
        }

        /// <summary>
        /// Saves an item in cache with option to slide expiration
        /// </summary>
        /// <param name="key">The key to save the object with.</param>
        /// <param name="value">The object to save.</param>
        /// <param name="minutesToExpiration">the minutes after which the cached item will expire.</param>
        /// <param name="slidingExpiration">if true, sliding expiration is applied on the timespan value provided</param>
        public static void SaveInCache(string key, object value, int minutesToExpiration, bool slidingExpiration)
        {
            SaveInCache(key, value, RB3CacheItemPriority.Normal, TimeSpan.FromMinutes(minutesToExpiration), slidingExpiration);
        }

        /// <summary>
        /// Saves a provided object in cache with expiration time and sliding flag.
        /// </summary>
        /// <param name="key">The key to save the object with.</param>
        /// <param name="value">The object to save.</param>
        /// <param name="priority">Priority of the item, high priority items are fetched quickly and live longer.</param>
        /// <param name="timespan">The time till which an item remains in cache.</param>
        /// <param name="slidingExpiration">if true, sliding expiration is applied on the timespan value provided</param>
        public static void SaveInCache(string key, object value, RB3CacheItemPriority priority, TimeSpan timespan, bool slidingExpiration)
        {
            DefaultCustomCacheRefreshAction refreshAction = new DefaultCustomCacheRefreshAction();
            SaveInCache(key, value, GetCacheItemPriorityFromRB3CacheItemPriority(priority), refreshAction, timespan, slidingExpiration);
        }

        /// <summary>
        /// Saves a provided object in cache with expiration time and sliding flag.
        /// </summary>
        /// <param name="key">The key to save the object with.</param>
        /// <param name="value">The object to save.</param>
        /// <param name="priority">Priority of the item, high priority items are fetched quickly and live longer.</param>
        /// <param name="refreshAction">ICacheItemRefreshAction class's instance</param>
        /// <param name="timespan">The time till which an item remains in cache.</param>
        /// <param name="slidingExpiration">if true, sliding expiration is applied on the timespan value provided</param>
        public static void SaveInCache(string key, object value, CacheItemPriority priority, ICacheItemRefreshAction refreshAction, TimeSpan timespan, bool slidingExpiration)
        {
            CacheManager objCacheManager = GetRB3Cache;

            if (!slidingExpiration)
            {
                AbsoluteTime abstime = new AbsoluteTime(timespan);
                objCacheManager.Add(key, value, priority, refreshAction, abstime);

            }
            else
            {
                SlidingTime sldtime = new SlidingTime(timespan);
                objCacheManager.Add(key, value, priority, refreshAction, sldtime);
            }
        }
        #endregion SaveInCache

        /// <summary>
        /// Gets the CacheItemPriority from the provided RB3CacheItemPriority.
        /// </summary>
        /// <param name="RB3priority">RB3CacheItemPriority instance.</param>
        /// <returns></returns>
        private static CacheItemPriority GetCacheItemPriorityFromRB3CacheItemPriority(RB3CacheItemPriority RB3priority)
        {
            //   (8/12/2013): Check if a simple int case would have worked?

            CacheItemPriority priority = CacheItemPriority.Normal;

            switch (RB3priority)
            {
                case RB3CacheItemPriority.None:
                    priority = CacheItemPriority.None;
                    break;
                case RB3CacheItemPriority.Low:
                    priority = CacheItemPriority.Low;
                    break;
                case RB3CacheItemPriority.Normal:
                    priority = CacheItemPriority.Normal;
                    break;
                case RB3CacheItemPriority.High:
                    priority = CacheItemPriority.High;
                    break;
                case RB3CacheItemPriority.NotRemovable:
                    priority = CacheItemPriority.NotRemovable;
                    break;
                default:
                    priority = CacheItemPriority.None;
                    break;
            }

            return priority;
        }
    }



    #region ICacheItemRefreshAction implementations

    /// <summary>
    /// A custom class that implements the ICacheItemRefreshAction interface.
    /// The Refresh() method of the ICacheItemRefreshAction interface allows you to execute custom code
    /// whenever the item has been removed from the cache.
    /// </summary>
    [Serializable]
    public class DefaultCustomCacheRefreshAction : ICacheItemRefreshAction
    {

        /// <summary>
        /// Refreshes the specified key.
        /// </summary>
        /// <param name="key">The key.</param>
        /// <param name="expiredValue">The expired value.</param>
        /// <param name="removalReason">The removal reason.</param>
        public void Refresh(string key, object expiredValue, CacheItemRemovedReason removalReason)
        {
            // Item has been removed from cache. Perform desired actions here,  based upon the removal reason (e.g. refresh the cache with the item).
            Trace.WriteLine(String.Format("RB3CacheManager::CustomCacheRefreshAction()-> Item {0} removed from cache. Reason for removal is {1}", key, removalReason.ToString()));
        }
    }


    /// <summary>
    /// A custom class that implements the ICacheItemRefreshAction interface.
    /// The Refresh() method of the ICacheItemRefreshAction interface allows you to execute custom code
    /// whenever the item has been removed from the cache.
    /// This one logs to the default system log
    /// </summary>
    [Serializable]
    public class LoggedCacheRefreshAction : ICacheItemRefreshAction
    {
        /// <summary>
        /// Refreshes the specified key.
        /// </summary>
        /// <param name="key">The key.</param>
        /// <param name="expiredValue">The expired value.</param>
        /// <param name="removalReason">The removal reason.</param>
        public void Refresh(string key, object expiredValue, CacheItemRemovedReason removalReason)
        {
            // Item has been removed from cache. Perform desired actions here,  based upon the removal reason (e.g. refresh the cache with the item).
            MethodInfo method = (MethodInfo)MethodBase.GetCurrentMethod();
            Log4NetManager.LogInformation(method, String.Format("RB3CacheManager::CustomCacheRefreshAction()-> Item {0} removed from cache. Reason for removal is {1}", key, removalReason.ToString()));
        }
    }

    #endregion ICacheItemRefreshAction implementations
}
