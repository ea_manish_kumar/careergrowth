﻿using System;
using System.Collections.Generic;
using System.Runtime.Remoting.Messaging;
using System.Web;

namespace CommonModules.Caching
{
    public class ApiContextCacher
    {
        /// Gets the Current HTTPRequest from the CallContext
        /// </summary>
        /// <value>The current request.</value>
        public static HttpRequest CurrentRequest
        {
            get
            {
                if (IsWebContext)
                    return HttpContext.Current.Request;
                return null;
            }
        }

        /// <summary>
        ///     Gets the Current HTTPRequest from the CallContext
        /// </summary>
        /// <value>The current response.</value>
        public static HttpResponse CurrentResponse
        {
            get
            {
                if (IsWebContext)
                    return HttpContext.Current.Response;
                return null;
            }
        }

        /// <summary>
        ///     Gets or sets the NHSession object in the CallContext.
        /// </summary>
        /// <value>The NH session object .</value>
        public static object CurrentNHSession
        {
            get => GetDataFromContext(ApiContextCacherKey.SESSION_CONTEXT_KEY);
            set => AddDataToContext(ApiContextCacherKey.SESSION_CONTEXT_KEY, value);
        }

        /// <summary>
        ///     Gets or sets the NHSession object in the CallContext.
        /// </summary>
        /// <value>The NH session object .</value>
        public static string SessionGUID
        {
            get => (string) GetDataFromContext(ApiContextCacherKey.SESSION_GUID_KEY);
            set => AddDataToContext(ApiContextCacherKey.SESSION_GUID_KEY, value);
        }

        /// <summary>
        ///     Gets a value indicating whether this instance is web context.
        /// </summary>
        /// <value>
        ///     <c>true</c> if this instance is web context; otherwise, <c>false</c>.
        /// </value>
        public static bool IsWebContext => HttpContext.Current != null;

        /// <summary>
        ///     ClientCD of the authenticated client
        /// </summary>
        public static string ClientCD
        {
            get
            {
                if (GetDataFromContext("ClientCD") != null)
                    return GetDataFromContext("ClientCD").ToString();
                return "";
            }
            set => AddDataToContext("ClientCD", value);
        }

        /// <summary>
        ///     Client CD returned from the request token
        /// </summary>
        public static string TokenClientCD
        {
            get => GetDataFromContext("TokenClientCD").ToString();
            set => AddDataToContext("TokenClientCD", value);
        }

        /// <summary>
        ///     Client Request Reference number
        /// </summary>
        public static string ClientRequestReference
        {
            get
            {
                if (GetDataFromContext("ClientRequestReference") != null)
                    return GetDataFromContext("ClientRequestReference").ToString();
                return "";
            }
            set => AddDataToContext("ClientRequestReference", value);
        }

        /// <summary>
        ///     Request Id of the request
        /// </summary>
        public static int RequestID
        {
            get
            {
                var requestId = GetDataFromContext("RequestID");
                if (requestId != null)
                    return Convert.ToInt32(GetDataFromContext("RequestID"));
                return 0;
            }
            set => AddDataToContext("RequestID", value);
        }

        public static IDictionary<string, object> VariableValues
        {
            get
            {
                var _variableValues = GetDataFromContext("VariableValues");
                if (_variableValues == null)
                    _variableValues = new Dictionary<string, object>();

                return (IDictionary<string, object>) _variableValues;
            }
            set => AddDataToContext("VariableValues", value);
        }

        /// <summary>
        ///     Gets the Current HTTPApplicationState, either from the CallContext
        ///     or the WebORB ThreadContext
        /// </summary>
        public static HttpApplicationState CurrentApplicationState
        {
            get
            {
                if (IsWebContext)
                    return HttpContext.Current.Application;
                return null;
            }
        }

        /// <summary>
        ///     Allows the application to add some data to CallContext's Items collection.
        /// </summary>
        /// <param name="key">The key with which to add. Any previous data will be removed.</param>
        /// <param name="data">The data to add.</param>
        public static void AddDataToContext(string key, object data)
        {
            if (string.IsNullOrEmpty(key)) return;

            if (IsWebContext)
            {
                if (HttpContext.Current.Items.Contains(key))
                    HttpContext.Current.Items.Remove(key);

                HttpContext.Current.Items[key] = data;
            }
            else
            {
                CallContext.FreeNamedDataSlot(key);
                CallContext.SetData(key, data);
            }
        }

        /// <summary>
        ///     Gets the data matching the provided key from CallContext's Items collection.
        /// </summary>
        /// <param name="key">The key.</param>
        /// <returns></returns>
        public static object GetDataFromContext(string key)
        {
            if (IsWebContext)
                return HttpContext.Current.Items[key];
            return CallContext.GetData(key);
        }

        /// <summary>
        ///     Allows the application to add cookie and data to current context
        /// </summary>
        /// <param name="key">The key with which to add. Any previous data will be removed.</param>
        /// <param name="data">The data to add.</param>
        /// Added by Amit Dutta for LCMAIN-1807
        /// Changed by Amit Dutta for LCMAIN-1941
        public static void AddCookieToContext(string key, string data, string DomainName)
        {
            if (string.IsNullOrEmpty(key)) return;
            if (CurrentRequest != null && CurrentResponse != null)
            {
                HttpCookie cookie = null;
                if (CurrentRequest.Cookies[key] != null)
                {
                    var sCookieString = CurrentRequest.Cookies[key].Value;
                    if (sCookieString != null && sCookieString.Length > 0)
                        cookie = CurrentRequest.Cookies[key];
                    else
                        cookie = new HttpCookie(key);
                }
                else
                {
                    cookie = new HttpCookie(key);
                }

                if (cookie != null)
                {
                    cookie.Value = data;
                    cookie.Domain = DomainName;
                    CurrentResponse.Cookies.Add(cookie);
                }
            }
        }

        /// <summary>
        ///     Gets the cookie matching the provided key from CallContext's Items collection.
        /// </summary>
        /// <param name="key">The key.</param>
        /// <returns></returns>
        /// Added by Amit Dutta for LCMAIN-1807
        public static HttpCookie GetCookieFromContext(string key)
        {
            HttpCookie cookie = null;
            if (string.IsNullOrEmpty(key)) return cookie;
            if (CurrentRequest != null) cookie = CurrentRequest.Cookies[key];
            return cookie;
        }
    }

    public class ApiContextCacherKey
    {
        internal const string SESSION_CONTEXT_KEY = "RGSession-ContextKey";
        internal const string SESSION_GUID_KEY = "Session-GuidKey";
        public const string CULTURE_CD = "CultureCD";
        public const string REQUEST_ID = "RequestId";
    }
}