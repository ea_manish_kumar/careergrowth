﻿using System;
using System.Diagnostics;
using System.IO;
using System.Reflection;
using CommonModules.Logging;
using Microsoft.Practices.EnterpriseLibrary.Caching;
using Microsoft.Practices.EnterpriseLibrary.Caching.Expirations;

namespace CommonModules.Caching
{
    public class CGCacheManager
    {
        private static readonly int DEFAULT_TIME_TO_CACHE = 60; // 60 minutes

        /// <summary>
        ///     Gets the CacheItemPriority from the provided CacheItemPriority.
        /// </summary>
        /// <param name="CGpriority">CacheItemPriority instance.</param>
        /// <returns></returns>
        private static CacheItemPriority GetCacheItemPriorityFromCacheItemPriority(CacheItemPriority CGpriority)
        {
            //   (8/12/2013): Check if a simple int case would have worked?

            var priority = CacheItemPriority.Normal;

            switch (CGpriority)
            {
                case CacheItemPriority.None:
                    priority = CacheItemPriority.None;
                    break;
                case CacheItemPriority.Low:
                    priority = CacheItemPriority.Low;
                    break;
                case CacheItemPriority.Normal:
                    priority = CacheItemPriority.Normal;
                    break;
                case CacheItemPriority.High:
                    priority = CacheItemPriority.High;
                    break;
                case CacheItemPriority.NotRemovable:
                    priority = CacheItemPriority.NotRemovable;
                    break;
                default:
                    priority = CacheItemPriority.None;
                    break;
            }

            return priority;
        }

        #region KeyPrefix declarations

        // NOTE: All key name prefixes need to be declared here, so that we end up with a central repository of all keys being used in the system.
        //private string FaxAccountUsageDetails_key = "FaxAccountUsageDetails";

        /// <summary>
        ///     Cached instances of dynamically compiled web services proxies.
        /// </summary>
        public static string CachedWebServiceKey = "CachedWebServiceKey";

        /// <summary>
        ///     The key with which the compiled XslCompiledTransform is cached
        /// </summary>
        public static string DocExportXslCompiledTransformKey = "DocExportXslCompiledTransform";

        /// <summary>
        ///     The key with which the file BannedRemoteAddress.xml is cached
        /// </summary>
        public const string BannedUserAgentXmlFileNameCacheKey = "BannedUserAgent";

        /// <summary>
        ///     The key with which the file BannedUserAgent.xml is cached
        /// </summary>
        public const string BannedRemoteAddressXmlFileNameCacheKey = "BannedRemoteAddress";

        #endregion KeyPrefix declarations

        #region Helper Methods

        private static CacheManager GetCache => (CacheManager) CacheFactory.GetCacheManager("CGCacheManager");

        /// <summary>
        ///     Provides status about #items in the cache.
        /// </summary>
        /// <returns>status about #items in the cache.</returns>
        private static string WriteCacheStatus()
        {
            var objCacheManager = GetCache;
            return "# Items in cache - " + objCacheManager.Count + "\n ";
        }

        #endregion Helper Methods

        #region Public methods

        /// <summary>
        ///     Clears cache of everything
        /// </summary>
        public static void ClearCache()
        {
            var cacheManager = GetCache;
            cacheManager.Flush();
            var method = (MethodInfo) MethodBase.GetCurrentMethod();
            Log4NetManager.LogInformation(method,
                "CGCacheManager::ClearCache() called. Cache status is-" + WriteCacheStatus());
        }

        public static object GetFromCache(string key)
        {
            var cacheManager = GetCache;
            return cacheManager.GetData(key);
        }

        /// <summary>
        ///     Removes a given item from cache
        /// </summary>
        /// <param name="key">The key with which the item was stored.</param>
        public static void RemoveFromCache(string key)
        {
            var cacheManager = GetCache;
            cacheManager.Remove(key);
        }

        /// <summary>
        ///     This method can be used to check if an item matching the provided keyname already exists in cache.
        /// </summary>
        /// <param name="key">The key matching which the check need be perfomed.</param>
        /// <returns>true if found, else false.</returns>
        public static bool IsInCache(string key)
        {
            var cacheManager = GetCache;
            return cacheManager.Contains(key);
        }

        #endregion Public methods

        #region Cache with file dependency

        /// <summary>
        ///     Saves a fiven file in cache and attaches FileDependency to it.
        /// </summary>
        /// <param name="key">The key to save the object with.</param>
        /// <param name="filePath">The path to the file to be cached.</param>
        public static void SaveFileInCache(string key, string filePath)
        {
            var dep = new FileDependency(filePath);
            var value = File.ReadAllText(filePath);
            SaveInCache(key, value, dep);
        }

        /// <summary>
        ///     Saves a given object in cache and attaches a FileDependency for a provided file path to it.
        /// </summary>
        /// <param name="key">The key to save the object with.</param>
        /// <param name="value">The object to save </param>
        /// <param name="filePath">The path to the file upon which the object being cached depends.</param>
        public static void SaveInCache(string key, object value, string filePath)
        {
            var dep = new FileDependency(filePath);
            SaveInCache(key, value, dep);
        }

        private static void SaveInCache(string key, object value, FileDependency dep)
        {
            var refreshAction = new LoggedCacheRefreshAction(); //any changes to the file will be logged
            var sldtime = new SlidingTime(TimeSpan.FromMinutes(DEFAULT_TIME_TO_CACHE));
            ICacheItemExpiration[] expirations = {dep, sldtime};

            var cacheManager = GetCache;
            cacheManager.Add(key, value, CacheItemPriority.Normal, refreshAction, expirations);
        }

        #endregion Cache with file dependency

        #region SaveInCache

        /// <summary>
        ///     Saves the in cache.
        /// </summary>
        /// <param name="key">The key.</param>
        /// <param name="value">The value.</param>
        public static void SaveInCache(string key, object value)
        {
            SaveInCache(key, value, DEFAULT_TIME_TO_CACHE);
        }

        /// <summary>
        ///     Saves a provided object in cache with expiration time and sliding flag.
        /// </summary>
        /// <param name="key">The key to save the object with.</param>
        /// <param name="value">The object to save.</param>
        /// <param name="minutesToExpiration">the minutes after which the cached item will expire.</param>
        public static void SaveInCache(string key, object value, int minutesToExpiration)
        {
            SaveInCache(key, value, minutesToExpiration, false);
        }

        /// <summary>
        ///     Saves an item in cache with option to slide expiration
        /// </summary>
        /// <param name="key">The key to save the object with.</param>
        /// <param name="value">The object to save.</param>
        /// <param name="minutesToExpiration">the minutes after which the cached item will expire.</param>
        /// <param name="slidingExpiration">if true, sliding expiration is applied on the timespan value provided</param>
        public static void SaveInCache(string key, object value, int minutesToExpiration, bool slidingExpiration)
        {
            SaveInCache(key, value, CacheItemPriority.Normal, TimeSpan.FromMinutes(minutesToExpiration),
                slidingExpiration);
        }

        /// <summary>
        ///     Saves a provided object in cache with expiration time and sliding flag.
        /// </summary>
        /// <param name="key">The key to save the object with.</param>
        /// <param name="value">The object to save.</param>
        /// <param name="priority">Priority of the item, high priority items are fetched quickly and live longer.</param>
        /// <param name="timespan">The time till which an item remains in cache.</param>
        /// <param name="slidingExpiration">if true, sliding expiration is applied on the timespan value provided</param>
        public static void SaveInCache(string key, object value, CacheItemPriority priority, TimeSpan timespan,
            bool slidingExpiration)
        {
            var refreshAction = new DefaultCustomCacheRefreshAction();
            SaveInCache(key, value, GetCacheItemPriorityFromCacheItemPriority(priority), refreshAction, timespan,
                slidingExpiration);
        }

        /// <summary>
        ///     Saves a provided object in cache with expiration time and sliding flag.
        /// </summary>
        /// <param name="key">The key to save the object with.</param>
        /// <param name="value">The object to save.</param>
        /// <param name="priority">Priority of the item, high priority items are fetched quickly and live longer.</param>
        /// <param name="refreshAction">ICacheItemRefreshAction class's instance</param>
        /// <param name="timespan">The time till which an item remains in cache.</param>
        /// <param name="slidingExpiration">if true, sliding expiration is applied on the timespan value provided</param>
        public static void SaveInCache(string key, object value, CacheItemPriority priority,
            ICacheItemRefreshAction refreshAction, TimeSpan timespan, bool slidingExpiration)
        {
            var objCacheManager = GetCache;

            if (!slidingExpiration)
            {
                var abstime = new AbsoluteTime(timespan);
                objCacheManager.Add(key, value, priority, refreshAction, abstime);
            }
            else
            {
                var sldtime = new SlidingTime(timespan);
                objCacheManager.Add(key, value, priority, refreshAction, sldtime);
            }
        }

        #endregion SaveInCache
    }


    #region ICacheItemRefreshAction implementations

    /// <summary>
    ///     A custom class that implements the ICacheItemRefreshAction interface.
    ///     The Refresh() method of the ICacheItemRefreshAction interface allows you to execute custom code
    ///     whenever the item has been removed from the cache.
    /// </summary>
    [Serializable]
    public class DefaultCustomCacheRefreshAction : ICacheItemRefreshAction
    {
        /// <summary>
        ///     Refreshes the specified key.
        /// </summary>
        /// <param name="key">The key.</param>
        /// <param name="expiredValue">The expired value.</param>
        /// <param name="removalReason">The removal reason.</param>
        public void Refresh(string key, object expiredValue, CacheItemRemovedReason removalReason)
        {
            // Item has been removed from cache. Perform desired actions here,  based upon the removal reason (e.g. refresh the cache with the item).
            Trace.WriteLine(string.Format(
                "CGCacheManager::CustomCacheRefreshAction()-> Item {0} removed from cache. Reason for removal is {1}",
                key, removalReason.ToString()));
        }
    }


    /// <summary>
    ///     A custom class that implements the ICacheItemRefreshAction interface.
    ///     The Refresh() method of the ICacheItemRefreshAction interface allows you to execute custom code
    ///     whenever the item has been removed from the cache.
    ///     This one logs to the default system log
    /// </summary>
    [Serializable]
    public class LoggedCacheRefreshAction : ICacheItemRefreshAction
    {
        /// <summary>
        ///     Refreshes the specified key.
        /// </summary>
        /// <param name="key">The key.</param>
        /// <param name="expiredValue">The expired value.</param>
        /// <param name="removalReason">The removal reason.</param>
        public void Refresh(string key, object expiredValue, CacheItemRemovedReason removalReason)
        {
            // Item has been removed from cache. Perform desired actions here,  based upon the removal reason (e.g. refresh the cache with the item).
            var method = (MethodInfo) MethodBase.GetCurrentMethod();
            Log4NetManager.LogInformation(method,
                string.Format(
                    "CGCacheManager::CustomCacheRefreshAction()-> Item {0} removed from cache. Reason for removal is {1}",
                    key, removalReason.ToString()));
        }
    }

    #endregion ICacheItemRefreshAction implementations
}