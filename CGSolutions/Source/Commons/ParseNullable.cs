﻿using System;

namespace CommonModules
{
    public static class ParseExtensions
    {
        private static T? ParseNullable<T>(string s, ParseDelegate<T> parse) where T : struct
        {
            if (string.IsNullOrEmpty(s))
                return null;
            return parse(s);
        }

        public static int? ParseNullableInt(string s)
        {
            return ParseNullable(s, int.Parse);
        }

        public static short? ParseNullableShort(string s)
        {
            return ParseNullable(s, short.Parse);
        }

        public static DateTime? ParseNullableDateTime(string s)
        {
            return ParseNullable(s, DateTime.Parse);
        }

        public static int? ToNullableInt32(this string s)
        {
            int i;
            if (int.TryParse(s, out i)) return i;
            return null;
        }

        public static short? ToNullableInt16(this string s)
        {
            short i;
            if (short.TryParse(s, out i)) return i;
            return null;
        }

        public static DateTime? ToNullableDateTime(this string s)
        {
            DateTime dt;
            if (DateTime.TryParse(s, out dt)) return dt;
            return null;
        }

        private delegate T ParseDelegate<T>(string s);
    }
}