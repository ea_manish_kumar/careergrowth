using System;
using System.Configuration;
using CommonModules.Exceptions;
using Microsoft.Azure;

namespace CommonModules.Configuration
{
    /// <summary>
    ///     Methods for configuration file
    /// </summary>
    public class ConfigManager
    {
        /// <summary>
        ///     static constructor..
        /// </summary>
        static ConfigManager()
        {
            SetApplicationMode();
        }

        //<summary>
        //Provides the current Application mode
        //</summary>
        //<value>The app mode.</value>
        public static string AppMode { get; private set; } = ApplicationModes.Development;

        /// <summary>
        ///     Returns the keyvalue for the keyname provided.
        /// </summary>
        /// <param name="keyName">Key name</param>
        /// <returns>Keyvalue as string.</returns>
        public static string GetConfig(string keyName)
        {
            return GetConfig(keyName, true);
        }

        public static string GetConfig(string keyName, bool showAssert)
        {
            if (!string.IsNullOrEmpty(CloudConfigurationManager.GetSetting(keyName)))
                return CloudConfigurationManager.GetSetting(keyName);
            if (ConfigurationManager.AppSettings[keyName] != null)
                return ConfigurationManager.AppSettings[keyName];
            throw new Exception("Key Unavailable: " + keyName);
        }

        public static string GetConnectionString()
        {
            if (!(ConfigurationManager.ConnectionStrings["ConnString"].ConnectionString == null))
                return ConfigurationManager.ConnectionStrings["ConnString"].ConnectionString;
            throw new Exception("Invalid ConnectionString");
        }

        public static void SetApplicationMode()
        {
            var Mode = GetConfig("App_Mode");
            switch (Mode)
            {
                case ApplicationModes.Development:
                    AppMode = ApplicationModes.Development;
                    break;
                case ApplicationModes.Production:
                    AppMode = ApplicationModes.Production;
                    break;
                default:
                    AppMode = ApplicationModes.Development;
                    break;
            }

            ;
        }
    }
}