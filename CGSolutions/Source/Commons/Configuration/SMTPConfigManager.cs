﻿using System.Configuration;

namespace CommonModules.Configuration
{
    public class SMTPConfigManager : ConfigurationSection
    {
        // Create a "UserName" attribute.
        [ConfigurationProperty("username", DefaultValue = "", IsRequired = false)]
        public string UserName
        {
            get => (string) this["username"];
            set => this["username"] = value;
        }

        // Create a "Password" attribute.
        [ConfigurationProperty("password", DefaultValue = "", IsRequired = false)]
        public string Password
        {
            get => (string) this["password"];
            set => this["password"] = value;
        }

        // Create a "smtpserver" attribute.
        [ConfigurationProperty("smtpserver", DefaultValue = "", IsRequired = true)]
        public string SMTPServer
        {
            get => (string) this["smtpserver"];
            set => this["smtpserver"] = value;
        }

        // Create a "port" attribute.
        [ConfigurationProperty("port", DefaultValue = 0, IsRequired = false)]
        public int Port
        {
            get => (int) this["port"];
            set => this["port"] = value;
        }

        // Create a "defaultcredentials" attribute.
        [ConfigurationProperty("defaultcredentials", DefaultValue = "false", IsRequired = false)]
        public bool DefaultCredentials
        {
            get => (bool) this["defaultcredentials"];
            set => this["defaultcredentials"] = value;
        }
    }
}