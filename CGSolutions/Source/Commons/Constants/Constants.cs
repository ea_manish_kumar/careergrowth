﻿namespace CommonModules.Constants
{
    public class RetryStrategies
    {
        public const string Incremental = "incremental";
        public const string FixedInterval = "fixedinterval";
        public const string ExponentialBackoff = "exponentialBackoff";
    }

    public class RequestHeaders
    {
        public const string HTTP_X_FORWARDED_FOR = "HTTP_X_FORWARDED_FOR";
        public const string REMOTE_ADDR = "REMOTE_ADDR";
        public const string UserAgent = "User-Agent";
        public const string AcceptLanguage = "Accept-Language";
        public const string JSON_Format = "application/json";
        public const string GZIP = "gzip";
        public const string DEFLATE = "deflate";
        public const string RequestReferenceNumber = "reference_number";
        public const string AuthKey = "Auth";
        public const string AuthToken = "AuthToken: ";
    }
}