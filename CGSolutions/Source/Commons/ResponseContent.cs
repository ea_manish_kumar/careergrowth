﻿using System.IO;

namespace CommonModules
{
    public class ResponseContent
    {
        private string message;
        private MemoryStream stream;

        public string ReturnResponse(ResponseType responseType)
        {
            stream = new MemoryStream();
            switch (responseType)
            {
                case ResponseType.NotAuthorized:
                    message = ResponseMessage.NOT_AUTHORIZED;
                    break;
            }

            return ToString();
        }

        public override string ToString()
        {
            return "{ \nMessage : " + message + " \n} ";
        }
    }

    public enum ResponseType : short
    {
        NotAuthorized = 0
    }

    public class ResponseMessage
    {
        public const string NOT_AUTHORIZED = "You are not authorized to access this resource.";
    }

    public class Products
    {
        public const int ResumeHelpUS = 1;
        public const int ResumeHelpUK = 2;
    }
}