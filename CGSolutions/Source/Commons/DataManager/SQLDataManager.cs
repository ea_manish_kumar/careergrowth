﻿using System;
using System.Collections;
using System.Data;
using System.Data.SqlClient;
using System.Reflection;
using CommonModules.Configuration;
using CommonModules.Exceptions;

namespace CommonModules.DataManager
{
    public class SQLDataManager
    {
        //Get the connection string
        private readonly string connectionString = ConfigManager.GetConnectionString();

        #region GetDetail

        /// <summary>
        ///     ADO.Net Get Scalar result
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="query"></param>
        /// <param name="parameters"></param>
        /// <param name="commandType"></param>
        /// <returns></returns>
        public T ExecuteScalar<T>(string query, SqlParameter[] parameters, CommandType commandType)
        {
            var result = default(T);

            try
            {
                using (var con = new SqlConnection(connectionString))
                {
                    using (var cmd = CreateCommand(query, parameters, con, commandType))
                    {
                        if (con.State != ConnectionState.Open)
                            con.Open();

                        result = (T) cmd.ExecuteScalar();
                        con.Close();
                    }
                }
            }
            catch (Exception ex)
            {
                var htParams = new Hashtable(2);
                htParams.Add("Query", query);
                var objMethod = (MethodInfo) MethodBase.GetCurrentMethod();
                GlobalExceptionHandler.customExceptionHandler(ex, objMethod, htParams, ErrorSeverityType.Normal);
            }

            return result;
        }

        #endregion


        public bool UpdateDetails(string query, SqlParameter[] parameters, CommandType commandType = CommandType.Text)
        {
            var updated = false;
            try
            {
                using (var con = new SqlConnection(connectionString))
                {
                    using (var cmd = CreateCommand(query, parameters, con, commandType))
                    {
                        if (con.State != ConnectionState.Open)
                            con.Open();

                        var rowsUpdated = cmd.ExecuteNonQuery();
                        if (rowsUpdated > 0)
                            updated = true;

                        con.Close();
                    }
                }
            }
            catch (Exception ex)
            {
                var htParams = new Hashtable(1);
                htParams.Add("Query", query);
                var objMethod = (MethodInfo) MethodBase.GetCurrentMethod();
                GlobalExceptionHandler.customExceptionHandler(ex, objMethod, htParams, ErrorSeverityType.Normal);
            }

            return updated;
        }

        private SqlCommand CreateCommand(string query, SqlParameter[] parameters, SqlConnection con,
            CommandType commandType)
        {
            //Create the SqlCommand object
            var cmd = new SqlCommand(query, con)
            {
                CommandType = commandType
            };
            //Add the input parameters to the command object
            if (parameters != null && parameters.Length > 0)
                foreach (var param in parameters)
                    if (param != null)
                        cmd.Parameters.Add(param);
            return cmd;
        }


        #region GetDetails

        public DataTable GetDetails(string query, SqlParameter[] parameters)
        {
            return GetDetails(query, parameters, CommandType.Text, 0);
        }

        public DataTable GetDetails(string query, SqlParameter[] parameters, CommandType commandType)
        {
            return GetDetails(query, parameters, commandType, 0);
        }

        /// <summary>
        ///     ADO.Net Get Results in Table
        /// </summary>
        /// <param name="query"></param>
        /// <param name="parameters"></param>
        /// <param name="commandType"></param>
        /// <param name="connectionTimeout">In Seconds</param>
        /// <returns></returns>
        public DataTable GetDetails(string query, SqlParameter[] parameters, CommandType commandType,
            int connectionTimeout)
        {
            DataTable table = null;
            SqlDataAdapter adapter = null;
            try
            {
                using (var con = new SqlConnection(connectionString))
                {
                    var cmd = CreateCommand(query, parameters, con, commandType);
                    if (connectionTimeout > 0)
                        cmd.CommandTimeout = connectionTimeout;

                    if (con.State != ConnectionState.Open)
                        con.Open();

                    adapter = new SqlDataAdapter(cmd);
                    table = new DataTable();
                    adapter.Fill(table);

                    con.Close();
                }
            }
            catch (Exception ex)
            {
                var htParams = new Hashtable(2);
                htParams.Add("Query", query);
                htParams.Add("ConnectionTimeout", connectionTimeout);
                var objMethod = (MethodInfo) MethodBase.GetCurrentMethod();
                GlobalExceptionHandler.customExceptionHandler(ex, objMethod, htParams, ErrorSeverityType.Normal);
            }
            finally
            {
                if (adapter != null) adapter.Dispose();
            }

            return table;
        }

        #endregion
    }
}