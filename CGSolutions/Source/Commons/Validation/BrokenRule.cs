﻿using System.Collections.Generic;
using System.Text;
using CommonModules.Exceptions;

namespace CommonModules.Validation
{
    /// <summary>
    ///     Class to define attributes of a broken validation rule
    /// </summary>
    public class BrokenRule
    {
        /// <summary>
        ///     Initializes a new instance of the <see cref="BrokenRule" /> class.
        /// </summary>
        public BrokenRule()
        {
        }

        /// <summary>
        ///     Initializes a new instance of the <see cref="BrokenRule" /> class.
        /// </summary>
        /// <param name="message">The message.</param>
        public BrokenRule(string message)
        {
            Message = message;
        }

        /// <summary>
        ///     Gets or sets the name of the field.
        /// </summary>
        /// <value>The name of the field.</value>
        public string FieldName { get; set; }

        /// <summary>
        ///     Gets or sets the message.
        /// </summary>
        /// <value>The message.</value>
        public string Message { get; set; }

        /// <summary>
        ///     Gets or sets the severity.
        /// </summary>
        /// <value>The severity.</value>
        public ErrorSeverityType Severity { get; set; } = ErrorSeverityType.Normal;
    }

    /// <summary>
    ///     Collection of BrokenRule objects
    /// </summary>
    public class BrokenRules : List<BrokenRule>
    {
        /// <summary>
        ///     Constructs a Description from the Message property of the child BrokenRules
        /// </summary>
        /// <value>The description.</value>
        public string Description
        {
            get
            {
                var sbDesc = new StringBuilder();
                foreach (var Rule in this) sbDesc.AppendLine(Rule.Message);

                return sbDesc.ToString();
            }
        }
    }
}