﻿using System;

namespace CommonModules.Validation
{
    /// <summary>
    ///     Custom Exception Class to be used to Throw validation Exceptions
    /// </summary>
    public class ValidationException : Exception
    {
        private BrokenRules _brokenrules;

        /// <summary>
        ///     List of BrokenRule objects
        /// </summary>
        /// <value>The broken rules.</value>
        public BrokenRules BrokenRules
        {
            get
            {
                if (_brokenrules == null)
                    _brokenrules = new BrokenRules();

                return _brokenrules;
            }

            set => _brokenrules = value;
        }

        /// <summary>
        ///     Overrides the default Exception message with the Message generated from the Broken Rules
        /// </summary>
        /// <value></value>
        /// <returns>
        ///     The error message that explains the reason for the exception, or an empty string("").
        /// </returns>
        public override string Message => BrokenRules.Description;

        #region Constructors

        /// <summary>
        ///     Default constructor
        /// </summary>
        public ValidationException()
        {
            //Does nothing
        }

        /// <summary>
        ///     Overloaded Constructor that sets the Exception.Message property
        /// </summary>
        /// <param name="ErrMessage">The err message.</param>
        public ValidationException(string ErrMessage)
        {
            BrokenRules.Add(new BrokenRule(ErrMessage));
        }

        #endregion
    }
}