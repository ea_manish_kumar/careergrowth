﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace CommonModules
{
    public static class CollectionExtensions
    {
        public static bool IsAny<T>(this IEnumerable<T> data)
        {
            return data != null && data.Any();
        }

        public static bool IsAny<T>(this IEnumerable<T> data, Func<T, bool> predicate)
        {
            return data != null && data.Any(predicate);
        }
    }
}