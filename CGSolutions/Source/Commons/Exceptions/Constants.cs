﻿namespace CommonModules.Exceptions
{
    /// <summary>
    /// </summary>
    public class SubSystem
    {
        /// <summary>
        ///     Unknown
        /// </summary>
        public const string Unknown = "UNKN";

        /// <summary>
        ///     None
        /// </summary>
        public const string None = "NONE";

        /// <summary>
        ///     Billing
        /// </summary>
        public const string Billing = "BILL";

        /// <summary>
        ///     Ecommerce
        /// </summary>
        public const string Ecommerce = "ECOM";

        /// <summary>
        ///     Documents
        /// </summary>
        public const string Documents = "DOCS";

        /// <summary>
        ///     Communications
        /// </summary>
        public const string Communications = "COMM";

        /// <summary>
        ///     Event
        /// </summary>
        public const string Event = "EVNT";

        /// <summary>
        ///     >
        /// </summary>
        public const string Survey = "SRVY";

        /// <summary>
        ///     Content
        /// </summary>
        public const string Content = "CNTN";

        /// <summary>
        ///     User
        /// </summary>
        public const string User = "USER";

        /// <summary>
        ///     Location
        /// </summary>
        public const string Location = "LCTN";

        /// <summary>
        ///     Framework
        /// </summary>
        public const string Framework = "FRWK";

        /// <summary>
        ///     Core
        /// </summary>
        public const string Core = "CORE";

        /// <summary>
        ///     Presentation
        /// </summary>
        public const string Presentation = "WEBUI";

        /// <summary>
        ///     Account
        /// </summary>
        public const string Account = "ACCN";

        /// <summary>
        ///     Subscription
        /// </summary>
        public const string Subscription = "SUBS";

        /// <summary>
        ///     Messaging
        /// </summary>
        public const string Messaging = "MESG";

        /// <summary>
        ///     Posting
        /// </summary>
        public const string Posting = "POST";

        /// <summary>
        ///     JobAlert
        /// </summary>
        public const string JobAlert = "JALRT";


        /// <summary>
        ///     Offline Task
        /// </summary>
        public const string OfflineTask = "OFFT";

        /// <summary>
        ///     Mailer Task
        /// </summary>
        public const string Mailer = "MAIL";

        /// <summary>
        ///     Export Document
        /// </summary>
        public const string ExportDoc = "XDOC";

        /// <summary>
        ///     SOLR
        /// </summary>
        public const string Solar = "SOLR";

        /// <summary>
        ///     CAND
        /// </summary>
        public const string Candidate = "CAND";

        /// <summary>
        ///     SEGM
        /// </summary>
        public const string SegmentIO = "SEGM";
    }

    /// <summary>
    /// </summary>
    public class Application
    {
        /// <summary>
        ///     Unknown
        /// </summary>
        public const string Unknown = "UNKN";

        /// <summary>
        ///     Web
        /// </summary>
        public const string WebApp = "WebApp";
    }

    /// <summary>
    ///     The application modes
    /// </summary>
    public class ApplicationModes
    {
        /// <summary>
        ///     Local
        /// </summary>
        public const string Local = "LOCAL";

        /// <summary>
        ///     Development
        /// </summary>
        public const string Development = "DEV";

        /// <summary>
        ///     Production
        /// </summary>
        public const string Production = "PROD";
    }

    internal class SubSystemMapping
    {
        /// <summary>
        ///     Gets the Subsystem mapped to the specified assembly.
        ///     If none is found, then returns a blank string
        /// </summary>
        /// <param name="sAssemblyName">Name of the s assembly.</param>
        /// <returns></returns>
        public static string GetSubSystem(string sAssemblyName)
        {
            return string.Empty;
        }
    }
}