﻿using System;
using System.Collections;
using System.Diagnostics;
using System.IO;
using System.Net;
using System.Reflection;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading;
using System.Web;
using CommonModules.Caching;
//using CommonModules.Logging;
using CommonModules.Validation;
using Newtonsoft.Json;

namespace CommonModules.Exceptions
{
    public class GlobalExceptionHandler
    {
        #region Main implementations

        public static CustomException customExceptionHandler(Exception objEx, MethodInfo objMethod, Hashtable htArgs,
            string strSubSystem, string strApplication, ErrorSeverityType Severity, string Comments, bool LogError)
        {
            var ErrorID = -1;
            if (objEx is ThreadAbortException) Thread.ResetAbort();
            if (objEx is ValidationException || objEx is AuthenticationException) throw objEx;

            var customEx = default(CustomException);
            var objParams = objMethod.GetParameters();

            try
            {
                if (!(objEx is CustomException))
                {
                    customEx = new CustomException(objEx);
                    customEx.SetMessage(GetSQLEncodedString(objEx.Message));
                    customEx.SetStackTrace(GetSQLEncodedString(objEx.StackTrace));
                    customEx.Source = objEx.Source;
                    customEx.Description = GetSQLEncodedString(Comments);
                    customEx.Severity = Severity;
                    customEx.SubSystem = GetSubSystem(objMethod, strSubSystem);
                    customEx.ApplicationCD = strApplication;
                    customEx.UserID = ContextCacher.UserID;
                    customEx.Timestamp = DateTime.Now;
                    customEx.Type = objEx.GetType().FullName;

                    if (LogError) customEx.ActivityID = "{" + Guid.NewGuid() + "}";

                    if (HttpContext.Current != null)
                    {
                        var QS = "";
                        if (!string.IsNullOrEmpty(HttpContext.Current.Request.ServerVariables["QUERY_STRING"]))
                            QS = "?" + HttpContext.Current.Request.ServerVariables["QUERY_STRING"];

                        var LocalSource = HttpContext.Current.Request.ServerVariables["PATH_INFO"] + QS;
                        customEx.LocalSource = GetSQLEncodedString(LocalSource);
                    }

                    if (objEx.InnerException != null)
                    {
                        var InnerEx = objEx.InnerException;
                        customEx.ErrorStackItems.Add(GetErrorStackItem(InnerEx, InnerEx.TargetSite.GetParameters(),
                            InnerEx.TargetSite, null, InnerEx.GetType() + ":" + InnerEx.Message, false));

                        if (InnerEx.InnerException != null)
                        {
                            InnerEx = InnerEx.InnerException;
                            customEx.ErrorStackItems.Add(GetErrorStackItem(InnerEx, InnerEx.TargetSite.GetParameters(),
                                InnerEx.TargetSite, null, InnerEx.GetType() + ":" + InnerEx.Message, false));
                        }
                    }

                    customEx.ErrorStackItems.Add(GetErrorStackItem(objEx, objParams, objMethod, htArgs, Comments,
                        true));
                }
                else
                {
                    customEx = (CustomException) objEx;

                    if (Severity != ErrorSeverityType.Unknown &&
                        Convert.ToInt16(Severity) > Convert.ToInt16(customEx.Severity))
                        customEx.Severity = Severity;

                    customEx.SubSystem = GetSubSystem(objMethod, strSubSystem);

                    customEx.ApplicationCD = strApplication;

                    if (string.IsNullOrEmpty(customEx.Description))
                        customEx.Description = Comments;

                    customEx.ErrorStackItems.Add(
                        GetErrorStackItem(objEx, objParams, objMethod, htArgs, Comments, false));
                }
            }
            catch (Exception ex)
            {
                var method = (MethodInfo) MethodBase.GetCurrentMethod();
                //Log4NetManager.LogException(method, ex);
            }
            finally
            {
                if (customEx != null) throw customEx;
            }

            return customEx;
        }

        #endregion

        #region BuildExceptionMessage

        /// <summary>
        ///     Builds an error string with the exception given.
        /// </summary>
        /// <param name="exception">A given exception where an error string can be built upon it and its inner exceptions.</param>
        /// <returns>The error string built by the information in the exception class.</returns>
        public static string BuildExceptionMessage(Exception exception)
        {
            Exception objexception;
            var strresult = string.Empty;

            try
            {
                objexception = exception;
                while (objexception != null)
                {
                    strresult += objexception.Message;
                    objexception = objexception.InnerException;
                }
            }
            catch (Exception buildexception)
            {
                throw buildexception;
            }

            return strresult;
        }

        #endregion BuildExceptionMessage

        #region EComExceptionHandler

        public static EComError LogEComException(string serialisedData, WebException webex, string serviceURL)
        {
            EComError error = null;
            if (webex != null && webex.Response != null)
            {
                var errorDetails = string.Empty;
                error = new EComError();
                var responseStream = webex.Response.GetResponseStream();
                if (responseStream != null)
                {
                    errorDetails = new StreamReader(responseStream).ReadToEnd();
                    if (!string.IsNullOrEmpty(errorDetails))
                    {
                        if (errorDetails.Contains("Error_Code") && errorDetails.Contains("Description"))
                        {
                            error = JsonConvert.DeserializeObject<EComError>(errorDetails);
                        }
                        else
                        {
                            error = new EComError();
                            error.Description = errorDetails;
                            error.Error_Code = "FAIL";
                        }

                        if (error != null)
                            errorDetails = string.Format("Service URL:{0}, Error Code: {1}, Description: {2}",
                                serviceURL, error.Error_Code, error.Description);
                    }
                }

                var objMethod = (MethodInfo) MethodBase.GetCurrentMethod();
                customExceptionHandler(webex, objMethod, null, "ECOM", "ECOM", ErrorSeverityType.High,
                    "Request=" + serialisedData + " response=" + errorDetails, true);
            }

            return error;
        }

        #endregion

        #region Overloaded implementations of customExceptionHandler

        /// <summary>
        ///     This is an overload of the main Error Handling routine and internally calls the main routine
        /// </summary>
        /// <param name="objEx">Exception Object, which could be of type customError</param>
        /// <param name="objMethod">Method Info</param>
        /// <param name="htArgs">Method Parameter values</param>
        /// <param name="Severity">Error Severity, use the CommonModules.Exceptions.ErrorSeverityType enum</param>
        [DebuggerStepThrough]
        public static void customExceptionHandler(Exception objEx, MethodInfo objMethod, Hashtable htArgs,
            ErrorSeverityType Severity)
        {
            customExceptionHandler(objEx, objMethod, htArgs, SubSystem.Unknown, Application.Unknown, Severity, "",
                false);
        }

        /// <summary>
        ///     This is an overload of the main Error Handling routine and internally calls the main routine
        /// </summary>
        /// <param name="objEx">Exception Object, which could be of type customError</param>
        /// <param name="objMethod">Method Info</param>
        /// <param name="htArgs">Method Parameter values</param>
        /// <param name="Severity">Error Severity, use the CommonModules.Exceptions.ErrorSeverityType enum</param>
        /// <param name="Comments">
        ///     Developer can use this field to provide some custom information about the Exception.
        ///     This should only be used if there is some meaningful EXTRA information.
        /// </param>
        [DebuggerStepThrough]
        public static void customExceptionHandler(Exception objEx, MethodInfo objMethod, Hashtable htArgs,
            ErrorSeverityType Severity, string Comments)
        {
            customExceptionHandler(objEx, objMethod, htArgs, SubSystem.Unknown, Application.Unknown, Severity, Comments,
                false);
        }

        public static void customExceptionHandler(Exception objEx, ParameterInfo[] objParams, MethodInfo objMethod,
            object[] objArgs, string strSubSystem)
        {
            Hashtable HT = null;

            try
            {
                if (objArgs.Length > 0)
                {
                    HT = new Hashtable();

                    var iCount = 0;
                    var iPos = 0;
                    while ((iCount < objParams.Length) & (iCount < objArgs.Length))
                    {
                        iPos = objParams[iCount].Position;

                        HT.Add(objParams[iCount].Name, objArgs[iPos]);

                        iCount = iCount + 1;
                    }
                }
            }
            catch
            {
                //swallow
            }

            customExceptionHandler(objEx, objMethod, HT, strSubSystem, Application.WebApp, ErrorSeverityType.Unknown,
                objEx.Message, false);
        }

        #endregion

        #region Private helper methods

        private static ErrorStackItem GetErrorStackItem(Exception Ex, ParameterInfo[] objParams, MethodBase objMethod,
            Hashtable htArgs, string Comments, bool IsNew)
        {
            var StackItem = new ErrorStackItem();
            StackItem.ClassName = objMethod.DeclaringType.FullName;
            StackItem.Module = objMethod.Module.Name;
            StackItem.MethodSignature = GetMethodSignature(objMethod, objParams);
            StackItem.VariableValues = GetVariableValuesSB(htArgs);
            StackItem.Comments = GetSQLEncodedString(Comments);
            StackItem.Machine = Environment.MachineName;

            if (IsNew)
                StackItem.LineNumber = GetLineNumber(Ex.StackTrace);

            return StackItem;
        }

        private static string GetMethodSignature(MethodBase objMethod, ParameterInfo[] objParams)
        {
            var sbSign = new StringBuilder();
            var paramCount = 1;

            sbSign.Append(objMethod.Name);
            sbSign.Append("(");
            foreach (var parameter in objParams)
            {
                sbSign.Append(parameter.ParameterType + " " + parameter.Name);
                if (paramCount < objParams.Length)
                    sbSign.Append(", ");
                paramCount++;
            }

            sbSign.Append(")");

            return sbSign.ToString();
        }

        private static string GetLineNumber(string StackTrace)
        {
            var LineNum = "";
            if (!string.IsNullOrEmpty(StackTrace) && StackTrace.Contains(":line"))
            {
                var regex = new Regex(@":line\s*\d+");
                var match = regex.Match(StackTrace);
                if (match.Success)
                    LineNum = match.Value.Replace(":line", "").Replace(" ", "");
            }

            return LineNum;
        }

        private static string GetVariableValues(Hashtable HT)
        {
            var sb = new StringBuilder();
            object ValueObject;
            var Value = "";

            if (HT != null && HT.Keys != null)
                foreach (var Key in HT.Keys)
                    if (Key != null)
                    {
                        ValueObject = HT[Key];

                        if (ValueObject == null)
                            Value = "NULL";
                        else
                            Value = ValueObject.ToString();

                        //Get the Name and Value of the parameter
                        sb.AppendLine(Key + ":" + Value + " || ");
                    }

            return GetSQLEncodedString(sb.ToString());
        }

        private static StringBuilder GetVariableValuesSB(Hashtable HT)
        {
            var sb = new StringBuilder();
            object ValueObject;
            var Value = "";

            if (HT != null && HT.Keys != null)
                foreach (var Key in HT.Keys)
                    if (Key != null)
                    {
                        ValueObject = HT[Key];

                        if (ValueObject == null)
                            Value = "NULL";
                        else
                            Value = ValueObject.ToString();

                        //Get the Name and Value of the parameter
                        sb.AppendLine(Key + ":" + Value + " || ");
                    }

            return sb.Replace("''", "'");
        }

        private static string GetSubSystem(MethodBase objMethod, string strSubSystem)
        {
            try
            {
                var sAssemblyName = objMethod.Module.Assembly.GetName().Name;

                if (strSubSystem != SubSystem.Unknown) return strSubSystem;

                var sMappedSubSystem = SubSystemMapping.GetSubSystem(sAssemblyName);
                if (sMappedSubSystem.Length > 0)
                {
                    if (sMappedSubSystem == SubSystem.Framework)
                        return strSubSystem;
                    return sMappedSubSystem;
                }

                return strSubSystem;
            }
            catch
            {
                return strSubSystem;
            }
        }

        private static string GetSQLEncodedString(string strSQL)
        {
            if (string.IsNullOrEmpty(strSQL))
                return "";

            var strRet = strSQL.Replace("'", "''");
            return strRet;
        }

        #endregion

        #region DumpException

        //
        /// <summary>
        ///     Dumps the stack trace in string form.
        /// </summary>
        /// <param name="ex">The Exception object that needs to be dumped.</param>
        /// <param name="methodInfo">The source method</param>
        /// <returns>A System.String containing the stack trace.</returns>
        public static string DumpException(Exception ex, MethodInfo methodInfo)
        {
            return DumpException(ex, methodInfo.DeclaringType.Name + "::" + methodInfo.Name);
        }

        /// <summary>
        ///     Dumps the stack trace in a string form
        /// </summary>
        /// <param name="ex">The Exception object that needs to be dumped.</param>
        /// <param name="source">The class and function name, preferably in [classname::functionName] format</param>
        /// <returns>A System.String containing the stack trace.</returns>
        public static string DumpException(Exception ex, string source)
        {
            //string retstr;
            var sb = new StringBuilder(20);

            try
            {
                sb.Append(" \r\n\r\n" + Environment.NewLine);
                sb.Append("----------------------------------------------------------------------------\r\n");
                sb.Append("EXCEPTION OCCURRED in application '" + source + "' at " +
                          DateTime.Now.ToString("dd/MM/yy - HH:mm:ss") + "\r\n");
                sb.Append("----------------------------------------------------------------------------\r\n");

                Exception objexception;
                objexception = ex;

                // The loop will iterate all inner exception objects contained within this Exception
                // object and print stack trace of each one of them.
                while (objexception != null)
                {
                    //sb.Append("Target Site: " + objexception.TargetSite);
                    //					sb.Append(" \r\n");
                    //					sb.Append("--- ---- ---- --- ---- ---- --- ---- ---- --- ---- ---- \r\n");
                    sb.Append("Type: " + objexception.GetType() + "\r\n");
                    //					sb.Append(" \r\n");
                    sb.Append("Message: " + objexception.Message + "\r\n");
                    //					sb.Append(" \r\n");					
                    if (objexception.StackTrace != null)
                    {
                        sb.Append("-----------Stack Trace follows-----------\r\n");
                        //						sb.Append(" \r\n");
                        sb.Append(objexception.StackTrace + "\r\n");

                        //StackTrace strace = objexception.StackTrace;

                        //						for(int j = 0; j < strace.FrameCount;j++)
                        //							sb.Append(strace.GetFrame(j).ToString() + "\r\n");
                        //
                        //						strace = null;

                        //						sb.Append(" \r\n");
                        sb.Append("-----------Stack Trace ends.-----------\r\n");
                    }

                    objexception = objexception.InnerException;
                }
            }
            catch (Exception exception)
            {
                Trace.WriteLine(source,
                    "Error occurred in DumpException " + exception.Message + "\r\n original exception is " +
                    ex.Message);
            }

            return sb.ToString();
        }

        #endregion DumpException
    }
}