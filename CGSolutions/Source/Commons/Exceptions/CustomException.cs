using System;
using System.Collections.Generic;
using System.Text;

namespace CommonModules.Exceptions
{
    public class CustomException : Exception
    {
        #region Constructors

        /// <summary>
        ///     The parameterized constructor.
        /// </summary>
        /// <param name="errorID">The error ID.</param>
        /// <param name="activityID">The activity ID.</param>
        /// <param name="number">The number.</param>
        /// <param name="message">The message.</param>
        /// <param name="description">The description.</param>
        /// <param name="source">The source.</param>
        /// <param name="localSource">The local source.</param>
        /// <param name="stackTrace">The stack trace.</param>
        /// <param name="severity">The severity.</param>
        /// <param name="subSystem">The sub system.</param>
        /// <param name="applicationCD">The application CD.</param>
        /// <param name="UserID">The party ID.</param>
        /// <param name="timestamp">The timestamp.</param>
        /// <param name="type">The exception type</param>
        public CustomException(int errorID, string activityID, int? number, string message, string description,
            string source, string localSource, string stackTrace, ErrorSeverityType severity, string subSystem,
            string applicationCD, int? UserID, DateTime? timestamp, string type)
        {
            ErrorID = errorID;
            ActivityID = activityID;
            Number = number;
            Message = message;
            Description = description;
            Source = source;
            LocalSource = localSource;
            StackTrace = stackTrace;
            Severity = severity;
            SubSystem = subSystem;
            ApplicationCD = applicationCD;
            this.UserID = UserID;
            Timestamp = timestamp;
            Type = type;
        }

        /// <summary>
        ///     Default Constructor
        /// </summary>
        public CustomException()
        {
        }

        /// <summary>
        ///     Overloaded Constructor that sets the Message property
        /// </summary>
        /// <param name="ExMessage">The ex message.</param>
        public CustomException(string ExMessage)
            : base(ExMessage)
        {
            _message = ExMessage;
        }

        /// <summary>
        ///     Initializes a new instance of the <see cref="CustomException" /> class.
        /// </summary>
        /// <param name="message">The message.</param>
        /// <param name="ex">The ex.</param>
        public CustomException(string message, Exception ex)
            : base(message, ex)
        {
        }

        /// <summary>
        ///     Initializes a new instance of the <see cref="CustomException" /> class.
        /// </summary>
        /// <param name="Ex">The ex.</param>
        public CustomException(Exception Ex)
            : base(Ex.Message, Ex)
        {
            _message = Ex.Message;
            _stackTrace = Ex.StackTrace;
            Source = Ex.Source;
        }

        #endregion

        #region Property Declarations

        /// <summary>
        ///     Private variable to hold the value for property ErrorID.
        /// </summary>
        private int _errorID;

        /// <summary>
        ///     The error ID..
        /// </summary>
        /// <value>The error ID.</value>
        public int ErrorID
        {
            get => _errorID;
            set
            {
                _errorID = value;
                //Set the ErrorID property for all Child StackItems
                foreach (var StackItem in ErrorStackItems) StackItem.ErrorID = _errorID;
            }
        }

        /// <summary>
        ///     Private variable to hold the value for property ActivityID.
        /// </summary>
        private string _activityID;

        /// <summary>
        ///     The activity ID..
        /// </summary>
        /// <value>The activity ID.</value>
        public string ActivityID
        {
            get => _activityID;
            set => _activityID = value;
        }

        /// <summary>
        ///     Private variable to hold the value for property Number.
        /// </summary>
        private int? _number;

        /// <summary>
        ///     The error number..
        /// </summary>
        /// <value>The number.</value>
        public int? Number
        {
            get => _number;
            set => _number = value;
        }

        /// <summary>
        ///     Private variable to hold the value for property Message.
        /// </summary>
        private string _message;

        /// <summary>
        ///     The error message that explains the reason for the exception, or an empty string("")..
        /// </summary>
        /// <value></value>
        /// <returns>
        ///     The error message that explains the reason for the exception, or an empty string("").
        /// </returns>
        public new string Message
        {
            get => _message;
            set => _message = value;
        }

        /// <summary>
        ///     Private variable to hold the value for property Description.
        /// </summary>
        private string _description;

        /// <summary>
        ///     The error description..
        /// </summary>
        /// <value>The description.</value>
        public string Description
        {
            get => _description;
            set => _description = value;
        }

        /// <summary>
        ///     Private variable to hold the value for property Source.
        /// </summary>
        private string _source;

        /// <summary>
        ///     The name of the application or the object that causes the error..
        /// </summary>
        /// <value></value>
        /// <returns>
        ///     The name of the application or the object that causes the error.
        /// </returns>
        public new string Source
        {
            get => _source;
            set => _source = value;
        }

        /// <summary>
        ///     Private variable to hold the value for property LocalSource.
        /// </summary>
        private string _localSource;

        /// <summary>
        ///     The local source..
        /// </summary>
        /// <value>The local source.</value>
        public string LocalSource
        {
            get => _localSource;
            set => _localSource = value;
        }

        /// <summary>
        ///     Private variable to hold the value for property StackTrace.
        /// </summary>
        private string _stackTrace;

        /// <summary>
        ///     A string that describes the contents of the call stack, with the most recent method call appearing first..
        /// </summary>
        /// <value></value>
        /// <returns>
        ///     A string that describes the contents of the call stack, with the most recent method call appearing first.
        /// </returns>
        /// <PermissionSet>
        ///     <IPermission
        ///         class="System.Security.Permissions.FileIOPermission, mscorlib, Version=2.0.3600.0, Culture=neutral, PublicKeyToken=b77a5c561934e089"
        ///         version="1" PathDiscovery="*AllFiles*" />
        /// </PermissionSet>
        public new string StackTrace
        {
            get => _stackTrace;
            set => _stackTrace = value;
        }

        /// <summary>
        ///     Private variable to hold the value for property Severity.
        /// </summary>
        private ErrorSeverityType _severity = ErrorSeverityType.Normal;

        /// <summary>
        ///     The severity..
        /// </summary>
        /// <value>The severity.</value>
        public ErrorSeverityType Severity
        {
            get => _severity;
            set => _severity = value;
        }

        /// <summary>
        ///     Private variable to hold the value for property SubSystem.
        /// </summary>
        private string _subSystem;

        /// <summary>
        ///     The sub system..
        /// </summary>
        /// <value>The sub system.</value>
        public string SubSystem
        {
            get => _subSystem;
            set => _subSystem = value;
        }

        /// <summary>
        ///     Private variable to hold the value for property ApplicationCD.
        /// </summary>
        private string _applicationCD;

        /// <summary>
        ///     The application CD..
        /// </summary>
        /// <value>The application CD.</value>
        public string ApplicationCD
        {
            get => _applicationCD;
            set => _applicationCD = value;
        }

        /// <summary>
        ///     Private variable to hold the value for property UserID.
        /// </summary>
        private int? _UserID;

        /// <summary>
        ///     The party ID..
        /// </summary>
        /// <value>The party ID.</value>
        public int? UserID
        {
            get => _UserID;
            set => _UserID = value;
        }


        /// <summary>
        ///     Private variable to hold the value for property Timestamp.
        /// </summary>
        private DateTime? _timestamp;

        /// <summary>
        ///     The timestamp..
        /// </summary>
        /// <value>The timestamp.</value>
        public DateTime? Timestamp
        {
            get => _timestamp;
            set => _timestamp = value;
        }

        /// <summary>
        ///     Gets or sets the error stack items.
        /// </summary>
        /// <value>The error stack items.</value>
        public IList<ErrorStackItem> ErrorStackItems { get; set; } = new List<ErrorStackItem>();

        public string Type { get; set; }

        #endregion Property Declarations

        #region Methods

        #region ToString() Implementation

        public override string ToString()
        {
            var sb = new StringBuilder(30);

            sb.AppendLine("ErrorID: " + ErrorID);
            sb.AppendLine("ActivityID: " + (ActivityID != null ? ActivityID : "<NULL>"));
            sb.AppendLine("Number: " + (Number != null ? Number.ToString() : "<NULL>"));
            sb.AppendLine("Message: " + (Message != null ? Message : "<NULL>"));
            sb.AppendLine("Description: " + (Description != null ? Description : "<NULL>"));
            sb.AppendLine("Source: " + (Source != null ? Source : "<NULL>"));
            sb.AppendLine("LocalSource: " + (LocalSource != null ? LocalSource : "<NULL>"));
            sb.AppendLine("StackTrace: " + (StackTrace != null ? StackTrace : "<NULL>"));
            sb.AppendLine("Severity: " + Severity);
            sb.AppendLine("SubSystem: " + (SubSystem != null ? SubSystem : "<NULL>"));
            sb.AppendLine("ApplicationCD: " + (ApplicationCD != null ? ApplicationCD : "<NULL>"));
            sb.AppendLine("UserID: " + (UserID != null ? UserID.ToString() : "<NULL>"));
            sb.AppendLine("Timestamp: " + (Timestamp != null ? Timestamp.ToString() : "<NULL>"));
            sb.AppendLine("Type: " + (Type != null ? Type : "<NULL>"));

            sb.AppendLine("---ErrorStackItem start---");
            foreach (var objErrorStackItem in ErrorStackItems) sb.AppendLine(objErrorStackItem.ToString());

            return sb.ToString();
        }

        #endregion ToString() Implementation

        #region GetClone() Implementation

        /// <summary>
        ///     This methods returns a cloned instance of this class.
        /// </summary>
        /// <returns>A cloned instance of this class.</returns>
        public CustomException GetClone()
        {
            var clone = new CustomException();

            clone.ErrorID = ErrorID;
            clone.ActivityID = ActivityID;
            clone.Number = Number;
            clone.Message = Message;
            clone.Description = Description;
            clone.Source = Source;
            clone.LocalSource = LocalSource;
            clone.StackTrace = StackTrace;
            clone.Severity = Severity;
            clone.SubSystem = SubSystem;
            clone.ApplicationCD = ApplicationCD;
            clone.UserID = UserID;
            clone.Timestamp = Timestamp;
            clone.Type = Type;

            foreach (var objErrorStackItem in ErrorStackItems)
            {
                var CloneErrorStackItem = objErrorStackItem.GetClone();
                clone.ErrorStackItems.Add(CloneErrorStackItem);
            }

            return clone;
        }

        #endregion GetClone() Implementation

        /// <summary>
        ///     Sets the message.
        /// </summary>
        /// <param name="strMessage">The STR message.</param>
        public void SetMessage(string strMessage)
        {
            _message = strMessage;
        }

        /// <summary>
        ///     Sets the stack trace.
        /// </summary>
        /// <param name="strStackTrace">The STR stack trace.</param>
        public void SetStackTrace(string strStackTrace)
        {
            _stackTrace = strStackTrace;
        }

        #endregion
    }
}