﻿using System;

namespace CommonModules.Exceptions
{
    /// <summary>
    /// </summary>
    public class AuthenticationException : Exception
    {
        private string _message;

        /// <summary>
        ///     Initializes a new instance of the <see cref="AuthenticationException" /> class.
        /// </summary>
        /// <param name="AuthExceptionType">Type of the auth exception.</param>
        public AuthenticationException(AuthenticationExceptionType AuthExceptionType)
        {
            ExceptionType = AuthExceptionType;
        }

        /// <summary>
        ///     The custom Error Code for this Authentication Exception
        ///     This value is prefixed to the Message and is used by Flex to modulate its behaviour
        /// </summary>
        /// <value>The type of the exception.</value>
        public AuthenticationExceptionType ExceptionType { get; set; }

        /// <summary>
        ///     Overrides the Exception.Message property by Prefixing the ErrorCode to the Message
        /// </summary>
        /// <value></value>
        /// <returns>
        ///     The error message that explains the reason for the exception, or an empty string("").
        /// </returns>
        public override string Message
        {
            get
            {
                short ErrorCode = 0;
                switch (ExceptionType)
                {
                    case AuthenticationExceptionType.SessionExpired:
                        ErrorCode = (short) AuthenticationExceptionType.SessionExpired;
                        _message = "[" + ErrorCode + "] Session has expired";
                        break;
                    case AuthenticationExceptionType.InvalidGUID:
                        ErrorCode = (short) AuthenticationExceptionType.InvalidGUID;
                        _message = "[" + ErrorCode + "] Authentication failed. Invalid Session GUID";
                        break;
                    case AuthenticationExceptionType.NoGUID:
                        ErrorCode = (short) AuthenticationExceptionType.NoGUID;
                        _message = "[" + ErrorCode + "] Authentication failed. No Session GUID found";
                        break;
                    case AuthenticationExceptionType.Unauthorized_Request_For_Document:
                        ErrorCode = (short) AuthenticationExceptionType.Unauthorized_Request_For_Document;
                        _message = "[" + ErrorCode + "] Unauthorized request for Document";
                        break;
                    case AuthenticationExceptionType.Unauthorized_Request_For_Order:
                        ErrorCode = (short) AuthenticationExceptionType.Unauthorized_Request_For_Order;
                        _message = "[" + ErrorCode + "] Unauthorized request for Order";
                        break;
                    case AuthenticationExceptionType.Unauthorized_Request_For_Subscription:
                        ErrorCode = (short) AuthenticationExceptionType.Unauthorized_Request_For_Subscription;
                        _message = "[" + ErrorCode + "] Unauthorized request for Subscription";
                        break;

                    default: throw new ArgumentException("Invalid ExceptionType - " + ExceptionType);
                }

                return _message;
            }
        }
    }
}