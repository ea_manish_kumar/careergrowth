﻿namespace CommonModules.Exceptions
{
    /// <summary>
    ///     ErrorSeverityType
    /// </summary>
    public enum ErrorSeverityType : short
    {
        /// <summary>
        /// </summary>
        Unknown = 0,

        /// <summary>
        /// </summary>
        Info = 1,

        /// <summary>
        /// </summary>
        Low = 2,

        /// <summary>
        /// </summary>
        Normal = 3,

        /// <summary>
        /// </summary>
        High = 4,

        /// <summary>
        /// </summary>
        Critical = 5
    }

    /// <summary>
    ///     AuthenticationExceptionType
    /// </summary>
    public enum AuthenticationExceptionType : short
    {
        /// <summary>
        /// </summary>
        None = 0,

        /// <summary>
        ///     SessionExpired
        /// </summary>
        SessionExpired = 100,

        /// <summary>
        ///     InvalidGUID
        /// </summary>
        InvalidGUID = 101,

        /// <summary>
        ///     NoGUID
        /// </summary>
        NoGUID = 102,

        /// <summary>
        ///     Unauthorized_Request_For_Document
        /// </summary>
        Unauthorized_Request_For_Document = 201,

        /// <summary>
        ///     Unauthorized_Request_For_Order
        /// </summary>
        Unauthorized_Request_For_Order = 301,

        /// <summary>
        ///     Unauthorized_Request_For_Subscription
        /// </summary>
        Unauthorized_Request_For_Subscription = 302
    }

    ///// <summary>
    ///// The application modes
    ///// </summary>
    //public enum ApplicationModes
    //{
    //    /// <summary>
    //    /// Development
    //    /// </summary>
    //    Development = 0,
    //    /// <summary>
    //    /// QA
    //    /// </summary>
    //    QA = 1,
    //    /// <summary>
    //    /// Staging
    //    /// </summary>
    //    Staging = 2,
    //    /// <summary>
    //    /// Production
    //    /// </summary>
    //    Production = 3
    //}
}