using System.Text;

namespace CommonModules.Exceptions
{
    internal class ErrorField
    {
        #region Constructors

        #endregion

        #region Declarations

        private string _name;
        private string _value;

        #endregion

        #region Methods

        /// <summary>
        ///     Serves as a hash function for a particular type.
        /// </summary>
        /// <returns>
        ///     A hash code for the current <see cref="T:System.Object" />.
        /// </returns>
        public override int GetHashCode()
        {
            var sb = new StringBuilder();

            sb.Append(GetType().FullName);
            sb.Append(_name);
            sb.Append(_value);

            return sb.ToString().GetHashCode();
        }

        /// <summary>
        ///     Gets the clone.
        /// </summary>
        /// <returns></returns>
        public virtual ErrorField GetClone()
        {
            var Clone = new ErrorField();
            Clone.ErrorStackItemID = ErrorStackItemID;
            Clone.Name = Name;
            Clone.Value = Value;

            return Clone;
        }

        #endregion

        #region Properties

        /// <summary>
        ///     Gets or sets the error stack item ID.
        /// </summary>
        /// <value>The error stack item ID.</value>
        public virtual int? ErrorStackItemID { get; set; }

        /// <summary>
        ///     Gets or sets the name.
        /// </summary>
        /// <value>The name.</value>
        public virtual string Name
        {
            get => _name;
            set => _name = value;
        }

        /// <summary>
        ///     Gets or sets the value.
        /// </summary>
        /// <value>The value.</value>
        public virtual string Value
        {
            get => _value;
            set => _value = value;
        }

        #endregion
    }
}