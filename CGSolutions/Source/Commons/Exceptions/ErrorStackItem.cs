using System;
using System.Text;

namespace CommonModules.Exceptions
{
    [Serializable]
    public class ErrorStackItem
    {
        #region ToString() Implementation

        /// <summary>
        ///     This methods returns a string representation of this class.
        /// </summary>
        /// <returns>A string representation of this class.</returns>
        public new string ToString()
        {
            var sb = new StringBuilder(11);

            sb.AppendLine("------------ErrorStackItem dump starts---------------");
            sb.AppendLine("ErrorStackItemID: " + ErrorStackItemID);
            sb.AppendLine("ErrorID: " + ErrorID);
            sb.AppendLine("Module: " + (Module != null ? Module : "<NULL>"));
            sb.AppendLine("ClassName: " + (ClassName != null ? ClassName : "<NULL>"));
            sb.AppendLine("MethodSignature: " + (MethodSignature != null ? MethodSignature : "<NULL>"));
            sb.AppendLine("LineNumber: " + (LineNumber != null ? LineNumber : "<NULL>"));
            sb.AppendLine("VariableValues: " + (VariableValues != null ? VariableValues.ToString() : "<NULL>"));
            sb.AppendLine("Comments: " + (Comments != null ? Comments : "<NULL>"));
            sb.AppendLine("Machine: " + (Machine != null ? Machine : "<NULL>"));
            sb.AppendLine("------------ErrorStackItem dump ends---------------");

            return sb.ToString();
        }

        #endregion ToString() Implementation

        #region GetClone() Implementation

        /// <summary>
        ///     This methods returns a cloned instance of this class.
        /// </summary>
        /// <returns>A string cloned instance of this class.</returns>
        public ErrorStackItem GetClone()
        {
            var clone = new ErrorStackItem();

            clone.ErrorStackItemID = ErrorStackItemID;
            clone.ErrorID = ErrorID;
            clone.Module = Module;
            clone.ClassName = ClassName;
            clone.MethodSignature = MethodSignature;
            clone.LineNumber = LineNumber;
            clone.VariableValues = VariableValues;
            clone.Comments = Comments;
            clone.Machine = Machine;

            return clone;
        }

        #endregion GetClone() Implementation

        #region GetHashCode() implementation.

        /// <summary>
        ///     Serves as a hash function for a particular type.
        /// </summary>
        /// <returns>
        ///     A hash code for the current ErrorStackItem.
        /// </returns>
        public override int GetHashCode()
        {
            return (GetType().FullName + ErrorStackItemID).GetHashCode();
        }

        #endregion GetHashCode() Implementation

        #region Equals() implementation.

        /// <summary>
        ///     Determines whether the specified ErrorStackItem is equal to the current ErrorStackItem.
        /// </summary>
        /// <param name="obj">The ErrorStackItem to compare with the current ErrorStackItem.</param>
        /// <returns>
        ///     true if the specified ErrorStackItem is equal to the current ErrorStackItem; otherwise, false.
        /// </returns>
        /// <remarks>
        ///     Tests whether this and another object are equal in a way that will still pass
        ///     when proxy objects are being used.
        /// </remarks>
        public override bool Equals(object obj)
        {
            //Same reference check !
            if (ReferenceEquals(this, obj)) return true;

            //Type check !
            var other = obj as ErrorStackItem;
            if (other == null) return false;

            if (ErrorStackItemID == 0)
                //Do a per-property search
                return
                    ErrorID == other.ErrorID &&
                    Module == other.Module &&
                    ClassName == other.ClassName &&
                    MethodSignature == other.MethodSignature &&
                    LineNumber == other.LineNumber &&
                    VariableValues == other.VariableValues &&
                    Comments == other.Comments &&
                    Machine == other.Machine;
            return ErrorStackItemID == other.ErrorStackItemID;
        }

        #endregion Equals() Implementation

        #region Constructors

        /// <summary>
        ///     Initializes a new instance of the <see cref="ErrorStackItem" /> class.
        /// </summary>
        public ErrorStackItem()
        {
            //TODO:Write constructor stuff here..
        }

        /// <summary>
        ///     The parameterized constructor.
        /// </summary>
        /// <param name="errorStackItemID">The error stack item ID.</param>
        /// <param name="errorID">The error ID.</param>
        /// <param name="module">The module.</param>
        /// <param name="className">Name of the class.</param>
        /// <param name="methodSignature">The method signature.</param>
        /// <param name="lineNumber">The line number.</param>
        /// <param name="variableValues">The variable values.</param>
        /// <param name="comments">The comments.</param>
        /// <param name="machine">The machine.</param>
        public ErrorStackItem(int errorStackItemID, int errorID, string module, string className,
            string methodSignature, string lineNumber, StringBuilder variableValues, string comments, string machine)
        {
            ErrorStackItemID = errorStackItemID;
            ErrorID = errorID;
            Module = module;
            ClassName = className;
            MethodSignature = methodSignature;
            LineNumber = lineNumber;
            VariableValues = variableValues;
            Comments = comments;
            Machine = machine;
        }

        #endregion

        #region Property Declarations

        /// <summary>
        ///     Private variable to hold the value for property ErrorStackItemID.
        /// </summary>
        private int _errorStackItemID;

        /// <summary>
        ///     The error stack item ID..
        /// </summary>
        /// <value>The error stack item ID.</value>
        public int ErrorStackItemID
        {
            get => _errorStackItemID;
            set => _errorStackItemID = value;
        }

        /// <summary>
        ///     Private variable to hold the value for property ErrorID.
        /// </summary>
        private int _errorID;

        /// <summary>
        ///     The error ID..
        /// </summary>
        /// <value>The error ID.</value>
        public int ErrorID
        {
            get => _errorID;
            set => _errorID = value;
        }

        /// <summary>
        ///     Private variable to hold the value for property Module.
        /// </summary>
        private string _module;

        /// <summary>
        ///     The module..
        /// </summary>
        /// <value>The module.</value>
        public string Module
        {
            get => _module;
            set => _module = value;
        }

        /// <summary>
        ///     Private variable to hold the value for property ClassName.
        /// </summary>
        private string _className;

        /// <summary>
        ///     The name of the class..
        /// </summary>
        /// <value>The name of the class.</value>
        public string ClassName
        {
            get => _className;
            set => _className = value;
        }

        /// <summary>
        ///     Private variable to hold the value for property MethodSignature.
        /// </summary>
        private string _methodSignature;

        /// <summary>
        ///     The method signature..
        /// </summary>
        /// <value>The method signature.</value>
        public string MethodSignature
        {
            get => _methodSignature;
            set => _methodSignature = value;
        }

        /// <summary>
        ///     Private variable to hold the value for property LineNumber.
        /// </summary>
        private string _lineNumber;

        /// <summary>
        ///     The line number..
        /// </summary>
        /// <value>The line number.</value>
        public string LineNumber
        {
            get => _lineNumber;
            set => _lineNumber = value;
        }

        /// <summary>
        ///     Private variable to hold the value for property VariableValues.
        /// </summary>
        private StringBuilder _variableValues;

        /// <summary>
        ///     The variable values..
        /// </summary>
        /// <value>The variable values.</value>
        public StringBuilder VariableValues
        {
            get => _variableValues;
            set => _variableValues = value;
        }

        /// <summary>
        ///     Private variable to hold the value for property Comments.
        /// </summary>
        private string _comments;

        /// <summary>
        ///     The comments..
        /// </summary>
        /// <value>The comments.</value>
        public string Comments
        {
            get => _comments;
            set => _comments = value;
        }

        /// <summary>
        ///     Private variable to hold the value for property Machine.
        /// </summary>
        private string _machine;

        /// <summary>
        ///     The machine..
        /// </summary>
        /// <value>The machine.</value>
        public string Machine
        {
            get => _machine;
            set => _machine = value;
        }

        #endregion Property Declarations
    }
}