﻿namespace CommonModules.Exceptions
{
    public class EComError
    {
        public string Error_Code { get; set; }
        public string Description { get; set; }
    }
}