﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Reflection;
using CommonModules.Configuration;
using CommonModules.DataManager;
using CommonModules.Exceptions;
using DataAccess.Base;
using DomainModel.Core;
using NHibernate.Criterion;

namespace DataAccess.Core
{
    public class SessionDAL : DataManagerBase<Session, Guid>
    {
        public Session GetSessionById(string SessionID)
        {
            Session session = null;
            SQLDataManager dataManager = null;
            SqlParameter[] parameters;
            DataTable dtSession = null;
            //string sQuery = "SELECT [SessionID],[Campaign],[Source],[Medium],[UserAgent],[IPAddress],[VisitType],[CreatedOn],[ExpiredOn],[IsAuth],[UserAgentTypeID],[Utm_Source],[Utm_Term],[Utm_Keyword],[Utm_Content],[Requested_URL],[URL_Referrer],[DomainCD] FROM [Session] with (nolock) where sessionid = @sessionID";
            var sQuery =
                "SELECT [SessionID],[Campaign],[Source],[Medium],[UserAgent],[IPAddress],[CreatedOn],[UserAgentTypeID],[Utm_Source],[Utm_Term],[Utm_Keyword],[Utm_Content],[Requested_URL],[URL_Referrer],[DomainCD] FROM [Session] with (nolock) where sessionid = @sessionID";

            try
            {
                if (!string.IsNullOrEmpty(SessionID))
                {
                    parameters = new SqlParameter[1];
                    parameters[0] = new SqlParameter("@sessionID", SqlDbType.NVarChar, 36);
                    parameters[0].Value = SessionID;
                    dataManager = new SQLDataManager();
                    dtSession = dataManager.GetDetails(sQuery, parameters);

                    if (dtSession != null && dtSession.Rows.Count > 0)
                    {
                        session = new Session();
                        session.Campaign = Convert.ToString(dtSession.Rows[0]["Campaign"]);
                        if (dtSession.Rows[0]["CreatedOn"] != DBNull.Value)
                            session.CreatedOn = Convert.ToDateTime(dtSession.Rows[0]["CreatedOn"]);

                        //if (dtSession.Rows[0]["ExpiredOn"] != DBNull.Value)
                        //{
                        //    session.ExpiredOn = Convert.ToDateTime(dtSession.Rows[0]["ExpiredOn"]);
                        //}
                        session.Id = new Guid(dtSession.Rows[0]["SessionID"].ToString());
                        session.IPAddress = Convert.ToString(dtSession.Rows[0]["IPAddress"]);
                        //session.IsAuth = Convert.ToBoolean(dtSession.Rows[0]["IsAuth"]);
                        session.Medium = Convert.ToString(dtSession.Rows[0]["Medium"]);
                        session.Source = Convert.ToString(dtSession.Rows[0]["Source"]);
                        session.UserAgent = Convert.ToString(dtSession.Rows[0]["UserAgent"]);
                        session.UserAgentTypeID = Convert.ToInt32(dtSession.Rows[0]["UserAgentTypeID"]);
                        //session.VisitType = Convert.ToString(dtSession.Rows[0]["VisitType"]);
                        session.Utm_Source = Convert.ToString(dtSession.Rows[0]["Utm_Source"]);
                        session.Utm_Keyword = Convert.ToString(dtSession.Rows[0]["Utm_Keyword"]);
                        session.Utm_Term = Convert.ToString(dtSession.Rows[0]["Utm_Term"]);
                        session.Utm_Content = Convert.ToString(dtSession.Rows[0]["Utm_Content"]);
                        session.Requested_URL = Convert.ToString(dtSession.Rows[0]["Requested_URL"]);
                        session.URL_Referrer = Convert.ToString(dtSession.Rows[0]["URL_Referrer"]);
                        session.DomainCD = Convert.ToString(dtSession.Rows[0]["DomainCD"]);
                        return session;
                    }
                }
            }
            catch (Exception ex)
            {
                var htParams = new Hashtable(2);
                htParams.Add("SessionID", SessionID);
                htParams.Add("LastRecordedSqlStatement", LastRecordedSqlStatement);
                var objMethod = (MethodInfo) MethodBase.GetCurrentMethod();
                GlobalExceptionHandler.customExceptionHandler(ex, objMethod, htParams, ErrorSeverityType.Normal);
                return null;
            }

            return null;
        }

        /// <summary>
        ///     Refresh session, increase expiredon by current timestamp
        /// </summary>
        /// <param name="sessionId"></param>
        public void RefreshSession(string sessionId)
        {
            var iExtendMinutes = -1;
            var query = "update [Session] set [ExpiredOn]=@expiredOn where [SessionID]=@sessionID";
            var newExpiredOnDate = DateTime.Now;
            SqlParameter[] parameters;
            SQLDataManager dataManager = null;

            try
            {
                if (!string.IsNullOrEmpty(sessionId))
                {
                    int.TryParse(ConfigManager.GetConfig("SESSION_EXTEND"), out iExtendMinutes);
                    if (iExtendMinutes <= 0)
                        iExtendMinutes = 30;
                    // increase ExpiredOn 
                    newExpiredOnDate = DateTime.Now.AddMinutes(iExtendMinutes);
                    // add parameters
                    parameters = new SqlParameter[2];
                    parameters[0] = new SqlParameter("@expiredOn", newExpiredOnDate);
                    parameters[0].SqlDbType = SqlDbType.DateTime;
                    parameters[1] = new SqlParameter("@sessionID", SqlDbType.NVarChar, 36);
                    parameters[1].Value = sessionId;
                    // update via ADO.Net
                    dataManager = new SQLDataManager();
                    dataManager.UpdateDetails(query, parameters);
                }
            }
            catch (Exception ex)
            {
                var htParams = new Hashtable(3);
                htParams.Add("SessionID", sessionId);
                htParams.Add("ExtendMinutes", iExtendMinutes);
                htParams.Add("Query", query);
                var objMethod = (MethodInfo) MethodBase.GetCurrentMethod();
                GlobalExceptionHandler.customExceptionHandler(ex, objMethod, htParams, ErrorSeverityType.Normal);
            }
        }

        public void ExpireSession(string sessionId)
        {
            try
            {
                if (!string.IsNullOrEmpty(sessionId))
                {
                    var sQuery = "Update [Session] Set [ExpiredOn]=@expiredOn Where [SessionID]=@sessionID";
                    var parameters = new SqlParameter[2];
                    parameters[0] = new SqlParameter("@expiredOn", DateTime.Now);
                    parameters[0].SqlDbType = SqlDbType.DateTime;
                    parameters[1] = new SqlParameter("@sessionID", SqlDbType.NVarChar, 36);
                    parameters[1].Value = sessionId;
                    // update via ADO.Net
                    var dataManager = new SQLDataManager();
                    dataManager.UpdateDetails(sQuery, parameters);
                }
            }
            catch (Exception ex)
            {
                var htParams = new Hashtable(1);
                htParams.Add("SessionID", sessionId);
                var objMethod = (MethodInfo) MethodBase.GetCurrentMethod();
                GlobalExceptionHandler.customExceptionHandler(ex, objMethod, htParams, ErrorSeverityType.Normal);
            }
        }

        public IList<Session> GetSessionUserAgentTypeByCriteria(string SessionId, int UserAgentTypeId)
        {
            try
            {
                var criteria = GetNewCriteria();
                criteria.Add(Restrictions.Eq("Id", new Guid(SessionId)));
                criteria.Add(Restrictions.Eq("UserAgentTypeID", UserAgentTypeId));
                return criteria.List<Session>();
            }
            catch (Exception Ex)
            {
                var htParams = new Hashtable(1);
                var objMethod = (MethodInfo) MethodBase.GetCurrentMethod();
                GlobalExceptionHandler.customExceptionHandler(Ex, objMethod, htParams, ErrorSeverityType.Normal);
                return null;
            }
        }

        public void UpdateSessionUserAgent(string sessionID, int userAgentTypeID, string userAgent)
        {
            try
            {
                var sQuery =
                    "UPDATE [Session] SET [UserAgentTypeID] = @UserAgentTypeID, [UserAgent] = @UserAgent WHERE [SessionID] = @SessionID";
                var parameters = new SqlParameter[3];
                parameters[0] = new SqlParameter("@SessionID", SqlDbType.NVarChar, 36);
                parameters[0].Value = sessionID;
                parameters[1] = new SqlParameter("@UserAgentTypeID", SqlDbType.Int);
                parameters[1].Value = userAgentTypeID;
                parameters[2] = new SqlParameter("@UserAgent", SqlDbType.NVarChar);
                parameters[2].Value = userAgent;

                // update via ADO.Net
                var dataManager = new SQLDataManager();
                dataManager.UpdateDetails(sQuery, parameters);
            }
            catch (Exception ex)
            {
                var htParams = new Hashtable(3);
                htParams.Add("SessionID", sessionID);
                htParams.Add("UserAgentTypeID", userAgentTypeID);
                htParams.Add("UserAgent", userAgent);
                var objMethod = (MethodInfo) MethodBase.GetCurrentMethod();
                GlobalExceptionHandler.customExceptionHandler(ex, objMethod, htParams, ErrorSeverityType.Normal);
            }
        }

        public new void Save(Session session, bool commitChanges)
        {
            SaveOrUpdate(session, commitChanges);
        }

        //public new void SaveOrUpdate(Session session, bool commitChanges)
        //{
        //    DateTime newExpiredOnDate = DateTime.Now;
        //    SqlParameter[] parameters;
        //    SQLDataManager dataManager = null;
        //    string sQuery = string.Empty;

        //    try
        //    {
        //        sQuery = "prc_Session_Save";

        //        if (session != null)
        //        {
        //            parameters = new SqlParameter[18];
        //            // Campaign
        //            if (session.Campaign != null) parameters[0] = new SqlParameter("@campaign", session.Campaign);
        //            // CreatedOn
        //            if (session.CreatedOn != null) parameters[1] = new SqlParameter("@createdOn", session.CreatedOn);
        //            // ExpiredOn
        //            if (session.ExpiredOn != null) parameters[2] = new SqlParameter("@expiredOn", session.ExpiredOn);
        //            // SessionID
        //            parameters[3] = new SqlParameter("@sessionID", SqlDbType.NVarChar, 36);
        //            if (session.Id != null && !session.Id.Equals(Guid.Parse("00000000-0000-0000-0000-000000000000")))
        //                parameters[3].Value = session.Id.ToString();
        //            else
        //            {
        //                session.Id = Guid.NewGuid();
        //                parameters[3].Value = session.Id.ToString();
        //            }
        //            // IPAddress
        //            if (session.IPAddress != null) parameters[4] = new SqlParameter("@ipAddress", session.IPAddress);
        //            // IsAuth
        //            parameters[5] = new SqlParameter("@isAuth", session.IsAuth);
        //            // Medium
        //            if (session.Medium != null) parameters[6] = new SqlParameter("@medium", session.Medium);
        //            // Source
        //            if (session.Source != null) parameters[7] = new SqlParameter("@source", session.Source);
        //            // UserAgent
        //            if (session.UserAgent != null) parameters[8] = new SqlParameter("@userAgent", session.UserAgent);
        //            // UserAgentTypeID
        //            parameters[9] = new SqlParameter("@userAgentTypeID", session.UserAgentTypeID);
        //            // VisitType
        //            if (session.VisitType != null) parameters[10] = new SqlParameter("@visitType", session.VisitType);
        //            // Campaign
        //            if (session.Utm_Source != null) parameters[11] = new SqlParameter("@utm_source", session.Utm_Source);
        //            // Campaign
        //            if (session.Utm_Keyword != null) parameters[12] = new SqlParameter("@utm_keyword", session.Utm_Keyword);
        //            // Campaign
        //            if (session.Utm_Term != null) parameters[13] = new SqlParameter("@utm_term", session.Utm_Term);
        //            // Campaign
        //            if (session.Utm_Content != null) parameters[14] = new SqlParameter("@utm_content", session.Utm_Content);
        //            //Requested URL
        //            if (session.Requested_URL != null) parameters[15] = new SqlParameter("@requested_url", session.Requested_URL);
        //            //URL Referral
        //            if (session.URL_Referrer != null) parameters[16] = new SqlParameter("@url_referrer", session.URL_Referrer);
        //            //Domain Code
        //            if (session.DomainCD != null) parameters[17] = new SqlParameter("@domain_code", session.DomainCD);

        //            dataManager = new SQLDataManager();
        //            dataManager.UpdateDetails(sQuery.ToString(), parameters, CommandType.StoredProcedure);
        //        }
        //    }
        //    catch (Exception ex)
        //    {
        //        Hashtable htParams = new Hashtable(1);
        //        htParams.Add("Session", session);
        //        MethodInfo objMethod = (MethodInfo)MethodBase.GetCurrentMethod();
        //        GlobalExceptionHandler.customExceptionHandler(ex, objMethod, htParams, ErrorSeverityType.Normal);
        //    }
        //}

        public new void SaveOrUpdate(Session session, bool commitChanges)
        {
            var newExpiredOnDate = DateTime.Now;
            SqlParameter[] parameters;
            SQLDataManager dataManager = null;
            var sQuery = string.Empty;

            try
            {
                sQuery = "prc_Session_Save";

                if (session != null)
                {
                    parameters = new SqlParameter[18];
                    // Campaign
                    if (session.Campaign != null) parameters[0] = new SqlParameter("@campaign", session.Campaign);
                    // CreatedOn
                    if (session.CreatedOn != null) parameters[1] = new SqlParameter("@createdOn", session.CreatedOn);
                    // ExpiredOn
                    //if (session.ExpiredOn != null) parameters[2] = new SqlParameter("@expiredOn", session.ExpiredOn);
                    // SessionID
                    parameters[2] = new SqlParameter("@sessionID", SqlDbType.NVarChar, 36);
                    if (!session.Id.Equals(Guid.Empty))
                    {
                        parameters[2].Value = session.Id.ToString();
                    }
                    else
                    {
                        session.Id = Guid.NewGuid();
                        parameters[2].Value = session.Id.ToString();
                    }

                    // IPAddress
                    if (session.IPAddress != null) parameters[3] = new SqlParameter("@ipAddress", session.IPAddress);
                    // IsAuth
                    //parameters[5] = new SqlParameter("@isAuth", session.IsAuth);
                    // Medium
                    if (session.Medium != null) parameters[4] = new SqlParameter("@medium", session.Medium);
                    // Source
                    if (session.Source != null) parameters[5] = new SqlParameter("@source", session.Source);
                    // UserAgent
                    if (session.UserAgent != null) parameters[6] = new SqlParameter("@userAgent", session.UserAgent);
                    // UserAgentTypeID
                    parameters[7] = new SqlParameter("@userAgentTypeID", session.UserAgentTypeID);
                    // VisitType
                    //if (session.VisitType != null) parameters[10] = new SqlParameter("@visitType", session.VisitType);
                    // Campaign
                    if (session.Utm_Source != null) parameters[7] = new SqlParameter("@utm_source", session.Utm_Source);
                    // Campaign
                    if (session.Utm_Keyword != null)
                        parameters[9] = new SqlParameter("@utm_keyword", session.Utm_Keyword);
                    // Campaign
                    if (session.Utm_Term != null) parameters[10] = new SqlParameter("@utm_term", session.Utm_Term);
                    // Campaign
                    if (session.Utm_Content != null)
                        parameters[11] = new SqlParameter("@utm_content", session.Utm_Content);
                    //Requested URL
                    if (session.Requested_URL != null)
                        parameters[12] = new SqlParameter("@requested_url", session.Requested_URL);
                    //URL Referral
                    if (session.URL_Referrer != null)
                        parameters[13] = new SqlParameter("@url_referrer", session.URL_Referrer);
                    //Domain Code
                    if (session.DomainCD != null) parameters[14] = new SqlParameter("@domain_code", session.DomainCD);

                    dataManager = new SQLDataManager();
                    dataManager.UpdateDetails(sQuery, parameters, CommandType.StoredProcedure);
                }
            }
            catch (Exception ex)
            {
                var htParams = new Hashtable(1);
                htParams.Add("Session", session);
                var objMethod = (MethodInfo) MethodBase.GetCurrentMethod();
                GlobalExceptionHandler.customExceptionHandler(ex, objMethod, htParams, ErrorSeverityType.Normal);
            }
        }

        public string GetSessionMediumById(string sessionId)
        {
            string sessionMedium = null;
            var sQuery = "SELECT TOP (1) [Medium] FROM [Session] with (nolock) where [SessionID] = @SessionID";

            try
            {
                if (!string.IsNullOrEmpty(sessionId))
                {
                    var parameters = new SqlParameter[1];
                    parameters[0] = new SqlParameter("@SessionID", SqlDbType.NVarChar, 36);
                    parameters[0].Value = sessionId;

                    var dataManager = new SQLDataManager();
                    sessionMedium = dataManager.ExecuteScalar<string>(sQuery, parameters, CommandType.Text);
                }
            }
            catch (Exception ex)
            {
                var htParams = new Hashtable(1);
                htParams.Add("SessionID", sessionId);
                var objMethod = (MethodInfo) MethodBase.GetCurrentMethod();
                GlobalExceptionHandler.customExceptionHandler(ex, objMethod, htParams, ErrorSeverityType.Normal);
            }

            return sessionMedium;
        }

        public void SetSessionAuthenticated(string sessionId, string visitType)
        {
            try
            {
                var sQuery = string.Empty;
                SqlParameter[] parameters = null;
                if (!string.IsNullOrEmpty(visitType))
                {
                    parameters = new SqlParameter[3];
                    parameters[2] = new SqlParameter("@VisitType", SqlDbType.Char, 1);
                    parameters[2].Value = visitType;
                    sQuery =
                        "UPDATE [Session] SET [IsAuth] = @IsAuth, [VisitType] = @VisitType WHERE [SessionID] = @SessionID";
                }
                else
                {
                    parameters = new SqlParameter[2];
                    sQuery = "UPDATE [Session] SET [IsAuth] = @IsAuth WHERE [SessionID] = @SessionID";
                }

                parameters[0] = new SqlParameter("@IsAuth", true);
                parameters[0].SqlDbType = SqlDbType.Bit;
                parameters[1] = new SqlParameter("@SessionID", SqlDbType.NVarChar, 36);
                parameters[1].Value = sessionId;

                // update via ADO.Net
                var dataManager = new SQLDataManager();
                dataManager.UpdateDetails(sQuery, parameters);
            }
            catch (Exception ex)
            {
                var htParams = new Hashtable(2);
                htParams.Add("SessionId", session);
                htParams.Add("VisitType", visitType);
                var objMethod = (MethodInfo) MethodBase.GetCurrentMethod();
                GlobalExceptionHandler.customExceptionHandler(ex, objMethod, htParams, ErrorSeverityType.Normal);
            }
        }
    }
}