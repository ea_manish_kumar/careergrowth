﻿using DataAccess.Base;
using DomainModel.Tests;

namespace DataAccess.Tests
{
    /// <summary>
    ///     ABTest data classes
    /// </summary>
    public class ABTestDAL : DataManagerBase<ABTest, int>
    {
        #region Constructror

        /// <summary>
        /// </summary>
        public ABTestDAL()
        {
        }

        /// <summary>
        ///     Initializes a new instance of the <see cref="ABTestDataData" /> class.
        /// </summary>
        /// <param name="session">The session.</param>
        public ABTestDAL(INHibernateSession session)
            : base(session)
        {
        }

        #endregion

        //public int InsertABTest(string sABTestName, string sDescription, bool bIsActive)
        //{
        //    IQuery Q = GetNamedQuery("ABTestData.AddABTest");
        //    Q.SetParameter("ABTestName", sABTestName);
        //    Q.SetParameter("Description", sDescription);
        //    Q.SetParameter("IsActive", bIsActive);
        //    return Q.ExecuteUpdate();
        //}

        //public int UpdateABTestStatus(int iABTestID, bool bIsActive)
        //{
        //    IQuery Q = GetNamedQuery("ABTestData.UpdateABTest");
        //    Q.SetParameter("ABTestID", iABTestID);
        //    Q.SetParameter("IsActive", bIsActive);
        //    return Q.ExecuteUpdate();
        //}       
    }
}