﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using CommonModules.Exceptions;
using DataAccess.Base;
using DomainModel.Tests;
using NHibernate;
using NHibernate.Criterion;

namespace DataAccess.Core
{
    public class ABTestDataSessionDAL : DataManagerBase<ABTestDataSession, int>
    {
        /// <summary>
        ///     Get case index for sessionid from ABTestDataSession table
        /// </summary>
        /// <param name="sSessionID"></param>
        /// <param name="iABTestID"></param>
        /// <returns></returns>
        public int? GetCaseIndexBySessionID(string sSessionID, int iABTestID)
        {
            int? CaseIndex = null;
            IList<ABTestDataSession> lstABTestDataSession = null;
            ICriteria criteria = null;

            try
            {
                criteria = GetNewCriteria();
                if (!string.IsNullOrEmpty(sSessionID))
                {
                    criteria.Add(Restrictions.Eq("SessionID", new Guid(sSessionID)));
                    criteria.Add(Restrictions.Eq("ABTestID", iABTestID));

                    lstABTestDataSession = criteria.List<ABTestDataSession>();
                    if (lstABTestDataSession != null && lstABTestDataSession.Count > 0)
                        CaseIndex = lstABTestDataSession[0].CaseIndex;
                }

                //IQuery query = GetSQLQuery("select caseIndex from ABTestData where Abtestid=" + iABTestID.ToString() + " and UserID= " + UserID.ToString());
                //object result = query.UniqueResult();
                //if (result != null)
                //{
                //    CaseIndex = (int)result;
                //}
            }
            catch (Exception ex)
            {
                var htParams = new Hashtable(2);
                htParams.Add("ABTestID", iABTestID);
                htParams.Add("SessionID", sSessionID);
                var objMethod = (MethodInfo) MethodBase.GetCurrentMethod();
                GlobalExceptionHandler.customExceptionHandler(ex, objMethod, htParams, ErrorSeverityType.Normal);
            }

            return CaseIndex;
        }


        public List<ABTestDataSession> GetABTestDataSessionBySessionID(string sSessionID)
        {
            List<ABTestDataSession> lstABTestDataSession = null;
            ICriteria criteria = null;

            try
            {
                criteria = GetNewCriteria();
                if (!string.IsNullOrEmpty(sSessionID))
                {
                    criteria.Add(Restrictions.Eq("SessionID", new Guid(sSessionID)));

                    lstABTestDataSession = criteria.List<ABTestDataSession>().ToList();
                }
            }
            catch (Exception ex)
            {
                var htParams = new Hashtable(2);
                htParams.Add("SessionID", sSessionID);
                var objMethod = (MethodInfo) MethodBase.GetCurrentMethod();
                GlobalExceptionHandler.customExceptionHandler(ex, objMethod, htParams, ErrorSeverityType.Normal);
            }

            return lstABTestDataSession;
        }
    }
}