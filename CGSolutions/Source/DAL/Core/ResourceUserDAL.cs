﻿using System;
using System.Collections;
using System.Reflection;
using CommonModules.Exceptions;
using DataAccess.Base;
using DomainModel.Core;
using NHibernate.Criterion;

namespace DataAccess.Core
{
    public class ResourceUserDAL : DataManagerBase<ResourceUser, Guid>
    {
        public ResourceUser GetBySessionId(string SessionID)
        {
            try
            {
                var cr = GetNewCriteria();
                cr.Add(Restrictions.Eq("Id", new Guid(SessionID)));

                var lst = cr.List<ResourceUser>();

                if (lst != null && lst.Count > 0) return lst[0];
            }
            catch (Exception ex)
            {
                var htParams = new Hashtable(2);
                htParams.Add("SessionID", SessionID);
                htParams.Add("LastRecordedSqlStatement", LastRecordedSqlStatement);
                var objMethod = (MethodInfo) MethodBase.GetCurrentMethod();
                GlobalExceptionHandler.customExceptionHandler(ex, objMethod, htParams, ErrorSeverityType.Normal);
                return null;
            }

            return null;
        }

        public ResourceUser GetByUserIDAndSessionID(string SessionID, int UserID)
        {
            try
            {
                var cr = GetNewCriteria();
                cr.Add(Restrictions.Eq("Id", new Guid(SessionID)));
                cr.Add(Restrictions.Eq("UserID", UserID));

                var lst = cr.List<ResourceUser>();

                if (lst != null && lst.Count > 0) return lst[0];
            }
            catch (Exception ex)
            {
                var htParams = new Hashtable(2);
                htParams.Add("SessionID", SessionID);
                htParams.Add("LastRecordedSqlStatement", LastRecordedSqlStatement);
                var objMethod = (MethodInfo) MethodBase.GetCurrentMethod();
                GlobalExceptionHandler.customExceptionHandler(ex, objMethod, htParams, ErrorSeverityType.Normal);
                return null;
            }

            return null;
        }
    }
}