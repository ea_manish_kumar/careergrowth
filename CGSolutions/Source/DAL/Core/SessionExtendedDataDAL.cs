﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Reflection;
using CommonModules.Exceptions;
using DataAccess.Base;
using DomainModel.Core;
using NHibernate.Criterion;

namespace DataAccess.Core
{
    public class SessionExtendedDataDAL : DataManagerBase<SessionExtended, Guid>
    {
        public SessionExtended Get(Guid sessionID, Guid browserID)
        {
            SessionExtended sessionextended = null;
            IList<SessionExtended> lstSessionExtended = null;

            try
            {
                var criteria = GetNewCriteria();
                criteria.Add(Restrictions.Eq("SessionID", sessionID));
                criteria.Add(Restrictions.Eq("BrowserID", browserID));
                lstSessionExtended = criteria.List<SessionExtended>();
                if (lstSessionExtended != null && lstSessionExtended.Count > 0) sessionextended = lstSessionExtended[0];
            }
            catch (Exception Ex)
            {
                var htParams = new Hashtable(2);
                htParams.Add("SessionGuid", sessionID);
                htParams.Add("BrowserGuid", browserID);
                var objMethod = (MethodInfo) MethodBase.GetCurrentMethod();
                GlobalExceptionHandler.customExceptionHandler(Ex, objMethod, htParams, ErrorSeverityType.Normal);
                return null;
            }

            return sessionextended;
        }

        #region Constructors

        internal SessionExtendedDataDAL()
        {
        }

        public SessionExtendedDataDAL(INHibernateSession session)
            : base(session)
        {
        }

        #endregion
    }
}