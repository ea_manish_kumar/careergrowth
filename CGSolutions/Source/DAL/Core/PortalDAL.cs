﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Reflection;
using CommonModules.Exceptions;
using DataAccess.Base;
using DomainModel.Core;

namespace DataAccess.Core
{
    public class PortalDAL : DataManagerBase<Portal, int>
    {
        #region Get Methods

        public IList<PortalDAL> GetPortalDAL()
        {
            try
            {
                var criteria = GetNewCriteria();
                return criteria.List<PortalDAL>();
            }
            catch (Exception Ex)
            {
                var htParams = new Hashtable(1);
                var objMethod = (MethodInfo) MethodBase.GetCurrentMethod();
                GlobalExceptionHandler.customExceptionHandler(Ex, objMethod, htParams, ErrorSeverityType.Normal);
                return null;
            }
        }

        #endregion Get Methods

        #region Constructors

        internal PortalDAL()
        {
        }

        /// <summary>
        ///     Initializes a new instance of the <see cref="ABTestAdminNoteDAL" /> class.
        /// </summary>
        /// <param name="session">The session.</param>
        public PortalDAL(INHibernateSession session)
            : base(session)
        {
        }

        #endregion
    }
}