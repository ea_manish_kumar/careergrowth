﻿namespace DataAccess.Core
{
    public class CoreFactory
    {
        #region Constructors

        #endregion

        #region Get Methods

        public static SessionDAL SessionDAL => new SessionDAL();

        public static SessionDataDAL SessionDataDAL => new SessionDataDAL();

        public static SessionExtendedDataDAL SessionExtendedDataDAL => new SessionExtendedDataDAL();

        public static ResourceUserDAL ResourceUserDAL => new ResourceUserDAL();

        public static UserAgentTypeDAL UserAgentTypeDAL => new UserAgentTypeDAL();
        //public static SessionUserAgentTypeDAL SessionUserAgentTypeDAL
        //{
        //    get
        //    {
        //        return new SessionUserAgentTypeDAL();
        //    }
        //}

        public static CultureDAL CultureDAL => new CultureDAL();

        public static PortalDAL PortalDAL => new PortalDAL();

        public static DomainDAL DomainDAL => new DomainDAL();

        #endregion
    }
}