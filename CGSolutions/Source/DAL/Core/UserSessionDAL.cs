﻿using System;
using System.Collections.Generic;
using System.Collections;
using System.Reflection;
using NHibernate;
using NHibernate.Criterion;
using RB3.DAL.Base;
using RB3.Commons.Exceptions;
using RB3.DomainModel.Core;

namespace RB3.DAL.Core
{
    public class UserSessionDAL : DataManagerBase<UserSession, Guid>
    {
        public UserSession GetBySessionId(string SessionID)
        {
            UserSession usrSession = null;
            try
            {
                ICriteria cr = GetNewCriteria();
                cr.Add(Restrictions.Eq("Id", new Guid(SessionID)));
                usrSession = cr.SetMaxResults(1).UniqueResult<UserSession>();
            }
            catch (Exception ex)
            {
                Hashtable htParams = new Hashtable(2);
                htParams.Add("SessionID", SessionID);
                htParams.Add("LastRecordedSqlStatement", base.LastRecordedSqlStatement);
                MethodInfo objMethod = (MethodInfo)MethodBase.GetCurrentMethod();
                GlobalExceptionHandler.customExceptionHandler(ex, objMethod, htParams, RB3SubSystem.User, RB3Application.RB3, ErrorSeverityType.Normal, "Exception while getting user session", true);
            }
            return usrSession;
        }

        public int GetUserIdBySessionId(string SessionID)
        {
            int userId = 0;
            try
            {
                ICriteria cr = GetNewCriteria();
                cr.Add(Restrictions.Eq("Id", new Guid(SessionID)));
                var userSessionData = cr.SetMaxResults(1).UniqueResult<UserSession>();
                if (userSessionData != null)
                    userId = userSessionData.UserID;
            }
            catch (Exception ex)
            {
                Hashtable htParams = new Hashtable(2);
                htParams.Add("SessionID", SessionID);
                htParams.Add("LastRecordedSqlStatement", base.LastRecordedSqlStatement);
                MethodInfo objMethod = (MethodInfo)MethodBase.GetCurrentMethod();
                GlobalExceptionHandler.customExceptionHandler(ex, objMethod, htParams, ErrorSeverityType.Normal);
            }
            return userId;
        }

        public string GetSessionIdByUserId(int userId)
        {
            IList<Guid> lstSessionId = null;
            string sessionGUID = "";
            try
            {
                IQuery Q = GetNamedQueryByStatelessSession("UserSession.GetSessionByUserId");
                Q.SetInt32("UserID", userId);
                lstSessionId = Q.List<Guid>();

                if (lstSessionId != null & lstSessionId.Count > 0)
                    sessionGUID = lstSessionId[0].ToString();
            }
            catch (Exception ex)
            {
                MethodInfo objMethod = (MethodInfo)MethodBase.GetCurrentMethod();
                Hashtable htParams = new Hashtable(1);
                htParams.Add("UserID", userId.ToString());
                GlobalExceptionHandler.customExceptionHandler(ex, objMethod, htParams, ErrorSeverityType.Normal);
            }
            return sessionGUID;
        }

        public void SetSessionUserLink(string sessionId, int UserID)
        {
            try
            {
                UserSession userSession = GetBySessionId(sessionId);
                if (userSession != null)
                {
                    Delete(userSession, true);
                }

                userSession = new UserSession();
                userSession.Id = new Guid(sessionId);
                userSession.UserID = UserID;
                SaveOrUpdate(userSession, true);
            }
            catch (Exception ex)
            {
                MethodInfo objMethod = (MethodInfo)MethodBase.GetCurrentMethod();
                Hashtable htParams = new Hashtable(2);
                htParams.Add("SessionId", sessionId.ToString());
                htParams.Add("UserID", UserID.ToString());
                GlobalExceptionHandler.customExceptionHandler(ex, objMethod, htParams, ErrorSeverityType.Normal);
            }
        }

    }
}
