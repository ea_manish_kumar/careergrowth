﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Reflection;
using CommonModules.Exceptions;
using DataAccess.Base;
using DomainModel.Core;
using NHibernate;

namespace DataAccess.Core
{
    public class DomainDAL : DataManagerBase<Domain, int>
    {
        #region Constructors

        internal DomainDAL()
        {
        }

        /// <summary>
        ///     Initializes a new instance of the <see cref="DomainDAL" /> class.
        /// </summary>
        /// <param name="session">The session.</param>
        public DomainDAL(INHibernateSession session)
            : base(session)
        {
        }

        public override IList<Domain> GetAll()
        {
            try
            {
                var criteria = GetNewCriteria();
                criteria.SetFetchMode("Portal", FetchMode.Eager);
                return criteria.List<Domain>();
            }
            catch (Exception Ex)
            {
                var htParams = new Hashtable(1);
                var objMethod = (MethodInfo) MethodBase.GetCurrentMethod();
                GlobalExceptionHandler.customExceptionHandler(Ex, objMethod, htParams, ErrorSeverityType.Normal);
                return null;
            }
        }

        #endregion
    }
}