﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Reflection;
using CommonModules.Exceptions;
using DataAccess.Base;
using DomainModel.Core;
using NHibernate.Criterion;

namespace DataAccess.Core
{
    public class SessionDataDAL : DataManagerBase<SessionData, Guid>
    {
        /// <summary>
        ///     Saves the specified sessiondata.
        /// </summary>
        /// <param name="objSessionData">The SessionData.</param>
        /// <param name="CommitChanges">if set to <c>true</c> [commit changes].</param>
        public new void Save(SessionData objSessionData, bool CommitChanges)
        {
            try
            {
                base.Save(objSessionData, CommitChanges);
            }
            catch (Exception ex)
            {
                var htParams = new Hashtable(3);
                htParams.Add("SessionData", objSessionData.ToString());
                htParams.Add("CommitChanges", CommitChanges);
                htParams.Add("LastRecordedSqlStatement", LastRecordedSqlStatement);
                var objMethod = (MethodInfo) MethodBase.GetCurrentMethod();
                GlobalExceptionHandler.customExceptionHandler(ex, objMethod, htParams, ErrorSeverityType.Normal);
            }
        }

        public void SaveToSession(string sessionID, string SessionDataTypeCode, object data)
        {
            try
            {
                var sessionGuid = new Guid(sessionID);
                var sessionData = GetSessionDataBySessionIdandType(sessionGuid, SessionDataTypeCode);

                if (sessionData != null)
                {
                    sessionData.Data = data.ToString();
                }
                else
                {
                    sessionData = new SessionData();
                    sessionData.SessionID = sessionGuid;
                    sessionData.Data = data.ToString();
                    sessionData.DataTypeCD = SessionDataTypeCode;
                }

                CoreFactory.SessionDataDAL.SaveOrUpdate(sessionData, true);
            }
            catch (Exception ex)
            {
                var htParams = new Hashtable();
                htParams.Add("sessionID", sessionID);
                htParams.Add("SessionDataTypeCode", SessionDataTypeCode);
                htParams.Add("data", data.ToString());
                htParams.Add("LastRecordedSqlStatement", LastRecordedSqlStatement);
                var objMethod = (MethodInfo) MethodBase.GetCurrentMethod();
                GlobalExceptionHandler.customExceptionHandler(ex, objMethod, htParams, ErrorSeverityType.Normal);
            }
        }

        public SessionData GetFromSession(string sessionID, string SessionDataTypeCode)
        {
            var sessionData = new SessionData();
            try
            {
                if (string.IsNullOrEmpty(sessionID))
                    return null;

                var sessionGuid = new Guid(sessionID);
                sessionData = GetSessionDataBySessionIdandType(sessionGuid, SessionDataTypeCode);
            }
            catch (Exception ex)
            {
                var htParams = new Hashtable();
                htParams.Add("sessionID", sessionID);
                htParams.Add("SessionDataTypeCode", SessionDataTypeCode);
                var objMethod = (MethodInfo) MethodBase.GetCurrentMethod();
                GlobalExceptionHandler.customExceptionHandler(ex, objMethod, htParams, ErrorSeverityType.Normal);
            }

            return sessionData;
        }

        #region Constructors

        internal SessionDataDAL()
        {
        }

        /// <summary>
        ///     Initializes a new instance of the <see cref="UserData" /> class.
        /// </summary>
        /// <param name="session">The session.</param>
        public SessionDataDAL(INHibernateSession session)
            : base(session)
        {
        }

        #endregion


        #region Get Methods

        public SessionData GetSessionDataBySessionIdandType(Guid SessionID, string SessionDataTypeCode)
        {
            IList<SessionData> lstsd = null;
            try
            {
                var qry = CoreFactory.SessionDataDAL.Session.GetISession().CreateCriteria(typeof(SessionData));

                qry.Add(Restrictions.Eq("SessionID", SessionID));
                qry.Add(Restrictions.Eq("DataTypeCD", SessionDataTypeCode));
                lstsd = qry.List<SessionData>();
                if (lstsd != null && lstsd.Count > 0)
                    return lstsd[0];
            }
            catch (Exception ex)
            {
                var htParams = new Hashtable();
                htParams.Add("SessionID", SessionID);
                htParams.Add("DataTypeCode", SessionDataTypeCode);
                var objMethod = (MethodInfo) MethodBase.GetCurrentMethod();
                GlobalExceptionHandler.customExceptionHandler(ex, objMethod, htParams, ErrorSeverityType.Normal);
            }

            return null;
        }

        public IList<SessionData> GetSessionDataBySessionId(Session Session)
        {
            IList<SessionData> lstSd = null;
            try
            {
                var qry = CoreFactory.SessionDataDAL.Session.GetISession().CreateCriteria(typeof(SessionData));

                qry.Add(Restrictions.Eq("Session", Session));
                lstSd = qry.List<SessionData>();
            }
            catch (Exception ex)
            {
                var htParams = new Hashtable();
                htParams.Add("SessionID", Session.Id);

                var objMethod = (MethodInfo) MethodBase.GetCurrentMethod();
                GlobalExceptionHandler.customExceptionHandler(ex, objMethod, htParams, ErrorSeverityType.Normal);
            }

            return lstSd;
        }

        #endregion
    }
}