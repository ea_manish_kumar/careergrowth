﻿using System;
using System.Collections.Generic;
using System.Reflection;
using CommonModules.Exceptions;
using DataAccess.Base;
using DomainModel.Core;

public class CultureDAL : DataManagerBase<Culture, int>
{
    #region Get Methods

    /// <summary>
    ///     Returns All Culture objects available in the system from the NH Second Level Cache
    /// </summary>
    /// <returns></returns>
    public override IList<Culture> GetAll()
    {
        IList<Culture> lstCulture = null;
        try
        {
            var criteria = GetNewCriteria();
            lstCulture = criteria.List<Culture>();
        }
        catch (Exception ex)
        {
            var objMethod = (MethodInfo) MethodBase.GetCurrentMethod();
            GlobalExceptionHandler.customExceptionHandler(ex, objMethod, null, ErrorSeverityType.Normal);
        }

        return lstCulture;
    }

    #endregion

    #region Constructors

    internal CultureDAL()
    {
    }

    /// <summary>
    ///     Initializes a new instance of the <see cref="Culture" /> class.
    /// </summary>
    /// <param name="session">The session.</param>
    public CultureDAL(INHibernateSession session)
        : base(session)
    {
    }

    #endregion
}