﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Reflection;
using CommonModules.Exceptions;
using DataAccess.Base;
using DomainModel.Tests;
using NHibernate;
using NHibernate.Criterion;

namespace DataAccess.Tests
{
    /// <summary>
    ///     ABTestCaseData
    /// </summary>
    public class ABTestCaseDAL : DataManagerBase<ABTestCase, int>
    {
        #region Constructror

        /// <summary>
        ///     default constructror
        /// </summary>
        public ABTestCaseDAL()
        {
        }

        /// <summary>
        ///     Initializes a new instance of the <see cref="ABTestDataData" /> class.
        /// </summary>
        /// <param name="session">The session.</param>
        public ABTestCaseDAL(INHibernateSession session)
            : base(session)
        {
        }

        #endregion

        #region GetMethods

        /// <summary>
        ///     Get Total weigth fora test case
        /// </summary>
        /// <param name="ABTestId"></param>
        /// <returns></returns>
        /// <modifiedby>Promothash</modifiedby>
        /// <date>05/23/2012</date>
        public int GetABTestTotalWeight(int ABTestId)
        {
            //Commented by Promothash 23/5/2012
            var TotalWeight = 0;
            //IList<ABTestCase> lst = new List<ABTestCase>();
            // ICriteria crCm = null;
            try
            {
                //crCm = GetNewCriteria();
                //crCm.Add(Expression.Eq("ParentABTest.Id", ABTestId));
                //lst = crCm.List<ABTestCase>();
                //foreach (ABTestCase abtestcase in lst)
                //{
                //    TotalWeight += Convert.ToInt32(abtestcase.Weight);
                //}
                IQuery query = GetSQLQuery("select Sum(weight)TotalWeight from ABTestCase where ABTestId=" + ABTestId);
                var result = query.UniqueResult();
                if (result != null) TotalWeight = (int) result;
            }
            catch (Exception ex)
            {
                var htParams = new Hashtable();
                var objMethod = (MethodInfo) MethodBase.GetCurrentMethod();
                GlobalExceptionHandler.customExceptionHandler(ex, objMethod, htParams, ErrorSeverityType.Normal);
            }


            return TotalWeight;
        }

        /// <summary>
        ///     Returns a list of test case for a ABTestID
        /// </summary>
        /// <param name="ABTestID"></param>
        /// <returns></returns>
        public IList<ABTestCase> GetABTestCasesByID(int ABTestID)
        {
            IList<ABTestCase> lst = null;
            try
            {
                var criteria = GetNewCriteria();
                criteria.Add(Restrictions.Eq("ParentABTest.Id", ABTestID));
                lst = criteria.List<ABTestCase>();
            }
            catch (Exception ex)
            {
                var htParams = new Hashtable();
                var objMethod = (MethodInfo) MethodBase.GetCurrentMethod();
                GlobalExceptionHandler.customExceptionHandler(ex, objMethod, htParams, ErrorSeverityType.Normal);
            }

            return lst;
        }

        public IList<ABTestCase> GetABTestCasesByABTestID(int ABTestID)
        {
            IList<ABTestCase> lst = null;
            try
            {
                var criteria = GetNewCriteria();
                criteria.Add(Restrictions.Eq("ParentABTest.Id", ABTestID));
                lst = criteria.List<ABTestCase>();
            }
            catch (Exception ex)
            {
                var htParams = new Hashtable();
                var objMethod = (MethodInfo) MethodBase.GetCurrentMethod();
                GlobalExceptionHandler.customExceptionHandler(ex, objMethod, htParams, ErrorSeverityType.Normal);
            }

            return lst;
        }

        public ABTestCase GetABTestCasesByABTestID_CaseIndex(int ABTestId, int CaseIndex)
        {
            ABTestCase oABTestCase = null;
            try
            {
                var criteria = GetNewCriteria();
                criteria.Add(Restrictions.Eq("ParentABTest.Id", ABTestId));
                criteria.Add(Restrictions.Eq("CaseIndex", CaseIndex));
                oABTestCase = criteria.UniqueResult<ABTestCase>();
            }
            catch (Exception ex)
            {
                var htParams = new Hashtable();
                var objMethod = (MethodInfo) MethodBase.GetCurrentMethod();
                GlobalExceptionHandler.customExceptionHandler(ex, objMethod, htParams, ErrorSeverityType.Normal);
            }

            return oABTestCase;
        }

        #endregion
    }
}