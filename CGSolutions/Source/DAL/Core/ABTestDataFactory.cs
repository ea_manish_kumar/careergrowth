﻿using DataAccess.Tests;

namespace DataAccess.Core
{
    /// <summary>
    ///     This class provides access to the Data Manager objects
    /// </summary>
    public class ABTestDataFactory
    {
        #region Constructors

        #endregion

        #region Declarations

        #endregion

        #region Get Methods

        /// <summary>
        ///     Returns an instance of the ABTestData class
        /// </summary>
        /// <value>The product data.</value>
        public static ABTestDAL ABTestDAL => new ABTestDAL();

        /// <summary>
        ///     Returns an instance of the ABTestData class
        /// </summary>
        /// <value>The product data.</value>
        public static ABTestDataDAL ABTestDataDAL => new ABTestDataDAL();

        public static ABTestCaseDAL ABTestCaseDAL => new ABTestCaseDAL();

        public static ABTestAdminNoteDAL ABTestAdminNoteDAL => new ABTestAdminNoteDAL();

        public static ABTestDataSessionDAL ABTestDataSessionDAL => new ABTestDataSessionDAL();

        #endregion
    }
}