﻿using System;
using System.Collections.Generic;
using System.Reflection;
using System.Collections;
using RB3.DAL.Base;
using RB3.Commons.Exceptions;
using NHibernate;
using NHibernate.Criterion;
using RB3.DomainModel.Core;

namespace RB3.DAL.Core
{
    public partial class SessionUserAgentTypeDAL : DataManagerBase<SessionUserAgentType, int>
    {

        #region Constructors

        internal SessionUserAgentTypeDAL()
            : base()
        {
        }
        /// <summary>
        /// Initializes a new instance of the <see cref="RevenueDAL"/> class.
        /// </summary>
        /// <param name="session">The session.</param>
        public SessionUserAgentTypeDAL(INHibernateSession session)
            : base(session)
        {
        }

        #endregion

        #region Get Methods
        public IList<SessionUserAgentType> GetSessionUserAgentTypeByCriteria(string SessionId, int UserAgentTypeId)
        {
            try
            {
                ICriteria criteria = GetNewCriteria();
                criteria.Add(NHibernate.Criterion.Expression.Eq("SessionID", SessionId));
                criteria.Add(NHibernate.Criterion.Expression.Eq("UserAgentTypeID", UserAgentTypeId));
                return criteria.List<SessionUserAgentType>();
            }
            catch (Exception Ex)
            {
                Hashtable htParams = new Hashtable(1);
                MethodInfo objMethod = (MethodInfo)MethodBase.GetCurrentMethod();
                GlobalExceptionHandler.customExceptionHandler(Ex, objMethod, htParams, ErrorSeverityType.Normal);                
                 return null;
            }          
        }

        
        #endregion Get Methods
    }
}
