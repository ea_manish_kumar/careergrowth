﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Reflection;
using CommonModules.Exceptions;
using DataAccess.Base;
using DomainModel.Tests;
using NHibernate.Criterion;

namespace DataAccess.Tests
{
    public class ABTestAdminNoteDAL : DataManagerBase<ABTestAdminNote, int>
    {
        #region Get Methods

        public IList<ABTestAdminNote> GetABTestAdminNotesByABTestId(int ABTestId)
        {
            try
            {
                var criteria = GetNewCriteria();
                criteria.Add(Restrictions.Eq("ABTestID", ABTestId));

                return criteria.List<ABTestAdminNote>();
            }
            catch (Exception Ex)
            {
                var htParams = new Hashtable(1);
                htParams.Add("ABTestID", ABTestId.ToString());
                var objMethod = (MethodInfo) MethodBase.GetCurrentMethod();
                GlobalExceptionHandler.customExceptionHandler(Ex, objMethod, htParams, ErrorSeverityType.Normal);
            }

            return null;
        }

        #endregion Get Methods

        #region Constructors

        internal ABTestAdminNoteDAL()
        {
        }

        /// <summary>
        ///     Initializes a new instance of the <see cref="ABTestAdminNoteDAL" /> class.
        /// </summary>
        /// <param name="session">The session.</param>
        public ABTestAdminNoteDAL(INHibernateSession session)
            : base(session)
        {
        }

        #endregion
    }
}