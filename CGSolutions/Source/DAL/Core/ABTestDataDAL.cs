﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Reflection;
using CommonModules.Exceptions;
using DataAccess.Base;
using DomainModel.Tests;
using NHibernate;
using NHibernate.Criterion;

namespace DataAccess.Tests
{
    /// <summary>
    ///     Class for ABTestDataData
    /// </summary>
    public class ABTestDataDAL : DataManagerBase<ABTestData, int>
    {
        #region Constructror

        /// <summary>
        ///     Default Constructror
        /// </summary>
        public ABTestDataDAL()
        {
        }

        /// <summary>
        ///     Initializes a new instance of the <see cref="ABTestDataData" /> class.
        /// </summary>
        /// <param name="session">The session.</param>
        public ABTestDataDAL(INHibernateSession session)
            : base(session)
        {
        }

        #endregion

        #region Get Methods

        /// <summary>
        ///     Get ABTestData for User by ABTestID
        /// </summary>
        /// <param name="UserID"></param>
        /// <param name="ABTestID"></param>
        /// <returns></returns>
        public ABTestData GetABTestByUserID(int UserID, int ABTestID)
        {
            IList<ABTestData> lst = null;
            ABTestData abTestData = null;

            try
            {
                var q = GetNewCriteria();
                q.Add(Restrictions.Eq("UserID", UserID));
                q.Add(Restrictions.Eq("ABTestID", ABTestID));
                lst = q.List<ABTestData>();

                if (lst != null && lst.Count > 0) abTestData = lst[0];
            }
            catch (Exception ex)
            {
                var htParams = new Hashtable(2);
                htParams.Add("UserID", UserID);
                htParams.Add("ABTestID", ABTestID);
                var objMethod = (MethodInfo) MethodBase.GetCurrentMethod();
                GlobalExceptionHandler.customExceptionHandler(ex, objMethod, htParams, ErrorSeverityType.Normal);
            }

            return abTestData;
        }

        /// <summary>
        ///     Updates the UserID of the Document.
        /// </summary>
        /// <param name="FromUserID"></param>
        /// <param name="ToUserID"></param>
        /// <returns></returns>
        public int UpdateABTestDataUserID(int FromUserID, int ToUserID)
        {
            //Create a Query object for the Named Query specified in the Documents.NamedQueries.hbm.xml file
            var Q = GetNamedQuery("ABTestData.UpdateABTestDataUserID");
            //Set the value for parameter FromUserID
            Q.SetInt32("FromUserID", FromUserID);
            //Set the value for parameter DocID
            Q.SetInt32("ToUserID", ToUserID);
            //Run the query
            return Q.ExecuteUpdate();
        }

        public int? GetABTestCaseIndex(int UserID, int ABTestID)
        {
            int? CaseIndex = null;
            IList<int> result = null;

            try
            {
                IQuery query = GetSQLQuery("select caseIndex from ABTestData where Abtestid=" + ABTestID +
                                           " and UserID= " + UserID);
                result = query.List<int>();
                if (result != null && result.Count > 0) CaseIndex = result[0];
            }
            catch (Exception ex)
            {
                var htParams = new Hashtable(1);
                htParams.Add("UserID", UserID);
                htParams.Add("ABTestID", ABTestID);
                var objMethod = (MethodInfo) MethodBase.GetCurrentMethod();
                GlobalExceptionHandler.customExceptionHandler(ex, objMethod, htParams, ErrorSeverityType.Normal);
            }

            return CaseIndex;
        }

        // <summary>
        /// Get ABTest Report for given AbTestId and UserId
        /// </summary>
        /// <param name="ABTestId">ABTestId</param>
        /// <param name="StartDate">StartDate</param>
        /// <param name="EndDate">EndDate</param>
        /// <returns>List of object ABTestDTO</returns>
        public int ConductABTest(int ABTestID, int UserID)
        {
            int? abTestCase = null;
            try
            {
                var Q = GetNamedQuery("ABTests.GetABTestCaseIndex");
                Q.SetParameter("ABTestID", ABTestID);
                Q.SetParameter("UserID", UserID);
                var abTestLst = Q.List<int>();

                if ((abTestLst != null) & (abTestLst.Count > 0))
                    abTestCase = abTestLst[0];
            }
            catch (Exception Ex)
            {
                var htParams = new Hashtable(2);
                htParams.Add("ABTestID", ABTestID);
                htParams.Add("UserID", UserID);
                var objMethod = (MethodInfo) MethodBase.GetCurrentMethod();
                GlobalExceptionHandler.customExceptionHandler(Ex, objMethod, htParams, ErrorSeverityType.Normal);
            }

            return abTestCase.HasValue ? abTestCase.Value : 1;
        }

        #endregion
    }
}