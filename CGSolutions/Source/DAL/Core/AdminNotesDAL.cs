﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Reflection;
using RB3.DAL.Base;
using RB3.DomainModel.Tests;
using NHibernate;
using NHibernate.Criterion;
using RB3.Commons.Exceptions;

namespace RB3.DAL.Tests
{
    /// <summary>
    /// Class for AdminNoteDAL
    /// </summary>
    public class AdminNoteDAL : DataManagerBase<RB3.DomainModel.Tests.AdminNote, int>
    {
        #region Constructror
        /// <summary>
        /// Default Constructror
        /// </summary>
        public AdminNoteDAL()
            : base()
        {

        }

        /// <summary>
        /// Initializes a new instance of the <see cref="AdminNoteDAL"/> class.
        /// </summary>
        /// <param name="session">The session.</param>
        public AdminNoteDAL(INHibernateSession session)
            : base(session)
        {
        }
        #endregion

        #region Get Methods

        /// <summary>
        /// Resurns List of ABtestData for PartyId by ABTestId
        /// </summary>
        /// <param name="PartyID"></param>
        /// <param name="ABTestID"></param>
        /// <returns></returns>
        public IList<AdminNoteDAL> GetAdminNotes(int ABTestId, int adminUserID)
        {
            IList<AdminNoteDAL> lst = null;
            try
            {
                IQuery Q = GetNamedQuery("AdminNoteDAL.GetABTestAdminNotes");
                Q.SetParameter("ABTestID", ABTestId);
                Q.SetParameter("adminUserID", adminUserID);

                lst = Q.List<AdminNoteDAL>();
                //lst = q.List<RB3.DomainModel.Tests.AdminNoteDAL>();
            }
            catch (Exception ex)
            {
                Hashtable htParams = new Hashtable(1);
                htParams.Add("ABTestID", ABTestId);
                htParams.Add("adminUserID", adminUserID);
                MethodInfo objMethod = (MethodInfo)MethodBase.GetCurrentMethod();
                GlobalExceptionHandler.customExceptionHandler(ex, objMethod, htParams, ErrorSeverityType.Normal);

            }
            return lst;
        }

        #endregion
    }
}
