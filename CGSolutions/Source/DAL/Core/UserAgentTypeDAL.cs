﻿using System;
using System.Collections;
using System.Reflection;
using CommonModules.Exceptions;
using DataAccess.Base;
using DomainModel.Core;
using NHibernate.Criterion;

namespace DataAccess.Core
{
    public class UserAgentTypeDAL : DataManagerBase<UserAgentType, int>
    {
        #region Get Methods

        public int GetUserAgentIDByCriteria(string BrowserName, string BrowserVersion, string EngineName,
            string EngineVersion, string OSName, string OSVersion, string DeviceModel, string DeviceType,
            string DTDeviceType)
        {
            var userAgentTypeId = 0;
            try
            {
                var criteria = GetNewCriteria();
                criteria.Add(Restrictions.Eq("BrowserName", BrowserName ?? string.Empty));
                criteria.Add(Restrictions.Eq("BrowserVersion", BrowserVersion ?? string.Empty));
                criteria.Add(Restrictions.Eq("EngineName", EngineName ?? string.Empty));
                criteria.Add(Restrictions.Eq("EngineVersion", EngineVersion ?? string.Empty));
                criteria.Add(Restrictions.Eq("OSName", OSName ?? string.Empty));
                criteria.Add(Restrictions.Eq("OSVersion", OSVersion ?? string.Empty));
                criteria.Add(Restrictions.Eq("DeviceModel", DeviceModel ?? string.Empty));
                criteria.Add(Restrictions.Eq("DeviceType", DeviceType ?? string.Empty));
                criteria.Add(Restrictions.Eq("DTDeviceType", DTDeviceType ?? string.Empty));
                criteria.SetProjection(Projections.Property("UserAgentTypeID"));
                criteria.SetMaxResults(1);
                userAgentTypeId = criteria.UniqueResult<int>();
            }
            catch (Exception Ex)
            {
                var htParams = new Hashtable(1);
                var objMethod = (MethodInfo) MethodBase.GetCurrentMethod();
                GlobalExceptionHandler.customExceptionHandler(Ex, objMethod, htParams, ErrorSeverityType.Normal);
            }

            return userAgentTypeId;
        }

        #endregion Get Methods

        #region Constructors

        internal UserAgentTypeDAL()
        {
        }

        /// <summary>
        ///     Initializes a new instance of the <see cref="RevenueDAL" /> class.
        /// </summary>
        /// <param name="session">The session.</param>
        public UserAgentTypeDAL(INHibernateSession session)
            : base(session)
        {
        }

        #endregion
    }
}