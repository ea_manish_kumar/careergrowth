﻿using System;
using System.Collections;
using System.IO;
using System.Text;
using System.Web;
using NHibernate;
using NHibernate.SqlCommand;
using NHibernate.Type;

namespace DataAccess.Base
{
    public class Interceptor : EmptyInterceptor, IDisposable
    {
        /// <summary>
        ///     Used for singleton object locking
        /// </summary>
        private static readonly object syncRoot = new object();

        private bool _disposed;

        /// <summary>
        ///     Private variable to hold value for property LastRecordedSqlStatement
        /// </summary>
        private string _lastRecordedSqlStatement;

        /// <summary>
        ///     holds the string data of stdout
        /// </summary>
        private readonly StringBuilder _sb = new StringBuilder();

        /// <summary>
        ///     the stdout redirector
        /// </summary>
        private readonly TextWriter _writer;

        /// <summary>
        ///     Initializes a new instance of the <see cref="Interceptor" /> class.
        ///     Also hooks the standard output to the local TextReader
        /// </summary>
        public Interceptor()
        {
            // Vikas Jindal (8/26/2013): TODO: We are in affect capturing the stdout for the whole application here. This job was better done in App FW maybe.

            _writer = new StringWriter(_sb);
            Console.SetOut(_writer);
            LastRecordedSqlStatement = string.Empty;
        }

        /// <summary>
        ///     This property will be used to get/set value of LastRecordedSqlStatement (string)
        /// </summary>
        public string LastRecordedSqlStatement
        {
            get => _lastRecordedSqlStatement;
            private set
            {
                lock (syncRoot)
                {
                    _lastRecordedSqlStatement = value;
                }
            }
        }

        /// <summary>
        ///     Gets a value indicating whether this instance is web context.
        /// </summary>
        /// <value>
        ///     <c>true</c> if this instance is web context; otherwise, <c>false</c>.
        /// </value>
        private bool IsWebContext => HttpContext.Current != null;


        /// <summary>
        ///     Performs application-defined tasks associated with freeing, releasing, or resetting unmanaged resources.
        /// </summary>
        /// <remarks>
        ///     // Implement IDisposable.Do not make this method virtual.A derived class should not be able to override this
        ///     method.
        /// </remarks>
        public void Dispose()
        {
            Dispose(true);
            // This object will be cleaned up by the Dispose method.
            // Therefore, you should call GC.SupressFinalize to
            // take this object off the finalization queue
            // and prevent finalization code for this object
            // from executing a second time.
            GC.SuppressFinalize(this);
        }

        /// <summary>
        ///     Called before an object is saved
        /// </summary>
        /// <param name="entity"></param>
        /// <param name="id"></param>
        /// <param name="propertyNames"></param>
        /// <param name="state"></param>
        /// <param name="types"></param>
        /// <remarks>
        ///     The interceptor may modify the <c>state</c>, which will be used for the SQL <c>INSERT</c>
        ///     and propagated to the persistent object
        /// </remarks>
        /// <returns><see langword="true" /> if the user modified the <c>state</c> in any way</returns>
        public override bool OnSave(object entity, object id, object[] state, string[] propertyNames, IType[] types)
        {
            //Debug.WriteLine(String.Format("OnSave-> Entity:{0}, params- {1}", entity.GetType().Name, DumpStringArray(propertyNames)));            
            //DumpStream("OnSave1");
            var retval = base.OnSave(entity, id, state, propertyNames, types);
            //DumpStream("OnSave2");
            return retval;
        }

        /// <summary>
        ///     Called when an object is detected to be dirty, during a flush.
        /// </summary>
        /// <param name="entity">The entity.</param>
        /// <param name="id">The id.</param>
        /// <param name="currentState">State of the current.</param>
        /// <param name="previousState">State of the previous.</param>
        /// <param name="propertyNames">The property names.</param>
        /// <param name="types">The types.</param>
        /// <remarks>
        ///     The interceptor may modify the detected currentState, which will be propagated to both the database and the
        ///     persistent object. Note that all flushes end in an actual synchronization with the database, in which as the new
        ///     currentState will be propagated to the object, but not necessarily (immediately) to the database. It is strongly
        ///     recommended that the interceptor not modify the previousState.
        /// </remarks>
        /// <returns>True if the user modified the currentState in any way</returns>
        public override bool OnFlushDirty(object entity, object id, object[] currentState, object[] previousState,
            string[] propertyNames, IType[] types)
        {
            //Debug.WriteLine(String.Format("OnFlushDirty-> entity name- {0}, current state- {1}, prev state- {2}, params- {3}", entity.GetType().Name, DumpStringArray(currentState), DumpStringArray(previousState), DumpStringArray(propertyNames)));            
            //DumpStream("OnFlushDirty1");
            var retval = base.OnFlushDirty(entity, id, currentState, previousState, propertyNames, types);
            //DumpStream("OnFlushDirty2");
            return retval;
        }

        /// <summary>
        ///     Called before an object is deleted
        /// </summary>
        /// <param name="entity">The entity.</param>
        /// <param name="id">The id.</param>
        /// <param name="state">The state.</param>
        /// <param name="propertyNames">The property names.</param>
        /// <param name="types">The types.</param>
        /// <remarks>
        ///     It is not recommended that the interceptor modify the <c>state</c>.
        /// </remarks>
        public override void OnDelete(object entity, object id, object[] state, string[] propertyNames, IType[] types)
        {
            //Debug.WriteLine(String.Format("OnDelete-> entity name:{0}, id:{1},state:{2}, params- {3}", entity.GetType().Name, id.ToString(), DumpStringArray(state), DumpStringArray(propertyNames)));            
            //DumpStream("OnDelete1");
            base.OnDelete(entity, id, state, propertyNames, types);
            //DumpStream("OnDelete2");
        }

        /// <summary>
        ///     Called when sql string is being prepared.
        /// </summary>
        /// <param name="sql">sql to be prepared </param>
        /// <returns> original or modified sql </returns>
        public override SqlString OnPrepareStatement(SqlString sql)
        {
            //Debug.WriteLine("OnPrepareStatement->" + sql.ToString());
            //DumpStream("OnPrepareStatement1");            
            var sstring = base.OnPrepareStatement(sql);
            //DumpStream("OnPrepareStatement2");
            return sstring;
        }

        /// <summary>
        ///     Called just before an object is initialized
        /// </summary>
        /// <param name="entity">The entity.</param>
        /// <param name="id">The id.</param>
        /// <param name="state">The state.</param>
        /// <param name="propertyNames">The property names.</param>
        /// <param name="types">The types.</param>
        /// <remarks>
        ///     The interceptor may change the <c>state</c>, which will be propagated to the persistent
        ///     object. Note that when this method is called, <c>entity</c> will be an empty
        ///     uninitialized instance of the class.
        /// </remarks>
        /// <returns><see langword="true" /> if the user modified the <c>state</c> in any way</returns>
        public override bool OnLoad(object entity, object id, object[] state, string[] propertyNames, IType[] types)
        {
            //Debug.WriteLine(String.Format("OnLoad-> entity name: {0}, id- {1}, params- {2}", entity.GetType().Name, id.ToString(), DumpStringArray(propertyNames)));
            //DumpStream("OnLoad1");
            var retval = base.OnLoad(entity, id, state, propertyNames, types);
            //DumpStream("OnLoad2");
            return retval;
        }

        /// <summary>
        ///     Called after a flush that actually ends in execution of the SQL statements required to
        ///     synchronize in-memory state with the database.
        /// </summary>
        /// <param name="entities">The entities</param>
        public override void PostFlush(ICollection entities)
        {
            // ReadOutStream("PostFlush1");
            base.PostFlush(entities);
            //DumpStream("PostFlush2");
        }

        /// <summary>
        ///     Called after a transaction is committed or rolled back.
        /// </summary>
        /// <param name="tx">The tx.</param>
        public override void AfterTransactionCompletion(ITransaction tx)
        {
            //  ReadOutStream("AfterTransactionCompletion1");
            base.AfterTransactionCompletion(tx);
            //DumpStream("AfterTransactionCompletion2");
        }

        /// <summary>
        ///     Reads the stdout stream.
        /// </summary>
        /// <param name="method">The method name of the caller. for debug purposes only.</param>
        private void ReadOutStream(string method)
        {
            var streamdata = "";
            //****** Start Testing StringBuilder data by assigning it to blank and null************************
            //_sb = null;
            //_sb.Remove(0,_sb.Length);
            //*************Testing data enda*******************************************************************
            //Before assigning the value to string from stringbuilder, ensure that Stringbuilder has data in it.
            if (_sb != null)
                if (!string.IsNullOrEmpty(_sb.ToString()))
                {
                    streamdata = _sb.ToString();

                    if (streamdata.Trim().Length > 0)
                    {
                        if (streamdata.StartsWith("NHibernate:"))
                            LastRecordedSqlStatement = streamdata.Replace("NHibernate:", Environment.NewLine);
                        //LastRecordedSqlStatement = streamdata.Substring(11, streamdata.Length - 11);

                        //Debug.WriteLine(method + ":stdout->" + streamdata);
                        //Debug.WriteLine(method + ":" + LastRecordedSqlStatement);
                        _sb.Remove(0, _sb.Length);
                        _writer.Flush();
                    }
                }
        }

        // Dispose(bool disposing) executes in two distinct scenarios.
        // If disposing equals true, the method has been called directly
        // or indirectly by a user's code. Managed and unmanaged resources
        // can be disposed.
        // If disposing equals false, the method has been called by the
        // runtime from inside the finalizer and you should not reference
        // other objects. Only unmanaged resources can be disposed.
        private void Dispose(bool disposing)
        {
            // Check to see if Dispose has already been called.
            if (!_disposed)
            {
                // If disposing equals true, dispose all managed
                // and unmanaged resources.
                if (disposing)
                    // Dispose managed resources.
                    if (_writer != null)
                        _writer.Close();

                // Call the appropriate methods to clean up
                // unmanaged resources here.
                // If disposing is false,
                // only the following code is executed.
                //CloseHandle(handle);
                //handle = IntPtr.Zero;

                // Note disposing has been done.
                _disposed = true;
            }
        }
    }
}