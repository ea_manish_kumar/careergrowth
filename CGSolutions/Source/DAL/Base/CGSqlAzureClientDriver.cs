﻿using System;
using System.Collections.Generic;
using CommonModules.Configuration;
using CommonModules.Constants;
using Microsoft.Practices.EnterpriseLibrary.WindowsAzure.TransientFaultHandling;
using Microsoft.Practices.EnterpriseLibrary.WindowsAzure.TransientFaultHandling.SqlAzure;
using Microsoft.Practices.TransientFaultHandling;
using NHibernate.SqlAzure;

namespace DataAccess.Base
{
    public class CGSqlAzureClientDriver : SqlAzureClientDriver
    {
        protected override ReliableSqlConnection CreateReliableConnection()
        {
            var connectionRetry =
                RetryPolicyFactory.GetRetryPolicy<CGSqlAzureTransientErrorDetectionStrategy>(
                    RetryStrategies.Incremental);
            var commandRetry =
                RetryPolicyFactory.GetRetryPolicy<CGSqlAzureTransientErrorDetectionStrategy>(RetryStrategies
                    .ExponentialBackoff);

            connectionRetry.Retrying += Retry_Event_Handler;
            commandRetry.Retrying += Retry_Event_Handler;

            var connection = new ReliableSqlConnection(null, connectionRetry, commandRetry);
            return connection;
        }

        private void Retry_Event_Handler(object sender, RetryingEventArgs args)
        {
            if (ConfigManager.GetConfig("SqlTrensientFaultLogging").ToUpper().Equals("TRUE"))
            {
                IList<KeyValuePair<string, object>> attributes = new List<KeyValuePair<string, object>>();
                attributes.Add(new KeyValuePair<string, object>("Count", args.CurrentRetryCount.ToString()));
                attributes.Add(new KeyValuePair<string, object>("Delay", args.Delay.ToString()));
                attributes.Add(new KeyValuePair<string, object>("Exception", args.LastException.ToString()));
                attributes.Add(new KeyValuePair<string, object>("TimeStamp", DateTime.Now.ToString()));
            }
        }
    }
}