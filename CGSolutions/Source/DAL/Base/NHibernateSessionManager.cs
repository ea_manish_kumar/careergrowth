using System;
using System.Reflection;
using System.Web;
using CommonModules.Caching;
using CommonModules.Exceptions;
//using CommonModules.Logging;
using NHibernate;
using NHibernate.Cfg;

namespace DataAccess.Base
{
    /// <summary>
    ///     TODO: Comments missing
    /// </summary>
    public interface INHibernateSessionManager : IDisposable
    {
        // Properties
        /// <summary>
        ///     Gets the session.
        /// </summary>
        /// <value>The session.</value>
        INHibernateSession Session { get; }

        // Methods
        /// <summary>
        ///     Creates the session.
        /// </summary>
        /// <returns></returns>
        INHibernateSession CreateSession();

        /// <summary>
        ///     Creates the I session.
        /// </summary>
        /// <returns></returns>
        ISession CreateISession();

        /// <summary>
        ///     Creates the stateless I session.
        /// </summary>
        /// <returns></returns>
        IStatelessSession CreateIStatelessSession();
    }

    /// <summary>
    ///     A Singleton that creates and persits a single SessionFactory for the to program to access globally.
    ///     This uses the .Net CallContext to store a session for each thread.
    ///     This is heavely based on 'NHibernate Best Practices with ASP.NET' By Billy McCafferty.
    ///     http://www.codeproject.com/KB/architecture/NHibernateBestPractices.aspx
    /// </summary>
    public class NHibernateSessionManager : INHibernateSessionManager
    {
        #region Static Content

        /// <summary>
        ///     Used for singletion object locking
        /// </summary>
        private static readonly object syncRoot = new object();

        /// <summary>
        ///     Stores the value of the "show_sql" NH property
        /// </summary>
        private bool _showsql;

        /// <summary>
        ///     NH interceptor instance
        /// </summary>
        private static Interceptor _interceptor = default(Interceptor);

        // The variable is declared to be volatile to ensure that assignment to the instance variable completes before the instance 
        // variable can be accessed
        private static volatile INHibernateSessionManager _nHibernateSessionManager;


        /// <summary>
        ///     Set method is exposed so that the NHibernateSessionManager can be swapped out for Unit Testing.
        ///     NOTE: Cannot set Instance after it has been initialized, and calling Get will automatically intialize the Instance.
        /// </summary>
        /// <value>The instance.</value>
        public static INHibernateSessionManager Instance
        {
            get
            {
                lock (syncRoot)
                {
                    if (_nHibernateSessionManager == null)
                        _nHibernateSessionManager = new NHibernateSessionManager();
                }

                return _nHibernateSessionManager;
            }
            set
            {
                if (_nHibernateSessionManager != null)
                    throw new Exception("Cannot set Instance after it has been initialized.");
                _nHibernateSessionManager = value;
            }
        }

        public static string LastRecordedSqlStatement
        {
            get
            {
                if (_interceptor != null)
                    return _interceptor.LastRecordedSqlStatement;
                return null;
            }
        }

        #endregion

        #region Declarations

        /// <summary>
        ///     The session factory isntance
        /// </summary>
        private readonly ISessionFactory _sessionFactory;

        /// <summary>
        ///     NHibernate configuration
        /// </summary>
        private readonly Configuration _NHConfig;

        #endregion

        #region Constructors & Finalizers

        /// <summary>
        ///     This will load the NHibernate settings from the App.config.
        ///     Note: This can/should be expanded to support multiple databases.
        /// </summary>
        private NHibernateSessionManager()
        {
            //Read NH config
            _NHConfig = new Configuration().Configure();

            //Read "show_sql" flag
            ReadShowSqlFlag();

            //Create session factory
            _sessionFactory = _NHConfig.BuildSessionFactory();
        }

        /// <summary>
        ///     Releases unmanaged resources and performs other cleanup operations before the
        ///     <see cref="NHibernateSessionManager" /> is reclaimed by garbage collection.
        /// </summary>
        ~NHibernateSessionManager()
        {
            Dispose(true);
        }

        #endregion

        #region IDisposable

        private bool _isDisposed;

        /// <summary>
        ///     Performs application-defined tasks associated with freeing, releasing, or resetting unmanaged resources.
        /// </summary>
        public void Dispose()
        {
            Dispose(false);
        }

        private void Dispose(bool finalizing)
        {
            if (!_isDisposed && _sessionFactory != null)
            {
                // Close SessionFactory
                _sessionFactory.Close();
                _sessionFactory.Dispose();

                // Flag as disposed.
                _isDisposed = true;
                if (!finalizing)
                    GC.SuppressFinalize(this);
            }
        }

        #endregion

        #region Methods

        /// <summary>
        ///     Creates the session.
        /// </summary>
        /// <returns></returns>
        public INHibernateSession CreateSession()
        {
            return new NHibernateSession(CreateISession());
        }

        /// <summary>
        ///     reads the show_sql flag from NH config.
        /// </summary>
        private void ReadShowSqlFlag()
        {
            //Retrieve "show_sql" value
            var val = _NHConfig.GetProperty("show_sql");

            if (val != null && val.Trim().Length > 0)
                _showsql = bool.Parse(val);
        }

        /// <summary>
        ///     Clears this instance's NH session.
        /// </summary>
        public static void ClearNHSession()
        {
            var method = (MethodInfo) MethodBase.GetCurrentMethod();

            // Vikas Jindal (8/16/2013): NH Session cleanup stuff
            try
            {
                //Rollback
                try
                {
                    if (Instance.Session.HasOpenTransaction)
                        Instance.Session.RollbackTransaction();
                }
                catch (Exception ex1)
                {
                    var msg = "LastRecordedSqlStatement" + LastRecordedSqlStatement;
                    msg += GlobalExceptionHandler.DumpException(ex1, method);
                    //Log4NetManager.LogException(method.DeclaringType.Name, msg);
                }

                //flush
                try
                {
                    if (Instance.Session.IsOpen)
                        Instance.Session.GetISession().Flush();
                }
                catch (Exception ex2)
                {
                    var msg = "LastRecordedSqlStatement" + LastRecordedSqlStatement;
                    msg += GlobalExceptionHandler.DumpException(ex2, method);
                    //Log4NetManager.LogException(method.DeclaringType.Name, msg);
                }

                //Clear
                try
                {
                    Instance.Session.GetISession().Clear();
                }
                catch (Exception ex3)
                {
                    var msg = "LastRecordedSqlStatement" + LastRecordedSqlStatement;
                    msg += GlobalExceptionHandler.DumpException(ex3, method);
                    //Log4NetManager.LogException(method.DeclaringType.Name, msg);
                }

                //dispose
                Instance.Session.Dispose();
                ContextCacher.RemoveCurrentNHSession();
                //NHibernateSessionManager.Instance.Session.GetISession().Close();
                //NHibernateSessionManager.Instance.Session.GetISession().Dispose();
                //NHibernateSessionManager.Instance.Dispose();
            }
            catch (Exception ex)
            {
                var msg = "LastRecordedSqlStatement" + LastRecordedSqlStatement;
                msg += GlobalExceptionHandler.DumpException(ex, method);
                //Log4NetManager.LogException(method.DeclaringType.Name, msg);
            }
        }

        /// <summary>
        ///     Creates the ISession.
        /// </summary>
        /// <returns></returns>
        public ISession CreateISession()
        {
            ISession iSession;
            lock (_sessionFactory)
            {
                if (_showsql)
                {
                    _interceptor = new Interceptor();
                    iSession = _sessionFactory.OpenSession(_interceptor);
                }
                else
                {
                    iSession = _sessionFactory.OpenSession();
                }
            }

            //System.Reflection.MethodInfo Method = (System.Reflection.MethodInfo)System.Reflection.MethodBase.GetCurrentMethod();
            //CommonModules.Logging.Log4NetManager.LogInformation(Method, "New NH Session Created");
            return iSession;
        }

        public IStatelessSession CreateIStatelessSession()
        {
            IStatelessSession iStatelessSession;
            lock (_sessionFactory)
            {
                iStatelessSession = _sessionFactory.OpenStatelessSession();
            }

            //System.Reflection.MethodInfo Method = (System.Reflection.MethodInfo)System.Reflection.MethodBase.GetCurrentMethod();
            //CommonModules.Logging.Log4NetManager.LogInformation(Method, "New NH Session Created");
            return iStatelessSession;
        }

        #endregion

        #region Properties

        /// <summary>
        ///     Gets the session.
        /// </summary>
        /// <value>The session.</value>
        public INHibernateSession Session
        {
            get
            {
                var session = ContextSession;

                // If the thread does not yet have a session, create one.
                if (session == null)
                {
                    session = CreateSession();

                    // Save to CallContext.
                    ContextSession = session;
                }

                return session;
            }
        }

        /// <summary>
        ///     Gets or sets the context session.
        /// </summary>
        /// <value>The context session.</value>
        private INHibernateSession ContextSession
        {
            get
            {
                //Check if an NHSession is cached in the Context
                if (ContextCacher.CurrentNHSession != null)
                    return (INHibernateSession) ContextCacher.CurrentNHSession;
                return null;
            }
            //Cache the NHSession into the Context
            set => ContextCacher.CurrentNHSession = value;
        }

        /// <summary>
        ///     Gets a value indicating whether this instance is web context.
        /// </summary>
        /// <value>
        ///     <c>true</c> if this instance is web context; otherwise, <c>false</c>.
        /// </value>
        private bool IsWebContext => HttpContext.Current != null;

        #endregion
    }
}