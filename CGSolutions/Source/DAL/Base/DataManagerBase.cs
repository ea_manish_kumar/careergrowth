using System;
using System.Collections.Generic;
using NHibernate;
using NHibernate.Criterion;

namespace DataAccess.Base
{
    /// <summary>
    ///     TODO: Comments missing
    /// </summary>
    /// <typeparam name="T">The entity type</typeparam>
    /// <typeparam name="TKey">the primary key type</typeparam>
    public abstract class DataManagerBase<T, TKey> where T : class
    {
        #region Declarations

        /// <summary>
        ///     private variable to hold the NHibernate Session
        /// </summary>
        protected INHibernateSession session;

        /// <summary>
        ///     The Default for the Maximum number of results that can be returned by an NHibernate query
        ///     This has been set to avoid NHibernate having to work with unusually massive recordsets
        ///     which is not efficient. This value can be overridden in individual Criteria and Named queries
        /// </summary>
        protected const int defaultMaxResults = 1000;

        #endregion Declarations

        #region Constructors

        /// <summary>
        ///     Initializes a new instance of the <see cref="DataManagerBase&lt;T, TKey&gt;" /> class.
        /// </summary>
        public DataManagerBase()
        {
            session = NHibernateSessionManager.Instance.Session;
        }

        /// <summary>
        ///     Initializes a new instance of the <see cref="DataManagerBase&lt;T, TKey&gt;" /> class.
        /// </summary>
        /// <param name="session">The session.</param>
        public DataManagerBase(INHibernateSession session)
        {
            this.session = session;
        }

        #endregion Constructors

        #region Get Methods

        /// <summary>
        ///     Gets the by id.
        /// </summary>
        /// <param name="id">The id.</param>
        /// <returns></returns>
        public virtual T GetById(TKey id)
        {
            return (T) Session.GetISession().Get(typeof(T), id);
        }

        /// <summary>
        ///     Gets all.
        /// </summary>
        /// <returns></returns>
        public virtual IList<T> GetAll()
        {
            return GetByCriteria(defaultMaxResults);
        }

        /// <summary>
        ///     Gets all.
        /// </summary>
        /// <param name="maxResults">The max results.</param>
        /// <returns></returns>
        public IList<T> GetAll(int maxResults)
        {
            return GetByCriteria(maxResults);
        }

        /// <summary>
        ///     Gets the by criteria.
        /// </summary>
        /// <param name="criterionList">The criterion list.</param>
        /// <returns></returns>
        protected IList<T> GetByCriteria(params ICriterion[] criterionList)
        {
            return GetByCriteria(defaultMaxResults, criterionList);
        }

        /// <summary>
        ///     Gets the by criteria.
        /// </summary>
        /// <param name="maxResults">The max results.</param>
        /// <param name="criterionList">The criterion list.</param>
        /// <returns></returns>
        protected IList<T> GetByCriteria(int maxResults, params ICriterion[] criterionList)
        {
            var criteria = GetNewCriteria().SetMaxResults(maxResults);

            foreach (var criterion in criterionList)
                criteria.Add(criterion);

            return criteria.List<T>();
        }

        /// <summary>
        ///     Gets the unique by criteria.
        /// </summary>
        /// <param name="criterionList">The criterion list.</param>
        /// <returns></returns>
        protected T GetUniqueByCriteria(params ICriterion[] criterionList)
        {
            var criteria = GetNewCriteria();

            foreach (var criterion in criterionList)
                criteria.Add(criterion);

            return criteria.UniqueResult<T>();
        }

        /// <summary>
        ///     Gets the by example.
        /// </summary>
        /// <param name="exampleObject">The example object.</param>
        /// <param name="excludePropertyList">The exclude property list.</param>
        /// <returns></returns>
        protected IList<T> GetByExample(T exampleObject, params string[] excludePropertyList)
        {
            var criteria = GetNewCriteria();
            var example = Example.Create(exampleObject);

            foreach (var excludeProperty in excludePropertyList)
                example.ExcludeProperty(excludeProperty);

            criteria.Add(example);

            return criteria.List<T>();
        }

        /// <summary>
        ///     Gets the by query.
        /// </summary>
        /// <param name="query">The query.</param>
        /// <returns></returns>
        protected IList<T> GetByQuery(string query)
        {
            return GetByQuery(defaultMaxResults, query);
        }

        /// <summary>
        ///     Gets the by query.
        /// </summary>
        /// <param name="maxResults">The max results.</param>
        /// <param name="query">The query.</param>
        /// <returns></returns>
        protected IList<T> GetByQuery(int maxResults, string query)
        {
            var iQuery = Session.GetISession().CreateQuery(query).SetMaxResults(maxResults);
            return iQuery.List<T>();
        }

        /// <summary>
        ///     Gets the unique by query.
        /// </summary>
        /// <param name="query">The query.</param>
        /// <returns></returns>
        protected T GetUniqueByQuery(string query)
        {
            var iQuery = Session.GetISession().CreateQuery(query);
            return iQuery.UniqueResult<T>();
        }

        #endregion Get Methods

        #region Transaction

        public void BeginTx()
        {
            session.BeginTransaction();
        }

        public void CommitTx()
        {
            session.CommitChanges();
        }

        public void RollbackTx()
        {
            session.RollbackTransaction();
        }

        #endregion Transaction

        #region CRUD Methods

        /// <summary>
        ///     Save the Entity to the Nhibernate Session. Note that this will not persist the changes to the database
        ///     To persist to the DB, an explicit Session.CommitChanges() will need to be called or use the public void Save(T
        ///     entity, bool CommitChanges) overload
        /// </summary>
        /// <param name="entity">The entity.</param>
        public void Save(T entity)
        {
            Session.GetISession().Save(entity);
        }

        /// <summary>
        ///     Save the entity to the NHibernate Session AND persist data to the database immediately
        /// </summary>
        /// <param name="entity">The entity.</param>
        /// <param name="CommitChanges">Set to True to persist data to DB immediately</param>
        public void Save(T entity, bool CommitChanges)
        {
            Session.GetISession().Save(entity);
            //If specified, commit the changes immediately to the DB
            if (CommitChanges)
                Session.CommitChanges();
        }

        /// <summary>
        ///     Save list of entities in one go.
        /// </summary>
        /// <param name="entity"></param>
        public void SaveAll(IList<T> entity)
        {
            var session = Session.GetISession();
            Session.BeginTransaction();
            foreach (var item in entity) session.Save(item);
            Session.CommitTransaction();
            Session.CommitChanges();
        }

        /// <summary>
        ///     Save the Entity to the Nhibernate Session. Note that this will not persist the changes to the database
        ///     To persist to the DB, an explicit Session.CommitChanges() will need to be called or use the public void
        ///     SaveOrUpdate(T entity, bool CommitChanges) overload
        /// </summary>
        /// <param name="entity">The entity.</param>
        public void SaveOrUpdate(T entity)
        {
            Session.GetISession().SaveOrUpdate(entity);
        }

        /// <summary>
        ///     Save the entity to the NHibernate Session AND persist data to the database immediately
        /// </summary>
        /// <param name="entity">The entity.</param>
        /// <param name="CommitChanges">Set to True to persist data to DB immediately</param>
        public void SaveOrUpdate(T entity, bool CommitChanges)
        {
            Session.GetISession().SaveOrUpdate(entity);
            //If specified, commit the changes immediately to the DB
            if (CommitChanges)
                Session.CommitChanges();
        }

        public void Merge(string Name, T entity)
        {
            Session.GetISession().Merge(Name, entity);
            //If specified, commit the changes immediately to the DB

            Session.CommitChanges();
        }

        /// <summary>
        /// Save the Entity to the Nhibernate Session. This method forces NH to load the entity from the DB
        /// and applies the changes to the newly loaded entity, as per the entity passed in
        /// Note that this will not persist the changes to the database
        /// To persist to the DB, an explicit Session.CommitChanges() will need to be called or use the public void SaveOrUpdateCopy(T entity, bool CommitChanges) overload
        /// </summary>
        /// <param name="entity">The entity.</param>
        //public void SaveOrUpdateCopy(T entity)
        //{
        //    //Session.GetISession().SaveOrUpdateCopy(entity);
        //    //Session.CommitChanges();
        //}

        /// <summary>
        ///     Clars NH session
        /// </summary>
        public void Clear()
        {
            Session.GetISession().Clear();
        }

        /// <summary>
        ///     Save the Entity to the Nhibernate Session. Note that this will not persist the changes to the database
        ///     To persist to the DB, an explicit Session.CommitChanges() will need to be called or use the public void Delete(T
        ///     entity, bool CommitChanges) overload
        /// </summary>
        /// <param name="entity">The entity.</param>
        public void Delete(T entity)
        {
            Session.GetISession().Delete(entity);
        }

        /// <summary>
        ///     Save the entity to the NHibernate Session AND persist data to the database immediately
        /// </summary>
        /// <param name="entity">The entity.</param>
        /// <param name="CommitChanges">Set to True to persist data to DB immediately</param>
        public void Delete(T entity, bool CommitChanges)
        {
            Session.GetISession().Delete(entity);
            //If specified, commit the changes immediately to the DB
            if (CommitChanges)
                Session.CommitChanges();
        }

        /// <summary>
        ///     Save the Entity to the Nhibernate Session. Note that this will not persist the changes to the database
        ///     To persist to the DB, an explicit Session.CommitChanges() will need to be called or use the public void Update(T
        ///     entity, bool CommitChanges) overload
        /// </summary>
        /// <param name="entity">The entity.</param>
        public void Update(T entity)
        {
            Session.GetISession().Update(entity);
        }

        /// <summary>
        ///     Save the entity to the NHibernate Session AND persist data to the database immediately
        /// </summary>
        /// <param name="entity">The entity.</param>
        /// <param name="CommitChanges">Set to True to persist data to DB immediately</param>
        public void Update(T entity, bool CommitChanges)
        {
            Session.GetISession().Update(entity);
            //If specified, commit the changes immediately to the DB
            if (CommitChanges)
                Session.CommitChanges();
        }

        /// <summary>
        ///     Refresh the Entity from the Database
        /// </summary>
        /// <param name="entity">The entity.</param>
        public void Refresh(T entity)
        {
            Session.GetISession().Refresh(entity);
        }

        /// <summary>
        ///     Evict the Entity from the Session
        /// </summary>
        /// <param name="entity">The entity.</param>
        public void Evict(T entity)
        {
            Session.GetISession().Evict(entity);
        }

        /// <summary>
        ///     Checks if a given entity is available in NH session.
        /// </summary>
        /// <param name="entity">The entity.</param>
        /// <returns>
        ///     true if the entity is available in NH session, else false.
        /// </returns>
        public bool Contains(T entity)
        {
            return Session.GetISession().Contains(entity);
        }

        /// <summary>
        ///     Reattach a definitely unmodified detached object to a session (detached =&gt; persistent)
        /// </summary>
        /// <param name="entity">The entity.</param>
        public void Lock(T entity)
        {
            Lock(entity, LockMode.None);
        }

        /// <summary>
        ///     reattach a definitely unmodified detached object to a session (detached =&gt; persistent)
        /// </summary>
        /// <param name="entity">The entity.</param>
        /// <param name="lockMode">Lock mode</param>
        public void Lock(T entity, LockMode lockMode)
        {
            Session.GetISession().Lock(entity, lockMode);
        }


        /// <summary>
        ///     Save or Update list of entities in one go.
        /// </summary>
        /// <param name="entity"></param>
        public void SaveOrUpdateAll(List<T> entity)
        {
            var session = Session.GetISession();
            Session.BeginTransaction();
            foreach (var item in entity) session.SaveOrUpdate(item);
            Session.CommitTransaction();
            Session.CommitChanges();
        }

        #endregion CRUD Methods

        #region Helper Methods

        /// <summary>
        ///     returns a new Criteria object instance that is typed to the calling Type
        /// </summary>
        /// <returns></returns>
        protected ICriteria GetNewCriteria()
        {
            return Session.GetISession().CreateCriteria(typeof(T));
        }

        /// <summary>
        ///     returns a new Criteria object instance that is typed to the calling Type
        /// </summary>
        /// <returns></returns>
        protected IMultiCriteria GetNewMultiCriteria()
        {
            return Session.GetISession().CreateMultiCriteria();
        }

        /// <summary>
        ///     Returns an IQuery instance for the specified query string
        /// </summary>
        /// <param name="QueryString"></param>
        /// <returns></returns>
        protected IQuery CreateSQLQuery(string QueryString)
        {
            return Session.GetISession().CreateSQLQuery(QueryString);
        }

        /// <summary>
        ///     Returns an IQuery instance for the specified named query
        /// </summary>
        /// <param name="QueryName">Name of the query.</param>
        /// <returns></returns>
        protected IQuery GetNamedQuery(string QueryName)
        {
            return Session.GetISession().GetNamedQuery(QueryName);
        }

        /// <summary>
        ///     Returns an IQuery instance for the specified named query
        /// </summary>
        /// <param name="QueryName">Name of the query.</param>
        /// <returns></returns>
        protected IQuery GetNamedQueryByStatelessSession(string QueryName)
        {
            return Session.GetIStatelessSession().GetNamedQuery(QueryName);
        }

        /// <summary>
        ///     Returns an ISQLQuery instance for the specified Query string
        /// </summary>
        /// <param name="Query">The query.</param>
        /// <returns></returns>
        protected ISQLQuery GetSQLQuery(string Query)
        {
            return session.GetISession().CreateSQLQuery(Query);
        }

        /// <summary>
        ///     Returns an ISQLQuery instance for the specified Query string
        /// </summary>
        /// <param name="Query"></param>
        /// <returns></returns>
        protected IQuery GetQuery(string Query)
        {
            return session.GetISession().CreateQuery(Query);
        }

        #endregion Helper Methods

        #region Properties

        /// <summary>
        ///     The NHibernate Session object is exposed only to the Manager class and should not be directly manipulated by the
        ///     Service Layer
        ///     except to begin and end transactions. You may edit the NHibernateSession class to provide more functions as
        ///     required
        /// </summary>
        /// <value>The session.</value>
        public INHibernateSession Session => session;

        /// <summary>
        ///     Gets the type.
        /// </summary>
        /// <value>The type.</value>
        public Type Type => typeof(T);

        public string LastRecordedSqlStatement => NHibernateSessionManager.LastRecordedSqlStatement;

        #endregion Properties
    }

    ///// <summary>
    ///// Concrete class for the DataManagerBase.
    ///// This can be used to initialize and manipulate the NH session
    ///// </summary>
    //public class DataManager : DataManagerBase<int, int>
    //{
    //    /// <summary>
    //    /// Initializes a new instance of the <see cref="DataManager"/> class.
    //    /// </summary>
    //    public DataManager()
    //        : base()
    //    { }
    //}
}