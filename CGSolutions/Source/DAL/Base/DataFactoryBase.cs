﻿namespace DataAccess.Base
{
    /// <summary>
    ///     base class for all DataManager classes
    /// </summary>
    public class DataFactoryBase
    {
        ///// <summary>
        ///// private variable to hold the NHibernate Session
        ///// </summary>
        //protected static INHibernateSession session;

        /// <summary>
        ///     The NHibernate Session object is exposed only to the Data Layer and should not be directly manipulated by the
        ///     Service Layer
        ///     except to begin and end transactions. You may edit the NHibernateSession class to provide more functions as
        ///     required
        /// </summary>
        /// <value>The session.</value>
        public static INHibernateSession Session => NHibernateSessionManager.Instance.Session;
    }
}