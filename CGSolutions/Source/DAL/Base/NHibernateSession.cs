using System;
using NHibernate;

namespace DataAccess.Base
{
    /// <summary>
    ///     TODO: Comments missing
    /// </summary>
    public interface INHibernateSession : IDisposable
    {
        // Properties
        /// <summary>
        ///     Gets a value indicating whether this instance has open transaction.
        /// </summary>
        /// <value>
        ///     <c>true</c> if this instance has open transaction; otherwise, <c>false</c>.
        /// </value>
        bool HasOpenTransaction { get; }

        /// <summary>
        ///     Gets a value indicating whether this instance is open.
        /// </summary>
        /// <value><c>true</c> if this instance is open; otherwise, <c>false</c>.</value>
        bool IsOpen { get; }

        // Methods
        /// <summary>
        ///     Commits the changes.
        /// </summary>
        void CommitChanges();

        /// <summary>
        ///     Closes this instance.
        /// </summary>
        void Close();

        /// <summary>
        ///     Begins the transaction.
        /// </summary>
        /// <returns></returns>
        bool BeginTransaction();

        /// <summary>
        ///     Commits the transaction.
        /// </summary>
        /// <returns></returns>
        bool CommitTransaction();

        /// <summary>
        ///     Rollbacks the transaction.
        /// </summary>
        /// <returns></returns>
        bool RollbackTransaction();

        /// <summary>
        ///     Gets the I session.
        /// </summary>
        /// <returns></returns>
        ISession GetISession();

        /// <summary>
        ///     Gets the state less I session
        /// </summary>
        /// <returns></returns>
        IStatelessSession GetIStatelessSession();

        /// <summary>
        ///     Clears all the entities from the Session.
        /// </summary>
        void Clear();

        /// <summary>
        ///     Flushes all the entities from the Session.
        /// </summary>
        void Flush();

        /// <summary>
        ///     Begins the transaction.
        /// </summary>
        /// <returns></returns>
        ITransaction BeginTran();

        /// <summary>
        ///     Commits the transaction.
        /// </summary>
        /// <returns></returns>
        void CommitTran();
    }

    /// <summary>
    ///     This is a wrapper around the NHibernate Session object, which is the heart of the NHibernate framework.
    ///     The Actual NHibernate Session object is exposed only to the Manager class and should not be directly manipulated by
    ///     the Service Layer
    ///     except to begin and end transactions. You may edit this class to provide more NHibernate functionality as required
    /// </summary>
    public class NHibernateSession : INHibernateSession
    {
        #region Declarations

        /// <summary>
        ///     TODO: Comments missing
        /// </summary>
        protected ITransaction transaction;

        /// <summary>
        ///     TODO: Comments missing
        /// </summary>
        protected ISession iSession;

        /// <summary>
        ///     This is stateless session created to execute the named queries.
        /// </summary>
        protected IStatelessSession iStatelessSession;

        #endregion

        #region Constructor & Destructor

        /// <summary>
        ///     Initializes a new instance of the <see cref="NHibernateSession" /> class.
        /// </summary>
        /// <param name="session">The session.</param>
        public NHibernateSession(ISession session)
        {
            iSession = session;
            //Console.WriteLine("New NH Session created on thread number " + System.Threading.Thread.CurrentThread.Name);
        }

        /// <summary>
        ///     Releases unmanaged resources and performs other cleanup operations before the
        ///     <see cref="NHibernateSession" /> is reclaimed by garbage collection.
        /// </summary>
        ~NHibernateSession()
        {
            Dispose(true);
        }

        #endregion

        #region IDisposable

        private bool _isDisposed;

        /// <summary>
        ///     Performs application-defined tasks associated with freeing, releasing, or resetting unmanaged resources.
        /// </summary>
        public void Dispose()
        {
            Dispose(false);
        }

        /// <summary>
        ///     Releases unmanaged and - optionally - managed resources
        /// </summary>
        /// <param name="finalizing">
        ///     <c>true</c> to release both managed and unmanaged resources; <c>false</c> to release only
        ///     unmanaged resources.
        /// </param>
        private void Dispose(bool finalizing)
        {
            //Console.WriteLine("NH Session disposed on thread name " + System.Threading.Thread.CurrentThread.Name);

            if (!_isDisposed)
            {
                // Close Session
                Close();

                // Flag as disposed.
                _isDisposed = true;
                if (!finalizing)
                    GC.SuppressFinalize(this);
            }
        }

        #endregion

        #region Methods

        /// <summary>
        ///     Commit the changes to the Database. If there is an open transaction, it will also be committed
        /// </summary>
        public void CommitChanges()
        {
            if (HasOpenTransaction)
                CommitTransaction();
            else
                iSession.Flush();
        }

        /// <summary>
        ///     Closes this instance.
        /// </summary>
        public void Close()
        {
            if (iSession.IsOpen) iSession.Close();
        }

        /// <summary>
        ///     Clears all the entities from the Session.
        /// </summary>
        public void Clear()
        {
            iSession.Clear();
        }

        /// <summary>
        ///     Flushes all the entities from the Session.
        /// </summary>
        public void Flush()
        {
            iSession.Flush();
        }

        /// <summary>
        ///     Begins the transaction.
        /// </summary>
        /// <returns></returns>
        public bool BeginTransaction()
        {
            var result = !HasOpenTransaction;
            if (result)
                transaction = iSession.BeginTransaction();
            return result;
        }

        /// <summary>
        ///     Begins the transaction.
        /// </summary>
        /// <returns></returns>
        public ITransaction BeginTran()
        {
            return iSession.BeginTransaction();
            ;
        }

        /// <summary>
        ///     Commits the transaction.
        /// </summary>
        /// <returns></returns>
        public void CommitTran()
        {
            transaction.Commit();
        }


        /// <summary>
        ///     Commits the transaction.
        /// </summary>
        /// <returns></returns>
        public bool CommitTransaction()
        {
            var result = HasOpenTransaction;
            if (result)
                try
                {
                    transaction.Commit();
                    transaction = null;
                }
                catch (HibernateException)
                {
                    transaction.Rollback();
                    transaction = null;
                    throw;
                }

            return result;
        }

        /// <summary>
        ///     Rollbacks the transaction.
        /// </summary>
        /// <returns></returns>
        public bool RollbackTransaction()
        {
            var result = HasOpenTransaction;
            if (result)
            {
                transaction.Rollback();
                transaction.Dispose();
                transaction = null;

                // I dont know why, but it seems that after you rollback a transaction you need to reset the session.
                // Personally, I dislike this; I find it inefficent, and it means that I have to expose a method to
                // get an ISession from the NHibernateSessionManager...does anyone know how to get around this problem?
                iSession.Close();
                iSession.Dispose();
                iSession = NHibernateSessionManager.Instance.CreateISession();
            }

            return result;
        }

        /// <summary>
        ///     Gets the ISession instance.
        /// </summary>
        /// <returns></returns>
        public ISession GetISession()
        {
            return iSession;
        }


        public IStatelessSession GetIStatelessSession()
        {
            return iStatelessSession ??
                   (iStatelessSession = NHibernateSessionManager.Instance.CreateIStatelessSession());
        }

        #endregion

        #region Properties

        /// <summary>
        ///     Gets a value indicating whether this instance has open transaction.
        /// </summary>
        /// <value>
        ///     <c>true</c> if this instance has open transaction; otherwise, <c>false</c>.
        /// </value>
        public bool HasOpenTransaction =>
            transaction != null && !transaction.WasCommitted && !transaction.WasRolledBack;

        /// <summary>
        ///     Gets a value indicating whether this instance is open.
        /// </summary>
        /// <value><c>true</c> if this instance is open; otherwise, <c>false</c>.</value>
        public bool IsOpen => iSession.IsOpen;

        #endregion
    }
}