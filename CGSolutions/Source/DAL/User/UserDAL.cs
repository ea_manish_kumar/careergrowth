﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Reflection;
using CommonModules;
using CommonModules.Configuration;
using CommonModules.Exceptions;
using DataAccess.Base;
using DomainModel.User;
using NHibernate;
using NHibernate.Criterion;

namespace DataAccess.User
{
    public class UserDAL : DataManagerBase<UserMaster, int>
    {
        public bool IsAlreadyRegisteredEmail(string email, int portalID)
        {
            var IsAlreadyRegisteredEmail = false;
            try
            {
                var cr = GetNewCriteria();
                cr.Add(Restrictions.Eq("UserName", email));
                cr.Add(Restrictions.Eq("PortalID", portalID));

                var lstUser = cr.List<UserMaster>();

                if (lstUser != null && lstUser.Count > 0) IsAlreadyRegisteredEmail = true;
            }
            catch (Exception ex)
            {
                var htParams = new Hashtable(3);
                htParams.Add("UserName", email);
                htParams.Add("PortalID", portalID);
                htParams.Add("LastRecordedSqlStatement", LastRecordedSqlStatement);
                var objMethod = (MethodInfo) MethodBase.GetCurrentMethod();
                GlobalExceptionHandler.customExceptionHandler(ex, objMethod, htParams, ErrorSeverityType.Normal);
            }

            return IsAlreadyRegisteredEmail;
        }

        public UserMaster GetUserByEmail(string email, int portalId)
        {
            UserMaster user = null;
            try
            {
                var cr = GetNewCriteria();
                cr.Add(Restrictions.Eq("UserName", email));
                cr.Add(Restrictions.Eq("PortalID", portalId));
                user = cr.SetMaxResults(1).UniqueResult<UserMaster>();
            }
            catch (Exception ex)
            {
                var htParams = new Hashtable(1);
                htParams.Add("UserName", email);
                htParams.Add("LastRecordedSqlStatement", LastRecordedSqlStatement);
                var objMethod = (MethodInfo) MethodBase.GetCurrentMethod();
                GlobalExceptionHandler.customExceptionHandler(ex, objMethod, htParams, ErrorSeverityType.Normal);
            }

            return user;
        }

        public List<UserMaster> GetUsersBySearchCriteria(int userid, string email, string firstname, string lastname)
        {
            List<UserMaster> lstUser = null;

            try
            {
                var cr = GetNewCriteria();

                var orCondition = Restrictions.Disjunction();

                if (userid > 0) orCondition.Add(Restrictions.Eq("Id", userid));

                if (!string.IsNullOrEmpty(email))
                    orCondition.Add(Restrictions.Like("UserName", email, MatchMode.Anywhere));

                if (!string.IsNullOrEmpty(firstname))
                    orCondition.Add(Restrictions.Like("FirstName", firstname, MatchMode.Anywhere));

                if (!string.IsNullOrEmpty(lastname))
                    orCondition.Add(Restrictions.Like("LastName", lastname, MatchMode.Anywhere));

                cr.Add(orCondition);
                cr.AddOrder(Order.Asc("FirstName"));

                var users = cr.List<UserMaster>();

                if (users != null)
                    if (users.Count > 0)
                        lstUser = users.ToList();
            }
            catch (Exception ex)
            {
                var htParams = new Hashtable(1);
                htParams.Add("UserName", email);
                htParams.Add("LastRecordedSqlStatement", LastRecordedSqlStatement);
                var objMethod = (MethodInfo) MethodBase.GetCurrentMethod();
                GlobalExceptionHandler.customExceptionHandler(ex, objMethod, htParams, ErrorSeverityType.Normal);
            }

            return lstUser;
        }

        public IList<UserMaster> GetUsersByDateCreated(DateTime createDate, DateTime endDate, short userLevelID,
            short userRoleID, int batchSize)
        {
            IList<UserMaster> lstUser = new List<UserMaster>();
            IList<UserMaster> lstUserToEmail = new List<UserMaster>();
            var sConn = ConfigManager.GetConnectionString();
            var sQuery = string.Empty;

            if (createDate == endDate)
                sQuery = "use ResumeGig SELECT Top " + batchSize +
                         " * FROM [User] u left join [UserData] ud on u.UserID=ud.userid inner join useroptins uo on u.userid=uo.userid where u.RoleID=" +
                         userRoleID + " and u.IsActive=1 and u.UserLevelID=" + userLevelID +
                         " and (ud.datatypecd is null or ud.datatypecd!='EMGD') and uo.optinid=1 and uo.response=1 and u.creationdate>='" +
                         createDate + "' ORDER BY u.creationdate DESC";
            else
                sQuery = "use ResumeGig SELECT Top " + batchSize +
                         " * FROM [User] u left join [UserData] ud on u.UserID=ud.userid inner join useroptins uo on u.userid=uo.userid where u.RoleID=" +
                         userRoleID + " and u.IsActive=1 and u.UserLevelID=" + userLevelID +
                         " and (ud.datatypecd is null or ud.datatypecd!='EMGD') and uo.optinid=1 and uo.response=1 and u.creationdate>='" +
                         createDate + "' and u.creationdate<'" + endDate + "' ORDER BY u.creationdate DESC";
            DataSet dsUsers = null;
            try
            {
                var da = new SqlDataAdapter(sQuery, sConn);
                dsUsers = new DataSet();
                da.Fill(dsUsers);

                if (dsUsers != null && dsUsers.Tables.Count > 0)
                    foreach (DataRow myRow in dsUsers.Tables[0].Rows)
                    {
                        var user = new UserMaster();
                        user.ID = int.Parse(myRow["UserID"].ToString());
                        user.FirstName = myRow["FirstName"].ToString();
                        user.UserName = myRow["UserName"].ToString();
                        user.DomainCD = myRow["DomainCD"].ToString();
                        user.CreationDate = DateTime.Parse(myRow["CreationDate"].ToString());
                        lstUser.Add(user);
                    }

                if (lstUser != null && lstUser.Count > 0 && createDate == endDate)
                {
                    foreach (var user in lstUser)
                        if (user.CreationDate.Date == createDate)
                            lstUserToEmail.Add(user);
                    return lstUserToEmail;
                }

                return lstUser;
            }
            catch (Exception ex)
            {
                var htParams = new Hashtable(1);
                htParams.Add("LastRecordedSqlStatement", LastRecordedSqlStatement);
                var objMethod = (MethodInfo) MethodBase.GetCurrentMethod();
                GlobalExceptionHandler.customExceptionHandler(ex, objMethod, htParams, ErrorSeverityType.Normal);
                return null;
            }
        }

        public IList<UserSearchDetailECOM> GetUserDetailBySearchCriteria(int userid, string email, string firstname,
            string lastname, string domainCodes, bool isSandBoxUser, int cancellationNumber, int count,
            bool ShowDeActiveUsers = false)
        {
            IQuery Q = null;
            IList<UserSearchDetailECOM> usersearchdetail = null;
            try
            {
                Q = GetNamedQuery("User.SearchUserECOM");
                if (userid > 0)
                    Q.SetParameter("UserID", userid);
                else
                    Q.SetParameter("UserID", 0, NHibernateUtil.Int32);

                if (!string.IsNullOrEmpty(email))
                    Q.SetParameter("UserName", email);
                else
                    Q.SetParameter("UserName", "", NHibernateUtil.String);

                if (!string.IsNullOrEmpty(firstname))
                    Q.SetParameter("FirstName", firstname);
                else
                    Q.SetParameter("FirstName", "", NHibernateUtil.String);

                if (!string.IsNullOrEmpty(lastname))
                    Q.SetParameter("LastName", lastname);
                else
                    Q.SetParameter("LastName", "", NHibernateUtil.String);

                if (cancellationNumber > 0)
                    Q.SetParameter("CancellationNumber", cancellationNumber);
                else
                    Q.SetParameter("CancellationNumber", 0, NHibernateUtil.Int32);

                if (!string.IsNullOrEmpty(domainCodes))
                    Q.SetParameter("DomainCodes", domainCodes);
                else
                    Q.SetParameter("DomainCodes", "", NHibernateUtil.String);

                if (!string.IsNullOrEmpty("IsSandBoxUser"))
                    Q.SetParameter("IsSandBoxUser", isSandBoxUser);
                else
                    Q.SetParameter("IsSandBoxUser", 0, NHibernateUtil.Binary);

                if (count > 0)
                    Q.SetParameter("Count", count);
                else
                    Q.SetParameter("Count", 1000, NHibernateUtil.Int32);

                Q.SetParameter("ShowDeActiveUsers", ShowDeActiveUsers);

                usersearchdetail = Q.List<UserSearchDetailECOM>();
            }
            catch (Exception ex)
            {
                var objMethod = (MethodInfo) MethodBase.GetCurrentMethod();
                var htParams = new Hashtable(6);
                htParams.Add("UserID", userid.ToString());
                htParams.Add("UserName", email);
                htParams.Add("FirstName", firstname);
                htParams.Add("LastName", lastname);
                htParams.Add("isSandBoxUser", isSandBoxUser);
                htParams.Add("cancellationNumber", cancellationNumber.ToString());
                htParams.Add("count", count.ToString());
                GlobalExceptionHandler.customExceptionHandler(ex, objMethod, htParams, ErrorSeverityType.Normal);
                return null;
            }

            return usersearchdetail;
        }

        public IList<UserSearchDetailDTO> GetUserDetailBySearchCriteria(int userid, string email, string firstname,
            string lastname, int orderid, string getewayref)
        {
            IQuery Q = null;
            IList<UserSearchDetailDTO> usersearchdetaildto = null;
            try
            {
                Q = GetNamedQuery("User.SearchUser");
                if (userid > 0)
                    Q.SetParameter("UserID", userid);
                else
                    Q.SetParameter("UserID", null, NHibernateUtil.Int32);

                if (!string.IsNullOrEmpty(email))
                    Q.SetParameter("UserName", email);
                else
                    Q.SetParameter("UserName", null, NHibernateUtil.String);

                if (!string.IsNullOrEmpty(firstname))
                    Q.SetParameter("FirstName", firstname);
                else
                    Q.SetParameter("FirstName", null, NHibernateUtil.String);

                if (!string.IsNullOrEmpty(lastname))
                    Q.SetParameter("LastName", lastname);
                else
                    Q.SetParameter("LastName", null, NHibernateUtil.String);

                if (orderid > 0)
                    Q.SetParameter("OrderID", orderid);
                else
                    Q.SetParameter("OrderID", null, NHibernateUtil.Int32);

                if (!string.IsNullOrEmpty(getewayref))
                    Q.SetParameter("GateWayRef", getewayref);
                else
                    Q.SetParameter("GateWayRef", null, NHibernateUtil.String);

                usersearchdetaildto = Q.List<UserSearchDetailDTO>();

                if (usersearchdetaildto != null)
                    usersearchdetaildto = usersearchdetaildto.Where(u => u != null).ToList();
            }
            catch (Exception ex)
            {
                var objMethod = (MethodInfo) MethodBase.GetCurrentMethod();
                var htParams = new Hashtable(1);
                htParams.Add("UserID", userid.ToString());
                htParams.Add("UserName", email);
                htParams.Add("FirstName", firstname);
                htParams.Add("LastName", lastname);
                htParams.Add("OrderID", orderid.ToString());
                htParams.Add("GateWayRef", getewayref);
                GlobalExceptionHandler.customExceptionHandler(ex, objMethod, htParams, ErrorSeverityType.Normal);
                return null;
            }

            return usersearchdetaildto;
        }

        /// <summary>
        ///     Gets the person by partyid.
        /// </summary>
        /// ///
        /// <param name="partyid">The partyid.</param>
        /// <returns></returns>
        public IList<UserMaster> GetUsersByUserIdList(List<int> userIds)
        {
            var cr = GetNewCriteria();
            cr.Add(Restrictions.In("Id", userIds));
            var lstperson = cr.List<UserMaster>().ToList();

            return lstperson;
        }

        /// <summary>
        ///     Gets the user session duration by user ID.
        /// </summary>
        /// <param name="partyID">The user ID.</param>
        /// <returns></returns>
        public IList<UserSessionDurationDetail> GetUserSessionDurationByPartyID(int userID)
        {
            IQuery Q = null;

            try
            {
                Q = GetNamedQuery("User.GetUserSessionDurationDetail");
                Q.SetInt32("UserID", userID);
                return Q.List<UserSessionDurationDetail>();
            }
            catch (Exception ex)
            {
                var htParams = new Hashtable(1);
                htParams.Add("UserID", userID);
                htParams.Add("LastRecordedSqlStatement", LastRecordedSqlStatement);
                var objMethod = (MethodInfo) MethodBase.GetCurrentMethod();
                GlobalExceptionHandler.customExceptionHandler(ex, objMethod, htParams, ErrorSeverityType.Normal);
                return null;
            }
        }

        /// <summary>
        ///     Returns UserID against the user name provided
        /// </summary>
        /// <param name="userName">UserName</param>
        /// <returns></returns>
        public int GetUserIDByEmail(string userName)
        {
            IList<UserMaster> lstUser = null;
            var userID = 0;

            try
            {
                var cr = GetNewCriteria();
                cr.Add(Restrictions.Eq("UserName", userName));

                lstUser = cr.List<UserMaster>();

                if ((lstUser != null) & (lstUser.Count > 0)) userID = lstUser[0].ID;
            }
            catch (Exception ex)
            {
                var htParams = new Hashtable(1);
                htParams.Add("UserName", userName);
                htParams.Add("LastRecordedSqlStatement", LastRecordedSqlStatement);
                var objMethod = (MethodInfo) MethodBase.GetCurrentMethod();
                GlobalExceptionHandler.customExceptionHandler(ex, objMethod, htParams, ErrorSeverityType.Normal);
            }

            return userID;
        }

        /// <summary>
        ///     Returns first name of a user
        /// </summary>
        /// <param name="iUserId"></param>
        /// <returns></returns>
        public string GetUserFirstName(int iUserId)
        {
            IList<UserMaster> lstUser = null;
            var firstName = string.Empty;

            try
            {
                var cr = GetNewCriteria();
                cr.Add(Restrictions.Eq("Id", iUserId));

                lstUser = cr.List<UserMaster>();

                if ((lstUser != null) & (lstUser.Count > 0)) firstName = lstUser[0].FirstName;
            }
            catch (Exception ex)
            {
                var htParams = new Hashtable(1);
                htParams.Add("UserID", iUserId);
                htParams.Add("LastRecordedSqlStatement", LastRecordedSqlStatement);
                var objMethod = (MethodInfo) MethodBase.GetCurrentMethod();
                GlobalExceptionHandler.customExceptionHandler(ex, objMethod, htParams, ErrorSeverityType.Normal);
            }

            return firstName;
        }

        public string GetLatestJobTitle(int userId)
        {
            IQuery Q = null;
            IList<string> jobTitleList = null;
            try
            {
                Q = GetNamedQuery("Document.GetJobTitle");
                if (userId > 0)
                    Q.SetParameter("UserID", userId);
                else
                    Q.SetParameter("UserID", null, NHibernateUtil.Int32);
                jobTitleList = Q.List<string>();
            }
            catch (Exception ex)
            {
                var objMethod = (MethodInfo) MethodBase.GetCurrentMethod();
                var htParams = new Hashtable(1);
                htParams.Add("UserID", userId.ToString());
                GlobalExceptionHandler.customExceptionHandler(ex, objMethod, htParams, ErrorSeverityType.Normal);
                return null;
            }

            return jobTitleList != null && jobTitleList.Count > 0 ? jobTitleList[0] : string.Empty;
        }

        public string GetLatestJobLocation(int userId)
        {
            IQuery Q = null;
            IList<string> jobLocationList = null;
            try
            {
                Q = GetNamedQuery("Document.GetJobLocation");
                if (userId > 0)
                    Q.SetParameter("UserID", userId);
                else
                    Q.SetParameter("UserID", null, NHibernateUtil.Int32);
                jobLocationList = Q.List<string>();
            }
            catch (Exception ex)
            {
                var objMethod = (MethodInfo) MethodBase.GetCurrentMethod();
                var htParams = new Hashtable(1);
                htParams.Add("UserID", userId.ToString());
                GlobalExceptionHandler.customExceptionHandler(ex, objMethod, htParams, ErrorSeverityType.Normal);
                return null;
            }

            return jobLocationList != null && jobLocationList.Count > 0 ? jobLocationList[0] : string.Empty;
        }

        public int GetRoleIDByUserID(int userId)
        {
            var roleID = 0;
            try
            {
                var cr = GetNewCriteria();
                cr.Add(Restrictions.Eq("Id", userId));
                var userMasterData = cr.SetMaxResults(1).UniqueResult<UserMaster>();
                if (userMasterData != null)
                    roleID = userMasterData.RoleID;
            }
            catch (Exception ex)
            {
                var htParams = new Hashtable(1);
                htParams.Add("userId", userId);
                htParams.Add("LastRecordedSqlStatement", LastRecordedSqlStatement);
                var objMethod = (MethodInfo) MethodBase.GetCurrentMethod();
                GlobalExceptionHandler.customExceptionHandler(ex, objMethod, htParams, ErrorSeverityType.Normal);
            }

            return roleID;
        }

        public int GetPortalByUserID(int userID)
        {
            IList<UserMaster> userList = null;
            var portalID = Products.ResumeHelpUS;

            try
            {
                var cr = GetNewCriteria();
                cr.Add(Restrictions.Eq("Id", userID));
                userList = cr.List<UserMaster>();
                if ((userList != null) & (userList.Count > 0))
                    portalID = userList[0].PortalID.HasValue ? userList[0].PortalID.Value : Products.ResumeHelpUS;
            }
            catch (Exception ex)
            {
                var htParams = new Hashtable(2);
                htParams.Add("UserID", userID);
                htParams.Add("LastRecordedSqlStatement", LastRecordedSqlStatement);
                var objMethod = (MethodInfo) MethodBase.GetCurrentMethod();
                GlobalExceptionHandler.customExceptionHandler(ex, objMethod, htParams, ErrorSeverityType.Normal);
            }

            return portalID;
        }


        /// Set User Stage for given UserId and stage code
        /// </summary>
        /// <param name="UserId">UserId</param>
        /// <param name="StageCD">StageCD</param>
        /// <returns>Status(true/false)</returns>
        public bool SetProgress(int userId, string stageCD)
        {
            var Status = false;
            try
            {
                var Q = GetNamedQuery("User.SetProgress");
                Q.SetParameter("UserID", userId);
                Q.SetParameter("Progress", stageCD);
                Q.ExecuteUpdate();
                Status = true;
            }
            catch (Exception Ex)
            {
                var htParams = new Hashtable(2);
                htParams.Add("UserID", userId);
                htParams.Add("NextProgressCD", stageCD);
                var objMethod = (MethodInfo) MethodBase.GetCurrentMethod();
                GlobalExceptionHandler.customExceptionHandler(Ex, objMethod, htParams, ErrorSeverityType.Normal);
            }

            return Status;
        }
    }
}