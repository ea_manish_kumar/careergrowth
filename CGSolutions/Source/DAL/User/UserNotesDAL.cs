﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Reflection;
using CommonModules.Exceptions;
using DataAccess.Base;
using DomainModel.User;

namespace DataAccess.User
{
    public class UserNotesDAL : DataManagerBase<UserNotesDTO, string>
    {
        #region Get Methods

        /// <summary>
        ///     GetUserAdminNotesByUserID
        /// </summary>
        /// <param name="UserID"></param>
        /// <returns></returns>
        public IList<UserNotesDetailDTO> GetUserAdminNotesByUserID(int UserID)
        {
            IList<UserNotesDetailDTO> lst = null;
            try
            {
                var Q = GetNamedQuery("User.GetUserAdminNotes");
                Q.SetInt32("UserID", UserID);

                lst = Q.List<UserNotesDetailDTO>();
            }
            catch (Exception ex)
            {
                var objMethod = (MethodInfo) MethodBase.GetCurrentMethod();
                var htParams = new Hashtable(1);
                htParams.Add("UserID", UserID.ToString());
                GlobalExceptionHandler.customExceptionHandler(ex, objMethod, htParams, ErrorSeverityType.Normal);
            }

            return lst;
        }

        #endregion
    }
}