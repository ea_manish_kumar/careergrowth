﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Reflection;
using CommonModules.Exceptions;
using DataAccess.Base;
using DomainModel.User;
using NHibernate.Criterion;

namespace DataAccess.User
{
    public class AddressDAL : DataManagerBase<Address, int>
    {
        public IList<Address> GetUserAllAddress(int UserID)
        {
            IList<Address> lstAddress = null;
            try
            {
                var cr = GetNewCriteria();
                cr.Add(Restrictions.Eq("UserID", UserID));
                lstAddress = cr.List<Address>();
            }
            catch (Exception ex)
            {
                var htParams = new Hashtable(2);
                htParams.Add("UserID", UserID);
                htParams.Add("LastRecordedSqlStatement", LastRecordedSqlStatement);
                var objMethod = (MethodInfo) MethodBase.GetCurrentMethod();
                GlobalExceptionHandler.customExceptionHandler(ex, objMethod, htParams, ErrorSeverityType.Normal);
            }

            return lstAddress;
        }

        public Address GetAddressByUserID(int UserID)
        {
            var address = new Address();
            try
            {
                var cr = GetNewCriteria();
                cr.Add(Restrictions.Eq("UserID", UserID));

                var lst = cr.List<Address>();

                if (lst != null && lst.Count > 0) return lst[0];
            }
            catch (Exception ex)
            {
                var htParams = new Hashtable(2);
                htParams.Add("UserID", UserID);
                htParams.Add("LastRecordedSqlStatement", LastRecordedSqlStatement);
                var objMethod = (MethodInfo) MethodBase.GetCurrentMethod();
                GlobalExceptionHandler.customExceptionHandler(ex, objMethod, htParams, ErrorSeverityType.Normal);
                return address;
            }

            return address;
        }

        public Address GetAddressByUserID(int UserID, string addressTypeCD)
        {
            Address address = null;
            try
            {
                var cr = GetNewCriteria();
                cr.Add(Restrictions.Eq("UserID", UserID));
                cr.Add(Restrictions.Eq("AddressTypeCD", addressTypeCD));
                var lst = cr.List<Address>();

                if (lst != null && lst.Count > 0)
                {
                    address = new Address();
                    address = lst[0];
                }
            }
            catch (Exception ex)
            {
                var htParams = new Hashtable(2);
                htParams.Add("UserID", UserID);
                htParams.Add("AddressTypeCD", addressTypeCD);
                htParams.Add("LastRecordedSqlStatement", LastRecordedSqlStatement);
                var objMethod = (MethodInfo) MethodBase.GetCurrentMethod();
                GlobalExceptionHandler.customExceptionHandler(ex, objMethod, htParams, ErrorSeverityType.Normal);
                return address;
            }

            return address;
        }
    }
}