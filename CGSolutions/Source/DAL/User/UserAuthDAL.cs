﻿using System;
using System.Collections;
using System.Reflection;
using CommonModules.Exceptions;
using DataAccess.Base;
using DomainModel.User;

namespace DataAccess.User
{
    public class UserAuthDAL : DataManagerBase<UserAuth, int>
    {
        public void RefreshUserAuth(int userAuthId, DateTime expiredOn)
        {
            try
            {
                var Q = CreateSQLQuery(
                    "Update [UserAuth] Set ExpiredOn = :@ExpiredOn, ModifiedOn = :@ModifiedOn Where UserAuthID = :@UserAuthID");
                Q.SetParameter("@UserAuthID", userAuthId);
                Q.SetParameter("@ExpiredOn", expiredOn);
                Q.SetParameter("@ModifiedOn", DateTime.Now);
                //Run the query
                Q.ExecuteUpdate();
            }
            catch (Exception ex)
            {
                var htParams = new Hashtable(2);
                htParams.Add("UserAuthID", userAuthId);
                htParams.Add("ExpiredOn", expiredOn);
                var objMethod = (MethodInfo) MethodBase.GetCurrentMethod();
                GlobalExceptionHandler.customExceptionHandler(ex, objMethod, htParams, ErrorSeverityType.Normal);
            }
        }

        public void SignOutUserAuth(int userAuthId, DateTime expiredOn)
        {
            try
            {
                var Q = CreateSQLQuery(
                    "Update [UserAuth] Set ExpiredOn = :@ExpiredOn, IsSignedOut = 1, ModifiedOn = :@ModifiedOn Where UserAuthID = :@UserAuthID");
                Q.SetParameter("@UserAuthID", userAuthId);
                Q.SetParameter("@ExpiredOn", expiredOn);
                Q.SetParameter("@ModifiedOn", DateTime.Now);
                //Run the query
                Q.ExecuteUpdate();
            }
            catch (Exception ex)
            {
                var htParams = new Hashtable(2);
                htParams.Add("userAuthId", userAuthId);
                htParams.Add("ExpiredOn", expiredOn);
                var objMethod = (MethodInfo) MethodBase.GetCurrentMethod();
                GlobalExceptionHandler.customExceptionHandler(ex, objMethod, htParams, ErrorSeverityType.Normal);
            }
        }
    }
}