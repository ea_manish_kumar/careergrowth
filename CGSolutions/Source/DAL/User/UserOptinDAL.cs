﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Reflection;
using CommonModules.Exceptions;
using DataAccess.Base;
using DomainModel.User;
using NHibernate.Criterion;

namespace DataAccess.User
{
    /// <summary>
    ///     TODO: Comments missing
    /// </summary>
    public class UserOptinDAL : DataManagerBase<UserOptin, string>
    {
        #region Constructors

        internal UserOptinDAL()
        {
        }

        /// <summary>
        ///     Initializes a new instance of the <see cref="UserOptinData" /> class.
        /// </summary>
        /// <param name="session">The session.</param>
        public UserOptinDAL(INHibernateSession session)
            : base(session)
        {
        }

        #endregion

        #region Get Methods

        /// <summary>
        ///     Gets the UserOptin record for the specified ID parameters
        /// </summary>
        /// <param name="UserID">The party ID.</param>
        /// <param name="OptinID">The optin ID.</param>
        /// <returns></returns>
        public UserOptin GetByID(int UserID, short OptinID)
        {
            IList<UserOptin> lstUserOptin = null;

            try
            {
                var cr = GetNewCriteria();
                cr.Add(Restrictions.Eq("UserID", UserID));
                cr.Add(Restrictions.Eq("OptinID", OptinID));

                lstUserOptin = cr.List<UserOptin>();

                if (lstUserOptin != null && lstUserOptin.Count > 0) return lstUserOptin[0];
            }
            catch (Exception ex)
            {
                var htParams = new Hashtable(2);
                htParams.Add("UserID", UserID);
                htParams.Add("OptinID", OptinID);
                var objMethod = (MethodInfo) MethodBase.GetCurrentMethod();
                GlobalExceptionHandler.customExceptionHandler(ex, objMethod, htParams, ErrorSeverityType.Normal);
            }

            return null;
        }

        public IList<UserOptin> GetByUserID(int UserID)
        {
            IList<UserOptin> lstUserOptin = null;
            try
            {
                var cr = GetNewCriteria();
                cr.Add(Restrictions.Eq("UserID", UserID));
                lstUserOptin = cr.List<UserOptin>();
            }
            catch (Exception ex)
            {
                var htParams = new Hashtable(1);
                htParams.Add("UserID", UserID);
                var objMethod = (MethodInfo) MethodBase.GetCurrentMethod();
                GlobalExceptionHandler.customExceptionHandler(ex, objMethod, htParams, ErrorSeverityType.Normal);
            }

            return lstUserOptin;
        }

        #endregion
    }
}