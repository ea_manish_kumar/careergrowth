﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Reflection;
using CommonModules.Exceptions;
using DataAccess.Base;
using DomainModel.User;
using NHibernate.Criterion;

namespace DataAccess.User
{
    public class SharedAuthDAL : DataManagerBase<SharedAuth, string>
    {
        #region Get Methods

        public SharedAuth GetAuthUserByUserIdAndAuthProvider(int UserID, string AuthProviderCD)
        {
            IList<SharedAuth> lstSharedAuthUser = null;
            SharedAuth objSharedAuthUser = null;
            try
            {
                var criteria = GetNewCriteria();
                criteria.Add(Restrictions.Eq("UserID", UserID));
                criteria.Add(Restrictions.Eq("AuthProviderCD", AuthProviderCD));
                lstSharedAuthUser = criteria.List<SharedAuth>();
                if (lstSharedAuthUser != null && lstSharedAuthUser.Count > 0) objSharedAuthUser = lstSharedAuthUser[0];
            }
            catch (Exception ex)
            {
                var htErrorArgs = new Hashtable(1);
                htErrorArgs.Add("UserID", UserID.ToString());
                var objMethod = (MethodInfo) MethodBase.GetCurrentMethod();
                GlobalExceptionHandler.customExceptionHandler(ex, objMethod, htErrorArgs, ErrorSeverityType.Critical);
            }

            return objSharedAuthUser;
        }

        #endregion
    }
}