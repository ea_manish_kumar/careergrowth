﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Reflection;
using CommonModules.Exceptions;
using DataAccess.Base;
using DomainModel.User;
using NHibernate.Criterion;

namespace DataAccess.User
{
    public class UserExtendedDAL : DataManagerBase<UserExtended, int>
    {
        #region Get Methods

        /// <summary>
        ///     Gets the UserExtended record for the specified ID parameters
        /// </summary>
        /// <param name="UserID">The party ID.</param>
        /// <returns></returns>
        public UserExtended GetByUserID(int UserID)
        {
            IList<UserExtended> lstUserExtended = null;

            try
            {
                var cr = GetNewCriteria();
                cr.Add(Restrictions.Eq("UserID", UserID));

                lstUserExtended = cr.List<UserExtended>();

                if (lstUserExtended != null && lstUserExtended.Count > 0) return lstUserExtended[0];
            }
            catch (Exception ex)
            {
                var htParams = new Hashtable(1);
                htParams.Add("UserID", UserID);
                var objMethod = (MethodInfo) MethodBase.GetCurrentMethod();
                GlobalExceptionHandler.customExceptionHandler(ex, objMethod, htParams, ErrorSeverityType.Normal);
            }

            return null;
        }

        #endregion
    }
}