﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Reflection;
using CommonModules.DataManager;
using CommonModules.Exceptions;
using DataAccess.Base;
using DomainModel.Base.UtilityClasses;
using DomainModel.User;
using NHibernate.Criterion;

namespace DataAccess.User
{
    public class UserDataDAL : DataManagerBase<UserData, string>
    {
        #region Get Methods

        public UserData GetUserDataByDataTypeCD(int UserID, string DataTypeCD)
        {
            IList<UserData> lstUserData = null;
            UserData objUserData = null;
            try
            {
                var criteria = GetNewCriteria();
                criteria.Add(Restrictions.Eq("UserID", UserID));
                criteria.Add(Restrictions.Eq("DataTypeCD", DataTypeCD));
                lstUserData = criteria.List<UserData>();
                if (lstUserData != null && lstUserData.Count > 0) objUserData = lstUserData[0];
            }
            catch (Exception ex)
            {
                var htErrorArgs = new Hashtable(2);
                htErrorArgs.Add("UserID", UserID.ToString());
                htErrorArgs.Add("DataTypeCD", DataTypeCD);
                var objMethod = (MethodInfo) MethodBase.GetCurrentMethod();
                GlobalExceptionHandler.customExceptionHandler(ex, objMethod, htErrorArgs, ErrorSeverityType.Critical);
            }

            return objUserData;
        }

        #endregion

        #region CRUD Methods

        public bool UpdateByOldData(int userId, string dataTypeCD, string oldData, string newData)
        {
            try
            {
                /// Here is the catch, the Update command of Sql will enforce a lock on the Row, which can be updated only by one Sql Thread at a time. SQL Row Level Locking is Soul of all of this.
                /// So in case of multiple simultaneous threads hitting this method at same time, only one thread will be able to run the Update command successfully.
                /// Thus by this logic of Update command locking over, other threads can be deemed as multiple click generated threads.

                var sqlParams = new SqlParameter[4];

                /// @UserID
                sqlParams[0] = new SqlParameter("@UserID", userId);
                sqlParams[0].SqlDbType = SqlDbType.Int;

                /// @DataTypeCD
                sqlParams[1] = new SqlParameter("@DataTypeCD", dataTypeCD);
                sqlParams[1].SqlDbType = SqlDbType.VarChar;

                /// @OldData
                sqlParams[2] = new SqlParameter("@OldData", oldData);
                sqlParams[2].SqlDbType = SqlDbType.VarChar;

                /// @NewData
                sqlParams[3] = new SqlParameter("@NewData", newData);
                sqlParams[3].SqlDbType = SqlDbType.VarChar;

                var dataManager = new SQLDataManager();
                var isUpdated = dataManager.UpdateDetails(SqlQueries.UpdateUserDataByDataTpeCode, sqlParams);
                return isUpdated;
            }
            catch (Exception ex)
            {
                var htParams = new Hashtable(4);
                htParams.Add("UserID", userId);
                htParams.Add("DataTypeCD", dataTypeCD);
                htParams.Add("OldData", oldData);
                htParams.Add("NewData", newData);
                var objMethod = (MethodInfo) MethodBase.GetCurrentMethod();
                GlobalExceptionHandler.customExceptionHandler(ex, objMethod, htParams, ErrorSeverityType.Normal);
            }

            return false;
        }

        #endregion
    }
}