﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using CommonModules.Exceptions;
using DataAccess.Base;
using DomainModel.User;
using NHibernate.Criterion;

namespace DataAccess.User
{
    public class UserJobAlertsDAL : DataManagerBase<UserJobAlerts, int>
    {
        public List<UserJobAlerts> GetJobAlertSubscriber(int UserID)
        {
            List<UserJobAlerts> lstJobAlerts = null;
            try
            {
                var cr = GetNewCriteria();
                cr.Add(Restrictions.Eq("UserID", UserID));
                lstJobAlerts = cr.List<UserJobAlerts>().ToList();
            }
            catch (Exception ex)
            {
                var htParams = new Hashtable(2);
                htParams.Add("UserID:", UserID);
                var objMethod = (MethodInfo) MethodBase.GetCurrentMethod();
                GlobalExceptionHandler.customExceptionHandler(ex, objMethod, htParams, ErrorSeverityType.Normal);
            }

            return lstJobAlerts;
        }

        public void DeleteJobAlertSubscribers(int userID)
        {
            try
            {
                var query = string.Format("delete from [UserJobAlerts] where [UserID] = {0}", userID);
                var Q = CreateSQLQuery(query);
                Q.ExecuteUpdate();
            }
            catch (Exception ex)
            {
                var htParams = new Hashtable(2);
                htParams.Add("UserID", userID);
                var objMethod = (MethodInfo) MethodBase.GetCurrentMethod();
                GlobalExceptionHandler.customExceptionHandler(ex, objMethod, htParams, ErrorSeverityType.Normal);
            }
        }
    }
}