﻿namespace DataAccess.User
{
    public class UserFactory
    {
        #region Constructors

        #endregion

        #region Get Methods

        /// <summary>
        /// </summary>
        public static UserDAL UserDAL => new UserDAL();

        public static AddressDAL AddressDAL => new AddressDAL();

        public static UserOptinDAL UserOptinDAL => new UserOptinDAL();

        public static ProgressDAL ProgressDAL => new ProgressDAL();

        public static UserNotesDAL UserNotesDAL => new UserNotesDAL();

        public static UserDataDAL UserDataDAL => new UserDataDAL();

        public static UserJobAlertsDAL UserJobAlertsDAL => new UserJobAlertsDAL();

        public static UserExtendedDAL UserExtendedDAL => new UserExtendedDAL();

        public static ProgressHistoryDAL ProgressHistoryDAL => new ProgressHistoryDAL();

        public static SharedAuthDAL SharedAuthDAL => new SharedAuthDAL();

        public static UserAuthDAL UserAuthDAL => new UserAuthDAL();

        public static UserAuthSessionDAL UserAuthSessionDAL => new UserAuthSessionDAL();

        #endregion
    }
}