﻿using DataAccess.Base;
using DomainModel;

namespace DataAccess.Documents
{
    /// <summary>
    /// </summary>
    public class SkinStyleDAL : DataManagerBase<SkinStyle, int>
    {
        #region Constructors

        internal SkinStyleDAL()
        {
        }


        public SkinStyleDAL(INHibernateSession session)
            : base(session)
        {
        }

        #endregion
    }
}