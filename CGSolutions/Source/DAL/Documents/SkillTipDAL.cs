﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Reflection;
using CommonModules.Exceptions;
using DataAccess.Base;
using DomainModel;
using NHibernate.Criterion;

namespace DataAccess.Documents
{
    public class SkillTipDAL : DataManagerBase<SkillTip, int>
    {
        public IList<SkillTip> GetSkillTips(string jobtitle)
        {
            IList<SkillTip> skillTips = null;

            try
            {
                var criteria = GetNewCriteria();
                criteria.Add(Restrictions.Eq("JobTitle", jobtitle));
                criteria.AddOrder(Order.Desc("ImportancePercentage"));

                skillTips = criteria.List<SkillTip>();
            }
            catch (Exception ex)
            {
                var htParams = new Hashtable(1);
                htParams.Add("jobtitle", jobtitle);
                var objMethod = (MethodInfo) MethodBase.GetCurrentMethod();
                GlobalExceptionHandler.customExceptionHandler(ex, objMethod, htParams, ErrorSeverityType.Normal);
            }

            return skillTips;
        }
    }
}