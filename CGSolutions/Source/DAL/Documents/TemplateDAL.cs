﻿using System;
using System.Collections.Generic;
using System.Reflection;
using CommonModules.Exceptions;
using DataAccess.Base;
using DomainModel;
using NHibernate;

namespace DataAccess.Documents
{
    public class TemplateDAL : DataManagerBase<Template, int>
    {
        public IList<Template> GetAllTemplates()
        {
            IList<Template> lstTemplate;

            try
            {
                var criteria = GetNewCriteria();
                criteria.SetFetchMode("TemplateExtended", FetchMode.Join);
                lstTemplate = criteria.List<Template>();

                return lstTemplate;
            }
            catch (Exception ex)
            {
                var objMethod = (MethodInfo) MethodBase.GetCurrentMethod();
                GlobalExceptionHandler.customExceptionHandler(ex, objMethod, null, ErrorSeverityType.Normal);
            }

            return null;
        }
    }
}