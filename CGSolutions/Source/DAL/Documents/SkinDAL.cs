﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Reflection;
using CommonModules.Exceptions;
using DataAccess.Base;
using DomainModel;

namespace DataAccess.Documents
{
    /// <summary>
    /// </summary>
    public class SkinDAL : DataManagerBase<Skin, int>
    {
        /// <summary>
        ///     Get all segments
        /// </summary>
        /// <returns></returns>
        public IList<Skin> GetAllSkins()
        {
            IList<Skin> lstSkins = null;
            try
            {
                var criteria = GetNewCriteria();
                lstSkins = criteria.List<Skin>();
            }
            catch (Exception ex)
            {
                var htParams = new Hashtable(1);
                var objMethod = (MethodInfo) MethodBase.GetCurrentMethod();
                GlobalExceptionHandler.customExceptionHandler(ex, objMethod, htParams, ErrorSeverityType.Normal);
            }

            return lstSkins;
        }

        #region Constructors

        internal SkinDAL()
        {
        }


        public SkinDAL(INHibernateSession session)
            : base(session)
        {
        }

        #endregion
    }
}