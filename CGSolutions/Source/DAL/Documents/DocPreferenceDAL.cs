﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Reflection;
using CommonModules.Exceptions;
using DataAccess.Base;
using DomainModel;
using NHibernate.Criterion;

namespace DataAccess.Documents
{
    /// <summary>
    ///     DAL for DocumentPreference
    /// </summary>
    public class DocPreferenceDAL : DataManagerBase<DocPreference, int>
    {
        /// <summary>
        ///     Saves or updates a provided DocPreference record.
        /// </summary>
        /// <param name="docPref">The doc pref record.</param>
        /// <param name="CommitChanges">if set to <c>true</c> [commit changes].</param>
        public new void SaveOrUpdate(DocPreference docPref, bool CommitChanges)
        {
            try
            {
                base.SaveOrUpdate(docPref, CommitChanges);
            }
            catch (Exception ex)
            {
                var htParams = new Hashtable(3);
                htParams.Add("DocPreference", docPref.ToString());
                htParams.Add("CommitChanges", CommitChanges);
                htParams.Add("LastRecordedSqlStatement", LastRecordedSqlStatement);
                var objMethod = (MethodInfo) MethodBase.GetCurrentMethod();
                GlobalExceptionHandler.customExceptionHandler(ex, objMethod, htParams, ErrorSeverityType.Normal);
            }
        }

        /// <summary>
        ///     Saves or updates a provided DocPreference record.
        /// </summary>
        /// <param name="docPref">The doc pref record.</param>
        public new void SaveOrUpdate(DocPreference docPref)
        {
            try
            {
                base.SaveOrUpdate(docPref);
            }
            catch (Exception ex)
            {
                var htParams = new Hashtable(3);
                htParams.Add("DocPreference", docPref.ToString());
                htParams.Add("LastRecordedSqlStatement", LastRecordedSqlStatement);
                var objMethod = (MethodInfo) MethodBase.GetCurrentMethod();
                GlobalExceptionHandler.customExceptionHandler(ex, objMethod, htParams, ErrorSeverityType.Normal);
            }
        }

        public DocPreference Add(int DocumentID, string DocPreferenceTypeCD, string sValue)
        {
            var docPref = new DocPreference();
            docPref.DocumentID = DocumentID;
            docPref.DocPreferenceTypeCD = DocPreferenceTypeCD;
            docPref.Value = sValue;
            DocumentFactory.DocPreferenceDAL.Save(docPref);
            return docPref;
        }

        #region Constructors

        internal DocPreferenceDAL()
        {
        }

        /// <summary>
        ///     Initializes a new instance of the <see cref="DocPreferenceData" /> class.
        /// </summary>
        /// <param name="session">The session.</param>
        public DocPreferenceDAL(INHibernateSession session)
            : base(session)
        {
        }

        #endregion

        #region Get Methods

        /// <summary>
        ///     Gets the by doc Id.
        /// </summary>
        /// <param name="Doc">The doc.</param>
        /// <returns></returns>
        public IList<DocPreference> GetByDocID(Document Doc)
        {
            try
            {
                var cr = GetNewCriteria();
                cr.Add(Restrictions.Eq("Document", Doc));
                return cr.List<DocPreference>();
            }
            catch (Exception ex)
            {
                var htParams = new Hashtable(1);
                htParams.Add("Doc", Doc);
                var objMethod = (MethodInfo) MethodBase.GetCurrentMethod();
                GlobalExceptionHandler.customExceptionHandler(ex, objMethod, htParams, ErrorSeverityType.Normal);
                return null;
            }
        }

        /// <summary>
        ///     Returns a Unique record corresponding to the DocPreferenceTypeCD passed in
        /// </summary>
        /// <param name="Doc">The doc.</param>
        /// <param name="DocPreferenceTypeCD">The doc preference type CD.</param>
        /// <returns></returns>
        public DocPreference GetByPreferenceType(int DocumentId, string DocPreferenceTypeCD)
        {
            try
            {
                var cr = GetNewCriteria();
                cr.Add(Restrictions.Eq("DocumentID", DocumentId));
                cr.Add(Restrictions.Eq("DocPreferenceTypeCD", DocPreferenceTypeCD));
                var DocPreferenceList = cr.List<DocPreference>();
                if (DocPreferenceList != null && DocPreferenceList.Count > 0) return DocPreferenceList[0];
                return null;
            }
            catch (Exception ex)
            {
                var htParams = new Hashtable(2);
                htParams.Add("DocumentID", DocumentId);
                htParams.Add("DocPreferenceTypeCD", DocPreferenceTypeCD);
                var objMethod = (MethodInfo) MethodBase.GetCurrentMethod();
                GlobalExceptionHandler.customExceptionHandler(ex, objMethod, htParams, ErrorSeverityType.Normal);
                return null;
            }
        }


        public IList<DocPreference> GetPreferences(int DocumentId)
        {
            try
            {
                var cr = GetNewCriteria();
                cr.Add(Restrictions.Eq("DocumentID", DocumentId));
                var DocPreferenceList = cr.List<DocPreference>();
                return DocPreferenceList;
            }
            catch (Exception ex)
            {
                var htParams = new Hashtable(2);
                htParams.Add("DocumentID", DocumentId);
                var objMethod = (MethodInfo) MethodBase.GetCurrentMethod();
                GlobalExceptionHandler.customExceptionHandler(ex, objMethod, htParams, ErrorSeverityType.Normal);
                return null;
            }
        }

        #endregion
    }
}