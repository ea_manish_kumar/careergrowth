﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using CommonModules.Exceptions;
using DataAccess.Base;
using DomainModel;
using NHibernate.Criterion;

namespace DataAccess.Documents
{
    /// <summary>
    /// </summary>
    public class DocStyleDAL : DataManagerBase<DocStyle, int>
    {
        public DocStyle GetDocStyleByDocument(int DocumentID, string StyleCD)
        {
            try
            {
                var criteria = GetNewCriteria();
                criteria.Add(Restrictions.Eq("DocumentID", DocumentID));
                criteria.Add(Restrictions.Eq("StyleCD", StyleCD));
                return criteria.UniqueResult<DocStyle>();
            }
            catch (Exception ex)
            {
                var htParams = new Hashtable(2);
                htParams.Add("DocumentID", DocumentID);
                htParams.Add("StyleCD", StyleCD);
                var objMethod = (MethodInfo) MethodBase.GetCurrentMethod();
                GlobalExceptionHandler.customExceptionHandler(ex, objMethod, htParams, ErrorSeverityType.Normal);
            }

            return null;
        }

        public List<DocStyle> GetDocStylesByDocumentId(int documentID, List<string> styles)
        {
            List<DocStyle> result = null;

            try
            {
                var criteria = GetNewCriteria();
                criteria.Add(Restrictions.Eq("DocumentID", documentID));
                criteria.Add(Restrictions.In("StyleCD", styles));
                var styleData = criteria.List<DocStyle>();

                if (styleData != null) result = styleData.ToList();
            }
            catch (Exception ex)
            {
                var htParams = new Hashtable(2);
                htParams.Add("DocumentID", documentID);
                htParams.Add("Styles", string.Join(",", styles.ToArray()));
                var objMethod = (MethodInfo) MethodBase.GetCurrentMethod();
                GlobalExceptionHandler.customExceptionHandler(ex, objMethod, htParams, ErrorSeverityType.Normal);
            }

            return result;
        }

        // <summary>
        /// Update DocStyle for a document also update modified date of document.
        /// </summary>
        /// <param name="DocumentId">documentId</param>
        /// <param name="StyleCD">styleCD</param>
        /// <param name="StyleValue">styleValue</param>
        /// <returns>integer</returns>
        public int UpdateDocStyle(int documentId, string styleCD, string styleValue)
        {
            try
            {
                var Q = GetNamedQuery("DocStyle.UpdateDocStyle");
                Q.SetParameter("DocumentID", documentId);
                Q.SetParameter("StyleCD", styleCD);
                Q.SetParameter("StyleValue", styleValue);
                return Q.ExecuteUpdate();
            }
            catch (Exception Ex)
            {
                var htParams = new Hashtable(3);
                htParams.Add("DocumentID", documentId);
                htParams.Add("StyleCD", styleCD);
                htParams.Add("StyleValue", styleValue);
                var objMethod = (MethodInfo) MethodBase.GetCurrentMethod();
                GlobalExceptionHandler.customExceptionHandler(Ex, objMethod, htParams, ErrorSeverityType.Normal);
            }

            return -1;
        }

        #region Constructors

        internal DocStyleDAL()
        {
        }


        public DocStyleDAL(INHibernateSession session)
            : base(session)
        {
        }

        #endregion
    }
}