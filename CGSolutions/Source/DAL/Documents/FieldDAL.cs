﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using CommonModules.Exceptions;
using DataAccess.Base;
using DomainModel;

namespace DataAccess.Documents
{
    public class FieldDAL : DataManagerBase<Field, string>
    {
        internal FieldDAL()
        {
        }

        public FieldDAL(INHibernateSession session)
            : base(session)
        {
        }


        #region Get Methods

        /// <summary>
        ///     Get all segments
        /// </summary>
        /// <returns></returns>
        public IList<Field> GetAllFields()
        {
            IList<Field> lstField = null;
            try
            {
                var criteria = GetNewCriteria();
                lstField = criteria.List<Field>();
            }
            catch (Exception ex)
            {
                var htParams = new Hashtable(1);
                var objMethod = (MethodInfo) MethodBase.GetCurrentMethod();
                GlobalExceptionHandler.customExceptionHandler(ex, objMethod, htParams, ErrorSeverityType.Normal);
            }

            return lstField;
        }

        #endregion

        public Field GetByFieldCD(string FieldCD)
        {
            Field lstField = null;
            try
            {
                var criteria = GetNewCriteria();
                lstField = criteria.List<Field>().FirstOrDefault(FieldType => FieldType.FieldCD == FieldCD);
            }
            catch (Exception ex)
            {
                var htParams = new Hashtable(1);
                var objMethod = (MethodInfo) MethodBase.GetCurrentMethod();
                GlobalExceptionHandler.customExceptionHandler(ex, objMethod, htParams, ErrorSeverityType.Normal);
            }

            return lstField;
        }

        //public static Field GetByFieldID(string p)
        //{
        //    throw new NotImplementedException();
        //}
    }
}