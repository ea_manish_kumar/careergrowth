﻿using DataAccess.Base;
using DomainModel;

namespace DataAccess.Documents
{
    /// <summary>
    /// </summary>
    public class SectionTypeDAL : DataManagerBase<SectionType, string>
    {
        #region Constructors

        internal SectionTypeDAL()
        {
        }

        /// <summary>
        ///     Initializes a new instance of the <see cref="SkinData" /> class.
        /// </summary>
        /// <param name="session">The session.</param>
        public SectionTypeDAL(INHibernateSession session)
            : base(session)
        {
        }

        #endregion
    }
}