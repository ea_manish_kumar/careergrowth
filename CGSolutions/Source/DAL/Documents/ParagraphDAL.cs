﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using CommonModules.Exceptions;
using DataAccess.Base;
using DomainModel;
using NHibernate;
using NHibernate.Criterion;

namespace DataAccess.Documents
{
    /// <summary>
    /// </summary>
    public class ParagraphDAL : DataManagerBase<Paragraph, int>
    {
        public Paragraph GetParagraphBySectionAndIndex(Section section, short paragraphindex)
        {
            ICriteria criteria = null;
            List<Paragraph> lstParagraph = null;

            try
            {
                criteria = GetNewCriteria();
                criteria.Add(Restrictions.Eq("Section", section));
                criteria.Add(Restrictions.Eq("SortIndex", paragraphindex));

                lstParagraph = criteria.List<Paragraph>().ToList();
                if (lstParagraph != null && lstParagraph.Count > 0) return lstParagraph[0];
            }
            catch (Exception ex)
            {
                var htParams = new Hashtable(3);
                htParams.Add("Section", section.ToString());
                htParams.Add("ParagraphIndex", paragraphindex);
                if (lstParagraph != null) htParams.Add("ParagraphCount", lstParagraph.Count);
                var objMethod = (MethodInfo) MethodBase.GetCurrentMethod();
                GlobalExceptionHandler.customExceptionHandler(ex, objMethod, htParams, ErrorSeverityType.Normal);
            }

            return null;
        }

        public int GetParagraphCount(Section section)
        {
            try
            {
                var criteria = GetNewCriteria();
                criteria.Add(Restrictions.Eq("Section", section));
                criteria.SetProjection(Projections.Count("Id"));
                return criteria.UniqueResult<int>();
            }
            catch (Exception ex)
            {
                var htParams = new Hashtable(1);
                htParams.Add("Section", section);
                var objMethod = (MethodInfo) MethodBase.GetCurrentMethod();
                GlobalExceptionHandler.customExceptionHandler(ex, objMethod, htParams, ErrorSeverityType.Normal);
            }

            return -1;
        }

        #region Constructors

        internal ParagraphDAL()
        {
        }

        /// <summary>
        ///     Initializes a new instance of the <see cref="SkinData" /> class.
        /// </summary>
        /// <param name="session">The session.</param>
        public ParagraphDAL(INHibernateSession session)
            : base(session)
        {
        }

        #endregion
    }
}