﻿using DataAccess.Base;
using DomainModel;
using NHibernate.Criterion;

namespace DataAccess.Documents
{
    public class DocDataDAL : DataManagerBase<DocData, int>
    {
        /// <summary>
        ///     Returns a single Field instance based on the unique FieldCD
        /// </summary>
        /// <param name="FieldCD">The field CD.</param>
        /// <returns></returns>
        public Field GetByFieldCD(string FieldCD)
        {
            var criteria = GetNewCriteria();

            criteria.Add(Restrictions.Eq("FieldCD", FieldCD));

            return criteria.UniqueResult<Field>();
        }


        //public static Field GetByFieldCD(string p)
        //{
        //    throw new NotImplementedException();
        //}
    }
}