﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using CommonModules.Exceptions;
using DataAccess.Base;
using DomainModel;
using NHibernate.Criterion;
using Utilities.Constants.Documents;

namespace DataAccess.Documents
{
    public class DocFileDAL : DataManagerBase<DocFile, int>
    {
        /// <summary>
        ///     This method will get the existing DocFile by document and file format type
        /// </summary>
        /// <param name="dmDoc"></param>
        /// <param name="sFileFormatType"></param>
        /// <returns></returns>
        public DocFile GetExportedDocFileByDocumentAndFileType(Document document, string fileFormat)
        {
            DocFile docFile = null;
            try
            {
                var cr = GetNewCriteria();

                cr.Add(Restrictions.Eq("Document", document));
                cr.Add(Restrictions.Eq("DocFileTypeCD", DocFileTypeCD.Exported));
                cr.CreateAlias("File", "File");
                cr.Add(Restrictions.Eq("File.FileTypeCD", fileFormat));
                docFile = cr.List<DocFile>().FirstOrDefault();
            }
            catch (Exception ex)
            {
                var htParams = new Hashtable(2);
                htParams.Add("Document", document);
                htParams.Add("DocFileTypeCD", DocFileTypeCD.Exported);
                htParams.Add("FileFormatType", fileFormat);
                htParams.Add("LastRecordedSqlStatement", LastRecordedSqlStatement);
                var objMethod = (MethodInfo) MethodBase.GetCurrentMethod();
                GlobalExceptionHandler.customExceptionHandler(ex, objMethod, htParams, ErrorSeverityType.Normal);
            }

            return docFile;
        }

        public void SaveOrUpdate(DocFile docFile)
        {
            try
            {
                if (docFile != null && DocumentFactory.DocFileDAL.GetById(docFile.Id) != null)
                    Merge("DocFile", docFile);
                else
                    base.SaveOrUpdate(docFile, true);
            }
            catch (Exception ex)
            {
                var objMethod = (MethodInfo) MethodBase.GetCurrentMethod();
                var htParams = new Hashtable(1);
                htParams.Add("file", docFile != null ? docFile.ToString() : "File is NULL");

                GlobalExceptionHandler.customExceptionHandler(ex, objMethod, htParams, ErrorSeverityType.Normal);
            }
        }

        public DocFile GetDocThumbnail(Document dmDoc, string docType = DocFileTypeCD.ThumbNail)
        {
            try
            {
                var cr = GetNewCriteria();
                cr.Add(Restrictions.Eq("Document", dmDoc));
                cr.Add(Restrictions.Eq("DocFileTypeCD", docType));
                cr.AddOrder(Order.Desc("Id"));
                var Lst = cr.List<DocFile>();

                if (Lst != null && Lst.Count > 0)
                    return Lst[0];
                return null;
            }
            catch (Exception ex)
            {
                var htParams = new Hashtable(2);
                htParams.Add("dmDoc", dmDoc);
                htParams.Add("LastRecordedSqlStatement", LastRecordedSqlStatement);
                var objMethod = (MethodInfo) MethodBase.GetCurrentMethod();
                GlobalExceptionHandler.customExceptionHandler(ex, objMethod, htParams, ErrorSeverityType.Normal);
                return null;
            }
        }

        /// <summary>
        ///     Return DocFile for a FileID
        /// </summary>
        /// <param name="fileID"></param>
        /// <returns></returns>
        public DocFile GetDocFileByFileID(File file)
        {
            IList<DocFile> lstDocFile = null;
            try
            {
                var cr = GetNewCriteria();
                cr.Add(Restrictions.Eq("File", file));
                lstDocFile = cr.List<DocFile>();
                if (lstDocFile != null && lstDocFile.Count > 0) return lstDocFile[0];
            }
            catch (Exception ex)
            {
                var htParams = new Hashtable(1);
                htParams.Add("File", file.ToString());
                var objMethod = (MethodInfo) MethodBase.GetCurrentMethod();
                GlobalExceptionHandler.customExceptionHandler(ex, objMethod, htParams, ErrorSeverityType.Normal);
            }

            return null;
        }

        /// <summary>
        ///     This method will get the list of existing DocFile by document and file format type
        /// </summary>
        /// <param name="dmDoc"></param>
        /// <param name="sFileFormatType"></param>
        /// <returns></returns>
        public DocFile GetWaterMarkedDocFile(Document document, string fileFormat)
        {
            DocFile docFile = null;
            try
            {
                var cr = GetNewCriteria();

                cr.Add(Restrictions.Eq("Document", document));
                cr.Add(Restrictions.Eq("DocFileTypeCD", DocFileTypeCD.WaterMarked));
                cr.CreateAlias("File", "File");
                cr.Add(Restrictions.Eq("File.FileTypeCD", fileFormat));

                var lstDocFile = cr.List<DocFile>().ToList();
                if (lstDocFile != null && lstDocFile.Count > 0)
                    docFile = lstDocFile[0];
            }
            catch (Exception ex)
            {
                var htParams = new Hashtable(4);
                htParams.Add("Document", document != null ? document.ToString() : "<NULL>");
                htParams.Add("DocFileTypeCD", DocFileTypeCD.WaterMarked);
                htParams.Add("FileFormatType", fileFormat);
                htParams.Add("LastRecordedSqlStatement", LastRecordedSqlStatement);
                var objMethod = (MethodInfo) MethodBase.GetCurrentMethod();
                GlobalExceptionHandler.customExceptionHandler(ex, objMethod, htParams, ErrorSeverityType.Normal);
            }

            return docFile;
        }
    }
}