﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Reflection;
using CommonModules.Exceptions;
using DataAccess.Base;
using DomainModel;
using NHibernate;
using NHibernate.Criterion;
using Utilities.Constants.Documents;

namespace DataAccess.Documents
{
    /// <summary>
    /// </summary>
    public class DocumentDAL : DataManagerBase<Document, int>
    {
        public IList<Document> GetAllFromBase(int DocNo)
        {
            try
            {
                return base.GetAll(DocNo);
            }
            catch (Exception ex)
            {
                var htParams = new Hashtable(1);
                htParams.Add("DocNo", DocNo);
                var objMethod = (MethodInfo) MethodBase.GetCurrentMethod();
                GlobalExceptionHandler.customExceptionHandler(ex, objMethod, htParams, ErrorSeverityType.Normal);
                return null;
            }
        }

        /// <summary>
        ///     Returns All Resumes available in the system from the NH Second Level Cache
        /// </summary>
        /// <returns>Resumes List</returns>
        public new IList<Document> GetAll(int userid)
        {
            return GetAll(userid, DocumentTypeCD.Resume);
        }

        public bool CheckResumeName(int UserID, string ResumeName)
        {
            var result = false;
            IList<Document> lstdoc = null;

            try
            {
                var criteria = GetNewCriteria();
                criteria.Add(Restrictions.Eq("UserID", UserID));
                criteria.Add(Restrictions.Eq("Name", ResumeName));
                lstdoc = criteria.List<Document>();
                if (lstdoc != null && lstdoc.Count > 0) result = true;
            }
            catch (Exception ex)
            {
                var htParams = new Hashtable(2);
                htParams.Add("UserID", UserID);
                htParams.Add("ResumeName", ResumeName);
                var objMethod = (MethodInfo) MethodBase.GetCurrentMethod();
                GlobalExceptionHandler.customExceptionHandler(ex, objMethod, htParams, ErrorSeverityType.Normal);
            }

            return result;
        }

        /// <summary>
        ///     This method will update date modified of the document
        /// </summary>
        public void UpdateDateModified(int documentId)
        {
            try
            {
                var Q = CreateSQLQuery("Update Document set DateModified = :@DateModified Where DocumentID = :@DocID");
                Q.SetParameter("@DocID", documentId);
                Q.SetParameter("@DateModified", DateTime.Now);
                //Run the query
                Q.ExecuteUpdate();
            }
            catch (Exception ex)
            {
                var htParams = new Hashtable(1);
                htParams.Add("DocumentID", documentId);
                var objMethod = (MethodInfo) MethodBase.GetCurrentMethod();
                GlobalExceptionHandler.customExceptionHandler(ex, objMethod, htParams, ErrorSeverityType.Normal);
            }
        }

        /// <summary>
        ///     Update SkinCD and DateModified of the document
        /// </summary>
        public void UpdateDocumentSkin(int documentId, string skinCD)
        {
            try
            {
                var Q = CreateSQLQuery(
                    "Update Document set SkinCD = :@SkinCD, DateModified = :@DateModified Where DocumentID = :@DocID");
                Q.SetParameter("@DocID", documentId);
                Q.SetParameter("@SkinCD", skinCD);
                Q.SetParameter("@DateModified", DateTime.Now);
                //Run the query
                Q.ExecuteUpdate();
            }
            catch (Exception ex)
            {
                var htParams = new Hashtable(2);
                htParams.Add("DocumentID", documentId);
                htParams.Add("SkinCD", skinCD);
                var objMethod = (MethodInfo) MethodBase.GetCurrentMethod();
                GlobalExceptionHandler.customExceptionHandler(ex, objMethod, htParams, ErrorSeverityType.Normal);
            }
        }

        public static void ClearDocument(int DocID)
        {
            throw new NotImplementedException();
        }

        /// <summary>
        ///     Update Default Sections Bit in Document table
        /// </summary>
        public void UpdateDocumentDefaultSectionsBit(int documentId, short defSecBit)
        {
            try
            {
                var Q = CreateSQLQuery("Update Document set IsDefSec = :@DefSecBit Where DocumentID = :@DocID");
                Q.SetParameter("@DocID", documentId);
                Q.SetParameter("@DefSecBit", defSecBit);
                //Run the query
                Q.ExecuteUpdate();
            }
            catch (Exception ex)
            {
                var htParams = new Hashtable(1);
                htParams.Add("DocumentID", documentId);
                var objMethod = (MethodInfo) MethodBase.GetCurrentMethod();
                GlobalExceptionHandler.customExceptionHandler(ex, objMethod, htParams, ErrorSeverityType.Normal);
            }
        }

        public int CopyDocument(int documentID, int userID, string DocumentName)
        {
            IList<int> lstDocId = null;
            var iDocumentID = -1;

            try
            {
                var Q = CreateSQLQuery("exec [dbo].prc_Document_Copy :@DocumentID, :@UserID, :@DocumentName");
                Q.SetParameter("@DocumentID", documentID);
                Q.SetParameter("@UserID", userID);
                Q.SetParameter("@DocumentName", DocumentName);
                lstDocId = Q.List<int>();

                if ((lstDocId != null) & (lstDocId.Count > 0))
                    iDocumentID = lstDocId[0];
            }
            catch (Exception ex)
            {
                var htParams = new Hashtable(2);
                htParams.Add("DocumentID", documentID);
                htParams.Add("UserID", userID);
                var objMethod = (MethodInfo) MethodBase.GetCurrentMethod();
                GlobalExceptionHandler.customExceptionHandler(ex, objMethod, htParams, ErrorSeverityType.Normal);
            }

            return iDocumentID;
        }

        public int CopyDocument_RD(int documentID, int userID)
        {
            IList<int> lstDocId = null;
            var iDocumentID = -1;

            try
            {
                var Q = CreateSQLQuery("exec [dbo].prc_Document_Copy_RD :@DocumentID, :@UserID");
                Q.SetParameter("@DocumentID", documentID);
                Q.SetParameter("@UserID", userID);
                lstDocId = Q.List<int>();

                if ((lstDocId != null) & (lstDocId.Count > 0))
                    iDocumentID = lstDocId[0];
            }
            catch (Exception ex)
            {
                var htParams = new Hashtable(2);
                htParams.Add("DocumentID", documentID);
                htParams.Add("UserID", userID);
                var objMethod = (MethodInfo) MethodBase.GetCurrentMethod();
                GlobalExceptionHandler.customExceptionHandler(ex, objMethod, htParams, ErrorSeverityType.Normal);
            }

            return iDocumentID;
        }

        public int DeleteDocumentByID(int documentID)
        {
            var iDocumentID = -1;
            try
            {
                var Q = CreateSQLQuery("exec [dbo].prc_DeleteDocumentById :@DocumentID");
                Q.SetParameter("@DocumentID", documentID);
                Q.ExecuteUpdate();
            }
            catch (Exception ex)
            {
                var htParams = new Hashtable(2);
                htParams.Add("DocumentID", documentID);
                var objMethod = (MethodInfo) MethodBase.GetCurrentMethod();
                GlobalExceptionHandler.customExceptionHandler(ex, objMethod, htParams, ErrorSeverityType.Normal);
            }

            return iDocumentID;
        }

        public int GetUserIdByFileId(File file)
        {
            DocFile docFile = null;
            var userID = 0;

            try
            {
                docFile = DocumentFactory.DocFileDAL.GetDocFileByFileID(file);
                if (docFile != null) userID = docFile.Document.UserID;
            }
            catch (Exception ex)
            {
                var htParams = new Hashtable(1);
                htParams.Add("File", file.ToString());
                var objMethod = (MethodInfo) MethodBase.GetCurrentMethod();
                GlobalExceptionHandler.customExceptionHandler(ex, objMethod, htParams, ErrorSeverityType.Normal);
            }

            return userID;
        }

        /// <summary>
        ///     Get first document for a user
        /// </summary>
        /// <returns>Document</returns>
        public Document GetFrstDocByUserId(int userid)
        {
            IList<Document> lstDocument = null;
            Document document = null;

            try
            {
                var criteria = GetNewCriteria();
                criteria.Add(Restrictions.Eq("UserID", userid));
                criteria.AddOrder(Order.Asc("Id"));
                criteria.SetMaxResults(1);
                lstDocument = criteria.List<Document>();

                if (lstDocument != null && lstDocument.Count > 0) document = lstDocument[0];
            }
            catch (Exception ex)
            {
                var htParams = new Hashtable(1);
                htParams.Add("userid", userid);
                var objMethod = (MethodInfo) MethodBase.GetCurrentMethod();
                GlobalExceptionHandler.customExceptionHandler(ex, objMethod, htParams, ErrorSeverityType.Normal);
            }

            return document;
        }

        /// <summary>
        ///     Returns all documents list by DocumentTypeCD like 'RSME', 'LETR'
        /// </summary>
        /// <returns>Documents List</returns>
        public IList<Document> GetAll(int userid, string documentTypeCd)
        {
            try
            {
                var criteria = GetNewCriteria();
                criteria.Add(Restrictions.Eq("UserID", userid));
                if (!string.IsNullOrEmpty(documentTypeCd))
                    criteria.Add(Restrictions.Eq("DocumentTypeCD", documentTypeCd));

                return criteria.List<Document>();
            }
            catch (Exception ex)
            {
                var htParams = new Hashtable(2);
                htParams.Add("UserID", userid);
                htParams.Add("DocumentTypeCD", documentTypeCd);
                var objMethod = (MethodInfo) MethodBase.GetCurrentMethod();
                GlobalExceptionHandler.customExceptionHandler(ex, objMethod, htParams, ErrorSeverityType.Normal);
                return null;
            }
        }


        public Document GetDocumentByType(int documentId, string documentTypeCd)
        {
            Document doc = null;
            try
            {
                var criteria = GetNewCriteria();
                criteria.Add(Restrictions.Eq("Id", documentId));
                criteria.Add(Restrictions.Eq("DocumentTypeCD", documentTypeCd));
                criteria.SetMaxResults(1);

                var lstDocs = criteria.List<Document>();
                if (lstDocs != null && lstDocs.Count > 0)
                    doc = lstDocs[0];
            }
            catch (Exception ex)
            {
                var htParams = new Hashtable(2);
                htParams.Add("DocumentID", documentId);
                htParams.Add("DocumentTypeCD", documentTypeCd);
                var objMethod = (MethodInfo) MethodBase.GetCurrentMethod();
                GlobalExceptionHandler.customExceptionHandler(ex, objMethod, htParams, ErrorSeverityType.Normal);
            }

            return doc;
        }

        public int GetUserDocumentCount(int userId, string documentTypeCd)
        {
            try
            {
                var criteria = GetNewCriteria();
                criteria.Add(Restrictions.Eq("UserID", userId));
                criteria.Add(Restrictions.Eq("DocumentTypeCD", documentTypeCd));
                criteria.SetProjection(Projections.Count(Projections.Id()));

                var countObj = criteria.List<int>();

                if (countObj != null && countObj.Count > 0)
                    return countObj[0];
            }
            catch (Exception ex)
            {
                var htParams = new Hashtable(1);
                htParams.Add("UserID", userId);
                var objMethod = (MethodInfo) MethodBase.GetCurrentMethod();
                GlobalExceptionHandler.customExceptionHandler(ex, objMethod, htParams, ErrorSeverityType.Normal);
            }

            return 0;
        }

        public int GetDocumentLength(int documentID)
        {
            var documentLength = 0;
            try
            {
                var Q = GetNamedQuery("Documents.DocumentLength");
                Q.SetInt32("DocumentID", documentID);
                var response = Q.List<object>();
                if (response != null && response.Count > 0 && response[0] != null)
                    int.TryParse(response[0].ToString(), out documentLength);
            }
            catch (Exception ex)
            {
                var htParams = new Hashtable(1);
                htParams.Add("DocumentID", documentID);
                var objMethod = (MethodInfo) MethodBase.GetCurrentMethod();
                GlobalExceptionHandler.customExceptionHandler(ex, objMethod, htParams, ErrorSeverityType.Normal);
            }

            return documentLength;
        }

        /// <summary>
        ///     Get latest resume of user
        /// </summary>
        /// <returns>Document</returns>
        public Document GetLatestDocumentByUserId(int userId, string documentTypeCD)
        {
            Document doc = null;
            IList<Document> lstDocument = null;
            try
            {
                var criteria = GetNewCriteria();
                criteria.Add(Restrictions.Eq("UserID", userId));
                criteria.Add(Restrictions.Eq("DocumentTypeCD", documentTypeCD));
                criteria.AddOrder(Order.Desc("DateModified"));
                criteria.SetMaxResults(1);
                lstDocument = criteria.List<Document>();
                if (lstDocument != null && lstDocument.Count > 0)
                    doc = lstDocument[0];
            }
            catch (Exception ex)
            {
                var htParams = new Hashtable(2);
                htParams.Add("UserID", userId);
                htParams.Add("DocumentTypeCD", documentTypeCD);
                var objMethod = (MethodInfo) MethodBase.GetCurrentMethod();
                GlobalExceptionHandler.customExceptionHandler(ex, objMethod, htParams, ErrorSeverityType.Normal);
            }

            return doc;
        }

        /// <summary>
        ///     Get latest resume id of user
        /// </summary>
        /// <returns>int</returns>
        public int GetLatestDocumentIdByUserId(int userid, string documentTypeCd)
        {
            var documentId = 0;
            try
            {
                IQuery query =
                    GetSQLQuery(
                        "Select top 1 DocumentID from Document where Userid = :UserID and DocumentTypeCD = :DocumentTypeCd Order by DocumentID desc");
                query.SetParameter("UserID", userid);
                query.SetParameter("DocumentTypeCd", documentTypeCd);
                var result = query.UniqueResult();
                if (result != null) documentId = (int) result;
            }
            catch (Exception ex)
            {
                var htParams = new Hashtable(2);
                htParams.Add("UserID", userid);
                htParams.Add("DocumentTypeCD", documentTypeCd);
                var objMethod = (MethodInfo) MethodBase.GetCurrentMethod();
                GlobalExceptionHandler.customExceptionHandler(ex, objMethod, htParams, ErrorSeverityType.Normal);
            }

            return documentId;
        }

        #region Constructors

        internal DocumentDAL()
        {
        }

        /// <summary>
        ///     Initializes a new instance of the <see cref="SkinData" /> class.
        /// </summary>
        /// <param name="session">The session.</param>
        public DocumentDAL(INHibernateSession session)
            : base(session)
        {
        }

        #endregion
    }
}