﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Reflection;
using CommonModules.Exceptions;
using DataAccess.Base;
using DomainModel;
using NHibernate.Criterion;

namespace DataAccess.Documents
{
    /// <summary>
    /// </summary>
    public class SectionDAL : DataManagerBase<Section, int>
    {
        /// <summary>
        ///     Get section from DocumentId and SectionTypeCD.
        /// </summary>
        /// <param name="docId"></param>
        /// <param name="sectionTypeCD"></param>
        /// <returns></returns>
        public Section GetSectionByDocIDAndTypeCD(Document doc, string sectionTypeCD)
        {
            Section section = null;
            try
            {
                var criteria = GetNewCriteria();
                criteria.Add(Restrictions.Eq("Document", doc));
                criteria.Add(Restrictions.Eq("SectionTypeCD", sectionTypeCD));
                criteria.SetMaxResults(1);

                var lstSection = criteria.List<Section>();
                if (lstSection != null && lstSection.Count > 0)
                    section = lstSection[0];
            }
            catch (Exception ex)
            {
                var htParams = new Hashtable(2);
                htParams.Add("Document", doc);
                htParams.Add("SectionTypeCD", sectionTypeCD);
                var objMethod = (MethodInfo) MethodBase.GetCurrentMethod();
                GlobalExceptionHandler.customExceptionHandler(ex, objMethod, htParams, ErrorSeverityType.Normal);
            }

            return section;
        }


        public IList<Section> GetSectionByDocIDAndTypeCD(Document doc, string[] sectionTypeCDs)
        {
            IList<Section> sections = null;
            try
            {
                var criteria = GetNewCriteria();
                criteria.Add(Restrictions.Eq("Document", doc));
                criteria.Add(Restrictions.In("SectionTypeCD", sectionTypeCDs));
                return criteria.List<Section>();
            }

            catch (Exception ex)
            {
                var htParams = new Hashtable(2);
                htParams.Add("Document", doc);
                htParams.Add("SectionTypeCD", sectionTypeCDs);
                var objMethod = (MethodInfo) MethodBase.GetCurrentMethod();
                GlobalExceptionHandler.customExceptionHandler(ex, objMethod, htParams, ErrorSeverityType.Normal);
            }

            return sections;
        }

        /// <summary>
        /// </summary>
        /// <param name="docid"></param>
        /// <param name="SectionCD"></param>
        /// <returns></returns>
        public IList<Section> GetAllSectionsByCode(int docid, string SectionCD)
        {
            try
            {
                //ICriteria criteria = GetNewCriteria();
                //criteria.Add(Restrictions.Eq("DocumentID", docid));
                //criteria.Add(Restrictions.Eq("SectionCD", SectionCD));
                //return criteria.List<Section>();
                return null;
            }
            catch (Exception ex)
            {
                var objMethod = (MethodInfo) MethodBase.GetCurrentMethod();
                GlobalExceptionHandler.customExceptionHandler(ex, objMethod, null, ErrorSeverityType.Normal);
                return null;
            }
        }


        public short GetMaxSortIndex(Document doc)
        {
            try
            {
                var criteria = GetNewCriteria();
                criteria.Add(Restrictions.Eq("Document", doc));
                criteria.SetProjection(Projections.Max("SortIndex"));
                return criteria.UniqueResult<short>();
            }
            catch (Exception ex)
            {
                var htParams = new Hashtable(1);
                htParams.Add("Document", doc);
                var objMethod = (MethodInfo) MethodBase.GetCurrentMethod();
                GlobalExceptionHandler.customExceptionHandler(ex, objMethod, htParams, ErrorSeverityType.Normal);
            }

            return -1;
        }

        public int GetSectionCount(Document document)
        {
            try
            {
                var criteria = GetNewCriteria();
                criteria.Add(Restrictions.Eq("Document", document));
                criteria.SetProjection(Projections.Count("Id"));
                return criteria.UniqueResult<int>();
            }
            catch (Exception ex)
            {
                var htParams = new Hashtable(1);
                htParams.Add("Document", document);
                var objMethod = (MethodInfo) MethodBase.GetCurrentMethod();
                GlobalExceptionHandler.customExceptionHandler(ex, objMethod, htParams, ErrorSeverityType.Normal);
            }

            return -1;
        }

        #region Constructors

        internal SectionDAL()
        {
        }

        /// <summary>
        ///     Initializes a new instance of the <see cref="SkinData" /> class.
        /// </summary>
        /// <param name="session">The session.</param>
        public SectionDAL(INHibernateSession session)
            : base(session)
        {
        }

        #endregion
    }
}