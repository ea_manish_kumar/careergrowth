﻿using DataAccess.Base;

namespace DataAccess.Documents
{
    public class DocumentFactory : DataFactoryBase
    {
        #region Constructors

        #endregion

        #region Get Methods

        /// <summary>
        /// </summary>
        public static DocumentDAL DocumentDAL => new DocumentDAL();

        /// <summary>
        /// </summary>
        public static SectionDAL SectionDAL => new SectionDAL();

        /// <summary>
        /// </summary>
        public static SectionTypeDAL SectionTypeDAL => new SectionTypeDAL();

        /// <summary>
        /// </summary>
        public static ParagraphDAL ParagraphDAL => new ParagraphDAL();


        public static ExperienceTipDAL ExperienceTipDAL => new ExperienceTipDAL();


        public static SkillTipDAL SkillTipDAL => new SkillTipDAL();


        public static DocStyleDAL DocStyleDAL => new DocStyleDAL();

        public static DocFileDAL DocFileDAL => new DocFileDAL();

        public static FileDAL FileDAL => new FileDAL();

        public static SkinStyleDAL SkinStyleDAL => new SkinStyleDAL();

        public static SkinDAL SkinDAL => new SkinDAL();

        public static FieldDAL FieldDAL => new FieldDAL();

        public static DocDataDAL DocDataDAL => new DocDataDAL();

        public static DocPreferenceDAL DocPreferenceDAL => new DocPreferenceDAL();

        public static TemplateDAL TemplateDAL => new TemplateDAL();

        #endregion
    }
}