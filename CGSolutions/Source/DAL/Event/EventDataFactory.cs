﻿namespace DataAccess.Event
{
    /// <summary>
    ///     This class provides access to the Data Manager objects
    /// </summary>
    public class EventDataFactory
    {
        #region Constructors

        #endregion

        #region Declarations

        #endregion

        #region Get Methods

        /// <summary>
        ///     Returns an instance of the DocumentEventData class.
        ///     Use this to work with Events related to ONLY Documents
        /// </summary>
        /// <value>The document event data.</value>
        public static DocumentEventData DocumentEventData => new DocumentEventData();

        /// <summary>
        ///     Gets the event data data.
        /// </summary>
        /// <value>The event data data.</value>
        public static EventDataData EventDataData => new EventDataData();

        /// <summary>
        ///     Gets the event data data.
        /// </summary>
        /// <value>The event data data.</value>
        public static UserEventData UserEventData => new UserEventData();

        /// <summary>
        ///     Gets the EventCode data
        /// </summary>
        /// <value>The event code data.</value>
        public static EventCodeData EventCodeData => new EventCodeData();

        /// <summary>
        ///     Returns an instance of the HybridDataEventData class.
        ///     Use this to work with Events related to multiple entities like Documents, Communications and Pages
        /// </summary>
        /// <value>The hybrid data event data.</value>
        public static HybridDataEventData HybridDataEventData => new HybridDataEventData();

        #endregion
    }
}