﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Reflection;
using CommonModules.Exceptions;
using DataAccess.Base;
using DomainModel.Event;
using NHibernate.Criterion;

namespace DataAccess.Event
{
    public class DocumentEventData : DataManagerBase<DocumentEvent, Guid>
    {
        /// <summary>
        ///     Saves or updates a provided DocumentEvent record.
        /// </summary>
        /// <param name="docevent">The doc event  record.</param>
        /// <param name="CommitChanges">if set to <c>true</c> [commit changes].</param>
        public new void Save(DocumentEvent docevent, bool CommitChanges)
        {
            try
            {
                base.Save(docevent, CommitChanges);
            }
            catch (Exception ex)
            {
                var htParams = new Hashtable(3);
                htParams.Add("DocumentEvent", docevent.ToString());
                htParams.Add("CommitChanges", CommitChanges);
                htParams.Add("LastRecordedSqlStatement", LastRecordedSqlStatement);
                var objMethod = (MethodInfo) MethodBase.GetCurrentMethod();
                GlobalExceptionHandler.customExceptionHandler(ex, objMethod, htParams, ErrorSeverityType.Normal);
            }
        }

        /// <summary>
        ///     Saves or updates a provided DocumentEvent record.
        /// </summary>
        /// <param name="docevent">The doc event  record.</param>
        public new void Save(DocumentEvent docevent)
        {
            try
            {
                base.Save(docevent);
            }
            catch (Exception ex)
            {
                var htParams = new Hashtable(3);
                htParams.Add("DocumentEvent", docevent.ToString());
                htParams.Add("LastRecordedSqlStatement", LastRecordedSqlStatement);
                var objMethod = (MethodInfo) MethodBase.GetCurrentMethod();
                GlobalExceptionHandler.customExceptionHandler(ex, objMethod, htParams, ErrorSeverityType.Normal);
            }
        }

        #region Constructors

        internal DocumentEventData()
        {
        }

        /// <summary>
        ///     Initializes a new instance of the <see cref="DocumentEventData" /> class.
        /// </summary>
        /// <param name="session">The session.</param>
        public DocumentEventData(INHibernateSession session)
            : base(session)
        {
        }

        #endregion

        #region Get Methods

        /// <summary>
        ///     Gets the latest Document Event for the specified DocumentEventCode
        /// </summary>
        /// <param name="DocID">The doc ID.</param>
        /// <param name="DocumentEventCode">The document event code.</param>
        /// <returns></returns>
        public DocumentEvent GetLatestByDocumentEventCode(int DocID, DocumentEventCodeType DocumentEventCode)
        {
            try
            {
                return GetLatestByDocumentEventCodeID(DocID, (int) DocumentEventCode);
            }
            catch (Exception ex)
            {
                var htParams = new Hashtable(2);
                htParams.Add("DocID", DocID);
                htParams.Add("DocumentEventCode", DocumentEventCode);
                var objMethod = (MethodInfo) MethodBase.GetCurrentMethod();
                GlobalExceptionHandler.customExceptionHandler(ex, objMethod, htParams, ErrorSeverityType.Normal);
                return null;
            }
        }

        /// <summary>
        ///     Gets the latest Document Event for the specified HybridEventCode
        /// </summary>
        /// <param name="DocID">The doc ID.</param>
        /// <param name="HybridEventCode">The hybrid event code.</param>
        /// <returns></returns>
        public DocumentEvent GetLatestByDocumentEventCode(int DocID, HybridEventCodeType HybridEventCode)
        {
            try
            {
                return GetLatestByDocumentEventCodeID(DocID, (int) HybridEventCode);
            }
            catch (Exception ex)
            {
                var htParams = new Hashtable(2);
                htParams.Add("DocID", DocID);
                htParams.Add("HybridEventCode", HybridEventCode);
                var objMethod = (MethodInfo) MethodBase.GetCurrentMethod();
                GlobalExceptionHandler.customExceptionHandler(ex, objMethod, htParams, ErrorSeverityType.Normal);
                return null;
            }
        }

        /// <summary>
        ///     Gets the latest Document Event for the specified EventCode
        /// </summary>
        private DocumentEvent GetLatestByDocumentEventCodeID(int DocID, int EventCodeID)
        {
            try
            {
                var Q = GetNamedQuery("Event.GetDocumentEventsByDocIDEventCodeID");
                Q.SetInt32("DocID", DocID);
                Q.SetInt32("EventCodeID", EventCodeID);
                return Q.UniqueResult<DocumentEvent>();
            }
            catch (Exception ex)
            {
                var htParams = new Hashtable(2);
                htParams.Add("DocID", DocID);
                htParams.Add("EventCodeID", EventCodeID);
                var objMethod = (MethodInfo) MethodBase.GetCurrentMethod();
                GlobalExceptionHandler.customExceptionHandler(ex, objMethod, htParams, ErrorSeverityType.Normal);
                return null;
            }
        }

        public IList<DocumentEvent> GetByDocumentID(int DocID)
        {
            try
            {
                var Criteria = GetNewCriteria();
                var DocCriteria = Criteria.CreateCriteria("Documents");
                DocCriteria.Add(Restrictions.Eq("Id", DocID));

                return Criteria.List<DocumentEvent>();
            }
            catch (Exception ex)
            {
                var htParams = new Hashtable(1);
                htParams.Add("DocID", DocID);
                var objMethod = (MethodInfo) MethodBase.GetCurrentMethod();
                GlobalExceptionHandler.customExceptionHandler(ex, objMethod, htParams, ErrorSeverityType.Normal);
                return null;
            }
        }

        public int GetDocumentViewCount(int DocID)
        {
            try
            {
                var eventCode_DVON = EventDataFactory.EventCodeData.GetByEventCode("DVON");
                var eventCode_DVOR = EventDataFactory.EventCodeData.GetByEventCode("DVON");
                var eventCode_DVOF = EventDataFactory.EventCodeData.GetByEventCode("DVON");
                var Criteria = GetNewCriteria();
                Criteria.SetProjection(Projections.Alias(Projections.RowCount(), "EventDocumentID"));

                var DocCriteria = Criteria.CreateCriteria("Documents");
                DocCriteria.Add(Restrictions.Eq("Id", DocID));
                Criteria.Add(Restrictions.In("EventCode",
                    new[] {eventCode_DVON.Id, eventCode_DVOR.Id, eventCode_DVOF.Id}));
                return Criteria.UniqueResult<int>();
            }
            catch (Exception ex)
            {
                var htParams = new Hashtable(1);
                htParams.Add("DocID", DocID);
                var objMethod = (MethodInfo) MethodBase.GetCurrentMethod();
                GlobalExceptionHandler.customExceptionHandler(ex, objMethod, htParams, ErrorSeverityType.Normal);
                return -1;
            }
        }

        /// <summary>
        ///     Gets the type of the event count by document ID event.
        /// </summary>
        public int GetEventCountByDocumentIDEventType(int DocID, EventCode eEventCodeType)
        {
            try
            {
                var Criteria = GetNewCriteria();
                Criteria.SetProjection(Projections.Alias(Projections.RowCount(), "EventDocumentID"));

                var DocCriteria = Criteria.CreateCriteria("Documents");
                DocCriteria.Add(Restrictions.Eq("Id", DocID));
                Criteria.Add(Restrictions.Eq("EventCode", eEventCodeType.Id));
                return Criteria.UniqueResult<int>();
            }
            catch (Exception ex)
            {
                var htParams = new Hashtable(2);
                htParams.Add("DocID", DocID);
                htParams.Add("eEventCodeType", eEventCodeType);
                var objMethod = (MethodInfo) MethodBase.GetCurrentMethod();
                GlobalExceptionHandler.customExceptionHandler(ex, objMethod, htParams, ErrorSeverityType.Normal);
                return -1;
            }
        }

        #endregion
    }
}