﻿using System;
using DataAccess.Base;
using DomainModel.Event;

namespace DataAccess.Event
{
    public class UserEventData : DataManagerBase<UserEvent, Guid>
    {
        #region Constructors

        internal UserEventData()
        {
        }

        /// <summary>
        ///     Initializes a new instance of the <see cref="SystemEventData" /> class.
        /// </summary>
        /// <param name="session">The session.</param>
        public UserEventData(INHibernateSession session)
            : base(session)
        {
        }

        #endregion

        #region Get Methods

        #endregion
    }
}