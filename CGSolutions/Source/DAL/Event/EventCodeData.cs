﻿using System;
using System.Collections;
using System.Reflection;
using CommonModules.Exceptions;
using DataAccess.Base;
using DomainModel.Event;
using NHibernate.Criterion;

namespace DataAccess.Event
{
    public class EventCodeData : DataManagerBase<EventCode, int>
    {
        #region Get Methods

        /// <summary>
        ///     Gets the by event code.
        /// </summary>
        /// <param name="EventCode">The event code.</param>
        /// <returns></returns>
        public EventCode GetByEventCode(string EventCode)
        {
            EventCode eventcode = null;
            try
            {
                var crCm = GetNewCriteria();
                crCm.Add(Restrictions.Eq("EventCodeType", EventCode));
                eventcode = crCm.UniqueResult<EventCode>();
            }
            catch (Exception ex)
            {
                var htParams = new Hashtable();
                var objMethod = (MethodInfo) MethodBase.GetCurrentMethod();
                GlobalExceptionHandler.customExceptionHandler(ex, objMethod, htParams, ErrorSeverityType.Normal);
            }

            return eventcode;
        }

        #endregion

        #region Constructors

        public EventCodeData()
        {
        }

        /// <summary>
        ///     Initializes a new instance of the <see cref="EventCodeData" /> class.
        /// </summary>
        /// <param name="session">The session.</param>
        public EventCodeData(INHibernateSession session)
            : base(session)
        {
        }

        #endregion
    }
}