﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Reflection;
using CommonModules.Exceptions;
using DataAccess.Base;
using DataAccess.Event.DataTransfer;
using DomainModel.Event;
using NHibernate.Criterion;

namespace DataAccess.Event
{
    /// <summary>
    ///     TODO: Comments missing
    /// </summary>
    public class HybridDataEventData : DataManagerBase<HybridDataEvent, int>
    {
        #region Set Methods

        /// <summary>
        ///     Updates the PartyID of the Document.
        /// </summary>
        /// <param name="FromPartyID"></param>
        /// <param name="ToPartyID"></param>
        /// <returns></returns>
        public int UpdateUserEventDataPartyID(int FromPartyID, int ToPartyID)
        {
            //Create a Query object for the Named Query specified in the Documents.NamedQueries.hbm.xml file
            var Q = GetNamedQuery("Events.UpdateUserEventPartyid");
            //Set the value for parameter FromPartyID
            Q.SetInt32("FromPartyID", FromPartyID);
            //Set the value for parameter DocID
            Q.SetInt32("ToPartyID", ToPartyID);
            //Run the query
            return Q.ExecuteUpdate();
        }

        #endregion

        #region Constructors

        internal HybridDataEventData()
        {
        }

        /// <summary>
        ///     Initializes a new instance of the <see cref="CommunicationEventData" /> class.
        /// </summary>
        /// <param name="session">The session.</param>
        public HybridDataEventData(INHibernateSession session)
            : base(session)
        {
        }

        #endregion

        #region Get Methods

        ///// <summary>
        ///// This function returns the list of HybridDataEvent
        ///// </summary>
        ///// <param name="objEventQuery">The obj event query.</param>
        ///// <param name="TotalRows">The total rows.</param>
        ///// <returns></returns>
        //public IList<HybridDataEvent> GetByPartyIDDocumentID(EventQuery objEventQuery, out int TotalRows)
        //{
        //    try
        //    {
        //        TotalRows = 0;
        //        IMultiCriteria multiCrit = GetNewMultiCriteria();
        //        ICriteria crCm = GetNewCriteria();
        //        ICriteria crCm1 = GetNewCriteria();

        //        crCm1.SetProjection(Projections.Alias(Projections.RowCount(), "EventCode"));
        //        crCm1.Add(Expression.Between("Timestamp", objEventQuery.DateRange.StartDate, objEventQuery.DateRange.EndDate));
        //        crCm1.Add(Expression.Eq("PartyID", objEventQuery.PartyID));

        //        //Check for Visibility of Event
        //        crCm1.CreateAlias("EventCode", "EventCode");
        //        crCm.CreateAlias("EventCode", "EventCode");


        //        if (objEventQuery.Visibility)
        //        {
        //            crCm1.Add(Expression.Eq("EventCode.VisibilityTypeCD", "VSBL"));
        //            crCm.Add(Expression.Eq("EventCode.VisibilityTypeCD", "VSBL"));
        //        }
        //        else
        //        {
        //            // crCm1.Add(Expression.Eq("EventCode.VisibilityTypeCD", "NO"));
        //            // crCm.Add(Expression.Eq("EventCode.VisibilityTypeCD", "NO"));
        //        }

        //        if (objEventQuery.DocumentID > 0)
        //        {
        //            //To DO: Add DocumentID to search criteria
        //            crCm1.CreateAlias("Documents", "Documents");
        //            crCm1.Add(Expression.Eq("Documents.Id", objEventQuery.DocumentID));

        //            crCm.CreateAlias("Documents", "Documents");
        //            crCm.Add(Expression.Eq("Documents.Id", objEventQuery.DocumentID));
        //        }

        //        multiCrit.Add(crCm1);

        //        crCm.Add(Expression.Between("Timestamp", objEventQuery.DateRange.StartDate, objEventQuery.DateRange.EndDate));
        //        crCm.Add(Expression.Eq("PartyID", objEventQuery.PartyID));


        //        //Order by Timestamp Desc
        //        crCm.AddOrder(Order.Desc("Timestamp"));

        //        // for Paging , Check if page size is greater than 0
        //        if (objEventQuery.PageSize > 0)
        //        {
        //            int FirstResultIndex = ((objEventQuery.PageNumber - 1) * objEventQuery.PageSize);
        //            crCm.SetFirstResult(FirstResultIndex);
        //            crCm.SetMaxResults(objEventQuery.PageSize);
        //        }

        //        multiCrit.Add(typeof(HybridDataEvent), crCm);

        //        IList results = multiCrit.List();
        //        IList<HybridDataEvent> lstHyDataEvents = new List<HybridDataEvent>();

        //        lstHyDataEvents = (IList<HybridDataEvent>)results[1];
        //        //Out parameter
        //        TotalRows = (int)((IList)results[0])[0];

        //        return lstHyDataEvents;
        //    }
        //    catch (Exception ex)
        //    {
        //        MethodInfo objMethod = (MethodInfo)MethodBase.GetCurrentMethod();
        //        GlobalExceptionHandler.customExceptionHandler(ex, objMethod, null, ErrorSeverityType.Normal);
        //        TotalRows = 0;
        //        return null;
        //    }
        //}

        /// <summary>
        ///     This function returns the list of HybridDataEvent for admin
        /// </summary>
        /// <param name="objEventQuery">The obj event query.</param>
        /// <param name="TotalRows">The total rows.</param>
        /// <returns></returns>
        public IList<HybridDataEvent> GetByPartyIDDocumentIDForAdmin(EventQuery objEventQuery, out int TotalRows)
        {
            try
            {
                TotalRows = 0;
                var multiCrit = GetNewMultiCriteria();
                var crCm = GetNewCriteria();
                var crCm1 = GetNewCriteria();

                crCm1.SetProjection(Projections.Alias(Projections.RowCount(), "EventCode"));
                crCm1.Add(Restrictions.Between("Timestamp", objEventQuery.DateRange.StartDate,
                    objEventQuery.DateRange.EndDate));
                crCm1.Add(Restrictions.Eq("UserID", objEventQuery.PartyID));

                //Check for Visibility of Event
                crCm1.CreateAlias("EventCode", "EventCode");
                crCm.CreateAlias("EventCode", "EventCode");


                if (objEventQuery.Visibility)
                {
                    crCm1.Add(Restrictions.Eq("EventCode.AdminVisibilityTypeCD", "VSBL"));
                    crCm.Add(Restrictions.Eq("EventCode.AdminVisibilityTypeCD", "VSBL"));
                }

                if (objEventQuery.DocumentID > 0)
                {
                    //To DO: Add DocumentID to search criteria
                    crCm1.CreateAlias("Documents", "Documents");
                    crCm1.Add(Restrictions.Eq("Documents.Id", objEventQuery.DocumentID));

                    crCm.CreateAlias("Documents", "Documents");
                    crCm.Add(Restrictions.Eq("Documents.Id", objEventQuery.DocumentID));
                }

                multiCrit.Add(crCm1);

                crCm.Add(Restrictions.Between("Timestamp", objEventQuery.DateRange.StartDate,
                    objEventQuery.DateRange.EndDate));
                crCm.Add(Restrictions.Eq("UserID", objEventQuery.PartyID));


                //Order by Timestamp Desc
                crCm.AddOrder(Order.Desc("Timestamp"));

                // for Paging , Check if page size is greater than 0
                if (objEventQuery.PageSize > 0)
                {
                    var FirstResultIndex = (objEventQuery.PageNumber - 1) * objEventQuery.PageSize;
                    crCm.SetFirstResult(FirstResultIndex);
                    crCm.SetMaxResults(objEventQuery.PageSize);
                }

                multiCrit.Add(typeof(HybridDataEvent), crCm);

                var results = multiCrit.List();
                IList<HybridDataEvent> lstHyDataEvents = new List<HybridDataEvent>();

                lstHyDataEvents = (IList<HybridDataEvent>) results[1];
                //Out parameter
                TotalRows = (int) ((IList) results[0])[0];

                return lstHyDataEvents;
            }
            catch (Exception ex)
            {
                var objMethod = (MethodInfo) MethodBase.GetCurrentMethod();
                GlobalExceptionHandler.customExceptionHandler(ex, objMethod, null, ErrorSeverityType.Normal);
                TotalRows = 0;
                return null;
            }
        }

        /// <summary>
        ///     This function returns Total Pages
        /// </summary>
        /// <param name="TotalRecords"></param>
        /// <param name="PageSize"></param>
        /// <returns>int TotalPages</returns>
        private int GetTotalPages(int TotalRecords, int PageSize)
        {
            if (TotalRecords % PageSize == 0)
                return 0;
            return TotalRecords / PageSize;
        }

        ///// <summary>
        ///// Gets the by party ID.
        ///// </summary>
        ///// <param name="objEventQuery">The obj event query.</param>
        ///// <returns></returns>
        //public IList<HybridDataEvent> GetByPartyID(EventQuery objEventQuery)
        //{
        //    try
        //    {
        //        IMultiCriteria multiCrit = GetNewMultiCriteria();

        //        ICriteria crCm1 = GetNewCriteria();

        //        crCm1.SetProjection(Projections.Alias(Projections.RowCount(), "EventCode"));
        //        crCm1.Add(Expression.Between("Timestamp", objEventQuery.DateRange.StartDate, objEventQuery.DateRange.EndDate));
        //        crCm1.Add(Expression.Sql("PartyID=" + objEventQuery.PartyID));

        //        multiCrit.Add(crCm1);

        //        //int totalcount = crCm1.UniqueResult<int>();

        //        ICriteria crCm = GetNewCriteria();
        //        crCm.Add(Expression.Between("Timestamp", objEventQuery.DateRange.StartDate, objEventQuery.DateRange.EndDate));
        //        crCm.Add(Expression.Eq("PartyID", objEventQuery.PartyID));
        //        //Order by Timestamp Desc
        //        crCm.AddOrder(Order.Desc("Timestamp"));

        //        // for Paging
        //        int FirstResultIndex = ((objEventQuery.PageNumber - 1) * objEventQuery.PageSize) + 1;
        //        crCm.SetFirstResult(FirstResultIndex);
        //        crCm.SetMaxResults(objEventQuery.PageSize);

        //        multiCrit.Add(typeof(HybridDataEvent), crCm);

        //        IList results = multiCrit.List();

        //        IList<HybridDataEvent> HyDataEvents = new List<HybridDataEvent>();
        //        HyDataEvents = (IList<HybridDataEvent>)results[1];

        //        //Total count of records
        //        int count = (int)((IList)results[0])[0];
        //        objEventQuery.TotalPages = GetTotalPages(count, objEventQuery.PageSize);

        //        return HyDataEvents;
        //    }
        //    catch (Exception ex)
        //    {
        //        MethodInfo objMethod = (MethodInfo)MethodBase.GetCurrentMethod();
        //        GlobalExceptionHandler.customExceptionHandler(ex, objMethod, htParams, ErrorSeverityType.Normal);
        //        return null;
        //    }
        //}

        #endregion
    }
}