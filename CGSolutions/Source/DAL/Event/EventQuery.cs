﻿using System;
using DomainModel.Base.UtilityClasses;

namespace DataAccess.Event.DataTransfer
{
    /// <summary>
    /// 
    /// </summary>
    [Serializable]
    public class EventQuery
    {
        /// <summary>
        /// get or set Date Range
        /// </summary>
        private DateRange _dateRange;

        /// <summary>
        /// get or set DocumentID
        /// </summary>
        /// 
        private int _documentID;

        private int _firstResultIndex;


        private int _pageNumber;

        //following two properties (PageSize,PageNumber) are added for paging
        private int _pageSize;

        /// <summary>
        /// get or set party ID
        /// </summary>
        private int _partyId;

        private int _totalPages;


        private bool _visibility;

        /// <summary>
        /// Gets or sets the party ID.
        /// </summary>
        /// <value>The party ID.</value>
        public int PartyID
        {
            get => _partyId;
            set => _partyId = value;
        }

        /// <summary>
        /// Gets or sets the date range.
        /// </summary>
        /// <value>The date range.</value>
        public DateRange DateRange
        {
            get => _dateRange;
            set => _dateRange = value;
        }

        /// <summary>
        /// Gets or sets the document ID.
        /// </summary>
        /// <value>The document ID.</value>
        public int DocumentID
        {
            get => _documentID;
            set => _documentID = value;
        }

        /// <summary>
        /// Gets or sets the first index of the rseult.
        /// </summary>
        /// <value>The first index of the rseult.</value>
        public int FirstRseultIndex
        {
            get => _firstResultIndex;
            set => _firstResultIndex = value;
        }

        /// <summary>
        /// get or set page size for paging
        /// </summary>
        /// <value>The size of the page.</value>
        public virtual int PageSize
        {
            get => _pageSize;
            set => _pageSize = value;
        }

        /// <summary>
        /// get or set Currrent page in paging
        /// </summary>
        /// <value>The page number.</value>
        public virtual int PageNumber
        {
            get => _pageNumber;
            set => _pageNumber = value;
        }

        /// <summary>
        /// Gets or sets the total pages.
        /// </summary>
        /// <value>The total pages.</value>
        public int TotalPages
        {
            get => _totalPages;
            set => _totalPages = value;
        }

        /// <summary>
        /// Gets or sets a value indicating whether this <see cref="EventQuery"/> is visibility.
        /// </summary>
        /// <value><c>true</c> if visibility; otherwise, <c>false</c>.</value>
        public bool Visibility
        {
            get => _visibility;
            set => _visibility = value;
        }
    }
}
