﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Reflection;
using CommonModules.Exceptions;
using DataAccess.Base;
using DomainModel.Event;
using NHibernate.Criterion;

namespace DataAccess.Event
{
    /// <summary>
    /// </summary>
    public class EventDataData : DataManagerBase<EventData, Guid>
    {
        #region Get Methods

        /// <summary>
        ///     Gets the event data by event ID.
        /// </summary>
        /// <param name="guid">The GUID.</param>
        /// <returns></returns>
        public IList<EventData> GetEventDataByEventID(Guid guid)
        {
            IList<EventData> results = new List<EventData>();
            try
            {
                var crCm = GetNewCriteria();
                crCm.Add(Restrictions.Eq("UserEvent.Id", guid));
                results = crCm.List<EventData>();
            }
            catch (Exception ex)
            {
                var htParams = new Hashtable();
                var objMethod = (MethodInfo) MethodBase.GetCurrentMethod();
                GlobalExceptionHandler.customExceptionHandler(ex, objMethod, htParams, ErrorSeverityType.Normal);
            }

            return results;
        }

        #endregion Get Methods

        #region Constructors

        internal EventDataData()
        {
        }

        /// <summary>
        ///     Initializes a new instance of the <see cref="EventDataData" /> class.
        /// </summary>
        /// <param name="session">The session.</param>
        public EventDataData(INHibernateSession session)
            : base(session)
        {
        }

        #endregion Constructors
    }
}