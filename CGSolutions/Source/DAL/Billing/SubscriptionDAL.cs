﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Reflection;
using CommonModules.Exceptions;
using DataAccess.Base;
using DomainModel.Billing;
using NHibernate.Criterion;

namespace DataAccess.Billing
{
    public class SubscriptionDAL : DataManagerBase<Subscription, int>
    {
        public IList<Subscription> GetAllSubscriptionsByUserId(int userId)
        {
            try
            {
                var criteria = GetNewCriteria();
                criteria.Add(Restrictions.Eq("UserID", userId));
                return criteria.List<Subscription>();
            }
            catch (Exception ex)
            {
                var parameters = new Hashtable(1);
                parameters.Add("UserID", userId);
                var method = (MethodInfo) MethodBase.GetCurrentMethod();
                GlobalExceptionHandler.customExceptionHandler(ex, method, parameters, ErrorSeverityType.Normal);
                return null;
            }
        }
    }
}