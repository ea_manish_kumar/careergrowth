﻿namespace DataAccess.Billing
{
    public static class BillingFactory
    {
        public static FeatureDAL FeatureDAL => new FeatureDAL();

        public static PlanDAL PlanDAL => new PlanDAL();

        public static PlanTypeDAL PlanTypeDAL => new PlanTypeDAL();

        public static PlanTypeSkinDAL PlanTypeSkinDAL => new PlanTypeSkinDAL();

        public static PlanTypFeatureDAL PlanTypFeatureDAL => new PlanTypFeatureDAL();

        public static SubscriptionDAL SubscriptionDAL => new SubscriptionDAL();

        public static OrderDAL OrderDAL => new OrderDAL();

        public static OrderItemDAL OrderItemDAL => new OrderItemDAL();

        public static TransactionDAL TransactionDAL => new TransactionDAL();
    }
}