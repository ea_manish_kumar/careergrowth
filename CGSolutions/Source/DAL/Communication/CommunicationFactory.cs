﻿namespace DataAccess.Communication
{
    public class CommunicationFactory
    {
        #region Constructors

        #endregion

        #region Get Methods

        /// <summary>
        /// </summary>
        public static MailoutDAL MailoutDAL => new MailoutDAL();

        public static EmailSentDAL EmailSentDAL => new EmailSentDAL();

        public static EmailSentDTODAL EmailSentDTODAL => new EmailSentDTODAL();

        public static EmailLinkDAL EmailLinkDAL => new EmailLinkDAL();

        #endregion
    }
}