﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Reflection;
using CommonModules.Exceptions;
using DataAccess.Base;
using DomainModel.Communication;

namespace DataAccess.Communication
{
    public class EmailSentDTODAL : DataManagerBase<EmailSent, int>
    {
        #region Get Methods

        public IList<EmailSentDetailDTO> getEmailSentByUserID(int UserID)
        {
            try
            {
                var Q = GetNamedQuery("Communication.GetEmailSentDetailsByUserID");
                Q.SetInt32("UserID", UserID);

                return Q.List<EmailSentDetailDTO>();
            }
            catch (Exception ex)
            {
                var objMethod = (MethodInfo) MethodBase.GetCurrentMethod();
                var htParams = new Hashtable(1);
                htParams.Add("UserID", UserID.ToString());
                GlobalExceptionHandler.customExceptionHandler(ex, objMethod, htParams, ErrorSeverityType.Normal);
                return null;
            }
        }

        #endregion Get Methods

        #region Constructors

        internal EmailSentDTODAL()
        {
        }

        /// <summary>
        ///     Initializes a new instance of the <see cref="EmailSentDAL" /> class.
        /// </summary>
        /// <param name="session">The session.</param>
        public EmailSentDTODAL(INHibernateSession session)
            : base(session)
        {
        }

        #endregion
    }
}