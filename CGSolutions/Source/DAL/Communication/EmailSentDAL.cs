﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Reflection;
using CommonModules.Exceptions;
using DataAccess.Base;
using DomainModel.Communication;
using NHibernate.Criterion;

namespace DataAccess.Communication
{
    public class EmailSentDAL : DataManagerBase<EmailSent, int>
    {
        #region Get Methods

        public IList<EmailSent> GetEmailSent(int UserID)
        {
            IList<EmailSent> results = new List<EmailSent>();
            try
            {
                var crCm = GetNewCriteria();
                crCm.Add(Restrictions.Eq("UserID", UserID));
                results = crCm.List<EmailSent>();
            }
            catch (Exception ex)
            {
                var objMethod = (MethodInfo) MethodBase.GetCurrentMethod();
                var htParams = new Hashtable(1);
                htParams.Add("UserID", UserID.ToString());
                GlobalExceptionHandler.customExceptionHandler(ex, objMethod, htParams, ErrorSeverityType.Normal);
                return null;
            }

            return results;
        }

        #endregion Get Methods

        #region Constructors

        internal EmailSentDAL()
        {
        }

        /// <summary>
        ///     Initializes a new instance of the <see cref="EmailSentDAL" /> class.
        /// </summary>
        /// <param name="session">The session.</param>
        public EmailSentDAL(INHibernateSession session)
            : base(session)
        {
        }

        #endregion
    }
}