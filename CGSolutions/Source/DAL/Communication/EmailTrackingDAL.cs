﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Reflection;
using CommonModules.Exceptions;
using DataAccess.Base;
using DomainModel.Communication;
using NHibernate.Criterion;

namespace DataAccess.Communication
{
    public class EmailTrackingDAL : DataManagerBase<EmailTracking, int>
    {
        public EmailTracking GetEmailTrackingByMailOutTrackingDate(int mailOutId, DateTime trackingDate)
        {
            IList<EmailTracking> lstEmailTrckin = null;

            try
            {
                var cr = GetNewCriteria();
                cr.Add(Restrictions.Eq("MailOutID", mailOutId));
                cr.Add(Restrictions.Eq("TrackingDate", trackingDate));
                lstEmailTrckin = cr.List<EmailTracking>();
                if (lstEmailTrckin != null && lstEmailTrckin.Count > 0) return lstEmailTrckin[0];
            }
            catch (Exception ex)
            {
                var objMethod = (MethodInfo) MethodBase.GetCurrentMethod();
                var htParams = new Hashtable(1);
                htParams.Add("UserID", mailOutId.ToString());
                htParams.Add("TrackingDate", trackingDate.ToShortDateString());
                GlobalExceptionHandler.customExceptionHandler(ex, objMethod, htParams, ErrorSeverityType.Normal);
            }

            return null;
        }
    }
}