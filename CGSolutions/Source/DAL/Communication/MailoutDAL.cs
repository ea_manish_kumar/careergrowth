﻿using DataAccess.Base;
using DomainModel.Communication;

namespace DataAccess.Communication
{
    /// <summary>
    /// </summary>
    public class MailoutDAL : DataManagerBase<Mailout, int>
    {
        #region Constructors

        internal MailoutDAL()
        {
        }


        public MailoutDAL(INHibernateSession session)
            : base(session)
        {
        }

        #endregion

        //public DocStyle GetDocStyleByDocument(int DocumentID, string StyleCD)
        //{
        //    try
        //    {
        //        ICriteria criteria = GetNewCriteria();
        //        criteria.Add(Restrictions.Eq("DocumentID", DocumentID));
        //        criteria.Add(Restrictions.Eq("StyleCD", StyleCD));
        //        return criteria.UniqueResult<DocStyle>();
        //    }
        //    catch (Exception ex)
        //    {

        //    }
        //    return null;
        //}
    }
}