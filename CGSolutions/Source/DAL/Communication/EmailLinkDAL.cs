﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Reflection;
using CommonModules.Exceptions;
using DataAccess.Base;
using DomainModel.Communication;
using NHibernate.Criterion;

namespace DataAccess.Communication
{
    public class EmailLinkDAL : DataManagerBase<EmailLink, int>
    {
        /// <summary>
        ///     Returns EmailLink by MailOutID and Name
        /// </summary>
        /// <param name="mailoutId">Email Template ID</param>
        /// <param name="name">Link Name</param>
        /// <returns>EmailLink</returns>
        public EmailLink GetUniqueEmailLink(int mailoutId, string name)
        {
            IList<EmailLink> lstEmailLink = new List<EmailLink>();
            EmailLink emailLink = null;

            try
            {
                var cr = GetNewCriteria();
                cr.Add(Restrictions.Eq("MailOutID", mailoutId));
                cr.Add(Restrictions.Eq("Name", name));
                lstEmailLink = cr.List<EmailLink>();

                if (lstEmailLink != null && lstEmailLink.Count > 0) emailLink = lstEmailLink[0];
            }
            catch (Exception ex)
            {
                var objMethod = (MethodInfo) MethodBase.GetCurrentMethod();
                var htParams = new Hashtable(2);
                htParams.Add("MailOutID", mailoutId.ToString());
                htParams.Add("LinkName", name);
                GlobalExceptionHandler.customExceptionHandler(ex, objMethod, htParams, ErrorSeverityType.Normal);
                return null;
            }

            return emailLink;
        }
    }
}