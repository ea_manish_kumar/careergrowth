﻿using System;
using System.Collections.Generic;
using System.Reflection;
using System.Collections;
using RB3.DAL.Base;
using RB3.Commons.Exceptions;
using NHibernate;
using NHibernate.Criterion;
using RB3.DomainModel.Analytics;



namespace RB3.DAL.Analytics
{
    public partial class UserAgentTypeDAL : DataManagerBase<UserAgentTypeDTO, int>
    {
         #region Constructors

        internal UserAgentTypeDAL()
            : base()
        {
        }
        /// <summary>
        /// Initializes a new instance of the <see cref="RevenueDAL"/> class.
        /// </summary>
        /// <param name="session">The session.</param>
        public UserAgentTypeDAL(INHibernateSession session)
            : base(session)
        {
        }

        #endregion

        #region Get Methods

        public IList<UserAgentTypeDTO> GetByCriteria(string BrowserName, string BrowserVersion, string EngineName, string EngineVersion, string OSName, string OSVersion, string DeviceModel, string DeviceType, string DTDeviceType)
        {
            try
            {
                ICriteria criteria = GetNewCriteria();
                criteria.Add(NHibernate.Criterion.Expression.Eq("BrowserName", BrowserName));
                criteria.Add(NHibernate.Criterion.Expression.Eq("BrowserVersion", BrowserVersion));
                criteria.Add(NHibernate.Criterion.Expression.Eq("EngineName", EngineName));
                criteria.Add(NHibernate.Criterion.Expression.Eq("EngineVersion", EngineVersion));
                criteria.Add(NHibernate.Criterion.Expression.Eq("OSName", OSName));
                criteria.Add(NHibernate.Criterion.Expression.Eq("OSVersion", OSVersion));
                criteria.Add(NHibernate.Criterion.Expression.Eq("DeviceModel", DeviceModel));
                criteria.Add(NHibernate.Criterion.Expression.Eq("DeviceType", DeviceType));
                criteria.Add(NHibernate.Criterion.Expression.Eq("DTDeviceType", DTDeviceType));
                return criteria.List<UserAgentTypeDTO>();
            }
            catch (Exception Ex)
            {
                Hashtable htParams = new Hashtable(1);
                MethodInfo objMethod = (MethodInfo)MethodBase.GetCurrentMethod();
                GlobalExceptionHandler.customExceptionHandler(Ex, objMethod, htParams, ErrorSeverityType.Normal);                
                 return null;
            }          
        }        
        
        #endregion Get Methods

    }
}
