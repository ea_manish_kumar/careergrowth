﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Reflection;
using CommonModules.Exceptions;
using DataAccess.Base;
using DomainModel.Analytics;

namespace DataAccess.Analytics
{
    public class SubscriptionSnapshotDAL : DataManagerBase<SubscriptionSnapshotDTO, int>
    {
        #region Get Methods

        /// <summary>
        ///     Get SubscriptionSnapshot Report for between supplied start date and end date
        /// </summary>
        /// <param name="StartDate">StartDate</param>
        /// <param name="EndDate">EndDate</param>
        /// <returns>List of object SubscriptionSnapshotDTO</returns>
        public IList<SubscriptionSnapshotDTO> GetSubscriptionSnapshotReport(DateTime StartDate, DateTime EndDate)
        {
            IList<SubscriptionSnapshotDTO> lstSubscriptionSnapshot = null;

            try
            {
                var Q = GetNamedQuery("Analytics.GetSubscriptionSnapshotReport");
                Q.SetParameter("StartDate", StartDate);
                Q.SetParameter("EndDate", EndDate);
                lstSubscriptionSnapshot = Q.List<SubscriptionSnapshotDTO>();
            }
            catch (Exception Ex)
            {
                var htParams = new Hashtable(1);
                var objMethod = (MethodInfo) MethodBase.GetCurrentMethod();
                GlobalExceptionHandler.customExceptionHandler(Ex, objMethod, htParams, ErrorSeverityType.Normal);
                return null;
            }

            return lstSubscriptionSnapshot;
        }

        #endregion Get Methods

        #region Constructors

        internal SubscriptionSnapshotDAL()
        {
        }

        /// <summary>
        ///     Initializes a new instance of the <see cref="RevenueDAL" /> class.
        /// </summary>
        /// <param name="session">The session.</param>
        public SubscriptionSnapshotDAL(INHibernateSession session)
            : base(session)
        {
        }

        #endregion
    }
}