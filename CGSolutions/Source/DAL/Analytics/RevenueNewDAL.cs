﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Reflection;
using CommonModules.Exceptions;
using DataAccess.Base;
using DomainModel.Analytics;

namespace DataAccess.Analytics
{
    public class RevenueNewDAL : DataManagerBase<RevenueByProductNewDTO, int>
    {
        #region Get Methods

        /// <summary>
        ///     Get Revenue Report for between supplied start date and end date
        /// </summary>
        /// <param name="StartDate">StartDate</param>
        /// <param name="EndDate">EndDate</param>
        /// <returns>List of object RevenueByProductDTO</returns>
        public IList<RevenueByProductNewDTO> GetRevenueReport(DateTime StartDate, DateTime EndDate)
        {
            IList<RevenueByProductNewDTO> lstRevenue = null;

            try
            {
                var Q = GetNamedQuery("Analytics.GetRevenueReportNew");
                Q.SetParameter("StartDate", StartDate);
                Q.SetParameter("EndDate", EndDate);
                lstRevenue = Q.List<RevenueByProductNewDTO>();
            }
            catch (Exception Ex)
            {
                var htParams = new Hashtable(1);
                var objMethod = (MethodInfo) MethodBase.GetCurrentMethod();
                GlobalExceptionHandler.customExceptionHandler(Ex, objMethod, htParams, ErrorSeverityType.Normal);
                return null;
            }

            return lstRevenue;
        }

        #endregion Get Methods

        #region Constructors

        internal RevenueNewDAL()
        {
        }

        /// <summary>
        ///     Initializes a new instance of the <see cref="RevenueDAL" /> class.
        /// </summary>
        /// <param name="session">The session.</param>
        public RevenueNewDAL(INHibernateSession session)
            : base(session)
        {
        }

        #endregion
    }
}