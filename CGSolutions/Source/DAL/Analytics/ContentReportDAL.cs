﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Reflection;
using CommonModules.Exceptions;
using DataAccess.Base;
using DomainModel.Analytics;

namespace DataAccess.Analytics
{
    /// <summary>
    ///     Class for ContentReportDTO
    /// </summary>
    public class ContentDAL : DataManagerBase<ContentReportDTO, int>
    {
        #region Get Methods

        /// <summary>
        ///     Resurns List of ContentReport for User ID
        /// </summary>
        /// <param name="PartyID"></param>
        /// <param name="ABTestID"></param>
        /// <returns></returns>
        public IList<ContentReportDTO> GetContentReport(DateTime StartDate, DateTime EndDate)
        {
            IList<ContentReportDTO> lstContentRepot = null;

            try
            {
                var Q = GetNamedQuery("Analytics.GetContentReport");
                Q.SetParameter("StartDate", StartDate);
                Q.SetParameter("EndDate", EndDate);
                lstContentRepot = Q.List<ContentReportDTO>();
            }
            catch (Exception Ex)
            {
                var htParams = new Hashtable(1);
                var objMethod = (MethodInfo) MethodBase.GetCurrentMethod();
                GlobalExceptionHandler.customExceptionHandler(Ex, objMethod, htParams, ErrorSeverityType.Normal);
                return null;
            }

            return lstContentRepot;
        }

        #endregion

        #region Constructror

        /// <summary>
        ///     Default Constructror
        /// </summary>
        public ContentDAL()
        {
        }

        /// <summary>
        ///     Initializes a new instance of the <see cref="AdminNotesDTO" /> class.
        /// </summary>
        /// <param name="session">The session.</param>
        public ContentDAL(INHibernateSession session)
            : base(session)
        {
        }

        #endregion
    }
}