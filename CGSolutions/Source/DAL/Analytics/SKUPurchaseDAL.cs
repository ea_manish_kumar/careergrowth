﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Reflection;
using CommonModules.Exceptions;
using DataAccess.Base;
using DomainModel.Analytics;

namespace DataAccess.Analytics
{
    public class SKUPurchaseDAL : DataManagerBase<SKUPurchaseDTO, int>
    {
        #region Get Methods

        /// <summary>
        ///     Get SKUPurchase Report for between supplied start date and end date
        /// </summary>
        /// <param name="StartDate">StartDate</param>
        /// <param name="EndDate">EndDate</param>
        /// <returns>List of object SKUPurchaseDTO</returns>
        public IList<SKUPurchaseDTO> GetSKUPurchaseReport(DateTime StartDate, DateTime EndDate)
        {
            IList<SKUPurchaseDTO> lstSKUPurchase = null;

            try
            {
                var Q = GetNamedQuery("Analytics.GetSKUPurchaseReport");
                Q.SetParameter("StartDate", StartDate);
                Q.SetParameter("EndDate", EndDate);
                lstSKUPurchase = Q.List<SKUPurchaseDTO>();
            }
            catch (Exception Ex)
            {
                var htParams = new Hashtable(1);
                var objMethod = (MethodInfo) MethodBase.GetCurrentMethod();
                GlobalExceptionHandler.customExceptionHandler(Ex, objMethod, htParams, ErrorSeverityType.Normal);
                return null;
            }

            return lstSKUPurchase;
        }

        #endregion Get Methods

        #region Constructors

        internal SKUPurchaseDAL()
        {
        }

        /// <summary>
        ///     Initializes a new instance of the <see cref="RevenueDAL" /> class.
        /// </summary>
        /// <param name="session">The session.</param>
        public SKUPurchaseDAL(INHibernateSession session)
            : base(session)
        {
        }

        #endregion
    }
}