﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Reflection;
using CommonModules.Exceptions;
using DataAccess.Base;
using DomainModel.Analytics;

namespace DataAccess.Analytics
{
    public class RenewalBySKUDAL : DataManagerBase<RenewalBySKUDTO, int>
    {
        #region Get Methods

        /// <summary>
        ///     Get RenewalBySKU Report for supplied skuid, start date and period
        /// </summary>
        /// <param name="SKUId"></param>
        /// <param name="StartDate"></param>
        /// <param name="Period"></param>
        /// <returns></returns>
        public IList<RenewalBySKUDTO> GetRenewalBySKU(string SKUId, DateTime StartDate, int Period)
        {
            IList<RenewalBySKUDTO> lstRenewalBySKU = null;

            try
            {
                var Q = GetNamedQuery("Analytics.GetRenewalBySKU");
                Q.SetParameter("SKUID", SKUId);
                Q.SetParameter("Startdate", StartDate);
                Q.SetParameter("Period", Period);
                lstRenewalBySKU = Q.List<RenewalBySKUDTO>();
            }
            catch (Exception Ex)
            {
                var htParams = new Hashtable(1);
                var objMethod = (MethodInfo) MethodBase.GetCurrentMethod();
                GlobalExceptionHandler.customExceptionHandler(Ex, objMethod, htParams, ErrorSeverityType.Normal);
                return null;
            }

            return lstRenewalBySKU;
        }

        #endregion Get Methods

        #region Constructors

        internal RenewalBySKUDAL()
        {
        }

        /// <summary>
        ///     Initializes a new instance of the <see cref="RenewalBySKUDAL" /> class.
        /// </summary>
        /// <param name="session">The session.</param>
        public RenewalBySKUDAL(INHibernateSession session)
            : base(session)
        {
        }

        #endregion
    }
}