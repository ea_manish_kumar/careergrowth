﻿using System;
using System.Collections.Generic;
using System.Reflection;
using System.Collections;
using RB3.DAL.Base;
using RB3.Commons.Exceptions;
using NHibernate;
using NHibernate.Criterion;
using RB3.DomainModel.Analytics;

namespace RB3.DAL.Analytics
{
    /// <summary>
    /// 
    /// </summary>
    public partial class CLFunnelReportDAL : DataManagerBase<ReportByCLFunnelDTO, int>
    {
        #region Constructors

        internal CLFunnelReportDAL()
            : base()
        {
        }
        /// <summary>
        /// Initializes a new instance of the <see cref="RevenueDAL"/> class.
        /// </summary>
        /// <param name="session">The session.</param>
        public CLFunnelReportDAL(INHibernateSession session)
            : base(session)
        {
        }

        #endregion

        #region Get Methods
        /// <summary>
        /// Get Revenue Report for between supplied start date and end date
        /// </summary>
        /// <param name="StartDate">StartDate</param>
        /// <param name="EndDate">EndDate</param>
        /// <returns>List of object ReportByCLFunnelDTO</returns>
        public IList<ReportByCLFunnelDTO> GetCLFunnelReport(DateTime StartDate, DateTime EndDate)
        {
            IList<ReportByCLFunnelDTO> lstRevenue = null;

            try
            {
                IQuery Q = GetNamedQuery("Analytics.GetCLFunnelReport");
                Q.SetParameter("StartDate", StartDate);
                Q.SetParameter("EndDate", EndDate);
               lstRevenue = Q.List<ReportByCLFunnelDTO>();
            }
            catch (Exception Ex)
            {
                Hashtable htParams = new Hashtable(1);
                MethodInfo objMethod = (MethodInfo)MethodBase.GetCurrentMethod();
                GlobalExceptionHandler.customExceptionHandler(Ex, objMethod, htParams, ErrorSeverityType.Normal);
                return null;
            }
            return lstRevenue;
        }
       
        #endregion Get Methods
        
    }
}
