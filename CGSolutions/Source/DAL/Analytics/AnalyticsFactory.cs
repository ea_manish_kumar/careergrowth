﻿namespace DataAccess.Analytics
{
    public class AnalyticsFactory
    {
        #region Constructors

        #endregion

        #region Get Methods

        public static SKUPurchaseDAL SKUPurchaseDAL => new SKUPurchaseDAL();

        public static SubscriptionSnapshotDAL SubscriptionSnapshotDAL => new SubscriptionSnapshotDAL();

        public static ABTestDAL ABTestDAL => new ABTestDAL();

        public static AdminNotesDAL AdminNotesDAL => new AdminNotesDAL();

        public static RenewalBySKUDAL RenewalBySKUDAL => new RenewalBySKUDAL();

        public static RevenueNewDAL RevenueNewDAL => new RevenueNewDAL();

        public static ContentDAL ContentReportDAL => new ContentDAL();

        public static CoverLetterFunnelDAL CLFunnelReportDAL => new CoverLetterFunnelDAL();

        public static RevenueByCoverLetterInsightDAL RevenueByRgCLInsightDAL => new RevenueByCoverLetterInsightDAL();

        #endregion Get Methods
    }
}