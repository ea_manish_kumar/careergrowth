﻿using DataAccess.Base;
using DomainModel.Analytics;

namespace DataAccess.Analytics
{
    /// <summary>
    ///     Class for AdminNotesDTO
    /// </summary>
    public class AdminNotesDAL : DataManagerBase<AdminNotesDTO, int>
    {
        #region Constructror

        /// <summary>
        ///     Default Constructror
        /// </summary>
        public AdminNotesDAL()
        {
        }

        /// <summary>
        ///     Initializes a new instance of the <see cref="AdminNotesDTO" /> class.
        /// </summary>
        /// <param name="session">The session.</param>
        public AdminNotesDAL(INHibernateSession session)
            : base(session)
        {
        }

        #endregion

        #region Get Methods

        ///// <summary>
        ///// Resurns List of ABtestData for PartyId by ABTestId
        ///// </summary>
        ///// <param name="PartyID"></param>
        ///// <param name="ABTestID"></param>
        ///// <returns></returns>
        //public IList<AdminNotesDTO> GetAdminNotes(int ABTestId, int adminUserID)
        //{
        //    IList<AdminNotesDTO> lst = null;
        //    try
        //    {
        //        IQuery Q = GetNamedQuery("AdminNotesDTO.GetAdminNotes");
        //        Q.SetParameter("ABTestID", ABTestId);
        //        Q.SetParameter("AdminUserID", adminUserID);

        //        lst = Q.List<DomainModel.Analytics.AdminNotesDTO>();
        //        //lst = q.List<DomainModel.Tests.AdminNotesDTO>();
        //    }
        //    catch (Exception ex)
        //    {
        //        Hashtable htParams = new Hashtable(1);
        //        htParams.Add("ABTestID", ABTestId);
        //        htParams.Add("adminUserID", adminUserID);
        //        MethodInfo objMethod = (MethodInfo)MethodBase.GetCurrentMethod();
        //        GlobalExceptionHandler.customExceptionHandler(ex, objMethod, htParams, ErrorSeverityType.Normal);

        //    }
        //    return lst;
        //}

        #endregion
    }
}