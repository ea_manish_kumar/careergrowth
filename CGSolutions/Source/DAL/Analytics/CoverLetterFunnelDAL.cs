﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Reflection;
using CommonModules.Exceptions;
using DataAccess.Base;
using DomainModel.Analytics;

namespace DataAccess.Analytics
{
    /// <summary>
    /// </summary>
    public class CoverLetterFunnelDAL : DataManagerBase<ReportByCverLetterFunnelDTO, int>
    {
        #region Get Methods

        /// <summary>
        ///     Get Revenue Report for between supplied start date and end date
        /// </summary>
        /// <param name="StartDate">StartDate</param>
        /// <param name="EndDate">EndDate</param>
        /// <returns>List of object ReportByCLFunnelDTO</returns>
        public IList<ReportByCverLetterFunnelDTO> GetCLFunnelReport(DateTime StartDate, DateTime EndDate)
        {
            IList<ReportByCverLetterFunnelDTO> lstRevenue = null;

            try
            {
                var Q = GetNamedQuery("Analytics.GetCLFunnelReport");
                Q.SetParameter("StartDate", StartDate);
                Q.SetParameter("EndDate", EndDate);
                lstRevenue = Q.List<ReportByCverLetterFunnelDTO>();
            }
            catch (Exception Ex)
            {
                var htParams = new Hashtable(1);
                var objMethod = (MethodInfo) MethodBase.GetCurrentMethod();
                GlobalExceptionHandler.customExceptionHandler(Ex, objMethod, htParams, ErrorSeverityType.Normal);
                return null;
            }

            return lstRevenue;
        }

        #endregion Get Methods

        #region Constructors

        internal CoverLetterFunnelDAL()
        {
        }

        /// <summary>
        ///     Initializes a new instance of the <see cref="RevenueDAL" /> class.
        /// </summary>
        /// <param name="session">The session.</param>
        public CoverLetterFunnelDAL(INHibernateSession session)
            : base(session)
        {
        }

        #endregion
    }
}