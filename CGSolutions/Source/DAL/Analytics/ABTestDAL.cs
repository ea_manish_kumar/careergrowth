﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Reflection;
using CommonModules.DataManager;
using CommonModules.Exceptions;
using DataAccess.Base;
using DomainModel.Analytics;

namespace DataAccess.Analytics
{
    public class ABTestDAL : DataManagerBase<ABTestDTO, int>
    {
        #region Get Methods

        /// <summary>
        ///     Get ABTest Report for between supplied AbTestId and start date and end date
        /// </summary>
        /// <param name="ABTestId">ABTestId</param>
        /// <param name="StartDate">StartDate</param>
        /// <param name="EndDate">EndDate</param>
        /// <returns>List of object ABTestDTO</returns>
        public IList<ABTestDTO> GetABTestReport(int ABTestID, DateTime StartDate, DateTime EndDate)
        {
            IList<ABTestDTO> lstABTest = null;

            try
            {
                var sqlQuery = "prc_ABTest_Report";
                var parameters = new SqlParameter[3];
                parameters[0] = new SqlParameter("@ABTestID", ABTestID);
                parameters[1] = new SqlParameter("@StartDate", StartDate);
                parameters[2] = new SqlParameter("@EndDate", EndDate);

                var dataManager = new SQLDataManager();
                var dtReport =
                    dataManager.GetDetails(sqlQuery, parameters, CommandType.StoredProcedure,
                        600); /// Timeout set in seconds to 10 minutes.

                if (dtReport != null && dtReport.Rows != null && dtReport.Rows.Count > 0)
                {
                    lstABTest = new List<ABTestDTO>();
                    for (var i = 0; i < dtReport.Rows.Count; i++)
                    {
                        var objABTestDTO = new ABTestDTO();
                        objABTestDTO.Name = dtReport.Rows[i].Field<string>("Name");
                        objABTestDTO.Label = dtReport.Rows[i].Field<string>("Label");
                        objABTestDTO.ABTestID = dtReport.Rows[i].Field<int>("ABTestID");
                        objABTestDTO.CaseIndex = dtReport.Rows[i].Field<int>("CaseIndex");
                        objABTestDTO.Par = dtReport.Rows[i].Field<int>("Par");
                        objABTestDTO.Reg = dtReport.Rows[i].Field<int>("Reg");
                        objABTestDTO.Edt = dtReport.Rows[i].Field<int>("Edt");
                        objABTestDTO.Subs = dtReport.Rows[i].Field<int>("Subs");
                        objABTestDTO.SubsAmount = dtReport.Rows[i].Field<decimal?>("Subs$");
                        objABTestDTO.RecurAmount = dtReport.Rows[i].Field<decimal?>("Recur$");
                        objABTestDTO.RfndAmount = dtReport.Rows[i].Field<decimal?>("Rfnd$");
                        objABTestDTO.ChbkAmount = dtReport.Rows[i].Field<decimal?>("Chbk$");
                        objABTestDTO.TotalSubsRevAmount = dtReport.Rows[i].Field<decimal?>("TotalSubsRev$");

                        lstABTest.Add(objABTestDTO);
                    }
                }
            }
            catch (Exception Ex)
            {
                var htParams = new Hashtable(3);
                htParams.Add("ABTestID", ABTestID);
                htParams.Add("StartDate", StartDate);
                htParams.Add("EndDate", EndDate);
                var objMethod = (MethodInfo) MethodBase.GetCurrentMethod();
                GlobalExceptionHandler.customExceptionHandler(Ex, objMethod, htParams, ErrorSeverityType.Normal);
            }

            return lstABTest;
        }

        #endregion Get Methods

        #region Constructors

        internal ABTestDAL()
        {
        }

        /// <summary>
        ///     Initializes a new instance of the <see cref="RevenueDAL" /> class.
        /// </summary>
        /// <param name="session">The session.</param>
        public ABTestDAL(INHibernateSession session)
            : base(session)
        {
        }

        #endregion
    }
}