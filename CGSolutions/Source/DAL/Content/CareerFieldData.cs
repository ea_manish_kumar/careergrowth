﻿using System.Collections;
using System.Collections.Generic;
using System.Reflection;
using System;
using RB3.DAL.Base;
using RB3.Commons.Exceptions;
using NHibernate;
using NHibernate.Criterion;
using System.Linq;
using RB3.DomainModel.Content;
using RB3.Commons.Caching;
using RB3.DAL.Base;

namespace RB3.DAL.Content
{
     /// <summary>
    /// TODO: Comments missing
    /// </summary>
    public partial class CareerFieldData : DataManagerBase<RB3.DomainModel.Content.CareerField, string>
    {
        #region Constructors

        internal CareerFieldData()
            : base()
        {
        }


        public CareerFieldData(INHibernateSession session)
            : base(session)
        {
        }

        #endregion


        //public IList<RB3.DomainModel.Content.CareerField> GetByParentIDID(int iParentID)
        //{
        //    try
        //    {
        //        ICriteria criteria = GetNewCriteria();
        //        criteria.Add(Expression.Eq("ParentID", iParentID));
        //        criteria.SetCacheable(true);

        //        return criteria.List<RB3.DomainModel.Content.CareerField>();
        //    }
        //    catch (Exception ex)
        //    {
        //        Hashtable htErrorArgs = new Hashtable(1);
        //        htErrorArgs.Add("ParentID", iParentID);
        //        MethodInfo objMethod = (MethodInfo)MethodBase.GetCurrentMethod();
        //        GlobalExceptionHandler.customExceptionHandler(ex, objMethod, htErrorArgs, ErrorSeverityType.Critical);
        //    }
        //    return null;
        //}
    }
}
