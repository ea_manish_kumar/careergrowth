﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Reflection;
using CommonModules.Exceptions;
using DataAccess.Base;
using DomainModel.Content;

namespace DataAccess.Content
{
    public class ContentsDAL : DataManagerBase<Contents, string>
    {
        public IList<string> GetExamples(int sectionTypeId, string jobTitle)
        {
            IList<string> examples = null;
            try
            {
                var Q = GetNamedQuery("Content.GetExamples");
                Q.SetParameter("SectionTypeID", sectionTypeId);
                Q.SetParameter("JobTitle", jobTitle);
                examples = Q.List<string>();
            }
            catch (Exception ex)
            {
                var htParams = new Hashtable(2);
                htParams.Add("SectionTypeId", sectionTypeId);
                htParams.Add("JobTitle", jobTitle);
                var objMethod = (MethodInfo) MethodBase.GetCurrentMethod();
                GlobalExceptionHandler.customExceptionHandler(ex, objMethod, htParams, ErrorSeverityType.Normal);
            }

            return examples;
        }

        public IList<string> GetGenericExamples(int sectionTypeId)
        {
            IList<string> examples = null;
            try
            {
                var Q = GetNamedQuery("Content.GetGenericExamples");
                Q.SetParameter("SectionTypeID", sectionTypeId);
                examples = Q.List<string>();
            }
            catch (Exception ex)
            {
                var htParams = new Hashtable(1);
                htParams.Add("SectionTypeId", sectionTypeId);
                var objMethod = (MethodInfo) MethodBase.GetCurrentMethod();
                GlobalExceptionHandler.customExceptionHandler(ex, objMethod, htParams, ErrorSeverityType.Normal);
            }

            return examples;
        }
    }
}