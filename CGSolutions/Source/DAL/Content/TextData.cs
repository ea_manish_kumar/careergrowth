using System;
using DataAccess.Base;
using DomainModel.Content;

namespace DataAccess.Content
{
    /// <summary>
    ///     TODO: Comments missing
    /// </summary>
    public class TextData : DataManagerBase<Text, int>
    {
        //Add any custom code here. The .generated file should not be hand edited as the changes will get overwritten next time the file is generated from the DB
        //This file will not get regenerated

        /// <summary>
        ///     Initializes a new instance of the <see cref="TextData" /> class.
        /// </summary>
        /// <param name="LangType">Type of the lang.</param>
        public TextData(LanguageType LangType)
        {
            LanguageType = LangType;
        }

        /// <summary>
        ///     Gets or sets the type of the language.
        /// </summary>
        /// <value>The type of the language.</value>
        public LanguageType LanguageType
        {
            //if (_languagetype == default(Language))
            //    _languagetype = LanguageType.EN_US_Default;    //Default Language en-us
            get;
            set;
        } = LanguageType.EN_US_Default;

        public Text GetLanguageSpecificById(int id)
        {
            //Get the Text object
            var T = base.GetById(id);
            T.LanguageText = ContentDataFactory.TextItemData
                .GetUniqueByLanguageIDTextID(Convert.ToInt32(LanguageType), id).TextValue;
            return T;
        }
    }
}