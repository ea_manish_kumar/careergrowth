using DataAccess.Base;

namespace DataAccess.Content
{
    public class ContentDataFactory : DataFactoryBase
    {
        #region Get Methods

        public static TypeCodeData TypeCodeData => new TypeCodeData();

        internal static TextItemData TextItemData => new TextItemData();

        #endregion
    }
}