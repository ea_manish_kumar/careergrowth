﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Reflection;
using CommonModules.Exceptions;
using DataAccess.Base;
using DomainModel.Content;

namespace DataAccess.Content
{
    public class JobTitlesDAL : DataManagerBase<JobTitles, int>
    {
        public IList<string> GetJobTitles(string title)
        {
            IList<string> jobTitles = null;

            try
            {
                var Q = GetNamedQuery("Content.GetJobTitles");
                Q.SetParameter("Title", title);
                jobTitles = Q.List<string>();
            }
            catch (Exception ex)
            {
                var htParams = new Hashtable(1);
                htParams.Add("Title", title);
                var objMethod = (MethodInfo) MethodBase.GetCurrentMethod();
                GlobalExceptionHandler.customExceptionHandler(ex, objMethod, htParams, ErrorSeverityType.Normal);
            }

            return jobTitles;
        }
    }
}