﻿using DataAccess.Base;
using DomainModel.Content;

namespace DataAccess.Content
{
    public class TipsDAL : DataManagerBase<Tips, int>
    {
        #region Constructors

        internal TipsDAL()
        {
        }

        public TipsDAL(INHibernateSession session)
            : base(session)
        {
        }

        #endregion
    }
}