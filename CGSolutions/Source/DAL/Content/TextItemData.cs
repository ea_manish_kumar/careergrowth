using DataAccess.Base;
using DomainModel.Content;
using NHibernate.Criterion;

namespace DataAccess.Content
{
    /// <summary>
    /// </summary>
    public class TextItemData : DataManagerBase<TextItem, int>
    {
        //Add any custom code here. The .generated file should not be hand edited as the changes will get overwritten next time the file is generated from the DB
        //This file will not get regenerated

        /// <summary>
        ///     This function returns a Unique TextItem object based on the unique combination of
        ///     LanguageID and TextID
        /// </summary>
        /// <param name="languageID">The language ID.</param>
        /// <param name="TextID">The text ID.</param>
        /// <returns></returns>
        public TextItem GetUniqueByLanguageIDTextID(int languageID, int TextID)
        {
            var criteria = Session.GetISession().CreateCriteria(typeof(TextItem));

            var languageIDCriteria = criteria.CreateCriteria("Language");
            languageIDCriteria.Add(Restrictions.Eq("Id", languageID));

            var text2Criteria = criteria.CreateCriteria("Text");
            text2Criteria.Add(Restrictions.Eq("Id", TextID));

            return criteria.UniqueResult<TextItem>();
        }
    }
}