﻿using DataAccess.Base;

namespace DataAccess.Content
{
    public class ContentFactory : DataFactoryBase
    {
        public static WritingAssistanceDAL WritingAssistanceDAL => new WritingAssistanceDAL();

        public static JobTitlesDAL JobTitleDAL => new JobTitlesDAL();

        public static ContentsDAL ContentDAL => new ContentsDAL();

        public static TipsDAL TipsDAL => new TipsDAL();
    }
}