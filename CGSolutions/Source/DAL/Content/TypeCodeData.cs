using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using CommonModules.Exceptions;
using DataAccess.Base;
using NHibernate;
using NHibernate.Criterion;
using TypeCode = DomainModel.Content.TypeCode;

namespace DataAccess.Content
{
    /// <summary>
    ///     TODO: Comments missing
    /// </summary>
    public class TypeCodeData : DataManagerBase<TypeCode, string>
    {
        //Add any custom code here. The .generated file should not be hand edited as the changes will get overwritten next time the file is generated from the DB
        //This file will not get regenerated

        /// <summary>
        ///     Returns list of TypeCode objects for a specified CodeGroup
        /// </summary>
        /// <param name="codeGroup">The code group.</param>
        /// <returns></returns>
        public IList<TypeCode> GetByCodeGroup(string codeGroup)
        {
            try
            {
                var criteria = GetNewCriteria();
                criteria.Add(Restrictions.Eq("CodeGroup", codeGroup));
                criteria.AddOrder(Order.Asc("Description"));
                criteria.SetCacheable(true);

                return criteria.List<TypeCode>();
            }
            catch (Exception ex)
            {
                var htErrorArgs = new Hashtable(1);
                htErrorArgs.Add("CodeGroup", codeGroup);
                var objMethod = (MethodInfo) MethodBase.GetCurrentMethod();
                GlobalExceptionHandler.customExceptionHandler(ex, objMethod, htErrorArgs, ErrorSeverityType.Critical);
            }

            return null;
        }


        /// <summary>
        ///     Returns list of TypeCode objects for a specified CodeGroup
        /// </summary>
        /// <param name="codeGroup">The code group.</param>
        /// <returns></returns>
        public IList<TypeCode> GetByCodeGroupSorted(string codeGroup)
        {
            try
            {
                var criteria = GetNewCriteria();
                criteria.Add(Restrictions.Eq("CodeGroup", codeGroup));
                criteria.AddOrder(Order.Asc("SortIndex"));

                criteria.SetCacheable(true);

                return criteria.List<TypeCode>();
            }
            catch (Exception ex)
            {
                var htErrorArgs = new Hashtable(1);
                htErrorArgs.Add("CodeGroup", codeGroup);
                var objMethod = (MethodInfo) MethodBase.GetCurrentMethod();
                GlobalExceptionHandler.customExceptionHandler(ex, objMethod, htErrorArgs, ErrorSeverityType.Critical);
            }

            return null;
        }

        public IList<TypeCode> GetByCodeGroup(List<string> lstCodeGroup)
        {
            try
            {
                var disjunctionCodeGroups = Restrictions.Disjunction();
                foreach (var codeGroup in lstCodeGroup)
                    disjunctionCodeGroups.Add(Restrictions.Eq("CodeGroup", codeGroup));
                var criteria = GetNewCriteria();
                criteria.Add(disjunctionCodeGroups);
                criteria.AddOrder(Order.Asc("SortIndex"));

                criteria.SetCacheable(true);

                return criteria.List<TypeCode>();
            }
            catch (Exception ex)
            {
                var htErrorArgs = new Hashtable(1);
                htErrorArgs.Add("listCodeGroup", lstCodeGroup);
                var objMethod = (MethodInfo) MethodBase.GetCurrentMethod();
                GlobalExceptionHandler.customExceptionHandler(ex, objMethod, htErrorArgs, ErrorSeverityType.Critical);
            }

            return null;
        }

        public IList<TypeCode> GetByTypeCodes(List<string> lstCodes)
        {
            try
            {
                var disjunctionCodes = Restrictions.Disjunction();
                foreach (var typecode in lstCodes)
                    disjunctionCodes
                        .Add(Restrictions.Eq("TypeCD", typecode));
                return GetNewCriteria()
                    .Add(disjunctionCodes)
                    .AddOrder(Order.Asc("SortIndex"))
                    .SetCacheable(true)
                    .List<TypeCode>();
            }
            catch (Exception ex)
            {
                var htErrorArgs = new Hashtable(1);
                htErrorArgs.Add("lstCodes", lstCodes);
                var objMethod = (MethodInfo) MethodBase.GetCurrentMethod();
                GlobalExceptionHandler.customExceptionHandler(ex, objMethod, htErrorArgs, ErrorSeverityType.Critical);
            }

            return null;
        }

        public string GetTypeCodeLabel(string codeGroup, string typeCD)
        {
            var label = typeCD;
            try
            {
                IList<TypeCode> lstTypeCode = null;
                if (lstTypeCode == null) lstTypeCode = GetByCodeGroup(codeGroup);

                if (lstTypeCode != null && lstTypeCode.Count > 0)
                {
                    var dmTC = lstTypeCode.FirstOrDefault(tc => tc.TypeCD == typeCD);
                    if (dmTC != null)
                        label = dmTC.Label;
                }
            }
            catch (Exception ex)
            {
                var htParams = new Hashtable(2);
                htParams.Add("codeGroup", codeGroup);
                htParams.Add("typeCD", typeCD);
                var objMethod = (MethodInfo) MethodBase.GetCurrentMethod();
                GlobalExceptionHandler.customExceptionHandler(ex, objMethod, htParams, ErrorSeverityType.Normal);
            }

            return label;
        }

        public TypeCode GetById(string typeCD, string codeGroup)
        {
            var criteria = Session.GetISession().CreateCriteria(typeof(TypeCode));

            criteria.Add(Restrictions.Eq("TypeCD", typeCD));
            criteria.Add(Restrictions.Eq("CodeGroup", codeGroup));

            var result = (TypeCode) criteria.UniqueResult();

            if (result == null)
                throw new ObjectDeletedException("", null, null);

            return result;
        }
    }
}