﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Reflection;
using BusinessLogic.Content;
using CommonModules.Exceptions;

namespace Services.Content
{
    public class ContentService
    {
        public IList<string> GetJobTitleSuggestions(string jobTitle)
        {
            try
            {
                return ContentSearchProcess.GetJobTitles(jobTitle);
            }
            catch (Exception ex)
            {
                var parameters = new Hashtable(1);
                parameters.Add("JobTitle", jobTitle);
                var method = (MethodInfo) MethodBase.GetCurrentMethod();
                GlobalExceptionHandler.customExceptionHandler(ex, method, parameters, ErrorSeverityType.Normal);
            }

            return null;
        }

        public static IList<string> GetExamples(int sectionId, string jobTitle)
        {
            try
            {
                return ContentSearchProcess.GetExamples(sectionId, jobTitle);
            }
            catch (Exception ex)
            {
                var htParams = new Hashtable(3);
                htParams.Add("JobTitle", jobTitle);
                htParams.Add("SectionID", sectionId);
                var objMethod = (MethodInfo) MethodBase.GetCurrentMethod();
                GlobalExceptionHandler.customExceptionHandler(ex, objMethod, htParams, ErrorSeverityType.Normal);
            }

            return null;
        }

        public static IList<string> GetGenericExamples(short sectionId)
        {
            try
            {
                return ContentSearchProcess.GetGenericExamples(sectionId);
            }
            catch (Exception ex)
            {
                var htParams = new Hashtable(1);
                htParams.Add("SectionID", sectionId);
                var objMethod = (MethodInfo) MethodBase.GetCurrentMethod();
                GlobalExceptionHandler.customExceptionHandler(ex, objMethod, htParams, ErrorSeverityType.Normal);
            }

            return null;
        }
    }
}