﻿using BusinessLogicProcess.Documents;
using CommonModules.Exceptions;
using DataAccess.Documents;
using DomainModel;
using Newtonsoft.Json;
using Resources;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Resources;
using Utilities.Constants.Documents;
using File = System.IO.File;

namespace Services.Documents
{
    public class DocumentService
    {
        private static ResourceManager resourceMgr = Resource.ResourceManager;
        public static Dictionary<string, List<Theme>> Themes;
        public static Dictionary<string, Skin> SkinMapping;
        public static Dictionary<string, int> SectionTypeIDs;

        static DocumentService()
        {
            //TOD:Move the theme loading part here and make the object themes static
            PopulateThemes();
            PopulateSkins();
            PopulateSectionType();
        }

        private static void PopulateSectionType()
        {
            var sectionTypes = DocumentFactory.SectionTypeDAL.GetAll();
            SectionTypeIDs = new Dictionary<string, int>();
            foreach (var section in sectionTypes) SectionTypeIDs.Add(section.SectionTypeCD, section.SectionTypeID);
        }

        /// <summary>
        /// Gets the list of skins with skinId and SkinCD
        /// </summary>
        private static void PopulateSkins()
        {
            SkinMapping = new Dictionary<string, Skin>();
            try
            {
                var skinDetails = DocumentFactory.SkinDAL.GetAllSkins();
                foreach (var skin in skinDetails)
                    if (skin != null)
                        SkinMapping.Add(skin.SkinCD, skin);
            }
            catch (Exception ex)
            {
                var objMethod = (MethodInfo)MethodBase.GetCurrentMethod();
                GlobalExceptionHandler.customExceptionHandler(ex, objMethod, null, ErrorSeverityType.Normal);
            }
        }


        /// <summary>
        /// This function basically is to get the color combination of themes
        /// TODO: Needs to be called only once and stored as static
        /// </summary>
        /// <param name="docTheme"></param>
        /// <param name="p"></param>
        /// <returns></returns>
        public static Dictionary<string, List<Theme>> PopulateThemes()
        {
            Themes = new Dictionary<string, List<Theme>>();
            //For now reading it everytime, later on this needs to be moved to static object
            var themesDataPath = AppDomain.CurrentDomain.BaseDirectory + "css/Themes.json";
            var json = File.ReadAllText(themesDataPath);
            //Styles
            IDictionary<string, object> newThemes = JsonConvert.DeserializeObject<Dictionary<string, object>>(json);

            foreach (var skinCD in newThemes.Keys)
            {                
                var themeData = Convert.ToString(newThemes[skinCD]);
                var skinThemes = new List<Theme>();
                var skinStyles = JsonConvert.DeserializeObject<List<Dictionary<string, string>>>(themeData);

                foreach (IDictionary<string, string> themeStyles in skinStyles)
                {
                    var skinTheme = new Theme();
                    skinTheme.ThemeCD = Convert.ToString(themeStyles["ThemeCD"]);
                    skinTheme.DisplayColor1 = Convert.ToString(themeStyles["DisplayColor1"]);
                    skinTheme.DisplayColor2 = Convert.ToString(themeStyles["DisplayColor2"]);
                    skinTheme.DisplayColor3 = Convert.ToString(themeStyles["DisplayColor3"]);

                    if (!string.IsNullOrEmpty(skinTheme.ThemeCD))
                    {
                        skinTheme.Styles.Add(Styles.BACKGROUNDSIDE, GetStyleValue(themeStyles, Styles.BACKGROUNDSIDE));
                        skinTheme.Styles.Add(Styles.BACKGROUNDPARENT, GetStyleValue(themeStyles, Styles.BACKGROUNDPARENT));
                        skinTheme.Styles.Add(Styles.BACKGROUNDADDRESS, GetStyleValue(themeStyles, Styles.BACKGROUNDADDRESS));                      
                        skinTheme.Styles.Add(Styles.BACKGROUNDFOOTER, GetStyleValue(themeStyles, Styles.BACKGROUNDFOOTER));
                        skinTheme.Styles.Add(Styles.BACKGROUNDYEARCITY, GetStyleValue(themeStyles, Styles.BACKGROUNDYEARCITY));
                        skinTheme.Styles.Add(Styles.COLORADDRESS, GetStyleValue(themeStyles, Styles.COLORADDRESS));
                        skinTheme.Styles.Add(Styles.COLORBORDER, GetStyleValue(themeStyles, Styles.COLORBORDER));
                        skinTheme.Styles.Add(Styles.COLORFOOTER, GetStyleValue(themeStyles, Styles.COLORFOOTER));
                        skinTheme.Styles.Add(Styles.COLORH1, GetStyleValue(themeStyles, Styles.COLORH1));
                        skinTheme.Styles.Add(Styles.COLORH2, GetStyleValue(themeStyles, Styles.COLORH2));
                        skinTheme.Styles.Add(Styles.COLORLASTNAME, GetStyleValue(themeStyles, Styles.COLORLASTNAME));
                        skinTheme.Styles.Add(Styles.COLORNAME, GetStyleValue(themeStyles, Styles.COLORNAME));
                        skinTheme.Styles.Add(Styles.COLORPARENT, GetStyleValue(themeStyles, Styles.COLORPARENT));
                        skinTheme.Styles.Add(Styles.COLORSIDE, GetStyleValue(themeStyles, Styles.COLORSIDE));
                        skinTheme.Styles.Add(Styles.COLORYEARCITY, GetStyleValue(themeStyles, Styles.COLORYEARCITY));
                        skinTheme.Styles.Add(Styles.COLORADDRESSICONS, GetStyleValue(themeStyles, Styles.COLORADDRESSICONS));
                        skinTheme.Styles.Add(Styles.TOPBOTTOMMARGINS, GetStyleValue(themeStyles, Styles.TOPBOTTOMMARGINS));
                        //Add the skinTheme to current theme
                        skinThemes.Add(skinTheme);
                    }
                }
                Themes.Add(skinCD, skinThemes);
            }

            return Themes;
        }

        private static string GetStyleValue(IDictionary<string, string> themeStyles, string styleKey)
        {
            string styleValue;
            themeStyles.TryGetValue(styleKey, out styleValue);
            return styleValue;
        }
        

        public void PopulateThemeStyles(Document document, IDictionary<string, string> styles)
        {
            if (styles[Styles.THEME] != null)
            {
                //TODO: Make the Themes to be static and call directly, done on temp basis to get fresh json every time
                var docTheme = DocumentService.PopulateThemes()[document.SkinCD]
                    .FirstOrDefault(x => x.ThemeCD == styles[Styles.THEME]);
                if (docTheme != null)
                    foreach (var style in docTheme.Styles)
                    {
                        string currentValue;
                        if (styles.TryGetValue(style.Key, out currentValue))
                            styles[style.Key] = style.Value;
                        else
                            styles.Add(style);
                    }
            }
        }


        public Document CreateDocument(Document document, string styleTypeCode = Styles.Default)
        {
            DocumentsProcess process = null;
            var docStyles = new List<DocStyle>();
            try
            {
                if (document.DocStyles == null)
                {
                    process = new DocumentsProcess();
                    DocumentFactory.DocumentDAL.Save(document, true);

                    docStyles = process.AddDefaultDocStyles(document.SkinCD, styleTypeCode, document.Id);
                    document.DocStyles = docStyles;
                    DocumentFactory.DocStyleDAL.SaveAll(docStyles);
                }

            }
            catch (Exception ex)
            {
                var htParams = new Hashtable(1);
                htParams.Add("document", document);
                var objMethod = (MethodInfo)MethodBase.GetCurrentMethod();
                GlobalExceptionHandler.customExceptionHandler(ex, objMethod, htParams, ErrorSeverityType.Normal);
            }
            return document;
        }

        public bool SortExprSection(Section expSection)
        {
            var status = false;
            try
            {
                var docProcess = new DocumentsProcess();
                docProcess.SortExperienceReverseChronological(expSection);
                status = true;
            }
            catch (Exception ex)
            {
                var htParams = new Hashtable(1);
                htParams.Add("Section", expSection != null ? expSection.ToString() : "<NULL>");
                var objMethod = (MethodInfo)MethodBase.GetCurrentMethod();
                GlobalExceptionHandler.customExceptionHandler(ex, objMethod, htParams, ErrorSeverityType.Normal);
            }
            return status;
        }

        public bool SortEducationSection(Section eduSection)
        {
            var status = false;
            try
            {
                var docProcess = new DocumentsProcess();
                docProcess.SortEducationReverseChronological(eduSection);
                status = true;
            }
            catch (Exception ex)
            {
                var htParams = new Hashtable(1);
                htParams.Add("DocId", eduSection.Document.Id);
                htParams.Add("SectionId", eduSection.Id);
                var objMethod = (MethodInfo)MethodBase.GetCurrentMethod();
                GlobalExceptionHandler.customExceptionHandler(ex, objMethod, htParams, ErrorSeverityType.Normal);
            }
            return status;
        }

        public Document UpdateDocument(Document document)
        {
            try
            {
                document.DateModified = DateTime.Now;
                DocumentFactory.DocumentDAL.SaveOrUpdate(document, true);
            }
            catch (Exception ex)
            {
                var htParams = new Hashtable(1);
                htParams.Add("document", document);
                var objMethod = (MethodInfo)MethodBase.GetCurrentMethod();
                GlobalExceptionHandler.customExceptionHandler(ex, objMethod, htParams, ErrorSeverityType.Normal);
            }

            return document;
        }

        public void DeleteDocument(Document document)
        {
            try
            {
                DocumentFactory.DocumentDAL.Delete(document, true);
            }
            catch (Exception ex)
            {
                var htParams = new Hashtable(1);
                htParams.Add("document", document);
                var objMethod = (MethodInfo)MethodBase.GetCurrentMethod();
                GlobalExceptionHandler.customExceptionHandler(ex, objMethod, htParams, ErrorSeverityType.Normal);
            }
        }

        public void DeleteDocumentByID(int DocumentID)
        {
            try
            {
                DocumentFactory.DocumentDAL.DeleteDocumentByID(DocumentID);
            }
            catch (Exception ex)
            {
                var htParams = new Hashtable(1);
                htParams.Add("DocumentID", DocumentID);
                var objMethod = (MethodInfo)MethodBase.GetCurrentMethod();
                GlobalExceptionHandler.customExceptionHandler(ex, objMethod, htParams, ErrorSeverityType.Normal);
            }
        }

        /// <summary>
        /// Get resume based on document id and validate against the current user 
        /// </summary>
        /// <param name="DocumentID"></param>
        /// <param name="UserID"></param>
        /// <returns></returns>
        public Document GetResumeDocByID(int DocumentID, int UserID)
        {
            Document doc = null;
            try
            {
                doc = DocumentFactory.DocumentDAL.GetDocumentByType(DocumentID, DocumentTypeCD.Resume);
                var isReqValid = IsDocumentRequestValid(doc, UserID);

                if (isReqValid)
                    return doc;
                return null;
            }
            catch (Exception ex)
            {
                var htParams = new Hashtable(2);
                htParams.Add("DocumentID", DocumentID);
                htParams.Add("UserID", UserID);
                var objMethod = (MethodInfo)MethodBase.GetCurrentMethod();
                GlobalExceptionHandler.customExceptionHandler(ex, objMethod, htParams, ErrorSeverityType.High);
            }

            return null;
        }

        /// <summary>
        /// Get letter based on document id and validate against the current user 
        /// </summary>
        /// <param name="DocumentID"></param>
        /// <param name="UserID"></param>
        /// <returns></returns>
        public Document GetLetterDocByID(int DocumentID, int UserID)
        {
            Document doc = null;
            try
            {
                doc = DocumentFactory.DocumentDAL.GetDocumentByType(DocumentID, DocumentTypeCD.Letter);
                var isReqValid = IsDocumentRequestValid(doc, UserID);

                if (isReqValid)
                    return doc;
                return null;
            }
            catch (Exception ex)
            {
                var htParams = new Hashtable(2);
                htParams.Add("DocumentID", DocumentID);
                htParams.Add("UserID", UserID);
                var objMethod = (MethodInfo)MethodBase.GetCurrentMethod();
                GlobalExceptionHandler.customExceptionHandler(ex, objMethod, htParams, ErrorSeverityType.High);
            }

            return null;
        }

        /// <summary>
        /// Get document based on document id and validate against the current user 
        /// </summary>
        /// <param name="DocumentID"></param>
        /// <param name="currentUserID">User ID of current user</param>
        /// <returns>Valid Document</returns>
        /// 
        public Document GetDocumentById(int DocumentID)
        {
            Document Doc = null;
            try
            {
                Doc = DocumentFactory.DocumentDAL.GetById(DocumentID);
                return Doc;
            }
            catch (Exception ex)
            {
                var htParams = new Hashtable(2);
                htParams.Add("RequestedDocumentID", DocumentID);

                var objMethod = (MethodInfo)MethodBase.GetCurrentMethod();
                GlobalExceptionHandler.customExceptionHandler(ex, objMethod, htParams, ErrorSeverityType.High);
            }

            return null;
        }

        public Document GetDocumentByID(int DocumentID, int UserID)
        {
            Document Doc = null;
            try
            {
                Doc = DocumentFactory.DocumentDAL.GetById(DocumentID);
                var isReqValid = IsDocumentRequestValid(Doc, UserID);

                if (isReqValid)
                    return Doc;
                return null;
            }
            catch (Exception ex)
            {
                var htParams = new Hashtable(2);
                htParams.Add("RequestedDocumentID", DocumentID);
                htParams.Add("UserID", UserID);
                var objMethod = (MethodInfo)MethodBase.GetCurrentMethod();
                GlobalExceptionHandler.customExceptionHandler(ex, objMethod, htParams, ErrorSeverityType.High);
            }

            return null;
        }

        /// <summary>
        /// Check mapping of requested Document with Current user
        /// </summary>
        /// <param name="docID">Requested Document ID</param>
        /// <param name="UserID">Current User ID</param>
        /// <returns></returns>
        public bool IsDocumentRequestValid(int docID, int currentUserID)
        {
            var document = DocumentFactory.DocumentDAL.GetById(docID);
            if (document != null && document.UserID.Equals(currentUserID)) return true;
            return false;
        }

        public bool IsDocumentRequestValid(Document document, int currentUserID)
        {
            return document != null && document.UserID.Equals(currentUserID);
        }

        /// <summary>
        /// Fetch all documents for a user
        /// </summary>
        /// <param name="userID">UserID</param>
        /// <returns></returns>
        public IList<Document> GetDocumentsByUserID(int userID)
        {
            try
            {
                return DocumentFactory.DocumentDAL.GetAll(userID);
            }
            catch (Exception ex)
            {
                var htParams = new Hashtable(1);
                htParams.Add("userID", userID);
                var objMethod = (MethodInfo)MethodBase.GetCurrentMethod();
                GlobalExceptionHandler.customExceptionHandler(ex, objMethod, htParams, ErrorSeverityType.High);
            }

            return null;
        }

        /// <summary>
        /// Get all documents for a user
        /// </summary>
        /// <param name="userID">UserID</param>
        /// <param name="bOnlyResumes">Set true to fetch only resumes</param>
        /// <returns></returns>
        public IList<Document> GetDocumentsByUserID(int userID, string docTypCd)
        {
            try
            {
                return DocumentFactory.DocumentDAL.GetAll(userID, docTypCd);
            }
            catch (Exception ex)
            {
                var htParams = new Hashtable(2);
                htParams.Add("UserID", userID);
                htParams.Add("DocumentTypeCD", docTypCd);
                var objMethod = (MethodInfo)MethodBase.GetCurrentMethod();
                GlobalExceptionHandler.customExceptionHandler(ex, objMethod, htParams, ErrorSeverityType.High);
            }

            return null;
        }

        public bool UpdateUserOfDocument(int DocID, int UserID)
        {
            Document doc = null;

            try
            {
                doc = DocumentFactory.DocumentDAL.GetById(DocID);
                if (doc != null)
                {
                    var objExitDoc = DocumentFactory.DocumentDAL.GetAll(UserID).FirstOrDefault(Res => Res.Name == doc.Name);
                    doc.UserID = UserID;
                    if (objExitDoc != null)
                        if (objExitDoc.Id != DocID) doc.Name = GetNewResumeName(UserID, doc.Name);
                    // Set DateModified for the Document
                    doc.DateModified = DateTime.Now;
                    DocumentFactory.DocumentDAL.SaveOrUpdate(doc, true);
                }
            }
            catch (Exception ex)
            {
                var htParams = new Hashtable(1);
                htParams.Add("document", doc);
                var objMethod = (MethodInfo)MethodBase.GetCurrentMethod();
                GlobalExceptionHandler.customExceptionHandler(ex, objMethod, htParams, ErrorSeverityType.Normal);
            }

            return true;
        }

        /// <summary>
        /// This method will get section based on its type.
        /// It is kept here in document service because document id is inaccessible in section.
        /// </summary>
        /// <param name="documentid"></param>
        /// <param name="sectioncode"></param>
        /// <returns></returns>
        public Section GetSectionByCode(int documentid, string sectioncode)
        {
            Section section = null;
            Document document = null;

            try
            {
                document = DocumentFactory.DocumentDAL.GetById(documentid);
                section = GetSectionByCode(document, sectioncode);
            }
            catch (Exception ex)
            {
                var htParams = new Hashtable(1);
                htParams.Add("SectionCD", sectioncode);
                htParams.Add("DocumentID", documentid);
                var objMethod = (MethodInfo)MethodBase.GetCurrentMethod();
                GlobalExceptionHandler.customExceptionHandler(ex, objMethod, htParams, ErrorSeverityType.Normal);
            }

            return section;
        }

        /// <summary>
        /// returns section from document
        /// </summary>
        /// <param name="document"></param>
        /// <param name="sectioncode"></param>
        /// <returns></returns>
        public Section GetSectionByCode(Document document, string sectioncode)
        {
            Section section = null;
            DocumentsProcess docProcess = null;

            try
            {
                docProcess = new DocumentsProcess();
                section = docProcess.GetSectionByCode(document, sectioncode);
            }
            catch (Exception ex)
            {
                var htParams = new Hashtable(2);
                htParams.Add("SectionCD", sectioncode);
                if (document != null)
                    htParams.Add("DocumentID", document.Id);
                var objMethod = (MethodInfo)MethodBase.GetCurrentMethod();
                GlobalExceptionHandler.customExceptionHandler(ex, objMethod, htParams, ErrorSeverityType.Normal);
            }

            return section;
        }

        public void UpdateStyle(int docId, string styleCD, string styleValue)
        {
            try
            {
                DocumentFactory.DocStyleDAL.UpdateDocStyle(docId, styleCD, styleValue);
            }
            catch (Exception ex)
            {
                var htParams = new Hashtable(1);
                htParams.Add("DocId", docId.ToString());
                htParams.Add("StyleCD", styleCD);
                htParams.Add("StyleValue", styleValue);
                var objMethod = (MethodInfo)MethodBase.GetCurrentMethod();
                GlobalExceptionHandler.customExceptionHandler(ex, objMethod, htParams, ErrorSeverityType.Normal);
            }
        }

        public void UpdateStyles(Document doc, IDictionary<string, string> styles)
        {
            try
            {
                if (doc != null)
                {
                    if (doc.DocStyles == null)
                        doc.DocStyles = new List<DocStyle>();

                    #region AddUpdateStyles
                    if (styles != null)
                        foreach (var newStyle in styles)
                        {
                            var style = doc.DocStyles.FirstOrDefault(s => s.StyleCD == newStyle.Key);
                            if (style != null)
                            {
                                style.Value = newStyle.Value;
                            }
                            else
                            {
                                style = new DocStyle();
                                style.DocumentID = doc.Id;
                                style.StyleCD = newStyle.Key;
                                style.Value = newStyle.Value;
                                doc.DocStyles.Add(style);
                            }
                        }

                    #endregion

                    doc.DateModified = DateTime.Now;
                    DocumentFactory.DocumentDAL.Update(doc, true);
                }
            }
            catch (Exception ex)
            {
                var htParams = new Hashtable(1);
                htParams.Add("Document", doc);
                var objMethod = (MethodInfo)MethodBase.GetCurrentMethod();
                GlobalExceptionHandler.customExceptionHandler(ex, objMethod, htParams, ErrorSeverityType.Normal);
            }
        }

        /// <summary>
        /// Updates document skincd to selected skincd and DocStyles to the default docstyles of new skin
        /// </summary>
        public List<SkinStyle> UpdateSkin(int docId, string skinCD)
        {
            List<SkinStyle> lstSkinStyles = null;
            try
            {

                if (docId > 0 && !string.IsNullOrEmpty(skinCD))
                {
                    DocumentFactory.DocumentDAL.UpdateDocumentSkin(docId, skinCD);
                    lstSkinStyles = UpdateDocStyleToDefaultSettings(docId, skinCD);
                }
            }
            catch (Exception ex)
            {

            }

            return lstSkinStyles;
        }

        public List<SkinStyle> UpdateDocSkin(int docId, string skinCD)
        {
            List<SkinStyle> lstSkinStyles = null;

            try
            {
                if (!string.IsNullOrEmpty(skinCD))
                {
                    //DocumentFactory.DocumentDAL.UpdateDocumentSkin(docId, sKinCD);}
                    lstSkinStyles = UpdateDocStyleToDefaultSettings(docId, skinCD);
                }
            }
            catch (Exception ex)
            {
                var htParams = new Hashtable(2);
                htParams.Add("DocId", docId.ToString());
                htParams.Add("SKinCD", skinCD);
                var objMethod = (MethodInfo)MethodBase.GetCurrentMethod();
                GlobalExceptionHandler.customExceptionHandler(ex, objMethod, htParams, ErrorSeverityType.Normal);
            }

            return lstSkinStyles;
        }

        public void UpdateDocumentDateModified(int docId)
        {
            try
            {
                DocumentFactory.DocumentDAL.UpdateDateModified(docId);
            }
            catch (Exception ex)
            {
                var htParams = new Hashtable(2);
                htParams.Add("DocId", docId.ToString());
                htParams.Add("DateModified", DateTime.Now.ToString());
                var objMethod = (MethodInfo)MethodBase.GetCurrentMethod();
                GlobalExceptionHandler.customExceptionHandler(ex, objMethod, htParams, ErrorSeverityType.Normal);
            }
        }

        public int GetResumeCountByUserID(int userId)
        {
            try
            {
                var resumeCount = DocumentFactory.DocumentDAL.GetUserDocumentCount(userId, DocumentTypeCD.Resume);
                return resumeCount;
            }
            catch (Exception ex)
            {
                var htParams = new Hashtable(1);
                htParams.Add("DocumentID", userId);
                var objMethod = (MethodInfo)MethodBase.GetCurrentMethod();
                GlobalExceptionHandler.customExceptionHandler(ex, objMethod, htParams, ErrorSeverityType.High);
            }

            return 0;
        }

        /// <summary>
        /// Get last created resume id for user
        /// </summary>
        /// <param name="UserID"></param>
        /// <returns>int</returns>
        public int GetLatestResumeIdByUserID(int UserID)
        {
            var latestDocumentId = 0;
            try
            {
                latestDocumentId = DocumentFactory.DocumentDAL.GetLatestDocumentIdByUserId(UserID, DocumentTypeCD.Resume);
            }
            catch (Exception ex)
            {
                var htParams = new Hashtable(1);
                htParams.Add("UserId", UserID);
                var objMethod = (MethodInfo)MethodBase.GetCurrentMethod();
                GlobalExceptionHandler.customExceptionHandler(ex, objMethod, htParams, ErrorSeverityType.High);
            }
            return latestDocumentId;
        }

        public int GetUserIDByDocumentID(int DocumentID)
        {
            var iUsr = 0;
            Document Doc = null;

            try
            {
                Doc = DocumentFactory.DocumentDAL.GetById(DocumentID);
                if (Doc != null)
                    iUsr = Doc.UserID;
            }
            catch (Exception ex)
            {
                var htParams = new Hashtable(1);
                htParams.Add("DocumentID", DocumentID);
                var objMethod = (MethodInfo)MethodBase.GetCurrentMethod();
                GlobalExceptionHandler.customExceptionHandler(ex, objMethod, htParams, ErrorSeverityType.High);
            }

            return iUsr;
        }

        public void UpdateDocStylewithSkin(string docId, string styleGroupCD, string styleGroupValue, string skinCD)
        {
            var DocID = 0;
            DocStyle dsty = null;
            IList<SkinStyle> lstSkinStyles = null;
            Document doc = null;

            try
            {
                int.TryParse(docId, out DocID);
                if (DocID > 0)
                {
                    doc = DocumentFactory.DocumentDAL.GetById(DocID);
                    if (doc != null)
                        if (skinCD.ToLower() == "undefined") skinCD = doc.SkinCD;
                    lstSkinStyles = DocumentsProcess.GetAllSkinStyles.Where(s => s.SkinCD == skinCD && s.StyleGroupCD == styleGroupCD && s.StyleGroupvalue == styleGroupValue).ToList();

                    foreach (var item in lstSkinStyles)
                    {
                        dsty = DocumentFactory.DocStyleDAL.GetDocStyleByDocument(DocID, item.StyleCD);

                        if (dsty != null)
                        {
                            dsty.Value = item.Value;
                            DocumentFactory.DocStyleDAL.SaveOrUpdate(dsty, true);
                        }
                        else
                        {
                            dsty = new DocStyle();
                            dsty.DocumentID = DocID;
                            dsty.StyleCD = item.StyleCD;
                            dsty.Value = item.Value;
                            DocumentFactory.DocStyleDAL.Save(dsty, true);
                        }
                    }

                    //Save Doc Style Group Values
                    dsty = DocumentFactory.DocStyleDAL.GetDocStyleByDocument(DocID, styleGroupCD);

                    if (dsty != null)
                    {
                        dsty.Value = styleGroupValue;
                        DocumentFactory.DocStyleDAL.SaveOrUpdate(dsty, true);
                    }
                    else
                    {
                        dsty = new DocStyle();
                        dsty.DocumentID = DocID;
                        dsty.StyleCD = styleGroupCD;
                        dsty.Value = styleGroupValue;
                        DocumentFactory.DocStyleDAL.Save(dsty, true);
                    }

                    // Set DateModified for the Document
                    UpdateDocumentDateModified(DocID);
                }
            }
            catch (Exception ex)
            {
                var htParams = new Hashtable(4);
                htParams.Add("DocId", docId);
                htParams.Add("StyleCD", styleGroupCD);
                htParams.Add("StyleValue", styleGroupValue);
                htParams.Add("SkinCode", skinCD);
                var objMethod = (MethodInfo)MethodBase.GetCurrentMethod();
                GlobalExceptionHandler.customExceptionHandler(ex, objMethod, htParams, ErrorSeverityType.Normal);
            }
        }

        public string GetNewResumeName(int userId, string resumeName)
        {
            var objResponseDocCheckResult = false;
            var sNewName = resumeName;
            IList<Document> lstdoc = null;
            try
            {
                objResponseDocCheckResult = DocumentFactory.DocumentDAL.CheckResumeName(userId, resumeName);
                if (objResponseDocCheckResult)
                {
                    lstdoc = DocumentFactory.DocumentDAL.GetAll(userId);

                    //Check if there is already a Resume with this name for this User
                    var ExistingResume = new Document();

                    //Since there is already a Resume with this name, we need to find another Name
                    short count = 1;
                    var CharIndex = 0;
                    while (ExistingResume != default(Document))
                    {
                        CharIndex = sNewName.LastIndexOf(" ");
                        sNewName = sNewName.Substring(0, CharIndex + 1) + count;

                        //Check if there is already a Resume with this new name for this User
                        ExistingResume = lstdoc.FirstOrDefault(Res => Res.Name == sNewName);

                        //Increment the Resume Counter
                        count++;
                    }
                }
            }
            catch (Exception ex)
            {
                var htParams = new Hashtable(2);
                htParams.Add("UserID", userId.ToString());
                htParams.Add("ResumeName", resumeName);
                var objMethod = (MethodInfo)MethodBase.GetCurrentMethod();
                GlobalExceptionHandler.customExceptionHandler(ex, objMethod, htParams, ErrorSeverityType.Normal);
            }

            return sNewName;
        }

        /// <summary>
        /// Check and add missing default sections in document.
        /// default sections are contact, name, experience, skills, eduction, summary
        /// </summary>
        /// <param name="Doc"></param>
        public void CheckAndAddMissingSectionInResume(Document Doc)
        {
            try
            {
                // if default sections are not added already 
                if (Doc != null && !Doc.IsDefSec && DocumentTypeCD.Resume.Equals(Doc.DocumentTypeCD, StringComparison.OrdinalIgnoreCase))
                {
                    //Name
                    if (CheckSection(Doc, SectionTypeCD.Name)) AddNewNameSection(Doc, "", "");

                    //Contact
                    if (CheckSection(Doc, SectionTypeCD.Contact)) AddNewContactSection(Doc, "", "", "", "", "", "", "");

                    //Summary
                    if (CheckSection(Doc, SectionTypeCD.Summary)) AddNewSummarySection(Doc, "");

                    //Experience
                    if (CheckSection(Doc, SectionTypeCD.Experience)) AddNewExperienceSection(Doc, "", "", "", "", "", "");

                    //Education
                    if (CheckSection(Doc, SectionTypeCD.Education)) AddNewEducationSection(Doc, "", "", "", "", "", "", "");

                    //Skills
                    if (CheckSection(Doc, SectionTypeCD.Highlights)) AddNewSkillsSection(Doc, "", "");

                    //if (defSecAdded)
                    Doc.IsDefSec = true;
                    Doc.DateModified = DateTime.Now;
                    DocumentFactory.DocumentDAL.Update(Doc, true);
                }
            }
            catch (Exception ex)
            {
                var htParams = new Hashtable(1);
                htParams.Add("DocumentID", Doc.Id);
                var objMethod = (MethodInfo)MethodBase.GetCurrentMethod();
                GlobalExceptionHandler.customExceptionHandler(ex, objMethod, htParams, ErrorSeverityType.High);
            }
        }
        
        /// <summary>
        /// 
        /// </summary>
        /// <param name="DocumentID"></param>
        public void SetSortIndexOfSectionForDeaultSections(Document Doc)
        {
            short inSecionIndex = 0;

            try
            {
                foreach (var objSection in Doc.Sections)
                {
                    switch (objSection.SectionTypeCD)
                    {
                        case SectionTypeCD.Name:
                            objSection.SortIndex = 1;
                            break;
                        case SectionTypeCD.Contact:
                            objSection.SortIndex = 1;
                            break;
                        case SectionTypeCD.Summary:
                            objSection.SortIndex = 2;
                            break;
                        case SectionTypeCD.Skills:
                            objSection.SortIndex = 3;
                            break;
                        case SectionTypeCD.Experience:
                            objSection.SortIndex = 4;
                            break;
                        case SectionTypeCD.Education:
                            objSection.SortIndex = 5;
                            break;
                        default:
                            objSection.SortIndex = Convert.ToInt16(inSecionIndex + 1);
                            break;
                    }
                    inSecionIndex++;
                }
                DocumentFactory.DocumentDAL.Update(Doc, true);
            }
            catch (Exception ex)
            {
                var htParams = new Hashtable(1);
                htParams.Add("DocumentID", Doc.Id);
                var objMethod = (MethodInfo)MethodBase.GetCurrentMethod();
                GlobalExceptionHandler.customExceptionHandler(ex, objMethod, htParams, ErrorSeverityType.High);
            }
        }


        private bool CheckSection(Document Doc, string sSectionTypeCD)
        {
            Section section = null;
            section = Doc.Sections.FirstOrDefault(s => s.SectionTypeCD == sSectionTypeCD);

            if (section != default(Section)) return false;
            return true;
        }

        private void AddNewNameSection(Document doc, string firstname, string lastname)
        {
            var para = new Paragraph();

            var sName = new Section(Resource.NameCaps);
            sName.DocZoneTypeCD = DocZoneTypeCD.HEAD;
            sName.SectionTypeCD = SectionTypeCD.Name;
            para.Section = sName;
            para.SortIndex = 0;
            sName.Paragraphs.Add(para);

            if (!string.IsNullOrEmpty(firstname)) AddNewDocData(firstname, para, FieldTypeCD.FirstName);

            if (!string.IsNullOrEmpty(lastname)) AddNewDocData(lastname, para, FieldTypeCD.LastName);
            sName.SortIndex = 1;
            doc.AddSection(sName);
        }

        private void AddNewContactSection(Document doc, string email, string address, string city, string state, string zip, string homephone, string cellphone)
        {
            var para = new Paragraph();
            var sContact = new Section(Resource.CNTC);
            sContact.DocZoneTypeCD = DocZoneTypeCD.HEAD;
            sContact.SectionTypeCD = SectionTypeCD.Contact;
            para = new Paragraph();
            para.Section = sContact;
            para.SortIndex = 0;
            sContact.Paragraphs.Add(para);

            if (!string.IsNullOrEmpty(email)) AddNewDocData(email, para, FieldTypeCD.Email);

            if (!string.IsNullOrEmpty(address)) AddNewDocData(address, para, FieldTypeCD.Street);
            if (!string.IsNullOrEmpty(city)) AddNewDocData(city, para, FieldTypeCD.City);
            if (!string.IsNullOrEmpty(state)) AddNewDocData(state, para, FieldTypeCD.State);
            if (!string.IsNullOrEmpty(zip)) AddNewDocData(zip, para, FieldTypeCD.ZipCode);

            if (!string.IsNullOrEmpty(homephone)) AddNewDocData(homephone, para, FieldTypeCD.HomePhone);

            if (!string.IsNullOrEmpty(cellphone)) AddNewDocData(cellphone, para, FieldTypeCD.CellPhone);
            sContact.SortIndex = 1;
            doc.AddSection(sContact);
        }

        private void AddNewSummarySection(Document doc, string summary)
        {

            var para = new Paragraph();
            var sSummary = new Section(Resource.ProfessionalSummary);
            sSummary.DocZoneTypeCD = DocZoneTypeCD.HEAD;
            sSummary.SectionTypeCD = SectionTypeCD.Summary;
            para.Section = sSummary;
            para.SortIndex = 0;
            sSummary.Paragraphs.Add(para);

            if (!string.IsNullOrEmpty(summary)) AddNewDocData(summary, para, FieldTypeCD.FreeFormat);
            sSummary.SortIndex = 2;
            doc.AddSection(sSummary);
        }

        private void AddNewSkillsSection(Document doc, string skill1, string skill2)
        {
            //DocData dd = null;
            var para = new Paragraph();

            var sSec = new Section(Resource.HILT);
            sSec.DocZoneTypeCD = DocZoneTypeCD.HEAD;
            sSec.SectionTypeCD = SectionTypeCD.Highlights;
            para.Section = sSec;
            para.SortIndex = 0;
            sSec.Paragraphs.Add(para);
            sSec.SortIndex = 3;
            if (!string.IsNullOrEmpty(skill1.Trim())) AddNewDocData(skill1, para, FieldTypeCD.SkillsCollection_1);
            if (!string.IsNullOrEmpty(skill2.Trim())) AddNewDocData(skill2, para, FieldTypeCD.SkillsCollection_2);
            sSec.SortIndex = 3;
            doc.AddSection(sSec);
        }

        private void AddNewExperienceSection(Document doc, string companyName, string jobCity, string jobState, string jobTitle, string jobDateFrom, string jobDateTo)
        {
            var para = new Paragraph();
            var sSec = new Section(Resource.EXPR);
            sSec.DocZoneTypeCD = DocZoneTypeCD.HEAD;
            sSec.SectionTypeCD = SectionTypeCD.Experience;

            para.Section = sSec;
            para.SortIndex = 0;
            sSec.Paragraphs.Add(para);
            sSec.SortIndex = 4;

            try
            {
                para = new Paragraph();
                para.SortIndex = 0;

                #region ADDING DOCDATA FOR NEW EXPERIENCE PARA
                if (!string.IsNullOrEmpty(companyName))
                    AddNewDocData(companyName, para, FieldTypeCD.Company);

                if (!string.IsNullOrEmpty(jobCity))
                    AddNewDocData(jobCity, para, FieldTypeCD.JobCity);

                if (!string.IsNullOrEmpty(jobState))
                    AddNewDocData(jobState, para, FieldTypeCD.JobState);

                if (!string.IsNullOrEmpty(jobTitle))
                    AddNewDocData(jobTitle, para, FieldTypeCD.JobTitle);

                if (!string.IsNullOrEmpty(jobDateFrom))
                    AddNewDocData(jobDateFrom, para, FieldTypeCD.JobStartDate);

                if (!string.IsNullOrEmpty(jobDateTo))
                    AddNewDocData(jobDateTo, para, FieldTypeCD.JobEndDate);

                #endregion

                doc.AddSection(sSec);
            }
            catch (Exception ex)
            {
                throw ex;
            }


        }

        private void AddNewEducationSection(Document doc, string schoolName, string schoolCity, string schoolState, string schooldDegree, string fieldOfStudy, string schoolDateFrom, string schoolDateTo)
        {
            var para = new Paragraph();
            var sSec = new Section(Resource.EDUC);
            sSec.DocZoneTypeCD = DocZoneTypeCD.HEAD;
            sSec.SectionTypeCD = SectionTypeCD.Education;

            para.Section = sSec;
            para.SortIndex = 0;
            sSec.Paragraphs.Add(para);
            sSec.SortIndex = 5;

            try
            {
                para = new Paragraph();
                para.SortIndex = 0;

                #region ADDING DOCDATA FOR NEW EDUCATION PARA
                if (!string.IsNullOrEmpty(schoolName))
                    AddNewDocData(schoolName, para, FieldTypeCD.SchoolName);

                if (!string.IsNullOrEmpty(schoolCity))
                    AddNewDocData(schoolCity, para, FieldTypeCD.SchoolCity);

                if (!string.IsNullOrEmpty(schoolState))
                    AddNewDocData(schoolState, para, FieldTypeCD.SchoolState);

                if (!string.IsNullOrEmpty(schooldDegree))
                    AddNewDocData(schooldDegree, para, FieldTypeCD.DegreeEarned);

                if (!string.IsNullOrEmpty(fieldOfStudy))
                    AddNewDocData(fieldOfStudy, para, FieldTypeCD.FieldOfExpertise);

                if (!string.IsNullOrEmpty(schoolDateFrom))
                    AddNewDocData(schoolDateFrom, para, FieldTypeCD.FromDate);

                if (!string.IsNullOrEmpty(schoolDateTo))
                    AddNewDocData(schoolDateTo, para, FieldTypeCD.GraduationYear);

                #endregion

                doc.AddSection(sSec);
            }
            catch (Exception ex)
            {
                throw ex;
            }

        }

        private DocData AddNewDocData(string svalue, Paragraph para, string sFieldCD)
        {
            DocData dd = null;

            if (!string.IsNullOrEmpty(svalue))
            {
                dd = new DocData();
                dd.FieldCD = sFieldCD;
                dd.CharValue = svalue;
                dd.Paragraph = para;
                para.DocData.Add(dd);
            }
            return dd;
        }
        
        public int CopyDocument(int documentID, int userID, string DocumentName)
        {
            var newDocumentID = -1;
            DocPreference docPrefTemplateCategory = null;
            DocPreference docPrefTemplateCD = null;
            Document document = null;

            try
            {
                newDocumentID = DocumentFactory.DocumentDAL.CopyDocument(documentID, userID, DocumentName);
                if (newDocumentID > 0)
                {
                    DocumentFactory.DocPreferenceDAL.Add(newDocumentID, DocPreferenceTypeCD.DocumentCopied, documentID.ToString());

                    #region Copy Cover Letter DocPreferences
                    document = DocumentFactory.DocumentDAL.GetById(newDocumentID);
                    if (document != null && document.DocumentTypeCD == DocumentTypeCD.Letter)
                    {
                        docPrefTemplateCategory = DocumentFactory.DocPreferenceDAL.GetByPreferenceType(documentID, DocPreferenceTypeCD.CoverLetterTemplateCategoryTypeCD);
                        docPrefTemplateCD = DocumentFactory.DocPreferenceDAL.GetByPreferenceType(documentID, DocPreferenceTypeCD.CoverLetterTemplateCD);

                        if (docPrefTemplateCategory != null)
                            DocumentFactory.DocPreferenceDAL.Save(new DocPreference(newDocumentID, DocPreferenceTypeCD.CoverLetterTemplateCategoryTypeCD, docPrefTemplateCategory.Value), true);

                        if (docPrefTemplateCD != null)
                            DocumentFactory.DocPreferenceDAL.Save(new DocPreference(newDocumentID, DocPreferenceTypeCD.CoverLetterTemplateCD, docPrefTemplateCD.Value), true);
                    }
                    #endregion
                }
            }
            catch (Exception ex)
            {
                var htParams = new Hashtable(4);
                htParams.Add("DocumentID", documentID);
                htParams.Add("UserID", userID);
                htParams.Add("NewDocumentID", newDocumentID);
                htParams.Add("NewDocumentTypeCD", document != null ? document.DocumentTypeCD : "<NULL>");
                var objMethod = (MethodInfo)MethodBase.GetCurrentMethod();
                GlobalExceptionHandler.customExceptionHandler(ex, objMethod, htParams, ErrorSeverityType.High);
            }

            return newDocumentID;
        }

        /// <summary>
        /// Reset the doc style values for a document to Skin's default settings
        /// </summary>
        /// <param name="documentID"></param>
        /// <param name="skinCD"></param>
        /// <param name="styleTypeCd"></param>
        /// <returns>Default SkinStyles</returns>
        public List<SkinStyle> UpdateDocStyleToDefaultSettings(int documentID, string skinCD, string styleTypeCd = Styles.Default)
        {
            DocumentsProcess docProcess = null;
            IList<SkinStyle> lstSkinStyles = null;

            try
            {
                docProcess = new DocumentsProcess();
                var document = GetDocumentById(documentID);
                document.SkinCD = skinCD;
                IDictionary<string, string> styles = new Dictionary<string, string> { {Styles.THEME, ThemeCD.Default } };
                PopulateThemeStyles(document, styles);
                UpdateStyles(document, styles);
                lstSkinStyles = docProcess.UpdateDocStyleToDefaultSettings(documentID, skinCD, styleTypeCd);
            }
            catch (Exception ex)
            {
                var htParams = new Hashtable(3);
                htParams.Add("DocumentID", documentID.ToString());
                htParams.Add("SkinCD", skinCD);
                var objMethod = (MethodInfo)MethodBase.GetCurrentMethod();
                GlobalExceptionHandler.customExceptionHandler(ex, objMethod, htParams, ErrorSeverityType.High);
            }
            return lstSkinStyles.ToList();
        }

        public short RenameDocument(int DocID, string NewDocName)
        {
            //-1: Fail
            //1: Sucessfull (Document is Renamed)
            //0: A doc With This Name already exists
            short IsSucessfull = -1;
            var DocName = string.Empty;
            Document document = null;

            try
            {
                IsSucessfull = CheckDocumentName(DocID, NewDocName);
                if (IsSucessfull == 1)
                {
                    document = DocumentFactory.DocumentDAL.GetById(DocID);
                    if (document != null)
                    {
                        document.Name = NewDocName;
                        document.DateModified = DateTime.Now;
                        DocumentFactory.DocumentDAL.Update(document, true);
                    }
                }
            }
            catch (Exception ex)
            {
                var htParams = new Hashtable(2);
                htParams.Add("DocID", DocID);
                htParams.Add("NewDocName", NewDocName);
                var objMethod = (MethodInfo)MethodBase.GetCurrentMethod();
                GlobalExceptionHandler.customExceptionHandler(ex, objMethod, htParams, ErrorSeverityType.Normal);
            }
            return IsSucessfull;
        }

        public short CheckDocumentName(int DocID, string NewDocName)
        {
            //-1: Fail
            //1: Sucessfull (Document is Renamed)
            //0: A doc With This Name already exists
            short IsSucessfull = -1;
            var DocName = string.Empty;
            Document document = null;

            try
            {
                if (DocID == 0) return IsSucessfull;

                document = DocumentFactory.DocumentDAL.GetById(DocID);
                if (document != null)
                {
                    IList<Document> lst = null;
                    lst = DocumentFactory.DocumentDAL.GetAll(document.UserID, document.DocumentTypeCD);

                    DocName = document.Name;

                    lst = lst.Where(x => x.Name.Trim().ToUpper() == NewDocName.Trim().ToUpper()).ToList();

                    if (lst != null)
                    {
                        if (lst.Count < 1)
                            IsSucessfull = 1;
                        else
                            IsSucessfull = 0;
                    }
                }
            }
            catch (Exception ex)
            {
                var htParams = new Hashtable(2);
                htParams.Add("DocID", DocID);
                htParams.Add("NewDocName", NewDocName);
                var objMethod = (MethodInfo)MethodBase.GetCurrentMethod();
                GlobalExceptionHandler.customExceptionHandler(ex, objMethod, htParams, ErrorSeverityType.Normal);
            }
            return IsSucessfull;
        }
    }
}
