﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using CommonModules.Exceptions;
using DataAccess.Core;
using DomainModel.Tests;
using DomainModel.User;

namespace Servicess.Tests
{
    /// <summary>
    ///     Service class for ABTest service
    /// </summary>
    public class ABTestService
    {
        /// <summary>
        ///     Conduct ABTest for User
        /// </summary>
        /// <param name="UserID"></param>
        /// <param name="ABTestID"></param>
        /// <returns></returns>
        public int? ConductABTest(int UserID, int ABTestID)
        {
            int? val = null;
            try
            {
                val = ABTestDataFactory.ABTestDataDAL.ConductABTest(ABTestID, UserID);
            }
            catch (Exception Ex)
            {
                var htParam = new Hashtable();
                htParam.Add("ABTestID", ABTestID);
                htParam.Add("UserID", UserID);
                var objMethod = (MethodInfo) MethodBase.GetCurrentMethod();
                GlobalExceptionHandler.customExceptionHandler(Ex, objMethod, htParam, ErrorSeverityType.Normal);
            }

            return val;
        }

        /// <summary>
        ///     Get CaseIndex for User by ABTestID
        /// </summary>
        /// <param name="UserID"></param>
        /// <param name="ABTestID"></param>
        /// <returns></returns>
        public int? GetABTestCaseIndexByABTestIDAndUserID(int UserID, int ABTestID)
        {
            //case index
            int? Result = null;
            try
            {
                if (IsABTestActive(ABTestID))
                    Result = ABTestDataFactory.ABTestDataDAL.GetABTestCaseIndex(UserID, ABTestID);
            }
            catch (Exception Ex)
            {
                var objMethod = (MethodInfo) MethodBase.GetCurrentMethod();
                GlobalExceptionHandler.customExceptionHandler(Ex, objMethod, null, ErrorSeverityType.Normal);
            }

            return Result;
        }

        private int? SaveABTestData(int ABTestID, int UserID)
        {
            int? savedCaseIndex = null;
            try
            {
                // get new case index
                var NewCaseIndex = GetNewCaseIndex(ABTestID);
                if (NewCaseIndex > 0)
                {
                    // Save only if case index is > 0
                    var abTestData = new ABTestData();
                    abTestData.UserID = UserID;
                    abTestData.ABTestID = ABTestID;
                    abTestData.CaseIndex = NewCaseIndex;
                    abTestData.Data = null;
                    abTestData.TimeStamp = DateTime.Now;
                    ABTestDataFactory.ABTestDataDAL.Save(abTestData);

                    savedCaseIndex = NewCaseIndex;
                }
            }
            catch (Exception Ex)
            {
                var objMethod = (MethodInfo) MethodBase.GetCurrentMethod();
                GlobalExceptionHandler.customExceptionHandler(Ex, objMethod, null, ErrorSeverityType.Normal);
            }

            return savedCaseIndex;
        }

        /// <summary>
        ///     Determines whether [is AB test active] [the specified AB test Id].
        /// </summary>
        /// <param name="ABTestID">The AB test Id.</param>
        /// <returns>
        ///     <c>true</c> if [is AB test active] [the specified AB test Id]; otherwise, <c>false</c>.
        /// </returns>
        public bool IsABTestActive(int ABTestID)
        {
            var IsActive = false;
            try
            {
                var abtest = ABTestDataFactory.ABTestDAL.GetById(ABTestID);
                if (abtest != null)
                    if (abtest.Id > 0)
                        IsActive = abtest.IsActive;
            }
            catch (Exception Ex)
            {
                var objMethod = (MethodInfo) MethodBase.GetCurrentMethod();
                GlobalExceptionHandler.customExceptionHandler(Ex, objMethod, null, ErrorSeverityType.Normal);
            }

            return IsActive;
        }

        private int RandomNumber(int TotalWeight)
        {
            var RandomNumber = 0;
            try
            {
                var random = new Random();
                //RandomNumber = (int)(TotalWeight * random.Next(TotalWeight)) + 1;
                RandomNumber = random.Next(TotalWeight) + 1;
            }
            catch (Exception Ex)
            {
                var objMethod = (MethodInfo) MethodBase.GetCurrentMethod();
                GlobalExceptionHandler.customExceptionHandler(Ex, objMethod, null, ErrorSeverityType.Normal);
            }

            return RandomNumber;
        }


        /// <summary>
        ///     Returns  Case index for AbtestID
        /// </summary>
        /// <param name="TotalWeight"></param>
        /// <param name="ABTestID"></param>
        /// <returns></returns>
        private int GetNewCaseIndex(int ABTestID)
        {
            var CaseIndex = 0;

            try
            {
                var lst = ABTestDataFactory.ABTestCaseDAL.GetABTestCasesByID(ABTestID);
                if (lst != null && lst.Count > 0)
                {
                    var TotalWeight = lst.Where(c => c.Weight.HasValue).Sum(c => Convert.ToInt32(c.Weight.Value));
                    if (TotalWeight == 0)
                        return CaseIndex;
                    var j = RandomNumber(TotalWeight);
                    var k = 0;

                    foreach (var abtestcase in lst.Where(c => c.Weight.HasValue))
                    {
                        if (j <= abtestcase.Weight.Value + k)
                        {
                            CaseIndex = abtestcase.CaseIndex;
                            break;
                        }

                        k += abtestcase.Weight.Value;
                    }
                }
            }
            catch (Exception Ex)
            {
                var objMethod = (MethodInfo) MethodBase.GetCurrentMethod();
                GlobalExceptionHandler.customExceptionHandler(Ex, objMethod, null, ErrorSeverityType.Normal);
            }

            return CaseIndex;
        }

        private int GetCaseIndex(int ABTestID)
        {
            var CaseIndex = 0;

            try
            {
                var lst = ABTestDataFactory.ABTestCaseDAL.GetABTestCasesByID(ABTestID);
            }
            catch (Exception Ex)
            {
                var objMethod = (MethodInfo) MethodBase.GetCurrentMethod();
                GlobalExceptionHandler.customExceptionHandler(Ex, objMethod, null, ErrorSeverityType.Normal);
            }

            return CaseIndex;
        }

        /// <summary>
        ///     Returns ABTest
        /// </summary>
        /// <param name="ABTestId"></param>
        /// <returns></returns>
        public ABTest GetABTestByID(int ABTestId)
        {
            ABTest abTest = null;
            try
            {
                abTest = ABTestDataFactory.ABTestDAL.GetById(ABTestId);
            }
            catch (Exception Ex)
            {
                var objMethod = (MethodInfo) MethodBase.GetCurrentMethod();
                GlobalExceptionHandler.customExceptionHandler(Ex, objMethod, null, ErrorSeverityType.Normal);
            }

            return abTest;
        }

        public IList<ABTest> GetABTestAll()
        {
            IList<ABTest> lstABTest = null;
            try
            {
                lstABTest = ABTestDataFactory.ABTestDAL.GetAll();
            }
            catch (Exception Ex)
            {
                var objMethod = (MethodInfo) MethodBase.GetCurrentMethod();
                GlobalExceptionHandler.customExceptionHandler(Ex, objMethod, null, ErrorSeverityType.Normal);
            }

            return lstABTest;
        }

        public void AddABTest(string sName, string sDescription, bool bIsActive)
        {
            try
            {
                var abTest = new ABTest();

                abTest.Name = sName;
                abTest.Description = sDescription;
                abTest.IsActive = bIsActive;
                ABTestDataFactory.ABTestDAL.Save(abTest, true);
            }
            catch (Exception Ex)
            {
                var objMethod = (MethodInfo) MethodBase.GetCurrentMethod();
                GlobalExceptionHandler.customExceptionHandler(Ex, objMethod, null, ErrorSeverityType.Normal);
            }
        }

        public void UpdateABTest(ABTest abTest)
        {
            try
            {
                ABTestDataFactory.ABTestDAL.Update(abTest, true);
            }
            catch (Exception Ex)
            {
                var objMethod = (MethodInfo) MethodBase.GetCurrentMethod();
                GlobalExceptionHandler.customExceptionHandler(Ex, objMethod, null, ErrorSeverityType.Normal);
            }
        }

        public void UpdateABTest(int iAbTestId, string sName, string sDescription, bool bIsActive)
        {
            try
            {
                var abTest = new ABTest();

                abTest.Id = iAbTestId;
                abTest.Name = sName;
                abTest.Description = sDescription;
                abTest.IsActive = bIsActive;
                ABTestDataFactory.ABTestDAL.Update(abTest, true);
            }
            catch (Exception Ex)
            {
                var objMethod = (MethodInfo) MethodBase.GetCurrentMethod();
                GlobalExceptionHandler.customExceptionHandler(Ex, objMethod, null, ErrorSeverityType.Normal);
            }
        }

        public IList<ABTestCase> GetABTestCasesByABTestID(int ABTestId)
        {
            IList<ABTestCase> lstABTestCase = null;
            try
            {
                lstABTestCase = ABTestDataFactory.ABTestCaseDAL.GetABTestCasesByABTestID(ABTestId);
            }
            catch (Exception Ex)
            {
                var objMethod = (MethodInfo) MethodBase.GetCurrentMethod();
                GlobalExceptionHandler.customExceptionHandler(Ex, objMethod, null, ErrorSeverityType.Normal);
            }

            return lstABTestCase;
        }

        public ABTestCase GetABTestCasesByABTestID_CaseIndex(int ABTestId, int CaseIndex)
        {
            ABTestCase oABTestCase = null;
            try
            {
                oABTestCase = ABTestDataFactory.ABTestCaseDAL.GetABTestCasesByABTestID_CaseIndex(ABTestId, CaseIndex);
            }
            catch (Exception Ex)
            {
                var objMethod = (MethodInfo) MethodBase.GetCurrentMethod();
                GlobalExceptionHandler.customExceptionHandler(Ex, objMethod, null, ErrorSeverityType.Normal);
            }

            return oABTestCase;
        }

        public void AddABTestCase(int AbTestId, int iCaseIndex, string sLabel, short iWeight)
        {
            try
            {
                var abTestCase = new ABTestCase();

                abTestCase.ParentABTest = GetABTestByID(AbTestId);
                if (abTestCase.ParentABTest != null)
                {
                    abTestCase.CaseIndex = iCaseIndex;
                    abTestCase.Label = sLabel;
                    abTestCase.Weight = iWeight;

                    ABTestDataFactory.ABTestCaseDAL.Save(abTestCase, true);
                }
            }
            catch (Exception Ex)
            {
                var objMethod = (MethodInfo) MethodBase.GetCurrentMethod();
                GlobalExceptionHandler.customExceptionHandler(Ex, objMethod, null, ErrorSeverityType.Normal);
            }
        }

        public void UpdateABTestCase(int AbTestId, int iCaseIndex, string sLabel, short iWeight)
        {
            try
            {
                var oABTestCase = GetABTestCasesByABTestID_CaseIndex(AbTestId, iCaseIndex);

                oABTestCase.CaseIndex = iCaseIndex;
                oABTestCase.Label = sLabel;
                oABTestCase.Weight = iWeight;

                ABTestDataFactory.ABTestCaseDAL.Update(oABTestCase, true);
            }
            catch (Exception Ex)
            {
                var objMethod = (MethodInfo) MethodBase.GetCurrentMethod();
                GlobalExceptionHandler.customExceptionHandler(Ex, objMethod, null, ErrorSeverityType.Normal);
            }
        }

        public void AddABTestAdminNote(int iAdminUserID, int iABTestID, DateTime Timestamp, string sNote)
        {
            try
            {
                var abTestAdminNote = new ABTestAdminNote();

                abTestAdminNote.AdminUserID = iAdminUserID;
                abTestAdminNote.ABTestID = iABTestID;
                abTestAdminNote.Timestamp = Timestamp;
                abTestAdminNote.Note = sNote;

                ABTestDataFactory.ABTestAdminNoteDAL.Save(abTestAdminNote, true);
            }
            catch (Exception Ex)
            {
                var objMethod = (MethodInfo) MethodBase.GetCurrentMethod();
                GlobalExceptionHandler.customExceptionHandler(Ex, objMethod, null, ErrorSeverityType.Normal);
            }
        }

        public IList<ABTestAdminNote> GetABTestAdminNotes(int ABTestId)
        {
            IList<ABTestAdminNote> lstABTestAdminNotes = null;
            try
            {
                lstABTestAdminNotes = ABTestDataFactory.ABTestAdminNoteDAL.GetABTestAdminNotesByABTestId(ABTestId);
            }
            catch (Exception Ex)
            {
                var objMethod = (MethodInfo) MethodBase.GetCurrentMethod();
                GlobalExceptionHandler.customExceptionHandler(Ex, objMethod, null, ErrorSeverityType.Normal);
            }

            return lstABTestAdminNotes;
        }

        /// <summary>
        ///     Conduct ABTest by ABTestID and SessionID
        /// </summary>
        /// <param name="UserID"></param>
        /// <param name="iABTestID"></param>
        /// <returns></returns>
        public int? ConductABTest(string sSessionID, int iABTestID)
        {
            int? val = null;
            try
            {
                // check if AB test is active
                if (IsABTestActive(iABTestID))
                {
                    val = GetCaseIndexByABTestIDAndSessionID(sSessionID, iABTestID);
                    if (val == null || val == -1)
                    {
                        //insert new value in ABTest Data
                        val = SaveABTestDataSession(sSessionID, iABTestID);
                        if (val == null)
                            val = 1; /// Baseline
                    }
                }
                else
                {
                    val = 1; /// Baseline
                }
            }
            catch (Exception Ex)
            {
                var htParam = new Hashtable();
                htParam.Add("ABTestID", iABTestID);
                htParam.Add("SessionID", sSessionID);
                var objMethod = (MethodInfo) MethodBase.GetCurrentMethod();
                GlobalExceptionHandler.customExceptionHandler(Ex, objMethod, htParam, ErrorSeverityType.Normal);
            }

            return val;
        }

        /// <summary>
        ///     Conduct abtest and save abtestdata for current session
        /// </summary>
        /// <param name="iABTestID"></param>
        /// <param name="sSessionID"></param>
        /// <returns></returns>
        private int? SaveABTestDataSession(string sSessionID, int iABTestID)
        {
            int? savedCaseIndex = null;

            try
            {
                // get new case index
                var NewCaseIndex = GetNewCaseIndex(iABTestID);
                if (NewCaseIndex > 0)
                {
                    // Save only if case index is > 0
                    var abTestDataSession = new ABTestDataSession(iABTestID, NewCaseIndex, sSessionID, null);
                    ABTestDataFactory.ABTestDataSessionDAL.Save(abTestDataSession);
                    savedCaseIndex = NewCaseIndex;
                }
            }
            catch (Exception Ex)
            {
                var ht = new Hashtable(2);
                ht.Add("SessionID", sSessionID);
                ht.Add("ABTestID", iABTestID);
                var objMethod = (MethodInfo) MethodBase.GetCurrentMethod();
                GlobalExceptionHandler.customExceptionHandler(Ex, objMethod, ht, ErrorSeverityType.Normal);
            }

            return savedCaseIndex;
        }

        /// <summary>
        ///     Move Data from ABTESTDataSession To ABTestdata table for a user
        /// </summary>
        /// <param name="user"></param>
        /// <param name="sSessionID"></param>
        /// <returns>true/false</returns>
        public bool MoveSessionABTestInMainTable(UserMaster user, string sSessionID)
        {
            var status = false;
            List<ABTestDataSession> lstAbTestDataSession = null;
            try
            {
                lstAbTestDataSession =
                    ABTestDataFactory.ABTestDataSessionDAL.GetABTestDataSessionBySessionID(sSessionID);
                if (lstAbTestDataSession != null && lstAbTestDataSession.Count > 0)
                {
                    ABTestData abTestData = null;
                    ABTest abTest = null;
                    for (var idx = 0; idx < lstAbTestDataSession.Count; idx++)
                    {
                        abTest = GetABTestByID(lstAbTestDataSession[idx].ABTestID);
                        if (!abTest.StartDate.HasValue || user.CreationDate > abTest.StartDate.Value)
                        {
                            abTestData =
                                ABTestDataFactory.ABTestDataDAL.GetABTestByUserID(user.Id,
                                    lstAbTestDataSession[idx].ABTestID);
                            if (abTestData == null || abTestData == default(ABTestData))
                            {
                                abTestData = new ABTestData
                                {
                                    ABTestID = lstAbTestDataSession[idx].ABTestID,
                                    CaseIndex = lstAbTestDataSession[idx].CaseIndex,
                                    UserID = user.Id,
                                    Data = null,
                                    TimeStamp = DateTime.Now
                                };
                                ABTestDataFactory.ABTestDataDAL.Save(abTestData, true);
                            }
                        }
                    }
                }

                status = true;
            }
            catch (Exception Ex)
            {
                var htParam = new Hashtable();
                htParam.Add("UserId", user.Id);
                htParam.Add("SessionID", sSessionID);
                var objMethod = (MethodInfo) MethodBase.GetCurrentMethod();
                GlobalExceptionHandler.customExceptionHandler(Ex, objMethod, htParam, ErrorSeverityType.Normal);
            }

            return status;
        }

        /// <summary>
        ///     Get case index by abtestid and sessionid from abtestdatasession
        /// </summary>
        /// <param name="sSessionID"></param>
        /// <param name="iABTestID"></param>
        /// <returns></returns>
        private int? GetCaseIndexByABTestIDAndSessionID(string sSessionID, int iABTestID)
        {
            int? iResult = null;

            try
            {
                if (IsABTestActive(iABTestID))
                    iResult = ABTestDataFactory.ABTestDataSessionDAL.GetCaseIndexBySessionID(sSessionID, iABTestID);
            }
            catch (Exception Ex)
            {
                var ht = new Hashtable(2);
                ht.Add("SessionID", sSessionID);
                ht.Add("ABTestID", iABTestID);
                var objMethod = (MethodInfo) MethodBase.GetCurrentMethod();
                GlobalExceptionHandler.customExceptionHandler(Ex, objMethod, ht, ErrorSeverityType.Normal);
            }

            return iResult;
        }


        public void SaveABTestDataCaseIndex(int ABTestID, int UserID, int CaseIndex)
        {
            try
            {
                if (CaseIndex > 0)
                {
                    // Save only if case index is > 0
                    var abTestData = new ABTestData();
                    abTestData.UserID = UserID;
                    abTestData.ABTestID = ABTestID;
                    abTestData.CaseIndex = CaseIndex;
                    abTestData.Data = null;
                    abTestData.TimeStamp = DateTime.Now;
                    ABTestDataFactory.ABTestDataDAL.Save(abTestData, true);
                }
            }
            catch (Exception Ex)
            {
                var objMethod = (MethodInfo) MethodBase.GetCurrentMethod();
                GlobalExceptionHandler.customExceptionHandler(Ex, objMethod, null, ErrorSeverityType.Normal);
            }
        }


        public void UpdateABTestDataCaseIndex(int ABTestID, int UserID, int CaseIndex)
        {
            try
            {
                var abTestData = ABTestDataFactory.ABTestDataDAL.GetABTestByUserID(UserID, ABTestID);
                if (CaseIndex > 0)
                {
                    // Update only if case index is > 0
                    abTestData.UserID = UserID;
                    abTestData.ABTestID = ABTestID;
                    abTestData.CaseIndex = CaseIndex;
                    abTestData.TimeStamp = DateTime.Now;
                    ABTestDataFactory.ABTestDataDAL.Update(abTestData, true);
                }
            }
            catch (Exception Ex)
            {
                var objMethod = (MethodInfo) MethodBase.GetCurrentMethod();
                GlobalExceptionHandler.customExceptionHandler(Ex, objMethod, null, ErrorSeverityType.Normal);
            }
        }
    }
}