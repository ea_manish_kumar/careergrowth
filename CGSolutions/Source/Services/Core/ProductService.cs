﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using CommonModules.Exceptions;
using DataAccess.Core;
using DomainModel.Core;

namespace Services.Core
{
    public class ProductService
    {
        #region "PortalList"

        private static IList<Portal> PortalList;

        private static IList<Portal> GetPortalList()
        {
            IList<Portal> portalList = null;
            try
            {
                portalList = CoreFactory.PortalDAL.GetAll();
            }
            catch (Exception ex)
            {
                var objMethod = (MethodInfo) MethodBase.GetCurrentMethod();
                GlobalExceptionHandler.customExceptionHandler(ex, objMethod, null, SubSystem.Core, Application.WebApp,
                    ErrorSeverityType.High, "", true);
            }

            return portalList;
        }

        public static IList<Portal> GetAllPortals()
        {
            if (PortalList == null)
                PortalList = GetPortalList();

            return PortalList;
        }

        public Portal GetPortalById(int portalId)
        {
            Portal portal = null;
            portal = GetAllPortals().FirstOrDefault(p => p.Id == portalId);
            return portal;
        }

        #endregion

        #region "CultureList"

        private static IList<Culture> CultureList;

        private static IList<Culture> GetCultureList()
        {
            IList<Culture> cultureList = null;
            try
            {
                cultureList = CoreFactory.CultureDAL.GetAll();
            }
            catch (Exception ex)
            {
                var objMethod = (MethodInfo) MethodBase.GetCurrentMethod();
                GlobalExceptionHandler.customExceptionHandler(ex, objMethod, null, SubSystem.Core, Application.WebApp,
                    ErrorSeverityType.High, "", true);
            }

            return cultureList;
        }

        public static IList<Culture> GetAllCultures()
        {
            if (CultureList == null)
                CultureList = GetCultureList();

            return CultureList;
        }

        #endregion

        #region "LoadDomains"

        private static IList<Domain> _domains;

        public static IList<Domain> DomainList
        {
            get
            {
                if (_domains == null)
                    LoadDomainList();
                return _domains;
            }
        }

        private static void LoadDomainList()
        {
            _domains = GetDomainList();
        }

        private static IList<Domain> GetDomainList()
        {
            IList<Domain> domainList = null;
            try
            {
                domainList = CoreFactory.DomainDAL.GetAll();
            }
            catch (Exception ex)
            {
                var objMethod = (MethodInfo) MethodBase.GetCurrentMethod();
                GlobalExceptionHandler.customExceptionHandler(ex, objMethod, null, SubSystem.Core, Application.WebApp,
                    ErrorSeverityType.High, "", true);
            }

            return domainList;
        }

        public static Domain GetDomainByDomainCD(string domainCD)
        {
            var domain = DomainList.FirstOrDefault(d => d.DomainCD == domainCD);
            return domain;
        }

        public static string GetDomainDescription(string domainCD)
        {
            var domainDesc = string.Empty;
            var domain = DomainList.FirstOrDefault(d => d.DomainCD == domainCD);
            if (domain != null)
                domainDesc = domain.Description;
            return domainDesc;
        }

        public static Domain GetDomainByClientCD(string clientCD)
        {
            var domain = DomainList.FirstOrDefault(d => d.ClientCD == clientCD);
            return domain;
        }

        public static string GetDomainCodeByClientCD(string clientCD)
        {
            var domainCD = string.Empty;
            var domain = DomainList.FirstOrDefault(d => d.ClientCD == clientCD);
            if (domain != null)
                domainCD = domain.DomainCD;
            return domainCD;
        }

        #endregion
    }
}