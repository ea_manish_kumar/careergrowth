﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Reflection;
using CommonModules.Exceptions;
using DataAccess.Core;
using DomainModel.Core;
using DomainModel.User;

namespace Services.Core
{
    public class CoreService
    {
        public void SaveOrUpdateResourceUser(string sSessionGUID, int UserID)
        {
            ResourceUser usession = null;
            if (!string.IsNullOrEmpty(sSessionGUID))
            {
                usession = CoreFactory.ResourceUserDAL.GetBySessionId(sSessionGUID);
                if (usession != null)
                {
                    if (UserID > 0 && usession.UserID == 0)
                    {
                        usession.UserID = UserID;
                        CoreFactory.ResourceUserDAL.SaveOrUpdate(usession, true);
                    }
                }
                else
                {
                    usession = new ResourceUser();
                    usession.Id = new Guid(sSessionGUID);
                    usession.UserID = UserID;
                    CoreFactory.ResourceUserDAL.SaveOrUpdate(usession, true);
                }
            }
        }


        public void SaveSessionExtended(SessionExtended sessionExtended)
        {
            try
            {
                var sessionExtendedExists =
                    CoreFactory.SessionExtendedDataDAL.Get(sessionExtended.SessionID, sessionExtended.BrowserID);
                if (sessionExtendedExists == null)
                {
                    sessionExtended.CreatedOn = DateTime.Now;
                    CoreFactory.SessionExtendedDataDAL.Save(sessionExtended, true);
                }
            }
            catch (Exception ex)
            {
                var objMethod = (MethodInfo) MethodBase.GetCurrentMethod();
                GlobalExceptionHandler.customExceptionHandler(ex, objMethod, null, ErrorSeverityType.Normal);
            }
        }

        public int GetUserAgentTypeIDByCriteria(string BrowserName, string BrowserVersion, string EngineName,
            string EngineVersion, string OSName, string OSVersion, string DeviceModel, string DeviceType,
            string DTDeviceType)
        {
            var userAgentTypeID = 0;
            try
            {
                userAgentTypeID = CoreFactory.UserAgentTypeDAL.GetUserAgentIDByCriteria(BrowserName, BrowserVersion,
                    EngineName, EngineVersion, OSName, OSVersion, DeviceModel, DeviceType, DTDeviceType);
            }
            catch (Exception ex)
            {
                var htParams = new Hashtable(2);
                htParams.Add("BrowserName", BrowserName);
                htParams.Add("BrowserVersion", BrowserVersion);
                var objMethod = (MethodInfo) MethodBase.GetCurrentMethod();
                GlobalExceptionHandler.customExceptionHandler(ex, objMethod, htParams, ErrorSeverityType.Normal);
            }

            return userAgentTypeID;
        }

        public void SaveUserAgentType(UserAgentTypeDTO userAgentTypeDTO, string sessionId)
        {
            var userAgentTypeID = 0;
            if (userAgentTypeDTO != null)
            {
                userAgentTypeID = GetUserAgentTypeIDByCriteria(userAgentTypeDTO.BrowserName,
                    userAgentTypeDTO.BrowserVersion, userAgentTypeDTO.EngineName, userAgentTypeDTO.EngineVersion,
                    userAgentTypeDTO.OSName, userAgentTypeDTO.OSVersion, userAgentTypeDTO.DeviceModel,
                    userAgentTypeDTO.DeviceType, userAgentTypeDTO.DTDeviceType);
                if (userAgentTypeID == 0)
                    userAgentTypeID = SaveUserAgentType(userAgentTypeDTO.BrowserName, userAgentTypeDTO.BrowserVersion,
                        userAgentTypeDTO.EngineName, userAgentTypeDTO.EngineVersion, userAgentTypeDTO.OSName,
                        userAgentTypeDTO.OSVersion, userAgentTypeDTO.DeviceModel, userAgentTypeDTO.DeviceType,
                        userAgentTypeDTO.DTDeviceType);

                if (userAgentTypeID > 0 && !string.IsNullOrEmpty(sessionId))
                    UpdateSessionUserAgent(sessionId, userAgentTypeID, userAgentTypeDTO.UserAgent);
            }
        }

        public int SaveUserAgentType(string BrowserName, string BrowserVersion, string EngineName, string EngineVersion,
            string OSName, string OSVersion, string DeviceModel, string DeviceType, string DTDeviceType)
        {
            try
            {
                var UserAgentTypeData = new UserAgentType();
                UserAgentTypeData.BrowserName = BrowserName ?? string.Empty;
                UserAgentTypeData.BrowserVersion = BrowserVersion ?? string.Empty;
                UserAgentTypeData.EngineName = EngineName ?? string.Empty;
                UserAgentTypeData.EngineVersion = EngineVersion ?? string.Empty;
                UserAgentTypeData.OSName = OSName ?? string.Empty;
                UserAgentTypeData.OSVersion = OSVersion ?? string.Empty;
                UserAgentTypeData.DeviceModel = DeviceModel ?? string.Empty;
                UserAgentTypeData.DeviceType = DeviceType ?? string.Empty;
                UserAgentTypeData.DTDeviceType = DTDeviceType ?? string.Empty;

                CoreFactory.UserAgentTypeDAL.Save(UserAgentTypeData, true);
                return UserAgentTypeData.UserAgentTypeID;
            }
            catch (Exception Ex)
            {
                var objMethod = (MethodInfo) MethodBase.GetCurrentMethod();
                GlobalExceptionHandler.customExceptionHandler(Ex, objMethod, null, ErrorSeverityType.Normal);
            }

            return 0;
        }

        public IList<Session> GetSessionUserAgentTypeByCriteria(string SessionId, int UserAgentTypeId)
        {
            IList<Session> lstSessionUserAgentType = null;

            try
            {
                lstSessionUserAgentType =
                    (List<Session>) CoreFactory.SessionDAL.GetSessionUserAgentTypeByCriteria(SessionId,
                        UserAgentTypeId);
            }
            catch (Exception ex)
            {
                var htParams = new Hashtable(2);
                htParams.Add("SessionId", SessionId);
                htParams.Add("UserAgentTypeId", UserAgentTypeId);
                var objMethod = (MethodInfo) MethodBase.GetCurrentMethod();
                GlobalExceptionHandler.customExceptionHandler(ex, objMethod, htParams, ErrorSeverityType.Normal);
            }

            return lstSessionUserAgentType;
        }

        public void SaveSession(Session session)
        {
            try
            {
                CoreFactory.SessionDAL.Save(session, true);
            }
            catch (Exception Ex)
            {
                var objMethod = (MethodInfo) MethodBase.GetCurrentMethod();
                var htParams = new Hashtable();
                htParams.Add("Session", session != null ? session.ToString() : "<NULL>");
                GlobalExceptionHandler.customExceptionHandler(Ex, objMethod, htParams, ErrorSeverityType.Normal);
            }
        }

        public void UpdateSessionUserAgent(string sessionID, int userAgentTypeID, string userAgent)
        {
            try
            {
                CoreFactory.SessionDAL.UpdateSessionUserAgent(sessionID, userAgentTypeID, userAgent);
            }
            catch (Exception ex)
            {
                var htParams = new Hashtable(3);
                htParams.Add("SessionID", sessionID);
                htParams.Add("UserAgentTypeID", userAgentTypeID);
                htParams.Add("userAgent", userAgent);
                var objMethod = (MethodInfo) MethodBase.GetCurrentMethod();
                GlobalExceptionHandler.customExceptionHandler(ex, objMethod, htParams, ErrorSeverityType.Normal);
            }
        }

        public bool SaveToSessionData(string sessionId, string DataTypeCd, object data)
        {
            try
            {
                if (!string.IsNullOrEmpty(sessionId))
                    CoreFactory.SessionDataDAL.SaveToSession(sessionId, DataTypeCd, data);
            }
            catch (Exception ex)
            {
                var htParams = new Hashtable(1);
                htParams.Add("sessionID", sessionId);
                var objMethod = (MethodInfo) MethodBase.GetCurrentMethod();
                GlobalExceptionHandler.customExceptionHandler(ex, objMethod, htParams, ErrorSeverityType.Normal);
            }

            return true;
        }

        public string GetFromSessionData(string sessionId, string DataTypeCd)
        {
            SessionData sessionData = null;
            var data = string.Empty;

            try
            {
                if (!string.IsNullOrEmpty(sessionId))
                {
                    sessionData = CoreFactory.SessionDataDAL.GetFromSession(sessionId, DataTypeCd);
                    if (sessionData != null) data = sessionData.Data;
                }
            }
            catch (Exception ex)
            {
                var htParams = new Hashtable(1);
                htParams.Add("sessionID", sessionId);
                var objMethod = (MethodInfo) MethodBase.GetCurrentMethod();
                GlobalExceptionHandler.customExceptionHandler(ex, objMethod, htParams, ErrorSeverityType.Normal);
            }

            return data;
        }
    }
}