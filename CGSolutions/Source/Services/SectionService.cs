﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Reflection;
using CommonModules.Exceptions;
using DataAccess.Documents;
using DomainModel;
using Resources;
using Utilities.Constants.Documents;

namespace Services.Documents
{
    public class SectionService
    {
        public Section GetSection(int sectionId, int userId)
        {
            try
            {
                var section = DocumentFactory.SectionDAL.GetById(sectionId);
                if (section != null && section.Document != null && section.Document.UserID == userId)
                    return section;
            }
            catch (Exception ex)
            {
                var htParams = new Hashtable(2);
                htParams.Add("SectionID", sectionId);
                htParams.Add("UserID", userId);
                var objMethod = (MethodInfo) MethodBase.GetCurrentMethod();
                GlobalExceptionHandler.customExceptionHandler(ex, objMethod, htParams, ErrorSeverityType.Normal);
            }

            return null;
        }

        public void AddSection(Section section)
        {
            try
            {
                DocumentFactory.SectionDAL.Save(section, true);
            }
            catch (Exception ex)
            {
                var htParams = new Hashtable(1);
                htParams.Add("Section", section);
                var objMethod = (MethodInfo) MethodBase.GetCurrentMethod();
                GlobalExceptionHandler.customExceptionHandler(ex, objMethod, htParams, ErrorSeverityType.Normal);
            }
        }

        public void UpdateSection(Section section)
        {
            try
            {
                DocumentFactory.SectionDAL.Save(section, true);
            }
            catch (Exception ex)
            {
                var htParams = new Hashtable(1);
                htParams.Add("Section", section);
                var objMethod = (MethodInfo) MethodBase.GetCurrentMethod();
                GlobalExceptionHandler.customExceptionHandler(ex, objMethod, htParams, ErrorSeverityType.Normal);
            }
        }

        public void DeleteSection(Section section, out bool isDocumentDeleted)
        {
            isDocumentDeleted = false;
            try
            {
                if (section != null && section.Document != null)
                {
                    var sectionCount = DocumentFactory.SectionDAL.GetSectionCount(section.Document);
                    if (sectionCount == 1)
                    {
                        DocumentFactory.DocumentDAL.Delete(section.Document, true);
                        isDocumentDeleted = true;
                    }
                    else
                    {
                        DocumentFactory.SectionDAL.Delete(section, true);
                        DocumentFactory.DocumentDAL.UpdateDateModified(section.Document.Id);
                    }
                }
            }
            catch (Exception ex)
            {
                var htParams = new Hashtable(1);
                htParams.Add("Section", section);
                var objMethod = (MethodInfo) MethodBase.GetCurrentMethod();
                GlobalExceptionHandler.customExceptionHandler(ex, objMethod, htParams, ErrorSeverityType.Normal);
            }
        }

        public Section GetSectionByDocAndTypeCD(Document doc, string sectionTypeCD)
        {
            try
            {
                return DocumentFactory.SectionDAL.GetSectionByDocIDAndTypeCD(doc, sectionTypeCD);
            }
            catch (Exception ex)
            {
                var htParams = new Hashtable(2);
                htParams.Add("Document", doc);
                htParams.Add("sectionTypeCD", sectionTypeCD);
                var objMethod = (MethodInfo) MethodBase.GetCurrentMethod();
                GlobalExceptionHandler.customExceptionHandler(ex, objMethod, htParams, ErrorSeverityType.Normal);
            }

            return null;
        }

        public IList<Section> GetSectionByDocAndTypeCD(Document doc, string[] sectionTypeCDs)
        {
            try
            {
                return DocumentFactory.SectionDAL.GetSectionByDocIDAndTypeCD(doc, sectionTypeCDs);
            }
            catch (Exception ex)
            {
                var htParams = new Hashtable(2);
                htParams.Add("Document", doc);
                htParams.Add("sectionTypeCDs", sectionTypeCDs);
                var objMethod = (MethodInfo) MethodBase.GetCurrentMethod();
                GlobalExceptionHandler.customExceptionHandler(ex, objMethod, htParams, ErrorSeverityType.Normal);
            }

            return null;
        }

        public string GetLabelBySectionType(string sectionTypeCode)
        {
            var sLabel = string.Empty;
            switch (sectionTypeCode)
            {
                case SectionTypeCD.Volunteer:
                    sLabel = Resource.VLWK;
                    break;
                case SectionTypeCD.Language:
                    sLabel = Resource.LANG;
                    break;
                case SectionTypeCD.Affiliations:
                    sLabel = Resource.AFIL;
                    break;
                case SectionTypeCD.Awards:
                    sLabel = Resource.AWAR;
                    break;
                case SectionTypeCD.Additional_Information:
                    sLabel = Resource.ADDI;
                    break;
                case SectionTypeCD.Publication:
                    sLabel = Resource.Publications;
                    break;
                case SectionTypeCD.Accomplishments:
                    sLabel = Resource.ACCM;
                    break;
                case SectionTypeCD.Other:
                    sLabel = Resource.Otherg_db;
                    break;
                default:
                    sLabel = Resource.Otherg_db;
                    break;
            }

            return sLabel;
        }

        public short GetSectionSortIndexByCode(string sectionTypeCode, short iMaxSortIndex)
        {
            short iSortindex = 0;
            switch (sectionTypeCode)
            {
                case SectionTypeCD.Volunteer:
                    iSortindex = 1;
                    break;
                case SectionTypeCD.Language:
                    iSortindex = 2;
                    break;
                case SectionTypeCD.Affiliations:
                    iSortindex = 3;
                    break;
                case SectionTypeCD.Awards:
                    iSortindex = 4;
                    break;
                case SectionTypeCD.Additional_Information:
                    iSortindex = 5;
                    break;
                case SectionTypeCD.Publication:
                    iSortindex = 6;
                    break;
                case SectionTypeCD.Accomplishments:
                    iSortindex = 7;
                    break;
                case SectionTypeCD.Other:
                    iSortindex = 8;
                    break;
                default:
                    iSortindex = 0;
                    break;
            }

            return (short) (iSortindex + iMaxSortIndex);
        }

        public short GetMaxSortIndex(Document doc)
        {
            try
            {
                return DocumentFactory.SectionDAL.GetMaxSortIndex(doc);
            }
            catch (Exception ex)
            {
                var htParams = new Hashtable(1);
                htParams.Add("Document", doc);
                var objMethod = (MethodInfo) MethodBase.GetCurrentMethod();
                GlobalExceptionHandler.customExceptionHandler(ex, objMethod, htParams, ErrorSeverityType.Normal);
            }

            return -1;
        }
    }
}