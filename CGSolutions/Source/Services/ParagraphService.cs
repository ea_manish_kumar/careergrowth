﻿using System;
using System.Collections;
using System.Reflection;
using CommonModules.Exceptions;
using DataAccess.Documents;
using DomainModel;

namespace Services.Documents
{
    public class ParagraphService
    {
        public void AddParagraph(Paragraph paragraph)
        {
            try
            {
                DocumentFactory.ParagraphDAL.Save(paragraph, true);
            }
            catch (Exception ex)
            {
                var htParams = new Hashtable(1);
                htParams.Add("Paragraph", paragraph);
                var objMethod = (MethodInfo) MethodBase.GetCurrentMethod();
                GlobalExceptionHandler.customExceptionHandler(ex, objMethod, htParams, ErrorSeverityType.Normal);
            }
        }

        public void UpdateParagraph(Paragraph paragraph)
        {
            try
            {
                DocumentFactory.ParagraphDAL.SaveOrUpdate(paragraph, true);
            }
            catch (Exception ex)
            {
                var htParams = new Hashtable(1);
                htParams.Add("Paragraph", paragraph);
                var objMethod = (MethodInfo) MethodBase.GetCurrentMethod();
                GlobalExceptionHandler.customExceptionHandler(ex, objMethod, htParams, ErrorSeverityType.Normal);
            }
        }

        public void DeleteParagraph(Paragraph paragraph, out bool isSectionDeleted, out bool isDocumentDeleted)
        {
            isSectionDeleted = false;
            isDocumentDeleted = false;
            try
            {
                if (paragraph != null && paragraph.Section != null && paragraph.Section.Document != null)
                {
                    var paragraphCount = DocumentFactory.ParagraphDAL.GetParagraphCount(paragraph.Section);
                    if (paragraphCount == 1)
                    {
                        var sectionCount = DocumentFactory.SectionDAL.GetSectionCount(paragraph.Section.Document);
                        if (sectionCount == 1)
                        {
                            DocumentFactory.DocumentDAL.Delete(paragraph.Section.Document, true);
                            isDocumentDeleted = true;
                            isSectionDeleted = true;
                        }
                        else
                        {
                            DocumentFactory.SectionDAL.Delete(paragraph.Section, true);
                            DocumentFactory.DocumentDAL.UpdateDateModified(paragraph.Section.Document.Id);
                            isSectionDeleted = true;
                        }
                    }
                    else
                    {
                        DocumentFactory.ParagraphDAL.Delete(paragraph, true);
                        DocumentFactory.DocumentDAL.UpdateDateModified(paragraph.Section.Document.Id);
                    }
                }
            }
            catch (Exception ex)
            {
                var htParams = new Hashtable(1);
                htParams.Add("Paragraph", paragraph);
                var objMethod = (MethodInfo) MethodBase.GetCurrentMethod();
                GlobalExceptionHandler.customExceptionHandler(ex, objMethod, htParams, ErrorSeverityType.Normal);
            }
        }

        public Paragraph GetParagraph(int paragraphId, int userId)
        {
            try
            {
                var paragraph = DocumentFactory.ParagraphDAL.GetById(paragraphId);
                if (paragraph != null && paragraph.Section != null && paragraph.Section.Document != null &&
                    paragraph.Section.Document.UserID == userId)
                    return paragraph;
            }
            catch (Exception ex)
            {
                var htParams = new Hashtable(2);
                htParams.Add("ParagraphID", paragraphId);
                htParams.Add("UserID", userId);
                var objMethod = (MethodInfo) MethodBase.GetCurrentMethod();
                GlobalExceptionHandler.customExceptionHandler(ex, objMethod, htParams, ErrorSeverityType.Normal);
            }

            return null;
        }

        public Paragraph GetParagraphByID(int paragraphid)
        {
            try
            {
                return DocumentFactory.ParagraphDAL.GetById(paragraphid);
            }
            catch (Exception ex)
            {
                var htParams = new Hashtable(1);
                htParams.Add("ParagraphID", paragraphid);
                var objMethod = (MethodInfo) MethodBase.GetCurrentMethod();
                GlobalExceptionHandler.customExceptionHandler(ex, objMethod, htParams, ErrorSeverityType.Normal);
            }

            return null;
        }

        public Paragraph GetParagraphBySectionAndIndex(Section section, short paragraphindex)
        {
            Paragraph para = null;
            try
            {
                para = DocumentFactory.ParagraphDAL.GetParagraphBySectionAndIndex(section, paragraphindex);
            }
            catch (Exception ex)
            {
                var htParams = new Hashtable(1);
                htParams.Add("ParagraphIndex", paragraphindex);
                htParams.Add("Section", section);
                var objMethod = (MethodInfo) MethodBase.GetCurrentMethod();
                GlobalExceptionHandler.customExceptionHandler(ex, objMethod, htParams, ErrorSeverityType.Normal);
            }

            return para;
        }
    }
}