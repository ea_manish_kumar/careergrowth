﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Reflection;
using CommonModules.Exceptions;
using DataAccess.Analytics;
using DomainModel.Analytics; //using Services.User;

namespace Services.Analytics
{
    public class AnalyticService
    {
        /// <summary>
        ///     Get SKUPurchase Report for between supplied start date and end date
        /// </summary>
        /// <param name="StartDate">StartDate</param>
        /// <param name="EndDate">EndDate</param>
        /// <returns>List of object SKUPurchaseDTO</returns>
        public IList<SKUPurchaseDTO> GetSKUPurchaseReport(DateTime StartDate, DateTime EndDate)
        {
            IList<SKUPurchaseDTO> lstRevenue = null;
            try
            {
                lstRevenue = AnalyticsFactory.SKUPurchaseDAL.GetSKUPurchaseReport(StartDate, EndDate);
            }
            catch (Exception ex)
            {
                var htParams = new Hashtable(2);
                htParams.Add("StartDate", StartDate);
                htParams.Add("EndDate", EndDate);
                var objMethod = (MethodInfo) MethodBase.GetCurrentMethod();
                GlobalExceptionHandler.customExceptionHandler(ex, objMethod, htParams, ErrorSeverityType.Normal);
            }

            return lstRevenue;
        }

        /// <summary>
        ///     Get SubscriptionSnapshot Report for between supplied start date and end date
        /// </summary>
        /// <param name="StartDate">StartDate</param>
        /// <param name="EndDate">EndDate</param>
        /// <returns>List of object SubscriptionSnapshotDTO</returns>
        public IList<SubscriptionSnapshotDTO> GetSubscriptionSnapshotReport(DateTime StartDate, DateTime EndDate)
        {
            IList<SubscriptionSnapshotDTO> lstRevenue = null;
            try
            {
                lstRevenue = AnalyticsFactory.SubscriptionSnapshotDAL.GetSubscriptionSnapshotReport(StartDate, EndDate);
            }
            catch (Exception ex)
            {
                var htParams = new Hashtable(2);
                htParams.Add("StartDate", StartDate);
                htParams.Add("EndDate", EndDate);
                var objMethod = (MethodInfo) MethodBase.GetCurrentMethod();
                GlobalExceptionHandler.customExceptionHandler(ex, objMethod, htParams, ErrorSeverityType.Normal);
            }

            return lstRevenue;
        }

        /// <summary>
        ///     Get GetAB Test Report for between supplied AB Test ID and start date and end date
        /// </summary>
        /// <param name="ABTestID">ABTestID</param>
        /// <param name="StartDate">StartDate</param>
        /// <param name="EndDate">EndDate</param>
        /// <returns></returns>
        public IList<ABTestDTO> GetABTestReport(int ABTestID, DateTime StartDate, DateTime EndDate)
        {
            IList<ABTestDTO> lstRevenue = null;
            try
            {
                lstRevenue = AnalyticsFactory.ABTestDAL.GetABTestReport(ABTestID, StartDate, EndDate);
            }
            catch (Exception ex)
            {
                var htParams = new Hashtable(2);
                htParams.Add("StartDate", StartDate);
                htParams.Add("EndDate", EndDate);
                var objMethod = (MethodInfo) MethodBase.GetCurrentMethod();
                GlobalExceptionHandler.customExceptionHandler(ex, objMethod, htParams, ErrorSeverityType.Normal);
            }

            return lstRevenue;
        }

        /// <summary>
        ///     Get GetRenewalBySKU Report for supplied skuid, start date and period
        /// </summary>
        /// <param name="SKUId"></param>
        /// <param name="StartDate"></param>
        /// <param name="Period"></param>
        /// <returns></returns>
        public IList<RenewalBySKUDTO> GetRenewalBySKU(string SKUId, DateTime StartDate, int Period)
        {
            IList<RenewalBySKUDTO> lstRenewalBySKU = null;
            try
            {
                lstRenewalBySKU = AnalyticsFactory.RenewalBySKUDAL.GetRenewalBySKU(SKUId, StartDate, Period);
            }
            catch (Exception ex)
            {
                var htParams = new Hashtable(3);

                htParams.Add("SKUId", SKUId);
                htParams.Add("StartDate", StartDate);
                htParams.Add("Period", Period);
                var objMethod = (MethodInfo) MethodBase.GetCurrentMethod();
                GlobalExceptionHandler.customExceptionHandler(ex, objMethod, htParams, ErrorSeverityType.Normal);
            }

            return lstRenewalBySKU;
        }

        public IList<RevenueByProductNewDTO> GetRevenueReportNew(DateTime StartDate, DateTime EndDate)
        {
            IList<RevenueByProductNewDTO> lstRevenue = null;
            try
            {
                lstRevenue = AnalyticsFactory.RevenueNewDAL.GetRevenueReport(StartDate, EndDate);
            }
            catch (Exception ex)
            {
                var htParams = new Hashtable(2);
                htParams.Add("StartDate", StartDate);
                htParams.Add("EndDate", EndDate);
                var objMethod = (MethodInfo) MethodBase.GetCurrentMethod();
                GlobalExceptionHandler.customExceptionHandler(ex, objMethod, htParams, ErrorSeverityType.Normal);
            }

            return lstRevenue;
        }


        public IList<ContentReportDTO> GetContentReport(DateTime StartDate, DateTime EndDate)
        {
            IList<ContentReportDTO> lstRevenue = null;
            try
            {
                lstRevenue = AnalyticsFactory.ContentReportDAL.GetContentReport(StartDate, EndDate);
            }
            catch (Exception ex)
            {
                var htParams = new Hashtable(1);
                //htParams.Add("UserID", UserID);
                var objMethod = (MethodInfo) MethodBase.GetCurrentMethod();
                GlobalExceptionHandler.customExceptionHandler(ex, objMethod, htParams, ErrorSeverityType.Normal);
            }

            return lstRevenue;
        }

        /// <summary>
        ///     Get CL Funnel Report for between supplied start date and end date
        /// </summary>
        /// <param name="StartDate">StartDate</param>
        /// <param name="EndDate">EndDate</param>
        /// <returns>List of object ReportByCLFunnelDTO</returns>
        public IList<ReportByCverLetterFunnelDTO> GetCLFunnelReport(DateTime StartDate, DateTime EndDate)
        {
            IList<ReportByCverLetterFunnelDTO> lstRevenue = null;
            try
            {
                lstRevenue =
                    (List<ReportByCverLetterFunnelDTO>) AnalyticsFactory.CLFunnelReportDAL.GetCLFunnelReport(StartDate,
                        EndDate);
            }
            catch (Exception ex)
            {
                var htParams = new Hashtable(2);
                htParams.Add("StartDate", StartDate);
                htParams.Add("EndDate", EndDate);
                var objMethod = (MethodInfo) MethodBase.GetCurrentMethod();
                GlobalExceptionHandler.customExceptionHandler(ex, objMethod, htParams, ErrorSeverityType.Normal);
            }

            return lstRevenue;
        }


        /// <summary>
        ///     Get CL Funnel Report for between supplied start date and end date
        /// </summary>
        /// <param name="StartDate">StartDate</param>
        /// <param name="EndDate">EndDate</param>
        /// <returns>List of object ReportByCLFunnelDTO</returns>
        public IList<RevenueByCoverLetterInsightDTO> GetRevenueByRgCLInsight(DateTime StartDate, DateTime EndDate)
        {
            IList<RevenueByCoverLetterInsightDTO> lstRevenue = null;
            try
            {
                lstRevenue =
                    (List<RevenueByCoverLetterInsightDTO>) AnalyticsFactory.RevenueByRgCLInsightDAL
                        .GetRevenueByRgCLInsight(StartDate, EndDate);
            }
            catch (Exception ex)
            {
                var htParams = new Hashtable(2);
                htParams.Add("StartDate", StartDate);
                htParams.Add("EndDate", EndDate);
                var objMethod = (MethodInfo) MethodBase.GetCurrentMethod();
                GlobalExceptionHandler.customExceptionHandler(ex, objMethod, htParams, ErrorSeverityType.Normal);
            }

            return lstRevenue;
        }
    }

    //prc_Report_SKUPurchase
}