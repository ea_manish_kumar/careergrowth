﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Reflection;
using CommonModules.Exceptions;
using DataAccess.Documents;
using DomainModel;

namespace Services.Documents
{
    public class TemplateService
    {
        private static IList<Template> lstTemplates;

        static TemplateService()
        {
            lstTemplates = GetAllTemplates();
        }

        public static IList<Template> GetTemplates
        {
            get
            {
                //if the loading failed first time, try to reload the data
                if (lstTemplates == null)
                    lstTemplates = GetAllTemplates();

                return lstTemplates;
            }
        }

        private static IList<Template> GetAllTemplates()
        {
            IList<Template> lstTemplates = null;
            try
            {
                lstTemplates = DocumentFactory.TemplateDAL.GetAllTemplates();
            }
            catch (Exception ex)
            {
                var htParams = new Hashtable(1);
                htParams.Add("TemplatesList",
                    lstTemplates != null && lstTemplates.Count > 0 ? lstTemplates.ToString() : "<NULL>");
                var objMethod = (MethodInfo) MethodBase.GetCurrentMethod();
                GlobalExceptionHandler.customExceptionHandler(ex, objMethod, htParams, SubSystem.Content,
                    Application.WebApp, ErrorSeverityType.High, null, true);
            }

            return lstTemplates;
        }
    }
}