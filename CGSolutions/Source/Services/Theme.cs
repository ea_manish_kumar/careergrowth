﻿using System.Collections.Generic;

namespace Services.Documents
{
    public class Theme
    {
        public string DisplayColor1;
        public string DisplayColor2;
        public string DisplayColor3;
        public IDictionary<string, string> Styles = new Dictionary<string, string>();
        public string ThemeCD;
        public string ThemeName;
    }
}