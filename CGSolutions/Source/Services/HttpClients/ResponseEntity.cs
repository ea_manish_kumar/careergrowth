﻿using System.Net;

namespace Services.HttpClients
{
    public class ResponseEntity<V>
    {
        public V Data { get; set; }
        public HttpStatusCode StatusCode { get; set; }
        public string ErrorMessage { get; set; }
    }
}