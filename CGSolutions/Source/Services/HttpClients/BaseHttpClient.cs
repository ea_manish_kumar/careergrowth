﻿using System;
using System.Collections;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Net;
using System.Net.Http;
using System.Reflection;
using System.Text;
using CommonModules.Constants;
using CommonModules.Exceptions;
using Newtonsoft.Json;
using Utilities.HashingUtility;

namespace Services.HttpClients
{
    public class BaseHttpClient
    {
        private static readonly ConcurrentDictionary<string, HttpClient> httpClientsList =
            new ConcurrentDictionary<string, HttpClient>();

        private readonly string _serviceName;

        public BaseHttpClient(string serviceName, string serviceURL)
        {
            _serviceName = serviceName;
            ServiceUrl = serviceURL;
        }

        private string ServiceUrl { get; }

        protected ResponseEntity<V> GetResponseEntity<T, V>(string endpoint, HttpMethod type, T data,
            List<KeyValuePair<string, string>> headerKeys = null, bool enableAuth = true)
        {
            var responseEntity = new ResponseEntity<V>();
            try
            {
                var dataJSON = JsonConvert.SerializeObject(data, Formatting.Indented);
                var content = new StringContent(dataJSON, Encoding.UTF8, "application/json");
                var httpRequestMessage = new HttpRequestMessage(type, ServiceUrl + endpoint) {Content = content};
                if (headerKeys != null)
                    foreach (var customHeader in headerKeys)
                        httpRequestMessage.Headers.TryAddWithoutValidation(customHeader.Key, customHeader.Value);
                if (enableAuth)
                {
                    var guid = new Guid().ToString();
                    var hash = HashingUtility.ObfuscatePassword(guid);
                    httpRequestMessage.Headers.Add(RequestHeaders.AuthKey, guid);
                    httpRequestMessage.Headers.Add(RequestHeaders.AuthToken, hash);
                }

                var httpClient = GetClient(_serviceName);
                if (httpClient != null)
                    using (var httpResponse = httpClient.SendAsync(httpRequestMessage).Result)
                    {
                        using (var httpResponseContent = httpResponse.Content)
                        {
                            var responseString = httpResponseContent.ReadAsStringAsync().Result;
                            if (!string.IsNullOrEmpty(responseString))
                            {
                                var response = JsonConvert.DeserializeObject<V>(responseString);
                                responseEntity.Data = response;
                                responseEntity.StatusCode = HttpStatusCode.OK;
                            }
                        }
                    }
            }
            catch (Exception ex)
            {
                LogException(endpoint, ex.Message, ex);
            }

            return responseEntity;
        }

        private void LogException(string url, string message, Exception ex)
        {
            var htParams = new Hashtable(2);
            htParams.Add("Url", url);
            htParams.Add("ErrorMessage", message);
            var objMethod = (MethodInfo) MethodBase.GetCurrentMethod();
            GlobalExceptionHandler.customExceptionHandler(ex, objMethod, htParams, _serviceName, Application.WebApp,
                ErrorSeverityType.High, ex.Message, false);
        }

        private HttpClient GetClient(string serviceName)
        {
            HttpClient httpClient;
            if (!httpClientsList.TryGetValue(serviceName, out httpClient))
            {
                httpClient = new HttpClient(new HttpClientHandler
                    {AutomaticDecompression = DecompressionMethods.GZip | DecompressionMethods.Deflate});
                httpClientsList.TryAdd(serviceName, httpClient);
            }

            return httpClient;
        }
    }
}