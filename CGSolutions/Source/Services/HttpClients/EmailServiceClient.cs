﻿using System.Net.Http;
using CommonModules.Configuration;
using Services.HttpClients.Models;

namespace Services.HttpClients
{
    public class EmailServiceClient : BaseHttpClient
    {
        private static readonly string ServiceUrl = ConfigManager.GetConfig("EmailServiceURL");
        private static readonly string ServiceName = "EmailService";
        private readonly string CustomEmail = "CustomEmail";
        private readonly string ForgotPassword = "ForgotPasswordEmail";
        private readonly string WelcomeEmail = "WelcomeEmail";

        public EmailServiceClient() : base(ServiceName, ServiceUrl)
        {
        }

        public void SendWelcomeEmail(WelcomeEmail email)
        {
            var response = GetResponseEntity<WelcomeEmail, bool>(WelcomeEmail, HttpMethod.Post, email);
        }

        public void SendForgotPasswordEmail(ForgotEmail email)
        {
            var response = GetResponseEntity<ForgotEmail, bool>(ForgotPassword, HttpMethod.Post, email);
        }

        public void SendCustomizedEmail(CustomEmail email)
        {
            var response = GetResponseEntity<CustomEmail, bool>(CustomEmail, HttpMethod.Post, email);
        }
    }
}