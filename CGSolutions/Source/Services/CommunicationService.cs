﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Configuration;
using System.IO;
using System.Net;
using System.Net.Mail;
using System.Reflection;
using System.Text;
using System.Text.RegularExpressions;
using System.Web;
using BusinessLogicProcess.Documents;
using CommonModules.Configuration;
using CommonModules.Exceptions;
using DataAccess.Communication;
using DataAccess.Documents;
using DataAccess.User;
using DomainModel.Communication;
using DomainModel.User;
using Utilities;
using Utilities.Communication;
using Utilities.Constants;
using File = DomainModel.File;

namespace Services.Communication
{
    public class CommunicationService
    {
        private static readonly Dictionary<string, string> mailTemplatesRH = new Dictionary<string, string>();
        private static readonly List<string> templateFileNames = new List<string>();

        private StringDictionary _sdTemplate;

        private SMTPSETTINGS.eSMTPMODES? _smtpmode;
        private bool bIsGAMode;
        private string contactUsPageUrl = string.Empty;
        private readonly string domainCd = CGUtility.GetDomainCDFromConfig();
        private readonly string kmOpnPxl = string.Empty;

        private string landingPageUrl = string.Empty;
        private string privatePolicyPageUrl = string.Empty;
        private string resumeHomePageUrl = string.Empty;
        private string userAgreementPageUrl = string.Empty;
        private string userSettingPageUrl = string.Empty;

        static CommunicationService()
        {
            CreateMailTemplatesList();
            LoadMailTemplates();
        }

        public virtual SMTPSETTINGS.eSMTPMODES? SMTPMODE
        {
            get
            {
                if (_smtpmode != null && _smtpmode > 0)
                    return _smtpmode;
                return null;
            }
            set => _smtpmode = value;
        }

        private StringDictionary sdTemplate
        {
            get
            {
                lock (_sdTemplate)
                {
                    return _sdTemplate;
                }
            }
        }

        private static void CreateMailTemplatesList()
        {
            templateFileNames.Add("Welcome.html");
            templateFileNames.Add("Subscription-Confirmation.html");
            templateFileNames.Add("Reset-Password.html");
        }

        private static void LoadMailTemplates()
        {
            foreach (var template in templateFileNames)
            {
                var templateFilePathRH = string.Empty;
                if (HttpContext.Current == null)
                {
                    var folder = Path.GetDirectoryName(Assembly.GetEntryAssembly().Location);
                    templateFilePathRH = folder + "/resources/emailtemplates/" + template;
                }
                else
                {
                    templateFilePathRH = HttpContext.Current.Server.MapPath("/resources/emailtemplates/") + template;
                }

                var sbTemplateContentRH = new StringBuilder(new CGUtility().LoadFileContentFromURL(templateFilePathRH));
                if (!mailTemplatesRH.ContainsKey(template.ToUpper()))
                    mailTemplatesRH.Add(template.ToUpper(), sbTemplateContentRH.ToString());
            }
        }

        /// <summary>
        ///     Send mail, by providing the basic necessaties of a mail like to, from, body, subject
        /// </summary>
        /// <param name="toAddress"></param>
        /// <param name="fromAddress">uses outgoing email address from config file</param>
        /// <param name="mailBody"></param>
        /// <param name="subject"></param>
        /// <returns></returns>
        public bool SendEmail(string toAddress, string mailBody, string subject)
        {
            var isMailSent = false;

            try
            {
                var fromAddress = CGUtility.GetOutgoingEmailAddress(domainCd);

                var sdRecipients = new StringDictionary
                {
                    {TemplateIndex.eTI_To, ConfigManager.GetConfig(toAddress)},
                    {TemplateIndex.eTI_From, fromAddress},
                    {TemplateIndex.eTI_Subject, subject},
                    {TemplateIndex.eTI_HTMLBody, "<table width=500px;><tr><td>" + mailBody + "</td></tr></Table>"},
                    {TemplateIndex.eTI_Header, "SendGrid=send_contactus"}
                };

                if (sdRecipients.Count > 0)
                {
                    SMTPMODE = SMTPSETTINGS.eSMTPMODES.eTI_SGUSERGENERATEDMODE;
                    isMailSent = SendEmail(sdRecipients, null);
                }
            }
            catch (Exception ex)
            {
                var htParams = new Hashtable(2);
                htParams.Add("SendMail", "Basic_Send_Mail");
                var objMethod = (MethodInfo) MethodBase.GetCurrentMethod();
                GlobalExceptionHandler.customExceptionHandler(ex, objMethod, htParams, ErrorSeverityType.Normal);
            }

            return isMailSent;
        }

        //OverLoad of SendEmail for Admin agents
        public bool SendEmail(MailerConstants.eMailOut mailOutValue, StringDictionary recipientProperties,
            IList<Attachment> listAttachment = null, string userDomainCD = "")
        {
            var MailSentStatus = false;
            var templateFile = string.Empty;
            var templateFolder = string.Empty;
            var outgoingEmailAddress = string.Empty;
            var templateFilePath = string.Empty;
            var sbTemplateContent = string.Empty;
            var mailOutID = Convert.ToInt32(mailOutValue);
            var iRecordSteps = 0;
            try
            {
                if (userDomainCD == "")
                    userDomainCD = domainCd;
                var objMailout = CommunicationFactory.MailoutDAL.GetById(mailOutID);
                iRecordSteps = 1;
                templateFile = objMailout.TemplateFile;
                if (string.IsNullOrEmpty(templateFile))
                    throw new ArgumentException("No Mailout record for Mailout ID- " + mailOutID + " exists in DB");
                outgoingEmailAddress = CGUtility.GetOutgoingEmailAddress(userDomainCD);
                if (userDomainCD == DomainFriendlyNameConstants.FresherResume)
                {
                    iRecordSteps = 4;
                    sbTemplateContent = mailTemplatesRH[templateFile.ToUpper()];
                    iRecordSteps = 5;
                }
                else
                {
                    iRecordSteps = 4;
                    sbTemplateContent = mailTemplatesRH[templateFile.ToUpper()];
                    iRecordSteps = 5;
                    outgoingEmailAddress = ConfigManager.GetConfig(MailerConstants.RESUMEHELP_OUTGOING_EMAIL);
                }

                if (!string.IsNullOrEmpty(sbTemplateContent))
                {
                    iRecordSteps = 6;
                    var sTemplate = ProcessEmailTemplate(new StringBuilder(sbTemplateContent), userDomainCD);
                    iRecordSteps = 7;
                    SetTemplate(sTemplate);

                    // Add Customer Service Email Address
                    recipientProperties.Add(EmailConstants.OUTGOING_EMAILADDRESS, outgoingEmailAddress);
                    iRecordSteps = 8;
                    var sdRecipientTemplate = ProcessTemplate(recipientProperties);
                    iRecordSteps = 9;
                    if (sdRecipientTemplate != null && sdRecipientTemplate.Count > 0)
                        MailSentStatus = SendEmail(sdRecipientTemplate, listAttachment, userDomainCD);
                }
            }
            catch (Exception ex)
            {
                var htParams = new Hashtable(4);
                htParams.Add("mailOutID", mailOutID);
                htParams.Add("recipientPropertiesCount",
                    recipientProperties != null ? recipientProperties.Count.ToString() : "NULL");
                htParams.Add("listAttachment", listAttachment != null ? listAttachment.ToString() : "NULL");
                htParams.Add("LastStepRecorded", iRecordSteps.ToString());
                var objMethod = (MethodInfo) MethodBase.GetCurrentMethod();
                GlobalExceptionHandler.customExceptionHandler(ex, objMethod, htParams, ErrorSeverityType.Normal);
            }

            return MailSentStatus;
        }

        public bool SendEmail(StringDictionary sdRecipientTemplate, IList<Attachment> listAttachments)
        {
            return SendEmail(sdRecipientTemplate, listAttachments, domainCd);
        }

        //OverLoad of SendEmail for Admin agents
        //Chargeback - Admin
        private bool SendEmail(StringDictionary sdRecipientTemplate, IList<Attachment> listAttachments,
            string userDomainCD)
        {
            var sendMailStatus = false;
            var mailMessage = new MailMessage();
            string smtpHost, smtpUserName, smtpPassword;
            int smtpPort;

            // send mail functionality here 
            try
            {
                //********To**********************
                if (sdRecipientTemplate.ContainsKey(MailerConstants.eTemplateIndex.eTI_To.ToString()) &
                    !string.IsNullOrEmpty(sdRecipientTemplate[MailerConstants.eTemplateIndex.eTI_To.ToString()]))
                    mailMessage.To.Add(sdRecipientTemplate[MailerConstants.eTemplateIndex.eTI_To.ToString()].Trim());
                //*********from*********
                if (sdRecipientTemplate.ContainsKey(MailerConstants.eTemplateIndex.eTI_From.ToString()) &
                    !string.IsNullOrEmpty(sdRecipientTemplate[MailerConstants.eTemplateIndex.eTI_From.ToString()]))
                    mailMessage.From =
                        new MailAddress(sdRecipientTemplate[MailerConstants.eTemplateIndex.eTI_From.ToString()].Trim());
                //******Subject*****************
                if (sdRecipientTemplate.ContainsKey(MailerConstants.eTemplateIndex.eTI_Subject.ToString()) &
                    !string.IsNullOrEmpty(sdRecipientTemplate[MailerConstants.eTemplateIndex.eTI_Subject.ToString()]))
                    mailMessage.Subject = sdRecipientTemplate[MailerConstants.eTemplateIndex.eTI_Subject.ToString()]
                        .Trim();
                //******CC*****************
                if (sdRecipientTemplate.ContainsKey(MailerConstants.eTemplateIndex.eTI_CC.ToString()) &
                    !string.IsNullOrEmpty(sdRecipientTemplate[MailerConstants.eTemplateIndex.eTI_CC.ToString()]))
                    mailMessage.CC.Add(sdRecipientTemplate[MailerConstants.eTemplateIndex.eTI_CC.ToString()].Trim());
                //******BCC*****************
                if (sdRecipientTemplate.ContainsKey(MailerConstants.eTemplateIndex.eTI_BCC.ToString()) &
                    !string.IsNullOrEmpty(sdRecipientTemplate[MailerConstants.eTemplateIndex.eTI_BCC.ToString()]))
                    mailMessage.Bcc.Add(sdRecipientTemplate[MailerConstants.eTemplateIndex.eTI_BCC.ToString()].Trim());

                //******ReplyTo*****************
                //Added by AB on 19/06/2013 for VM-587
                if (sdRecipientTemplate.ContainsKey(TemplateIndex.eTI_ReplyTo) &
                    !string.IsNullOrEmpty(sdRecipientTemplate[TemplateIndex.eTI_ReplyTo]))
                    mailMessage.ReplyToList.Add(new MailAddress(sdRecipientTemplate[TemplateIndex.eTI_ReplyTo].Trim()));
                if (sdRecipientTemplate.ContainsKey(TemplateIndex.eTI_Header) &
                    !string.IsNullOrEmpty(sdRecipientTemplate[TemplateIndex.eTI_Header]))
                {
                    string[] arr = { };
                    string[] arr1 = { };
                    var sHeader = sdRecipientTemplate[TemplateIndex.eTI_Header];
                    if (!string.IsNullOrEmpty(sHeader))
                    {
                        arr = sHeader.Split('\n');
                        if (arr.Length > 0)
                            foreach (var item in arr)
                                if (!string.IsNullOrEmpty(item))
                                {
                                    arr1 = item.Split('=');
                                    if (arr1.Length > 0) mailMessage.Headers.Add(arr1[0], arr1[1]);
                                }

                        bIsGAMode = true;
                    }
                }


                //******Body*****************
                if (sdRecipientTemplate.ContainsKey(MailerConstants.eTemplateIndex.eTI_HTMLBody.ToString()) &
                    !string.IsNullOrEmpty(sdRecipientTemplate[MailerConstants.eTemplateIndex.eTI_HTMLBody.ToString()]))
                {
                    mailMessage.Body = sdRecipientTemplate[MailerConstants.eTemplateIndex.eTI_HTMLBody.ToString()]
                        .Trim();
                    mailMessage.IsBodyHtml = true;
                }
                else if (sdRecipientTemplate.ContainsKey(MailerConstants.eTemplateIndex.eTI_TextBody.ToString()) &
                         !string.IsNullOrEmpty(
                             sdRecipientTemplate[MailerConstants.eTemplateIndex.eTI_TextBody.ToString()]))
                {
                    mailMessage.Body = sdRecipientTemplate[MailerConstants.eTemplateIndex.eTI_TextBody.ToString()]
                        .Trim();
                    mailMessage.IsBodyHtml = false;
                }

                if (listAttachments != null)
                    if (listAttachments.Count > 0)
                        foreach (var attachment in listAttachments)
                            mailMessage.Attachments.Add(attachment);

                //******Host*****************
                if (sdRecipientTemplate.ContainsKey(MailerConstants.eTemplateIndex.eTI_SMTPServer.ToString()) &&
                    !string.IsNullOrEmpty(
                        sdRecipientTemplate[MailerConstants.eTemplateIndex.eTI_SMTPServer.ToString()]) &&
                    sdRecipientTemplate.ContainsKey(MailerConstants.eTemplateIndex.eTI_SMTPPort.ToString()) &&
                    !string.IsNullOrEmpty(sdRecipientTemplate[MailerConstants.eTemplateIndex.eTI_SMTPPort.ToString()]))
                {
                    smtpHost = sdRecipientTemplate[MailerConstants.eTemplateIndex.eTI_SMTPServer.ToString()].Trim();
                    smtpPort = int.Parse(sdRecipientTemplate[MailerConstants.eTemplateIndex.eTI_SMTPPort.ToString()]
                        .Trim());

                    var client = new SmtpClient(smtpHost, smtpPort);

                    if (sdRecipientTemplate.ContainsKey(MailerConstants.eTemplateIndex.eTI_SMTPUsername.ToString()) &&
                        !string.IsNullOrEmpty(
                            sdRecipientTemplate[MailerConstants.eTemplateIndex.eTI_SMTPUsername.ToString()]) &&
                        sdRecipientTemplate.ContainsKey(MailerConstants.eTemplateIndex.eTI_SMTPPassword.ToString()) &&
                        !string.IsNullOrEmpty(
                            sdRecipientTemplate[MailerConstants.eTemplateIndex.eTI_SMTPPassword.ToString()]))
                    {
                        smtpUserName = sdRecipientTemplate[MailerConstants.eTemplateIndex.eTI_SMTPUsername.ToString()]
                            .Trim();
                        smtpPassword = sdRecipientTemplate[MailerConstants.eTemplateIndex.eTI_SMTPPassword.ToString()]
                            .Trim();
                        client.Credentials = new NetworkCredential(smtpUserName, smtpPassword);
                    }


                    //Debug.WriteLine(mailMessage.Body.ToString());
                    sendMailStatus = SendEmail(mailMessage, client);
                }
                else
                {
                    // if smtp mode is assigned use the smtp settings from web.config
                    if (SMTPMODE != null && SMTPMODE != 0)
                        try
                        {
                            // steps to be executed here 
                            // brief : get the SMTP Values as per the SMTP Mode assigned.
                            // and try to send email using that credentials if not succeded use the general settings to send email .
                            SMTPConfigManager transactionconfig = null;
                            if (SMTPMODE == SMTPSETTINGS.eSMTPMODES.eTI_SMTPUSERGENERATEDMODE && !bIsGAMode)
                            {
                                transactionconfig = new SMTPConfigManager();
                                transactionconfig =
                                    (SMTPConfigManager) ConfigurationManager.GetSection(SMTPSETTINGS.EXTERNALSMTP);
                            }

                            if (SMTPMODE == SMTPSETTINGS.eSMTPMODES.eTI_SMTPTRANSACTIONALMODE && !bIsGAMode)
                            {
                                transactionconfig = new SMTPConfigManager();
                                transactionconfig =
                                    (SMTPConfigManager) ConfigurationManager.GetSection(SMTPSETTINGS.TRANSACTIONALSMTP);
                            }

                            if (SMTPMODE == SMTPSETTINGS.eSMTPMODES.eTI_SGTRANSACTIONALMODE && bIsGAMode)
                            {
                                transactionconfig = new SMTPConfigManager();
                                transactionconfig =
                                    (SMTPConfigManager) ConfigurationManager.GetSection(SMTPSETTINGS
                                        .SENDGRIDTRANSACTIONALSMTP);

                                if (userDomainCD == DomainFriendlyNameConstants.FreeResume)
                                    transactionconfig =
                                        (SMTPConfigManager) ConfigurationManager.GetSection(SMTPSETTINGS
                                            .SENDGRIDRGTRANSACTIONALSMTP);
                            }

                            if (SMTPMODE == SMTPSETTINGS.eSMTPMODES.eTI_SGUSERGENERATEDMODE && bIsGAMode)
                            {
                                transactionconfig = new SMTPConfigManager();
                                transactionconfig =
                                    (SMTPConfigManager) ConfigurationManager.GetSection(SMTPSETTINGS
                                        .SENDGRIDEXTERNALSMTP);


                                if (userDomainCD == DomainFriendlyNameConstants.FreeResume)
                                    transactionconfig =
                                        (SMTPConfigManager) ConfigurationManager.GetSection(SMTPSETTINGS
                                            .SENDGRIDRGEXTERNALSMTP);
                            }

                            //******Host*****************
                            if (transactionconfig != null)
                            {
                                smtpHost = transactionconfig.SMTPServer;
                                smtpPort = transactionconfig.Port;

                                var client = new SmtpClient(smtpHost, smtpPort);

                                if (!string.IsNullOrEmpty(transactionconfig.UserName) &&
                                    !string.IsNullOrEmpty(transactionconfig.Password))
                                {
                                    smtpUserName = transactionconfig.UserName;
                                    smtpPassword = transactionconfig.Password;
                                    client.Credentials = new NetworkCredential(smtpUserName, smtpPassword);
                                }

                                //Debug.WriteLine(mailMessage.Body.ToString());
                                sendMailStatus = SendEmail(mailMessage, client);
                            }
                            else
                            {
                                throw new ApplicationException(
                                    "Failed to send the email using smtpsettings : Please Check the Settings into web.config for the SMTP MODE ");
                            }
                        }
                        catch (Exception ex)
                        {
                            var objMethod = (MethodInfo) MethodBase.GetCurrentMethod();
                            GlobalExceptionHandler.customExceptionHandler(ex, objMethod, null, "Mailer", "CG",
                                ErrorSeverityType.Normal, "Error sending mail using SMTP no : " + SMTPMODE, true);
                        }
                    else
                        sendMailStatus = SendEmail(mailMessage);
                }
            }
            catch (SmtpException ex)
            {
                var objMethod = (MethodInfo) MethodBase.GetCurrentMethod();
                GlobalExceptionHandler.customExceptionHandler(ex, objMethod, null, "Communications", "CG",
                    ErrorSeverityType.High, "Error Sending Email from Template Mailer", true);
            }
            catch (Exception)
            {
            }

            return sendMailStatus;
        }

        public static bool SendEmail(MailMessage msg)
        {
            var mailclient =
                new SmtpClient(); //Don't configure SmtpClient here, its configured automatically through web.config
            return SendEmail(msg, mailclient);
        }

        public static bool SendEmail(MailMessage msg, SmtpClient mailclient)
        {
            try
            {
                mailclient.Send(msg);
                return true;
            }
            catch (SmtpFailedRecipientsException sfrex)
            {
                //there is one inner exception for each failed recipient.
                for (var i = 0; i < sfrex.InnerExceptions.Length; i++)
                {
                    var htParams = new Hashtable(2);

                    var ctr = 1;
                    foreach (var addr in msg.To)
                    {
                        htParams.Add("MailAddress" + ctr, addr.Address);
                        ctr++;
                    }

                    htParams.Add("Mail Message", DumpMailMessage(msg));
                    htParams.Add("SMTP Settings", DumpSMTPClientSettings(mailclient));
                    var message = string.Format("Failed to deliver message to {0}.SMTP Status Code is {1}.",
                        sfrex.InnerExceptions[i].FailedRecipient, sfrex.StatusCode.ToString());

                    var objMethod = (MethodInfo) MethodBase.GetCurrentMethod();
                    GlobalExceptionHandler.customExceptionHandler(sfrex, objMethod, htParams, "Communication", "CG",
                        ErrorSeverityType.Low, message, true);
                }

                return false;
            }
            catch (SmtpException smex)
            {
                //  (2/17/2010): We DO NOT throw exceptions from here. Its logged and swallowed.
                var htParams = new Hashtable(2);
                var ctr = 1;
                foreach (var addr in msg.To)
                {
                    htParams.Add("MailAddress" + ctr, addr.Address);
                    ctr++;
                }

                htParams.Add("Mail Message", DumpMailMessage(msg));
                htParams.Add("SMTP Settings", DumpSMTPClientSettings(mailclient));
                var message = string.Format("Failed to send email. SMTP Status Code is {0}.",
                    smex.StatusCode.ToString());

                //CustomException error = new CustomException(smex);
                var objMethod = (MethodInfo) MethodBase.GetCurrentMethod();
                GlobalExceptionHandler.customExceptionHandler(smex, objMethod, htParams, "Communication", "CG",
                    ErrorSeverityType.Low, message, true);

                return false;
            }
            catch (Exception ex)
            {
                var htParams = new Hashtable(2);
                htParams.Add("Mail Message", DumpMailMessage(msg));
                htParams.Add("SMTP Settings", DumpSMTPClientSettings(mailclient));

                var objMethod = (MethodInfo) MethodBase.GetCurrentMethod();
                GlobalExceptionHandler.customExceptionHandler(ex, objMethod, htParams, ErrorSeverityType.Normal);
                return false;
            }
        }

        public static string DumpMailMessage(MailMessage msg)
        {
            if (msg == null) return "Email message is empty";

            var sb = new StringBuilder();

            sb.AppendLine("");
            sb.AppendLine("---Email dump Starts at " + DateTime.Now.ToString("dd/MM/yy - HH:mm:ss") + "------");

            //From
            sb.AppendLine("From :");
            if (msg.From == null || msg.From.Address == null || msg.From.Address.Trim().Length == 0)
            {
                sb.Append("NOT SPECIFIED OR NULL");
            }
            else
            {
                if (!string.IsNullOrEmpty(msg.From.DisplayName))
                    sb.AppendLine(msg.From.DisplayName + "<" + msg.From.Address + ">");
                else
                    sb.AppendLine(msg.From.Address + "");
            }

            //To
            sb.AppendLine("To :");
            if (msg.To == null || msg.To.Count == 0)
                sb.AppendLine("NOT SPECIFIED");
            else
                for (var i = 0; i < msg.To.Count; i++)
                    if (!string.IsNullOrEmpty(msg.To[i].DisplayName))
                        sb.AppendLine(msg.To[i].DisplayName + "<" + msg.To[i].Address + ">;");
                    else
                        sb.AppendLine(msg.To[i].Address + ";");

            sb.AppendLine("CC :");
            if (msg.CC == null || msg.CC.Count == 0)
                sb.AppendLine("NOT SPECIFIED");
            else
                for (var i = 0; i < msg.CC.Count; i++)
                    if (!string.IsNullOrEmpty(msg.CC[i].DisplayName))
                        sb.AppendLine(msg.CC[i].DisplayName + "<" + msg.CC[i].Address + ">;");
                    else
                        sb.AppendLine(msg.CC[i].Address + ";");

            sb.AppendLine("BCC :");
            if (msg.Bcc == null || msg.Bcc.Count == 0)
                sb.AppendLine("NOT SPECIFIED");
            else
                for (var i = 0; i < msg.Bcc.Count; i++)
                    if (!string.IsNullOrEmpty(msg.Bcc[i].DisplayName))
                        sb.AppendLine(msg.Bcc[i].DisplayName + "<" + msg.Bcc[i].Address + ">;");
                    else
                        sb.AppendLine(msg.Bcc[i].Address + ";");

            sb.AppendLine("");
            sb.AppendLine("Priority :" + msg.Priority + "");
            sb.AppendLine("IsBodyHtml :" + msg.IsBodyHtml + "");

            //if (msg.ReplyTo != null)
            //    sb.AppendLine("ReplyTo :" + msg.ReplyTo.Address + "");
            if (msg.ReplyToList != null && msg.ReplyToList.Count > 0)
                sb.AppendLine("ReplyTo :" + msg.ReplyToList[0].Address);

            sb.AppendLine("Attachment count :" + msg.Attachments.Count + "");
            sb.AppendLine("Subject :" + msg.Subject + "");
            sb.AppendLine("Body :" + msg.Body + "");

            return sb.ToString();
        }

        private static string DumpSMTPClientSettings(SmtpClient client)
        {
            var sb = new StringBuilder();

            sb.AppendLine("Mail Server:[" + client.Host + "]");
            sb.AppendLine(" Server Port:[" + client.Port + "]");
            sb.AppendLine(" UseDefaultCredentials:[" + client.UseDefaultCredentials + "]");

            if (!client.UseDefaultCredentials && client.Credentials != null)
            {
                sb.AppendLine(" UserName:[" + ((NetworkCredential) client.Credentials).UserName + "]");
                sb.AppendLine(" Password:[" + ((NetworkCredential) client.Credentials).Password + "]");
            }

            sb.AppendLine(" DeliveryMethod:[" + client.DeliveryMethod + "]");

            if (client.DeliveryMethod == SmtpDeliveryMethod.SpecifiedPickupDirectory)
                sb.AppendLine(" PickupDirectoryLocation:[" + client.PickupDirectoryLocation + "]");

            return sb.ToString();
        }

        /// <summary>
        ///     Processes a template and fills in data
        /// </summary>
        /// <param name="recipientProperties"></param>
        /// <returns></returns>
        private StringDictionary ProcessTemplate(StringDictionary recipientProperties)
        {
            StringDictionary sdRecipientTemplate = null;

            try
            {
                if (sdTemplate != null)
                {
                    //Initializing sdRecipientTemplate here
                    sdRecipientTemplate = new StringDictionary();
                    var sPattern = MailerConstants.EMAIL_DATA_DELIMITER + "{0}[^\\n]+?" +
                                   MailerConstants.EMAIL_DATA_DELIMITER;

                    foreach (string sKey in sdTemplate.Keys)
                    {
                        var sValue = sdTemplate[sKey];
                        sValue = ReplaceRecipientData(string.Format(sPattern, MailerConstants.EMAILTAG_CDI), sValue,
                            recipientProperties);
                        sdRecipientTemplate.Add(sKey, sValue);
                    }
                }
            }
            catch (Exception ex)
            {
                var htParams = new Hashtable(2);
                htParams.Add("sdTemplateCount", sdTemplate != null ? sdTemplate.Count.ToString() : "<NULL>");
                htParams.Add("recipientPropertiesCount",
                    recipientProperties != null ? recipientProperties.Count.ToString() : "<NULL>");
                var objMethod = (MethodInfo) MethodBase.GetCurrentMethod();
                GlobalExceptionHandler.customExceptionHandler(ex, objMethod, htParams, ErrorSeverityType.Normal);
            }

            return sdRecipientTemplate;
        }

        public string ReplaceRecipientData(string sPattern, string sTemplate, StringDictionary recipientProperties)
        {
            var sValue = "";
            var sKey = "";

            var bCurrency = false;
            var bURL = false;

            try
            {
                if (Regex.IsMatch(sTemplate, sPattern, RegexOptions.Singleline))
                    foreach (Match oMatch in Regex.Matches(sTemplate, sPattern, RegexOptions.Singleline))
                        if (oMatch.Success)
                        {
                            sKey = oMatch.Value;

                            if (sKey.IndexOf(MailerConstants.EMAILTAG_FORMAT_URL) > 0)
                                bURL = true;
                            else
                                bURL = false;

                            if (sKey.IndexOf(MailerConstants.EMAILTAG_FORMAT_CUR) > 0)
                                bCurrency = true;
                            else
                                bCurrency = false;

                            sKey = sKey.Replace(MailerConstants.EMAIL_DATA_DELIMITER, "")
                                .Replace(MailerConstants.EMAILTAG_CDI, "")
                                .Replace(MailerConstants.EMAILTAG_FORMAT_CUR, "")
                                .Replace(MailerConstants.EMAILTAG_FORMAT_URL, "");

                            if (recipientProperties.ContainsKey(sKey)) sValue = recipientProperties[sKey];

                            if (bCurrency & !string.IsNullOrEmpty(sValue)) sValue = string.Format("{0:C}", sValue);

                            if (bURL & !string.IsNullOrEmpty(sValue))
                                //Or is it EscapeURIString?
                                sValue = Uri.EscapeDataString(sValue);

                            sTemplate = sTemplate.Replace(oMatch.Value, sValue);
                        }
            }
            catch (Exception ex)
            {
                throw ex;
            }

            return sTemplate;
        }

        private void SetTemplate(string sTemplate)
        {
            _sdTemplate = new StringDictionary();
            SetMailFields(sTemplate, _sdTemplate);
        }

        public string RetrieveFields(string sKey, string sPattern, string sTemplate)
        {
            var sValue = string.Empty;
            try
            {
                if (Regex.IsMatch(sTemplate, sPattern, RegexOptions.Singleline))
                    foreach (Match oMatch in Regex.Matches(sTemplate, sPattern, RegexOptions.Singleline))
                        if (oMatch.Success)
                            sValue = oMatch.Value.Replace("[" + sKey + "]", "");
            }
            catch (Exception ex)
            {
                var htParams = new Hashtable(3);
                htParams.Add("sKey", sKey);
                htParams.Add("sPattern", sPattern);
                htParams.Add("sTemplate", sTemplate);
                var objMethod = (MethodInfo) MethodBase.GetCurrentMethod();
                GlobalExceptionHandler.customExceptionHandler(ex, objMethod, htParams, ErrorSeverityType.Normal);
            }

            return sValue;
        }

        private void SetMailFields(string sTemplate, StringDictionary sdTemplate)
        {
            var sKey = string.Empty;
            var sValue = string.Empty;

            try
            {
                //**************************************************************************
                //MailoutTrace.AddLog("Setting mail Fields")
                sKey = "To";
                sValue = RetrieveFields(sKey, "\\[" + sKey + "\\]\\r\\n+[^\\n]+", sTemplate);
                if (!string.IsNullOrEmpty(sValue)) sdTemplate.Add(TemplateIndex.eTI_To, sValue);

                //else
                //    throw new InvalidOperationException("Failed to locate [To] field in the template");

                //***********************************************************************
                sKey = "Subject";
                sValue = RetrieveFields(sKey, "\\[" + sKey + "\\]\\r\\n+[^\\n]+", sTemplate);
                if (!string.IsNullOrEmpty(sValue)) sdTemplate.Add(TemplateIndex.eTI_Subject, sValue);

                //***********************************************************************
                sKey = "From";
                sValue = RetrieveFields(sKey, "\\[" + sKey + "\\]\\r\\n+[^\\n]+", sTemplate);
                if (!string.IsNullOrEmpty(sValue)) sdTemplate.Add(TemplateIndex.eTI_From, sValue);

                //***********************************************************************
                sKey = "CC";
                sValue = RetrieveFields(sKey, "\\[" + sKey + "\\]\\r\\n+[^\\n]+", sTemplate);
                if (!string.IsNullOrEmpty(sValue)) sdTemplate.Add(TemplateIndex.eTI_CC, sValue);

                //***********************************************************************
                sKey = "BCC";
                sValue = RetrieveFields(sKey, "\\[" + sKey + "\\]\\r\\n+[^\\n]+", sTemplate);
                if (!string.IsNullOrEmpty(sValue)) sdTemplate.Add(TemplateIndex.eTI_BCC, sValue);

                //***********************************************************************
                sKey = "Attachment";
                sValue = RetrieveFields(sKey, "\\[" + sKey + "\\]\\r\\n+[^\\n]+", sTemplate);
                if (!string.IsNullOrEmpty(sValue)) sdTemplate.Add(TemplateIndex.eTI_To, sValue);

                //***********************************************************************
                sKey = "SMTPServer";
                sValue = RetrieveFields(sKey, "\\[" + sKey + "\\]\\r\\n+[^\\n]+", sTemplate);
                if (!string.IsNullOrEmpty(sValue)) sdTemplate.Add(TemplateIndex.eTI_SMTPServer, sValue);

                //***********************************************************************
                sKey = "SMTPPort";
                sValue = RetrieveFields(sKey, "\\[" + sKey + "\\]\\r\\n+[^\\n]+", sTemplate);
                if (!string.IsNullOrEmpty(sValue)) sdTemplate.Add(TemplateIndex.eTI_SMTPPort, sValue);

                //***********************************************************************
                sKey = "SMTPUsername";
                sValue = RetrieveFields(sKey, "\\[" + sKey + "\\]\\r\\n+[^\\n]+", sTemplate);
                if (!string.IsNullOrEmpty(sValue)) sdTemplate.Add(TemplateIndex.eTI_SMTPUsername, sValue);

                //***********************************************************************
                sKey = "SMTPPassword";
                sValue = RetrieveFields(sKey, "\\[" + sKey + "\\]\\r\\n+[^\\n]+", sTemplate);
                if (!string.IsNullOrEmpty(sValue)) sdTemplate.Add(TemplateIndex.eTI_SMTPPassword, sValue);

                //**********************************************************************
                sKey = "HTMLBody";
                sValue = RetrieveFields(sKey, "\\[" + sKey + "\\](.+)", sTemplate);
                if (!string.IsNullOrEmpty(sValue)) sdTemplate.Add(TemplateIndex.eTI_HTMLBody, sValue);

                //**********************************************************************
                sKey = "Body";
                sValue = RetrieveFields(sKey, "\\[" + sKey + "\\](.+)", sTemplate);
                if (!string.IsNullOrEmpty(sValue)) sdTemplate.Add(TemplateIndex.eTI_TextBody, sValue);

                //**********************************************************************
                sKey = "TextBody";
                sValue = RetrieveFields(sKey, "\\[" + sKey + "\\](.+)", sTemplate);
                if (!string.IsNullOrEmpty(sValue)) sdTemplate.Add(TemplateIndex.eTI_TextBody, sValue);

                //**********************************************************************

                //**********************************************************************
                sKey = "ReplyTo";
                sValue = RetrieveFields(sKey, "\\[" + sKey + "\\]\\r\\n+[^\\n]+", sTemplate);
                if (!string.IsNullOrEmpty(sValue)) sdTemplate.Add(TemplateIndex.eTI_ReplyTo, sValue);

                //**********************************************************************

                //Code added by AB on 27/09/2012 to add custom headers
                if (sTemplate.Contains("[Headers]") && sTemplate.Contains("[HTMLBody]"))
                {
                    var sCustomHeaders = string.Empty;
                    var iStartIndex = sTemplate.IndexOf("[Headers]") + 9;
                    var iLastIndex = sTemplate.IndexOf("[HTMLBody]");
                    sCustomHeaders = sTemplate.Substring(iStartIndex, iLastIndex - iStartIndex).Replace("\r", "");
                    sdTemplate.Add(TemplateIndex.eTI_Header, sCustomHeaders);
                }
            }
            catch (Exception ex)
            {
                var htParams = new Hashtable(2);
                htParams.Add("sTemplate", sTemplate);
                htParams.Add("sdTemplate", sdTemplate != null ? sdTemplate.ToString() : "<NULL>");
                var objMethod = (MethodInfo) MethodBase.GetCurrentMethod();
                GlobalExceptionHandler.customExceptionHandler(ex, objMethod, htParams, ErrorSeverityType.Normal);
            }
        }

        public void SendWelcomeEmail(int userId, string customerName, string customerEmail)
        {
            CommunicationService svcComm = null;
            StringDictionary recipientProperties = null;

            try
            {
                if (userId > 0)
                {
                    svcComm = this;

                    svcComm.SMTPMODE = SMTPSETTINGS.eSMTPMODES.eTI_SGUSERGENERATEDMODE;

                    CreateCommonUrlsQueryParam(MailerConstants.eMailOut.eWelcome, userId,
                        TemplateName.WELCOME.ToLower(), domainCd);

                    recipientProperties = new StringDictionary
                    {
                        {EmailConstants.CURRENT_DATE, DateTime.Now.ToString("d-MMM-yyyy")},
                        {
                            EmailConstants.CUSTOMER_NAME,
                            string.IsNullOrEmpty(customerName) ? customerEmail : customerName
                        },
                        {EmailConstants.EMAIL_ADDRESS, customerEmail}
                    };


                    if (recipientProperties.Count > 0)
                    {
                        AddUrlProperties(recipientProperties);

                        svcComm.SendEmail(MailerConstants.eMailOut.eWelcome, recipientProperties);
                        SaveEmailSent(userId, 0, (int) MailerConstants.eMailOut.eWelcome, DateTime.Now, false);

                        EmailTrackingAnalytics(userId, TemplateName.WELCOME, DateTime.Now.ToShortDateString());
                    }
                }
            }
            catch (Exception ex)
            {
                var htParams = new Hashtable(2);
                htParams.Add("WelcomeEmail", "WelcomeEmail");
                htParams.Add("CustomerEmail", customerEmail);
                var objMethod = (MethodInfo) MethodBase.GetCurrentMethod();
                GlobalExceptionHandler.customExceptionHandler(ex, objMethod, htParams, ErrorSeverityType.Normal);
            }
        }

        public bool SendPasswordResetEmail(string userName, string newPassword, int userId, string userDomainCd = "")
        {
            var sendPasswordEmailStatus = false;
            CommunicationService svcComm = null;
            StringDictionary recipientProperties = null;

            var loginUrl = string.Empty;

            try
            {
                if (!string.IsNullOrEmpty(userName) && !string.IsNullOrEmpty(newPassword))
                {
                    svcComm = this;

                    svcComm.SMTPMODE = SMTPSETTINGS.eSMTPMODES.eTI_SGTRANSACTIONALMODE;

                    // create query string for common urls
                    CreateCommonUrlsQueryParam(MailerConstants.eMailOut.ePasswordReset, userId,
                        TemplateName.PASSWORD_EMAIL, domainCd);
                    // Create query string for Login page
                    loginUrl = CreateUrlQueryParam(MailerConstants.eMailOut.ePasswordReset, PageUrlName.LOGIN, userId,
                        domainCd, TemplateName.PASSWORD_EMAIL, "", false);

                    recipientProperties = new StringDictionary
                    {
                        {EmailConstants.CURRENT_DATE, DateTime.Now.ToString("d-MMM-yyyy")},
                        {EmailConstants.EMAIL_ADDRESS, userName},
                        {EmailConstants.USERNAME, userName},
                        {EmailConstants.PASSWORD, newPassword},
                        {EmailConstants.LOGIN_URL_QP, loginUrl}
                    };


                    if (recipientProperties.Count > 0)
                    {
                        AddUrlProperties(recipientProperties);

                        if (svcComm.SendEmail(MailerConstants.eMailOut.ePasswordReset, recipientProperties, null,
                            userDomainCd))
                        {
                            SaveEmailSent(MailerConstants.eMailOut.ePasswordReset, DateTime.Now, false);
                            EmailTrackingAnalytics(userId, TemplateName.PASSWORD_EMAIL,
                                DateTime.Now.ToShortDateString());
                            sendPasswordEmailStatus = true;
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                var htParams = new Hashtable(2);
                htParams.Add("PasswordReset", "PasswordReset");
                htParams.Add("CustomerEmail", userName);
                var objMethod = (MethodInfo) MethodBase.GetCurrentMethod();
                GlobalExceptionHandler.customExceptionHandler(ex, objMethod, htParams, ErrorSeverityType.Normal);
            }

            return sendPasswordEmailStatus;
        }

        public bool SendPasswordRecoveryEmail(int userId, string userEmail, string userFirstName,
            string passwordResetLink, string userDomainCd = "", string loginLink = "")
        {
            var sendPasswordEmailStatus = false;
            CommunicationService svcComm = null;
            StringDictionary recipientProperties = null;

            var loginUrl = string.Empty;

            try
            {
                if (!string.IsNullOrEmpty(userEmail) && !string.IsNullOrEmpty(passwordResetLink))
                {
                    svcComm = this;

                    svcComm.SMTPMODE = SMTPSETTINGS.eSMTPMODES.eTI_SGTRANSACTIONALMODE;

                    // create query string for common urls
                    CreateCommonUrlsQueryParam(MailerConstants.eMailOut.ePasswordReset, userId,
                        TemplateName.PASSWORD_EMAIL, domainCd);
                    // Create query string for Login page
                    loginUrl = loginLink + CreateUrlQueryParam(MailerConstants.eMailOut.ePasswordReset,
                                   PageUrlName.LOGIN, userId,
                                   domainCd, TemplateName.PASSWORD_EMAIL, "", false);


                    recipientProperties = new StringDictionary
                    {
                        {EmailConstants.CURRENT_DATE, DateTime.Now.ToString("d-MMM-yyyy")},
                        {EmailConstants.EMAIL_ADDRESS, userEmail},
                        {EmailConstants.USERNAME, userEmail},
                        {EmailConstants.FIRST_NAME, userFirstName},
                        {EmailConstants.PASSWORD_RESET_LINK, passwordResetLink},
                        {EmailConstants.LOGIN_URL_QP, loginUrl}
                    };


                    if (recipientProperties.Count > 0)
                    {
                        AddUrlProperties(recipientProperties);

                        if (svcComm.SendEmail(MailerConstants.eMailOut.ePasswordResetByUser, recipientProperties, null,
                            userDomainCd))
                        {
                            SaveEmailSent(MailerConstants.eMailOut.ePasswordReset, DateTime.Now, false);
                            EmailTrackingAnalytics(userId, TemplateName.PASSWORD_EMAIL,
                                DateTime.Now.ToShortDateString());
                            sendPasswordEmailStatus = true;
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                var htParams = new Hashtable(2);
                htParams.Add("PasswordReset", "PasswordReset");
                htParams.Add("CustomerEmail", userEmail);
                var objMethod = (MethodInfo) MethodBase.GetCurrentMethod();
                GlobalExceptionHandler.customExceptionHandler(ex, objMethod, htParams, ErrorSeverityType.Normal);
            }

            return sendPasswordEmailStatus;
        }

        public bool SendContactUsEmail(string to, string domainCD, string emailSubject, string body, string name,
            string ctname, string ctAddress)
        {
            var mailBody = new StringBuilder();
            mailBody.Append("<table width=500px;><tr><td>");
            mailBody.Append("<table>");
            mailBody.AppendFormat("<tr><td>{0} </td><td>{1}</td></tr>", ctname, name);
            mailBody.AppendFormat("<tr><td>{0} </td><td>{1}</td></tr>", ctAddress, to);
            mailBody.Append("<tr><td colspan=\"2\"></td></tr>");
            mailBody.Append("<tr><td colspan=\"2\">" + body + "</td></tr>");
            mailBody.Append("</table>");
            mailBody.Append("</td></tr></Table>");
            var selectedServiceType = emailSubject;
            if (domainCD == DomainFriendlyNameConstants.FresherResume)
                selectedServiceType = selectedServiceType.Replace("RESUMEGIG", "RESUMEHELP");


            var toAddress = ConfigManager.GetConfig(selectedServiceType);
            var sdRecipients = new StringDictionary
            {
                {TemplateIndex.eTI_To, toAddress},
                {TemplateIndex.eTI_From, to},
                {TemplateIndex.eTI_Subject, emailSubject},
                {TemplateIndex.eTI_HTMLBody, mailBody.ToString()},
                {TemplateIndex.eTI_Header, "SendGrid=send_contactus"}
            };
            return SendEmail(sdRecipients, null);
        }

        public static bool SendHtmlEmail(string from, string to, string subject, string body)
        {
            var msg = new MailMessage();

            msg = new MailMessage(from, to, subject, body);
            msg.IsBodyHtml = true;

            return SendEmail(msg);
        }

        public string GetAdminEmailFromConfig()
        {
            var TO_RECURRING_ADMIN_MAIL = string.Empty;
            try
            {
                TO_RECURRING_ADMIN_MAIL = ConfigManager.GetConfig(MailerConstants.RECURRING_ADMIN_MAIL);
            }
            catch (Exception ex)
            {
                var htParams = new Hashtable(1);
                htParams.Add("TO_RECURRING_ADMIN_MAIL", TO_RECURRING_ADMIN_MAIL);
                var objMethod = (MethodInfo) MethodBase.GetCurrentMethod();
                GlobalExceptionHandler.customExceptionHandler(ex, objMethod, htParams, "CG", "CG",
                    ErrorSeverityType.Info, "Value not added in Web.config for TO_RECURRING_ADMIN_MAIL ", true);
                // testing url
                TO_RECURRING_ADMIN_MAIL = "sanjay@Resumegig.com";
            }

            return TO_RECURRING_ADMIN_MAIL;
        }

        /// <summary>
        ///     Process email template HTML and set path for images and URLs, reads DomainName from Context
        /// </summary>
        /// <param name="sbTemplateContent">HTML of the email</param>
        /// <returns>HTML with we-urls for images and pages</returns>
        private string ProcessEmailTemplate(StringBuilder sbTemplateContent)
        {
            if (!string.IsNullOrEmpty(sbTemplateContent.ToString())) ProcessEmailTemplate(sbTemplateContent, domainCd);
            return sbTemplateContent.ToString();
        }

        /// <summary>
        ///     Process email template HTML and set path for images and URLs, accepts DomainName
        /// </summary>
        /// <param name="sbTemplateContent"></param>
        /// <param name="sDomainCD"></param>
        /// <returns></returns>
        private string ProcessEmailTemplate(StringBuilder sbTemplateContent, string sDomainCD)
        {
            var sImagePath = string.Empty;
            var sCDNDomain = string.Empty;
            var sDomain = string.Empty;
            var sUrlReplace = string.Empty;
            try
            {
                if (!string.IsNullOrEmpty(Convert.ToString(sbTemplateContent)))
                {
                    // SET URLs SERVER
                    sUrlReplace = ConfigManager.GetConfig(MailerConstants.EMAIL_URL_REPLACE);
                    sCDNDomain = ConfigManager.GetConfig(MailerConstants.CDN_DOMAIN_NAME);
                    // Read CDN URL, Domain Name URL for websites
                    sDomain = CGUtility.GetDomainURLFromConfig();
                    sbTemplateContent.Replace(sUrlReplace, sDomain);
                    sImagePath = sDomain + ConfigManager.GetConfig(MailerConstants.EMAIL_IMAGE_PATH);
                    var sEmailImageReplace = ConfigManager.GetConfig(MailerConstants.EMAIL_IMAGE_REPLACE);

                    if (!string.IsNullOrEmpty(sImagePath))
                        // Set Image Path
                        sbTemplateContent.Replace(sEmailImageReplace, sImagePath);
                    sbTemplateContent.Replace(ConfigManager.GetConfig(MailerConstants.TWITTER_URL_REPLACE),
                        ConfigManager.GetConfig(MailerConstants.TWITTER_URL));
                    sbTemplateContent.Replace(ConfigManager.GetConfig(MailerConstants.FACEBOOK_URL_REPLACE),
                        ConfigManager.GetConfig(MailerConstants.FACEBOOK_URL));
                    sbTemplateContent.Replace(ConfigManager.GetConfig(MailerConstants.GOOGLE_URL_REPLACE),
                        ConfigManager.GetConfig(MailerConstants.GOOGLE_PLUS_URL));
                    sbTemplateContent.Replace(ConfigManager.GetConfig(MailerConstants.LINKEDIN_URL_REPLACE),
                        ConfigManager.GetConfig(MailerConstants.LINKEDIN_URL));
                }
            }
            catch (Exception ex)
            {
                var htParams = new Hashtable(2);
                htParams.Add("sbTemplateContent", sbTemplateContent != null ? sbTemplateContent.ToString() : "NULL");
                htParams.Add("DomainCD", sDomainCD != null ? sDomainCD : "NULL");
                var objMethod = (MethodInfo) MethodBase.GetCurrentMethod();
                GlobalExceptionHandler.customExceptionHandler(ex, objMethod, htParams, "CG", "CG",
                    ErrorSeverityType.Info, "Value not added in Web.config for ProcessEmailTemplate method ", true);
                // testing url                
            }

            return Convert.ToString(sbTemplateContent);
        }

        public bool SendResumeReviewByMail(string sFileFormatType, File dmFile, string sTo, string sFrom,
            string sSubject, string sBody)
        {
            var result = false;
            var userID = 0;

            if (dmFile != null)
            {
                var process = new DocumentsProcess();

                var attachment = process.GetAttachmentStream(sFileFormatType, dmFile);

                if (attachment != null)
                {
                    var item = new Attachment(attachment, dmFile.Name + "." + sFileFormatType.ToLower(), null);

                    var domainName = domainCd;

                    if (string.IsNullOrEmpty(sFrom.Trim())) sFrom = CGUtility.GetCustomerServiceEmail(domainName);

                    var sdRecipients = new StringDictionary
                    {
                        {TemplateIndex.eTI_To, sTo},
                        {TemplateIndex.eTI_From, sFrom},
                        {TemplateIndex.eTI_Subject, sSubject},
                        {TemplateIndex.eTI_HTMLBody, "<table width=500px;><tr><td>" + sBody + "</td></tr></Table>"},
                        {TemplateIndex.eTI_Header, "SendGrid=send_resume_attachment"}
                    };

                    if (sdRecipients.Count > 0)
                    {
                        IList<Attachment> lstAttachment = new List<Attachment>();
                        lstAttachment.Add(item);

                        SMTPMODE = SMTPSETTINGS.eSMTPMODES.eTI_SGUSERGENERATEDMODE;
                        result = SendEmail(sdRecipients, lstAttachment);

                        userID = DocumentFactory.DocumentDAL.GetUserIdByFileId(dmFile);
                        SaveEmailSent(userID, 0, (int) MailerConstants.eMailOut.eSendResumeReview, DateTime.Now, false);
                    }
                }
            }

            return result;
        }

        public bool SendResumeByMail(string sFileFormatType, File docFile, string sTo, string sUserName,
            string sSubject, string sBody)
        {
            var result = false;
            var sFrom = string.Empty;
            var userID = -1;

            if (docFile != null)
            {
                var process = new DocumentsProcess();

                var attachment = process.GetAttachmentStream(sFileFormatType, docFile);

                if (attachment != null)
                {
                    var item = new Attachment(attachment, docFile.Name + "." + sFileFormatType.ToLower(), null);
                    var domainName = domainCd;
                    sFrom = CGUtility.GetCustomerServiceEmail(domainName);
                    var sdRecipients = new StringDictionary
                    {
                        {TemplateIndex.eTI_To, sTo},
                        {TemplateIndex.eTI_From, sFrom},
                        {TemplateIndex.eTI_Subject, sSubject},
                        {TemplateIndex.eTI_HTMLBody, "<table width=600px;><tr><td>" + sBody + "</td></tr></Table>"},
                        {TemplateIndex.eTI_Header, "SendGrid=send_resume_attachment"},
                        {TemplateIndex.eTI_ReplyTo, sUserName}
                    };

                    if (sdRecipients.Count > 0)
                    {
                        IList<Attachment> lstAttachment = new List<Attachment>();
                        lstAttachment.Add(item);

                        SMTPMODE = SMTPSETTINGS.eSMTPMODES.eTI_SGUSERGENERATEDMODE;
                        result = SendEmail(sdRecipients, lstAttachment);

                        userID = UserFactory.UserDAL.GetUserIDByEmail(sUserName);
                        SaveEmailSent(userID, 0, (int) MailerConstants.eMailOut.eSendResume, DateTime.Now, false);
                    }
                }
            }

            return result;
        }


        public void SaveEmailSent(int UserID, int OrderID, int MailoutID, DateTime TimeStamp, bool IsResent)
        {
            try
            {
                var oEmailSent = new EmailSent();
                if (UserID > 0)
                    oEmailSent.UserID = UserID;
                if (OrderID > 0)
                    oEmailSent.OrderID = OrderID;
                oEmailSent.MailoutID = MailoutID;
                oEmailSent.TimeStamp = TimeStamp;
                oEmailSent.IsResent = IsResent;

                CommunicationFactory.EmailSentDAL.Save(oEmailSent, true);
            }
            catch (Exception Ex)
            {
                var objMethod = (MethodInfo) MethodBase.GetCurrentMethod();
                GlobalExceptionHandler.customExceptionHandler(Ex, objMethod, null, ErrorSeverityType.Normal);
            }
        }

        /// <summary>
        ///     Save EmailSent data when UserID and OrderID are not available
        /// </summary>
        /// <param name="Mailout"></param>
        /// <param name="TimeStamp"></param>
        /// <param name="IsResent"></param>
        public void SaveEmailSent(MailerConstants.eMailOut Mailout, DateTime TimeStamp, bool IsResent)
        {
            try
            {
                SaveEmailSent(0, 0, (int) Mailout, TimeStamp, IsResent);
            }
            catch (Exception Ex)
            {
                var objMethod = (MethodInfo) MethodBase.GetCurrentMethod();
                GlobalExceptionHandler.customExceptionHandler(Ex, objMethod, null, ErrorSeverityType.Normal);
            }
        }

        public IList<EmailSent> GetEmailSent(int UserID)
        {
            IList<EmailSent> lstEmailSent = new List<EmailSent>();
            try
            {
                lstEmailSent = CommunicationFactory.EmailSentDAL.GetEmailSent(UserID);
            }
            catch (Exception ex)
            {
                var htParams = new Hashtable(1);
                htParams.Add("UserID", UserID);
                var objMethod = (MethodInfo) MethodBase.GetCurrentMethod();
                GlobalExceptionHandler.customExceptionHandler(ex, objMethod, htParams, ErrorSeverityType.Normal);
            }

            return lstEmailSent;
        }

        public IList<EmailSentDetailDTO> GetEmailSentByUserID(int UserID)
        {
            IList<EmailSentDetailDTO> lstEmailSent = null;
            try
            {
                lstEmailSent = CommunicationFactory.EmailSentDTODAL.getEmailSentByUserID(UserID);
            }
            catch (Exception ex)
            {
                var htParams = new Hashtable(1);
                htParams.Add("UserID", UserID);
                var objMethod = (MethodInfo) MethodBase.GetCurrentMethod();
                GlobalExceptionHandler.customExceptionHandler(ex, objMethod, htParams, ErrorSeverityType.Normal);
            }

            return lstEmailSent;
        }

        public void EmailTrackingAnalytics(int userId, string campaignType, string dateSent)
        {
        }

        /// <summary>
        ///     Prepare Url query string with parameters, creates eguid if not present
        /// </summary>
        /// <param name="mailOutId"></param>
        /// <param name="linkName"></param>
        /// <param name="userId"></param>
        /// <param name="domainCD"></param>
        /// <param name="template"></param>
        /// <returns></returns>
        public string CreateUrlQueryParam(MailerConstants.eMailOut mailOut, string linkName, int userId,
            string domainCD, string template, string userGuid = "", bool isAuthenticatedLink = true)
        {
            UserData userData = null;
            EmailLink emailLink = null;
            StringBuilder sbUrl = null;

            var mailOutId = -1;

            try
            {
                mailOutId = (int) mailOut;
                //string UserGuid = string.Empty;
                if (isAuthenticatedLink)
                {
                    if (userGuid == string.Empty)
                    {
                        userData = UserFactory.UserDataDAL.GetUserDataByDataTypeCD(userId, UserDataType.Email_Guid);
                        if (userData == null)
                        {
                            userData = new UserData(userId, UserDataType.Email_Guid, Guid.NewGuid().ToString());
                            UserFactory.UserDataDAL.SaveOrUpdate(userData, true);
                        }

                        userGuid = userData.Data;
                    }
                }
                else
                {
                    userGuid = string.Empty;
                }

                emailLink = CommunicationFactory.EmailLinkDAL.GetUniqueEmailLink(mailOutId, linkName);
                if (emailLink != null)
                {
                    sbUrl = new StringBuilder(string.Empty);
                    sbUrl.Append("?");
                    sbUrl.Append("lid=" + emailLink.Id);
                    sbUrl.Append("&eguid=" + userGuid);
                    sbUrl.Append("&uid=" + userId);
                    sbUrl.Append("&utm_source=" + domainCD.ToLower());
                    sbUrl.Append("&utm_medium=email");
                    sbUrl.Append("&utm_campaign=" + template.ToLower());

                    return sbUrl.ToString();
                }
            }
            catch (Exception ex)
            {
                var htParams = new Hashtable(4);
                htParams.Add("MAIL_OUT_ID", mailOutId.ToString());
                htParams.Add("UserID", userId.ToString());
                if (emailLink != null) htParams.Add("EMAIL_LINK", emailLink.ToString());
                if (userData != null) htParams.Add("USER_DATA", userData.ToString());
                var objMethod = (MethodInfo) MethodBase.GetCurrentMethod();
                GlobalExceptionHandler.customExceptionHandler(ex, objMethod, htParams, SubSystem.Communications,
                    Application.WebApp, ErrorSeverityType.Normal, ex.Message, true);
            }

            return string.Empty;
        }

        /// <summary>
        ///     Create Query Params for Landing Page, User Agreement, Privacy Policy & Contact Us pages
        /// </summary>
        /// <param name="mailOut">MailOut</param>
        /// <param name="userId">UserID</param>
        /// <param name="template">Email Template Name</param>
        /// <param name="domainCd">Domain</param>
        private void CreateCommonUrlsQueryParam(MailerConstants.eMailOut mailOut, int userId, string template,
            string domainCd)
        {
            var userData = UserFactory.UserDataDAL.GetUserDataByDataTypeCD(userId, UserDataType.Email_Guid);
            if (userData == null)
            {
                userData = new UserData(userId, UserDataType.Email_Guid, Guid.NewGuid().ToString());
                UserFactory.UserDataDAL.SaveOrUpdate(userData, true);
            }

            var userGuid = userData.Data;

            landingPageUrl = CreateUrlQueryParam(mailOut,
                PageUrlName.LANDING_PAGE,
                userId,
                domainCd,
                template, "", false);

            userAgreementPageUrl = CreateUrlQueryParam(mailOut,
                PageUrlName.USER_AGREEMENT,
                userId,
                domainCd,
                template, "", false);

            privatePolicyPageUrl = CreateUrlQueryParam(mailOut,
                PageUrlName.PRIVACY_POLICY,
                userId,
                domainCd,
                template, "", false);

            contactUsPageUrl = CreateUrlQueryParam(mailOut,
                PageUrlName.CONTACT_US,
                userId,
                domainCd,
                template, "", false);

            userSettingPageUrl = CreateUrlQueryParam(mailOut,
                PageUrlName.SETTINGS_PAGE,
                userId,
                domainCd,
                template, userGuid);
            resumeHomePageUrl = CreateUrlQueryParam(mailOut,
                PageUrlName.RESUME_HOME,
                userId,
                domainCd,
                template, userGuid);
        }

        /// <summary>
        ///     Add Landing Page, User Agreement, Privacy Policy & Contact Us properties in recipientProperties
        /// </summary>
        /// <param name="rcProp">List of Email Recipient Properties</param>
        private void AddUrlProperties(StringDictionary rcProp)
        {
            if (rcProp != null && rcProp.Count > 0)
            {
                rcProp.Add(EmailConstants.LP_URL_QP, landingPageUrl);
                rcProp.Add(EmailConstants.FOTR_QP_USAG, userAgreementPageUrl);
                rcProp.Add(EmailConstants.FOTR_QP_PRPO, privatePolicyPageUrl);
                rcProp.Add(EmailConstants.FOTR_QP_CNUS, contactUsPageUrl);
                rcProp.Add(EmailConstants.UNSUBSCRIBE_URL_QP, userSettingPageUrl);
                rcProp.Add(EmailConstants.KM_PIXEL_OPEN, kmOpnPxl);
                rcProp.Add(EmailConstants.ResumeHome_URL_QP, resumeHomePageUrl);
            }
        }

        private string GetKmOpenPixelUrl(string campaignName, int userId)
        {
            var kmKey = ConfigManager.GetConfig("KISSMetrics_API_KEY");
            var kmPxl = new StringBuilder(string.Empty);

            if (string.IsNullOrEmpty(kmKey))
                return string.Empty;

            // KM Pixel - <img src="http://trk.kissmetrics.com/e?_k=YOUR_API_KEY&_n=Viewed+E-mail&_p=john%40smith.com"/>

            kmPxl.Append("<img src=\"http://trk.kissmetrics.com/e?");
            kmPxl.Append("_k=" + kmKey);
            kmPxl.Append("&_n=Viewed-Email"); // _n, Event Name = Viewed-Email
            kmPxl.Append("&_p=" + userId); // _p, Identify User = User ID
            kmPxl.Append("&campaign-type=" + campaignName); // campaign-type = welcome etc.
            kmPxl.Append("&campaign-source=" + domainCd.ToLower()); // campaign-source = rg or rh
            kmPxl.Append("\"/>");

            return kmPxl.ToString();
        }

        public EmailLink GetEmailLinkByID(int linkId)
        {
            EmailLink emailLink = null;

            if (linkId > 0)
                try
                {
                    emailLink = CommunicationFactory.EmailLinkDAL.GetById(linkId);
                }
                catch (Exception ex)
                {
                    var htParams = new Hashtable(2);
                    htParams.Add("LINK_ID", linkId.ToString());
                    if (emailLink != null)
                        htParams.Add("EMAIL_LINK", emailLink.ToString());

                    var objMethod = (MethodInfo) MethodBase.GetCurrentMethod();
                    GlobalExceptionHandler.customExceptionHandler(ex, objMethod, htParams, ErrorSeverityType.Normal);
                }

            return emailLink;
        }
    }
}