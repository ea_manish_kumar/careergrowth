﻿using System;
using DomainModel.Base.UtilityClasses;

namespace Services.Event.DataTransfer
{
    /// <summary>
    /// 
    /// </summary>
    [Serializable]
    public class EventQueryOptions
    {
        #region Enumerations

        /// <summary>
        /// enum to specify Grouping
        /// </summary>
        public enum EventGroupBy
        {
            /// <summary>
            /// date
            /// </summary>
            Date
        }

        #endregion

        private DateRange _dateRange;
        private int _documentId;

        private EventGroupBy _groupBy;

        private int _pageNumber;

        private int _pageSize;

        private int _totalPages;

        private bool _visibility;

        ///<summary>        
        ///Gets or Sets the DocumentID
        ///</summary>
        ///<value> DocumentID </value>
        public virtual int DocumentID
        {
            get => _documentId;
            set => _documentId = value;
        }

        /// <summary>
        /// get or set page size
        /// </summary>
        /// <value >PageSize</value>
        public virtual int PageSize
        {
            get => _pageSize;
            set => _pageSize = value;
        }

        /// <summary>
        /// get or set pageno
        /// </summary>
        /// <value>Page Number</value> 
        public int PageNumber
        {
            get => _pageNumber;
            set => _pageNumber = value;
        }

        /// <summary>
        /// 
        /// </summary>
        public int TotalPages
        {
            get => _totalPages;

            set => _totalPages = value;
        }

        /// <summary>
        /// get or set date range
        /// </summary>
        /// <value>DateRange</value>
        /// 
        public DateRange DateRange
        {
            get => _dateRange;
            set => _dateRange = value;
        }

        /// <summary>
        /// get or set Group by
        /// </summary>
        /// <value>Date</value>
        public EventGroupBy GroupBy
        {
            get => _groupBy;
            set => _groupBy = value;
        }

        /// <summary>
        /// Gets or sets a value indicating whether this <see cref="EventQueryOptions"/> is visibility.
        /// </summary>
        /// <value><c>true</c> if visibility; otherwise, <c>false</c>.</value>
        public bool Visibility
        {
            get => _visibility;
            set => _visibility = value;
        }
    }
}
