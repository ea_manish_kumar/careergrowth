﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Reflection;
using BusinessLogicProcess.Event;
using CommonModules.Exceptions;
using DataAccess.Event;
using DataAccess.Event.DataTransfer;
using DomainModel;
using DomainModel.Event;
using Services.Event.DataTransfer;

namespace Services.Event
{
    public class DocumentEventService
    {
        private delegate Guid EventLogProcessDelegate(Document objDoc, DocumentEventCodeType eEvtCodeType,
            string datalist);

        private delegate DocumentEvent EventLogPrsDelegate(Document objDoc, DocumentEventCodeType eEvtCodeType,
            IList<EventData> datalist);

        #region Get Methods

        /// <summary>
        ///     Creates the document download event.
        /// </summary>
        /// <param name="document">The document.</param>
        /// <param name="FileFormatCD">The file format CD.</param>
        /// <returns></returns>
        public static Guid CreateDocumentDownloadEvent(Document document, string FileFormatCD)
        {
            var guid = new Guid();
            try
            {
                EventLogProcessDelegate caller = EventProcess.CreateDocumentEvent;
                var result = caller.BeginInvoke(document, DocumentEventCodeType.DocumentDownloaded,
                    document.Id.ToString(), null, null);
                guid = caller.EndInvoke(result);
            }
            catch (Exception ex)
            {
                var objMethod = (MethodInfo) MethodBase.GetCurrentMethod();
                var htArgs = new Hashtable();
                htArgs.Add("Document", document);
                GlobalExceptionHandler.customExceptionHandler(ex, objMethod, htArgs, ErrorSeverityType.Normal);
            }

            return guid;
        }

        /// <summary>
        ///     Creates the document created event.
        /// </summary>
        /// <param name="document">The document.</param>
        /// <returns></returns>
        public static Guid CreateDocumentCreatedEvent(Document document)
        {
            var guid = new Guid();
            try
            {
                var caller = new EventLogProcessDelegate(EventProcess.CreateDocumentEvent);
                var result = caller.BeginInvoke(document, DocumentEventCodeType.DocumentCreated, document.Id.ToString(),
                    null, null);
                guid = caller.EndInvoke(result);
            }
            catch (Exception ex)
            {
                var objMethod = (MethodInfo) MethodBase.GetCurrentMethod();
                var htArgs = new Hashtable();
                htArgs.Add("Document", document);
                GlobalExceptionHandler.customExceptionHandler(ex, objMethod, htArgs, ErrorSeverityType.Normal);
            }

            return guid;
        }

        /// <summary>
        ///     Gets Document Journals with ADMIN
        /// </summary>
        /// <param name="partyId"></param>
        /// <param name="objEventQueryOptions"></param>
        /// <param name="TotalRows"></param>
        /// <returns></returns>
        public IList<HybridDataEvent> GetDocumentJournalForAdmin(int partyId, EventQueryOptions objEventQueryOptions,
            out int TotalRows)
        {
            try
            {
                return GetDocumentJournalForAdmin(partyId, objEventQueryOptions, out TotalRows, true);
            }
            catch (Exception Ex)
            {
                var objMethod = (MethodInfo) MethodBase.GetCurrentMethod();
                GlobalExceptionHandler.customExceptionHandler(Ex, objMethod, null, ErrorSeverityType.Normal);
                TotalRows = 0;
                return null;
            }
        }

        /// <summary>
        ///     Gets the document journal for admin user.
        /// </summary>
        /// <param name="partyId">The party id.</param>
        /// <param name="objEventQueryOptions">The obj event query options.</param>
        /// <param name="TotalRows">The total rows.</param>
        /// <param name="withAdmin">if set to <c>true</c> [with admin].</param>
        /// <returns></returns>
        public IList<HybridDataEvent> GetDocumentJournalForAdmin(int partyId, EventQueryOptions objEventQueryOptions,
            out int TotalRows, bool withAdmin)
        {
            try
            {
                var objEventQuery = new EventQuery();
                //To Do remove this party ID
                objEventQuery.PartyID = partyId;
                // objEventQuery.PartyID = base.PartyID;
                objEventQuery.PageSize = objEventQueryOptions.PageSize;
                objEventQuery.PageNumber = objEventQueryOptions.PageNumber;
                //Below line comment has to be removed as to view data of one resume
                objEventQuery.DocumentID = objEventQueryOptions.DocumentID;
                objEventQuery.DateRange = objEventQueryOptions.DateRange;
                objEventQuery.Visibility = withAdmin;
                var lstHybDataEv =
                    EventDataFactory.HybridDataEventData.GetByPartyIDDocumentIDForAdmin(objEventQuery, out TotalRows);
                return lstHybDataEv;
            }
            catch (Exception Ex)
            {
                var objMethod = (MethodInfo) MethodBase.GetCurrentMethod();
                GlobalExceptionHandler.customExceptionHandler(Ex, objMethod, null, ErrorSeverityType.Normal);
                TotalRows = 0;
                return null;
            }
        }

        #endregion Get Methods
    }
}