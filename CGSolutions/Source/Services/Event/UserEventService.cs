﻿using System;
using System.Collections;
using System.Reflection;
using CommonModules.Exceptions;
using DataAccess.Event;
using DomainModel.Event;

namespace Services.Event
{
    public class UserEventService
    {
        /// <summary>
        ///     Creates the user subscribed event.
        /// </summary>
        /// <param name="UserId">The User id.</param>
        /// <param name="subscriptionId">The partner user id.</param>
        /// <param name="SkuId">The subscription id.</param>
        public static void CreateUserSubscribedEvent(int UserId, int subscriptionId, int SkuId)
        {
            try
            {
                WriteSubscriptionEvent(EventCodeCD.UserSubscribed, UserId, subscriptionId, SkuId);
            }
            catch (Exception ex)
            {
                var objMethod = (MethodInfo) MethodBase.GetCurrentMethod();
                var htArgs = new Hashtable();

                htArgs.Add("UserId", UserId);
                htArgs.Add("subscriptionId", subscriptionId);

                GlobalExceptionHandler.customExceptionHandler(ex, objMethod, htArgs, ErrorSeverityType.Normal);
            }
        }

        /// <summary>
        ///     Creates the recurring subscription event.
        /// </summary>
        /// <param name="UserId">The User id.</param>
        /// <param name="subscriptionId">The subscription id.</param>
        /// <param name="SKUID">The SKUID.</param>
        public static void CreateRecurringSubscriptionEvent(int UserId, int subscriptionId, int SKUID)
        {
            try
            {
                WriteSubscriptionEvent(EventCodeCD.RecurringSubscription, UserId, subscriptionId, SKUID);
            }
            catch (Exception ex)
            {
                var objMethod = (MethodInfo) MethodBase.GetCurrentMethod();
                var htArgs = new Hashtable();

                htArgs.Add("UserId", UserId);
                htArgs.Add("subscriptionId", subscriptionId);

                GlobalExceptionHandler.customExceptionHandler(ex, objMethod, htArgs, ErrorSeverityType.Normal);
            }
        }

        /// <summary>
        ///     Creates the subscription canceled event.
        /// </summary>
        /// <param name="UserId">The User id.</param>
        /// <param name="subscriptionId">The subscription id.</param>
        /// <param name="SKUID">The SKUID.</param>
        public static void CreateSubscriptionCanceledEvent(int UserId, int subscriptionId, int SKUID, bool Isadmin)
        {
            try
            {
                if (Isadmin)
                    WriteSubscriptionEvent(EventCodeCD.SubscriptionCancelledAdmin, UserId, subscriptionId, SKUID);
                else
                    WriteSubscriptionEvent(EventCodeCD.SubscriptionCancelledUser, UserId, subscriptionId, SKUID);
            }

            catch (Exception ex)
            {
                var objMethod = (MethodInfo) MethodBase.GetCurrentMethod();
                var htArgs = new Hashtable();

                htArgs.Add("UserId", UserId);
                htArgs.Add("subscriptionId", subscriptionId);

                GlobalExceptionHandler.customExceptionHandler(ex, objMethod, htArgs, ErrorSeverityType.Normal);
            }
        }

        /// <summary>
        ///     Creates the subscription refund event.
        /// </summary>
        /// <param name="UserId">The User id.</param>
        /// <param name="subscriptionId">The subscription id.</param>
        /// <param name="SKUID">The SKUID.</param>
        public static void CreateSubscriptionRefundEvent(int UserId, int subscriptionId, int SKUID)
        {
            try
            {
                WriteSubscriptionEvent(EventCodeCD.BillingTransactionRefunded, UserId, subscriptionId, SKUID);
            }
            catch (Exception ex)
            {
                var objMethod = (MethodInfo) MethodBase.GetCurrentMethod();
                var htArgs = new Hashtable();

                htArgs.Add("UserId", UserId);
                htArgs.Add("subscriptionId", subscriptionId);

                GlobalExceptionHandler.customExceptionHandler(ex, objMethod, htArgs, ErrorSeverityType.Normal);
            }
        }

        /// <summary>
        ///     Writes the subscription event.
        /// </summary>
        /// <param name="EventcodeCd">The eventcode cd.</param>
        /// <param name="UserId">The User id.</param>
        /// <param name="subscriptionId">The subscription id.</param>
        /// <param name="SKUID">The SKUID id.</param>
        private static void WriteSubscriptionEvent(string EventcodeCd, int UserId, int subscriptionId, int SKUID)
        {
            try
            {
                var eventCode = EventDataFactory.EventCodeData.GetByEventCode(EventcodeCd);

                var ue = new UserEvent
                {
                    EventCode = eventCode,
                    Timestamp = DateTime.Now,
                    UserID = UserId
                };
                EventDataFactory.UserEventData.Save(ue, true);

                var ed1 = new EventData
                {
                    DataTypeCD = EventDataTypeCD.Subscription,
                    EventDataDetails = Convert.ToString(subscriptionId),
                    UserEvent = ue
                };
                ue.AddEventData(ed1);


                var ed3 = new EventData
                {
                    DataTypeCD = EventDataTypeCD.Sku,
                    EventDataDetails = Convert.ToString(SKUID),
                    UserEvent = ue
                };
                ue.AddEventData(ed3);


                EventDataFactory.UserEventData.Save(ue, true);

                //WriteUserEvent(ue, UserId, PartnerUserID);
            }
            catch (Exception ex)
            {
                var objMethod = (MethodInfo) MethodBase.GetCurrentMethod();
                var htArgs = new Hashtable();

                htArgs.Add("UserId", UserId);
                htArgs.Add("subscriptionId", subscriptionId);
                htArgs.Add("SKUID", SKUID);
                htArgs.Add("EventCodeCd", EventcodeCd);
                GlobalExceptionHandler.customExceptionHandler(ex, objMethod, htArgs, ErrorSeverityType.Normal);
            }
        }

        /// <summary>
        ///     Writes the user event.
        /// </summary>
        /// <param name="EventcodeCd">The eventcode cd.</param>
        /// <param name="UserId">The user id.</param>
        public static void WriteUserEvent(string EventcodeCd, int UserId)
        {
            try
            {
                var eventCode = EventDataFactory.EventCodeData.GetByEventCode(EventcodeCd);
                var UsrEvent = new UserEvent
                {
                    EventCode = eventCode,
                    UserID = UserId,
                    Timestamp = DateTime.Now
                };
                EventDataFactory.UserEventData.Save(UsrEvent, true);
            }
            catch (Exception ex)
            {
                var objMethod = (MethodInfo) MethodBase.GetCurrentMethod();
                var htArgs = new Hashtable();
                htArgs.Add("UserId", UserId);
                htArgs.Add("EventCodeCd", EventcodeCd);
                GlobalExceptionHandler.customExceptionHandler(ex, objMethod, htArgs, ErrorSeverityType.Normal);
            }
        }

        /// <summary>
        ///     Writes the user event. and Event data
        /// </summary>
        /// <param name="EventcodeCd">The eventcode cd.</param>
        /// <param name="UserId">The User id.</param>
        /// <param name="eventDataTypeCd">The event data type cd.</param>
        /// <param name="EventData">The event data.</param>
        public static void WriteUserEvent(string EventcodeCd, int UserId, string eventDataTypeCd, string EventData)
        {
            try
            {
                var eventCode = EventDataFactory.EventCodeData.GetByEventCode(EventcodeCd);
                var UsrEvent = new UserEvent
                {
                    EventCode = eventCode,
                    UserID = UserId,
                    Timestamp = DateTime.Now
                };
                var Ed = new EventData
                {
                    DataTypeCD = eventDataTypeCd,
                    EventDataDetails = EventData,
                    UserEvent = UsrEvent
                };
                UsrEvent.AddEventData(Ed);
                EventDataFactory.UserEventData.Save(UsrEvent, true);
            }
            catch (Exception ex)
            {
                var objMethod = (MethodInfo) MethodBase.GetCurrentMethod();
                var htArgs = new Hashtable();
                htArgs.Add("UserId", UserId);
                htArgs.Add("EventCodeCd", EventcodeCd);
                GlobalExceptionHandler.customExceptionHandler(ex, objMethod, htArgs, ErrorSeverityType.Normal);
            }
        }
    }
}