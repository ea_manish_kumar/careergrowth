﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using BusinessLogic.Billing;
using CommonModules;
using CommonModules.Exceptions;
using DomainModel.Billing;

namespace Services.Billing
{
    public class BillingService2
    {
        public bool IsValidExportRequest(int userId, int skinId)
        {
            var billingProcess = new BillingProcess();
            var subscriptions = billingProcess.GetAllSubscriptionsByUserId(userId);
            if (subscriptions.IsAny(x => x.ExpireDate >= DateTime.Now))
            {
                var planTypeSkins = billingProcess.GetAllPlanTypeSkins();
                var activeSubscription = subscriptions.Where(x => x.ExpireDate >= DateTime.Now)
                    .OrderByDescending(x => x.ExpireDate).First();
                return planTypeSkins.Any(x => x.PlanTypeID == activeSubscription.PlanTypeID && x.SkinID == skinId);
            }

            return false;
        }

        public IList<Subscription> GetAllSubscriptionsByUserId(int userId)
        {
            IList<Subscription> subscriptions = null;

            try
            {
                var billingProcess = new BillingProcess();
                subscriptions = billingProcess.GetAllSubscriptionsByUserId(userId);
            }
            catch (Exception ex)
            {
                var parameters = new Hashtable(1);
                parameters.Add("UserID", userId);
                var method = (MethodInfo) MethodBase.GetCurrentMethod();
                GlobalExceptionHandler.customExceptionHandler(ex, method, parameters, ErrorSeverityType.Normal);
            }

            return subscriptions;
        }

        public Subscription CreateNewSubscription()
        {
            var billingProcess = new BillingProcess();
            return billingProcess.CreateNewSubscription();
        }

        public Transaction AddBillingTransaction()
        {
            var billingProcess = new BillingProcess();
            return billingProcess.AddBillingTransaction();
        }

        public IList<PlanType> GetAllPlanTypes()
        {
            IList<PlanType> planTypes = null;

            try
            {
                var billingProcess = new BillingProcess();
                planTypes = billingProcess.GetAllPlanTypes();
            }
            catch (Exception ex)
            {
                var parameters = new Hashtable();
                var method = (MethodInfo) MethodBase.GetCurrentMethod();
                GlobalExceptionHandler.customExceptionHandler(ex, method, parameters, ErrorSeverityType.Normal);
            }

            return planTypes;
        }

        public IList<Plan> GetAllPlansAvailableForThisPortal()
        {
            IList<Plan> plans = null;

            try
            {
                var billingProcess = new BillingProcess();
                plans = billingProcess.GetAllPlansAvailableForThisPortal();
            }
            catch (Exception ex)
            {
                var parameters = new Hashtable();
                var method = (MethodInfo) MethodBase.GetCurrentMethod();
                GlobalExceptionHandler.customExceptionHandler(ex, method, parameters, ErrorSeverityType.Normal);
            }

            return plans;
        }

        public Plan GetPlanById(int planId)
        {
            try
            {
                var billingProcess = new BillingProcess();
                return billingProcess.GetPlanById(planId);
            }
            catch (Exception ex)
            {
                var parameters = new Hashtable();
                var method = (MethodInfo) MethodBase.GetCurrentMethod();
                GlobalExceptionHandler.customExceptionHandler(ex, method, parameters, ErrorSeverityType.Normal);
            }

            return null;
        }

        public IList<PlanTypeSkin> GetAllPlanTypeSkins()
        {
            IList<PlanTypeSkin> planTypeSkins = null;

            try
            {
                var billingProcess = new BillingProcess();
                planTypeSkins = billingProcess.GetAllPlanTypeSkins();
            }
            catch (Exception ex)
            {
                var parameters = new Hashtable();
                var method = (MethodInfo) MethodBase.GetCurrentMethod();
                GlobalExceptionHandler.customExceptionHandler(ex, method, parameters, ErrorSeverityType.Normal);
            }

            return planTypeSkins;
        }

        public IList<PlanTypeSkin> GetAllPlanTypeBySkinId(int skinId)
        {
            IList<PlanTypeSkin> planTypeSkins = null;

            try
            {
                var billingProcess = new BillingProcess();
                planTypeSkins = billingProcess.GetAllPlanTypeSkins();
            }
            catch (Exception ex)
            {
                var parameters = new Hashtable();
                var method = (MethodInfo) MethodBase.GetCurrentMethod();
                GlobalExceptionHandler.customExceptionHandler(ex, method, parameters, ErrorSeverityType.Normal);
            }

            return planTypeSkins;
        }

        public Order GetOrder(int orderId)
        {
            Order order = null;
            try
            {
                var billingProcess = new BillingProcess();
                order = billingProcess.GetOrder(orderId);
            }
            catch (Exception ex)
            {
                var parameters = new Hashtable(1);
                parameters.Add("OrderId", orderId);
                var method = (MethodInfo) MethodBase.GetCurrentMethod();
                GlobalExceptionHandler.customExceptionHandler(ex, method, parameters, ErrorSeverityType.Normal);
            }

            return order;
        }

        public Order GetOrder(int userId, string statusCD)
        {
            try
            {
                var billingProcess = new BillingProcess();
                return billingProcess.GetOrder(userId, statusCD);
            }
            catch (Exception ex)
            {
                var parameters = new Hashtable(1);
                parameters.Add("UserId", userId);
                var method = (MethodInfo) MethodBase.GetCurrentMethod();
                GlobalExceptionHandler.customExceptionHandler(ex, method, parameters, ErrorSeverityType.Normal);
            }

            return null;
        }

        public IList<OrderItem> GetOrderItems(int orderId)
        {
            try
            {
                var billingProcess = new BillingProcess();
                return billingProcess.GetOrderItems(orderId);
            }
            catch (Exception ex)
            {
                var parameters = new Hashtable(1);
                parameters.Add("OrderId", orderId);
                var method = (MethodInfo) MethodBase.GetCurrentMethod();
                GlobalExceptionHandler.customExceptionHandler(ex, method, parameters, ErrorSeverityType.Normal);
            }

            return null;
        }

        private void SaveOrder(Order order)
        {
            try
            {
                var billingProcess = new BillingProcess();
                billingProcess.SaveOrUpdateOrder(order);
            }
            catch (Exception ex)
            {
                var parameters = new Hashtable(1);
                parameters.Add("Order", order);
                var method = (MethodInfo) MethodBase.GetCurrentMethod();
                GlobalExceptionHandler.customExceptionHandler(ex, method, parameters, ErrorSeverityType.Normal);
            }
        }

        public Order UpdateOrder()
        {
            Order order = null;
            try
            {
                order = new Order();
                var billingProcess = new BillingProcess();
                billingProcess.SaveOrUpdateOrder(order);
            }
            catch (Exception ex)
            {
                var parameters = new Hashtable(1);
                parameters.Add("Order", order);
                var method = (MethodInfo) MethodBase.GetCurrentMethod();
                GlobalExceptionHandler.customExceptionHandler(ex, method, parameters, ErrorSeverityType.Normal);
            }

            return order;
        }

        public Order PlaceOrder(int userId, int planId, Guid sessionId)
        {
            Order order = null;
            try
            {
                var plan = GetPlanById(planId);
                if (plan != null)
                {
                    var freshOrder = true;
                    order = GetOrder(userId, OrderStatusTypeCD.Pending);
                    if (order == null)
                        order = new Order();
                    else
                        freshOrder = false;
                    order.ModifiedDate = DateTime.Now;
                    order.StatusCD = OrderStatusTypeCD.Pending;
                    order.UserID = userId;
                    order.SessionId = sessionId;
                    order.Amount = plan.UnitPrice;

                    SaveOrder(order);
                    //if (order.Id > 0)
                    //{
                    //    OrderItem orderItem = null;
                    //    IList<OrderItem> orderitems = null;
                    //    if (!freshOrder)
                    //        orderitems = GetOrderItems(order.Id);
                    //    if (orderitems.IsAny())
                    //        orderItem = orderitems.OrderByDescending(o => o.CreationDate).First();
                    //    else
                    //        orderItem = new OrderItem();
                    //    orderItem.Order = order;
                    //    orderItem.SKU = sku;
                    //    orderItem.Quantity = 1;
                    //    orderItem.Descriptor = sku.Description;
                    //    orderItem.UnitPrice = sku.UnitPrice;
                    //    orderItem.CreatedOn = DateTime.Now;
                    //    orderItem.ModifiedOn = DateTime.Now;
                    //    orderItem.StatusCD = BillingStatusTypeCD.Pending;
                    //    BillingFactory.OrderSKUDAL.SaveOrUpdate(orderItem, true);
                    //}
                }
            }
            catch (Exception ex)
            {
                var htParams = new Hashtable(3);
                htParams.Add("UserId", userId);
                htParams.Add("PlanId", planId);
                htParams.Add("SessionId", sessionId);
                var objMethod = (MethodInfo) MethodBase.GetCurrentMethod();
                GlobalExceptionHandler.customExceptionHandler(ex, objMethod, htParams, ErrorSeverityType.Normal);
            }

            return order;
        }
    }
}