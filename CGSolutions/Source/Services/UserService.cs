﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using BusinessLogicProcess.User;
using CommonModules;
using CommonModules.Caching;
using CommonModules.Configuration;
using CommonModules.Exceptions;
using DataAccess.User;
using DomainModel.User;
using Utilities;
using Utilities.Constants;
using Utilities.HashingUtility;

namespace Services.User
{
    public class UserService
    {
        private static IList<Progress> lstProgressCode;

        static UserService()
        {
            lstProgressCode = GetAllProgressCode();
        }

        public bool IsPremiumUser(int userid)
        {
            UserMaster user = null;
            var response = false;

            try
            {
                user = UserFactory.UserDAL.GetById(userid);
                if (user != null && user.UserLevelID > 0) response = true;
            }
            catch (Exception ex)
            {
                var htParams = new Hashtable(1);
                htParams.Add("userid", userid);
                var objMethod = (MethodInfo) MethodBase.GetCurrentMethod();
                GlobalExceptionHandler.customExceptionHandler(ex, objMethod, htParams, ErrorSeverityType.Normal);
            }

            return response;
        }

        public bool IsPremiumUser(UserMaster user)
        {
            try
            {
                if (user != null && user.UserLevelID > 0) return true;
            }
            catch (Exception ex)
            {
                var htParams = new Hashtable(1);
                htParams.Add("UserMaster", user != null ? user.ToString() : "<NULL>");
                var objMethod = (MethodInfo) MethodBase.GetCurrentMethod();
                GlobalExceptionHandler.customExceptionHandler(ex, objMethod, htParams, ErrorSeverityType.Normal);
            }

            return false;
        }

        /// <summary>
        ///     Add optins to new user with selected response
        /// </summary>
        /// <param name="userId"></param>
        /// <param name="optinResponse"></param>
        /// <returns></returns>
        public UserOptin SetUserOptout(int userId, short optinResponse)
        {
            UserOptin userOptEmails = null;
            UserOptin userOptJobs = null;

            try
            {
                userOptEmails = new UserOptin();
                userOptEmails.UserID = userId;
                userOptEmails.OptinID = UserNotificationConstants.Emails;
                userOptEmails.Response = optinResponse;
                userOptEmails.TimeStamp = DateTime.Now;

                userOptJobs = new UserOptin();
                userOptJobs.UserID = userId;
                userOptJobs.OptinID = UserNotificationConstants.Jobs;
                userOptJobs.Response = optinResponse;
                userOptJobs.TimeStamp = DateTime.Now;

                var userOptins = new List<UserOptin>();
                userOptins.Add(userOptEmails);
                userOptins.Add(userOptJobs);
                SaveAllOptIn(userOptins);
            }
            catch (Exception ex)
            {
                var htParams = new Hashtable(4);
                htParams.Add("UserID", userId);
                htParams.Add("OptinResponse", optinResponse);
                if (userOptEmails != null) htParams.Add("UserOptCareerTips", userOptEmails);
                if (userOptJobs != null) htParams.Add("UserOptJobAlerts", userOptJobs);
                var objMethod = (MethodInfo) MethodBase.GetCurrentMethod();
                GlobalExceptionHandler.customExceptionHandler(ex, objMethod, htParams, ErrorSeverityType.Normal);
            }

            return userOptEmails;
        }

        public UserMaster RegisterUser(string email, string password, short roleId, short optout, int portalId)
        {
            UserMaster user = null;

            try
            {
                if (!IsAlreadyRegisteredEmail(email, portalId))
                {
                    user = new UserMaster();
                    user.DomainCD = CGUtility.GetDomainCDFromConfig();
                    user.PortalID = portalId;
                    user.ProgressCD = "CRTUSR";
                    user.UserName = email;
                    user.HashedPwd = HashingUtility.ObfuscatePassword(password);
                    user.RoleID = roleId;
                    UserFactory.UserDAL.Save(user, true);
                    SetUserOptout(user.Id, optout);
                }
            }
            catch (Exception ex)
            {
                var htParams = new Hashtable(5);
                htParams.Add("User", user);
                htParams.Add("RoleId", roleId);
                htParams.Add("Email", email);
                htParams.Add("PortalId", portalId);
                htParams.Add("Optout", optout);
                var objMethod = (MethodInfo) MethodBase.GetCurrentMethod();
                GlobalExceptionHandler.customExceptionHandler(ex, objMethod, htParams, ErrorSeverityType.Normal);
            }

            return user;
        }

        public SharedAuth RegisterAuthUser(int userid, string email, string fullName, string authProviderCD,
            string parentUID, string accessToken, int portalID, int roleid, short optout)
        {
            SharedAuth sharedUser = null;
            UserMaster user = null;
            try
            {
                if (userid > 0)
                {
                    user = UserFactory.UserDAL.GetUserByEmail(email, portalID);
                    if (user == null)
                    {
                        user = UserFactory.UserDAL.GetById(userid);
                        if (user != null)
                        {
                            user.UserName = email;
                            user.FirstName = fullName;
                            user.RoleID = (short) roleid;
                            UserFactory.UserDAL.SaveOrUpdate(user, true);
                            SetUserOptout(user.Id, optout);
                        }

                        sharedUser =
                            UserFactory.SharedAuthDAL.GetAuthUserByUserIdAndAuthProvider(userid, authProviderCD);
                        if (sharedUser == null)
                        {
                            var authUser = new SharedAuth();
                            authUser.UserID = userid;
                            authUser.AuthProviderCD = authProviderCD;
                            authUser.PUID = parentUID;
                            authUser.AccessToken = accessToken;
                            authUser.CreatedOn = DateTime.Now;
                            authUser.IsPrimary = true;
                            UserFactory.SharedAuthDAL.SaveOrUpdate(authUser, true);
                        }
                    }
                    else
                    {
                        sharedUser =
                            UserFactory.SharedAuthDAL.GetAuthUserByUserIdAndAuthProvider(user.Id, authProviderCD);
                        if (sharedUser == null)
                        {
                            var authUser = new SharedAuth();
                            authUser.UserID = user.Id;
                            authUser.AuthProviderCD = authProviderCD;
                            authUser.PUID = parentUID;
                            authUser.AccessToken = accessToken;
                            authUser.CreatedOn = DateTime.Now;
                            authUser.IsPrimary = false;
                            UserFactory.SharedAuthDAL.SaveOrUpdate(authUser, true);
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                var htParams = new Hashtable(2);
                htParams.Add("Email", email);
                htParams.Add("AuthProviderCD", authProviderCD);
                var objMethod = (MethodInfo) MethodBase.GetCurrentMethod();
                GlobalExceptionHandler.customExceptionHandler(ex, objMethod, htParams, ErrorSeverityType.Normal);
            }

            return sharedUser;
        }

        /// <summary>
        ///     Create a guest user with roleid = 0 and userstage = CRTUSR
        /// </summary>
        /// <returns></returns>
        public UserMaster RegisterGuestUser()
        {
            UserMaster user = null;

            try
            {
                user = new UserMaster();
                var domainName = CGUtility.GetDomainCDFromConfig();
                user.DomainCD = domainName;
                user.ProgressCD = "CRTUSR";
                UserFactory.UserDAL.Save(user, true);
                //VJ: Save UserID in ContextCacher
                ContextCacher.UserID = user.ID;
            }
            catch (Exception ex)
            {
                var htParams = new Hashtable(1);
                htParams.Add("GuestUser", user);
                var objMethod = (MethodInfo) MethodBase.GetCurrentMethod();
                GlobalExceptionHandler.customExceptionHandler(ex, objMethod, htParams, ErrorSeverityType.Normal);
            }

            return user;
        }

        /// <summary>
        ///     Create a guest user with roleid = 0 and userstage = CRTUSR,
        ///     save abtest values in ABTestData table
        /// </summary>
        /// <param name="sessionId"></param>
        /// <returns></returns>
        public UserMaster RegisterGuestUser(string sessionId, int portalID)
        {
            UserMaster user = null;
            try
            {
                #region Create User

                user = new UserMaster();
                user.DomainCD = CGUtility.GetDomainCDFromConfig();
                user.PortalID = portalID;
                user.ProgressCD = "CRTUSR";
                UserFactory.UserDAL.Save(user, true);

                #endregion

                if (user != null)
                    //VJ: Save UserID in ContextCacher, used in global exception handling
                    ContextCacher.UserID = user.ID;
            }
            catch (Exception ex)
            {
                var htParams = new Hashtable(3);
                if (user != null) htParams.Add("GuestUser", user);
                htParams.Add("PortalID", portalID);
                htParams.Add("SessionID", sessionId);
                var objMethod = (MethodInfo) MethodBase.GetCurrentMethod();
                GlobalExceptionHandler.customExceptionHandler(ex, objMethod, htParams, ErrorSeverityType.Normal);
            }

            return user;
        }

        public UserMaster Create(UserMaster user)
        {
            try
            {
                UserFactory.UserDAL.Save(user, true);

                if (user != null) return user; // CreateSession(user);  
            }
            catch (Exception ex)
            {
                var htParams = new Hashtable(1);
                htParams.Add("user", user);
                var objMethod = (MethodInfo) MethodBase.GetCurrentMethod();
                GlobalExceptionHandler.customExceptionHandler(ex, objMethod, htParams, ErrorSeverityType.Normal);
            }

            return null;
        }

        public bool ValidatePassword(string hashedPwd, string password)
        {
            var isUserAuthenticated = false;

            var userProcess = new UserProcess();
            isUserAuthenticated = userProcess.ValidatePassword(hashedPwd, password);

            return isUserAuthenticated;
        }


        public UserMaster Login(string email, string password, int portalId)
        {
            try
            {
                var userProcess = new UserProcess();
                var user = userProcess.ValidateUser(email, password, portalId);
                if (user != null) return user;
            }
            catch (Exception ex)
            {
                var htParams = new Hashtable(2);
                htParams.Add("email", email);
                htParams.Add("password", password);
                var objMethod = (MethodInfo) MethodBase.GetCurrentMethod();
                GlobalExceptionHandler.customExceptionHandler(ex, objMethod, htParams, ErrorSeverityType.Normal);
            }

            return null;
        }


        public bool UpdateUser(UserMaster user)
        {
            var result = false;

            try
            {
                if (user != null)
                {
                    UserFactory.UserDAL.SaveOrUpdate(user, true);
                    result = true;
                }
            }
            catch (Exception ex)
            {
                var htParams = new Hashtable(1);
                htParams.Add("userid", user.Id.ToString());
                htParams.Add("firstname", user.FirstName);
                htParams.Add("lastname", user.LastName);
                var objMethod = (MethodInfo) MethodBase.GetCurrentMethod();
                GlobalExceptionHandler.customExceptionHandler(ex, objMethod, htParams, ErrorSeverityType.Normal);
            }

            return result;
        }

        public bool DeActivateUser(int UserID)
        {
            var status = false;
            UserMaster usr = null;
            try
            {
                usr = UserFactory.UserDAL.GetById(UserID);
                if (usr != null)
                {
                    usr.IsActive = false;
                    usr.UserLevelID = 0;
                    UserFactory.UserDAL.SaveOrUpdate(usr, true);
                    status = true;
                }
            }
            catch (Exception ex)
            {
                var htParams = new Hashtable(1);
                htParams.Add("UserID", UserID);
                var objMethod = (MethodInfo) MethodBase.GetCurrentMethod();
                GlobalExceptionHandler.customExceptionHandler(ex, objMethod, htParams, ErrorSeverityType.Normal);
            }

            return status;
        }

        public bool ValidateUserByID(int userid)
        {
            try
            {
                if (UserFactory.UserDAL.GetById(userid) != null) return true;
            }
            catch (Exception ex)
            {
                var htParams = new Hashtable(1);
                htParams.Add("userid", userid);
                var objMethod = (MethodInfo) MethodBase.GetCurrentMethod();
                GlobalExceptionHandler.customExceptionHandler(ex, objMethod, htParams, ErrorSeverityType.Normal);
            }

            return false;
        }


        public bool SetUserLevel(int UserID)
        {
            UserMaster usr = null;

            try
            {
                usr = UserFactory.UserDAL.GetById(UserID);
                if (usr != null)
                {
                    usr.UserLevelID = 1;
                    UserFactory.UserDAL.SaveOrUpdate(usr, true);
                }
            }
            catch (Exception ex)
            {
                var htParams = new Hashtable(1);
                htParams.Add("UserID", UserID);
                var objMethod = (MethodInfo) MethodBase.GetCurrentMethod();
                GlobalExceptionHandler.customExceptionHandler(ex, objMethod, htParams, ErrorSeverityType.Normal);
            }

            return true;
        }

        public bool SetUserLevelToBasic(int UserID)
        {
            UserMaster usr = null;

            try
            {
                usr = UserFactory.UserDAL.GetById(UserID);
                if (usr != null)
                {
                    usr.UserLevelID = 0;
                    UserFactory.UserDAL.SaveOrUpdate(usr, true);
                }
            }
            catch (Exception ex)
            {
                var htParams = new Hashtable(1);
                htParams.Add("UserID", UserID);
                var objMethod = (MethodInfo) MethodBase.GetCurrentMethod();
                GlobalExceptionHandler.customExceptionHandler(ex, objMethod, htParams, ErrorSeverityType.Normal);
            }

            return true;
        }

        public UserMaster GetUserByEmail(string email, int portalId)
        {
            UserMaster result = null;
            try
            {
                result = UserFactory.UserDAL.GetUserByEmail(email, portalId);
            }
            catch (Exception ex)
            {
                var htParams = new Hashtable(2);
                htParams.Add("email", email);
                htParams.Add("portalId", portalId.ToString());
                var objMethod = (MethodInfo) MethodBase.GetCurrentMethod();
                GlobalExceptionHandler.customExceptionHandler(ex, objMethod, htParams, ErrorSeverityType.Normal);
            }

            return result;
        }

        public bool IsAlreadyRegisteredEmail(string email, int portalID)
        {
            var result = false;
            try
            {
                result = UserFactory.UserDAL.IsAlreadyRegisteredEmail(email, portalID);
            }
            catch (Exception ex)
            {
                var htParams = new Hashtable(2);
                htParams.Add("email", email);
                htParams.Add("portalID", portalID.ToString());
                var objMethod = (MethodInfo) MethodBase.GetCurrentMethod();
                GlobalExceptionHandler.customExceptionHandler(ex, objMethod, htParams, ErrorSeverityType.Normal);
            }

            return result;
        }

        public List<UserMaster> GetUsersBySearchCriteria(int userid, string email, string firstname, string lastname)
        {
            List<UserMaster> lstUser = null;

            try
            {
                lstUser = UserFactory.UserDAL.GetUsersBySearchCriteria(userid, email, firstname, lastname);
            }
            catch (Exception ex)
            {
                var htParams = new Hashtable(1);
                //htParams.Add("email", email);
                var objMethod = (MethodInfo) MethodBase.GetCurrentMethod();
                GlobalExceptionHandler.customExceptionHandler(ex, objMethod, htParams, ErrorSeverityType.Normal);
            }

            return lstUser;
        }

        public IList<UserSearchDetailDTO> GetUserDetailBySearchCriteria(int userid, string email, string firstname,
            string lastname, int orderid, string gatewayref)
        {
            IList<UserSearchDetailDTO> lstUser = null;

            try
            {
                lstUser = UserFactory.UserDAL.GetUserDetailBySearchCriteria(userid, email, firstname, lastname, orderid,
                    gatewayref);
            }
            catch (Exception ex)
            {
                var htParams = new Hashtable(1);
                //htParams.Add("email", email);
                var objMethod = (MethodInfo) MethodBase.GetCurrentMethod();
                GlobalExceptionHandler.customExceptionHandler(ex, objMethod, htParams, ErrorSeverityType.Normal);
            }

            return lstUser;
        }

        public IList<UserSearchDetailECOM> GetUserDetailBySearchCriteria(int userid, string email, string firstname,
            string lastname, bool isSandBoxUser, string DomainCodes = "", int cancellationNumber = 0, int count = 0,
            bool ShowDeActiveUsers = false)
        {
            IList<UserSearchDetailECOM> lstUser = null;

            try
            {
                lstUser = UserFactory.UserDAL.GetUserDetailBySearchCriteria(userid, email, firstname, lastname,
                    DomainCodes, isSandBoxUser, cancellationNumber, count, ShowDeActiveUsers);
            }
            catch (Exception ex)
            {
                var htParams = new Hashtable(1);
                //htParams.Add("email", email);
                var objMethod = (MethodInfo) MethodBase.GetCurrentMethod();
                GlobalExceptionHandler.customExceptionHandler(ex, objMethod, htParams, ErrorSeverityType.Normal);
            }

            return lstUser;
        }

        public IList<UserNotesDetailDTO> GetUserAdminNotes(int userid)
        {
            var lstNotes = UserFactory.UserNotesDAL.GetUserAdminNotesByUserID(userid);
            if (lstNotes != null && lstNotes.Count > 0)
                lstNotes = lstNotes.Where(x => x != null).OrderByDescending(x => x.TimeStamp).ToList();
            return lstNotes;
        }

        public bool AddUserNote(UserNotesDTO userNotes)
        {
            var isAdded = false;
            try
            {
                userNotes.TimeStamp = DateTime.Now;
                UserFactory.UserNotesDAL.Save(userNotes);
                isAdded = true;
            }
            catch (Exception)
            {
            }

            return isAdded;
        }

        public UserMaster GetUserByID(int userid)
        {
            UserMaster user = null;

            try
            {
                user = UserFactory.UserDAL.GetById(userid);
            }
            catch (Exception ex)
            {
                var htParams = new Hashtable(1);
                //htParams.Add("email", email);
                var objMethod = (MethodInfo) MethodBase.GetCurrentMethod();
                GlobalExceptionHandler.customExceptionHandler(ex, objMethod, htParams, ErrorSeverityType.Normal);
            }

            return user;
        }

        public int GetUserLevelByUserID(int userid)
        {
            var LevelID = 0;
            UserMaster user = null;

            try
            {
                user = UserFactory.UserDAL.GetById(userid);
                if (user != null)
                    LevelID = user.UserLevelID;
            }
            catch (Exception ex)
            {
                var htParams = new Hashtable(1);
                //htParams.Add("email", email);
                var objMethod = (MethodInfo) MethodBase.GetCurrentMethod();
                GlobalExceptionHandler.customExceptionHandler(ex, objMethod, htParams, ErrorSeverityType.Normal);
            }

            return LevelID;
        }

        public string GetProgressCDByUserID(int userid)
        {
            var userStageCD = "";
            UserMaster user = null;

            try
            {
                user = UserFactory.UserDAL.GetById(userid);
                if (user != null)
                    userStageCD = user.ProgressCD;
            }
            catch (Exception ex)
            {
                var htParams = new Hashtable(1);
                var objMethod = (MethodInfo) MethodBase.GetCurrentMethod();
                GlobalExceptionHandler.customExceptionHandler(ex, objMethod, htParams, ErrorSeverityType.Normal);
            }

            return userStageCD;
        }

        public IList<Address> GetUserAllAddress(int UserID)
        {
            try
            {
                return UserFactory.AddressDAL.GetUserAllAddress(UserID);
            }
            catch (Exception ex)
            {
                var htParams = new Hashtable(2);
                htParams.Add("UserID", UserID);
                var objMethod = (MethodInfo) MethodBase.GetCurrentMethod();
                GlobalExceptionHandler.customExceptionHandler(ex, objMethod, htParams, ErrorSeverityType.Normal);
                return null;
            }
        }

        public Address GetUserAddress(int UserID)
        {
            try
            {
                return UserFactory.AddressDAL.GetAddressByUserID(UserID);
            }
            catch (Exception ex)
            {
                var htParams = new Hashtable(2);
                htParams.Add("UserID", UserID);
                var objMethod = (MethodInfo) MethodBase.GetCurrentMethod();
                GlobalExceptionHandler.customExceptionHandler(ex, objMethod, htParams, ErrorSeverityType.Normal);
                return null;
            }
        }

        /// <summary>
        ///     Get User Address by AddressTypeCD like Home or Billing address
        /// </summary>
        /// <param name="UserID"></param>
        /// <param name="addressTypeCD"></param>
        /// <returns></returns>
        public Address GetUserAddress(int UserID, string addressTypeCD)
        {
            try
            {
                return UserFactory.AddressDAL.GetAddressByUserID(UserID, addressTypeCD);
            }
            catch (Exception ex)
            {
                var htParams = new Hashtable(2);
                htParams.Add("UserID", UserID);
                var objMethod = (MethodInfo) MethodBase.GetCurrentMethod();
                GlobalExceptionHandler.customExceptionHandler(ex, objMethod, htParams, ErrorSeverityType.Normal);
                return null;
            }
        }

        public void SaveUserAddress(Address address)
        {
            try
            {
                if (address.Id > 0)
                    UserFactory.AddressDAL.SaveOrUpdate(address, true);
                else
                    UserFactory.AddressDAL.Save(address, true);
            }
            catch (Exception ex)
            {
                var htParams = new Hashtable(1);
                htParams.Add("Address", address != null ? address.ToString() : "<NULL>");
                var objMethod = (MethodInfo) MethodBase.GetCurrentMethod();
                GlobalExceptionHandler.customExceptionHandler(ex, objMethod, htParams, ErrorSeverityType.Normal);
            }
        }

        public string GenerateTemporaryPassword()
        {
            return GenerateTemporaryPassword(6);
        }

        public string GenerateTemporaryPassword(int length)
        {
            var valid = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ1234567890";
            var res = "";
            var rnd = new Random();
            while (0 < length--)
                res += valid[rnd.Next(valid.Length)];
            return res;
        }

        public bool DoAuthentication(string sSessionID)
        {
            var result = true;
            try
            {
                //IList<Session> lst =  UserFactory.SessionDAL.GetBySessionID(sSessionID);
                //if (lst != null && lst.Count>0)
                //{
                //    for (int i = 0; i < lst.Count; i++)
                //    {
                //        if (lst[i].DateExpire > DateTime.Now)
                //        {
                //            return true;
                //        }
                //    }
                //}
            }
            catch (Exception ex)
            {
                var htParams = new Hashtable(1);
                htParams.Add("SessionID", sSessionID);
                var objMethod = (MethodInfo) MethodBase.GetCurrentMethod();
                GlobalExceptionHandler.customExceptionHandler(ex, objMethod, htParams, ErrorSeverityType.Normal);
            }

            return result;
        }

        public bool SetProgress(int userID, string stageCD)
        {
            if (!string.IsNullOrEmpty(stageCD))
            {
                UserMaster user = null;
                Progress userStage = null;
                Progress NextProgress = null;
                var stageUpdated = false;
                try
                {
                    NextProgress = ProgressCode.FirstOrDefault(x => x.ProgressCD.Trim() == stageCD.Trim());
                    if (NextProgress != null)
                    {
                        user = UserFactory.UserDAL.GetById(userID);
                        if (user != null)
                        {
                            if (string.IsNullOrEmpty(user.ProgressCD))
                            {
                                stageUpdated = true;
                                user.ProgressCD = stageCD;
                                UserFactory.UserDAL.Update(user, true);
                            }
                            else
                            {
                                userStage = ProgressCode.FirstOrDefault(x =>
                                    x.ProgressCD.Trim() == user.ProgressCD.Trim());
                                if (userStage != null && NextProgress.StageIndex.HasValue &&
                                    userStage.StageIndex.HasValue &&
                                    NextProgress.StageIndex.Value > userStage.StageIndex.Value)
                                {
                                    stageUpdated = true;
                                    user.ProgressCD = stageCD;
                                    UserFactory.UserDAL.Update(user, true);
                                }
                            }
                        }
                    }
                }
                catch (Exception ex)
                {
                    var htParams = new Hashtable(2);
                    if (user != null)
                        htParams.Add("UserId", user.ID.ToString());
                    htParams.Add("StageCD", stageCD);
                    var objMethod = (MethodInfo) MethodBase.GetCurrentMethod();
                    GlobalExceptionHandler.customExceptionHandler(ex, objMethod, htParams, ErrorSeverityType.Normal);
                    return stageUpdated;
                }

                return stageUpdated;
            }

            return false;
        }

        public bool SetProgressCLB(int UserID, string StageCD)
        {
            Progress NextUsrStage = null;
            Progress CurrentusrStage = null;
            UserData CurentUserData = null;
            try
            {
                CurentUserData = GetUserDataByDataTypeCD(UserID, CLBProgressCode.DATATYPECD);

                if (CurentUserData != null)
                {
                    if (CurentUserData.Data != StageCD)
                    {
                        NextUsrStage = ProgressCode.FirstOrDefault(stg => stg.ProgressCD == StageCD);
                        CurrentusrStage = ProgressCode.FirstOrDefault(stg => stg.ProgressCD == CurentUserData.Data);

                        if (CurrentusrStage.StageIndex.HasValue && NextUsrStage.StageIndex.HasValue &&
                            CurrentusrStage.StageIndex.Value < NextUsrStage.StageIndex.Value)
                            UpdateUserDataByDataTypeCD(UserID, CLBProgressCode.DATATYPECD, NextUsrStage.ProgressCD);
                    }
                }
                else
                {
                    NextUsrStage = ProgressCode.FirstOrDefault(stg => stg.ProgressCD == StageCD);
                    SetUserDataByDataTypeCD(UserID, CLBProgressCode.DATATYPECD, NextUsrStage.ProgressCD);
                }
            }
            catch (Exception ex)
            {
                var htParams = new Hashtable(1);
                htParams.Add("UserID", UserID.ToString());
                htParams.Add("StageCD", StageCD);
                var objMethod = (MethodInfo) MethodBase.GetCurrentMethod();
                GlobalExceptionHandler.customExceptionHandler(ex, objMethod, htParams, ErrorSeverityType.Normal);
                return false;
            }

            return true;
        }

        public int GetRoleIDByUserID(int userid)
        {
            var roleId = 0;
            try
            {
                roleId = UserFactory.UserDAL.GetRoleIDByUserID(userid);
            }
            catch (Exception ex)
            {
                var htParams = new Hashtable(1);
                htParams.Add("userid", userid);
                var objMethod = (MethodInfo) MethodBase.GetCurrentMethod();
                GlobalExceptionHandler.customExceptionHandler(ex, objMethod, htParams, ErrorSeverityType.Normal);
            }

            return roleId;
        }

        /// <summary>
        ///     Gets the user session duration by party ID.
        /// </summary>
        /// <param name="partyID">The party ID.</param>
        /// <returns></returns>
        public IList<UserSessionDurationDetail> GetUserSessionDurationByPartyID(int userID)
        {
            IList<UserSessionDurationDetail> lstUserSessionDetail = null;
            try
            {
                lstUserSessionDetail =
                    UserFactory.UserDAL
                        .GetUserSessionDurationByPartyID(
                            userID); //UserDataFactory.IBHSessionData.GetUserSessionDurationByPartyID(partyID);
            }
            catch (Exception ex)
            {
                var htParams = new Hashtable(1);
                htParams.Add("userID", userID);
                var objMethod = (MethodInfo) MethodBase.GetCurrentMethod();
                GlobalExceptionHandler.customExceptionHandler(ex, objMethod, htParams, ErrorSeverityType.Normal);
            }

            return lstUserSessionDetail;
        }

        public bool UpdateUser(UserMaster user, string newPassword)
        {
            var result = false;
            var hashedNewPassword = string.Empty;

            try
            {
                if (user != null)
                {
                    hashedNewPassword = HashingUtility.ObfuscatePassword(newPassword);
                    user.HashedPwd = hashedNewPassword;
                    result = UpdateUser(user);
                }
            }
            catch (Exception ex)
            {
                var htParams = new Hashtable(1);
                htParams.Add("userid", user.Id.ToString());
                htParams.Add("firstname", user.FirstName);
                htParams.Add("lastname", user.LastName);
                var objMethod = (MethodInfo) MethodBase.GetCurrentMethod();
                GlobalExceptionHandler.customExceptionHandler(ex, objMethod, htParams, ErrorSeverityType.Normal);
            }

            return result;
        }

        public string GetEmailByUserID(int userID)
        {
            UserMaster user = null;

            try
            {
                user = GetUserByID(userID);
                if (user != null) return user.UserName;
            }
            catch (Exception ex)
            {
                var htParams = new Hashtable(1);
                htParams.Add("userid", user.Id.ToString());
                var objMethod = (MethodInfo) MethodBase.GetCurrentMethod();
                GlobalExceptionHandler.customExceptionHandler(ex, objMethod, htParams, ErrorSeverityType.Normal);
            }

            return null;
        }

        public UserOptin GetByID(int UserID, short OptinID)
        {
            UserOptin userOpt = null;
            try
            {
                userOpt = UserFactory.UserOptinDAL.GetByID(UserID, OptinID);
            }
            catch (Exception ex)
            {
                var htParams = new Hashtable(2);
                htParams.Add("UserID", UserID);
                htParams.Add("OptinID", OptinID);
                var objMethod = (MethodInfo) MethodBase.GetCurrentMethod();
                GlobalExceptionHandler.customExceptionHandler(ex, objMethod, htParams, ErrorSeverityType.Normal);
            }

            return userOpt;
        }

        public IList<UserOptin> GetEmailSettingsByUserID(int UserID)
        {
            IList<UserOptin> userOptins = null;
            try
            {
                userOptins = UserFactory.UserOptinDAL.GetByUserID(UserID);
            }
            catch (Exception ex)
            {
                var htParams = new Hashtable(1);
                htParams.Add("UserID", UserID);
                var objMethod = (MethodInfo) MethodBase.GetCurrentMethod();
                GlobalExceptionHandler.customExceptionHandler(ex, objMethod, htParams, ErrorSeverityType.Normal);
            }

            return userOptins;
        }

        public void SaveOptIn(UserOptin newUserOptin)
        {
            UserOptin userOptin = null;

            try
            {
                userOptin = GetByID(newUserOptin.UserID, newUserOptin.OptinID);

                if (userOptin != null)
                {
                    userOptin.Response = newUserOptin.Response;
                    userOptin.TimeStamp = newUserOptin.TimeStamp;
                    UserFactory.UserOptinDAL.SaveOrUpdate(userOptin, true);
                }
                else
                {
                    UserFactory.UserOptinDAL.Save(newUserOptin, true);
                }
            }
            catch (Exception ex)
            {
                var htParams = new Hashtable(1);
                htParams.Add("UserID", newUserOptin.UserID.ToString());
                htParams.Add("OptinID", newUserOptin.OptinID.ToString());
                htParams.Add("Response", newUserOptin.Response.ToString());
                var objMethod = (MethodInfo) MethodBase.GetCurrentMethod();
                GlobalExceptionHandler.customExceptionHandler(ex, objMethod, htParams, ErrorSeverityType.Normal);
            }
        }

        /// <summary>
        ///     Get UserData by DataTypeCD
        /// </summary>
        /// <param name="UserID"></param>
        /// <param name="DataTypeCD"></param>
        /// <returns></returns>
        public UserData GetUserDataByDataTypeCD(int UserID, string DataTypeCD)
        {
            UserData objUserData = null;
            try
            {
                if (UserID > 0) objUserData = UserFactory.UserDataDAL.GetUserDataByDataTypeCD(UserID, DataTypeCD);
            }
            catch (Exception ex)
            {
                var htParams = new Hashtable(2);
                htParams.Add("UserID", UserID);
                htParams.Add("DataTypeCD", DataTypeCD);
                var objMethod = (MethodInfo) MethodBase.GetCurrentMethod();
                GlobalExceptionHandler.customExceptionHandler(ex, objMethod, htParams, ErrorSeverityType.Normal);
            }

            return objUserData;
        }

        /// <summary>
        ///     Save UserData with DataTypeCD and Data value
        /// </summary>
        /// <param name="UserID"></param>
        /// <param name="DataTypeCD"></param>
        /// <param name="Data"></param>
        public void SetUserDataByDataTypeCD(int UserID, string DataTypeCD, string Data)
        {
            UserData objUserData = null;
            try
            {
                objUserData = UserFactory.UserDataDAL.GetUserDataByDataTypeCD(UserID, DataTypeCD);
                if (objUserData == null) objUserData = new UserData();
                objUserData.UserID = UserID;
                objUserData.DataTypeCD = DataTypeCD;
                objUserData.Data = Data;

                UserFactory.UserDataDAL.SaveOrUpdate(objUserData, true);
            }
            catch (Exception ex)
            {
                var htParams = new Hashtable(3);
                htParams.Add("UserID", UserID);
                htParams.Add("DataTypeCD", DataTypeCD);
                htParams.Add("Data", Data);
                var objMethod = (MethodInfo) MethodBase.GetCurrentMethod();
                GlobalExceptionHandler.customExceptionHandler(ex, objMethod, htParams, ErrorSeverityType.Normal);
            }
        }

        public void UpdateUserDataByDataTypeCD(int UserID, string DataTypeCD, string Data)
        {
            UserData objUserData = null;
            try
            {
                objUserData = GetUserDataByDataTypeCD(UserID, DataTypeCD);
                objUserData.Data = Data;
                UserFactory.UserDataDAL.SaveOrUpdate(objUserData, true);
            }
            catch (Exception ex)
            {
                var htParams = new Hashtable(1);
                htParams.Add("UserData", objUserData);
                var objMethod = (MethodInfo) MethodBase.GetCurrentMethod();
                GlobalExceptionHandler.customExceptionHandler(ex, objMethod, htParams, ErrorSeverityType.Normal);
            }
        }

        public bool UpdateByOldData(int userId, string dataTypeCD, string oldData, string newData)
        {
            try
            {
                return UserFactory.UserDataDAL.UpdateByOldData(userId, dataTypeCD, oldData, newData);
            }
            catch (Exception ex)
            {
                var htParams = new Hashtable(4);
                htParams.Add("UserID", userId);
                htParams.Add("DataTypeCD", dataTypeCD);
                htParams.Add("OldData", oldData);
                htParams.Add("NewData", newData);
                var objMethod = (MethodInfo) MethodBase.GetCurrentMethod();
                GlobalExceptionHandler.customExceptionHandler(ex, objMethod, htParams, ErrorSeverityType.Normal);
            }

            return false;
        }

        public IList<UserMaster> GetUsersByDateCreatedSubscriptionLevel(DateTime StartDate, DateTime EndDate,
            short UserLevelID, short UserRoleId, int batchSize)
        {
            IList<UserMaster> result = null;

            try
            {
                result = UserFactory.UserDAL.GetUsersByDateCreated(StartDate, EndDate, UserLevelID, UserRoleId,
                    batchSize);
            }
            catch (Exception ex)
            {
                var htParams = new Hashtable(1);
                var objMethod = (MethodInfo) MethodBase.GetCurrentMethod();
                GlobalExceptionHandler.customExceptionHandler(ex, objMethod, htParams, ErrorSeverityType.Normal);
            }

            return result;
        }

        /// <summary>
        ///     Delete UserData by user and datatypecd
        /// </summary>
        /// <param name="iUserID"></param>
        /// <param name="dataTypeCD"></param>
        public void DeleteUserData(int iUserID, string dataTypeCD)
        {
            UserData userData = null;
            try
            {
                userData = GetUserDataByDataTypeCD(iUserID, dataTypeCD);
                if (userData != null) UserFactory.UserDataDAL.Delete(userData, true);
            }
            catch (Exception ex)
            {
                var htParams = new Hashtable(2);
                htParams.Add("UserID", iUserID);
                htParams.Add("DataTypeCD", dataTypeCD);
                var objMethod = (MethodInfo) MethodBase.GetCurrentMethod();
                GlobalExceptionHandler.customExceptionHandler(ex, objMethod, htParams, ErrorSeverityType.Normal);
            }
        }

        public bool IsTestUser(string email = "")
        {
            var isTestUser = false;
            try
            {
                var ipList = new List<string>(ConfigManager.GetConfig("Internal_IP").Split(','));
                var sIPAddress = CGUtility.GetUserIPAddress();
                if (ipList.Contains(sIPAddress) || email.ToLower().Contains("@cg.com"))
                    isTestUser = true;
            }
            catch (Exception ex)
            {
                var objMethod = (MethodInfo) MethodBase.GetCurrentMethod();
                var htParam = new Hashtable(1);
                htParam.Add("EmailId", email);
                GlobalExceptionHandler.customExceptionHandler(ex, objMethod, htParam, SubSystem.Presentation,
                    Application.WebApp, ErrorSeverityType.High, ex.Message, true);
            }

            return isTestUser;
        }

        /// <summary>
        /// </summary>
        /// <param name="iUserID"></param>
        /// <returns></returns>
        public void SetUserLastLoginDate(int UserID)
        {
            UserMaster usr = null;
            try
            {
                usr = UserFactory.UserDAL.GetById(UserID);
                if (usr != null)
                {
                    usr.LastLoginDate = DateTime.Now;
                    UserFactory.UserDAL.SaveOrUpdate(usr, true);
                }
            }
            catch (Exception ex)
            {
                var htParams = new Hashtable(1);
                htParams.Add("UserID", UserID);
                var objMethod = (MethodInfo) MethodBase.GetCurrentMethod();
                GlobalExceptionHandler.customExceptionHandler(ex, objMethod, htParams, ErrorSeverityType.Normal);
            }
        }

        public string GetLatestJobTitle(int UserID)
        {
            try
            {
                return UserFactory.UserDAL.GetLatestJobTitle(UserID);
            }
            catch (Exception ex)
            {
                var htParams = new Hashtable(1);
                htParams.Add("UserID", UserID.ToString());
                var objMethod = (MethodInfo) MethodBase.GetCurrentMethod();
                GlobalExceptionHandler.customExceptionHandler(ex, objMethod, htParams, ErrorSeverityType.High);
            }

            return string.Empty;
        }

        public string GetLatestJobLocation(int UserID)
        {
            try
            {
                return UserFactory.UserDAL.GetLatestJobLocation(UserID);
            }
            catch (Exception ex)
            {
                var htParams = new Hashtable(1);
                htParams.Add("UserID", UserID.ToString());
                var objMethod = (MethodInfo) MethodBase.GetCurrentMethod();
                GlobalExceptionHandler.customExceptionHandler(ex, objMethod, htParams, ErrorSeverityType.High);
            }

            return string.Empty;
        }

        public bool SaveJobAlertSubscriber(UserJobAlerts obj)
        {
            try
            {
                UserFactory.UserJobAlertsDAL.Save(obj);
                return true;
            }
            catch (Exception ex)
            {
                var htParams = new Hashtable(1);
                htParams.Add("UserJobAlerts", obj != null ? obj.ToString() : "<NULL>");
                var objMethod = (MethodInfo) MethodBase.GetCurrentMethod();
                GlobalExceptionHandler.customExceptionHandler(ex, objMethod, htParams, ErrorSeverityType.Normal);
                return false;
            }
        }

        public List<UserJobAlerts> GetJobAlertSubscribers(int userId)
        {
            List<UserJobAlerts> subscribers = null;
            try
            {
                subscribers = UserFactory.UserJobAlertsDAL.GetJobAlertSubscriber(userId);
            }
            catch (Exception ex)
            {
                var htParams = new Hashtable(1);
                htParams.Add("userId", userId);
                var objMethod = (MethodInfo) MethodBase.GetCurrentMethod();
                GlobalExceptionHandler.customExceptionHandler(ex, objMethod, htParams, ErrorSeverityType.Normal);
            }

            return subscribers;
        }

        public List<UserJobAlerts> DeleteJobAlertSubscribers(int userId)
        {
            List<UserJobAlerts> subscribers = null;
            try
            {
                UserFactory.UserJobAlertsDAL.DeleteJobAlertSubscribers(userId);
            }
            catch (Exception ex)
            {
                var htParams = new Hashtable(1);
                htParams.Add("userId", userId);
                var objMethod = (MethodInfo) MethodBase.GetCurrentMethod();
                GlobalExceptionHandler.customExceptionHandler(ex, objMethod, htParams, ErrorSeverityType.Normal);
            }

            return subscribers;
        }

        public bool IsExprNotifiedToUser(int UserID)
        {
            var isExprNotified = false;
            try
            {
                var objUserExtended = UserFactory.UserExtendedDAL.GetByUserID(UserID);
                if (objUserExtended != null)
                    isExprNotified = objUserExtended.IsExprNotified;
            }
            catch (Exception Ex)
            {
                var objMethod = (MethodInfo) MethodBase.GetCurrentMethod();
                GlobalExceptionHandler.customExceptionHandler(Ex, objMethod, null, ErrorSeverityType.Normal);
            }

            return isExprNotified;
        }

        public bool SaveUserExtendedData(int UserID)
        {
            var Status = false;
            try
            {
                var userExtended = new UserExtended();
                userExtended.UserID = UserID;
                userExtended.IsExprNotified = true;
                userExtended.TimeStamp = DateTime.Now;
                UserFactory.UserExtendedDAL.Save(userExtended);
                Status = true;
            }
            catch (Exception Ex)
            {
                var objMethod = (MethodInfo) MethodBase.GetCurrentMethod();
                GlobalExceptionHandler.customExceptionHandler(Ex, objMethod, null, ErrorSeverityType.Normal);
            }

            return Status;
        }

        public int GetPortalByUserID(int userID)
        {
            try
            {
                return UserFactory.UserDAL.GetPortalByUserID(userID);
            }
            catch (Exception ex)
            {
                var htParams = new Hashtable(1);
                htParams.Add("UserID", userID.ToString());
                var objMethod = (MethodInfo) MethodBase.GetCurrentMethod();
                GlobalExceptionHandler.customExceptionHandler(ex, objMethod, htParams, ErrorSeverityType.High);
            }

            return Products.ResumeHelpUS;
        }


        public void SaveAllOptIn(List<UserOptin> newUserOptin)
        {
            try
            {
                if (newUserOptin != null) UserFactory.UserOptinDAL.SaveOrUpdateAll(newUserOptin);
            }
            catch (Exception ex)
            {
                var htParams = new Hashtable(1);
                htParams.Add("UserID", newUserOptin[0].UserID.ToString());
                var objMethod = (MethodInfo) MethodBase.GetCurrentMethod();
                GlobalExceptionHandler.customExceptionHandler(ex, objMethod, htParams, ErrorSeverityType.Normal);
            }
        }

        public ProgressHistory GetProgressById(int userId)
        {
            var stgHistory = UserFactory.ProgressHistoryDAL.GetById(userId);
            return stgHistory;
        }

        public void SetProgressById(int userId, string preferenceCD, string value)
        {
            var stgHistory = new ProgressHistory();
            stgHistory.Id = userId;
            stgHistory.PreferenceCD = preferenceCD;
            stgHistory.Value = value;
            stgHistory.TimeStamp = DateTime.Now;
            UserFactory.ProgressHistoryDAL.SaveOrUpdate(stgHistory, true);
        }

        #region Get Methods

        public static IList<Progress> ProgressCode
        {
            get
            {
                if (lstProgressCode == null)
                    lstProgressCode = GetAllProgressCode();
                return lstProgressCode;
            }
        }

        private static IList<Progress> GetAllProgressCode()
        {
            try
            {
                return UserFactory.ProgressDAL.GetAll();
            }
            catch (Exception ex)
            {
                var objMethod = (MethodInfo) MethodBase.GetCurrentMethod();
                GlobalExceptionHandler.customExceptionHandler(ex, objMethod, null, SubSystem.Content,
                    Application.WebApp, ErrorSeverityType.High, null, true);
            }

            return null;
        }

        #endregion


        #region User Auth Methods

        public void SaveUserAuth(UserAuth userAuth)
        {
            try
            {
                userAuth.CreatedOn = DateTime.Now;
                userAuth.ModifiedOn = DateTime.Now;
                UserFactory.UserAuthDAL.Save(userAuth, true);
            }
            catch (Exception ex)
            {
                var htParams = new Hashtable(1);
                htParams.Add("UserAuth", userAuth);
                var objMethod = (MethodInfo) MethodBase.GetCurrentMethod();
                GlobalExceptionHandler.customExceptionHandler(ex, objMethod, htParams, ErrorSeverityType.Normal);
            }
        }

        public void SaveUserAuthSession(UserAuthSession userAuthSession)
        {
            try
            {
                userAuthSession.CreatedOn = DateTime.Now;
                UserFactory.UserAuthSessionDAL.Save(userAuthSession, true);
            }
            catch (Exception ex)
            {
                var htParams = new Hashtable(1);
                htParams.Add("UserAuth", userAuthSession);
                var objMethod = (MethodInfo) MethodBase.GetCurrentMethod();
                GlobalExceptionHandler.customExceptionHandler(ex, objMethod, htParams, ErrorSeverityType.Normal);
            }
        }

        public void RefreshUserAuth(int userAuthId, DateTime expiredOn)
        {
            try
            {
                UserFactory.UserAuthDAL.RefreshUserAuth(userAuthId, expiredOn);
            }
            catch (Exception ex)
            {
                var htParams = new Hashtable(2);
                htParams.Add("UserAuthID", userAuthId);
                htParams.Add("ExpiredOn", expiredOn);
                var objMethod = (MethodInfo) MethodBase.GetCurrentMethod();
                GlobalExceptionHandler.customExceptionHandler(ex, objMethod, htParams, ErrorSeverityType.Normal);
            }
        }

        public void SignOutUserAuth(int userAuthId, DateTime expiredOn)
        {
            try
            {
                UserFactory.UserAuthDAL.SignOutUserAuth(userAuthId, expiredOn);
            }
            catch (Exception ex)
            {
                var htParams = new Hashtable(2);
                htParams.Add("UserAuthID", userAuthId);
                htParams.Add("ExpiredOn", expiredOn);
                var objMethod = (MethodInfo) MethodBase.GetCurrentMethod();
                GlobalExceptionHandler.customExceptionHandler(ex, objMethod, htParams, ErrorSeverityType.Normal);
            }
        }

        #endregion
    }
}