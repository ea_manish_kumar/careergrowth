﻿using System.Linq;
using System.Text.RegularExpressions;

namespace Utilities
{
    public static class UrlUtil
    {
        private static readonly char[] InvalidChars =
            {'(', ')', '*', '.', '!', '\\', '/', '*', '?', '"', '<', '>', '|', ' ', ',', '\t', '&', '+'};

        public static bool IsSafe(string url, out string validationMsg)
        {
            validationMsg = "";
            foreach (var ch in InvalidChars)
                if (url.Contains(ch))
                {
                    validationMsg = "Validation fails! Word contains invalid character: '" + ch + "'";
                    return false;
                }

            return true;
        }

        public static string MakeSafe(string url)
        {
            foreach (var ch in InvalidChars)
                if (url.Contains(ch))
                    url = url.Replace(ch.ToString(), "");
            return url;
        }

        /// <summary>
        ///     Parses the string and replaces non string cahrs with "_".
        /// </summary>
        /// <param name="input">The input.</param>
        /// <returns></returns>
        public static string ParseString(string input)
        {
            var reg = new Regex("[^a-zA-Z0-9-]+");
            input = reg.Replace(input, "_");
            return input;
        }
    }
}