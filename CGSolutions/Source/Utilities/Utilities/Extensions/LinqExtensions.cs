﻿using System.Collections.Generic;
using System.Linq;

namespace Utilities
{
    public static class LinqExtensions
    {
        public static bool NotNullOrEmpty<T>(this IEnumerable<T> source)
        {
            return source != null && source.Any();
        }

        public static bool NullOrEmpty<T>(this IEnumerable<T> source)
        {
            return source == null || !source.Any();
        }
    }
}