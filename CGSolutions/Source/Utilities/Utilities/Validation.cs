﻿namespace Utilities
{
    public class Validation
    {
        public static bool ValidateEducation(string schoolName)
        {
            var isValid = true;

            if (CGUtility.IsEmpty(schoolName)) isValid = false;

            return isValid;
        }

        public static bool ValidateExperience(string companyName, string jobTitle)
        {
            var isValid = true;

            if (CGUtility.IsEmpty(companyName) && CGUtility.IsEmpty(jobTitle)) isValid = false;

            return isValid;
        }
    }
}