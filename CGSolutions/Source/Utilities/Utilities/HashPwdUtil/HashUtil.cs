﻿using System;
using System.Collections;
using System.Reflection;
using System.Security.Cryptography;
using CommonModules.Exceptions;
using Utilities.Constants;

namespace Utilities.HashingUtility
{
    public class HashingUtility
    {
        /// <summary>
        ///     Computes the Salt for hashing a password.
        /// </summary>
        /// <returns>A byte array of the salt.</returns>
        private static CustomException objex;

        public static byte[] GenerateSalt()
        {
            byte[] salt = null;
            try
            {
                salt = new byte[HashConstants.SALT_BYTES];
                // Create a new instance of the RNGCryptoServiceProvider.
                var rngCsp = new RNGCryptoServiceProvider();
                // Fill the array with random values.
                rngCsp.GetBytes(salt);
            }
            catch (Exception ex)
            {
                var ht = new Hashtable();
                objex = new CustomException(ex);
                var objMethod = (MethodInfo) MethodBase.GetCurrentMethod();
                GlobalExceptionHandler.customExceptionHandler(objex, objMethod, ht, ErrorSeverityType.Critical,
                    "GenerateSalt");
            }

            return salt;
        }

        /// <summary>
        ///     Generate the hash of a password.
        /// </summary>
        /// <param name="password">The password to hash.</param>
        /// <returns>A string of the obfuscated password.</returns>
        public static string ObfuscatePassword(string Password)
        {
            try
            {
                if (!string.IsNullOrEmpty(Password))
                {
                    var salt = GenerateSalt();
                    if (salt != null)
                    {
                        var hash = PBKDF2(Password, salt, HashConstants.PBKDF2_ITERATIONS, HashConstants.HASH_BYTES);
                        Password = HashConstants.PBKDF2_ITERATIONS + ":" +
                                   Convert.ToBase64String(salt) + ":" +
                                   Convert.ToBase64String(hash);
                        //sample - 1000:N9UCTPxy+IWKTSIndIJbulOhgPbyNF/+:RE5xGegFiW+EAMmrQu448LUC+tomN6pd
                    }
                }
            }
            catch (Exception ex)
            {
                var ht = new Hashtable(1);
                ht.Add("password: ", Password);
                objex = new CustomException(ex);
                var objMethod = (MethodInfo) MethodBase.GetCurrentMethod();
                GlobalExceptionHandler.customExceptionHandler(objex, objMethod, ht, ErrorSeverityType.Critical,
                    "ObfuscatePassword");
            }

            return Password;
        }

        /// <summary>
        ///     Computes the PBKDF2-SHA1 hash of a password.
        /// </summary>
        /// <param name="password">The password to hash.</param>
        /// <param name="salt">The salt.</param>
        /// <param name="iterations">The PBKDF2 iteration count.</param>
        /// <param name="outputBytes">The length of the hash to generate, in bytes.</param>
        /// <returns>A hash of the password.</returns>
        private static byte[] PBKDF2(string password, byte[] salt, int iterations, int outputBytes)
        {
            var pbkdf2 = new Rfc2898DeriveBytes(password, salt);
            pbkdf2.IterationCount = iterations;
            return pbkdf2.GetBytes(outputBytes);
        }

        /// <summary>
        ///     Validates a password given a hash of the correct one.
        /// </summary>
        /// <param name="password">The password to check.</param>
        /// <param name="goodHash">A hash of the correct password.</param>
        /// <returns>True if the password is correct. False otherwise.</returns>
        public static bool ValidatePassword(string password, string goodHash)
        {
            byte[] salt = null;
            byte[] hash = null;
            byte[] testHash = null;
            try
            {
                // Extract the parameters from the hash
                char[] delimiter = {':'};
                var split = goodHash.Split(delimiter);
                var iterations = int.Parse(split[HashConstants.ITERATION_INDEX]);
                salt = Convert.FromBase64String(split[HashConstants.SALT_INDEX]);
                hash = Convert.FromBase64String(split[HashConstants.PBKDF2_INDEX]);

                testHash = PBKDF2(password, salt, iterations, hash.Length);
            }
            catch (Exception ex)
            {
                var ht = new Hashtable(2);
                ht.Add("password: ", password);
                ht.Add("goodHash: ", goodHash);
                objex = new CustomException(ex);
                var objMethod = (MethodInfo) MethodBase.GetCurrentMethod();
                GlobalExceptionHandler.customExceptionHandler(objex, objMethod, ht, ErrorSeverityType.Critical,
                    "ValidatePassword");
            }

            return SlowEquals(hash, testHash);
        }

        /// <summary>
        ///     Compares two byte arrays in length-constant time. This comparison
        ///     method is used so that password hashes cannot be extracted from
        ///     on-line systems using a timing attack and then attacked off-line.
        /// </summary>
        /// <param name="a">The first byte array.</param>
        /// <param name="b">The second byte array.</param>
        /// <returns>True if both byte arrays are equal. False otherwise.</returns>
        private static bool SlowEquals(byte[] a, byte[] b)
        {
            var diff = (uint) a.Length ^ (uint) b.Length;
            for (var i = 0; i < a.Length && i < b.Length; i++)
                diff |= (uint) (a[i] ^ b[i]);
            return diff == 0;
        }
    }
}