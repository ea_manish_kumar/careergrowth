﻿using System;
using System.Configuration;
using System.Diagnostics;
using System.IO;
using System.Threading.Tasks;

namespace Utilities
{
    public class PDFProperties
    {
        public double LeftRightMargin { get; set; }
        public double TopBottomMargin { get; set; }

        public bool IsFooterPresent { get; set; }
        public string FooterText { get; set; }

        /// <summary>
        ///     PageSize needs to be Letter or A4
        /// </summary>
        public string PageSize { get; set; }
    }

    public class PDFUtility
    {
        public byte[] ConvertHTMLToPDF(string htmlContent, string uniqueFileName, PDFProperties properties)
        {
            //Dump all the contents to a local file path
            var folderPath = Path.Combine(AppDomain.CurrentDomain.BaseDirectory, "Documents");
            var inputPath = Path.Combine(AppDomain.CurrentDomain.BaseDirectory, "Documents", uniqueFileName + ".htm");
            var outputPath = Path.Combine(AppDomain.CurrentDomain.BaseDirectory, "Documents", uniqueFileName + ".pdf");

            //Create the directory in the first go
            if (!Directory.Exists(folderPath))
                Directory.CreateDirectory(folderPath);

            //Rename the top padding class
            htmlContent = htmlContent.Replace(".top-remove", ".do-not-apply");

            File.WriteAllText(inputPath, htmlContent);
            byte[] pdfData = null;
            var footerText = string.Empty;
            var currentDirectory = new DirectoryInfo(AppDomain.CurrentDomain.BaseDirectory);
            var exePath = string.Concat(currentDirectory.Parent.FullName,
                ConfigurationManager.AppSettings["PDFUtilityPath"]);
            //Unable to find PDF Exe Path, throw an exception
            if (!File.Exists(exePath))
                throw new Exception(exePath);
            if (string.IsNullOrEmpty(properties.PageSize))
                properties.PageSize = "Letter";
            
            //TODO: Rename the XYZ by domain name
            if (properties.IsFooterPresent)
            {
                if (string.IsNullOrEmpty(properties.FooterText))
                    footerText = "Created using XYZ";
                else
                    footerText = properties.FooterText;
            }

            var arguments = string.Format(
                " --zoom 1.25 --dpi 110 --javascript-delay 1500 -L {0} -R {1} -T {2} -B {3} --page-size {4} --footer-center \"{5}\" --footer-font-size 6 \"{6}\" \"{7}\"",
                properties.LeftRightMargin, properties.LeftRightMargin, properties.TopBottomMargin,
                properties.TopBottomMargin, properties.PageSize, footerText, inputPath, outputPath);
            ExecuteCommandSync(exePath, arguments);

            if (File.Exists(outputPath))
                pdfData = File.ReadAllBytes(outputPath);

            //Delete the temporary files
            Task.Run(() => DeleteFiles(inputPath, outputPath));

            return pdfData;
        }

        private void DeleteFiles(string inputPath, string outputPath)
        {
            //TODO:Uncomment the statement for delete
            //DeleteFile(inputPath);
            DeleteFile(outputPath);
        }

        /// <summary>
        ///     Delete File
        /// </summary>
        /// <param name="filePath"></param>
        private void DeleteFile(string filePath)
        {
            try
            {
                if (File.Exists(filePath))
                    File.Delete(filePath);
            }
            catch
            {
                //Swallow Exception
            }
        }

        /// <summary>
        ///     Executes a shell command synchronously.
        ///     Use this overloaded method if command have path with space(s).
        /// </summary>
        /// <param name="command">string command</param>
        /// <param name="argument">string argument</param>
        /// <returns>string, as output of the command.</returns>
        public void ExecuteCommandSync(string command, string argument)
        {
            Process proc = null;
            try
            {
                proc = new Process();
                proc.EnableRaisingEvents = false;
                proc.StartInfo.FileName = command;
                proc.StartInfo.Arguments = argument;
                // The following commands are needed to redirect the standard output.
                // This means that it will be redirected to the Process.StandardOutput StreamReader.
                // Do not create the black window.
                proc.StartInfo.RedirectStandardOutput = true;
                proc.StartInfo.UseShellExecute = false;
                proc.StartInfo.CreateNoWindow = true;
                proc.Start();
                proc.WaitForExit();
            }
            catch (Exception objException)
            {
                //TODO:Capture any exception
                //Logger.Write(objException);
            }
            finally
            {
                if (proc != null)
                {
                    proc.Close();
                    proc.Dispose();
                }
            }
        }
    }
}