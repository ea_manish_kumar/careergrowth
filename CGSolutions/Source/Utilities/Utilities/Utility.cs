﻿using System;
using System.CodeDom;
using System.CodeDom.Compiler;
using System.Collections;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Diagnostics;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Net;
using System.Reflection;
using System.Security.Cryptography;
using System.Text;
using System.Text.RegularExpressions;
using System.Web;
using System.Web.Services.Description;
using System.Xml;
using System.Xml.Schema;
using System.Xml.Serialization;
using CommonModules.Caching;
using CommonModules.Configuration;
using CommonModules.Exceptions;
using Utilities.Communication;
using Utilities.Constants;
using Utilities.Constants.Documents;

namespace Utilities
{
    /// <summary>
    ///     Contains FW wise utility methods
    /// </summary>
    public class CGUtility
    {
        private static XmlDocument InvaidEmailPrefixDoc;
        private static List<string> lstCanada;

        /// <summary>
        ///     Regex checks a given string if it only contains numbers
        /// </summary>
        /// <param name="text">number as text</param>
        /// <returns>true if valid, else false</returns>
        public static bool IsNumber(string text)
        {
            var regex = new Regex(@"^[-+]?[0-9]*\.?[0-9]+$");
            return regex.IsMatch(text);
        }

        /// <summary>
        ///     Determines whether the specified any string is numeric.
        /// </summary>
        /// <param name="anyString">Any string.</param>
        /// <returns>
        ///     <c>true</c> if the specified any string is numeric; otherwise, <c>false</c>.
        /// </returns>
        public static bool IsNumeric(string anyString)
        {
            if (anyString == null) anyString = "";
            if (anyString.Length > 0)
            {
                var dummyOut = new double();
                var cultureInfo =
                    new CultureInfo("en-US", true);

                return double.TryParse(anyString, NumberStyles.Any,
                    cultureInfo.NumberFormat, out dummyOut);
            }

            return false;
        }

        /// <summary>
        ///     Determines whether the specified any string is date.
        /// </summary>
        /// <param name="anyString">Any string.</param>
        /// <returns>
        ///     <c>true</c> if the specified any string is date; otherwise, <c>false</c>.
        /// </returns>
        public static bool IsDate(string anyString)
        {
            if (anyString == null) anyString = "";
            if (anyString.Length > 0)
            {
                var dummyDate = DateTime.MinValue;
                try
                {
                    dummyDate = DateTime.Parse(anyString);
                }
                catch (Exception ex)
                {
                    var str = ex.Message;
                    return false;
                }

                return true;
            }

            return false;
        }

        /// <summary>
        ///     Checks whether the provided email address is in valid form.
        /// </summary>
        /// <param name="emailAddress">email address to check</param>
        /// <returns>true if valid, else false</returns>
        public static bool IsValidEmailAddress(string emailAddress)
        {
            var isValidEmail = false;
            var theEmailPattern = @"^([A-Za-z0-9_\.-]+)@([\dA-Za-z\.-]+)\.([A-Za-z\.]{2,6})$";

            isValidEmail = Regex.IsMatch(emailAddress, theEmailPattern);

            if (isValidEmail)
                isValidEmail = GenericPrefixInEmail(emailAddress);

            return isValidEmail;
        }

        private static bool GenericPrefixInEmail(string emailAddress)
        {
            if (InvaidEmailPrefixDoc == null)
            {
                InvaidEmailPrefixDoc = new XmlDocument();
                var sXmlPath = HttpContext.Current.Server.MapPath("~/resources/") + "GenericPrefixInEmailAddress.xml";
                InvaidEmailPrefixDoc.Load(sXmlPath);
            }

            var isValidPrefix = true;

            var xnList = InvaidEmailPrefixDoc.SelectNodes("/Filters/Filter");
            foreach (XmlNode xn in xnList)
            {
                var prefix = xn["value"].InnerText;
                if (emailAddress.Contains(prefix))
                {
                    isValidPrefix = false;
                    break;
                }
            }

            return isValidPrefix;
        }

        /// <summary>
        ///     Obfuscates the credit card number.
        /// </summary>
        /// <param name="ccNumber">The cc number.</param>
        /// <returns></returns>
        public static string ObfuscateCreditCardNumber(string ccNumber)
        {
            if (ccNumber != null && ccNumber.Trim() != "" && ccNumber.Length > 6)
                return ccNumber.Substring(0, 2) + "XXXXXXXXXX" + ccNumber.Substring(ccNumber.Length - 4, 4);

            return "";
        }

        /// <summary>
        ///     Checks if a provided object's type is a nullable (int?)
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="obj">The obj.</param>
        /// <returns>
        ///     <c>true</c> if the specified obj is nullable; otherwise, <c>false</c>.
        /// </returns>
        public static bool IsNullable<T>(T obj)
        {
            if (obj == null) return true; // obvious
            var type = typeof(T);
            if (!type.IsValueType) return true; // ref-type
            if (Nullable.GetUnderlyingType(type) != null) return true; // Nullable<T>
            return false; // value-type
        }

        /// <summary>
        ///     Get Current IP Address of the running system
        /// </summary>
        /// <returns></returns>
        public static IPAddress GetCurrentIPAddress()
        {
            var myHost = Dns.GetHostName();
            return Dns.GetHostEntry(myHost).AddressList[0];
        }

        /// <summary>
        ///     Get Current IP Address of the User.
        /// </summary>
        /// <returns></returns>
        public static string GetUserIPAddress()
        {
            var ipAddress = string.Empty;
            var ipAddressList =
                !string.IsNullOrEmpty(HttpContext.Current.Request.ServerVariables[RequestHeader.HTTP_X_FORWARDED_FOR])
                    ? HttpContext.Current.Request.ServerVariables[RequestHeader.HTTP_X_FORWARDED_FOR]
                    : HttpContext.Current.Request.ServerVariables[RequestHeader.REMOTE_ADDR];
            if (!string.IsNullOrEmpty(ipAddressList))
            {
                var clientIpAddress = ipAddressList.Split(',')[0];
                ipAddress = StripPortFromIPAddress(clientIpAddress.Trim());
            }

            if (string.IsNullOrEmpty(ipAddress) || ipAddress == "::1") ipAddress = "127.0.0.1";
            return ipAddress;
        }

        public static string StripPortFromIPAddress(string ipAddress)
        {
            var splitList = ipAddress.Split(':');
            if (splitList.Length == 1) return ipAddress;

            if (splitList.Length == 2)
            {
                ipAddress = splitList[0];
            }
            else
            {
                IPAddress ip;
                if (IPAddress.TryParse(ipAddress, out ip))
                    ipAddress = ip.ToString();
            }

            return ipAddress;
        }

        public static string GetUTMSource()
        {
            var sSource = string.Empty;
            var sTarget = string.Empty;
            var sReferrer = string.Empty;

            //Sequence flow to segregate users            
            //1st is UTM Source read from QueryString, override any cookie values captured in the 1st Step/If not found in QS, Read from cookie
            //2nd If source is still empty, check if HTTP_Referer exists: Source: Only capture the URL without QS params. 
            //If the Referer is from Google or Bing, capture the source as Google or Bing else capture the HTTP_REFERER Value
            //Also, if HTTP_Referer exists: Medium: Organic (if Referrer is Google/Bing) else Medium: Referral
            //3rd is QS - Read from src/source
            //4th is if nothing is present: Direct

            //Check the UTM_SOURCE details
            if (HttpContext.Current.Request.QueryString[UserTracking.UTM_SOURCE] != null)
            {
                sSource = HttpContext.Current.Request.QueryString[UserTracking.UTM_SOURCE];
                if (!string.IsNullOrEmpty(sSource))
                    WriteCookie(UserTracking.UTM_SOURCE, sSource, true);
            }
            else
            {
                var utmSource = ReadCookie(UserTracking.UTM_SOURCE);
                if (!string.IsNullOrEmpty(utmSource))
                    sSource = utmSource;
            }

            //Capture the data from HTTP_Referrer
            if (string.IsNullOrEmpty(sSource) &&
                !string.IsNullOrEmpty(HttpContext.Current.Request.ServerVariables[UserTracking.HTTP_REFERER]))
            {
                var referrer = Convert.ToString(HttpContext.Current.Request.ServerVariables[UserTracking.HTTP_REFERER]);

                //If its a search engine result, mark the source from Search Engines directly
                //else if it contains URL with QueryString Params, take only the main URL
                //else directly record the referer value
                if (referrer.ToLower().StartsWith(HttpContext.Current.Request.Url.Scheme.ToLower() + "://" +
                                                  HttpContext.Current.Request.Url.Host.ToLower()))
                    sSource = UserTracking.INTERNAL_REFERRAL;
                else if (referrer.Contains(UserTracking.GOOGLE))
                    sSource = UserTracking.GOOGLE;
                else if (referrer.Contains(UserTracking.BING))
                    sSource = UserTracking.BING;
                else if (referrer.Contains(UserTracking.QS_SEPARATOR))
                    sSource = referrer.Split(UserTracking.QS_SEPARATOR)[0];
                else
                    sSource = referrer;
            }

            //Read from QueryString SRC/Source details if present
            if (string.IsNullOrEmpty(sSource) &&
                !string.IsNullOrEmpty(HttpContext.Current.Request.QueryString[UserTracking.QS_SOURCE]))
                sSource = HttpContext.Current.Request.QueryString[UserTracking.QS_SOURCE];
            else if (string.IsNullOrEmpty(sSource) &&
                     !string.IsNullOrEmpty(HttpContext.Current.Request.QueryString[UserTracking.QS_SRC]))
                sSource = HttpContext.Current.Request.QueryString[UserTracking.QS_SRC];

            if (string.IsNullOrEmpty(sSource))
                sSource = UserTracking.DIRECT;

            return sSource;
        }

        public static string GetUTMCampaign()
        {
            var utm_campaign = string.Empty;
            if (HttpContext.Current.Request.QueryString[UserTracking.UTM_CAMPAIGN] != null)
                utm_campaign = HttpContext.Current.Request.QueryString[UserTracking.UTM_CAMPAIGN];
            return utm_campaign;
        }

        public static string GetUTMMedium()
        {
            var utm_medium = string.Empty;
            if (HttpContext.Current.Request.QueryString[UserTracking.UTM_MEDIUM] != null)
            {
                utm_medium = HttpContext.Current.Request.QueryString[UserTracking.UTM_MEDIUM];
            }
            else if (!string.IsNullOrEmpty(HttpContext.Current.Request.ServerVariables[UserTracking.HTTP_REFERER]))
            {
                var referrer = Convert.ToString(HttpContext.Current.Request.ServerVariables[UserTracking.HTTP_REFERER]);
                if (referrer.ToLower().StartsWith(HttpContext.Current.Request.Url.Scheme.ToLower() + "://" +
                                                  HttpContext.Current.Request.Url.Host.ToLower()))
                    utm_medium = UserTracking.INTERNAL_MEDIUM;
                //Its a search engine result, mark the medium as organic
                else if (referrer.Contains(UserTracking.GOOGLE) || referrer.Contains(UserTracking.BING))
                    utm_medium = UserTracking.ORGANIC;
                else
                    utm_medium = UserTracking.REFERRAL;
            }

            return utm_medium;
        }

        public static string GetUnprocessedUTMSource()
        {
            var utm_source = string.Empty;
            if (HttpContext.Current.Request.QueryString[UserTracking.UTM_SOURCE] != null)
                utm_source = HttpContext.Current.Request.QueryString[UserTracking.UTM_SOURCE];
            return utm_source;
        }

        public static string GetFullRequestedURL()
        {
            var requested_url = string.Empty;
            if (HttpContext.Current.Request.Url != null) requested_url = HttpContext.Current.Request.Url.ToString();
            return requested_url;
        }

        public static string GetUrlReferrer()
        {
            var url_Referrer = string.Empty;
            if (HttpContext.Current.Request.UrlReferrer != null)
                url_Referrer = HttpContext.Current.Request.UrlReferrer.ToString();
            return url_Referrer;
        }

        public static string GetUTMTerm()
        {
            var utm_term = string.Empty;
            if (HttpContext.Current.Request.QueryString[UserTracking.UTM_TERM] != null)
                utm_term = HttpContext.Current.Request.QueryString[UserTracking.UTM_TERM];
            return utm_term;
        }

        public static string GetUTMKeyword()
        {
            var utm_keyword = string.Empty;
            if (HttpContext.Current.Request.QueryString[UserTracking.UTM_KEYWORD] != null)
                utm_keyword = HttpContext.Current.Request.QueryString[UserTracking.UTM_KEYWORD];
            return utm_keyword;
        }

        public static string GetUTMContent()
        {
            var utm_content = string.Empty;
            if (HttpContext.Current.Request.QueryString[UserTracking.UTM_CONTENT] != null)
                utm_content = HttpContext.Current.Request.QueryString[UserTracking.UTM_CONTENT];
            return utm_content;
        }

        /// <summary>
        ///     Gets a string from MemoryStream. Doesn't close/dispose the MemoryStream.
        /// </summary>
        /// <param name="memoryStream">The MemoryStream to extract the string from.</param>
        /// <returns></returns>
        public static string GetStringFromMemoryStream(MemoryStream memoryStream)
        {
            if (memoryStream == null)
                return null;

            var origpos = memoryStream.Position;

            memoryStream.Position = 0;

            var text = Encoding.ASCII.GetString(memoryStream.GetBuffer(), 0, (int) memoryStream.Length);
            memoryStream.Position = origpos;
            return text;
        }

        /// <summary>
        ///     Gets a MemoryStream from string.
        /// </summary>
        /// <param name="data">The data.</param>
        /// <returns></returns>
        public static MemoryStream GetMemoryStreamFromString(string data)
        {
            if (string.IsNullOrEmpty(data))
                return null;

            var ms = new MemoryStream(Encoding.ASCII.GetBytes(data));
            ms.Position = 0;
            return ms;
        }

        /// <summary>
        ///     Serializes a given object to XML.
        /// </summary>
        /// <param name="obj">The object to Serialize.</param>
        /// <param name="type">The type of the object.</param>
        /// <returns>Object serialized as Xml</returns>
        public static string SerializeObjectToXml(object obj, Type type)
        {
            if (obj == null) return null;

            // Vikas Jindal (3/30/2013): We need to send a xml of the serialized object now
            var x = new XmlSerializer(type);
            var retXml = string.Empty;

            using (var ms = new MemoryStream())
            {
                x.Serialize(ms, obj);
                retXml = GetStringFromMemoryStream(ms);
            }

            return retXml;
        }


        /// <summary>
        ///     Deserializes an object from serialized Xml
        /// </summary>
        /// <typeparam name="T">Type of the expected object</typeparam>
        /// <param name="xml">Xml obtained via Serialization</param>
        /// <returns>the deserialized object</returns>
        public static T DesrializeObjectFromXml<T>(string xml)
        {
            if (string.IsNullOrEmpty(xml)) return default(T);

            // Vikas Jindal (3/30/2013): We need to send a xml of the serialized object now
            var x = new XmlSerializer(typeof(T));
            var rettype = default(T);

            var str = GetMemoryStreamFromString(xml);

            if (str == null)
                throw new ArgumentException("Failed to stream the input string.");

            rettype = (T) x.Deserialize(str);
            return rettype;
        }

        /// <summary>
        ///     Checks if a provided decimal value has "." in it or not, if not it appends it
        /// </summary>
        /// <param name="decValue">The decimal value to check</param>
        /// <returns>
        ///     Another decimal with ".00" if no "." is found.
        /// </returns>
        public static decimal GetDecimalWithDotAndZeros(decimal decValue)
        {
            var decvaluestr = decValue.ToString("#0.00");
            return decimal.Parse(decvaluestr);
        }

        /// <summary>
        ///     Converts a string sentence to title case.
        /// </summary>
        /// <param name="inputString">The input string.</param>
        /// <returns></returns>
        public static string ConvertToTitleCase(string inputString)
        {
            var strtitleCase = string.Empty;
            string[] sTitleArray = null;
            var sLoopString = string.Empty;
            var i = 0;
            char[] splitchar = {' '};

            sTitleArray = inputString.Split(splitchar);

            if (sTitleArray != null && sTitleArray.Length > 0)
            {
                for (i = 0; i <= sTitleArray.Length - 1; i++)
                    if (sTitleArray[i].Trim().Length > 0)
                    {
                        sLoopString = sTitleArray[i].Substring(0, 1).ToUpper() +
                                      sTitleArray[i].Substring(1, sTitleArray[i].Length - 1).ToLower();
                        strtitleCase = strtitleCase + " " + sLoopString;
                    }

                strtitleCase = strtitleCase.Trim();
            }

            return strtitleCase;
        }

        /// <summary>
        ///     Replace only the first instance of the string item.
        /// </summary>
        /// <param name="input"> the input string variable which need to manipulated</param>
        /// <param name="searchvalue">the search item required to be replaced</param>
        /// <param name="replacement">the replacement item </param>
        /// <returns></returns>
        public static string ReplaceFirstItem(string input, string searchvalue, string replacement)
        {
            var pos = input.IndexOf(searchvalue);
            if (pos < 0) return input;
            return input.Substring(0, pos) + replacement + input.Substring(pos + searchvalue.Length);
        }


        /// <summary>
        ///     Validates if a provided regular expression matches the provided input
        /// </summary>
        /// <param name="sInput">The provided input</param>
        /// <param name="sRegExp">Thes reg exp .</param>
        /// <returns>true if the regex matches</returns>
        public static bool ValidateRegExp(string sInput, string sRegExp)
        {
            var functionReturnValue = false;

            var RegexObj = new Regex(sRegExp, RegexOptions.IgnoreCase);
            var MatchObj = RegexObj.Match(sInput);

            if (MatchObj.Success) functionReturnValue = true;

            return functionReturnValue;
        }

        /// <summary>
        ///     Validates the XML schema.
        /// </summary>
        /// <param name="xml">The XML.</param>
        /// <param name="targetNamespace">The target namespace.</param>
        /// <param name="schemaFileName">Name of the schema file.</param>
        /// <returns></returns>
        public static string ValidateXmlSchema(string xml, string targetNamespace, string schemaFileName)
        {
            var errormessages = new StringBuilder();

            try
            {
                var settings = new XmlReaderSettings();
                settings.ValidationType = ValidationType.Schema;
                settings.ValidationFlags |= XmlSchemaValidationFlags.ReportValidationWarnings;
                settings.Schemas.Add(targetNamespace, schemaFileName);

                //Useful when u want all compilation errors in one go..
                //settings.ValidationEventHandler += new System.Xml.Schema.ValidationEventHandler(ShowCompileErrors);

                using (var reader = XmlReader.Create(new StringReader(xml), settings))
                {
                    while (reader.Read())
                    {
                    }
                }

                Debug.WriteLine("Completed validating xmlfragment");
            }
            catch (XmlSchemaException XmlSchExp)
            {
                //Debug.WriteLine(XmlSchExp.Message);
                errormessages.Append(XmlSchExp.Message);
            }
            catch (Exception ex)
            {
                var htParams = new Hashtable(2);
                htParams.Add("xml", xml);
                htParams.Add("schemaFileName", schemaFileName);
                var objMethod = (MethodInfo) MethodBase.GetCurrentMethod();
                GlobalExceptionHandler.customExceptionHandler(ex, objMethod, htParams, ErrorSeverityType.Normal);
            }

            return errormessages.ToString();
        }

        /// <summary>
        ///     posts the provided data to the Url provided.
        /// </summary>
        /// <param name="url">The URL , can include query string parameters.</param>
        /// <param name="HttpMethod">The HTTP methodcan only be "GET" or "POST".</param>
        /// <param name="dataToPost">The data to post.</param>
        /// <returns>The webresponse deserialized as string</returns>
        public static string DoHttpPOST(string url, string HttpMethod, string dataToPost)
        {
            string retresponse = null;

            try
            {
                var request = (HttpWebRequest) WebRequest.Create(url);
                request.Method = HttpMethod;
                request.ContentType = "text/xml";
                request.Timeout = 60000;
                if (HttpMethod == "POST")
                {
                    var data = Encoding.UTF8.GetBytes(dataToPost);
                    var newStream = request.GetRequestStream();
                    newStream.Write(data, 0, data.Length);
                    newStream.Close();
                }

                var response = request.GetResponse();

                var respstream = response.GetResponseStream();
                var encode = Encoding.GetEncoding("utf-8");
                var sr = new StreamReader(respstream, encode);

                retresponse = sr.ReadToEnd();
            }
            catch (WebException webEx) // catch web related exceptions
            {
                var errorString = new StringBuilder();
                // Protocol errors are things like
                // 404 - Not Found
                // 401 - Unauthorized etc...
                if (webEx.Status == WebExceptionStatus.ProtocolError)
                {
                    // we can get the actual status code from the original response
                    // that is exposed through the Response property of the WebException class
                    errorString.AppendFormat("Status Code : {0}", ((HttpWebResponse) webEx.Response).StatusCode);

                    // the same goes for the description of the returned status
                    errorString.AppendFormat("Status Description : {0}",
                        ((HttpWebResponse) webEx.Response).StatusDescription);
                }

                throw new Exception(errorString.ToString(), webEx);
            }
            catch (Exception ex)
            {
                var htParams = new Hashtable(3);
                htParams.Add("url", url);
                htParams.Add("HTTPMethod", HttpMethod);
                htParams.Add("dataToPost", dataToPost);
                var objMethod = (MethodInfo) MethodBase.GetCurrentMethod();
                GlobalExceptionHandler.customExceptionHandler(ex, objMethod, htParams, ErrorSeverityType.Normal);
            }

            return retresponse;
        }

        /// <summary>
        ///     Reads the raw HTTP request data.
        /// </summary>
        /// <param name="request">The request.</param>
        /// <returns></returns>
        public static string ReadRawHttpRequestData(HttpRequest request)
        {
            string rawdata = null;

            try
            {
                var requestStream = new MemoryStream();

                if (request.ContentLength > 0)
                {
                    var buffer = new byte[request.ContentLength];
                    request.InputStream.Read(buffer, 0, request.ContentLength);
                    requestStream.Write(buffer, 0, request.ContentLength);

                    rawdata = GetStringFromMemoryStream(requestStream);
                    requestStream.Close();
                    requestStream.Dispose();
                }
            }
            catch (Exception ex)
            {
                var htParams = new Hashtable(3);
                htParams.Add("Method", request.HttpMethod);
                htParams.Add("Path", request.Path);
                htParams.Add("Server vars", DumpNameValueCollection(request.ServerVariables));
                var objMethod = (MethodInfo) MethodBase.GetCurrentMethod();
                GlobalExceptionHandler.customExceptionHandler(ex, objMethod, htParams, ErrorSeverityType.Normal);
            }

            return rawdata;
        }

        /// <summary>
        ///     Dumps the name value collection.
        /// </summary>
        /// <param name="myCol">My col.</param>
        public static string DumpNameValueCollection(NameValueCollection myCol)
        {
            if (myCol == null) return string.Empty;
            var sb = new StringBuilder();

            var myEnumerator = myCol.GetEnumerator();

            foreach (var s in myCol.AllKeys)
                sb.AppendLine(string.Format("{0} - {1}", s, myCol[s]));

            return sb.ToString();
        }

        /// <summary>
        ///     Helper Method to dump a StringDictionary
        /// </summary>
        /// <param name="stringdict">The StringDictionary instance.</param>
        /// <returns></returns>
        public static string DumpStringDictionary(StringDictionary stringdict)
        {
            if (stringdict == null) return string.Empty;
            var sb = new StringBuilder();

            foreach (DictionaryEntry keyval in stringdict)
                sb.AppendLine("Key: " + Convert.ToString(keyval.Key) + " Key Value: " + Convert.ToString(keyval.Value));
            return sb.ToString();
        }

        /// <summary>
        ///     Dumps a given IList.
        /// </summary>
        /// <param name="lst">The IList to dump.</param>
        /// <returns></returns>
        public static string DumpIList(IList<Type> lst)
        {
            if (lst == null) return string.Empty;

            var sb = new StringBuilder();
            foreach (var item in lst) sb.AppendLine(item.ToString());
            return sb.ToString();
        }


        /// <summary>
        ///     Dumps a provided object array.
        /// </summary>
        /// <param name="array">The array.</param>
        /// <returns>a String with array's contents</returns>
        public static string DumpObjectArray(object[] array)
        {
            if (array == null) return "<NULL>";

            var sb = new StringBuilder(array.Length + 2);

            for (var i = 0; i < array.Length; i++) sb.AppendLine("[" + i + "]" + array[i]);
            return sb.ToString();
        }

        /// <summary>
        ///     Dumps  a provided string array.
        /// </summary>
        /// <param name="array">The array.</param>
        /// <returns>a String with array's contents</returns>
        public static string DumpStringArray(string[] array)
        {
            if (array == null) return "<NULL>";

            var sb = new StringBuilder(array.Length + 2);

            for (var i = 0; i < array.Length; i++) sb.AppendLine("[" + i + "]" + array[i]);
            return sb.ToString();
        }

        public static bool IsHashedPassword()
        {
            var bReturn = false;
            if (!string.IsNullOrEmpty(ConfigManager.GetConfig("USE_HASH_PWD")))
                bReturn = Convert.ToBoolean(ConfigManager.GetConfig("USE_HASH_PWD"));

            return bReturn;
        }

        public static string GetEncryptedData(string sData)
        {
            if (!string.IsNullOrEmpty(sData))
            {
                var data = Encoding.ASCII.GetBytes(sData);
                var rgbKey = Encoding.ASCII.GetBytes("12345678");
                var rgbIV = Encoding.ASCII.GetBytes("87654321");
                //1024-bit encryption
                var memoryStream = new MemoryStream(1024);
                var desCryptoServiceProvider = new DESCryptoServiceProvider();
                var cryptoStream = new CryptoStream(memoryStream,
                    desCryptoServiceProvider.CreateEncryptor(rgbKey, rgbIV), CryptoStreamMode.Write);
                cryptoStream.Write(data, 0, data.Length);
                cryptoStream.FlushFinalBlock();
                var result = new byte[(int) memoryStream.Position];
                memoryStream.Position = 0;
                memoryStream.Read(result, 0, result.Length);
                cryptoStream.Close();
                memoryStream.Dispose();
                sData = Convert.ToBase64String(result);
            }

            return sData;
        }

        public static string GetDecryptedData(string sEncryptedData)
        {
            var decrypted = string.Empty;
            if (!string.IsNullOrEmpty(sEncryptedData))
            {
                if (sEncryptedData.Contains("keeploggedin"))
                {
                    sEncryptedData = GetEncryptedData(sEncryptedData);
                    if (HttpContext.Current.Request.Cookies["userinfo"] != null)
                        if (HttpContext.Current.Request.Cookies["userinfo"]["userid"] != null)
                            CreateKeepLoggedInCookie(
                                Convert.ToInt32(HttpContext.Current.Request.Cookies["userinfo"]["userid"]));
                }

                var data = Convert.FromBase64String(sEncryptedData);
                var rgbKey = Encoding.ASCII.GetBytes("12345678");
                var rgbIV = Encoding.ASCII.GetBytes("87654321");
                var memoryStream = new MemoryStream(data.Length);
                var desCryptoServiceProvider = new DESCryptoServiceProvider();
                var x = desCryptoServiceProvider.CreateDecryptor(rgbKey, rgbIV);
                var cryptoStream = new CryptoStream(memoryStream, x, CryptoStreamMode.Read);
                memoryStream.Write(data, 0, data.Length);
                memoryStream.Position = 0;
                decrypted = new StreamReader(cryptoStream).ReadToEnd();
                cryptoStream.Close();
                memoryStream.Dispose();
            }

            return decrypted;
        }

        public static bool IsEncryptedCookieKeyExists(string sCookieName, string sKey)
        {
            var bExists = false;
            if (!string.IsNullOrEmpty(GetDecryptedCookieKeyValue(sCookieName, sKey))) bExists = true;
            return bExists;
        }


        public static string GetDecryptedCookieKeyValue(string sCookieName, string sKey)
        {
            var sCookieValue = string.Empty;
            var sValue = string.Empty;

            if (HttpContext.Current.Request.Cookies[sCookieName] != null)
            {
                sValue = GetDecryptedData(HttpContext.Current.Request.Cookies[sCookieName].Value);
                if (!string.IsNullOrEmpty(sValue))
                {
                    var qscoll = HttpUtility.ParseQueryString(sValue);
                    foreach (var s in qscoll.AllKeys)
                        if (s == sKey)
                            sCookieValue = qscoll[s];
                }
            }

            return sCookieValue;
        }

        public static void CreateKeepLoggedInCookie(int iUserID, string sKeepMeLoggedIn = "true", string sDomain = "")
        {
            var sCookieValue = string.Empty;
            HttpCookie Cookie = null;
            if (HttpContext.Current.Request.Cookies["userinfo"] == null)
                Cookie = new HttpCookie("userinfo");
            else
                Cookie = HttpContext.Current.Request.Cookies["userinfo"];

            sCookieValue = "userid=" + iUserID;
            sCookieValue = sCookieValue + "&keeploggedin=" + sKeepMeLoggedIn;
            Cookie.Value = GetEncryptedData(sCookieValue);
            Cookie.Expires = DateTime.Now.AddYears(1);
            HttpContext.Current.Response.Cookies.Add(Cookie);
        }

        public static string HashText(string text)
        {
            var hasher = new SHA512Managed();
            var salt = "W@e$lc0me@199916h@$&$#";
            var textWithSaltBytes = Encoding.UTF8.GetBytes(string.Concat(text, salt));
            var hashedBytes = hasher.ComputeHash(textWithSaltBytes);
            hasher.Clear();
            return Convert.ToBase64String(hashedBytes);
        }

        public static string GetDomainURLFromConfig()
        {
            return HttpContext.Current.Request.Url.GetLeftPart(UriPartial.Authority);
        }

        public static string GetDomainCDFromConfig()
        {
            return ConfigManager.GetConfig("DomainCD");
        }

        public static string GetDomainTitleFromConfig()
        {
            return ConfigManager.GetConfig("DomainTitle");
        }

        public static int GetPortalId()
        {
            var portalId = 0;
            int.TryParse(ConfigManager.GetConfig("PortalID"), out portalId);
            return portalId;
        }

        /// <summary>
        ///     Get PageTitle From Config
        /// </summary>
        /// <returns>Returns Page Title</returns>
        public static string GetDomainTitle()
        {
            return ConfigManager.GetConfig("DomainTitle");
        }

        public static string GetCustomerServiceEmailFromConfig()
        {
            return ConfigManager.GetConfig("DomainMail");
        }

        public string GetFromContext(string keyName)
        {
            var value = string.Empty;
            if (HttpContext.Current != null)
            {
                if (HttpContext.Current.Items[keyName] != null)
                    value = HttpContext.Current.Items[keyName].ToString();

                if (string.IsNullOrEmpty(value))
                    value = GetContextFromURL();
            }

            return value;
        }

        private string GetContextFromURL()
        {
            return ConfigManager.GetConfig("DomainCD");
        }

        public int GetQSint(string Key)
        {
            var iQSString = -1;
            if (IsKeyInQS(Key)) int.TryParse(HttpContext.Current.Request.QueryString[Key], out iQSString);
            return iQSString;
        }

        public string GetQString(string Key)
        {
            var sQSString = "";
            if (IsKeyInQS(Key)) sQSString = HttpContext.Current.Request.QueryString[Key].Trim();
            return sQSString;
        }

        public bool IsKeyInQS(string Key)
        {
            var sQSString = HttpContext.Current.Request.QueryString[Key];
            if (sQSString != null)
                return true;
            return false;
        }

        public static string GetValueFromQS(string queryString, string key)
        {
            var qsCollection = HttpUtility.ParseQueryString(queryString);
            if (qsCollection != null)
                return qsCollection.Get(key);
            return string.Empty;
        }

        /// <summary>
        ///     Check if, provided input is empty, then return true
        /// </summary>
        /// <param name="input"></param>
        /// <returns></returns>
        public static bool IsEmpty(string input)
        {
            return string.IsNullOrEmpty(input);
        }

        /// <summary>
        ///     Returns the file format
        /// </summary>
        public static void GetFileFormat(string docType, out string fileFormat, out string mimeType,
            out string extension)
        {
            mimeType = "";

            try
            {
                switch (docType.ToUpper())
                {
                    case FileTypeCD.PDF:
                    default:
                        fileFormat = FileTypeCD.PDF;
                        mimeType = "Application/pdf";
                        extension = ".pdf";
                        break;
                    case FileTypeCD.Word:
                        fileFormat = FileTypeCD.Word;
                        mimeType = "application/msword";
                        extension = ".docx";
                        break;
                    case FileTypeCD.Text:
                        fileFormat = FileTypeCD.Text;
                        mimeType = "text/plain";
                        extension = ".txt";
                        break;
                    case FileTypeCD.HTML:
                    case FileTypeCD.HTM:
                        fileFormat = FileTypeCD.HTML;
                        mimeType = "text/html";
                        extension = ".htm";
                        break;
                }
            }
            catch (Exception e)
            {
                fileFormat = FileTypeCD.PDF;
                extension = ".pdf";
                var objMethod = (MethodInfo) MethodBase.GetCurrentMethod();
                GlobalExceptionHandler.customExceptionHandler(e, objMethod, null, ErrorSeverityType.Normal);
            }
        }

        public static DateTime ConvertDateValueToDateTime(DateTime? value)
        {
            var dtime = default(DateTime);
            if (value.HasValue)
                DateTime.TryParse(value.ToString(), out dtime);
            return dtime;
        }

        public static string ConvertDateValueToDateTimeString(DateTime? value)
        {
            var sResponse = "";

            var dtime = default(DateTime);
            if (value.HasValue)
            {
                DateTime.TryParse(value.ToString(), out dtime);
                sResponse = dtime.ToString("d");
            }

            return sResponse;
        }

        public static string ConvertDateValueToDateTimeString(DateTime? value, string format)
        {
            var sResponse = "";

            var dtime = default(DateTime);
            if (value.HasValue)
            {
                DateTime.TryParse(value.ToString(), out dtime);
                sResponse = dtime.ToString(format);
            }

            return sResponse;
        }

        /// <summary>
        ///     This function converts a string separated date (dd mm yyyy) to date format
        /// </summary>
        public static string ConvertStringValueToDate(string sDate, string format, char separator)
        {
            var sResponse = string.Empty;
            if (IsEmpty(sDate))
                return sResponse;

            var dtArr = sDate.Split(separator);
            int day, month, year;

            if (dtArr.Length == 2)
            {
                if (int.TryParse(dtArr[0], out month) && int.TryParse(dtArr[1], out year))
                    if (month >= 1 && month <= 12 && year >= 1799)
                    {
                        // Prepare Date As Per CharValue, As we store Only Month/Year, HardCoded DatePart to 01
                        var dtime = new DateTime(year, month, 1);
                        sResponse = dtime.ToString(format);
                    }
            }
            else if (dtArr.Length == 3)
            {
                if (int.TryParse(dtArr[0], out day) && int.TryParse(dtArr[1], out month) &&
                    int.TryParse(dtArr[2], out year))
                    if (month >= 1 && month <= 12 && year >= 1799)
                        if (day >= 1 && day <= DateTime.DaysInMonth(year, month))
                        {
                            var dtime = new DateTime(year, month, day);
                            sResponse = dtime.ToString(format);
                        }
            }

            return sResponse;
        }

        public static string SanitizeString(string input)
        {
            if (input == null) return null;

            var buffer = new StringBuilder(input.Length);

            foreach (var c in input)
                if (IsLegalChar(c))
                    buffer.Append(c);

            return buffer.ToString();
        }

        public static bool IsLegalChar(int character)
        {
            if (character == 0x0E || character == 0x1E || character == 0x08 || character == 0x11 || character == 0x1B ||
                character == 0x18
                || character == 0x1A || character == 0x1D || character == 0x18 || character == 0x02 ||
                character == 0x0B || character == 0x04
                || character == 0x03 || character == 0x1F || character == 0x17 || character == 0x07 ||
                character == 0x01 || character == 0x1C
                || character == 0x06 || character == 0x0C || character == 0x14 || character == 0x19 ||
                character == 0x15 || character == 0x0F
                || character == 0x10 || character == 0x12 || character == 0x05 || character == 0x16 ||
                character == 0x13)
                return false;
            return true;
        }

        public string LoadFileContentFromURL(string sURL)
        {
            WebResponse result = null;
            var output = "";

            try
            {
                var req = WebRequest.Create(sURL);
                result = req.GetResponse();
                var ReceiveStream = result.GetResponseStream();
                var encode = Encoding.GetEncoding("utf-8");
                var sr = new StreamReader(ReceiveStream, encode);
                var read = new char[256];
                var count = sr.Read(read, 0, read.Length);
                while (count > 0)
                {
                    var str = new string(read, 0, count);
                    output += str;
                    count = sr.Read(read, 0, read.Length);
                }
            }
            catch (Exception)
            {
            }

            return output;
        }

        /// <summary>
        ///     Create url for a page
        /// </summary>
        /// <param name="pageName"></param>
        /// <param name="folderName"></param>
        /// <returns></returns>
        public string CreateRedirUrl(string pageName, string folderName)
        {
            var url = new StringBuilder(string.Empty);

            url.Append(GetDomainURLFromConfig());

            if (!string.IsNullOrEmpty(folderName))
            {
                url.Append(folderName);
                url.Append("/");
            }

            if (!string.IsNullOrEmpty(pageName)) url.Append(pageName);

            return url.ToString();
        }

        public string CreateRedirectUrl(params string[] urls)
        {
            return string.Concat(GetDomainURLFromConfig(), string.Join("/", urls));
        }

        public static int GetStableHash(string s)
        {
            uint hash = 0;
            // if you care this can be done much faster with unsafe 
            // using fixed char* reinterpreted as a byte*
            foreach (var b in Encoding.Unicode.GetBytes(s))
            {
                hash += b;
                hash += hash << 10;
                hash ^= hash >> 6;
            }

            // final avalanche
            hash += hash << 3;
            hash ^= hash >> 11;
            hash += hash << 15;
            // helpfully we only want positive integer < MUST_BE_LESS_THAN
            // so simple truncate cast is ok if not perfect
            return (int) hash;
        }


        public static int GetDaysInAYear(int year)
        {
            var days = 0;
            for (var i = 1; i <= 12; i++) days += DateTime.DaysInMonth(year, i);
            return days;
        }

        public static int GetMonths(DateTime startDate, DateTime endDate)
        {
            var months = endDate.Year * 12 + endDate.Month - (startDate.Year * 12 + startDate.Month);

            if (endDate.Day >= startDate.Day) months++;

            return months;
        }


        /// <summary>
        ///     Thie method return Fresh Desk key on the basis of Domain Code
        /// </summary>
        /// <param name="isRHDomain"></param>
        /// <returns></returns>
        public static string GetFreshDeskKey(string domainCD)
        {
            var sbFreshDeskKey = new StringBuilder(string.Empty);
            if (domainCD == "RH")
            {
                sbFreshDeskKey.Append(
                    "eyJ3aWRnZXRfc2l0ZV91cmwiOiJoaXJpbmdwcm9kdWN0cy5mcmVzaGRlc2suY29tIiwicHJvZHVjdF9pZCI6NjAwMDAwMTQ3NywibmFtZSI6IlJlc3VtZUhlbHAiLCJ3aWRnZXRfZXh0ZXJuYWxfaWQiOjYwMDAwMDE0NzcsIndpZGdldF9pZCI6IjcwOWQ2ZTgyLTQ3");
                sbFreshDeskKey.Append(
                    "OWMtNDJiNi05NGJkLTE3MjEzNjk4MWQ3ZCIsInNob3dfb25fcG9ydGFsIjpmYWxzZSwicG9ydGFsX2xvZ2luX3JlcXVpcmVkIjpmYWxzZSwiaWQiOjYwMDAwMzExMjgsIm1haW5fd2lkZ2V0IjpmYWxzZSwiZmNfaWQiOiI2YjlmZmE4ZGYwM2I4OTAzOThhYTY2MWQwO");
                sbFreshDeskKey.Append(
                    "WY2ZGViZCIsInNob3ciOjEsInJlcXVpcmVkIjoyLCJoZWxwZGVza25hbWUiOiJIaXJpbmdQcm9kdWN0cyIsIm5hbWVfbGFiZWwiOiJOYW1lIiwibWFpbF9sYWJlbCI6IkVtYWlsIiwibWVzc2FnZV9sYWJlbCI6Ik1lc3NhZ2UiLCJwaG9uZV9sYWJlbCI6IlBob25lIE");
                sbFreshDeskKey.Append(
                    "51bWJlciIsInRleHRmaWVsZF9sYWJlbCI6IlRleHRmaWVsZCIsImRyb3Bkb3duX2xhYmVsIjoiRHJvcGRvd24iLCJ3ZWJ1cmwiOiJoaXJpbmdwcm9kdWN0cy5mcmVzaGRlc2suY29tIiwibm9kZXVybCI6ImNoYXQuZnJlc2hkZXNrLmNvbSIsImRlYnVnIjoxLCJtZSI6");
                sbFreshDeskKey.Append(
                    "Ik1lIiwiZXhwaXJ5IjoxNDM3Njk2MzgyMDAwLCJlbnZpcm9ubWVudCI6InByb2R1Y3Rpb24iLCJkZWZhdWx0X3dpbmRvd19vZmZzZXQiOjMwLCJkZWZhdWx0X21heGltaXplZF90aXRsZSI6IkNoYXQgaW4gcHJvZ3Jlc3MiLCJkZWZhdWx0X21pbmltaXplZF90aXRsZ");
                sbFreshDeskKey.Append(
                    "SI6IkxldCdzIHRhbGshIiwiZGVmYXVsdF90ZXh0X3BsYWNlIjoiWW91ciBNZXNzYWdlIiwiZGVmYXVsdF9jb25uZWN0aW5nX21zZyI6IldhaXRpbmcgZm9yIGFuIGFnZW50IiwiZGVmYXVsdF93ZWxjb21lX21lc3NhZ2UiOiJIaSEgSG93IGNhbiB3ZSBoZWxwIHlvdS");
                sbFreshDeskKey.Append(
                    "B0b2RheT8iLCJkZWZhdWx0X3dhaXRfbWVzc2FnZSI6Ik9uZSBvZiB1cyB3aWxsIGJlIHdpdGggeW91IHJpZ2h0IGF3YXksIHBsZWFzZSB3YWl0LiIsImRlZmF1bHRfYWdlbnRfam9pbmVkX21zZyI6Int7YWdlbnRfbmFtZX19IGhhcyBqb2luZWQgdGhlIGNoYXQiLCJk");
                sbFreshDeskKey.Append(
                    "ZWZhdWx0X2FnZW50X2xlZnRfbXNnIjoie3thZ2VudF9uYW1lfX0gaGFzIGxlZnQgdGhlIGNoYXQiLCJkZWZhdWx0X2FnZW50X3RyYW5zZmVyX21zZ190b192aXNpdG9yIjoiWW91ciBjaGF0IGhhcyBiZWVuIHRyYW5zZmVycmVkIHRvIHt7YWdlbnRfbmFtZX19IiwiZ");
                sbFreshDeskKey.Append(
                    "GVmYXVsdF90aGFua19tZXNzYWdlIjoiVGhhbmsgeW91IGZvciBjaGF0dGluZyB3aXRoIHVzLiBJZiB5b3UgaGF2ZSBhZGRpdGlvbmFsIHF1ZXN0aW9ucywgZmVlbCBmcmVlIHRvIHBpbmcgdXMhIiwiZGVmYXVsdF9ub25fYXZhaWxhYmlsaXR5X21lc3NhZ2UiOiJPdX");
                sbFreshDeskKey.Append(
                    "IgYWdlbnRzIGFyZSB1bmF2YWlsYWJsZSByaWdodCBub3cuIFNvcnJ5IGFib3V0IHRoYXQsIGJ1dCBwbGVhc2UgbGVhdmUgdXMgYSBtZXNzYWdlIGFuZCB3ZSdsbCBnZXQgcmlnaHQgYmFjay4iLCJkZWZhdWx0X3ByZWNoYXRfbWVzc2FnZSI6IldlIGNhbid0IHdhaXQg");
                sbFreshDeskKey.Append(
                    "dG8gdGFsayB0byB5b3UuIEJ1dCBmaXJzdCwgcGxlYXNlIHRlbGwgdXMgYSBiaXQgYWJvdXQgeW91cnNlbGYuIiwiYWdlbnRfdHJhbnNmZXJlZF9tc2ciOiJZb3VyIGNoYXQgaGFzIGJlZW4gdHJhbnNmZXJyZWQgdG8ge3thZ2VudF9uYW1lfX0iLCJhZ2VudF9yZW9wZW");
                sbFreshDeskKey.Append(
                    "5fY2hhdF9tc2ciOiJ7e2FnZW50X25hbWV9fSByZW9wZW5lZCB0aGUgY2hhdCIsInZpc2l0b3Jfc2lkZV9pbmFjdGl2ZV9tc2ciOiJUaGlzIGNoYXQgaGFzIGJlZW4gaW5hY3RpdmUgZm9yIHRoZSBwYXN0IDIwIG1pbnV0ZXMuIiwiYWdlbnRfZGlzY29ubmVjdF9tc2c");
                sbFreshDeskKey.Append(
                    "iOiJ7e2FnZW50X25hbWV9fSBoYXMgYmVlbiBkaXNjb25uZWN0ZWQiLCJzaXRlX2lkIjoiNmI5ZmZhOGRmMDNiODkwMzk4YWE2NjFkMDlmNmRlYmQiLCJhY3RpdmUiOnRydWUsIndpZGdldF9wcmVmZXJlbmNlcyI6eyJ3aW5kb3dfY29sb3IiOiIjMzAzQjQwIiwid2lu");
                sbFreshDeskKey.Append(
                    "ZG93X3Bvc2l0aW9uIjoiQm90dG9tIFJpZ2h0Iiwid2luZG93X29mZnNldCI6IjMwIiwidGV4dF9wbGFjZSI6IllvdXIgTWVzc2FnZSIsImNvbm5lY3RpbmdfbXNnIjoiV2FpdGluZyBmb3IgYW4gYWdlbnQiLCJhZ2VudF9sZWZ0X21zZyI6Int7YWdlbnRfbmFtZX19IG");
                sbFreshDeskKey.Append(
                    "hhcyBsZWZ0IHRoZSBjaGF0IiwiYWdlbnRfam9pbmVkX21zZyI6Int7YWdlbnRfbmFtZX19IGhhcyBqb2luZWQgdGhlIGNoYXQiLCJtaW5pbWl6ZWRfdGl0bGUiOiJMZXQncyB0YWxrISIsIm1heGltaXplZF90aXRsZSI6IkNoYXQgaW4gcHJvZ3Jlc3MiLCJ3ZWxjb21l");
                sbFreshDeskKey.Append(
                    "X21lc3NhZ2UiOiJIaSEgSG93IGNhbiB3ZSBoZWxwIHlvdSB0b2RheT8iLCJ0aGFua19tZXNzYWdlIjoiVGhhbmsgeW91IGZvciBjaGF0dGluZyB3aXRoIHVzLiBJZiB5b3UgaGF2ZSBhZGRpdGlvbmFsIHF1ZXN0aW9ucywgZmVlbCBmcmVlIHRvIHBpbmcgdXMhIiwid");
                sbFreshDeskKey.Append(
                    "2FpdF9tZXNzYWdlIjoiT25lIG9mIHVzIHdpbGwgYmUgd2l0aCB5b3UgcmlnaHQgYXdheSwgcGxlYXNlIHdhaXQuIn0sInJvdXRpbmciOnsiZHJvcGRvd25fYmFzZWQiOiJmYWxzZSIsImNob2ljZXMiOnsiQWNjb3VudCBpc3N1ZXMiOlsiMCJdLCJCaWxsaW5nIHF1ZX");
                sbFreshDeskKey.Append(
                    "N0aW9ucyI6WyIwIl0sIkNhbmNlbCI6WyIwIl0sIkNoZWNrIGNhbmNlbC9yZWZ1bmQgc3RhdHVzIjpbIjAiXSwiR2VuZXJhbCBRdWVzdGlvbnMiOlsiMCJdLCJJIHdhbnQgdG8gcHVyY2hhc2Ugc29tZXRoaW5nIjpbIjAiXSwiTG9zdCBteSBwYXNzd29yZCI6WyIwIl0s");
                sbFreshDeskKey.Append(
                    "IlJlZnVuZCI6WyIwIl0sIlJlZnVuZCBhbmQgY2FuY2VsIHN1YnNjcmlwdGlvbiI6WyIwIl0sIlJlcG9ydCBhIGJ1ZyI6WyIwIl0sIlRlY2huaWNhbCBoZWxwL2lzc3VlIjpbIjAiXSwiZGVmYXVsdCI6WyI2MDAwMDk2ODU2Il19fSwicHJlY2hhdF9mb3JtIjp0cnVlLC");
                sbFreshDeskKey.Append(
                    "JwcmVjaGF0X21lc3NhZ2UiOiJXZSBjYW4ndCB3YWl0IHRvIHRhbGsgdG8geW91LiBCdXQgZmlyc3QsIHBsZWFzZSB0YWtlIGEgY291cGxlIG9mIG1vbWVudHMgdG8gdGVsbCB1cyBhIGJpdCBhYm91dCB5b3Vyc2VsZi4iLCJwcmVjaGF0X2ZpZWxkcyI6eyJuYW1lIjp");
                sbFreshDeskKey.Append(
                    "7InRpdGxlIjoiRnVsbCBOYW1lIiwic2hvdyI6IjIifSwiZW1haWwiOnsidGl0bGUiOiJFbWFpbCIsInNob3ciOiIyIn0sInBob25lIjp7InRpdGxlIjoiUGhvbmUgTnVtYmVyIiwic2hvdyI6IjEifSwidGV4dGZpZWxkIjp7InRpdGxlIjoiQWNjb3VudCBJRCIsInNo");
                sbFreshDeskKey.Append(
                    "b3ciOiIxIn0sImRyb3Bkb3duIjp7InRpdGxlIjoiUmVhc29uIGZvciBjaGF0Iiwic2hvdyI6IjIiLCJvcHRpb25zIjpbIkFjY291bnQgaXNzdWVzIiwiQmlsbGluZyBxdWVzdGlvbnMiLCJDYW5jZWwiLCJDaGVjayBjYW5jZWwvcmVmdW5kIHN0YXR1cyIsIkdlbmVyY");
                sbFreshDeskKey.Append(
                    "WwgUXVlc3Rpb25zIiwiSSB3YW50IHRvIHB1cmNoYXNlIHNvbWV0aGluZyIsIkxvc3QgbXkgcGFzc3dvcmQiLCJSZWZ1bmQiLCJSZWZ1bmQgYW5kIGNhbmNlbCBzdWJzY3JpcHRpb24iLCJSZXBvcnQgYSBidWciLCJUZWNobmljYWwgaGVscC9pc3N1ZSJdfX0sImJ1c2");
                sbFreshDeskKey.Append(
                    "luZXNzX2NhbGVuZGFyIjpudWxsLCJub25fYXZhaWxhYmlsaXR5X21lc3NhZ2UiOnsidGV4dCI6Ik91ciBhZ2VudHMgYXJlIHVuYXZhaWxhYmxlIHJpZ2h0IG5vdy4gU29ycnkgYWJvdXQgdGhhdCwgYnV0IHBsZWFzZSBsZWF2ZSB1cyBhIG1lc3NhZ2UgYW5kIHdlJ2x");
                sbFreshDeskKey.Append(
                    "sIGdldCByaWdodCBiYWNrLiIsImN1c3RvbUxpbmsiOiIwIiwiY3VzdG9tTGlua1VybCI6IiJ9LCJwcm9hY3RpdmVfY2hhdCI6ZmFsc2UsInByb2FjdGl2ZV90aW1lIjoxNSwic2l0ZV91cmwiOiJoaXJpbmdwcm9kdWN0cy5mcmVzaGRlc2suY29tIiwiZXh0ZXJuYWxf");
                sbFreshDeskKey.Append(
                    "aWQiOjYwMDAwMDE0NzcsImRlbGV0ZWQiOmZhbHNlLCJvZmZsaW5lX2NoYXQiOnsic2hvdyI6IjAiLCJmb3JtIjp7Im5hbWUiOiJOYW1lIiwiZW1haWwiOiJFbWFpbCIsIm1lc3NhZ2UiOiJNZXNzYWdlIn0sIm1lc3NhZ2VzIjp7InRpdGxlIjoiTGVhdmUgdXMgYSBtZ");
                sbFreshDeskKey.Append(
                    "XNzYWdlISIsInRoYW5rIjoiVGhhbmsgeW91IGZvciB3cml0aW5nIHRvIHVzLiBXZSB3aWxsIGdldCBiYWNrIHRvIHlvdSBzaG9ydGx5LiIsInRoYW5rX2hlYWRlciI6IlRoYW5rIHlvdSEifX0sIm1vYmlsZSI6dHJ1ZSwiY3JlYXRlZF9hdCI6IjIwMTUtMDYtMTlUMj");
                sbFreshDeskKey.Append("A6MDA6MzEuMDAwWiIsInVwZGF0ZWRfYXQiOiIyMDE1LTA2LTE5VDIxOjQ3OjAxLjAwMFoifQ==");
            }
            else if (domainCD == "RHUK")
            {
                sbFreshDeskKey.Append(
                    @"eyJ3aWRnZXRfc2l0ZV91cmwiOiJoaXJpbmdwcm9kdWN0cy5mcmVzaGRlc2suY29tIiwicHJvZHVjdF9pZCI6MjQwMDAwMDAwNTIsIm5hbWUiOiJSZXN1bWVIZWxwIFVLIiwid2lkZ2V0X2V4dGVybmFsX2lkIjoyNDAwMDAwMDA1Miwid2lkZ2V0X2lkIjoiNjk3OTJmZWMtNzhhMS00OTEwLTlhZGEtODkzNjNmYTQyODY2Iiwic2hvd19vbl9wb3J0YWwiOmZhbHNlLCJwb3J0YWxfbG9naW5fcmVxdWlyZWQiOmZhbHNlLCJsYW5ndWFnZSI6ImVuIiwidGltZXpvbmUiOiJDZW50cmFsIFRpbWUgKFVTICYgQ2FuYWRhKSIsImlkIjoyNDAwMDAwMDA1MiwibWFpbl93aWRnZXQiOjAsImZjX2lkIjoiNmI5ZmZhOGRmMDNiODkwMzk4YWE2NjFkMDlmNmRlYmQiLCJzaG93IjoxLCJyZXF1aXJlZCI6MiwiaGVscGRlc2tuYW1lIjoiSGlyaW5nUHJvZHVjdHMiLCJuYW1lX2xhYmVsIjoiTmFtZSIsIm1lc3NhZ2VfbGFiZWwiOiJNZXNzYWdlIiwicGhvbmVfbGFiZWwiOiJQaG9uZSIsInRleHRmaWVsZF9sYWJlbCI6IlRleHRmaWVsZCIsImRyb3Bkb3duX2xhYmVsIjoiRHJvcGRvd24iLCJ3ZWJ1cmwiOiJoaXJpbmdwcm9kdWN0cy5mcmVzaGRlc2suY29tIiwibm9kZXVybCI6ImNoYXQuZnJlc2hkZXNrLmNvbSIsImRlYnVnIjoxLCJtZSI6Ik1lIiwiZXhwaXJ5IjowLCJlbnZpcm9ubWVudCI6InByb2R1Y3Rpb24iLCJlbmRfY2hhdF90aGFua19tc2ciOiJUaGFuayB5b3UhISEiLCJlbmRfY2hhdF9lbmRfdGl0bGUiOiJFbmQiLCJlbmRfY2hhdF9jYW5jZWxfdGl0bGUiOiJDYW5jZWwiLCJzaXRlX2lkIjoiNmI5ZmZhOGRmMDNiODkwMzk4YWE2NjFkMDlmNmRlYmQiLCJhY3RpdmUiOjEsInJvdXRpbmciOnsiY2hvaWNlcyI6eyJBY2NvdW50IHByb2JsZW1zIjpbIjAiXSwiQmlsbGluZyBwcm9ibGVtcyI6WyIwIl0sIkdlbmVyYWwgcXVlc3Rpb25zIjpbIjAiXSwiSSB3b3VsZCBsaWtlIHRvIGJ1eSI6WyIwIl0sIkZvcmdvdHRlbiBteSBwYXNzd29yZCI6WyIwIl0sIlJlcG9ydCBhIGJ1ZyI6WyIwIl0sIlRlY2huaWNhbCBoZWxwL3Byb2JsZW0iOlsiMCJdLCJkZWZhdWx0IjpbIjAiXX0sImRyb3Bkb3duX2Jhc2VkIjoiZmFsc2UifSwicHJlY2hhdF9mb3JtIjoxLCJidXNpbmVzc19jYWxlbmRhciI6bnVsbCwicHJvYWN0aXZlX2NoYXQiOjAsInByb2FjdGl2ZV90aW1lIjpudWxsLCJzaXRlX3VybCI6ImhpcmluZ3Byb2R1Y3RzLmZyZXNoZGVzay5jb20iLCJleHRlcm5hbF9pZCI6MjQwMDAwMDAwNTIsImRlbGV0ZWQiOjAsIm1vYmlsZSI6MSwiYWNjb3VudF9pZCI6bnVsbCwiY3JlYXRlZF9hdCI6IjIwMTctMDMtMTZUMTk6NTU6MzAuMDAwWiIsInVwZGF0ZWRfYXQiOiIyMDE3LTAzLTIyVDE1OjQ0OjA3LjAwMFoiLCJjYkRlZmF1bHRNZXNzYWdlcyI6eyJjb2Jyb3dzaW5nX3N0YXJ0X21zZyI6IllvdXIgc2NyZWVuc2hhcmUgc2Vzc2lvbiBoYXMgc3RhcnRlZCIsImNvYnJvd3Npbmdfc3RvcF9tc2ciOiJZb3VyIHNjcmVlbnNoYXJpbmcgc2Vzc2lvbiBoYXMgZW5kZWQiLCJjb2Jyb3dzaW5nX2RlbnlfbXNnIjoiWW91ciByZXF1ZXN0IHdhcyBkZWNsaW5lZCIsImNvYnJvd3NpbmdfYWdlbnRfYnVzeSI6IkFnZW50IGlzIGluIHNjcmVlbiBzaGFyZSBzZXNzaW9uIHdpdGggY3VzdG9tZXIiLCJjb2Jyb3dzaW5nX3ZpZXdpbmdfc2NyZWVuIjoiWW91IGFyZSB2aWV3aW5nIHRoZSB2aXNpdG9y4oCZcyBzY3JlZW4iLCJjb2Jyb3dzaW5nX2NvbnRyb2xsaW5nX3NjcmVlbiI6IllvdSBoYXZlIGFjY2VzcyB0byB2aXNpdG9y4oCZcyBzY3JlZW4uIiwiY29icm93c2luZ19yZXF1ZXN0X2NvbnRyb2wiOiJSZXF1ZXN0IHZpc2l0b3IgZm9yIHNjcmVlbiBhY2Nlc3MgIiwiY29icm93c2luZ19naXZlX3Zpc2l0b3JfY29udHJvbCI6IkdpdmUgYWNjZXNzIGJhY2sgdG8gdmlzaXRvciAiLCJjb2Jyb3dzaW5nX3N0b3BfcmVxdWVzdCI6IkVuZCB5b3VyIHNjcmVlbnNoYXJpbmcgc2Vzc2lvbiAiLCJjb2Jyb3dzaW5nX3JlcXVlc3RfY29udHJvbF9yZWplY3RlZCI6IllvdXIgcmVxdWVzdCB3YXMgZGVjbGluZWQgIiwiY29icm93c2luZ19jYW5jZWxfdmlzaXRvcl9tc2ciOiJTY3JlZW5zaGFyaW5nIGlzIGN1cnJlbnRseSB1bmF2YWlsYWJsZSAiLCJjb2Jyb3dzaW5nX2FnZW50X3JlcXVlc3RfY29udHJvbCI6IkFnZW50IGlzIHJlcXVlc3RpbmcgYWNjZXNzIHRvIHlvdXIgc2NyZWVuICIsImNiX3ZpZXdpbmdfc2NyZWVuX3ZpIjoiQWdlbnQgY2FuIHZpZXcgeW91ciBzY3JlZW4gIiwiY2JfY29udHJvbGxpbmdfc2NyZWVuX3ZpIjoiQWdlbnQgaGFzIGFjY2VzcyB0byB5b3VyIHNjcmVlbiAiLCJjYl92aWV3X21vZGVfc3VidGV4dCI6IllvdXIgYWNjZXNzIHRvIHRoZSBzY3JlZW4gaGFzIGJlZW4gd2l0aGRyYXduICIsImNiX2dpdmVfY29udHJvbF92aSI6IkFsbG93IGFnZW50IHRvIGFjY2VzcyB5b3VyIHNjcmVlbiAiLCJjYl92aXNpdG9yX3Nlc3Npb25fcmVxdWVzdCI6IkFnZW50IHNlZWtzIGFjY2VzcyB0byB5b3VyIHNjcmVlbiAifX0=");
            }
            else
            {
                sbFreshDeskKey.Append(
                    "eyJ3aWRnZXRfc2l0ZV91cmwiOiJoaXJpbmdwcm9kdWN0cy5mcmVzaGRlc2suY29tIiwicHJvZHVjdF9pZCI6NjAwMDAwMTQ3OCwibmFtZSI6IlJlc3VtZUdpZyIsIndpZGdldF9leHRlcm5hbF9pZCI6NjAwMDAwMTQ3OCwid2lkZ2V0X2lkIjoiMDgwZTU4ZjYtZmQx");
                sbFreshDeskKey.Append(
                    "My00ZWEyLWEzNTItMmIxNGIwMjY5MDQxIiwic2hvd19vbl9wb3J0YWwiOmZhbHNlLCJwb3J0YWxfbG9naW5fcmVxdWlyZWQiOmZhbHNlLCJpZCI6NjAwMDAzMTEyOSwibWFpbl93aWRnZXQiOmZhbHNlLCJmY19pZCI6IjZiOWZmYThkZjAzYjg5MDM5OGFhNjYxZDA5Zj");
                sbFreshDeskKey.Append(
                    "ZkZWJkIiwic2hvdyI6MSwicmVxdWlyZWQiOjIsImhlbHBkZXNrbmFtZSI6IkhpcmluZ1Byb2R1Y3RzIiwibmFtZV9sYWJlbCI6Ik5hbWUiLCJtYWlsX2xhYmVsIjoiRW1haWwiLCJtZXNzYWdlX2xhYmVsIjoiTWVzc2FnZSIsInBob25lX2xhYmVsIjoiUGhvbmUgTnVt");
                sbFreshDeskKey.Append(
                    "YmVyIiwidGV4dGZpZWxkX2xhYmVsIjoiVGV4dGZpZWxkIiwiZHJvcGRvd25fbGFiZWwiOiJEcm9wZG93biIsIndlYnVybCI6ImhpcmluZ3Byb2R1Y3RzLmZyZXNoZGVzay5jb20iLCJub2RldXJsIjoiY2hhdC5mcmVzaGRlc2suY29tIiwiZGVidWciOjEsIm1lIjoiTW");
                sbFreshDeskKey.Append(
                    "UiLCJleHBpcnkiOjE0Mzc2OTYzODIwMDAsImVudmlyb25tZW50IjoicHJvZHVjdGlvbiIsImRlZmF1bHRfd2luZG93X29mZnNldCI6MzAsImRlZmF1bHRfbWF4aW1pemVkX3RpdGxlIjoiQ2hhdCBpbiBwcm9ncmVzcyIsImRlZmF1bHRfbWluaW1pemVkX3RpdGxlIjoi");
                sbFreshDeskKey.Append(
                    "TGV0J3MgdGFsayEiLCJkZWZhdWx0X3RleHRfcGxhY2UiOiJZb3VyIE1lc3NhZ2UiLCJkZWZhdWx0X2Nvbm5lY3RpbmdfbXNnIjoiV2FpdGluZyBmb3IgYW4gYWdlbnQiLCJkZWZhdWx0X3dlbGNvbWVfbWVzc2FnZSI6IkhpISBIb3cgY2FuIHdlIGhlbHAgeW91IHRvZG");
                sbFreshDeskKey.Append(
                    "F5PyIsImRlZmF1bHRfd2FpdF9tZXNzYWdlIjoiT25lIG9mIHVzIHdpbGwgYmUgd2l0aCB5b3UgcmlnaHQgYXdheSwgcGxlYXNlIHdhaXQuIiwiZGVmYXVsdF9hZ2VudF9qb2luZWRfbXNnIjoie3thZ2VudF9uYW1lfX0gaGFzIGpvaW5lZCB0aGUgY2hhdCIsImRlZmF1b");
                sbFreshDeskKey.Append(
                    "HRfYWdlbnRfbGVmdF9tc2ciOiJ7e2FnZW50X25hbWV9fSBoYXMgbGVmdCB0aGUgY2hhdCIsImRlZmF1bHRfYWdlbnRfdHJhbnNmZXJfbXNnX3RvX3Zpc2l0b3IiOiJZb3VyIGNoYXQgaGFzIGJlZW4gdHJhbnNmZXJyZWQgdG8ge3thZ2VudF9uYW1lfX0iLCJkZWZhdWx");
                sbFreshDeskKey.Append(
                    "0X3RoYW5rX21lc3NhZ2UiOiJUaGFuayB5b3UgZm9yIGNoYXR0aW5nIHdpdGggdXMuIElmIHlvdSBoYXZlIGFkZGl0aW9uYWwgcXVlc3Rpb25zLCBmZWVsIGZyZWUgdG8gcGluZyB1cyEiLCJkZWZhdWx0X25vbl9hdmFpbGFiaWxpdHlfbWVzc2FnZSI6Ik91ciBhZ2Vud");
                sbFreshDeskKey.Append(
                    "HMgYXJlIHVuYXZhaWxhYmxlIHJpZ2h0IG5vdy4gU29ycnkgYWJvdXQgdGhhdCwgYnV0IHBsZWFzZSBsZWF2ZSB1cyBhIG1lc3NhZ2UgYW5kIHdlJ2xsIGdldCByaWdodCBiYWNrLiIsImRlZmF1bHRfcHJlY2hhdF9tZXNzYWdlIjoiV2UgY2FuJ3Qgd2FpdCB0byB0YWx");
                sbFreshDeskKey.Append(
                    "rIHRvIHlvdS4gQnV0IGZpcnN0LCBwbGVhc2UgdGVsbCB1cyBhIGJpdCBhYm91dCB5b3Vyc2VsZi4iLCJhZ2VudF90cmFuc2ZlcmVkX21zZyI6IllvdXIgY2hhdCBoYXMgYmVlbiB0cmFuc2ZlcnJlZCB0byB7e2FnZW50X25hbWV9fSIsImFnZW50X3Jlb3Blbl9jaGF0X");
                sbFreshDeskKey.Append(
                    "21zZyI6Int7YWdlbnRfbmFtZX19IHJlb3BlbmVkIHRoZSBjaGF0IiwidmlzaXRvcl9zaWRlX2luYWN0aXZlX21zZyI6IlRoaXMgY2hhdCBoYXMgYmVlbiBpbmFjdGl2ZSBmb3IgdGhlIHBhc3QgMjAgbWludXRlcy4iLCJhZ2VudF9kaXNjb25uZWN0X21zZyI6Int7YWd");
                sbFreshDeskKey.Append(
                    "lbnRfbmFtZX19IGhhcyBiZWVuIGRpc2Nvbm5lY3RlZCIsInNpdGVfaWQiOiI2YjlmZmE4ZGYwM2I4OTAzOThhYTY2MWQwOWY2ZGViZCIsImFjdGl2ZSI6dHJ1ZSwid2lkZ2V0X3ByZWZlcmVuY2VzIjp7IndpbmRvd19jb2xvciI6IiMzMDNCNDAiLCJ3aW5kb3dfcG9za");
                sbFreshDeskKey.Append(
                    "XRpb24iOiJCb3R0b20gUmlnaHQiLCJ3aW5kb3dfb2Zmc2V0IjoiMzAiLCJ0ZXh0X3BsYWNlIjoiWW91ciBNZXNzYWdlIiwiY29ubmVjdGluZ19tc2ciOiJXYWl0aW5nIGZvciBhbiBhZ2VudCIsImFnZW50X2xlZnRfbXNnIjoie3thZ2VudF9uYW1lfX0gaGFzIGxlZnQ");
                sbFreshDeskKey.Append(
                    "gdGhlIGNoYXQiLCJhZ2VudF9qb2luZWRfbXNnIjoie3thZ2VudF9uYW1lfX0gaGFzIGpvaW5lZCB0aGUgY2hhdCIsIm1pbmltaXplZF90aXRsZSI6IkxldCdzIHRhbGshIiwibWF4aW1pemVkX3RpdGxlIjoiQ2hhdCBpbiBwcm9ncmVzcyIsIndlbGNvbWVfbWVzc2FnZ");
                sbFreshDeskKey.Append(
                    "SI6IkhpISBIb3cgY2FuIHdlIGhlbHAgeW91IHRvZGF5PyIsInRoYW5rX21lc3NhZ2UiOiJUaGFuayB5b3UgZm9yIGNoYXR0aW5nIHdpdGggdXMuIElmIHlvdSBoYXZlIGFkZGl0aW9uYWwgcXVlc3Rpb25zLCBmZWVsIGZyZWUgdG8gcGluZyB1cyEiLCJ3YWl0X21lc3N");
                sbFreshDeskKey.Append(
                    "hZ2UiOiJPbmUgb2YgdXMgd2lsbCBiZSB3aXRoIHlvdSByaWdodCBhd2F5LCBwbGVhc2Ugd2FpdC4ifSwicm91dGluZyI6eyJjaG9pY2VzIjp7IkFjY291bnQgaXNzdWVzIjpbIjAiXSwiQmlsbGluZyBxdWVzdGlvbnMiOlsiMCJdLCJDYW5jZWwiOlsiMCJdLCJDaGVja");
                sbFreshDeskKey.Append(
                    "yBjYW5jZWwvcmVmdW5kIHN0YXR1cyI6WyIwIl0sIkdlbmVyYWwgUXVlc3Rpb25zIjpbIjAiXSwiSSB3YW50IHRvIHB1cmNoYXNlIHNvbWV0aGluZyI6WyIwIl0sIkxvc3QgbXkgcGFzc3dvcmQiOlsiMCJdLCJSZWZ1bmQiOlsiMCJdLCJSZWZ1bmQgYW5kIGNhbmNlbCB");
                sbFreshDeskKey.Append(
                    "zdWJzY3JpcHRpb24iOlsiMCJdLCJSZXBvcnQgYSBidWciOlsiMCJdLCJUZWNobmljYWwgaGVscC9pc3N1ZSI6WyIwIl0sImRlZmF1bHQiOlsiNjAwMDA5Njg1NiJdfSwiZHJvcGRvd25fYmFzZWQiOiJmYWxzZSJ9LCJwcmVjaGF0X2Zvcm0iOnRydWUsInByZWNoYXRfb");
                sbFreshDeskKey.Append(
                    "WVzc2FnZSI6IldlIGNhbid0IHdhaXQgdG8gdGFsayB0byB5b3UuIEJ1dCBmaXJzdCwgcGxlYXNlIHRha2UgYSBjb3VwbGUgb2YgbW9tZW50cyB0byB0ZWxsIHVzIGEgYml0IGFib3V0IHlvdXJzZWxmLiIsInByZWNoYXRfZmllbGRzIjp7Im5hbWUiOnsidGl0bGUiOiJ");
                sbFreshDeskKey.Append(
                    "GdWxsIE5hbWUiLCJzaG93IjoiMiJ9LCJlbWFpbCI6eyJ0aXRsZSI6IkVtYWlsIiwic2hvdyI6IjIifSwicGhvbmUiOnsidGl0bGUiOiJQaG9uZSBOdW1iZXIiLCJzaG93IjoiMSJ9LCJ0ZXh0ZmllbGQiOnsidGl0bGUiOiJBY2NvdW50IElEIiwic2hvdyI6IjEifSwiZ");
                sbFreshDeskKey.Append(
                    "HJvcGRvd24iOnsidGl0bGUiOiJSZWFzb24gZm9yIGNoYXQiLCJzaG93IjoiMiIsIm9wdGlvbnMiOlsiQWNjb3VudCBpc3N1ZXMiLCJCaWxsaW5nIHF1ZXN0aW9ucyIsIkNhbmNlbCIsIkNoZWNrIGNhbmNlbC9yZWZ1bmQgc3RhdHVzIiwiR2VuZXJhbCBRdWVzdGlvbnM");
                sbFreshDeskKey.Append(
                    "iLCJJIHdhbnQgdG8gcHVyY2hhc2Ugc29tZXRoaW5nIiwiTG9zdCBteSBwYXNzd29yZCIsIlJlZnVuZCIsIlJlZnVuZCBhbmQgY2FuY2VsIHN1YnNjcmlwdGlvbiIsIlJlcG9ydCBhIGJ1ZyIsIlRlY2huaWNhbCBoZWxwL2lzc3VlIl19fSwiYnVzaW5lc3NfY2FsZW5kY");
                sbFreshDeskKey.Append(
                    "XIiOm51bGwsIm5vbl9hdmFpbGFiaWxpdHlfbWVzc2FnZSI6eyJ0ZXh0IjoiT3VyIGFnZW50cyBhcmUgdW5hdmFpbGFibGUgcmlnaHQgbm93LiBTb3JyeSBhYm91dCB0aGF0LCBidXQgcGxlYXNlIGxlYXZlIHVzIGEgbWVzc2FnZSBhbmQgd2UnbGwgZ2V0IHJpZ2h0IGJ");
                sbFreshDeskKey.Append(
                    "hY2suIiwiY3VzdG9tTGluayI6IjAiLCJjdXN0b21MaW5rVXJsIjoiIn0sInByb2FjdGl2ZV9jaGF0IjpmYWxzZSwicHJvYWN0aXZlX3RpbWUiOjE1LCJzaXRlX3VybCI6ImhpcmluZ3Byb2R1Y3RzLmZyZXNoZGVzay5jb20iLCJleHRlcm5hbF9pZCI6NjAwMDAwMTQ3O");
                sbFreshDeskKey.Append(
                    "CwiZGVsZXRlZCI6ZmFsc2UsIm9mZmxpbmVfY2hhdCI6eyJzaG93IjoiMCIsImZvcm0iOnsibmFtZSI6Ik5hbWUiLCJlbWFpbCI6IkVtYWlsIiwibWVzc2FnZSI6Ik1lc3NhZ2UifSwibWVzc2FnZXMiOnsidGl0bGUiOiJMZWF2ZSB1cyBhIG1lc3NhZ2UhIiwidGhhbms");
                sbFreshDeskKey.Append(
                    "iOiJUaGFuayB5b3UgZm9yIHdyaXRpbmcgdG8gdXMuIFdlIHdpbGwgZ2V0IGJhY2sgdG8geW91IHNob3J0bHkuIiwidGhhbmtfaGVhZGVyIjoiVGhhbmsgeW91ISJ9fSwibW9iaWxlIjp0cnVlLCJjcmVhdGVkX2F0IjoiMjAxNS0wNi0xOVQyMDowMDozMS4wMDBaIiwid");
                sbFreshDeskKey.Append("XBkYXRlZF9hdCI6IjIwMTUtMDYtMTlUMjE6NDY6NTAuMDAwWiJ9");
            }

            return sbFreshDeskKey.ToString();
        }

        public static List<string> populateCanada()
        {
            if (lstCanada == null)
            {
                lstCanada = new List<string>();
                lstCanada.Add("AB");
                lstCanada.Add("BC");
                lstCanada.Add("MB");
                lstCanada.Add("NB");
                lstCanada.Add("NL");
                lstCanada.Add("NS");
                lstCanada.Add("NT");
                lstCanada.Add("NU");
                lstCanada.Add("ON");
                lstCanada.Add("PE");
                lstCanada.Add("QC");
                lstCanada.Add("SK");
                lstCanada.Add("YT");
            }

            return lstCanada;
        }

        public static string GetOutgoingEmailAddress(string domainCD)
        {
            var outgoingEmail = string.Empty;
            switch (domainCD)
            {
                case DomainFriendlyNameConstants.FreeResume:
                    outgoingEmail = ConfigManager.GetConfig(MailerConstants.RESUMEGIG_OUTGOING_EMAIL);
                    break;
                default:
                    outgoingEmail = ConfigManager.GetConfig(MailerConstants.RESUMEHELP_OUTGOING_EMAIL);
                    break;
            }

            return outgoingEmail;
        }

        public static string GetCustomerServiceEmail(string domainCD)
        {
            var customerserviceEmail = string.Empty;
            switch (domainCD)
            {
                case DomainFriendlyNameConstants.FreeResume:
                    customerserviceEmail = ConfigManager.GetConfig(MailerConstants.RESUMEGIG_CUSTOMERSERVICE_EMAIL);
                    break;
                default:
                    customerserviceEmail = ConfigManager.GetConfig(MailerConstants.RESUMEHELP_CUSTOMERSERVICE_EMAIL);
                    break;
            }

            return customerserviceEmail;
        }

        public static bool AnySessionABTestOn()
        {
            bool sessionABTestOn;
            var value = ConfigManager.GetConfig(AppSettings.SESSION_ABTEST_ON);
            bool.TryParse(value, out sessionABTestOn);
            return sessionABTestOn;
        }

        #region Cookie Read/Write

        public static string ReadCookie(string cookieName)
        {
            var value = string.Empty;
            if (HttpContext.Current.Request.Cookies[cookieName] != null)
                value = HttpContext.Current.Request.Cookies[cookieName].Value;
            return value;
        }

        public static void WriteCookie(string cookieName, string value, bool isPermanent)
        {
            var cookie = HttpContext.Current.Request.Cookies[cookieName];
            if (cookie == null)
                cookie = new HttpCookie(cookieName);

            cookie.Value = value;
            cookie.Secure = true;
            cookie.HttpOnly = true;
            if (isPermanent)
                cookie.Expires = DateTime.Now.AddYears(5);

            if (HttpContext.Current.Request.Cookies[cookieName] != null)
                HttpContext.Current.Response.Cookies.Set(cookie);
            else
                HttpContext.Current.Response.Cookies.Add(cookie);
        }

        public static void DeleteCookie(string cookieName)
        {
            var cookie = HttpContext.Current.Request.Cookies[cookieName];
            if (cookie == null)
                cookie = new HttpCookie(cookieName);
            cookie.Expires = DateTime.Now.AddMonths(-1);
            cookie.Value = string.Empty;
            HttpContext.Current.Response.Cookies.Set(cookie);
        }

        #endregion
    }
}