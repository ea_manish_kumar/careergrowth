﻿namespace Utilities.ImageExport
{
    /// <summary>
    ///     Image dimentions struct
    /// </summary>
    public struct ImageDimensions
    {
        /// <summary>
        ///     Image height
        /// </summary>
        public int Height;

        /// <summary>
        ///     Image width
        /// </summary>
        public int Width;

        /// <summary>
        ///     Class constructor
        /// </summary>
        /// <param name="height">The height.</param>
        /// <param name="width">The width.</param>
        public ImageDimensions(int height, int width)
        {
            Height = height;
            Width = width;
        }
    }
}