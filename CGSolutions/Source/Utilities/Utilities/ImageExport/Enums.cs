﻿namespace Utilities.ImageExport
{
    /// <summary>
    ///     Image File Types
    /// </summary>
    public enum ImageFileTypes
    {
        /// <summary>
        ///     JPG image.
        /// </summary>
        JPG = 0,

        /// <summary>
        ///     PNG image.
        /// </summary>
        PNG = 1,

        /// <summary>
        ///     GIF image.
        /// </summary>
        GIF = 2,

        /// <summary>
        ///     TIFF image.
        /// </summary>
        TIFF = 3,

        /// <summary>
        ///     BMP image.
        /// </summary>
        BMP = 4
    }

    /// <summary>
    ///     Image quality
    /// </summary>
    public enum GraphicsQuality
    {
        /// <summary>
        ///     Highest graphics quality
        /// </summary>
        Highest = 0,

        /// <summary>
        ///     High graphics quality
        /// </summary>
        High = 1,

        /// <summary>
        ///     Medium graphics quality
        /// </summary>
        Medium = 2,

        /// <summary>
        ///     Low graphics quality
        /// </summary>
        Low = 3,

        /// <summary>
        ///     Default graphics quality
        /// </summary>
        Default = 4
    }
}