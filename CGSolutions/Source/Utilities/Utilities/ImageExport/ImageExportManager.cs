﻿using System;
using System.Collections;
using System.Drawing;
using System.Drawing.Drawing2D;
using System.Drawing.Imaging;
using System.IO;
using System.Reflection;
using CommonModules.Exceptions;

namespace Utilities.ImageExport
{
    public class ImageExportManager
    {
        public Stream ExportImage(Image originalImage, GraphicsQuality graphicsQuality, ImageFileTypes exportFileType,
            long encodingQuality, ImageDimensions dimensions)
        {
            var newimage = default(Image);
            Stream outputstream = new MemoryStream();
            try
            {
                if (dimensions.Height < 0) dimensions.Height = originalImage.Height;
                if (dimensions.Width < 0) dimensions.Width = originalImage.Width;
                if (encodingQuality < 0) encodingQuality = 0;

                newimage = new Bitmap(dimensions.Width, dimensions.Height);
                var graphics = default(Graphics);
                graphics = Graphics.FromImage(newimage);
                SetGraphicsQuality(graphics, graphicsQuality);

                //New image
                graphics.DrawImage(originalImage, 0, 0, dimensions.Width, dimensions.Height);

                //Get the correct codec
                var imagecodec = GetCodecEncoder(exportFileType);

                var encodeParams = new EncoderParameters(1);

                var qualityencoder = Encoder.Quality;
                //System.Drawing.Imaging.Encoder colordepthencoder = System.Drawing.Imaging.Encoder.ColorDepth;

                var qualityParam = new EncoderParameter(qualityencoder, encodingQuality);
                //EncoderParameter colordepthParam = new EncoderParameter(colordepthencoder, 24L);// Save the image with a color depth of 24 bits per pixel.

                encodeParams.Param[0] = qualityParam;
                //encodeParams.Param[1] = colordepthParam;
                //newimage.Save(@"C:\Temp\" + DateTime.Now.Ticks.ToString() + ".jpg",ImageFormat.Jpeg);
                newimage.Save(outputstream, imagecodec, encodeParams);
                //Image outputimage = Image.FromStream(outputstream);
                //outputstream = null;	
            }
            catch (Exception ex)
            {
                var htParams = new Hashtable(5);
                htParams.Add("originalImage", originalImage);
                htParams.Add("graphicsQuality", graphicsQuality);
                htParams.Add("exportFileType", exportFileType);
                htParams.Add("encodingQuality", encodingQuality);
                htParams.Add("dimensions", dimensions);
                var objMethod = (MethodInfo) MethodBase.GetCurrentMethod();
                GlobalExceptionHandler.customExceptionHandler(ex, objMethod, htParams, ErrorSeverityType.Normal);
            }

            return outputstream;
        }

        /// <summary>
        ///     Sets the desired quality params depending upon the qulaity desired
        /// </summary>
        /// <param name="graphics">Settings will be applied to this Grpahics object</param>
        /// <param name="graphicsQuality">The required graphics qulaity</param>
        private void SetGraphicsQuality(Graphics graphics, GraphicsQuality graphicsQuality)
        {
            switch (graphicsQuality)
            {
                case GraphicsQuality.Highest:
                    graphics.InterpolationMode = InterpolationMode.HighQualityBicubic;
                    graphics.SmoothingMode = SmoothingMode.HighQuality;
                    graphics.PixelOffsetMode = PixelOffsetMode.HighQuality;
                    graphics.CompositingQuality = CompositingQuality.HighQuality;
                    break;
                case GraphicsQuality.High:
                    graphics.InterpolationMode = InterpolationMode.High;
                    graphics.SmoothingMode = SmoothingMode.HighQuality;
                    graphics.PixelOffsetMode = PixelOffsetMode.HighQuality;
                    graphics.CompositingQuality = CompositingQuality.HighQuality;
                    break;
                case GraphicsQuality.Medium:
                    graphics.InterpolationMode = InterpolationMode.NearestNeighbor;
                    graphics.SmoothingMode = SmoothingMode.HighQuality;
                    graphics.PixelOffsetMode = PixelOffsetMode.Half;
                    graphics.CompositingQuality = CompositingQuality.GammaCorrected;
                    break;
                case GraphicsQuality.Low:
                    graphics.InterpolationMode = InterpolationMode.Low;
                    graphics.SmoothingMode = SmoothingMode.HighSpeed;
                    graphics.PixelOffsetMode = PixelOffsetMode.HighSpeed;
                    graphics.CompositingQuality = CompositingQuality.HighSpeed;
                    break;
                case GraphicsQuality.Default:
                    graphics.InterpolationMode = InterpolationMode.Default;
                    graphics.SmoothingMode = SmoothingMode.Default;
                    graphics.PixelOffsetMode = PixelOffsetMode.Default;
                    graphics.CompositingQuality = CompositingQuality.Default;
                    break;
                default:
                    graphics.InterpolationMode = InterpolationMode.Default;
                    graphics.SmoothingMode = SmoothingMode.Default;
                    graphics.PixelOffsetMode = PixelOffsetMode.Default;
                    graphics.CompositingQuality = CompositingQuality.Default;
                    break;
            }
        }

        /// <summary>
        ///     Locates the right encoder
        /// </summary>
        /// <param name="exportFileType"></param>
        /// <returns></returns>
        private ImageCodecInfo GetCodecEncoder(ImageFileTypes exportFileType)
        {
            ImageCodecInfo retcodec = null;
            var mimetype = "image/jpeg";


            switch (exportFileType)
            {
                case ImageFileTypes.JPG:
                    mimetype = "image/jpeg";
                    break;
                case ImageFileTypes.PNG:
                    mimetype = "image/png";
                    break;
                case ImageFileTypes.GIF:
                    mimetype = "image/gif";
                    break;
                case ImageFileTypes.TIFF:
                    mimetype = "image/tiff";
                    break;
                case ImageFileTypes.BMP:
                    mimetype = "image/bmp";
                    break;
                default:
                    mimetype = "image/jpeg";
                    break;
            }

            try
            {
                var codecs = ImageCodecInfo.GetImageEncoders();

                foreach (var codec in codecs)
                    //Debug.WriteLine(codec.MimeType.ToString());
                    if (codec.MimeType == mimetype)
                        retcodec = codec;
            }
            catch (Exception ex)
            {
                throw new Exception("Error processing GetEncoder (MimeType)", ex);
            }

            return retcodec;
        }

        /// <summary>
        ///     Pulls data from a stream and returns a byte[]
        /// </summary>
        /// <param name="stream">The stream.</param>
        /// <returns></returns>
        /// <remarks>http://stackoverflow.com/questions/1080442/how-to-convert-an-stream-into-a-byte-in-c</remarks>
        public byte[] ReadToEnd(Stream stream)
        {
            var originalPosition = stream.Position;
            stream.Position = 0;

            try
            {
                var readBuffer = new byte[4096];

                var totalBytesRead = 0;
                int bytesRead;

                while ((bytesRead = stream.Read(readBuffer, totalBytesRead, readBuffer.Length - totalBytesRead)) > 0)
                {
                    totalBytesRead += bytesRead;

                    if (totalBytesRead == readBuffer.Length)
                    {
                        var nextByte = stream.ReadByte();
                        if (nextByte != -1)
                        {
                            var temp = new byte[readBuffer.Length * 2];
                            Buffer.BlockCopy(readBuffer, 0, temp, 0, readBuffer.Length);
                            Buffer.SetByte(temp, totalBytesRead, (byte) nextByte);
                            readBuffer = temp;
                            totalBytesRead++;
                        }
                    }
                }

                var buffer = readBuffer;
                if (readBuffer.Length != totalBytesRead)
                {
                    buffer = new byte[totalBytesRead];
                    Buffer.BlockCopy(readBuffer, 0, buffer, 0, totalBytesRead);
                }

                return buffer;
            }
            finally
            {
                stream.Position = originalPosition;
            }
        }
    }
}