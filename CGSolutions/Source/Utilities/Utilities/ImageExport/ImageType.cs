﻿namespace IBH.Utilities.ImageExport
{
    public class ImageTypes
    {
        /// <summary>
        ///     Print preview Id
        /// </summary>
        public int PrintPreviewId;

        /// <summary>
        ///     Thumbnail Id
        /// </summary>
        public int ThumbnailId;

        public ImageTypes()
        {
        }

        public ImageTypes(int thumbnailId, int printPreviewId)
        {
            ThumbnailId = thumbnailId;
            PrintPreviewId = printPreviewId;
        }
    }
}