using System.Configuration;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Web;

namespace Utilities.GeoIPLookup
{
    public class GeoIPLookupService
    {
        public static string GeoLookupDBPath
        {
            get
            {
                if (HttpContext.Current == null)
                {
                    var binFolder = Path.GetDirectoryName(Assembly.GetEntryAssembly().Location);
                    return binFolder + "/resources/GIPDB/GeoIP.dat/";
                }

                return HttpContext.Current.Server.MapPath("/resources/GIPDB/GeoIP.dat");
            }
        }

        public static string GeoLiteLookupDBPath
        {
            get
            {
                if (HttpContext.Current == null)
                {
                    var binFolder = Path.GetDirectoryName(Assembly.GetEntryAssembly().Location);
                    return binFolder + "/resources/GIPDB/GeoLiteCity.dat/";
                }

                return HttpContext.Current.Server.MapPath("/resources/GIPDB/GeoLiteCity.dat");
            }
        }

        public string GetCountryByIP(string IP)
        {
            var sCountryCode = string.Empty;

            var ls = new LookupService(GeoLookupDBPath, LookupService.GEOIP_MEMORY_CACHE);

            var c = ls.getCountry(IP);
            if (c == null) return null;

            if (!string.IsNullOrWhiteSpace(IP))
            {
                var ipList = ConfigurationManager.AppSettings["Internal_IP"].Split(',').ToList();
                if (ipList.Contains(IP))
                {
                    sCountryCode = "US";
                }
                else
                {
                    sCountryCode = c.getCode();

                    if (!string.IsNullOrEmpty(sCountryCode))
                        sCountryCode = sCountryCode.Replace("--", "").Trim();

                    if (string.IsNullOrEmpty(sCountryCode))
                        sCountryCode = "US";
                    //**************************************Code End***************************************************************
                }

                return sCountryCode;
            }

            return null;
        }

        #region Method for getting City/Zipcode from GeoIPLookUpService

        /// <summary>
        ///     Method for getting City/Zipcode from GeoIPLookUpService
        /// </summary>
        /// <param name="IP"></param>
        /// <returns></returns>
        /// RB - 07/04/2011-Harsh-1458-Indeed Jobs on Registration
        public string GetLocationByIP(string IP)
        {
            var ls = new LookupService(GeoLiteLookupDBPath, LookupService.GEOIP_STANDARD);
            var loc = ls.getLocation(IP);
            if (loc == null)
                return string.Empty;
            return loc.city;
        }

        public string GetPostalCodeByIP(string IP)
        {
            var ls = new LookupService(GeoLiteLookupDBPath, LookupService.GEOIP_STANDARD);

            var loc = ls.getLocation(IP);

            if (loc == null)
                return string.Empty;
            return loc.postalCode;
        }

        public string GetStateByIP(string IP)
        {
            var ls = new LookupService(GeoLiteLookupDBPath, LookupService.GEOIP_STANDARD);

            var loc = ls.getLocation(IP);
            if (loc == null)
                return string.Empty;
            return loc.region;
        }

        public Location GetGeoLocationByIP(string IP)
        {
            var ls = new LookupService(GeoLiteLookupDBPath, LookupService.GEOIP_STANDARD);

            var loc = ls.getLocation(IP);

            return loc;
        }

        #endregion
    }
}