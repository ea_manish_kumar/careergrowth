﻿namespace Utilities.Localization
{
    public class ResourceKeys
    {
        public const string FailHeadingRELN = "FailHeadingRELN";
        public const string ResCheckLenText = "ResCheckLenText";
        public const string ResCheckMinLenHeading = "ResCheckMinLenHeading";
        public const string ResCheckMinLenText = "ResCheckMinLenText";
        public const string ResCheckSummaryShort = "ResCheckSummaryShort";
        public const string ResCheckSummaryLong = "ResCheckSummaryLong";
        public const string FailHeadingFIWD = "FailHeadingFIWD";
        public const string ResCheckGrammarCheck = "ResCheckGrammarCheck";
        public const string ResCheckVECH = "ResCheckVECH";


        public const string SkinNameClassic = "SkinNameClassic";
        public const string SkinNameExecutive = "SkinNameExecutive";
        public const string SkinNameCollegiate = "SkinNameCollegiate";
        public const string SkinNameContemporary = "SkinNameContemporary";
        public const string SkinNameAuthentic = "SkinNameAuthentic";
        public const string SkinNameAcademic = "SkinNameAcademic";
        public const string SkinNameModern = "SkinNameModern";
        public const string SkinNameAccomplished = "SkinNameAccomplished";
        public const string SkinNameBoldOne = "SkinNameBoldOne";
        public const string SkinNameScholastic = "SkinNameScholastic";
        public const string SkinNameComposed = "SkinNameComposed";
        public const string SkinNameAbstractOne = "SkinNameAbstractOne";
        public const string SkinNameAbstractTwo = "SkinNameAbstractTwo";
        public const string SkinNameArtistic = "SkinNameArtistic";
        public const string SkinNameEclectic = "SkinNameEclectic";
        public const string SkinNameMetropolitan = "SkinNameMetropolitan";
        public const string SkinNameDramatic = "SkinNameDramatic";
        public const string SkinNameExecutiveTwo = "SkinNameExecutiveTwo";
        public const string SkinNameOriginalTwo = "SkinNameOriginalTwo";
        public const string SkinNameIndustrial = "SkinNameIndustrial";
        public const string SkinNameElegant = "SkinNameElegant";
        public const string SkinNameOriginalOne = "SkinNameOriginalOne";
        public const string SkinNameBoldTwo = "SkinNameBoldTwo";
        public const string SkinNameWithLogo = "SkinNameWithLogo";
        public const string SkinNameInitialsGraphic = "SkinNameInitialsGraphic";
        public const string SkinNameWithIcon = "SkinNameWithIcon";
        public const string SkinNameSourceSanProGoogle = "SkinNameSourceSanProGoogle";
    }
}