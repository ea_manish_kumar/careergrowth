﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Threading;
using CommonModules.Exceptions;
using Resources;

namespace Utilities.Localization
{
    public class Intl
    {
        /// <summary>
        ///     It fetches Key from resource file based on value field
        /// </summary>
        /// <param name="value"></param>
        /// <returns></returns>
        public static string GetResxKeyByValue(string value)
        {
            var resourceKey = string.Empty;
            try
            {
                var resourceManager = Resource.ResourceManager;
                var entry = resourceManager.GetResourceSet(Thread.CurrentThread.CurrentCulture, true, true)
                    .OfType<DictionaryEntry>().FirstOrDefault(e =>
                        string.Equals(e.Value.ToString(), value, StringComparison.OrdinalIgnoreCase) &&
                        e.Key.ToString().StartsWith("url"));
                if (entry.Key != null)
                    resourceKey = entry.Key.ToString();
            }
            catch (Exception ex)
            {
                var objMethod = (MethodInfo) MethodBase.GetCurrentMethod();
                var ht = new Hashtable(1);
                ht.Add("ResourceValue", value);
                GlobalExceptionHandler.customExceptionHandler(ex, objMethod, ht, ErrorSeverityType.Normal);
            }

            //Remove 'url_' keyword
            if (resourceKey.Length > 4) resourceKey = resourceKey.Remove(0, 4);

            // Remove '_' by '-' [That is provided for seperation]
            resourceKey = resourceKey.Replace('_', '-');
            return resourceKey;
        }

        /// <summary>
        ///     Fetches Url from resource file
        /// </summary>
        /// <param name="resourcekey"></param>
        /// <param name="isIncludeTrainlingSlash"></param>
        /// <param name="resourceFileName"></param>
        /// <returns></returns>
        public static string GetUrlFromResource(string pageName)
        {
            var resourceValue = string.Empty;
            if (!string.IsNullOrEmpty(pageName))
            {
                var pageNameArr = pageName.Split('.');
                if (pageNameArr != null && pageNameArr.Length > 0)
                {
                    var resourceKey = "url_" + pageNameArr[0].Replace('-', '_');
                    try
                    {
                        var resourceManager = Resource.ResourceManager;
                        resourceValue = resourceManager.GetString(resourceKey);
                    }
                    catch (Exception ex)
                    {
                        var htParam = new Hashtable();
                        htParam.Add("PageName", pageName);
                        var objMethod = (MethodInfo) MethodBase.GetCurrentMethod();
                        GlobalExceptionHandler.customExceptionHandler(ex, objMethod, htParam, ErrorSeverityType.Normal);
                    }
                }
            }

            return resourceValue;
        }

        public static IDictionary<int, string> GetShortMonths()
        {
            IDictionary<int, string> shortMonths = new Dictionary<int, string>();
            shortMonths.Add(1, Resource.ShortMonthJan);
            shortMonths.Add(2, Resource.ShortMonthFeb);
            shortMonths.Add(3, Resource.ShortMonthMar);
            shortMonths.Add(4, Resource.ShortMonthApr);
            shortMonths.Add(5, Resource.MonthMay);
            shortMonths.Add(6, Resource.ShortMonthJun);
            shortMonths.Add(7, Resource.ShortMonthJul);
            shortMonths.Add(8, Resource.ShortMonthAug);
            shortMonths.Add(9, Resource.ShortMonthSep);
            shortMonths.Add(10, Resource.ShortMonthOct);
            shortMonths.Add(11, Resource.ShortMonthNov);
            shortMonths.Add(12, Resource.ShortMonthDec);
            return shortMonths;
        }

        public static IEnumerable<string> GetDegrees()
        {
            IList<string> degrees = new List<string>();
            degrees.Add(Resource.DegreeOptions1);
            degrees.Add(Resource.DegreeOptions2);
            degrees.Add(Resource.DegreeOptions3);
            degrees.Add(Resource.DegreeOptions4);
            degrees.Add(Resource.DegreeOptions5);
            degrees.Add(Resource.DegreeOptions6);
            degrees.Add(Resource.DegreeOptions7);
            degrees.Add(Resource.DegreeOptions8);
            degrees.Add(Resource.DegreeOptions9);
            degrees.Add(Resource.DegreeOptions10);
            degrees.Add(Resource.DegreeOptions11);
            degrees.Add(Resource.DegreeOptions12);
            degrees.Add(Resource.DegreeOptions13);
            degrees.Add(Resource.DegreeOptions14);
            return degrees.AsEnumerable();
        }

        public static IDictionary<string, string> GetDocTypes()
        {
            IDictionary<string, string> docTypes = new Dictionary<string, string>();
            docTypes.Add("PDF", Resource.AdobePDF);
            docTypes.Add("TXT", Resource.PlainText);
            docTypes.Add("HTML", Resource.WebPage);
            docTypes.Add("DOCX", Resource.MSWord);
            return docTypes;
        }

        public static string PriceFormat(decimal amount)
        {
            if (amount == 0)
                return " 0";
            return amount.ToString("#.##");
        }
    }
}