﻿using Resources;

namespace Utilities.Communication
{
    public class EmailConstants
    {
        /// <summary>
        ///     Email address for welcome Email
        /// </summary>
        public const string EMAIL_ADDRESS = "EmailAddress";

        /// <summary>
        ///     Return URl for WelcomeEmail
        /// </summary>
        public const string RETURN_URL = "ReturnUrl";

        /// <summary>
        ///     User Name
        /// </summary>
        public const string USERNAME = "username";

        /// <summary>
        ///     first name
        /// </summary>
        public const string FIRST_NAME = "firstname";

        /// <summary>
        ///     Password
        /// </summary>
        public const string PASSWORD = "password";

        /// <summary>
        ///     Date of Creation
        /// </summary>
        public const string CREATION_DATE = "CreationDate";

        /// <summary>
        ///     Partner Name
        /// </summary>
        public const string PARTNER_NAME = "PartnerName";

        public const string PURCHASE_DETAILS = "PurchaseDetails";
        public const string CANCELLATION_NUMBER = "CancellationNumber";

        public const string RENEWAL_DATE = "RenewDate";
        public const string RENEWAL_AMOUNT = "RenewAmt";
        public const string DOMAINMSG = "DomainMsg";

        public const string WELCOME_TEXT = "welcometext";

        public const string OUTGOING_EMAILADDRESS = "OutgoingEmailAddress";

        /// <summary>
        ///     Landing Page Url Query Parameters
        /// </summary>
        public const string LP_URL_QP = "LP_URL_QP";

        /// <summary>
        ///     UserAgreement in Footer
        /// </summary>
        public const string FOTR_QP_USAG = "FOTR_QP_USAG";

        /// <summary>
        ///     Privacy Policy in Footer
        /// </summary>
        public const string FOTR_QP_PRPO = "FOTR_QP_PRPO";

        /// <summary>
        ///     Terms Of Use in Footer
        /// </summary>
        public const string FOTR_QP_CNUS = "FOTR_QP_CNUS";

        /// <summary>
        ///     Login Page Url Query Parameters
        /// </summary>
        public const string LOGIN_URL_QP = "LOGIN_URL_QP";

        /// <summary>
        ///     Unsubscribe Page Url Query Parameters
        /// </summary>
        public const string UNSUBSCRIBE_URL_QP = "UNSUBSCRIBE_URL_QP";

        /// <summary>
        ///     Kissmetrics Pixel for Open Events
        /// </summary>
        public const string KM_PIXEL_OPEN = "KM_PIXEL_OPEN";

        /// <summary>
        ///     resume home Page Url Query Parameters
        /// </summary>
        public const string ResumeHome_URL_QP = "ResumeHome_URL_QP";

        /// <summary>
        ///     Password Reset Link
        /// </summary>
        public const string PASSWORD_RESET_LINK = "password_reset_link";

        # region OrderEmail.cdi specific

        /// <summary>
        ///     Current Date
        /// </summary>
        public const string CURRENT_DATE = "CurrentDate";

        /// <summary>
        ///     Current Date
        /// </summary>
        public const string DATE = "Date";

        public const string CC_EMAILADDRESS = "CCEmailAddress";
        public const string CUSTOMER_NAME = "CustomerName";

        /// <summary>
        ///     Nudge Email Subject Line
        /// </summary>
        public const string SUBJECT_LINE = "SubjectLine";

        /// <summary>
        ///     Nudge Email Page URL for link
        /// </summary>
        public const string PAGE_URL = "PAGE_URL";

        /// <summary>
        /// </summary>
        public const string CUSTOMER_ADDRESS = "CustomerAddress";

        /// <summary>
        ///     LastTransactionAmount
        /// </summary>
        public const string LAST_TRANSACTION_AMOUNT = "LastTransactionAmount";

        /// <summary>
        ///     CustomerStateCode
        /// </summary>
        public const string CUSTOMER_STATE_CODE = "CustomerStateCode";

        /// <summary>
        /// </summary>
        public const string CUSTOMER_CITY = "CustomerCity";

        /// <summary>
        ///     Customer Zip code
        /// </summary>
        public const string CUSTOMER_ZIP = "CustomerZip";

        /// <summary>
        ///     RENEW date for the subscription
        /// </summary>
        public const string RENEW_DATE = "RenewsDate";

        public const string REVIEW_MESSAGE = "ReviewMessage";

        /// <summary>
        ///     Actual unit price of the subscription
        /// </summary>
        public const string UNIT_PRICE = "UnitPrice";

        /// <summary>
        ///     Trial amount of the subscription.
        /// </summary>
        public const string TRIAL_AMOUNT = "TrialAmount";

        /// <summary>
        ///     Trial days for the subscription.
        /// </summary>
        public const string TRIAL_DAYS = "TrialDays";

        /// <summary>
        /// </summary>
        public const string CUSTOMER_COUNTRY = "CustomerCountry";

        /// <summary>
        /// </summary>
        public const string ORDER_NUMBER = "OrderNumber";

        /// <summary>
        /// </summary>
        public const string ORDER_DATE = "OrderDate";

        /// <summary>
        /// </summary>
        public const string USER_ID = "UserID";

        /// <summary>
        /// </summary>
        public const string CUSTOMER_EMAIL_ADDRESS = "CustomerEmailAddress";

        /// <summary>
        /// </summary>
        public const string CUSTOMER_CC_NUMBER = "CustomerCCNumber";


        /// <summary>
        ///     From Email address like customercare
        /// </summary>
        public const string CC_FROMEMAILADDRESS = "FromEmailAddress";

        /// <summary>
        ///     First Name of the Customer
        /// </summary>
        public const string CUSTOMER_FIRST_NAME = "FirstName";

        /// <summary>
        ///     Expiry Date of the Subscription
        /// </summary>
        public const string EXPIRY_DATE = "ExpiryDate";

        /// <summary>
        ///     payment date for subscription cancel
        /// </summary>
        public const string PAYMENT_DATE = "PaymentDate";

        /// <summary>
        ///     Amount of the subscription
        /// </summary>
        public const string AMOUNT = "Amount";

        /// <summary>
        ///     Recurring Schedule for the subscription
        /// </summary>
        public const string RECURRING_SCHEDULE_TYPE = "RecurringSchedule";

        /// <summary>
        ///     ScheduleType for the subscription
        /// </summary>
        public const string SCHEDULE_TYPE = "ScheduleType";

        /// <summary>
        ///     SKU Type
        /// </summary>
        public const string SKU_LABEL = "SkuLabel";

        /// <summary>
        ///     Amount for monthly SKU - deducted by recurring service
        /// </summary>
        public const string SUBSCRIPTION_AMOUNT = "SubscriptionAmount";

        /// <summary>
        ///     Subscription message to be shown in Order confirmation email
        /// </summary>
        public const string SUBSCRIPTION_MESSAGE = "SubscriptionMessage";

        /// <summary>
        ///     Amount for monthly SKU - deducted by recurring service
        /// </summary>
        public const string RESUME_BUILDER_AMOUNT = "ResumeBuilder";

        /// <summary>
        ///     Amount for monthly SKU - deducted by recurring service
        /// </summary>
        public const string RESUME_REVIEW_AMOUNT = "ResumeReview";

        public const string DOMAIN_NAME = "DomainName";
        public const string CREDITCARDSTATMENTTEXT = "CreditCardStatmentText";
        public const string CREDITCARDDETAILTEXT = "CreditCardDetailText";
        public const string CREDITCARDDIGITS = "CreditCardDigits";

        # endregion
    }

    public class MailerConstants
    {
        public enum eMailOut
        {
            /// <summary>
            ///     Welcome Email
            /// </summary>
            eWelcome = 1,

            /// <summary>
            ///     Subscription/Order confirmation
            /// </summary>
            eSubscriptionConfirmation = 2,

            /// <summary>
            ///     User Password changed by admin
            /// </summary>
            ePasswordReset = 3,

            /// <summary>
            ///     User Subscription Cancellation - SELF
            /// </summary>
            eUserSubscriptionCancellation = 4,

            /// <summary>
            ///     Refund Confirmation
            /// </summary>
            eRefundConfirmation = 5,

            /// <summary>
            ///     Charge Back Confirmation
            /// </summary>
            eSubscriptionchargeback = 6,

            /// <summary>
            ///     Pro-rate refund confirmation
            /// </summary>
            eProrateRefundConfirmation = 7,

            /// <summary>
            ///     Cancel Confirmation - ADMIN
            /// </summary>
            eCancelConfirmAdmin = 8,

            /// <summary>
            ///     Comp Subscription Confirmation - ADMIN
            /// </summary>
            eCompSubscriptionConfirmation = 9,

            /// <summary>
            ///     Suspended email
            /// </summary>
            eUserSubscriptionSuspended = 10,

            /// <summary>
            ///     Payment Retry Mail - ADMIN
            /// </summary>
            eRecurringRetry = 11,

            /// <summary>
            ///     Nudge Email
            /// </summary>
            eNudgeEmail = 12,

            /// <summary>
            ///     Send Resume By Mail
            /// </summary>
            eSendResume = 13,

            /// <summary>
            ///     Send Resume Review By Mail
            /// </summary>
            eSendResumeReview = 14,

            /// <summary>
            ///     reset password by user.
            /// </summary>
            ePasswordResetByUser = 32
        }


        public enum eTemplateIndex
        {
            /// <summary>
            /// </summary>
            eTI_To = 0,

            /// <summary>
            /// </summary>
            eTI_Subject = 1,

            /// <summary>
            /// </summary>
            eTI_From = 2,

            /// <summary>
            /// </summary>
            eTI_CC = 3,

            /// <summary>
            /// </summary>
            eTI_BCC = 4,

            /// <summary>
            /// </summary>
            eTI_TextBody = 5,

            /// <summary>
            /// </summary>
            eTI_HTMLBody = 6,

            /// <summary>
            /// </summary>
            eTI_Attachment = 7,

            /// <summary>
            /// </summary>
            eTI_SMTPServer = 8,

            /// <summary>
            /// </summary>
            eTI_SMTPPort = 9,

            /// <summary>
            /// </summary>
            eTI_SMTPUsername = 10,

            /// <summary>
            /// </summary>
            eTI_SMTPPassword = 11,

            /// <summary>
            ///     It will be send as passsword in Welcome Email
            /// </summary>
            eTI_UserPassword = 12,

            /// <summary>
            ///     Constant which will be used to send campaign in custom header
            /// </summary>
            eTI_Campaign = 13
        }

        public const string EMAIL_DATA_DELIMITER = "%%";
        public const string EMAILTAG_CDI = "CDI:";
        public const string EMAILTAG_CDI_METADATA = "CDIM:";
        public const string EMAILTAG_CDI_EXTENDED = "CDIX:";

        public const string EMAILTAG_FORMAT_URL = "URL:";

        //URL encode
        public const string EMAILTAG_FORMAT_CUR = "CUR:";

        public const string EMAIL_IMAGE_PATH = "EMAIL_IMAGE_PATH";
        public const string EMAIL_IMAGE_REPLACE = "EMAIL_IMAGE_REPLACE";
        public const string CDN_DOMAIN_NAME = "CDN_PATH";
        public const string EMAIL_URL_REPLACE = "EMAIL_URL_REPLACE";
        public const string RESUMEGIG_CUSTOMERSERVICE_EMAIL = "RESUMEGIG_CUSTOMERSERVICE_EMAIL";
        public const string RESUMEGIG_OUTGOING_EMAIL = "RESUMEGIG_OUTGOING_EMAIL";
        public const string RECURRING_ADMIN_MAIL = "RECURRING_ADMIN_MAIL";
        public const string RESUMEGIG_REVIEW_EMAILFROM = "RESUMEGIG_REVIEW_EMAIL_FROM";
        public const string RESUMEGIG_REVIEW_EMAILTO = "RESUMEGIG_REVIEW_EMAIL_TO";
        public const string RESUMEGIG_ERROR_NOTIFY_EMAIL = "RESUMEGIG_ERROR_NOTIFY_EMAIL";


        public const string RESUMEHELP_CUSTOMERSERVICE_EMAIL = "RESUMEHELP_CUSTOMERSERVICE_EMAIL";
        public const string RESUMEHELP_OUTGOING_EMAIL = "RESUMEHELP_OUTGOING_EMAIL";
        public const string RESUMEHELP_REVIEW_EMAILFROM = "RESUMEHELP_REVIEW_EMAIL_FROM";
        public const string RESUMEHELP_REVIEW_EMAILTO = "RESUMEHELP_REVIEW_EMAIL_TO";
        public const string RESUMEHELP_SUPPORT_EMAIL = "RESUMEHELP_SUPPORT_EMAIL";
        public const string RESUMEHELP_ERROR_NOTIFY_EMAIL = "RESUMEHELP_ERROR_NOTIFY_EMAIL";

        public const string RESUMEHELPUK_CUSTOMERSERVICE_EMAIL = "RESUMEHELPUK_CUSTOMERSERVICE_EMAIL";
        public const string RESUMEHELPUK_OUTGOING_EMAIL = "RESUMEHELPUK_OUTGOING_EMAIL";
        public const string RESUMEHELPUK_REVIEW_EMAILFROM = "RESUMEHELPUK_REVIEW_EMAIL_FROM";
        public const string RESUMEHELPUK_REVIEW_EMAILTO = "RESUMEHELPUK_REVIEW_EMAIL_TO";
        public const string RESUMEHELPUK_SUPPORT_EMAIL = "RESUMEHELPUK_SUPPORT_EMAIL";
        public const string RESUMEHELPUK_ERROR_NOTIFY_EMAIL = "RESUMEHELPUK_ERROR_NOTIFY_EMAIL";

        // SOCIAL NETWORK
        public const string TWITTER_URL = "TWITTER_URL";
        public const string FACEBOOK_URL = "FACEBOOK_URL";
        public const string GOOGLE_PLUS_URL = "GOOGLE_PLUS_URL";
        public const string LINKEDIN_URL = "LINKEDIN_URL";
        public const string TWITTER_URL_REPLACE = "TWITTER_URL_REPLACE";
        public const string FACEBOOK_URL_REPLACE = "FACEBOOK_URL_REPLACE";
        public const string GOOGLE_URL_REPLACE = "GOOGLE_URL_REPLACE";
        public const string LINKEDIN_URL_REPLACE = "LINKEDIN_URL_REPLACE";

        public static string WEEKLY_SUBSCRIPTION_MESSAGE = string.Format(Resource.WEEKLY_SUBSCRIPTION_MESSAGE,
            "%%ReviewMessage%%", "%%CDI:DomainName%%", "%%CDI:ResumeBuilder%%", "%%CDI:ResumeReview%%",
            "%%CDI:RenewsDate%%", "%%CDI:SubscriptionAmount%%");

        public static string WEEKLY_NEW_SUBSCRIPTION_MESSAGE = string.Format(Resource.WEEKLY_NEW_SUBSCRIPTION_MESSAGE,
            "%%ReviewMessage%%", "%%CDI:DomainName%%", "%%CDI:ResumeBuilder%%", "%%CDI:ResumeReview%%",
            "%%CDI:RenewsDate%%", "%%CDI:SubscriptionAmount%%");

        public static string MONTHLY_SUBSCRIPTION_MESSAGE = string.Format(Resource.MONTHLY_SUBSCRIPTION_MESSAGE,
            "%%ReviewMessage%%", "%%CDI:RenewsDate%%", "%%CDI:SubscriptionAmount%%");

        public static string YEARLY_SUBSCRIPTION_MESSAGE = string.Format(Resource.YEARLY_SUBSCRIPTION_MESSAGE,
            "%%ReviewMessage%%", "%%CDI:RenewsDate%%", "%%CDI:SubscriptionAmount%%");

        public static string RESUME_REVIEW_MESSAGE =
            string.Format(Resource.RESUME_REVIEW_MESSAGE, "<a href='{0}'>", "</a>.<br/><br/>");

        public static string WEEKLY_RENEWSUBSCRIPTION_MESSAGE = string.Format(Resource.WEEKLY_RENEWSUBSCRIPTION_MESSAGE,
            "%%ReviewMessage%%", "%%CDI:RenewsDate%%", "%%CDI:SubscriptionAmount%%");

        //COMPLEMENTARY EMAIL MESSAGE
        public static string COMP_SUBSCRIPTION_MESSAGE = string.Format(Resource.COMP_SUBSCRIPTION_MESSAGE, "{0}",
            "%%CDI:OrderDate%%", "%%CDI:RenewsDate%%");

        public static string REBATE_WEEKLY_MESSAGE = string.Format(Resource.REBATE_WEEKLY_MESSAGE, "%%CDI:DomainName%%",
            "%%CDI:RenewsDate%%", "%%CDI:SubscriptionAmount%%");

        public static string REBATE_MONTHLY_MESSAGE = string.Format(Resource.REBATE_MONTHLY_MESSAGE,
            "%%CDI:DomainName%%", "%%CDI:RenewsDate%%", "%%CDI:SubscriptionAmount%%");

        public static string REBATE_YEARLY_MESSAGE = string.Format(Resource.REBATE_YEARLY_MESSAGE, "%%CDI:DomainName%%",
            "%%CDI:RenewsDate%%", "%%CDI:SubscriptionAmount%%");

        public static string CreditCardStatementMessageRH =
            string.Format(Resource.CreditCardStatementMessageRH, "<strong>", "</strong>");

        public static string CreditCardStatementMessageRG =
            string.Format(Resource.CreditCardStatementMessageRG, "<strong>", "</strong>");

        public static string CardDetailText = string.Format(Resource.CardDetailText, "<strong>",
            "</strong><br />%%CDI:CreditCardDigits%%<br />");
    }

    public class SMTPSETTINGS
    {
        public enum eSMTPMODES
        {
            /// <summary>
            ///     Transactional Mode
            /// </summary>
            eTI_SMTPTRANSACTIONALMODE = 1,

            /// <summary>
            ///     External Mode
            /// </summary>
            eTI_SMTPUSERGENERATEDMODE = 2,

            /// <summary>
            ///     SG Transactional Mode
            /// </summary>
            eTI_SGTRANSACTIONALMODE = 3,

            /// <summary>
            ///     SG External Mode
            /// </summary>
            eTI_SGUSERGENERATEDMODE = 4
        }

        public const string TRANSACTIONALSMTP = "SMTPSETTINGS/TRANSACTIONAL";
        public const string EXTERNALSMTP = "SMTPSETTINGS/USERGENERATED";
        public const string SENDGRIDRGTRANSACTIONALSMTP = "SMTPSETTINGS/SENDGRIDRG/TRANSACTIONAL";
        public const string SENDGRIDRGEXTERNALSMTP = "SMTPSETTINGS/SENDGRIDRG/USERGENERATED";
        public const string SENDGRIDTRANSACTIONALSMTP = "SMTPSETTINGS/SENDGRID/TRANSACTIONAL";
        public const string SENDGRIDEXTERNALSMTP = "SMTPSETTINGS/SENDGRID/USERGENERATED";
    }

    /// <summary>
    ///     TemplateIndex
    /// </summary>
    public class TemplateIndex
    {
        /// <summary>
        /// </summary>
        public const string eTI_To = "eTI_To";

        /// <summary>
        /// </summary>
        public const string eTI_Subject = "eTI_Subject";

        /// <summary>
        /// </summary>
        public const string eTI_From = "eTI_From";

        /// <summary>
        /// </summary>
        public const string eTI_CC = "eTI_CC";

        /// <summary>
        /// </summary>
        public const string eTI_BCC = "eTI_BCC";

        /// <summary>
        /// </summary>
        public const string eTI_TextBody = "eTI_TextBody";

        /// <summary>
        /// </summary>
        public const string eTI_HTMLBody = "eTI_HTMLBody";

        /// <summary>
        /// </summary>
        public const string eTI_Attachment = "eTI_Attachment";

        /// <summary>
        /// </summary>
        public const string eTI_SMTPServer = "eTI_SMTPServer";

        /// <summary>
        /// </summary>
        public const string eTI_SMTPPort = "eTI_SMTPPort";

        /// <summary>
        /// </summary>
        public const string eTI_SMTPUsername = "eTI_SMTPUsername";

        /// <summary>
        /// </summary>
        public const string eTI_SMTPPassword = "eTI_SMTPPassword";

        public const string eTI_Header = "Header";
        public const string eTI_ReplyTo = "ReplyTo";
    }

    public class TemplateName
    {
        public const string WELCOME = "welcome";
        public const string SUBSCRIPTION_CONFIRMATION = "subscription_confirmation";
        public const string PASSWORD_EMAIL = "password_reset";
        public const string CANCELLATION_COFIRMATION = "cancellation_confirmation";
        public const string REFUND_CONFIRMATION = "refund_confirmation";
        public const string CHARGEBACK_CONFIRMATION = "chargeback_confirmation";
        public const string PRORATEREFUND_CONFIRMATION = "proraterefund_confirmation";
        public const string CANCELLATION_COFIRMATION_ADMIN = "cancellation_confirmation";
        public const string COMPLEMENTARY_SUBSCRIPTION = "complementary_subscription";
        public const string SUSPENDED_USER = "suspended_user";
        public const string RECURRING_RETRY_FAIL = "recurring_retry_fail";
        public const string NUDGEEMAIL = "nudge_email";
    }
}