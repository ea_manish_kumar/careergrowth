using Resources;

namespace Utilities.Constants
{
    public class UserConstants
    {
        public const int GuestUser = 0;
        public const int User = 1;
        public const int AdminUser = 100;
        public const int AdminUserWithoutReport = 101;
    }

    public enum ParserType
    {
        BurningGlass = 1
    }

    public static class HttpWebRequestMethods
    {
        /// <summary>
        ///     HTTPGet
        /// </summary>
        public static string HTTPGet = "GET";

        /// <summary>
        ///     HTTPPost
        /// </summary>
        public static string HTTPPost = "POST";
    }

    public class UserNotificationConstants
    {
        public const short Emails = 1;
        public const short Jobs = 2;
    }

    public class SessionDataTypeConstants
    {
        /// <summary>
        ///     Resume ID
        /// </summary>
        public static string DocumentId = "DCID";

        /// <summary>
        ///     Domain CD
        /// </summary>
        public static string DomainFriendlyName = "DMNM";

        /// <summary>
        ///     Referring Page
        /// </summary>
        public static string RefPageName = "RPGN";

        /// <summary>
        ///     Email Type
        /// </summary>
        public static string EmailRegisteredUser = "EMUS";

        /// <summary>
        ///     Cover Letter ID
        /// </summary>
        public static string CoverLetterID = "LTID";

        /// <summary>
        ///     Referring Page Target
        /// </summary>
        public static string PageTarget = "TARG";
    }

    public class UserDataType
    {
        /// <summary>
        /// Save the data for the end user's action on different buttons
        /// </summary>
        public const string UserNavigation = "UNAV";
        public static string Email_Guid = "EMLGD";
        public static string PaymentAction = "PAYMT";
        public static string PrintAction = "PRINT";
        public static string DownloadAction = "DOWLD";
        public static string EmailAction = "EMAIL";
        public static string PasswordEmailGuid = "PWDGD";
        public static string WelcomeBackFlow = "WELCB";


        /*TODO: Remove the Constants within this flower box, nothing being used in ==> DBConstants.UserDataType
        /// <summary>
        ///     Order ID
        /// </summary>
        public static string OrderID = "ORID";

        /// <summary>
        ///     Tips Call Out Stage CD
        /// </summary>
        public static string TipsCallOutStageCD = "TSTG";

        /// <summary>
        ///     Next Billing Date For New Ecom
        /// </summary>
        public static string NextBillingDate = "NXBD";

        /// <summary>
        ///     Job Title
        /// </summary>
        public static string JobTitle = "JTIT";

        /// <summary>
        ///     Last Additional section data
        /// </summary>
        public static string SectionData = "SCDT";

        /// <summary>
        ///     Additional section list
        /// </summary>
        public static string AdditionalSectionsList = "SCLS";

        
        /// <summary>
        ///     Credit Card Click Event Data
        /// </summary>
        public static string CreditCardClickEvent = "CCCE";

        /// <summary>
        ///     Current user searched by admin
        /// </summary>
        public static string UserIDForAdmin = "UADM";

        /// <summary>
        ///     SkinCD selected by user while creating Resume
        /// </summary>
        public static string SkinCD = "SKCD";

        /// <summary>
        ///     DocumentID by user while creating Resume
        /// </summary>
        public static string DocumentID = "DCID";
        */

    }

    public class DomainFriendlyNameConstants
    {
        public const string FreeResume = "FR";
        public const string FresherResume = "FS";
    }

    public class DomainTitle
    {
        public const string FreeResume = "FreeResume";
        public const string FresherResume = "FresherResume";
    }

    public class UserLevelCodes
    {
        public const short UnSubscribed_User = 0;
        public const short Subscribed_User = 1;
    }

    public class UserRoleCodes
    {
        public const short Guest_User = 0;
        public const short Registered_User = 1;
    }

    public class ABTestConstants
    {
        public const int RBVERSION = 1;
        public const int PARSER = 2;
        public const int PROGRESSINDICATOR = 3;
        public const int PREFILLEDBACKGROUND = 4;
        public const int ENGAGEMENTSPAGES = 5;
        public const int RESUMEREVIEW = 6;
        public const int EDUCATIONNEWUI = 7;
        public const int FINALIZERESUME = 8;
        public const int RESUMEGIGNEWCONTENT = 9;
        public const int RESUMECHECK = 10;
        public const int BROWSERNAVIGATION = 11;
        public const int PARSERUPLOADUI = 12;
        public const int CHOOSEDESIGN = 13;
        public const int COVERLETTER = 14;
        public const int ADDITIONALSECTION = 15;
        public const int WIZARDTIPSSECTION = 16;
        public const int ECOMMVALIDATION = 17;
        public const int PREWIZARDLOADSCREEN = 18;
        public const int WIZARDNEWUI = 19;
        public const int CANCELPATHWAY = 20;
        public const int CLBCVRTEST = 21;
        public const int DEFAULTCONTENT = 22;
        public const int ECOMAPI = 23;
        public const int BGSOVREN = 24;
        public const int PROGRESSBARWIZARD = 25;
        public const int UNIQUESKINTHUMBNAIL = 26;
        public const int HOVERONSKINS = 27;
        public const int AutoComplete = 28;
        public const int REDESIGNBUTTONS = 29;
        public const int EXPRNOTIFICATIONS = 30;
        public const int COPYPASTERESUME = 31;
        public const int WATERMARKEDPRINT = 32;
        public const int MobileFriendlyLayout = 33;
        public const int LANDINGPAGE = 35;
        public const int MORESECTIONINFUNNEL = 36;
        public const int CHECKOUTPAGE = 37;
        public const int UPDATETIPSDESIGN = 38;
        public const int ONLINEWEBCANCEL = 39;
        public const int RECURRINGVARIATION = 40;
        public const int Pagination = 41;
        public const int FINALIZEDESIGN = 42;
        public const int WELCOMEBACK = 43;
        public const int SMARTLOOK = 44;
        public const int MOBILE_FUNNEL = 45;
        public const int SELLPAGENEWDESIGN = 46;
        public const int EXPERIENCEPAGE = 47;
    }

    public class AddressTypeCD
    {
        public const string AddressTypeHome = "HOME";
        public const string AddressTypeBilling = "BILL";
    }

    public class ABTestCaseConstants
    {
        public const int RESUMEREVIEW_BASELINE = 1;
        public const int RESUMEREVIEW_FREERR = 2;
        public const int RESUMEREVIEW_BASELINE_NEWLP = 3;

        public const int RESUMECHECK_BASELINE = 1;
        public const int RESUMECHECK_CHECK = 2;

        public const int COPYPASTERESUME_BASELINE = 1;
        public const int COPYPASTERESUME_COPY = 2;

        public const int CANCELPATHWAY_BASELINE = 1;
        public const int CANCELPATHWAY_NEWCANCELPATHWAY = 2;

        public const int EXPRNOTIFICATIONS_BASELINE = 1;
        public const int EXPRNOTIFICATIONS_SHOW = 2;

        public const int WATERMARKEDPRINT_BASELINE = 1;
        public const int WATERMARKEDPRINT_PRINTPREVIEWBUTTON = 2;
        public const int WATERMARKEDPRINT_PRINTBUTTON = 3;

        public const int UPDATETIPSDESIGN_BASELINE = 1;
        public const int UPDATETIPSDESIGN_NEWDESIGN = 2;

        public const int ONLINEWEBCANCEL_BASELINE = 1;
        public const int ONLINEWEBCANCEL_NEWDESIGN = 2;

        public const int PAGINATION_BASELINE = 1;
        public const int PAGINATION_ENABLE = 2;

        public const int FINALIZEDESIGN_BASELINE = 1;
        public const int FINALIZEDESIGN_NEWDESIGN = 2;

        public const int WELCOMEBACK_BASELINE = 1;
        public const int WELCOMEBACK_MODEL = 2;
        public const int MOBILE_FUNNEL_BASELINE = 1;
        public const int MOBILE_FUNNEL_NEW = 2;

        public const int SELLPAGENEWDESIGN_BASELINE = 1;
        public const int SELLPAGENEWDESIGN_VARIATIONA = 2;

        public const int EXPERIENCEPAGE_BASELINE = 1;
        public const int EXPERIENCEPAGE_COMBINE = 2;

        public const int SMARTLOOK_SHOW = 1;
        public const int SMARTLOOK_HIDE = 2;
    }

    public class PageUrl
    {
        public const string CONTACT_US = "/contact-us.aspx";
        public const string PRIVACY_POLICY = "/privacy-policy.aspx";
        public const string USER_AGREEMENT = "/user-agreement.aspx";
        public const string LOGIN = "/user/login.aspx";
        public const string FINALIZE_RESUME_A = "/resume/finalize-resume.aspx";
        public const string RESUME_REVIEW = "/review/resume-review.aspx";
    }

    public class PageUrlName
    {
        public const string CONTACT_US = "ContactUs";
        public const string PRIVACY_POLICY = "PrivacyPolicy";
        public const string USER_AGREEMENT = "TermsOfUse";
        public const string LOGIN = "Login";
        public const string FINALIZE_RESUME_A = "Finalize-A";
        public const string LANDING_PAGE = "LandingPage";
        public const string RESUME_REVIEW = "ResumeReview";
        public const string SETTINGS_PAGE = "Settings";
        public const string RESUME_HOME = "UserHome";
    }

    public class AnalyticsProperties
    {
        public const string TRACKING_EMAIL = "Email-Tracking";
        public const string CAMPAIGN_TYPE = "Campaign-Type";
        public const string DATE_SENT = "Date-Sent";
        public const string LINK = "Link";
        public const string LINK_CLICKED_DATE = "Link-Clicked-Date";
        public const string EMAIL_PAID_EVENT = "Paid-Email";
        public const string PAYMENT_COMPLETE_EVENT = "Payment-Complete";
        public const string PAID_AMOUNT = "Paid-Amount";
        public const string CUSTOMERID = "CustoemrID";
        public const string PAYMENTID = "PaymentID";
        public const string EMAIL_REVENUE = "Revenue-Email";
        public const string CAMPAIGN_SOURCE = "Campaign-Source";
        public const string LINK_CAMPAIGN_TYPE = "Link-Campaign-Type";
        public const string TRACKING_LINK = "Link-Tracking";
    }

    public class CLBProgressCode
    {
        public const string DATATYPECD = "USTG";
        public const string CLSTYL = "CLSTYL";
        public const string CLTYPE = "CLTYPE";
        public const string CLCONT = "CLCONT";
        public const string RECIPT = "RECIPT";
        public const string SUBJCT = "SUBJCT";
        public const string GRETNG = "GRETNG";
        public const string OPENER = "OPENER";
        public const string CLBODY = "CLBODY";
        public const string CLTACT = "CLTACT";
        public const string CLOSER = "CLOSER";
        public const string CFINAL = "CFINAL";
    }

    public class WritingAssistanceTypeCodes
    {
        public const string EXAMPLES = "EXMP";
        public const string TIPS = "TIPS";
        public const string DEFN = "DEFN";
        public const string DEFAULT_TEXT = "DFTX";
    }

    public class RequestHeader
    {
        public const string HTTP_X_FORWARDED_FOR = "HTTP_X_FORWARDED_FOR";
        public const string REMOTE_ADDR = "REMOTE_ADDR";
        public const string X_FORWARDED_FOR = "X-Forwarded-For";
        public const string UserAgent = "User-Agent";
        public const string AcceptLanguage = "Accept-Language";
        public const string JSON_Format = "application/json";
        public const string GZIP = "gzip";
        public const string DEFLATE = "deflate";
        public const string ClientCD = "clientcd";
        public const string ClientRequestReference = "client_request_reference";
        public const string Authorization = "Authorization";
        public const string AuthorizationToken = "Authorization: ";
    }

    public class UserTracking
    {
        public const string QS_SOURCE = "source";
        public const string QS_SRC = "src";
        public const string GOOGLE = "google";
        public const string BING = "bing";
        public const string HTTP_REFERER = "HTTP_REFERER";
        public const string ORGANIC = "Organic";
        public const string DIRECT = "Direct";
        public const string REFERRAL = "Referral";
        public const string INTERNAL_MEDIUM = "Internal";
        public const string INTERNAL_REFERRAL = "Internal_Referral";
        public const string UTM_SOURCE = "utm_source"; //Required parameter to identify the source of the traffic 

        public const string
            UTM_MEDIUM =
                "utm_medium"; //Required parameter to identify the medium the link was used upon such as: email, CPC, or other method of sharing

        public const string
            UTM_CAMPAIGN =
                "utm_campaign"; //Required parameter to identify a specific product promotion or strategic campaign such as a spring sale or other promotion

        public const string
            UTM_TERM = "utm_term"; //Optional parameter suggested for paid search to identify keywords for the ad

        public const string
            UTM_KEYWORD =
                "utm_keyword"; //Our Marketing Team uses this instead of UTM_TERM, Optional parameter suggested for paid search to identify keywords for the ad

        public const string UTM_CONTENT = "utm_content";
        public const string UTM_ID = "utm_id";
        public const string AFFILIATE = "affiliate";
        public const char QS_SEPARATOR = '?';
        public const string ADMIN_SOURCE = "Admin";
    }

    public class StatusCodes
    {
        public const string ACTIVE = "ACTV";
    }

    public class ConfigKeys
    {
        public const string USE_INSERTDETECTRIGHT = "USE_INSERTDETECTRIGHT";
        public const string PRE_FETCH_RESOURCES = "PRE_FETCH_RESOURCES";
        public const string ECOM_MODE = "ECOM_MODE";
        public const string EnableBundling = "EnableBundling";
    }

    public class ApiHttpStatusCode
    {
        // Summary:
        //     Equivalent to HTTP status 100. System.Net.HttpStatusCode.Continue indicates
        //     that the client can continue with its request.
        public const string Continue = "Continue";

        //
        // Summary:
        //     Equivalent to HTTP status 101. System.Net.HttpStatusCode.SwitchingProtocols
        //     indicates that the protocol version or protocol is being changed.
        public const string SwitchingProtocols = "SwitchingProtocols";

        //
        // Summary:
        //     Equivalent to HTTP status 200. System.Net.HttpStatusCode.OK indicates that
        //     the request succeeded and that the requested information is in the response.
        //     This is the most common status code to receive.
        public const string OK = "OK";

        //
        // Summary:
        //     Equivalent to HTTP status 201. System.Net.HttpStatusCode.Created indicates
        //     that the request resulted in a new resource created before the response was
        //     sent.
        public const string Created = "Created";

        //
        // Summary:
        //     Equivalent to HTTP status 202. System.Net.HttpStatusCode.Accepted indicates
        //     that the request has been accepted for further processing.
        public const string Accepted = "Accepted";

        //
        // Summary:
        //     Equivalent to HTTP status 203. System.Net.HttpStatusCode.NonAuthoritativeInformation
        //     indicates that the returned metainformation is from a cached copy instead
        //     of the origin server and therefore may be incorrect.
        public const string NonAuthoritativeInformation = "NonAuthoritativeInformation";

        //
        // Summary:
        //     Equivalent to HTTP status 204. System.Net.HttpStatusCode.NoContent indicates
        //     that the request has been successfully processed and that the response is
        //     intentionally blank.
        public const string NoContent = "NoContent";

        //
        // Summary:
        //     Equivalent to HTTP status 205. System.Net.HttpStatusCode.ResetContent indicates
        //     that the client should reset (not reload) the current resource.
        public const string ResetContent = "ResetContent";

        //
        // Summary:
        //     Equivalent to HTTP status 206. System.Net.HttpStatusCode.PartialContent indicates
        //     that the response is a partial response as requested by a GET request that
        //     includes a byte range.
        public const string PartialContent = "PartialContent";

        //
        // Summary:
        //     Equivalent to HTTP status 300. System.Net.HttpStatusCode.MultipleChoices
        //     indicates that the requested information has multiple representations. The
        //     default action is to treat this status as a redirect and follow the contents
        //     of the Location header associated with this response.
        public const string MultipleChoices = "MultipleChoices";

        //
        // Summary:
        //     Equivalent to HTTP status 300. System.Net.HttpStatusCode.Ambiguous indicates
        //     that the requested information has multiple representations. The default
        //     action is to treat this status as a redirect and follow the contents of the
        //     Location header associated with this response.
        public const string Ambiguous = "Ambiguous";

        //
        // Summary:
        //     Equivalent to HTTP status 301. System.Net.HttpStatusCode.MovedPermanently
        //     indicates that the requested information has been moved to the URI specified
        //     in the Location header. The default action when this status is received is
        //     to follow the Location header associated with the response.
        public const string MovedPermanently = "MovedPermanently";

        //
        // Summary:
        //     Equivalent to HTTP status 301. System.Net.HttpStatusCode.Moved indicates
        //     that the requested information has been moved to the URI specified in the
        //     Location header. The default action when this status is received is to follow
        //     the Location header associated with the response. When the original request
        //     method was POST; the redirected request will use the GET method.
        public const string Moved = "Moved";

        //
        // Summary:
        //     Equivalent to HTTP status 302. System.Net.HttpStatusCode.Found indicates
        //     that the requested information is located at the URI specified in the Location
        //     header. The default action when this status is received is to follow the
        //     Location header associated with the response. When the original request method
        //     was POST; the redirected request will use the GET method.
        public const string Found = "Found";

        //
        // Summary:
        //     Equivalent to HTTP status 302. System.Net.HttpStatusCode.Redirect indicates
        //     that the requested information is located at the URI specified in the Location
        //     header. The default action when this status is received is to follow the
        //     Location header associated with the response. When the original request method
        //     was POST; the redirected request will use the GET method.
        public const string Redirect = "Redirect";

        //
        // Summary:
        //     Equivalent to HTTP status 303. System.Net.HttpStatusCode.SeeOther automatically
        //     redirects the client to the URI specified in the Location header as the result
        //     of a POST. The request to the resource specified by the Location header will
        //     be made with a GET.
        public const string SeeOther = "SeeOther";

        //
        // Summary:
        //     Equivalent to HTTP status 303. System.Net.HttpStatusCode.RedirectMethod automatically
        //     redirects the client to the URI specified in the Location header as the result
        //     of a POST. The request to the resource specified by the Location header will
        //     be made with a GET.
        public const string RedirectMethod = "RedirectMethod";

        //
        // Summary:
        //     Equivalent to HTTP status 304. System.Net.HttpStatusCode.NotModified indicates
        //     that the client's cached copy is up to date. The contents of the resource
        //     are not transferred.
        public const string NotModified = "NotModified";

        //
        // Summary:
        //     Equivalent to HTTP status 305. System.Net.HttpStatusCode.UseProxy indicates
        //     that the request should use the proxy server at the URI specified in the
        //     Location header.
        public const string UseProxy = "UseProxy";

        //
        // Summary:
        //     Equivalent to HTTP status 306. System.Net.HttpStatusCode.Unused is a proposed
        //     extension to the HTTP/1.1 specification that is not fully specified.
        public const string Unused = "Unused";

        //
        // Summary:
        //     Equivalent to HTTP status 307. System.Net.HttpStatusCode.RedirectKeepVerb
        //     indicates that the request information is located at the URI specified in
        //     the Location header. The default action when this status is received is to
        //     follow the Location header associated with the response. When the original
        //     request method was POST; the redirected request will also use the POST method.
        public const string RedirectKeepVerb = "RedirectKeepVerb";

        //
        // Summary:
        //     Equivalent to HTTP status 307. System.Net.HttpStatusCode.TemporaryRedirect
        //     indicates that the request information is located at the URI specified in
        //     the Location header. The default action when this status is received is to
        //     follow the Location header associated with the response. When the original
        //     request method was POST; the redirected request will also use the POST method.
        public const string TemporaryRedirect = "TemporaryRedirect";

        //
        // Summary:
        //     Equivalent to HTTP status 400. System.Net.HttpStatusCode.BadRequest indicates
        //     that the request could not be understood by the server. System.Net.HttpStatusCode.BadRequest
        //     is sent when no other error is applicable; or if the exact error is unknown
        //     or does not have its own error code.
        public const string BadRequest = "BadRequest";

        //
        // Summary:
        //     Equivalent to HTTP status 401. System.Net.HttpStatusCode.Unauthorized indicates
        //     that the requested resource requires authentication. The WWW-Authenticate
        //     header contains the details of how to perform the authentication.
        public const string Unauthorized = "Unauthorized";

        //
        // Summary:
        //     Equivalent to HTTP status 402. System.Net.HttpStatusCode.PaymentRequired
        //     is reserved for future use.
        public const string PaymentRequired = "PaymentRequired";

        //
        // Summary:
        //     Equivalent to HTTP status 403. System.Net.HttpStatusCode.Forbidden indicates
        //     that the server refuses to fulfill the request.
        public const string Forbidden = "Forbidden";

        //
        // Summary:
        //     Equivalent to HTTP status 404. System.Net.HttpStatusCode.NotFound indicates
        //     that the requested resource does not exist on the server.
        public const string NotFound = "NotFound";

        //
        // Summary:
        //     Equivalent to HTTP status 405. System.Net.HttpStatusCode.MethodNotAllowed
        //     indicates that the request method (POST or GET) is not allowed on the requested
        //     resource.
        public const string MethodNotAllowed = "MethodNotAllowed";

        //
        // Summary:
        //     Equivalent to HTTP status 406. System.Net.HttpStatusCode.NotAcceptable indicates
        //     that the client has indicated with Accept headers that it will not accept
        //     any of the available representations of the resource.
        public const string NotAcceptable = "NotAcceptable";

        //
        // Summary:
        //     Equivalent to HTTP status 407. System.Net.HttpStatusCode.ProxyAuthenticationRequired
        //     indicates that the requested proxy requires authentication. The Proxy-authenticate
        //     header contains the details of how to perform the authentication.
        public const string ProxyAuthenticationRequired = "ProxyAuthenticationRequired";

        //
        // Summary:
        //     Equivalent to HTTP status 408. System.Net.HttpStatusCode.RequestTimeout indicates
        //     that the client did not send a request within the time the server was expecting
        //     the request.
        public const string RequestTimeout = "RequestTimeout";

        //
        // Summary:
        //     Equivalent to HTTP status 409. System.Net.HttpStatusCode.Conflict indicates
        //     that the request could not be carried out because of a conflict on the server.
        public const string Conflict = "Conflict";

        //
        // Summary:
        //     Equivalent to HTTP status 410. System.Net.HttpStatusCode.Gone indicates that
        //     the requested resource is no longer available.
        public const string Gone = "Gone";

        //
        // Summary:
        //     Equivalent to HTTP status 411. System.Net.HttpStatusCode.LengthRequired indicates
        //     that the required Content-length header is missing.
        public const string LengthRequired = "LengthRequired";

        //
        // Summary:
        //     Equivalent to HTTP status 412. System.Net.HttpStatusCode.PreconditionFailed
        //     indicates that a condition set for this request failed; and the request cannot
        //     be carried out. Conditions are set with conditional request headers like
        //     If-Match; If-None-Match; or If-Unmodified-Since.
        public const string PreconditionFailed = "PreconditionFailed";

        //
        // Summary:
        //     Equivalent to HTTP status 413. System.Net.HttpStatusCode.RequestEntityTooLarge
        //     indicates that the request is too large for the server to process.
        public const string RequestEntityTooLarge = "RequestEntityTooLarge";

        //
        // Summary:
        //     Equivalent to HTTP status 414. System.Net.HttpStatusCode.RequestUriTooLong
        //     indicates that the URI is too long.
        public const string RequestUriTooLong = "RequestUriTooLong";

        //
        // Summary:
        //     Equivalent to HTTP status 415. System.Net.HttpStatusCode.UnsupportedMediaType
        //     indicates that the request is an unsupported type.
        public const string UnsupportedMediaType = "UnsupportedMediaType";

        //
        // Summary:
        //     Equivalent to HTTP status 416. System.Net.HttpStatusCode.RequestedRangeNotSatisfiable
        //     indicates that the range of data requested from the resource cannot be returned;
        //     either because the beginning of the range is before the beginning of the
        //     resource; or the end of the range is after the end of the resource.
        public const string RequestedRangeNotSatisfiable = "RequestedRangeNotSatisfiable";

        //
        // Summary:
        //     Equivalent to HTTP status 417. System.Net.HttpStatusCode.ExpectationFailed
        //     indicates that an expectation given in an Expect header could not be met
        //     by the server.
        public const string ExpectationFailed = "ExpectationFailed";

        //
        public const string UpgradeRequired = "UpgradeRequired";

        //
        // Summary:
        //     Equivalent to HTTP status 500. System.Net.HttpStatusCode.InternalServerError
        //     indicates that a generic error has occurred on the server.
        public const string InternalServerError = "InternalServerError";

        //
        // Summary:
        //     Equivalent to HTTP status 501. System.Net.HttpStatusCode.NotImplemented indicates
        //     that the server does not support the requested function.
        public const string NotImplemented = "NotImplemented";

        //
        // Summary:
        //     Equivalent to HTTP status 502. System.Net.HttpStatusCode.BadGateway indicates
        //     that an intermediate proxy server received a bad response from another proxy
        //     or the origin server.
        public const string BadGateway = "BadGateway";

        //
        // Summary:
        //     Equivalent to HTTP status 503. System.Net.HttpStatusCode.ServiceUnavailable
        //     indicates that the server is temporarily unavailable; usually due to high
        //     load or maintenance.
        public const string ServiceUnavailable = "ServiceUnavailable";

        //
        // Summary:
        //     Equivalent to HTTP status 504. System.Net.HttpStatusCode.GatewayTimeout indicates
        //     that an intermediate proxy server timed out while waiting for a response
        //     from another proxy or the origin server.
        public const string GatewayTimeout = "GatewayTimeout";

        //
        // Summary:
        //     Equivalent to HTTP status 505. System.Net.HttpStatusCode.HttpVersionNotSupported
        //     indicates that the requested HTTP version is not supported by the server.
        public const string HttpVersionNotSupported = "HttpVersionNotSupported";
    }

    public class ApiErrorCode
    {
        public const string INVALID_REQUEST_ERROR = "INVALID_REQUEST_ERROR";
        public const string WRONG_API_ERROR = "WRONG_API_ERROR";
        public const string RESOURCE_NOT_FOUND = "RESOURCE_NOT_FOUND";
        public const string INVALID_HEADER_VALUES = "INVALID_HEADER_VALUES";
        public const string API_DOES_NOT_EXIST = "API_DOES_NOT_EXIST";
        public const string API_ERROR = "API_ERROR";
        public const string NOT_AUTHORIZED = "NOT_AUTHORIZED";
    }

    public class ApiErrorMessage
    {
        public const string API_ERROR = "Internal Server Error";
        public const string API_DOES_NOT_EXIST = "API you are looking for doesn't exist.";

        public const string INVALID_HEADER_VALUES =
            "Please provide valid cliend cd and client request reference in the header.";

        public const string NOT_AUTHORIZED = "You are not authorized to access the requested resource.";
    }


    public static class ApiConstants
    {
        public const string EXCEPTION_ERROR_MESSAGE = "Server is not able to proceed due to some internal error.";
        public const string CLIENT_REQUEST_REFERENCE = "client_request_reference";
        public const string CLIENT_CD = "ClientCD";
        public const string AUTHORIZATION = "Authorization";
        public const string NOT_AUTHORIZED = "You are not authorized to access the requested resource.";
        public const string BAD_REQUEST = "Bad Request";
        public const string DEFAULT_CYCLE = "DEFAULT";
    }

    public enum JobAlertUnsubscribeType
    {
        EMAIL = 0,
        PORTAL = 1,
        PAIDUSER = 2,
        REJECT = 3,
        HARD_BOUNCE = 4,
        SPAM = 5,
        UNSUB = 6,
        WIDGET = 7,
        NO_JOBSEARCH = 8,
        IRR_JOB_RESULTS = 9,
        EMAIL_BROKEN = 10,
        NO_SIGNUP = 11,
        INBOUND = 12,
        OTHER = 13
    }

    public class OrderCancellationReasonCD
    {
        public static string JOB = "JOB";

        public static string TEMP = "TEMP";

        public static string EXPS = "EXPS";

        public static string TECH = "TECH";

        public static string HELP = "HELP";

        public static string OTHR = "OTHR";
    }

    public class OrderCancellationTypeCD
    {
        public static string EMAL = "EMAL";

        public static string CHAT = "CHAT";

        public static string LETR = "LETR";

        public static string PHON = "PHON";

        public static string SELF = "SELF";
    }

    public static class ErrorMessage
    {
        public const string SubscriptionDetailsNotFound = "User Subscription Details Not Found";
        public const string ErrorInRefundProcessing = "There is some error while processing Refund/Chargeback";
        public const string InvalidRefundAmount = "Amount can't be greater than actual transaction amount";
        public const string InvalidRefundRequest = "Invalid refund request.";
    }


    public class CreditCardDetail
    {
        public string CreditCardNumber { get; set; }
        public string CardHolderName { get; set; }
        public short ExpiryYear { get; set; }
        public byte ExpiryMonth { get; set; }
        public string ccBIN { get; set; }
        public string CardType { get; set; }
        public string CreditCardCVV { get; set; }
    }

    public static class CookieConstants
    {
        public const string SessionId = "sid";
        public const string AdminSessionId = "asid";
        public const string BrowserId = "bid";
        public const string ApplicationCookie = "ruid";
        public const string LastUpdated = "lu";
        public const string SignedIn = "sn";
        public const string SignOutType = "so";
        public const string DocumentCookie = "dc";
        public const string CampaignCD = "cmpcd";
        public const string MobileABTest = "mbl";
    }

    public static class Roles
    {
        public const string Guest = "Guest";
        public const string User = "User";
        public const string Admin = "Admin";
        public const string AdminWithoutReport = "AdminWR";
    }

    public static class ClaimsType
    {
        public const string UserId = "UserId";
        public const string UserName = "UserName";
        public const string DisplayName = "DisplayName";
        public const string Role = "Role";
        public const string UserAuthId = "UserAuthId";
        public const string AdminId = "AdminId";
        public const string AdminRole = "AdminRole";
    }

    public static class UserAuthTypeCD
    {
        public const string Guest = "GUEST";
        public const string SignIn = "SGNIN";
        public const string SignInWizard = "SGNWZ";
        public const string Registered = "RGSTN";
        public const string AutoLogin = "ATLGN";
        public const string Admin = "ADMIN";
    }

    public static class QueryStringConstants
    {
        public const string EmailGUID = "eguid";
        public const string UserId = "uid";
        public const string UtmCamapign = "utm_campaign";
        public const string LinkId = "lid";
        public const string ReturnUrl = "ReturnUrl";
        public const string PasswordChangeGUID = "pguid";
        public const string campaignCD = "campaignCD";
        public const string OrderId = "oid";
        public const string SubscriptionLevel = "sub";
        public const string Domain = "dm";
    }

    public static class VisitType
    {
        public const string Returning = "R";
        public const string New = "N";
    }

    public class MobileRoutes
    {
        public static readonly string Mobile = Resource.url_mobile;
        public static readonly string Dashboard = Resource.url_dashboard;
        public static readonly string Resume = Resource.url_resume;
        public static readonly string Finalize = Resource.url_finalize;
        public static readonly string HowItWorks = Resource.url_How_It_Works;
        public static readonly string User = Resource.url_user;
        public static readonly string ResetPassword = Resource.url_reset_password;
        public static readonly string Account = Resource.url_account;
        public static readonly string Order = Resource.url_order;
        public static readonly string Routing = "routing";
        public static readonly string ContactUs = Resource.url_contact_us;
        public static readonly string PrivacyPolicy = Resource.url_privacy_policy;
        public static readonly string UserAgreement = Resource.url_user_agreement;

        public static readonly string CookieTrackingTechnologiesPolicy =
            Resource.url_cookie_tracking_technologies_policy;
    }

    public class AppSettings
    {
        public const string CDN_Path = "CDN_PATH";
        public const string CDN_Path2 = "CDN_PATH2";
        public const string STATIC_CDN_PATH = "STATIC_CDN_PATH";
        public const string PAYMENT_GATEWAY_MODE = "PAYMENT_GATEWAY_MODE";
        public const string SESSION_ABTEST_ON = "SESSION_ABTEST_ON";
        public const string EmailServiceURL = "Email_Service_URI";
        public const string ClientCD = "ClientCD";
        public const string ClientSecret = "SourceAppSecret";
    }

    public static class DocExportAPIConstants
    {
        public const string DocUrlpostfix = "api/v1/Export";
        public const string DocPDF = "/PDF";
        public const string DocThumbnail = "/Thumbnail";
        public const string ThumbnailPageCount = "/ThumbnailPageCount";
        public const string PageWiseThumbnail = "/PageWiseThumbnail";
        public const string WordDoc = "/WordDoc";
        public const string UrlSeparator = "/";
    }

    public static class DetectRightConstants
    {
        public const string MobileBrowser = "mobile_browser";
        public const string MobileBrowserVersion = "mobile_browser_version";
        public const string DeviceOS = "device_os";
        public const string DeviceOSVersion = "device_os_version";
        public const string DeviceModelName = "model_name";
        public const string DeviceType = "device_type";
        public const string DTDeviceType = "DTDeviceType";
        public const string IsSmallScreen = "is_small_screen";
        public const string IsTablet = "is_tablet";
        public const string EngineName = "EngineName";
        public const string EngineVersion = "EngineVersion";
    }

    public class RegexPatterns
    {
        public const string PositiveNumbers = "^[0-9]+$";
        public const string Email = @"^[A-Za-z0-9_][A-Za-z0-9._%+-]+@[A-Za-z0-9.-]+\.[A-Za-z]{2,4}$";
        public const string PhoneNumber = @"^[+]*[(]{0,1}[0-9]{1,4}[)]{0,1}[-\s\./0-9]*$";
        public const string ZipCode = @"^[a-z0-9\\-\\s]+$";
        public const string WebAddress = @"(http://([\w-]+\.)|([\w-]+\.))+[\w-]*(/[\w- ./?%=]*)?";
    }

    public class CountryCode
    {
        public const string Australia = "AU";
        public const string Canada = "CA";
        public const string NewZealand = "NZ";
        public const string UnitedStates = "US";
        public const string Maxico = "MX";
    }

    internal class HashConstants
    {
        public const int SALT_BYTES = 24;
        public const int HASH_BYTES = 24;
        public const int PBKDF2_ITERATIONS = 2000;
        public const int ITERATION_INDEX = 0;
        public const int SALT_INDEX = 1;
        public const int PBKDF2_INDEX = 2;
    }

    public enum AppPages
    {
        Login = 1,
        Funnel = 2
    }

    public enum PlanTypes
    {
        Freemium = 1,
        Basic,
        Professional,
        Premium
    }
}