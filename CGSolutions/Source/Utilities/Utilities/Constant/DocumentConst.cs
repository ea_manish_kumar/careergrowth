﻿//This File contains definitions for all Constants related to the Documents

namespace Utilities.Constants.Documents
{
    /// <summary>
    /// TODO:comments missing
    /// </summary>
    public class DocumentTypeCD
    {
        /// <summary>
        /// TODO:comments missing
        /// </summary>
        public const string Resume = "RSME";

        /// <summary>
        /// TODO:comments missing
        /// </summary>
        public const string Letter = "LETR";

        /// <summary>
        /// TODO:comments missing
        /// </summary>
        public const string Profile = "PRFL";
    }


    /// <summary>
    /// TODO:comments missing
    /// </summary>
    public class DocumentStatusTypeCD
    {
        /// <summary>
        /// TODO:comments missing
        /// </summary>
        public const string Active = "ACTV";

        /// <summary>
        /// TODO:comments missing
        /// </summary>
        public const string InActive = "INAC";

        /// <summary>
        /// TODO:comments missing
        /// </summary>
        public const string Archived = "ARCH";

        /// <summary>
        /// Document Deleted
        /// </summary>
        public const string Deleted = "DELE";
    }

    /// <summary>
    /// TODO:comments missing
    /// </summary>
    public class DocMediaTypeCD
    {
        /// <summary>
        /// TODO:comments missing
        /// </summary>
        public const string ProfilePhoto = "PRPH";
    }


    /// <summary>
    /// TODO:comments missing
    /// </summary>
    public class SectionTypeCD
    {
        public const string Accomplishments = "ACCM";
        public const string Additional_Information = "ADDI";
        public const string Affiliations = "AFIL";
        public const string Awards = "AWAR";
        public const string Body = "BODY";
        public const string Certifications = "CERT";
        public const string Closer = "CLSR";
        public const string Contact = "CNTC";
        public const string CalltoAction = "CTAC";
        public const string Date = "DATE";
        public const string Dissertation = "DIST";
        public const string Education = "EDUC";
        public const string ExtraCurricular = "EXCL";
        public const string Experience = "EXPR";
        public const string Volunteer = "VLTR";
        public const string Greeting = "GRTG";
        public const string Skills = "HILT";
        public const string Interests = "INTR";
        public const string Language = "LANG";
        public const string Name = "NAME";
        public const string Opener = "OPEN";
        public const string Other = "OTHR";
        public const string PersonalInformation = "PRIN";
        public const string Presentations = "PRSN";
        public const string Portfolio = "PTFL";
        public const string Publication = "PUBL";
        public const string Recipient = "RCNT";
        public const string References = "REFE";
        public const string Subject = "SUBJ";
        public const string Summary = "SUMM";
        public const string Training = "TRNG";
        public const string SocialService = "SCSV";
        public const string Objectives = "";
        public const string Highlights = "HILT";
    }

    /// <summary>
    /// TODO:comments missing
    /// </summary>
    public class LetterSectionTypeCD
    {
        /// <summary>
        /// TODO:comments missing
        /// </summary>
        public const string Name = "NAME";

        /// <summary>
        /// TODO:comments missing
        /// </summary>
        public const string Contact = "CNTC";

        /// <summary>
        /// TODO:comments missing
        /// </summary>
        public const string Subject = "SUBJ";

        /// <summary>
        /// TODO:comments missing
        /// </summary>
        public const string Greeting = "GRTG";

        /// <summary>
        /// TODO:comments missing
        /// </summary>
        public const string Opener = "OPEN";

        /// <summary>
        /// TODO:comments missing
        /// </summary>
        public const string Body = "BODY";

        /// <summary>
        /// TODO:comments missing
        /// </summary>
        public const string CallToAction = "CTAC";

        /// <summary>
        /// TODO:comments missing
        /// </summary>
        public const string Closer = "CLSR";

        /// <summary>
        /// RecipientContact
        /// </summary>
        public const string RecipientContact = "RCNT";

        /// <summary>
        /// Date Section
        /// </summary>
        public const string Date = "DATE";
    }

    /// <summary>
    /// TODO:comments missing
    /// </summary>
    public class DocRelationTypeCD
    {
        /// <summary>
        /// Signifies a BASED relation. for e.g. A Profile is BASED on a specific Resume
        /// </summary>
        public const string BASE = "BASE";

        /// <summary>
        /// Signifies a LINKED relation
        /// </summary>
        public const string LINK = "LINK";

        /// <summary>
        /// Resume Review
        /// </summary>
        public const string REVIEW = "RRVW";
    }

    /// <summary>
    /// Constant for CV group of countries
    /// </summary>
    
    public static class Constant
    {
        /// <summary>
        /// 
        /// </summary>
        public const int RESUMEWRITING_SKU_ENTRY_LAVEL = 10036;

        /// <summary>
        /// 
        /// </summary>
        public const int RESUMEWRITING_SKU_ENTRY_PROFESSIONAL = 10037;

        /// <summary>
        /// 
        /// </summary>
        public const int RESUMEWRITING_SKU_ENTRY_EXECUTIVE = 10038;


        public const int COVERLETTERWRITING_SKU = 10052;
        public const int COVERLETTERWRITING_SKU_60 = 10039;


        //public static string _sWebRoot = "http://localhost/";

        public const string RESUME_MARKETER = "RSMR";

        /// <summary>
        /// TODO:comments missing
        /// </summary>
        public static string[] _sArrCVGroupCountryAbbr = { "GB", "NZ", "ZA", "UK" };
    }

    /// <summary>
    /// Constants for Doc PreferenceType CDs
    /// </summary>
    public class DocPreferenceTypeCD
    {
        public const string ResumeUploaded = "RUPL";

        public const string ParsedSection = "PRSC";

        public const string DocumentCopied = "DCOP";

        public const string CoverLetterTemplateCategoryTypeCD = "CLTC";

        public const string CoverLetterTemplateCD = "CLTO";
        public const string CoverLetterTemplateSection = "CLTS";

        public const string DocumentFormattingRemoved = "DFRM";
    }


    public class LetterTypes
    {
        public const string CoverLetter = "COVL";
        public const string BusinessLetter = "BUSL";
        public const string ThankYouLetter = "THYL";

        public static class CoverLetters
        {
            public const string GeneralCoverLetter = "GNCL";
            public const string CareerChange = "CRCH";
            public const string LetterOfInterest = "LTIN";
            public const string Networking = "NETW";
            public const string ReferenceRequest = "RFRQ";
            public const string Internship = "INTP";
            public const string MilitaryTransition = "MILT";
            public const string RecruiterContact = "RCCT";
            public const string ReturningToWorkForce = "RTWF";
        }

        public static class BusinessLetters
        {
            public const string GeneralBusinessLetter = "GNBL";
            public const string JobAcceptance = "JBAC";
            public const string Resignation = "RESN";
            public const string SalaryRequirements = "SLRQ";
            public const string RejectionResponse = "RJRS";
            public const string SalaryNegotiation = "SALN";
        }
    }    
    
    public class DocFileTypeCD
    {
        /// <summary>
        /// Thumbnail
        /// </summary>
        public const string ThumbNail = "THMB";

        /// <summary>
        /// Export
        /// </summary>
        public const string Exported = "EXPT";

        /// <summary>
        /// Attached
        /// </summary>
        public const string Attached = "ATCH";

        /// <summary>
        /// Review PDF
        /// </summary>
        public const string ReviewPDF = "RPDF";

        /// <summary>
        /// Original
        /// </summary>
        public const string Original = "ORIG";

        /// <summary>
        /// Small Preview
        /// </summary>
        public const string SmallPreview = "SPRV";

        /// <summary>
        /// Large Preview
        /// </summary>
        public const string LargePreview = "LPRV";

        /// <summary>
        /// WaterMarked Preview
        /// </summary>
        public const string WaterMarked = "WPRV";
    }

    /// <summary>
    /// Field Response Codes
    /// </summary>
    public class FieldResponseCD
    {
        /// <summary>
        /// 
        /// </summary>
        public const string FullTime = "Full Time";

        public const string PartTime = "Part Time";
        public const string Employee = "Employee";
        public const string TemporaryContract = "Contract";
        public const string Internship = "Intern";
    }


    /// <summary>
    /// Field Codes
    /// </summary>
    public class FieldTypeCD
    {
        /// <summary>
        /// 
        /// </summary>
        public const string FreeFormat = "FRFM";

        /// <summary>
        /// 
        /// </summary>
        public const string FirstName = "FNAM";

        /// <summary>
        /// 
        /// </summary>
        public const string LastName = "LNAM";

        /// <summary>
        /// 
        /// </summary>
        public const string Street = "STRT";

        /// <summary>
        /// 
        /// </summary>
        public const string City = "CITY";

        /// <summary>
        /// 
        /// </summary>
        public const string State = "STAT";

        /// <summary>
        /// 
        /// </summary>
        public const string ZipCode = "ZIPC";

        /// <summary>
        /// 
        /// </summary>
        public const string Country = "CNTY";

        /// <summary>
        /// 
        /// </summary>
        public const string Email = "EMAI";

        /// <summary>
        /// 
        /// </summary>
        public const string HomePhone = "HPHN";

        /// <summary>
        /// 
        /// </summary>
        public const string AltPhone = "APHN";

        /// <summary>
        /// 
        /// </summary>
        public const string CellPhone = "CPHN";

        /// <summary>
        /// 
        /// </summary>
        public const string JobStartDate = "JSTD";

        /// <summary>
        /// 
        /// </summary>
        public const string JobEndDate = "EDDT";

        /// <summary>
        /// 
        /// </summary>
        public const string Company = "COMP";

        /// <summary>
        /// 
        /// </summary>
        /// 
        public const string CompanyLocation = "COLO";

        /// <summary>
        /// 
        /// </summary>
        public const string JobTitle = "JTIT";

        /// <summary>
        /// 
        /// </summary>
        public const string JobCity = "JCIT";

        /// <summary>
        /// 
        /// </summary>
        public const string JobState = "JSTA";

        /// <summary>
        /// 
        /// </summary>
        public const string JobDescription = "JDES";

        /// <summary>
        /// 
        /// </summary>
        public const string DegreeEarned = "DGRE";

        /// <summary>
        /// 
        /// </summary>
        public const string FieldOfExpertise = "STUY";

        /// <summary>
        /// 
        /// </summary>
        public const string GradePointAverage = "GRPA";

        /// <summary>
        /// 
        /// </summary>
        public const string GraduationYear = "GRYR";

        /// <summary>
        /// 
        /// </summary>
        public const string SchoolCity = "SCIT";

        /// <summary>
        /// 
        /// </summary>
        public const string SchoolCountry = "SCNT";

        /// <summary>
        /// 
        /// </summary>
        public const string SchoolState = "SSTA";

        /// <summary>
        /// 
        /// </summary>
        public const string SchoolName = "SCHO";

        /// <summary>
        /// 
        /// </summary>
        public const string University = "UNIV";

        /// <summary>
        /// 
        /// </summary>
        public const string CGPA = "CGPA";

        /// <summary>
        /// 
        /// </summary>
        public const string DegreeSpecialization = "DSPC";

        /// <summary>
        /// 
        /// </summary>
        public const string Skill = "SKIL";

        /// <summary>
        /// 
        /// </summary>
        public const string SkillLastUsed = "SLST";

        /// <summary>
        /// 
        /// </summary>
        public const string SkillRating = "SKRT";

        /// <summary>
        /// 
        /// </summary>
        public const string NumberOfYearsSkillUsed = "SKYR";

        /// <summary>
        /// 
        /// </summary>
        public const string SkillsCollection_1 = "SKC1";

        /// <summary>
        /// 
        /// </summary>
        public const string SkillsCollection_2 = "SKC2";


        /// <summary>
        /// 
        /// </summary>
        public const string CareerField = "CRFL";

        /// <summary>
        /// 
        /// </summary>
        public const string CareerSpecailaity = "SPCL";

        /// <summary>
        /// 
        /// </summary>
        public const string TargetSalaryRange = "SLRN";

        /// <summary>
        /// 
        /// </summary>
        public const string HourlySalaryRange = "HSLR";

        /// <summary>
        /// 
        /// </summary>
        public const string JobType = "JTYP";


        /// <summary>
        /// 
        /// </summary>
        public const string EmploymentType = "ETYP";

        /// <summary>
        /// 
        /// </summary>
        public const string Location = "LOCN";

        /// <summary>
        /// 
        /// </summary>
        public const string Relocate = "RELC";

        /// <summary>
        /// 
        /// </summary>
        public const string AuthorizeToWork = "AUTW";

        /// <summary>
        /// 
        /// </summary>
        public const string Telecommute = "TCOM";

        /// <summary>
        /// 
        /// </summary>
        public const string Alerts = "ALRT";

        /// <summary>
        /// 
        /// </summary>
        public const string HeadLine = "HDLN";

        /// <summary>
        /// 
        /// </summary>
        public const string Summary = "SUMM";

        /// <summary>
        /// 
        /// </summary>
        public const string Website = "WEBS";

        /// <summary>
        /// 
        /// </summary>
        public const string CareerLevel = "CRLV";

        /// <summary>
        /// 
        /// </summary>
        public const string HighestLevelOfEducation = "HLED";

        /// <summary>
        /// 
        /// </summary>
        public const string USMilitryVeteran = "USML";

        /// <summary>
        /// 
        /// </summary>
        public const string FromDate = "FRDT";

        /// <summary>
        /// 
        /// </summary>
        public const string ToDate = "TODT";

        /// <summary>
        /// 
        /// </summary>
        public const string Accomplishments = "ACCM";


        /// <summary>
        /// 
        /// </summary>
        public const string SchoolLocation = "SCLO";


        /// <summary>
        /// 
        /// </summary>
        public const string Subject = "SUBJ";

        /// <summary>
        /// 
        /// </summary>
        public const string USSecurityClearence = "SCLR";

        /// <summary>
        /// 
        /// </summary>
        public const string CertificationName = "CERT";

        /// <summary>
        /// 
        /// </summary>
        public const string InstituteName = "INST";

        /// <summary>
        /// 
        /// </summary>
        public const string CertificationDateReceived = "DATE";

        /// <summary>
        /// 
        /// </summary>
        public const string EmploymentStatus = "EMPS";

        /// <summary>
        /// 
        /// </summary>
        public const string NewJobStart = "NJST";

        /// <summary>
        /// 
        /// </summary>
        public const string Language = "LNGU";

        public const string Language1 = "LNG1";
        public const string Language2 = "LNG2";
        public const string Language3 = "LNG3";

        /// <summary>
        /// 
        /// </summary>
        public const string SkillName = "SKIL";

        /// <summary>
        /// 
        /// </summary>
        public const string Proficiencylevel = "PRLV";

        /// <summary>
        /// 
        /// </summary>
        public const string SkillLevel = "SKLV";

        /// <summary>
        /// 
        /// </summary>
        public const string BestTimeToContact = "BSCN";

        /// <summary>
        /// 
        /// </summary>
        public const string Keyword = "KWRD";

        /// <summary>
        /// 
        /// </summary>
        public const string CertificationSummary = "CSUM";

        /// <summary>
        /// Resume Title
        /// </summary>
        public const string ResumeTitle = "RTTL";

        /// <summary>
        /// Resume Main Title
        /// </summary>
        public const string MainTitle = "MTTL";

        /// <summary>
        /// Sub Title 1
        /// </summary>
        public const string SubTitle1 = "TTL1";

        /// <summary>
        /// Sub Title 2
        /// </summary>
        public const string SubTitle2 = "TTL2";

        /// <summary>
        /// Sub Title 3
        /// </summary>
        public const string SubTitle3 = "TTL3";

        /// <summary>
        /// Company Description
        /// </summary>
        public const string CompanyDescription = "CDES";

        // Date Field
        public const string Date = "DATE";

        // Office Phone for recipient info section
        public const string OfficePhone = "OPHN";

        /// <summary>
        /// Other Section Description
        /// </summary>
        public const string Description = "DESC";
    }

    /// <summary>
    /// File Types
    /// </summary>
    public class FileTypeCD
    {
        /// <summary>
        /// JPEG FIle
        /// </summary>
        public const string JPEG = "JPEG";

        /// <summary>
        /// PNG FIle
        /// </summary>
        public const string PNG = "PNG";

        /// <summary>
        /// PDF
        /// </summary>
        public const string PDF = "PDF";

        /// <summary>
        /// Word
        /// </summary>
        public const string Word = "DOCX";

        /// <summary>
        /// Text
        /// </summary>
        public const string Text = "TXT";

        /// <summary>
        /// html
        /// </summary>
        public const string HTML = "HTML";

        /// <summary>
        /// .htm
        /// </summary>
        public const string HTM = "HTM";

        /// <summary>
        /// GIF FIle
        /// </summary>
        public const string GIF = "GIF";
    }

    /// <summary>
    /// Detect OS
    /// </summary>
    public static class OSPlatform
    {
        public const int WINDOWS = 1;
        public const int MAC = 2;
    }

    /// <summary>
    /// TODO:comments missing
    /// </summary>
    public class TemplateCategoryTypeCD
    {
        /// <summary>
        /// TODO:comments missing
        /// </summary>
        public const string WorkHistory = "WHTR";

        /// <summary>
        /// TODO:comments missing
        /// </summary>
        public const string Category_On_Its_Own = "COIO";

        /// <summary>
        /// TODO:comments missing
        /// </summary>
        public const string Career_Level = "CARL";

        /// <summary>
        /// TODO:comments missing
        /// </summary>
        public const string Career_Field = "CARF";

        /// <summary>
        /// TODO:comments missing
        /// </summary>
        public const string Other = "OTHR";
    }

    /// <summary>
    /// TODO:comments missing
    /// </summary>
    public class TemplateFormatTypeCD
    {
        /// <summary>
        /// TODO:comments missing
        /// </summary>
        public const string Functional = "FUNC";

        /// <summary>
        /// TODO:comments missing
        /// </summary>
        public const string Chronological = "CHRO";

        /// <summary>
        /// TODO:comments missing
        /// </summary>
        public const string Hybrid = "HYBR";
    }

    /// <summary>
    /// The different purposes for letters available
    /// </summary>
    public class LetterPurposeTypeCD
    {
        /// <summary>
        /// Auto Cover Letter
        /// </summary>
        public const string Auto = "ATLR";

        /// <summary>
        /// Auto Cover Letter
        /// </summary>
        public const string Recommended = "RCCM";

        /// <summary>
        /// Auto Cover Letter
        /// </summary>
        public const string Template = "TMPL";

        /// <summary>
        /// Standard Letter
        /// </summary>
        public const string Standard = "STND";

        /// <summary>
        /// Simple Email Cover Letter
        /// </summary>
        public const string Simple_Email = "SECL";
    }

    /// <summary>
    /// The different sizes (heights) in which a document can be exported as an Image
    /// </summary>
    public class ImageHeight
    {
        /// <summary>
        /// Extra Small = 200px
        /// </summary>
        public const short ExtraSmall = 200;

        /// <summary>
        /// Small = 275px
        /// </summary>
        public const short Small = 275;

        /// <summary>
        /// Medium = 300px
        /// </summary>
        public const short Medium = 300;

        /// <summary>
        /// Large = 375px
        /// </summary>
        public const short Large = 375;

        /// <summary>
        /// Extra Large = 600px
        /// </summary>
        public const short ExtraLarge = 600;
    }

    /// <summary>
    /// The different DocZones defined for a document
    /// </summary>
    public class DocZoneTypeCD
    {
        /// <summary>
        /// Document Header
        /// </summary>
        public const string HEAD = "HEAD";

        /// <summary>
        /// Document Body
        /// </summary>
        public const string BODY = "BODY";

        /// <summary>
        /// Document Footer
        /// </summary>
        public const string FOOTER = "FOOT";
    }
    
    public class Styles
    {
        public const string hMargin = "HMRG";
        public const string PageSize = "PGSZ";
        public const string FONTFACE = "FTFC";
        public const string FONTSIZE = "FTSZ";
        public const string FONTSIZEADDRESS = "ADFT";
        public const string FONTSIZENAME = "NMFT";
        public const string FONTFAMILYH1 = "H1FF";
        public const string FONTSIZEH1 = "H1FT";
        public const string FONTSIZEH2 = "H2FT";
        public const string FAICONSWIDTH = "FAWD";
        public const string BACKGROUNDGRAPHIC = "BGRD";
        public const string TOPBOTTOMMARGINS = "VMRG";
        public const string THEME = "THME";
        public const string SIDEMARGINS = "HMRG";
        public const string Default = "DB";

        //Colors Starts Here
        public const string BACKGROUNDSIDE = "BGSD";
        public const string BACKGROUNDPARENT = "BGPR";
        public const string BACKGROUNDFOOTER = "BGFT";
        public const string BACKGROUNDYEARCITY = "BGYC";
        public const string BACKGROUNDADDRESS = "BGAD";
        public const string COLORSIDE = "CLSD";
        public const string COLORH1 = "CLH1";
        public const string COLORH2 = "CLH2";
        public const string COLORPARENT = "CLPR";
        public const string COLORNAME = "CLNM";
        public const string COLORBORDER = "CLBR";
        public const string COLORLASTNAME = "CLLN";
        public const string COLORADDRESS = "CLAD";
        public const string COLORFOOTER = "CLFT";
        public const string COLORYEARCITY = "CLYC";
        public const string COLORADDRESSICONS = "CLAI";
    }
    
    public class ThemeCD
    {
        public const string Default = "Default";
        public const string Vibrant = "Vibrant";
        public const string Soothing = "Soothing";
        public const string Pleasant = "Pleasant";
        public const string Classic = "Classic";
    }
    
    public class SkinCode
    {
        public const string Basic = "BASC";
        public const string Cool = "COOL";
        public const string Professional = "PROF";
        public const string Smart = "SMRT";
        public const string StandOut = "STDO";
        public const string Soothing = "SOTG";
        public const string MultiColor = "MLTC";
        public const string Superb = "SPRB";
        public const string Awesome = "AWSM";
        public const string Artistic = "ARTS";
        public const string Executive = "EXEC";
        public const string Elegant = "ELEG";
    }
    
    public class DocTemplateTypeCD
    {
        public const string Default = "DEFA";
    }
    public class WizardConstants
    {
        /// Default Sections
        public const string HowItWorks = "hiwk";

        public const string ChooseTemplate = "chtm";
        public const string Contact = "cntc";
        public const string Register = "regt";
        public const string Experience = "expr";
        public const string Education = "educ";
        public const string Skills = "skil";
        public const string Portfolio = "ptfl";
        public const string Summary = "summ";

        /// Other Sections
        public const string Accomplishments = "accm";

        public const string Awards = "awrd";
        public const string Affiliations = "affi";
        public const string Certifications = "cert";
        public const string ExtraCurricular = "extc";
        public const string Language = "lang";
        public const string PersonalInformation = "pers";
        public const string References = "refe";
        public const string SocialService = "scls";
        public const string Other = "othr";
    }
}
