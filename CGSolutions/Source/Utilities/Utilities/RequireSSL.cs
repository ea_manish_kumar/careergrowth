﻿using System;

namespace Utilities
{
    /// <summary>
    ///     Attribute decorated on classes that use SSL
    /// </summary>
    [AttributeUsage(AttributeTargets.Class)]
    public sealed class RequireSSL : Attribute
    {
    }
}