﻿using System;
using System.Collections;
using System.Linq;
using System.Reflection;
using System.Text.RegularExpressions;
using CommonModules.Exceptions;
using Resources;

namespace Utilities
{
    /// <summary>
    ///     Class for different Resume Utilities
    /// </summary>
    public class ResumeUtilities
    {
        /// <summary>
        ///     Method for checking the Proper Resume Name
        /// </summary>
        /// <param name="ResumeName">Resume Name</param>
        /// <param name="fName">First Name</param>
        /// <param name="lName">Last Name</param>
        /// <returns>Returns true if the user has the proper Resume Name</returns>
        public static bool CheckProperResumeName(string ResumeName, string fName, string lName)
        {
            try
            {
                var resName = ResumeName.ToLower();
                var fInitial = string.Empty;
                var lInitial = string.Empty;
                if (!string.IsNullOrEmpty(fName)) fInitial = fName.ToLower()[0].ToString();

                if (!string.IsNullOrEmpty(lName)) lInitial = lName.ToLower()[0].ToString();

                if (!string.IsNullOrEmpty(resName))
                {
                    if (!string.IsNullOrEmpty(lName) && resName.Contains(lName.ToLower()) ||
                        !string.IsNullOrEmpty(fName) && resName.Contains(fName.ToLower()))
                    {
                        if (resName.Contains(lName.ToLower()))
                            resName = resName.Remove(resName.IndexOf(lName.ToLower()), lName.Length);
                        if (resName.Contains(fName.ToLower()))
                            resName = resName.Remove(resName.IndexOf(fName.ToLower()), fName.Length);
                        if (resName.Contains("resume") || resName.Contains("res") || resName.Contains("cv") ||
                            resName.Contains("c.v."))
                        {
                            resName = RemoveResumeWords(resName);
                            if
                            (!string.IsNullOrEmpty(fName) &&
                             (resName.Contains(fName.ToLower()) || resName.Contains(fInitial) ||
                              ResumeName.Contains(fInitial.ToUpper())) ||
                             !string.IsNullOrEmpty(lName) &&
                             (resName.Contains(lName.ToLower()) || resName.Contains(lInitial) ||
                              ResumeName.Contains(lInitial.ToUpper())))
                                return true;
                            return false;
                        }

                        return false;
                    }

                    return false;
                }

                return false;
            }
            catch (Exception ex)
            {
                var ht = new Hashtable();
                ht.Add("Resume Name: ", ResumeName);
                ht.Add("First Name: ", fName);
                ht.Add("Last Name: ", lName);
                var objMethod = (MethodInfo) MethodBase.GetCurrentMethod();
                GlobalExceptionHandler.customExceptionHandler(ex, objMethod, ht, "", "", ErrorSeverityType.Low,
                    "Error in checking Proper Resume Name", false);
            }

            return false;
        }

        /// <summary>
        ///     Method for Checking whether the Resume has proper length or not
        /// </summary>
        /// <param name="resText">Resume in Text Format</param>
        /// <returns>Returns true if Resume has proper Length</returns>
        public static bool CheckProperResumeLength(string resText)
        {
            if (!string.IsNullOrEmpty(resText))
            {
                var rText = resText.Replace("  ", " ");
                float numWords = rText.Count(f => f == ' ') + 1;
                var resPages = numWords / 350;
                if (resPages > 2.0f)
                    return false;
                return true;
            }

            return true;
        }

        /// <summary>
        ///     Method to check the minimum number of words in Resume
        /// </summary>
        /// <param name="resText">Resume in Text Format</param>
        /// <returns>Returns true if Resume has minimum number of words</returns>
        public static bool CheckMinimumResumeLength(string resText)
        {
            if (!string.IsNullOrEmpty(resText))
            {
                var rText = resText.Replace("  ", " ");
                float numWords = rText.Count(f => f == ' ') + 1;
                if (numWords < 100.0f)
                    return false;
                return true;
            }

            return true;
        }


        /// <summary>
        ///     Method for checking whether Resume has same Format Dates
        /// </summary>
        /// <param name="startDate">Start Date</param>
        /// <param name="endDate">End Date</param>
        /// <param name="formatCount">Output object for storing words in existing Date Format</param>
        /// <returns>Returns true if the Start Date and end Date has same format</returns>
        public static bool CheckProperResumeDates(string startDate, string endDate, out int formatCount)
        {
            string[] separators = {" ", "/", "-", "\\", ","};
            formatCount = 0;
            if (!string.IsNullOrEmpty(startDate) && !string.IsNullOrEmpty(endDate))
            {
                var sDate = startDate.Split(separators, StringSplitOptions.RemoveEmptyEntries);
                var eDate = endDate.Split(separators, StringSplitOptions.RemoveEmptyEntries);
                if (formatCount == 0)
                {
                    formatCount = sDate.Length;
                }
                else
                {
                    if (formatCount != sDate.Length || formatCount != eDate.Length)
                        if (endDate.ToLower() != Resource.Present.ToLower() &&
                            endDate.ToLower() != Resource.Current.ToLower() &&
                            endDate.ToLower() != Resource.TillNow.ToLower())
                            return false;
                }

                if (formatCount == 1)
                {
                    if (Regex.IsMatch(startDate, @"(19|20)\d\d") && Regex.IsMatch(endDate, @"(19|20)\d\d")) return true;

                    if (endDate.ToLower() != Resource.Present.ToLower() &&
                        endDate.ToLower() != Resource.Current.ToLower() &&
                        endDate.ToLower() != Resource.TillNow.ToLower()) return false;
                    return true;
                }

                if (formatCount != sDate.Length || formatCount != eDate.Length)
                {
                    if (endDate.ToLower() != Resource.Present.ToLower() &&
                        endDate.ToLower() != Resource.Current.ToLower() &&
                        endDate.ToLower() != Resource.TillNow.ToLower()) return false;
                    return true;
                }

                return true;
            }

            return false;
        }

        public static string RemoveResumeWords(string resumeName)
        {
            string[] words = {"resume", "res", "cv", "c.v."};
            foreach (var word in words)
                if (resumeName.Contains(word))
                    resumeName = resumeName.Remove(resumeName.IndexOf(word), word.Length);
            return resumeName;
        }
    }
}