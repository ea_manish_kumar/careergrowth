﻿namespace App.Model.Filters
{
    public class UserFilter
    {
        public static UserFilter Empty = new UserFilter { UserId = -1 };

        public int UserId { get; set; }
    }
}