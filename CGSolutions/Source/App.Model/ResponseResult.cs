﻿namespace App.Model
{
    public class ResponseData<T>
    {
        public ResponseCode ResponseCode { get; set; }
        public T Data { get; set; }
        public bool IsSuccess { get { return ResponseCode.Equals(ResponseCode.Ok); } }
    }

    public enum ResponseCode : int
    {
        Ok = 200,
        BadRequest = 400,
        NotExists = 404
    }
}