﻿namespace App.Model.Validation
{
    public class ValidationResult
    {
        public bool IsValid { get; set; }
    }
}