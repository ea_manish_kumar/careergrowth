﻿namespace App.Model.Validation
{
    interface IValidate
    {
        bool IsValid();
        ValidationResult Validate();
    }
}