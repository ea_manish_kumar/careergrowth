﻿using Newtonsoft.Json;
using System.Collections.Generic;

namespace App.Model.Documents
{
    public class DocumentDTO
    {
        [JsonProperty("id")]
        public int Id { get; set; }

        [JsonProperty("sections")]
        public IEnumerable<SectionDTO> Sections { get; set; }
    }
}