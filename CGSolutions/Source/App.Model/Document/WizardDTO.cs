﻿using System.Collections.Generic;

namespace App.Model.Documents
{
    public class WizardDTO
    {
        public bool LoadData { get; set; }

        public int DocumentId { get; set; }

        public int SectionId { get; set; }

        public short SectionSortIndex { get; set; }

        public string SectionName { get; set; }

        public int SubSectionCount { get; set; }

        public int SubSectionId { get; set; }

        public bool ValidateSubSection { get; set; }

        public string NextRouteUrl { get; set; }

        public string ReviewRouteUrl { get; set; }

        public string BackRouteUrl { get; set; }

        public string GetSectionUrl { get; set; }

        public string SaveSectionUrl { get; set; }

        public string SortSubSectionsUrl { get; set; }

        public DocumentCookieDTO DocumentCookieDTO { get; set; }

        public IList<WizardMenuDTO> MenuRoutes { get; set; }
    }

    public class WizardMenuDTO
    {
        public string Route { get; set; }
        public string Icon { get; set; }
        public string SectionType { get; set; }
        public string SectionLabel { get; set; }
        public string SectionUrl { get; set; }
        public bool HasReviewScreen { get; set; }
        public bool RequireSubSection { get; set; }
        public bool IsMultiSubSection { get; set; }
        public short SortIndex { get; set; }

        public WizardMenuDTO Clone()
        {
            return new WizardMenuDTO
            {
                Route = Route,
                Icon = Icon,
                SectionType = SectionType,
                SectionLabel = SectionLabel,
                SectionUrl = SectionUrl,
                HasReviewScreen = HasReviewScreen,
                RequireSubSection = RequireSubSection,
                IsMultiSubSection = IsMultiSubSection,
                SortIndex = SortIndex
            };
        }
    }
}