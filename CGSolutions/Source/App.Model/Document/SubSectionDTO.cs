﻿using App.DomainModel;
using App.Model.Validation;
using App.Utility;
using App.Utility.Constants;
using App.Utility.Extensions;
using Newtonsoft.Json;
using System.Collections.Generic;
using System.Linq;

namespace App.Model.Documents
{
    public class SubSectionDTO : IValidate
    {
        public SubSectionDTO()
        {
        }

        [JsonProperty("id")]
        public int Id { get; set; }

        [JsonProperty("sectionId")]
        public int SectionId { get; set; }

        [JsonProperty("sortIndex")]
        public short SortIndex { get; set; }

        [JsonProperty("isDeleted")]
        public bool IsDeleted { get; set; }

        [JsonProperty("documentDatas")]
        public IList<DocumentDataDTO> DocumentDatas { get; set; }

        [JsonIgnore]
        public SectionDTO Section { get; set; }

        [JsonProperty("editAddress")]
        public string EditUrl { get; set; }

        [JsonProperty("deleteAddress")]
        public string DeleteUrl { get; set; }

        [JsonIgnore]
        public bool IsEmpty
        {
            get
            {
                return DocumentDatas.IsNullOrEmpty() || DocumentDatas.All(d => d.IsEmpty);
            }
        }

        public SubSection ToSubSection()
        {
            return new SubSection
            {
                Id = Id,
                SectionId = SectionId,
                SortIndex = SortIndex,
                DocumentDatas = DocumentDatas.Where(dd => !dd.IsEmpty).Select(dd => dd.ToDocumentData()).ToList()
            };
        }

        public void Initialize(IEnumerable<Field> fields)
        {
            if (DocumentDatas.IsNotNullOrEmpty())
            {
                foreach (var documentData in DocumentDatas)
                    documentData.Initialize(fields);
            }
        }

        public void Initialize(SubSection dbSubSection, IEnumerable<Field> fields)
        {
            Id = dbSubSection.Id;
            SectionId = dbSubSection.SectionId;
            SortIndex = dbSubSection.SortIndex;

            if (dbSubSection.DocumentDatas.IsNotNullOrEmpty())
            {
                DocumentDatas = new List<DocumentDataDTO>();
                foreach (var dbDocumentData in dbSubSection.DocumentDatas)
                {
                    var documentData = new DocumentDataDTO() { SubSection = this };
                    documentData.Initialize(dbDocumentData, fields);
                    DocumentDatas.Add(documentData);
                }
                SetCustomLocationField();
                SetCustomEducationFields();
            }
        }

        private void SetCustomLocationField()
        {
            if (Section != null && (Strings.Equals(Section.Name, SectionTypes.Experience) || Strings.Equals(Section.Name, SectionTypes.Education)))
            {
                string city = null, state = null;
                foreach (var documentData in DocumentDatas)
                {
                    if (Strings.Equals(documentData.Name, DocumentFieldTypes.City) && Strings.IsNotNullOrEmpty(documentData.Value))
                        city = documentData.Value;
                    if (Strings.Equals(documentData.Name, DocumentFieldTypes.State) && Strings.IsNotNullOrEmpty(documentData.Value))
                        state = documentData.Value;
                }
                if (Strings.IsNotNullOrEmpty(city) || Strings.IsNotNullOrEmpty(state))
                    DocumentDatas.Add(new DocumentDataDTO
                    {
                        Name = CustomDocumentFieldTypes.Location,
                        Value = string.Format("({0})", string.Join(", ", city, state))
                    });
            }
        }

        private void SetCustomEducationFields()
        {
            if (Strings.Equals(Section.Name, SectionTypes.Education))
            {
                string institute = null, university = null;
                foreach (var documentData in DocumentDatas)
                {
                    if (Strings.Equals(documentData.Name, DocumentFieldTypes.InstitutionName) && Strings.IsNotNullOrEmpty(documentData.Value))
                        institute = documentData.Value;
                    if (Strings.Equals(documentData.Name, DocumentFieldTypes.University) && Strings.IsNotNullOrEmpty(documentData.Value))
                        university = documentData.Value;
                }
                if (Strings.IsNotNullOrEmpty(institute) || Strings.IsNotNullOrEmpty(university))
                    DocumentDatas.Add(new DocumentDataDTO
                    {
                        Name = CustomDocumentFieldTypes.Institutions,
                        Value = string.Format("{0}", string.Join(", ", institute, university))
                    });
                string degree = null, specialization = null;
                foreach (var documentData in DocumentDatas)
                {
                    if (Strings.Equals(documentData.Name, DocumentFieldTypes.Degree) && Strings.IsNotNullOrEmpty(documentData.Value))
                        degree = documentData.Value;
                    if (Strings.Equals(documentData.Name, DocumentFieldTypes.DegreeSpecialization) && Strings.IsNotNullOrEmpty(documentData.Value))
                        specialization = documentData.Value;
                }
                if (Strings.IsNotNullOrEmpty(degree) || Strings.IsNotNullOrEmpty(specialization))
                    DocumentDatas.Add(new DocumentDataDTO
                    {
                        Name = CustomDocumentFieldTypes.DegreeDetails,
                        Value = string.Format("{0}", string.Join(", ", degree, specialization))
                    });
            }
        }

        public bool IsValid()
        {
            return DocumentDatas.IsNotNullOrEmpty() && DocumentDatas.All(sb => sb.IsValid());
        }

        public ValidationResult Validate()
        {
            throw new System.NotImplementedException();
        }
    }
}