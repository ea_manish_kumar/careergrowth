﻿using App.DomainModel;
using App.Utility.Extensions;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;

namespace App.Model.Documents
{
    public class DocumentCookieDTO
    {
        [JsonProperty("ts")]
        public string Timestamp { get; set; }

        [JsonProperty("sk")]
        public string SkinCode { get; set; }

        [JsonProperty("did")]
        public int DocumentId { get; set; }

        [JsonProperty("onv")]
        public IList<string> OtherSectionsNav { get; set; }

        [JsonProperty("olb")]
        public string OtherSectionLabel { get; set; }


        [JsonProperty("sec")]
        private IDictionary<string, int?> Sections { get; set; }

        [JsonProperty("asc")]
        private IList<string> AdditionalSections { get; set; }

        public IList<string> GetAdditionalSections()
        {
            return AdditionalSections;
        }

        public IList<string> GetSections()
        {
            return Sections.Keys.ToList();
        }

        public void AddSection(string sectionCD)
        {
            AddSection(sectionCD, null);
        }

        public void AddSection(string sectionCD, int? paragraphCount)
        {
            if (Sections == null)
                Sections = new Dictionary<string, int?>();
            if (HasSection(sectionCD))
                Sections[sectionCD] = paragraphCount;
            else
                Sections.Add(sectionCD, paragraphCount);
        }

        public bool HasSection(string sectionCD)
        {
            if (!string.IsNullOrEmpty(sectionCD) && Sections != null)
                return Sections.ContainsKey(sectionCD);
            return false;
        }

        public bool HasSection(string sectionCD, out bool hasSubSection)
        {
            hasSubSection = false;
            if (!string.IsNullOrEmpty(sectionCD) && Sections != null && Sections.ContainsKey(sectionCD))
            {
                hasSubSection = Sections[sectionCD].HasValue && Sections[sectionCD].Value > 0;
                return true;
            }
            return false;
        }

        public bool HasSection(string sectionCD, out int subSectionCount)
        {
            subSectionCount = 0;
            if (!string.IsNullOrEmpty(sectionCD) && Sections != null && Sections.ContainsKey(sectionCD))
            {
                if (Sections[sectionCD].HasValue)
                    subSectionCount = Sections[sectionCD].Value;
                return true;
            }
            return false;
        }

        public bool HasSubSection(string sectionCD)
        {
            return !string.IsNullOrEmpty(sectionCD) && Sections != null
                && Sections.TryGetValue(sectionCD, out int? subSectionCount)
                && subSectionCount.HasValue && subSectionCount.Value > 0;
        }

        public void RemoveSection(string sectionCD)
        {
            if (Sections != null && HasSection(sectionCD))
                Sections.Remove(sectionCD);
        }

        public void AddAdditionalSection(string additionalSectionCD)
        {
            if (AdditionalSections == null)
                AdditionalSections = new List<string>();
            if (!string.IsNullOrEmpty(additionalSectionCD) && !AdditionalSections.Any(s =>
                    additionalSectionCD.Equals(s, StringComparison.OrdinalIgnoreCase)))
                AdditionalSections.Add(additionalSectionCD);
        }


        public string ToJsonString()
        {
            return JsonConvert.SerializeObject(this, Formatting.None, new JsonSerializerSettings { NullValueHandling = NullValueHandling.Ignore });
        }

        public static DocumentCookieDTO Parse(string documentCookie)
        {
            return JsonConvert.DeserializeObject<DocumentCookieDTO>(documentCookie, new JsonSerializerSettings { NullValueHandling = NullValueHandling.Ignore });
        }

        public static DocumentCookieDTO Parse(Document document, IEnumerable<SectionType> sectionTypes)
        {
            if (document != null)
            {
                var docCookieModel = new DocumentCookieDTO
                {
                    DocumentId = document.Id,
                    SkinCode = document.SkinCD,
                    Timestamp = (document.ModifiedOnUtc.HasValue ? document.ModifiedOnUtc.Value.Ticks : document.CreatedOnUtc.Ticks).ToString()
                };
                if (document.Sections.IsNotNullOrEmpty() && sectionTypes.IsNotNullOrEmpty())
                {
                    docCookieModel.Sections = new Dictionary<string, int?>();
                    foreach (var section in document.Sections)
                    {
                        var sectionType = sectionTypes.FirstOrDefault(s => s.Id == section.SectionTypeId);
                        if (sectionType != null)
                        {
                            if (section.SubSections.IsNotNullOrEmpty())
                                docCookieModel.AddSection(sectionType.Name, section.SubSections.Count);
                            else
                                docCookieModel.AddSection(sectionType.Name);
                        }
                    }
                }
                return docCookieModel;
            }

            return null;
        }
    }
}