﻿using App.DomainModel;
using App.Model.Validation;
using App.Utility;
using App.Utility.Extensions;
using Newtonsoft.Json;
using System.Collections.Generic;
using System.Linq;

namespace App.Model.Documents
{
    public class DocumentDataDTO : IValidate
    {
        public DocumentDataDTO()
        {
        }

        [JsonProperty("id")]
        public int Id { get; set; }

        [JsonProperty("subSectionId")]
        public int SubSectionId { get; set; }

        [JsonProperty("name")]
        public string Name { get; set; }

        [JsonProperty("fieldId")]
        public short FieldId { get; set; }

        [JsonProperty("value")]
        public string Value { get; set; }

        public DocumentData ToDocumentData()
        {
            return new DocumentData
            {
                Id = Id,
                FieldId = FieldId,
                SubSectionId = SubSectionId,
                Data = Value
            };
        }

        [JsonIgnore]
        public SubSectionDTO SubSection { get; set; }

        [JsonIgnore]
        public bool IsEmpty
        {
            get
            {
                return Strings.IsNullOrEmpty(Value);
            }
        }

        public void Initialize(IEnumerable<Field> fields)
        {
            if (fields.IsNotNullOrEmpty() && (FieldId <= 0 || Strings.IsNullOrEmpty(Name)))
            {
                Field field = null;
                if (FieldId > 0)
                    field = fields.FirstOrDefault(f => f.Id.Equals(FieldId));
                else if (Strings.IsNotNullOrEmpty(Name))
                    field = fields.FirstOrDefault(f => Strings.Equals(f.Name, Name));
                if (field != null)
                {
                    FieldId = field.Id;
                    Name = field.Name;
                }
            }
        }

        public void Initialize(DocumentData dbDocumentData, IEnumerable<Field> fields)
        {
            Id = dbDocumentData.Id;
            SubSectionId = dbDocumentData.SubSectionId;
            FieldId = dbDocumentData.FieldId;
            Name = dbDocumentData.Field?.Name;
            Value = dbDocumentData.Data;

            if (fields.IsNotNullOrEmpty() && (FieldId < 0 || Strings.IsNullOrEmpty(Name)))
            {
                Field field = null;
                if (FieldId > 0)
                    field = fields.FirstOrDefault(f => f.Id.Equals(FieldId));
                else if (Strings.IsNotNullOrEmpty(Name))
                    field = fields.FirstOrDefault(f => Strings.Equals(f.Name, Name));
                if (field != null)
                {
                    FieldId = field.Id;
                    Name = field.Name;
                }
            }
        }

        public bool IsValid()
        {
            return FieldId > 0;
        }

        public ValidationResult Validate()
        {
            throw new System.NotImplementedException();
        }
    }
}