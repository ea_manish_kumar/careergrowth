﻿using App.DomainModel;
using App.Model.Validation;
using App.Utility;
using App.Utility.Extensions;
using Newtonsoft.Json;
using System.Collections.Generic;
using System.Linq;

namespace App.Model.Documents
{
    public class SectionDTO : IValidate
    {
        public SectionDTO()
        {
        }

        [JsonProperty("id")]
        public int Id { get; set; }

        [JsonProperty("documentId")]
        public int DocumentId { get; set; }

        [JsonProperty("name")]
        public string Name { get; set; }

        [JsonProperty("sectionTypeId")]
        public short SectionTypeId { get; set; }

        [JsonProperty("sortIndex")]
        public short SortIndex { get; set; }

        [JsonProperty("label")]
        public string Label { get; set; }

        [JsonProperty("subSections")]
        public IList<SubSectionDTO> SubSections { get; set; }

        [JsonProperty("editAddress")]
        public string EditUrl { get; set; }

        [JsonProperty("deleteAddress")]
        public string DeleteUrl { get; set; }

        [JsonProperty("addSubSectionAddress")]
        public string AddSubSectionUrl { get; set; }

        [JsonProperty("sortSubSectionAddress")]
        public string SortSubSectionsUrl { get; set; }

        public Section ToSection()
        {
            return new Section
            {
                Id = Id,
                DocumentId = DocumentId,
                SectionTypeId = SectionTypeId,
                SortIndex = SortIndex,
                Label = Label,
                SubSections = SubSections.Where(sb => !sb.IsEmpty).Select(sb => sb.ToSubSection()).ToList()
            };
        }

        public void Initialize(Section section)
        {
            Initialize(section, null, null);
        }

        public void Initialize(IEnumerable<SectionType> sectionTypes, IEnumerable<Field> fields)
        {
            if (sectionTypes.IsNotNullOrEmpty() && (SectionTypeId <= 0 || Strings.IsNullOrEmpty(Name) || Strings.IsNullOrEmpty(Label)))
            {
                SectionType sectionType = null;
                if (SectionTypeId > 0)
                    sectionType = sectionTypes.FirstOrDefault(s => s.Id.Equals(SectionTypeId));
                else if (Strings.IsNotNullOrEmpty(Name))
                    sectionType = sectionTypes.FirstOrDefault(s => Strings.Equals(s.Name, Name));
                if (sectionType != null)
                {
                    SectionTypeId = sectionType.Id;
                    Name = sectionType.Name;
                    Label = Label ?? sectionType.Label;
                }
            }

            if (SubSections.IsNotNullOrEmpty())
            {
                foreach (var subSection in SubSections)
                    subSection.Initialize(fields);
            }
        }

        public void Initialize(Section section, IEnumerable<SectionType> sectionTypes, IEnumerable<Field> fields)
        {
            if (section != null)
            {
                Id = section.Id;
                DocumentId = section.DocumentId;
                SectionTypeId = section.SectionTypeId;
                SortIndex = section.SortIndex;
                Name = section.SectionType?.Name;
                Label = section.Label ?? section.SectionType?.Label;

                if (sectionTypes.IsNotNullOrEmpty() && (SectionTypeId <= 0 || Strings.IsNullOrEmpty(Name) || Strings.IsNullOrEmpty(Label)))
                {
                    SectionType sectionType = null;
                    if (SectionTypeId > 0)
                        sectionType = sectionTypes.FirstOrDefault(s => s.Id.Equals(SectionTypeId));
                    else if (Strings.IsNotNullOrEmpty(Name))
                        sectionType = sectionTypes.FirstOrDefault(s => Strings.Equals(s.Name, Name));
                    if (sectionType != null)
                    {
                        SectionTypeId = sectionType.Id;
                        Name = sectionType.Name;
                        Label = Label ?? sectionType.Label;
                    }
                }

                if (section.SubSections.IsNotNullOrEmpty())
                {
                    SubSections = new List<SubSectionDTO>();
                    foreach (var dbSubSection in section.SubSections.OrderBy(o => o.SortIndex))
                    {
                        var subSection = new SubSectionDTO() { Section = this };
                        subSection.Initialize(dbSubSection, fields);
                        SubSections.Add(subSection);
                    }
                }
            }
        }

        public bool IsValidForSubSectionSorting()
        {
            return Id > 0 && SubSections.IsNotNullOrEmpty() && SubSections.All(sb => sb.Id > 0 && !sb.IsDeleted);
        }

        public bool IsValid()
        {
            return DocumentId > 0 && SectionTypeId > 0 && (SubSections.IsNullOrEmpty() || SubSections.All(sb => sb.IsValid()));
        }

        public ValidationResult Validate()
        {
            throw new System.NotImplementedException();
        }
    }
}