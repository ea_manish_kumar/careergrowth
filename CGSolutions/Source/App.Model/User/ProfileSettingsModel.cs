﻿using App.DomainModel;
using System;
using System.Collections.Generic;

namespace App.Model.Users
{
    public class ProfileSettingsModel
    {
        public string Name { get; set; }
        public int AccountId { get; set; }
        public string EmailAddress { get; set; }
        public DateTime MemberSince { get; set; }
        public IList<Subscription> Subscriptions { get; set; }
        public string AccountType { get; set; }
        public bool EmailsSubscribed { get; set; }
    }
}