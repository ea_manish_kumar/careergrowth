﻿using Resources;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace App.Model.Users
{
    public class LoginModel
    {
        [Required]
        public string UserName { get; set; }

        [Required]
        public string Password { get; set; }

        public bool RememberMe { get; set; }
    }

    public class ChangePasswordModel : IValidatableObject
    {
        public string Password { get; set; }

        public string NewPassword { get; set; }

        public string ConfirmPassword { get; set; }

        public IEnumerable<ValidationResult> Validate(ValidationContext validationContext)
        {
            if (string.IsNullOrWhiteSpace(Password))
                yield return new ValidationResult(Resource.EnterCurrentPassword, new List<string> { "Password" });
            if (string.IsNullOrWhiteSpace(NewPassword))
                yield return new ValidationResult(Resource.EnterNewPassword, new List<string> { "NewPassword" });
            if (!string.Equals(NewPassword, ConfirmPassword, System.StringComparison.Ordinal))
                yield return new ValidationResult(Resource.DifferentPasswordMsg, new List<string> { "ConfirmPassword" });
            yield return ValidationResult.Success;
        }
    }

    public class ResetPasswordModel : IValidatableObject
    {
        public string Password { get; set; }

        public string ConfirmPassword { get; set; }

        public IEnumerable<ValidationResult> Validate(ValidationContext validationContext)
        {
            if (string.IsNullOrWhiteSpace(Password))
                yield return new ValidationResult(Resource.EnterCurrentPassword, new List<string> { "Password" });
            if (!string.Equals(Password, ConfirmPassword, System.StringComparison.Ordinal))
                yield return new ValidationResult(Resource.DifferentPasswordMsg, new List<string> { "ConfirmPassword" });
            yield return ValidationResult.Success;
        }
    }

    public class RegisterModel
    {
        public string Name { get; set; }

        [Required]
        public string UserName { get; set; }

        [Required]
        public string Password { get; set; }
    }

    public enum SignInResult
    {
        NotFound = 1,
        Deactivated = 2,
        LockedOut = 3,
        PasswordMismatch = 4,
        Success = 5,
    }

    public class SignInOptions
    {
        public int TokenExpireMinutes { get; set; }
        public int SessionExpireMinutes { get; set; }
        public int PersistentSessionExpireMinutes { get; set; }
        public int LockoutExpireMinutes { get; set; }
        public int MaxPasswordFailedAttempts { get; set; }
    }
}