﻿namespace App.DomainModel
{
    public interface ISeedEntity
    {
        bool IdentifierEquals(object entity);
        bool ValueEquals(object entity);
        void Merge(object entity);
    }
}