﻿namespace App.DomainModel
{
    public class BaseEntityMaster : ISeedEntity
    {
        public virtual short Id { get; set; }
        public virtual string Name { get; set; }
        public virtual string Label { get; set; }

        public virtual bool IdentifierEquals(object entity)
        {
            if (entity is BaseEntityMaster entityMaster)
                return string.Equals(Name, entityMaster.Name, System.StringComparison.OrdinalIgnoreCase);
            else
                return false;
        }

        public virtual bool ValueEquals(object entity)
        {
            if (entity is BaseEntityMaster entityMaster)
                return string.Equals(Name, entityMaster.Name, System.StringComparison.OrdinalIgnoreCase)
                && string.Equals(Label, entityMaster.Label, System.StringComparison.OrdinalIgnoreCase);
            else
                return false;
        }

        public virtual void Merge(object entity)
        {
            if (entity is BaseEntityMaster entityMaster)
                Label = entityMaster.Label;
        }
    }
}