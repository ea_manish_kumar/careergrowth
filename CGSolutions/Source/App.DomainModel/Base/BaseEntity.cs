﻿using System;

namespace App.DomainModel
{
    public class BaseEntity
    {
        public virtual int Id { get; set; }
    }

    public class BaseEntityLong
    {
        public virtual long Id { get; set; }
    }

    public class BaseEntityGuid : BaseEntity
    {
        public virtual Guid UID { get; set; }
    }
}