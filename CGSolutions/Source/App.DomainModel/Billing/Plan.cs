﻿namespace App.DomainModel
{
    public class Plan : BaseEntityMaster
    {
        public virtual short PlanTypeID { get; set; }
        public virtual string CurrencyCD { get; set; }
        public virtual string CountryCD { get; set; }
        public virtual decimal UnitPrice { get; set; }
        public virtual int ValidityDays { get; set; }
        public virtual PlanType PlanType { get; set; }
    }
}