﻿namespace App.DomainModel
{
    /// <summary>
    ///     Order status codes
    /// </summary>
    public class OrderStatusTypeCD
    {
        /// <summary>
        ///     Order is pending
        /// </summary>
        public const string Pending = "PEND";

        /// <summary>
        ///     payment completed successfully
        /// </summary>
        public const string PaymentCompleted = "PMCP";

        /// <summary>
        ///     Error occured while processing this order
        /// </summary>
        public const string Error = "ERRR";

        /// <summary>
        ///     Payment Failed
        /// </summary>
        public const string PaymentFailed = "PMFL";

        /// <summary>
        ///     Order Fulfilled.
        /// </summary>
        public const string Fulfilled = "FULF";

        /// <summary>
        ///     Order Failed.
        /// </summary>
        public const string Failed = "FAIL";
    }

    /// <summary>
    /// User Level Details
    /// </summary>
    public class UserLevelDetails
    {
        public bool IsPremiumUser { get; set; }
        public string SubscriptionType { get; set; }
    }
}