﻿namespace App.DomainModel
{
    public class PlanTypeSkin : BaseEntity
    {
        public virtual short PlanTypeID { get; set; }
        public virtual short SkinID { get; set; }
        public virtual bool WaterMarkEnabled { get; set; }
        public virtual PlanType PlanType { get; set; }
        public virtual Skin Skin { get; set; } 
    }
}