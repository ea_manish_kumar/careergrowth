﻿using System;

namespace App.DomainModel
{ 
    public class Order : BaseEntity
    {
        public Order()
        {
            CreatedOnUtc = DateTime.Now;
        }

        public virtual int UserID { get; set; }
        public virtual decimal Amount { get; set; }
        public virtual string StatusCD { get; set; }
        public virtual int SessionID { get; set; }
        public virtual DateTime CreatedOnUtc { get; set; }
        public virtual DateTime ModifiedOnUtc { get; set; }
        public virtual User User { get; set; }
    }
}