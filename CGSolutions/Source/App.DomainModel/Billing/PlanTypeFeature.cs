﻿namespace App.DomainModel
{ 
    public class PlanTypeFeature : BaseEntity, ISeedEntity
    {
        public virtual short PlanTypeID { get; set; }
        public virtual short FeatureID { get; set; }
        public virtual PlanType PlanType { get; set; }
        public virtual Feature Feature { get; set; }

        public virtual bool IdentifierEquals(object entity)
        {
            if (entity is PlanTypeFeature planTypeFeature)
                return (PlanTypeID == planTypeFeature.PlanTypeID && FeatureID == planTypeFeature.FeatureID);
            else
                return false;
        }

        public virtual bool ValueEquals(object entity)
        {
            if (entity is PlanTypeFeature planTypeFeature)
                return (PlanTypeID == planTypeFeature.PlanTypeID && FeatureID == planTypeFeature.FeatureID);
            else
                return false;
        }

        public virtual void Merge(object entity)
        {
            if (entity is PlanTypeFeature planTypeFeature)
            {
                PlanTypeID = planTypeFeature.PlanTypeID;
                FeatureID = planTypeFeature.FeatureID;
            }
        }
    }
}