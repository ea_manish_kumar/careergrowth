﻿using System;

namespace App.DomainModel
{
    public class Subscription : BaseEntity
    {
        public virtual int UserID { get; set; }
        public virtual int OrderID { get; set; }
        public virtual short PlanTypeID { get; set; }
        public virtual DateTime StartDate { get; set; }
        public virtual DateTime ExpireDate { get; set; }
        public virtual string StatusCD { get; set; }
        public virtual DateTime CreatedOnUtc { get; set; }
        public virtual DateTime ModifiedOnUtc { get; set; }
        public virtual User User { get; set; }
        public virtual Order Order { get; set; }
        public virtual PlanType PlanType { get; set; }
    }
}