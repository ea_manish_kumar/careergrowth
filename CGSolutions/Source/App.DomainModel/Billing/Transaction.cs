﻿using System;

namespace App.DomainModel
{
    public class Transaction : BaseEntity
    {
        public virtual int UserID { get; set; }
        public virtual int OrderID { get; set; }
        public virtual string StatusCD { get; set; }
        public virtual string VendorCD { get; set; }
        public virtual string VendorReferenceCD { get; set; }
        public virtual decimal Amount { get; set; }
        public virtual string IPAddress { get; set; }
        public virtual int SessionID { get; set; }
        public virtual DateTime CreatedOnUtc { get; set; }
        public virtual DateTime ModifiedOnUtc { get; set; }
        public virtual User User { get; set; }
        public virtual Order Order { get; set; }
    }
}