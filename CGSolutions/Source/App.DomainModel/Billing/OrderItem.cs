﻿using System;

namespace App.DomainModel
{ 
    public class OrderItem : BaseEntity
    {
        public virtual int OrderID { get; set; }
        public virtual int PlanID { get; set; }
        public virtual decimal Price { get; set; }
        public virtual DateTime CreatedOnUtc { get; set; }
    }
}