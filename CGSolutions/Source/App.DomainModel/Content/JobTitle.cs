﻿namespace App.DomainModel
{
    public class JobTitle : BaseEntity
    {
        public virtual string OnetSocCode { get; set; }

        public virtual string Title { get; set; }

        public virtual int? OccupationId { get; set; }

        public virtual Occupation Occupation { get; set; }

    }
}