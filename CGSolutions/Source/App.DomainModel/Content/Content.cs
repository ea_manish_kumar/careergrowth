﻿using App.Utility.Constants;

namespace App.DomainModel
{
    public class Content : BaseEntity
    {
        public virtual int? OccupationId { get; set; }

        public virtual string OnetSocCode { get; set; }

        public virtual ContentType ContentType { get; set; }

        public virtual string Example { get; set; }

        public virtual SourceType SourceType { get; set; }

        public virtual Occupation Occupation { get; set; }

    }
}