﻿namespace App.DomainModel
{
    public class Occupation : BaseEntity
    {
        public virtual string OnetSocCode { get; set; }

        public virtual string Title { get; set; }

        public virtual string Description { get; set; }
    }
}