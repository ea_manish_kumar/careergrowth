﻿using System;

namespace App.DomainModel
{
    public class UserToken : BaseEntityGuid
    {
        public virtual int UserId { get; set; }
        public virtual UserTokenType TokenType { get; set; }
        public virtual string HashedToken { get; set; }
        public virtual DateTime CreatedOnUtc { get; set; }
        public virtual TimeSpan ExpiryTime { get; set; }
        public virtual DateTime? VerifiedOnUtc { get; set; }

        public virtual User User { get; set; }
    }

    public enum UserTokenType : byte
    {
        ResetPassword = 1
    }
}