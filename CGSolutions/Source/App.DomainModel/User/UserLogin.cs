﻿using System;

namespace App.DomainModel
{
    public class UserLogin : BaseEntity
    {
        public virtual string UniqueStamp { get; set; }
        public virtual int UserId { get; set; }
        public virtual UserLoginType LoginType { get; set; }
        public virtual DateTime CreatedOnUtc { get; set; }
        public virtual DateTime ExpiredOnUtc { get; set; }

        public virtual User User { get; set; }
    }

    public enum UserLoginType : byte
    {
        SignUp = 1,
        SignIn = 2,
        ChangePassword = 3
    }
}