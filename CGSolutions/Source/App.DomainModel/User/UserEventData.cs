﻿using System;

namespace App.DomainModel
{
    public class UserEventData : BaseEntity
    {
        public virtual int UserId { get; set; }
        public virtual short EventId { get; set; }
        public virtual string EventData { get; set; }
        public virtual DateTime CreatedOnUtc { get; set; }
        public virtual DateTime? ModifiedOnUtc { get; set; }
        public virtual User User { get; set; }
        public virtual UserEvent Event { get; set; }
    }
}