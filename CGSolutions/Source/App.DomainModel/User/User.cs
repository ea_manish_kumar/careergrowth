﻿using System;

namespace App.DomainModel
{
    public class User : BaseEntity
    {
        public virtual string UserName { get; set; }

        public virtual string Email { get; set; }

        public virtual short RoleId { get; set; }

        public virtual string Name { get; set; }

        public virtual string SecurityStamp { get; set; }

        public virtual string HashedPassword { get; set; }

        public virtual DateTime CreatedOnUtc { get; set; }

        public virtual DateTime? LockoutEndDateUTC { get; set; }

        public virtual int? AccessFailedCount { get; set; }

        public virtual bool IsActive { get; set; }

        public virtual UserRole Role { get; set; }

        /// TODO: Add PlanID later.
    }
}