﻿using System;

namespace App.DomainModel
{
    public class UserLoginSession : BaseEntity
    {
        public virtual int UserLoginId { get; set; }
        public virtual int SessionId { get; set; }
        public virtual DateTime CreatedOnUtc { get; set; }

        public virtual UserLogin UserLogin { get; set; }
    }
}