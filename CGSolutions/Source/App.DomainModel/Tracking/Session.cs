﻿using System;

namespace App.DomainModel
{
    public class Session : BaseEntityGuid
    {
        public virtual string RequestedUrl { get; set; }
        public virtual string UrlReferrer { get; set; }
        public virtual string UserAgent { get; set; }
        public virtual string IPAddress { get; set; }
        public virtual string Source { get; set; }
        public virtual string Medium { get; set; }
        public virtual string Campaign { get; set; }
        public virtual DateTime CreatedOnUtc { get; set; }
        public virtual int SessionBrowserId { get; set; }
        public virtual int? ReferrerId { get; set; }

        public virtual SessionBrowser SessionBrowser { get; set; }
        public virtual Referrer Referrer { get; set; }
    }
}
