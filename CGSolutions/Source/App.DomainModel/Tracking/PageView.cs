﻿using System;

namespace App.DomainModel
{
    public class PageView : BaseEntityLong
    {
        public virtual short PageId { get; set; }
        public virtual int SessionId { get; set; }
        public virtual int? UserId { get; set; }
        public virtual DateTime CreatedOnUtc { get; set; }

        public virtual Page Page { get; set; }
    }
}