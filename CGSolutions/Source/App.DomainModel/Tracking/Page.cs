﻿namespace App.DomainModel
{
    public class Page : BaseEntityMaster
    {
        public virtual byte? SortIndex { get; set; }

        public override void Merge(object entity)
        {
            base.Merge(entity);
            if (entity is Page page)
                SortIndex = page.SortIndex;
        }
    }
}