﻿namespace App.DomainModel
{
    public class DocumentData : BaseEntity
    {
        public virtual int SubSectionId { get; set; }

        public virtual short FieldId { get; set; }

        public virtual string Data { get; set; }

        public virtual SubSection SubSection { get; set; }

        public virtual Field Field { get; set; }
    }
}