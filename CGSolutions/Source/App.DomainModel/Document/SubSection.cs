﻿using App.Utility;
using App.Utility.Extensions;
using System.Collections.Generic;
using System.Linq;

namespace App.DomainModel
{
    public class SubSection : BaseEntity
    {
        public virtual int SectionId { get; set; }

        public virtual short SortIndex { get; set; }

        public virtual Section Section { get; set; }

        public virtual IList<DocumentData> DocumentDatas { get; set; }
    }
}