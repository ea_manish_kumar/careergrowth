﻿using System;
using System.Collections.Generic;

namespace App.DomainModel
{
    public class Document : BaseEntity
    {
        public virtual int UserId { get; set; }

        public virtual string DocName { get; set; }

        public virtual string DocTypeCD { get; set; }

        public virtual string SkinCD { get; set; }

        public virtual DateTime CreatedOnUtc { get; set; }

        public virtual DateTime? ModifiedOnUtc { get; set; }

        public virtual IList<Section> Sections { get; set; }

        public virtual IList<DocumentStyle> DocumentStyles { get; set; }
    }
}