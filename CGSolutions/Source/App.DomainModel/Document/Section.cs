﻿using System.Collections.Generic;

namespace App.DomainModel
{
    public class Section : BaseEntity
    {
        public virtual string Label { get; set; }

        public virtual int DocumentId { get; set; }

        public virtual short SectionTypeId { get; set; }

        public virtual short SortIndex { get; set; }

        public virtual Document Document { get; set; }

        public virtual SectionType SectionType { get; set; }

        public virtual IList<SubSection> SubSections { get; set; }
    }
}