﻿namespace App.DomainModel
{
    public class DocumentStyle : BaseEntity
    {
        public virtual short StyleId { get; set; }

        public virtual int DocumentId { get; set; }

        public virtual string Value { get; set; }

        public virtual Style Style { get; set; }

        public virtual Document Document { get; set; }
    }
}