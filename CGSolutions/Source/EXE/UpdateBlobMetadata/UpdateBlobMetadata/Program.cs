﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace UpdateBlobMetadata
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("Start updating Blob properties");
            UpdateService obj = new UpdateService();
            obj.StartService();
            Console.WriteLine("Updation completed");
            Console.ReadLine(); 
        }
    }
}
