﻿using Microsoft.WindowsAzure.Storage;
using Microsoft.WindowsAzure.Storage.Auth;
using Microsoft.WindowsAzure.Storage.Blob;
using Microsoft.WindowsAzure.Storage.Shared.Protocol;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;
using System.Timers;

namespace UploadStaticContentToAzureBlob
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("Start Upload Blob Contents");
            UploadServices obj = new UploadServices();
            obj.StartService();
            Console.WriteLine("Uploading completed");
            Console.ReadLine(); 
        }

    }
}
