﻿using Microsoft.WindowsAzure.Storage;
using Microsoft.WindowsAzure.Storage.Auth;
using Microsoft.WindowsAzure.Storage.Blob;
using Microsoft.WindowsAzure.Storage.Shared.Protocol;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.IO;

namespace UploadStaticContentToAzureBlob
{
    class UploadServices
    {
        string accountName = ConfigurationManager.AppSettings["BlobAccountName"];
        string accountKey = ConfigurationManager.AppSettings["BlobAccountKey"];
        CloudBlobContainer scriptContainerBlob = null, imagesContainerBlob = null, cssContainerBlob = null;
        CloudBlobClient client = null;
        string scriptContainer = ConfigurationManager.AppSettings["scriptContainer"];
        string imagesContainer = ConfigurationManager.AppSettings["imagesContainer"];
        string cssContainer = ConfigurationManager.AppSettings["cssContainer"];
        string scriptPath = ConfigurationManager.AppSettings["scriptPath"];
        string imagesPath = ConfigurationManager.AppSettings["imagesPath"];
        string cssPath = ConfigurationManager.AppSettings["cssPath"];
        Dictionary<string, string> mimeTypeList = new Dictionary<string, string>();

        public void StartService()
        {
            StorageCredentials creds = new StorageCredentials(accountName, accountKey);
            CloudStorageAccount account = new CloudStorageAccount(creds, useHttps: false);
            client = account.CreateCloudBlobClient();
            Console.WriteLine("Script blob of Get Container refrence");

            scriptContainerBlob = client.GetContainerReference(scriptContainer);
            Console.WriteLine("Check script Container Exists");
            scriptContainerBlob.CreateIfNotExists();
            scriptContainerBlob.SetPermissions(new BlobContainerPermissions() { PublicAccess = BlobContainerPublicAccessType.Container });

            imagesContainerBlob = client.GetContainerReference(imagesContainer);
            Console.WriteLine("Check image Container Exists");
            imagesContainerBlob.CreateIfNotExists();
            imagesContainerBlob.SetPermissions(new BlobContainerPermissions() { PublicAccess = BlobContainerPublicAccessType.Container });

            cssContainerBlob = client.GetContainerReference(cssContainer);
            Console.WriteLine("Check css Container Exists");
            cssContainerBlob.CreateIfNotExists();
            cssContainerBlob.SetPermissions(new BlobContainerPermissions() { PublicAccess = BlobContainerPublicAccessType.Container });
            
            Console.WriteLine("Initialize Setting");
            InitializeCors();

            Console.WriteLine("Populating Mime Types");
            PopulateMimeType();

            Console.WriteLine("Start Uploading Script files");
            try
            {
                UploadContents(scriptPath, scriptContainerBlob, scriptPath);
                Console.WriteLine("Completed Script file Uploading");
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
            }
            Console.WriteLine("Start Uploading image files");
            try
            {
                UploadContents(imagesPath, imagesContainerBlob, imagesPath);
                Console.WriteLine("Completed image file Uploading");
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
            }
            Console.WriteLine("Start Uploading css files");
            try
            {
                UploadContents(cssPath, cssContainerBlob, cssPath);
                Console.WriteLine("Completed css file Uploading");
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
            }
        }
        private void UploadContents(string filesPath, CloudBlobContainer containerBlob, string rootFilePath)
        {
            string[] filePaths = Directory.GetFiles(filesPath);
            if (filePaths != null)
            {
                foreach (string strFileName in Directory.GetFiles(filesPath))
                {
                    string actualfileName = strFileName.Replace(rootFilePath + "\\", "");
                    try
                    {
                        string extension = Path.GetExtension(strFileName).ToLower();
                        if (extension == ".orig" || extension == ".ds_store" || extension==".bundle")
                            continue;

                        CloudBlockBlob blockBlob = containerBlob.GetBlockBlobReference(actualfileName);

                        string mimeType = GetMimeType(extension);
                        blockBlob.Properties.ContentType = mimeType;

                        using (var fileStream = System.IO.File.OpenRead(strFileName))
                        {
                            blockBlob.UploadFromStream(fileStream);
                        }


                        //Set the cache control property for the blob to have max-age as 12 Days
                        blockBlob.Properties.CacheControl = "public, max-age=1036800";
                        blockBlob.SetProperties();
                    }
                    catch (Exception ex)
                    {
                        Console.WriteLine("Error occurred- FileName: " + strFileName + ", " + ex.Message);
                    }
                    // Recurse into subdirectories of this directory.
                }
            }

            string[] subDirectoryEntries = Directory.GetDirectories(filesPath);
            if (subDirectoryEntries != null)
            {
                foreach (string subdirectory in subDirectoryEntries)
                    UploadContents(subdirectory, containerBlob, rootFilePath);
            }
        }

        private string GetMimeType(string extension)
        {
            if (extension == null)
                throw new ArgumentNullException("extension");

            if (extension.StartsWith("."))
                extension = extension.Substring(1);

            try
            {
                string mimeType = mimeTypeList[extension];
                return mimeType;
            }
            catch(Exception ex)
            {
                Console.WriteLine("Extension:" + extension + ", Error Message:"+ ex.Message);
                return "application/octet-stream";
            }
        }

        private void PopulateMimeType()
        {
            #region populate mime types
            mimeTypeList.Add("323", "text/h323");
            mimeTypeList.Add("3g2", "video/3gpp2");
            mimeTypeList.Add("3gp", "video/3gpp");
            mimeTypeList.Add("3gp2", "video/3gpp2");
            mimeTypeList.Add("3gpp", "video/3gpp");
            mimeTypeList.Add("7z", "application/x-7z-compressed");
            mimeTypeList.Add("aa", "audio/audible");
            mimeTypeList.Add("aac", "audio/aac");
            mimeTypeList.Add("aaf", "application/octet-stream");
            mimeTypeList.Add("aax", "audio/vnd.audible.aax");
            mimeTypeList.Add("ac3", "audio/ac3");
            mimeTypeList.Add("aca", "application/octet-stream");
            mimeTypeList.Add("accda", "application/msaccess.addin");
            mimeTypeList.Add("accdb", "application/msaccess");
            mimeTypeList.Add("accdc", "application/msaccess.cab");
            mimeTypeList.Add("accde", "application/msaccess");
            mimeTypeList.Add("accdr", "application/msaccess.runtime");
            mimeTypeList.Add("accdt", "application/msaccess");
            mimeTypeList.Add("accdw", "application/msaccess.webapplication");
            mimeTypeList.Add("accft", "application/msaccess.ftemplate");
            mimeTypeList.Add("acx", "application/internet-property-stream");
            mimeTypeList.Add("addin", "text/xml");
            mimeTypeList.Add("ade", "application/msaccess");
            mimeTypeList.Add("adobebridge", "application/x-bridge-url");
            mimeTypeList.Add("adp", "application/msaccess");
            mimeTypeList.Add("adt", "audio/vnd.dlna.adts");
            mimeTypeList.Add("adts", "audio/aac");
            mimeTypeList.Add("afm", "application/octet-stream");
            mimeTypeList.Add("ai", "application/postscript");
            mimeTypeList.Add("aif", "audio/x-aiff");
            mimeTypeList.Add("aifc", "audio/aiff");
            mimeTypeList.Add("aiff", "audio/aiff");
            mimeTypeList.Add("air", "application/vnd.adobe.air-application-installer-package+zip");
            mimeTypeList.Add("amc", "application/x-mpeg");
            mimeTypeList.Add("application", "application/x-ms-application");
            mimeTypeList.Add("art", "image/x-jg");
            mimeTypeList.Add("asa", "application/xml");
            mimeTypeList.Add("asax", "application/xml");
            mimeTypeList.Add("ascx", "application/xml");
            mimeTypeList.Add("asd", "application/octet-stream");
            mimeTypeList.Add("asf", "video/x-ms-asf");
            mimeTypeList.Add("ashx", "application/xml");
            mimeTypeList.Add("asi", "application/octet-stream");
            mimeTypeList.Add("asm", "text/plain");
            mimeTypeList.Add("asmx", "application/xml");
            mimeTypeList.Add("aspx", "application/xml");
            mimeTypeList.Add("asr", "video/x-ms-asf");
            mimeTypeList.Add("asx", "video/x-ms-asf");
            mimeTypeList.Add("atom", "application/atom+xml");
            mimeTypeList.Add("au", "audio/basic");
            mimeTypeList.Add("avi", "video/x-msvideo");
            mimeTypeList.Add("axs", "application/olescript");
            mimeTypeList.Add("bas", "text/plain");
            mimeTypeList.Add("bcpio", "application/x-bcpio");
            mimeTypeList.Add("bin", "application/octet-stream");
            mimeTypeList.Add("bmp", "image/bmp");
            mimeTypeList.Add("c", "text/plain");
            mimeTypeList.Add("cab", "application/octet-stream");
            mimeTypeList.Add("caf", "audio/x-caf");
            mimeTypeList.Add("calx", "application/vnd.ms-office.calx");
            mimeTypeList.Add("cat", "application/vnd.ms-pki.seccat");
            mimeTypeList.Add("cc", "text/plain");
            mimeTypeList.Add("cd", "text/plain");
            mimeTypeList.Add("cdda", "audio/aiff");
            mimeTypeList.Add("cdf", "application/x-cdf");
            mimeTypeList.Add("cer", "application/x-x509-ca-cert");
            mimeTypeList.Add("chm", "application/octet-stream");
            mimeTypeList.Add("class", "application/x-java-applet");
            mimeTypeList.Add("clp", "application/x-msclip");
            mimeTypeList.Add("cmx", "image/x-cmx");
            mimeTypeList.Add("cnf", "text/plain");
            mimeTypeList.Add("cod", "image/cis-cod");
            mimeTypeList.Add("config", "application/xml");
            mimeTypeList.Add("contact", "text/x-ms-contact");
            mimeTypeList.Add("coverage", "application/xml");
            mimeTypeList.Add("cpio", "application/x-cpio");
            mimeTypeList.Add("cpp", "text/plain");
            mimeTypeList.Add("crd", "application/x-mscardfile");
            mimeTypeList.Add("crl", "application/pkix-crl");
            mimeTypeList.Add("crt", "application/x-x509-ca-cert");
            mimeTypeList.Add("cs", "text/plain");
            mimeTypeList.Add("csdproj", "text/plain");
            mimeTypeList.Add("csh", "application/x-csh");
            mimeTypeList.Add("csproj", "text/plain");
            mimeTypeList.Add("css", "text/css");
            mimeTypeList.Add("csv", "text/csv");
            mimeTypeList.Add("cur", "application/octet-stream");
            mimeTypeList.Add("cxx", "text/plain");
            mimeTypeList.Add("dat", "application/octet-stream");
            mimeTypeList.Add("datasource", "application/xml");
            mimeTypeList.Add("dbproj", "text/plain");
            mimeTypeList.Add("dcr", "application/x-director");
            mimeTypeList.Add("def", "text/plain");
            mimeTypeList.Add("deploy", "application/octet-stream");
            mimeTypeList.Add("der", "application/x-x509-ca-cert");
            mimeTypeList.Add("dgml", "application/xml");
            mimeTypeList.Add("dib", "image/bmp");
            mimeTypeList.Add("dif", "video/x-dv");
            mimeTypeList.Add("dir", "application/x-director");
            mimeTypeList.Add("disco", "text/xml");
            mimeTypeList.Add("dll", "application/x-msdownload");
            mimeTypeList.Add("dll.config", "text/xml");
            mimeTypeList.Add("dlm", "text/dlm");
            mimeTypeList.Add("doc", "application/msword");
            mimeTypeList.Add("docm", "application/vnd.ms-word.document.macroenabled.12");
            mimeTypeList.Add("docx", "application/vnd.openxmlformats-officedocument.wordprocessingml.document");
            mimeTypeList.Add("dot", "application/msword");
            mimeTypeList.Add("dotm", "application/vnd.ms-word.template.macroenabled.12");
            mimeTypeList.Add("dotx", "application/vnd.openxmlformats-officedocument.wordprocessingml.template");
            mimeTypeList.Add("dsp", "application/octet-stream");
            mimeTypeList.Add("dsw", "text/plain");
            mimeTypeList.Add("dtd", "text/xml");
            mimeTypeList.Add("dtsconfig", "text/xml");
            mimeTypeList.Add("dv", "video/x-dv");
            mimeTypeList.Add("dvi", "application/x-dvi");
            mimeTypeList.Add("dwf", "drawing/x-dwf");
            mimeTypeList.Add("dwp", "application/octet-stream");
            mimeTypeList.Add("dxr", "application/x-director");
            mimeTypeList.Add("eml", "message/rfc822");
            mimeTypeList.Add("emz", "application/octet-stream");
            mimeTypeList.Add("eot", "application/octet-stream");
            mimeTypeList.Add("eps", "application/postscript");
            mimeTypeList.Add("etl", "application/etl");
            mimeTypeList.Add("etx", "text/x-setext");
            mimeTypeList.Add("evy", "application/envoy");
            mimeTypeList.Add("exe", "application/octet-stream");
            mimeTypeList.Add("exe.config", "text/xml");
            mimeTypeList.Add("fdf", "application/vnd.fdf");
            mimeTypeList.Add("fif", "application/fractals");
            mimeTypeList.Add("filters", "application/xml");
            mimeTypeList.Add("fla", "application/octet-stream");
            mimeTypeList.Add("flr", "x-world/x-vrml");
            mimeTypeList.Add("flv", "video/x-flv");
            mimeTypeList.Add("fsscript", "application/fsharp-script");
            mimeTypeList.Add("fsx", "application/fsharp-script");
            mimeTypeList.Add("generictest", "application/xml");
            mimeTypeList.Add("gif", "image/gif");
            mimeTypeList.Add("group", "text/x-ms-group");
            mimeTypeList.Add("gsm", "audio/x-gsm");
            mimeTypeList.Add("gtar", "application/x-gtar");
            mimeTypeList.Add("gz", "application/x-gzip");
            mimeTypeList.Add("h", "text/plain");
            mimeTypeList.Add("hdf", "application/x-hdf");
            mimeTypeList.Add("hdml", "text/x-hdml");
            mimeTypeList.Add("hhc", "application/x-oleobject");
            mimeTypeList.Add("hhk", "application/octet-stream");
            mimeTypeList.Add("hhp", "application/octet-stream");
            mimeTypeList.Add("hlp", "application/winhlp");
            mimeTypeList.Add("hpp", "text/plain");
            mimeTypeList.Add("hqx", "application/mac-binhex40");
            mimeTypeList.Add("hta", "application/hta");
            mimeTypeList.Add("htc", "text/x-component");
            mimeTypeList.Add("htm", "text/html");
            mimeTypeList.Add("html", "text/html");
            mimeTypeList.Add("htt", "text/webviewhtml");
            mimeTypeList.Add("hxa", "application/xml");
            mimeTypeList.Add("hxc", "application/xml");
            mimeTypeList.Add("hxd", "application/octet-stream");
            mimeTypeList.Add("hxe", "application/xml");
            mimeTypeList.Add("hxf", "application/xml");
            mimeTypeList.Add("hxh", "application/octet-stream");
            mimeTypeList.Add("hxi", "application/octet-stream");
            mimeTypeList.Add("hxk", "application/xml");
            mimeTypeList.Add("hxq", "application/octet-stream");
            mimeTypeList.Add("hxr", "application/octet-stream");
            mimeTypeList.Add("hxs", "application/octet-stream");
            mimeTypeList.Add("hxt", "text/html");
            mimeTypeList.Add("hxv", "application/xml");
            mimeTypeList.Add("hxw", "application/octet-stream");
            mimeTypeList.Add("hxx", "text/plain");
            mimeTypeList.Add("i", "text/plain");
            mimeTypeList.Add("ico", "image/x-icon");
            mimeTypeList.Add("ics", "application/octet-stream");
            mimeTypeList.Add("idl", "text/plain");
            mimeTypeList.Add("ief", "image/ief");
            mimeTypeList.Add("iii", "application/x-iphone");
            mimeTypeList.Add("inc", "text/plain");
            mimeTypeList.Add("inf", "application/octet-stream");
            mimeTypeList.Add("inl", "text/plain");
            mimeTypeList.Add("ins", "application/x-internet-signup");
            mimeTypeList.Add("ipa", "application/x-itunes-ipa");
            mimeTypeList.Add("ipg", "application/x-itunes-ipg");
            mimeTypeList.Add("ipproj", "text/plain");
            mimeTypeList.Add("ipsw", "application/x-itunes-ipsw");
            mimeTypeList.Add("iqy", "text/x-ms-iqy");
            mimeTypeList.Add("isp", "application/x-internet-signup");
            mimeTypeList.Add("ite", "application/x-itunes-ite");
            mimeTypeList.Add("itlp", "application/x-itunes-itlp");
            mimeTypeList.Add("itms", "application/x-itunes-itms");
            mimeTypeList.Add("itpc", "application/x-itunes-itpc");
            mimeTypeList.Add("ivf", "video/x-ivf");
            mimeTypeList.Add("jar", "application/java-archive");
            mimeTypeList.Add("java", "application/octet-stream");
            mimeTypeList.Add("jck", "application/liquidmotion");
            mimeTypeList.Add("jcz", "application/liquidmotion");
            mimeTypeList.Add("jfif", "image/pjpeg");
            mimeTypeList.Add("jnlp", "application/x-java-jnlp-file");
            mimeTypeList.Add("jpb", "application/octet-stream");
            mimeTypeList.Add("jpe", "image/jpeg");
            mimeTypeList.Add("jpeg", "image/jpeg");
            mimeTypeList.Add("jpg", "image/jpeg");
            mimeTypeList.Add("js", "application/x-javascript");
            mimeTypeList.Add("json", "application/json"); /// http://stackoverflow.com/questions/404470/what-mime-type-if-json-is-being-returned-by-a-rest-api
            mimeTypeList.Add("jsx", "text/jscript");
            mimeTypeList.Add("jsxbin", "text/plain");
            mimeTypeList.Add("latex", "application/x-latex");
            mimeTypeList.Add("library-ms", "application/windows-library+xml");
            mimeTypeList.Add("lit", "application/x-ms-reader");
            mimeTypeList.Add("loadtest", "application/xml");
            mimeTypeList.Add("lpk", "application/octet-stream");
            mimeTypeList.Add("lsf", "video/x-la-asf");
            mimeTypeList.Add("lst", "text/plain");
            mimeTypeList.Add("lsx", "video/x-la-asf");
            mimeTypeList.Add("lzh", "application/octet-stream");
            mimeTypeList.Add("m13", "application/x-msmediaview");
            mimeTypeList.Add("m14", "application/x-msmediaview");
            mimeTypeList.Add("m1v", "video/mpeg");
            mimeTypeList.Add("m2t", "video/vnd.dlna.mpeg-tts");
            mimeTypeList.Add("m2ts", "video/vnd.dlna.mpeg-tts");
            mimeTypeList.Add("m2v", "video/mpeg");
            mimeTypeList.Add("m3u", "audio/x-mpegurl");
            mimeTypeList.Add("m3u8", "audio/x-mpegurl");
            mimeTypeList.Add("m4a", "audio/m4a");
            mimeTypeList.Add("m4b", "audio/m4b");
            mimeTypeList.Add("m4p", "audio/m4p");
            mimeTypeList.Add("m4r", "audio/x-m4r");
            mimeTypeList.Add("m4v", "video/x-m4v");
            mimeTypeList.Add("mac", "image/x-macpaint");
            mimeTypeList.Add("mak", "text/plain");
            mimeTypeList.Add("man", "application/x-troff-man");
            mimeTypeList.Add("manifest", "application/x-ms-manifest");
            mimeTypeList.Add("map", "text/plain");
            mimeTypeList.Add("master", "application/xml");
            mimeTypeList.Add("mda", "application/msaccess");
            mimeTypeList.Add("mdb", "application/x-msaccess");
            mimeTypeList.Add("mde", "application/msaccess");
            mimeTypeList.Add("mdp", "application/octet-stream");
            mimeTypeList.Add("me", "application/x-troff-me");
            mimeTypeList.Add("mfp", "application/x-shockwave-flash");
            mimeTypeList.Add("mht", "message/rfc822");
            mimeTypeList.Add("mhtml", "message/rfc822");
            mimeTypeList.Add("mid", "audio/mid");
            mimeTypeList.Add("midi", "audio/mid");
            mimeTypeList.Add("mix", "application/octet-stream");
            mimeTypeList.Add("mk", "text/plain");
            mimeTypeList.Add("mmf", "application/x-smaf");
            mimeTypeList.Add("mno", "text/xml");
            mimeTypeList.Add("mny", "application/x-msmoney");
            mimeTypeList.Add("mod", "video/mpeg");
            mimeTypeList.Add("mov", "video/quicktime");
            mimeTypeList.Add("movie", "video/x-sgi-movie");
            mimeTypeList.Add("mp2", "video/mpeg");
            mimeTypeList.Add("mp2v", "video/mpeg");
            mimeTypeList.Add("mp3", "audio/mpeg");
            mimeTypeList.Add("mp4", "video/mp4");
            mimeTypeList.Add("mp4v", "video/mp4");
            mimeTypeList.Add("mpa", "video/mpeg");
            mimeTypeList.Add("mpe", "video/mpeg");
            mimeTypeList.Add("mpeg", "video/mpeg");
            mimeTypeList.Add("mpf", "application/vnd.ms-mediapackage");
            mimeTypeList.Add("mpg", "video/mpeg");
            mimeTypeList.Add("mpp", "application/vnd.ms-project");
            mimeTypeList.Add("mpv2", "video/mpeg");
            mimeTypeList.Add("mqv", "video/quicktime");
            mimeTypeList.Add("ms", "application/x-troff-ms");
            mimeTypeList.Add("msi", "application/octet-stream");
            mimeTypeList.Add("mso", "application/octet-stream");
            mimeTypeList.Add("mts", "video/vnd.dlna.mpeg-tts");
            mimeTypeList.Add("mtx", "application/xml");
            mimeTypeList.Add("mvb", "application/x-msmediaview");
            mimeTypeList.Add("mvc", "application/x-miva-compiled");
            mimeTypeList.Add("mxp", "application/x-mmxp");
            mimeTypeList.Add("nc", "application/x-netcdf");
            mimeTypeList.Add("nsc", "video/x-ms-asf");
            mimeTypeList.Add("nws", "message/rfc822");
            mimeTypeList.Add("ocx", "application/octet-stream");
            mimeTypeList.Add("oda", "application/oda");
            mimeTypeList.Add("odc", "text/x-ms-odc");
            mimeTypeList.Add("odh", "text/plain");
            mimeTypeList.Add("odl", "text/plain");
            mimeTypeList.Add("odp", "application/vnd.oasis.opendocument.presentation");
            mimeTypeList.Add("ods", "application/oleobject");
            mimeTypeList.Add("odt", "application/vnd.oasis.opendocument.text");
            mimeTypeList.Add("one", "application/onenote");
            mimeTypeList.Add("onea", "application/onenote");
            mimeTypeList.Add("onepkg", "application/onenote");
            mimeTypeList.Add("onetmp", "application/onenote");
            mimeTypeList.Add("onetoc", "application/onenote");
            mimeTypeList.Add("onetoc2", "application/onenote");
            mimeTypeList.Add("orderedtest", "application/xml");
            mimeTypeList.Add("osdx", "application/opensearchdescription+xml");
            mimeTypeList.Add("p10", "application/pkcs10");
            mimeTypeList.Add("p12", "application/x-pkcs12");
            mimeTypeList.Add("p7b", "application/x-pkcs7-certificates");
            mimeTypeList.Add("p7c", "application/pkcs7-mime");
            mimeTypeList.Add("p7m", "application/pkcs7-mime");
            mimeTypeList.Add("p7r", "application/x-pkcs7-certreqresp");
            mimeTypeList.Add("p7s", "application/pkcs7-signature");
            mimeTypeList.Add("pbm", "image/x-portable-bitmap");
            mimeTypeList.Add("pcast", "application/x-podcast");
            mimeTypeList.Add("pct", "image/pict");
            mimeTypeList.Add("pcx", "application/octet-stream");
            mimeTypeList.Add("pcz", "application/octet-stream");
            mimeTypeList.Add("pdf", "application/pdf");
            mimeTypeList.Add("pfb", "application/octet-stream");
            mimeTypeList.Add("pfm", "application/octet-stream");
            mimeTypeList.Add("pfx", "application/x-pkcs12");
            mimeTypeList.Add("pgm", "image/x-portable-graymap");
            mimeTypeList.Add("pic", "image/pict");
            mimeTypeList.Add("pict", "image/pict");
            mimeTypeList.Add("pkgdef", "text/plain");
            mimeTypeList.Add("pkgundef", "text/plain");
            mimeTypeList.Add("pko", "application/vnd.ms-pki.pko");
            mimeTypeList.Add("pls", "audio/scpls");
            mimeTypeList.Add("pma", "application/x-perfmon");
            mimeTypeList.Add("pmc", "application/x-perfmon");
            mimeTypeList.Add("pml", "application/x-perfmon");
            mimeTypeList.Add("pmr", "application/x-perfmon");
            mimeTypeList.Add("pmw", "application/x-perfmon");
            mimeTypeList.Add("png", "image/png");
            mimeTypeList.Add("pnm", "image/x-portable-anymap");
            mimeTypeList.Add("pnt", "image/x-macpaint");
            mimeTypeList.Add("pntg", "image/x-macpaint");
            mimeTypeList.Add("pnz", "image/png");
            mimeTypeList.Add("pot", "application/vnd.ms-powerpoint");
            mimeTypeList.Add("potm", "application/vnd.ms-powerpoint.template.macroenabled.12");
            mimeTypeList.Add("potx", "application/vnd.openxmlformats-officedocument.presentationml.template");
            mimeTypeList.Add("ppa", "application/vnd.ms-powerpoint");
            mimeTypeList.Add("ppam", "application/vnd.ms-powerpoint.addin.macroenabled.12");
            mimeTypeList.Add("ppm", "image/x-portable-pixmap");
            mimeTypeList.Add("pps", "application/vnd.ms-powerpoint");
            mimeTypeList.Add("ppsm", "application/vnd.ms-powerpoint.slideshow.macroenabled.12");
            mimeTypeList.Add("ppsx", "application/vnd.openxmlformats-officedocument.presentationml.slideshow");
            mimeTypeList.Add("ppt", "application/vnd.ms-powerpoint");
            mimeTypeList.Add("pptm", "application/vnd.ms-powerpoint.presentation.macroenabled.12");
            mimeTypeList.Add("pptx", "application/vnd.openxmlformats-officedocument.presentationml.presentation");
            mimeTypeList.Add("prf", "application/pics-rules");
            mimeTypeList.Add("prm", "application/octet-stream");
            mimeTypeList.Add("prx", "application/octet-stream");
            mimeTypeList.Add("ps", "application/postscript");
            mimeTypeList.Add("psc1", "application/powershell");
            mimeTypeList.Add("psd", "application/octet-stream");
            mimeTypeList.Add("psess", "application/xml");
            mimeTypeList.Add("psm", "application/octet-stream");
            mimeTypeList.Add("psp", "application/octet-stream");
            mimeTypeList.Add("pub", "application/x-mspublisher");
            mimeTypeList.Add("pwz", "application/vnd.ms-powerpoint");
            mimeTypeList.Add("qht", "text/x-html-insertion");
            mimeTypeList.Add("qhtm", "text/x-html-insertion");
            mimeTypeList.Add("qt", "video/quicktime");
            mimeTypeList.Add("qti", "image/x-quicktime");
            mimeTypeList.Add("qtif", "image/x-quicktime");
            mimeTypeList.Add("qtl", "application/x-quicktimeplayer");
            mimeTypeList.Add("qxd", "application/octet-stream");
            mimeTypeList.Add("ra", "audio/x-pn-realaudio");
            mimeTypeList.Add("ram", "audio/x-pn-realaudio");
            mimeTypeList.Add("rar", "application/octet-stream");
            mimeTypeList.Add("ras", "image/x-cmu-raster");
            mimeTypeList.Add("rat", "application/rat-file");
            mimeTypeList.Add("rc", "text/plain");
            mimeTypeList.Add("rc2", "text/plain");
            mimeTypeList.Add("rct", "text/plain");
            mimeTypeList.Add("rdlc", "application/xml");
            mimeTypeList.Add("resx", "application/xml");
            mimeTypeList.Add("rf", "image/vnd.rn-realflash");
            mimeTypeList.Add("rgb", "image/x-rgb");
            mimeTypeList.Add("rgs", "text/plain");
            mimeTypeList.Add("rm", "application/vnd.rn-realmedia");
            mimeTypeList.Add("rmi", "audio/mid");
            mimeTypeList.Add("rmp", "application/vnd.rn-rn_music_package");
            mimeTypeList.Add("roff", "application/x-troff");
            mimeTypeList.Add("rpm", "audio/x-pn-realaudio-plugin");
            mimeTypeList.Add("rqy", "text/x-ms-rqy");
            mimeTypeList.Add("rtf", "application/rtf");
            mimeTypeList.Add("rtx", "text/richtext");
            mimeTypeList.Add("ruleset", "application/xml");
            mimeTypeList.Add("s", "text/plain");
            mimeTypeList.Add("safariextz", "application/x-safari-safariextz");
            mimeTypeList.Add("scd", "application/x-msschedule");
            mimeTypeList.Add("sct", "text/scriptlet");
            mimeTypeList.Add("sd2", "audio/x-sd2");
            mimeTypeList.Add("sdp", "application/sdp");
            mimeTypeList.Add("sea", "application/octet-stream");
            mimeTypeList.Add("searchconnector-ms", "application/windows-search-connector+xml");
            mimeTypeList.Add("setpay", "application/set-payment-initiation");
            mimeTypeList.Add("setreg", "application/set-registration-initiation");
            mimeTypeList.Add("settings", "application/xml");
            mimeTypeList.Add("sgimb", "application/x-sgimb");
            mimeTypeList.Add("sgml", "text/sgml");
            mimeTypeList.Add("sh", "application/x-sh");
            mimeTypeList.Add("shar", "application/x-shar");
            mimeTypeList.Add("shtml", "text/html");
            mimeTypeList.Add("sit", "application/x-stuffit");
            mimeTypeList.Add("sitemap", "application/xml");
            mimeTypeList.Add("skin", "application/xml");
            mimeTypeList.Add("sldm", "application/vnd.ms-powerpoint.slide.macroenabled.12");
            mimeTypeList.Add("sldx", "application/vnd.openxmlformats-officedocument.presentationml.slide");
            mimeTypeList.Add("slk", "application/vnd.ms-excel");
            mimeTypeList.Add("sln", "text/plain");
            mimeTypeList.Add("slupkg-ms", "application/x-ms-license");
            mimeTypeList.Add("smd", "audio/x-smd");
            mimeTypeList.Add("smi", "application/octet-stream");
            mimeTypeList.Add("smx", "audio/x-smd");
            mimeTypeList.Add("smz", "audio/x-smd");
            mimeTypeList.Add("snd", "audio/basic");
            mimeTypeList.Add("snippet", "application/xml");
            mimeTypeList.Add("snp", "application/octet-stream");
            mimeTypeList.Add("sol", "text/plain");
            mimeTypeList.Add("sor", "text/plain");
            mimeTypeList.Add("spc", "application/x-pkcs7-certificates");
            mimeTypeList.Add("spl", "application/futuresplash");
            mimeTypeList.Add("src", "application/x-wais-source");
            mimeTypeList.Add("srf", "text/plain");
            mimeTypeList.Add("ssisdeploymentmanifest", "text/xml");
            mimeTypeList.Add("ssm", "application/streamingmedia");
            mimeTypeList.Add("sst", "application/vnd.ms-pki.certstore");
            mimeTypeList.Add("stl", "application/vnd.ms-pki.stl");
            mimeTypeList.Add("sv4cpio", "application/x-sv4cpio");
            mimeTypeList.Add("sv4crc", "application/x-sv4crc");
            mimeTypeList.Add("svg", "image/svg+xml");   /// http://stackoverflow.com/questions/11918977/right-mime-type-for-svg-images-fonts, http://www.w3.org/TR/SVGTiny12/mimereg.html
            mimeTypeList.Add("svc", "application/xml");
            mimeTypeList.Add("swf", "application/x-shockwave-flash");
            mimeTypeList.Add("t", "application/x-troff");
            mimeTypeList.Add("tar", "application/x-tar");
            mimeTypeList.Add("tcl", "application/x-tcl");
            mimeTypeList.Add("testrunconfig", "application/xml");
            mimeTypeList.Add("testsettings", "application/xml");
            mimeTypeList.Add("tex", "application/x-tex");
            mimeTypeList.Add("texi", "application/x-texinfo");
            mimeTypeList.Add("texinfo", "application/x-texinfo");
            mimeTypeList.Add("tgz", "application/x-compressed");
            mimeTypeList.Add("thmx", "application/vnd.ms-officetheme");
            mimeTypeList.Add("thn", "application/octet-stream");
            mimeTypeList.Add("tif", "image/tiff");
            mimeTypeList.Add("tiff", "image/tiff");
            mimeTypeList.Add("tlh", "text/plain");
            mimeTypeList.Add("tli", "text/plain");
            mimeTypeList.Add("toc", "application/octet-stream");
            mimeTypeList.Add("tr", "application/x-troff");
            mimeTypeList.Add("trm", "application/x-msterminal");
            mimeTypeList.Add("trx", "application/xml");
            mimeTypeList.Add("ts", "video/vnd.dlna.mpeg-tts");
            mimeTypeList.Add("tsv", "text/tab-separated-values");
            mimeTypeList.Add("ttf", "application/octet-stream");
            mimeTypeList.Add("tts", "video/vnd.dlna.mpeg-tts");
            mimeTypeList.Add("txt", "text/plain");
            mimeTypeList.Add("u32", "application/octet-stream");
            mimeTypeList.Add("uls", "text/iuls");
            mimeTypeList.Add("user", "text/plain");
            mimeTypeList.Add("ustar", "application/x-ustar");
            mimeTypeList.Add("vb", "text/plain");
            mimeTypeList.Add("vbdproj", "text/plain");
            mimeTypeList.Add("vbk", "video/mpeg");
            mimeTypeList.Add("vbproj", "text/plain");
            mimeTypeList.Add("vbs", "text/vbscript");
            mimeTypeList.Add("vcf", "text/x-vcard");
            mimeTypeList.Add("vcproj", "application/xml");
            mimeTypeList.Add("vcs", "text/plain");
            mimeTypeList.Add("vcxproj", "application/xml");
            mimeTypeList.Add("vddproj", "text/plain");
            mimeTypeList.Add("vdp", "text/plain");
            mimeTypeList.Add("vdproj", "text/plain");
            mimeTypeList.Add("vdx", "application/vnd.ms-visio.viewer");
            mimeTypeList.Add("vml", "text/xml");
            mimeTypeList.Add("vscontent", "application/xml");
            mimeTypeList.Add("vsct", "text/xml");
            mimeTypeList.Add("vsd", "application/vnd.visio");
            mimeTypeList.Add("vsi", "application/ms-vsi");
            mimeTypeList.Add("vsix", "application/vsix");
            mimeTypeList.Add("vsixlangpack", "text/xml");
            mimeTypeList.Add("vsixmanifest", "text/xml");
            mimeTypeList.Add("vsmdi", "application/xml");
            mimeTypeList.Add("vspscc", "text/plain");
            mimeTypeList.Add("vss", "application/vnd.visio");
            mimeTypeList.Add("vsscc", "text/plain");
            mimeTypeList.Add("vssettings", "text/xml");
            mimeTypeList.Add("vssscc", "text/plain");
            mimeTypeList.Add("vst", "application/vnd.visio");
            mimeTypeList.Add("vstemplate", "text/xml");
            mimeTypeList.Add("vsto", "application/x-ms-vsto");
            mimeTypeList.Add("vsw", "application/vnd.visio");
            mimeTypeList.Add("vsx", "application/vnd.visio");
            mimeTypeList.Add("vtx", "application/vnd.visio");
            mimeTypeList.Add("wav", "audio/wav");
            mimeTypeList.Add("wave", "audio/wav");
            mimeTypeList.Add("wax", "audio/x-ms-wax");
            mimeTypeList.Add("wbk", "application/msword");
            mimeTypeList.Add("wbmp", "image/vnd.wap.wbmp");
            mimeTypeList.Add("wcm", "application/vnd.ms-works");
            mimeTypeList.Add("wdb", "application/vnd.ms-works");
            mimeTypeList.Add("wdp", "image/vnd.ms-photo");
            mimeTypeList.Add("webarchive", "application/x-safari-webarchive");
            mimeTypeList.Add("webtest", "application/xml");
            mimeTypeList.Add("wiq", "application/xml");
            mimeTypeList.Add("wiz", "application/msword");
            mimeTypeList.Add("wks", "application/vnd.ms-works");
            mimeTypeList.Add("wlmp", "application/wlmoviemaker");
            mimeTypeList.Add("wlpginstall", "application/x-wlpg-detect");
            mimeTypeList.Add("wlpginstall3", "application/x-wlpg3-detect");
            mimeTypeList.Add("wm", "video/x-ms-wm");
            mimeTypeList.Add("wma", "audio/x-ms-wma");
            mimeTypeList.Add("wmd", "application/x-ms-wmd");
            mimeTypeList.Add("wmf", "application/x-msmetafile");
            mimeTypeList.Add("wml", "text/vnd.wap.wml");
            mimeTypeList.Add("wmlc", "application/vnd.wap.wmlc");
            mimeTypeList.Add("wmls", "text/vnd.wap.wmlscript");
            mimeTypeList.Add("wmlsc", "application/vnd.wap.wmlscriptc");
            mimeTypeList.Add("wmp", "video/x-ms-wmp");
            mimeTypeList.Add("wmv", "video/x-ms-wmv");
            mimeTypeList.Add("wmx", "video/x-ms-wmx");
            mimeTypeList.Add("wmz", "application/x-ms-wmz");
            mimeTypeList.Add("woff", "application/x-font-woff");    /// http://stackoverflow.com/questions/3594823/mime-type-for-woff-fonts
            mimeTypeList.Add("woff2", "application/font-woff2");
            mimeTypeList.Add("wpl", "application/vnd.ms-wpl");
            mimeTypeList.Add("wps", "application/vnd.ms-works");
            mimeTypeList.Add("wri", "application/x-mswrite");
            mimeTypeList.Add("wrl", "x-world/x-vrml");
            mimeTypeList.Add("wrz", "x-world/x-vrml");
            mimeTypeList.Add("wsc", "text/scriptlet");
            mimeTypeList.Add("wsdl", "text/xml");
            mimeTypeList.Add("wvx", "video/x-ms-wvx");
            mimeTypeList.Add("x", "application/directx");
            mimeTypeList.Add("xaf", "x-world/x-vrml");
            mimeTypeList.Add("xaml", "application/xaml+xml");
            mimeTypeList.Add("xap", "application/x-silverlight-app");
            mimeTypeList.Add("xbap", "application/x-ms-xbap");
            mimeTypeList.Add("xbm", "image/x-xbitmap");
            mimeTypeList.Add("xdr", "text/plain");
            mimeTypeList.Add("xht", "application/xhtml+xml");
            mimeTypeList.Add("xhtml", "application/xhtml+xml");
            mimeTypeList.Add("xla", "application/vnd.ms-excel");
            mimeTypeList.Add("xlam", "application/vnd.ms-excel.addin.macroenabled.12");
            mimeTypeList.Add("xlc", "application/vnd.ms-excel");
            mimeTypeList.Add("xld", "application/vnd.ms-excel");
            mimeTypeList.Add("xlk", "application/vnd.ms-excel");
            mimeTypeList.Add("xll", "application/vnd.ms-excel");
            mimeTypeList.Add("xlm", "application/vnd.ms-excel");
            mimeTypeList.Add("xls", "application/vnd.ms-excel");
            mimeTypeList.Add("xlsb", "application/vnd.ms-excel.sheet.binary.macroenabled.12");
            mimeTypeList.Add("xlsm", "application/vnd.ms-excel.sheet.macroenabled.12");
            mimeTypeList.Add("xlsx", "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet");
            mimeTypeList.Add("xlt", "application/vnd.ms-excel");
            mimeTypeList.Add("xltm", "application/vnd.ms-excel.template.macroenabled.12");
            mimeTypeList.Add("xltx", "application/vnd.openxmlformats-officedocument.spreadsheetml.template");
            mimeTypeList.Add("xlw", "application/vnd.ms-excel");
            mimeTypeList.Add("xml", "text/xml");
            mimeTypeList.Add("xmta", "application/xml");
            mimeTypeList.Add("xof", "x-world/x-vrml");
            mimeTypeList.Add("xoml", "text/plain");
            mimeTypeList.Add("xpm", "image/x-xpixmap");
            mimeTypeList.Add("xps", "application/vnd.ms-xpsdocument");
            mimeTypeList.Add("xrm-ms", "text/xml");
            mimeTypeList.Add("xsc", "application/xml");
            mimeTypeList.Add("xsd", "text/xml");
            mimeTypeList.Add("xsf", "text/xml");
            mimeTypeList.Add("xsl", "text/xml");
            mimeTypeList.Add("xslt", "text/xml");
            mimeTypeList.Add("xsn", "application/octet-stream");
            mimeTypeList.Add("xss", "application/xml");
            mimeTypeList.Add("xtp", "application/octet-stream");
            mimeTypeList.Add("xwd", "image/x-xwindowdump");
            mimeTypeList.Add("z", "application/x-compress");
            mimeTypeList.Add("zip", "application/x-zip-compressed");
            #endregion
        }

        private void InitializeCors()
        {
            // CORS should be enabled once at service startup
            // Given a BlobClient, download the current Service Properties
            ServiceProperties scriptServiceProperties = scriptContainerBlob.ServiceClient.GetServiceProperties();
            ServiceProperties imageServiceProperties = imagesContainerBlob.ServiceClient.GetServiceProperties();
            ServiceProperties cssServiceProperties = cssContainerBlob.ServiceClient.GetServiceProperties();

            // Enable and Configure CORS
            ConfigureCors(scriptServiceProperties);
            ConfigureCors(imageServiceProperties);
            ConfigureCors(cssServiceProperties);
            // Commit the CORS changes into the Service Properties
            scriptContainerBlob.ServiceClient.SetServiceProperties(scriptServiceProperties);
            imagesContainerBlob.ServiceClient.SetServiceProperties(imageServiceProperties);
            cssContainerBlob.ServiceClient.SetServiceProperties(cssServiceProperties);
        }

        private void ConfigureCors(ServiceProperties serviceProperties)
        {
            serviceProperties.Cors = new CorsProperties();
            serviceProperties.Cors.CorsRules.Add(new CorsRule()
            {
                AllowedHeaders = new List<string>() { "*" },
                AllowedMethods = CorsHttpMethods.Get | CorsHttpMethods.Head,
                AllowedOrigins = new List<string>() { "*" },
                ExposedHeaders = new List<string>() { "*" },
                MaxAgeInSeconds = 1036800 // 12 Days
            });
        }

    }
}