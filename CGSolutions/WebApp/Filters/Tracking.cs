﻿using App.DomainModel;
using App.Services;
using System;
using System.Linq;
using System.Threading.Tasks;
using System.Web.Mvc;
using Utilities;
using WebApp.Constants;
using WebApp.Extensions;
using WebApp.Managers;

namespace WebApp.Filters
{
    public class TrackingAttribute : ActionFilterAttribute
    {
        public override void OnActionExecuting(ActionExecutingContext filterContext)
        {
            base.OnActionExecuting(filterContext);

            if (!filterContext.RequestContext.HttpContext.Request.IsAjaxRequest() && string.IsNullOrEmpty(filterContext.HttpContext.Request.Cookies.Read(SiteCookies.SessionUID)))
            {
                ITrackingService trackingService = DependencyResolver.Current.GetService<ITrackingService>();
                ISignInManager signInManager = DependencyResolver.Current.GetService<ISignInManager>();
                AsyncHelper.RunSync(async () => await TrackSessionAsync(filterContext, trackingService, signInManager));
            }
        }

        private async Task TrackSessionAsync(ActionExecutingContext filterContext, ITrackingService trackingService, ISignInManager signInManager)
        {
            var newSession = new Session
            {
                UID = Guid.NewGuid(),
                UrlReferrer = filterContext.HttpContext.Request.UrlReferrer?.ToString(),
                UserAgent = filterContext.HttpContext.Request.UserAgent,
                IPAddress = filterContext.HttpContext.Request.GetUserIPAddress(),
                RequestedUrl = filterContext.HttpContext.Request.Url.ToString(),
                Source = GetSource(filterContext),
                Medium = GetMedium(filterContext),
                Campaign = GetCampaign(filterContext),
                ReferrerId = await GetReferrerId(filterContext, trackingService)
            };
            if (Guid.TryParse(filterContext.HttpContext.Request.Cookies.Read(SiteCookies.SessionBrowserUID), out Guid sessionBrowserUID))
            {
                var sessionBrowserId = await trackingService.GetSessionBrowserId(sessionBrowserUID);
                if (sessionBrowserId.HasValue)
                    newSession.SessionBrowserId = sessionBrowserId.Value;
            }
            if (newSession.SessionBrowserId <= 0)
                newSession.SessionBrowser = new SessionBrowser { UID = Guid.NewGuid() };
            await trackingService.SaveSession(newSession);
            if (filterContext.HttpContext.User.IsSignedIn())
            {
                await signInManager.SaveUserLoginSession(filterContext.HttpContext.User.GetLoginId(), newSession.Id);
            }
            filterContext.HttpContext.Response.Cookies.Write(SiteCookies.SessionUID, newSession.UID.ToString());
            if (string.IsNullOrEmpty(filterContext.HttpContext.Request.Cookies.Read(SiteCookies.SessionBrowserUID)) && newSession.SessionBrowser != null)
                filterContext.HttpContext.Response.Cookies.Write(SiteCookies.SessionBrowserUID, newSession.SessionBrowser.UID.ToString(), true);
        }

        private string GetSource(ActionExecutingContext filterContext)
        {
            if (!string.IsNullOrEmpty(filterContext.HttpContext.Request.QueryString[SiteQueryStrings.Utm_Source]))
                return filterContext.HttpContext.Request.QueryString[SiteQueryStrings.Utm_Source];

            var urlReferrer = filterContext.HttpContext.Request.UrlReferrer;
            if (urlReferrer != null)
            {
                if (urlReferrer.Host == filterContext.HttpContext.Request.Url.Host)
                    return TrackingConstants.INTERNAL_REFERRAL;
                var referrer = urlReferrer.ToString();
                if (!string.IsNullOrEmpty(referrer))
                {
                    if (referrer.Contains(TrackingConstants.GOOGLE))
                        return TrackingConstants.GOOGLE;
                    else if (referrer.Contains(TrackingConstants.BING))
                        return TrackingConstants.BING;
                    else if (referrer.Contains(TrackingConstants.QS_SEPARATOR))
                        return referrer.Split(TrackingConstants.QS_SEPARATOR)[0];
                    else
                        return referrer;
                }
            }

            if (!string.IsNullOrEmpty(filterContext.HttpContext.Request.QueryString[SiteQueryStrings.Source]))
                return filterContext.HttpContext.Request.QueryString[SiteQueryStrings.Source];
            if (!string.IsNullOrEmpty(filterContext.HttpContext.Request.QueryString[SiteQueryStrings.Src]))
                return filterContext.HttpContext.Request.QueryString[SiteQueryStrings.Src];

            return null;
        }

        private string GetMedium(ActionExecutingContext filterContext)
        {
            if (filterContext.HttpContext.Request.QueryString[SiteQueryStrings.Utm_Medium] != null)
                return filterContext.HttpContext.Request.QueryString[SiteQueryStrings.Utm_Medium];

            var urlReferrer = filterContext.HttpContext.Request.UrlReferrer;
            if (urlReferrer != null)
            {
                if (urlReferrer.Host == filterContext.HttpContext.Request.Url.Host)
                    return TrackingConstants.INTERNAL_MEDIUM;
                var referrer = urlReferrer.ToString();
                if (!string.IsNullOrEmpty(referrer))
                {
                    if (referrer.Contains(TrackingConstants.GOOGLE) || referrer.Contains(TrackingConstants.BING))
                        return TrackingConstants.ORGANIC;
                    return TrackingConstants.REFERRAL;
                }
            }

            return null;
        }

        private string GetCampaign(ActionExecutingContext filterContext)
        {
            if (!string.IsNullOrEmpty(filterContext.HttpContext.Request.QueryString[SiteQueryStrings.Utm_Campaign]))
                return filterContext.HttpContext.Request.QueryString[SiteQueryStrings.Utm_Campaign];

            return null;
        }

        private async Task<int?> GetReferrerId(ActionExecutingContext filterContext, ITrackingService trackingService)
        {
            var code = filterContext.HttpContext.Request.QueryString[SiteQueryStrings.Referrer];
            if (!string.IsNullOrEmpty(code))
                return await trackingService.GetReferrerId(code.Trim());
            return null;
        }
    }
}