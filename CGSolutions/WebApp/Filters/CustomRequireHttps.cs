﻿using System;
using System.Text;
using System.Web.Mvc;

namespace WebApp.Filters
{
    public class CustomRequireHttpsAttribute : RequireHttpsAttribute
    {
        public uint MaxAge { get; }
        public bool IncludeSubDomains { get; set; }
        public bool Preload { get; set; }
        public CustomRequireHttpsAttribute(uint maxAge) : base() { Preload = false; MaxAge = maxAge; IncludeSubDomains = false; }
        protected override void HandleNonHttpsRequest(AuthorizationContext filterContext)
        {
            // The base only redirects GET, but we added HEAD as well. This avoids exceptions for bots crawling using HEAD.  
            // The other requests will throw an exception to ensure the correct verbs are used.         
            // We fall back to the base method as the mvc exceptions are marked as internal.  
            if (!string.Equals(filterContext.HttpContext.Request.HttpMethod, "GET", StringComparison.OrdinalIgnoreCase)
                && !string.Equals(filterContext.HttpContext.Request.HttpMethod, "HEAD", StringComparison.OrdinalIgnoreCase))
            {
                base.HandleNonHttpsRequest(filterContext);
            }

            // Redirect to HTTPS version of page
            // We updated this to redirect using 301 (permanent) instead of 302 (temporary). 
            string url = "https://" + filterContext.HttpContext.Request.Url.Host + filterContext.HttpContext.Request.RawUrl;
            filterContext.Result = new RedirectResult(url, true);
        }
        public override void OnAuthorization(AuthorizationContext filterContext)
        {
            if (filterContext == null)
            {
                throw new ArgumentNullException("filterContext");
            }
            if (filterContext.HttpContext.Request.IsSecureConnection)
            {
                if (Preload && (MaxAge < 10886400))
                {
                    throw new InvalidOperationException("In order to confirm HSTS preload list subscription expiry must be at least eighteen weeks (10886400 seconds).");
                }
                if (Preload && !IncludeSubDomains)
                {
                    throw new InvalidOperationException("In order to confirm HSTS preload list subscription subdomains must be included.");
                }
                var headerBuilder = new StringBuilder(); headerBuilder.AppendFormat("max-age={0}", MaxAge);
                if (IncludeSubDomains)
                {
                    headerBuilder.Append("; includeSubDomains");
                }
                if (Preload)
                {
                    headerBuilder.Append("; preload");
                }
                filterContext.HttpContext.Response.AppendHeader("Strict-Transport-Security", headerBuilder.ToString());
            }
            else
            {
                HandleNonHttpsRequest(filterContext);
            }
        }
    }
}