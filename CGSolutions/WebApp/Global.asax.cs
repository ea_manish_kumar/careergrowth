﻿using Newtonsoft.Json;
using Newtonsoft.Json.Converters;
using System;
using System.Dynamic;
using System.Globalization;
using System.IO;
using System.Web;
using System.Web.Helpers;
using System.Web.Mvc;
using System.Web.Optimization;
using System.Web.Routing;

namespace WebApp
{
    public class ResumeHelpApplication : HttpApplication
    {
        private void Application_Start(object sender, EventArgs e)
        {
            AreaRegistration.RegisterAllAreas();
            FilterConfig.RegisterGlobalFilters(GlobalFilters.Filters);
            RouteConfig.RegisterRoutes(RouteTable.Routes);
            BundleConfig.RegisterBundles(BundleTable.Bundles);
            AntiForgeryConfig.SuppressIdentityHeuristicChecks = true;
            MvcHandler.DisableMvcResponseHeader = true;
        }

        private void Application_Error(object sender, EventArgs e)
        {
            /*Exception ex = Server.GetLastError();
            HttpException httpException = ex as HttpException;
            RouteData routeData = new RouteData();
            routeData.Values.Add(RouteConstants.Controller, RouteConstants.Error);
            if (httpException == null)
            {
                LogUnhandledException(ex, httpException);
                routeData.Values.Add(RouteConstants.Action, RouteConstants.ServerError);
            }
            else
            {
                switch (httpException.GetHttpCode())
                {
                    case 404:
                        routeData.Values.Add(RouteConstants.Action, RouteConstants.NotFound);
                        break;
                    case 403:
                        routeData.Values.Add(RouteConstants.Action, RouteConstants.Forbidden);
                        break;
                    case 500:
                        LogUnhandledException(ex, httpException);
                        routeData.Values.Add(RouteConstants.Action, RouteConstants.ServerError);
                        break;
                    default:
                        LogUnhandledException(ex, httpException);
                        routeData.Values.Add(RouteConstants.Action, RouteConstants.ServerError);
                        break;
                }
            }
            Server.ClearError();
            Response.TrySkipIisCustomErrors = true;
            IController error = new ErrorController();
            error.Execute(new RequestContext(new HttpContextWrapper(Context), routeData));*/
        }
    }

    public sealed class JsonDotNetValueProviderFactory : ValueProviderFactory
    {
        public override IValueProvider GetValueProvider(ControllerContext controllerContext)
        {
            if (controllerContext == null)
                throw new ArgumentNullException("controllerContext");

            if (!controllerContext.HttpContext.Request.ContentType.StartsWith("application/json",
                StringComparison.OrdinalIgnoreCase))
                return null;

            var reader = new StreamReader(controllerContext.HttpContext.Request.InputStream);
            var bodyText = reader.ReadToEnd();
            if (!string.IsNullOrEmpty(bodyText))
            {
                var input = new DictionaryValueProvider<object>(
                    JsonConvert.DeserializeObject<ExpandoObject>(bodyText, new ExpandoObjectConverter()),
                    CultureInfo.CurrentCulture);
                return input;
            }

            return null;

            //return String.IsNullOrEmpty(bodyText) ? null : new DictionaryValueProvider<object>(Newtonsoft.Json.JsonConvert.DeserializeObject<System.Dynamic.ExpandoObject>(bodyText, new Newtonsoft.Json.Converters.ExpandoObjectConverter()), System.Globalization.CultureInfo.CurrentCulture);
        }
    }
}