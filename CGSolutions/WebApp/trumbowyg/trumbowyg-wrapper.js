﻿(function(win, $) {
    function _replaceText(container, search, replace, text_only) {
        var node = container.firstChild,
            val,
            new_val,

            // Elements to be removed at the end.
            remove = [];

        // Only continue if firstChild exists.
        if (node) {

            // Loop over all childNodes.
            do {

                // Only process text nodes.
                if (node.nodeType === 3) {

                    // The original node value.
                    val = node.nodeValue;

                    // The new value.
                    new_val = val.replace(search, replace);

                    // Only replace text if the new value is actually different!
                    if (new_val !== val) {

                        if (!text_only && /</.test(new_val)) {
                            // The new value contains HTML, set it in a slower but far more
                            // robust way.
                            $(node).before(new_val);

                            // Don't remove the node yet, or the loop will lose its place.
                            remove.push(node);
                        } else {
                            // The new value contains no HTML, so it can be set in this
                            // very fast, simple way.
                            node.nodeValue = new_val;
                        }
                    }
                }

            } while (node = node.nextSibling);
        }

        // Time to remove those elements!
        remove.length && $(remove).remove();
    }

    function spellCheckFactory() {
        var editor = null;

        function getInfo() {
            return {
                longname: "Trumbyog",
                author: "ProfileBuilder",
                authorurl: "TBD",
                infourl: "TBD",
                version: "0.0.1"
            };
        };

        function createControl(n, cm) {
            if (n == "jqueryspellchecker") {
                return cm.createButton(n,
                    {
                        title: "spellchecker.desc",
                        cmd: "mcejQuerySpellCheck",
                        scope: this
                    });
            }
        };

        function init(ed) {
            editor = ed;
        }

        function toggleCheck() {
            var t = this;

            if (t.isSpellChecked === false) {
                t.toggleCaller();
                t.createSpellchecker();
                t.check();
                t.disableEditor();
            } else {
                t.dispose();
                t.enableEditor();
                t.toggleCaller();
            }
        }

        function toggleCaller() {
            var t = this;
            if (window.Builder && Builder.TinyMCE && Builder.TinyMCE.ToggleSpellCheck) {
                var ed = t.editor;
                var id = ed.id;
                Builder.TinyMCE.ToggleSpellCheck(id);
            }
        }

        function noIncorrectWord() {
            var t = this;
            if (window.Builder && Builder.TinyMCE && Builder.TinyMCE.NoInCorrectWord) {
                var ed = t.editor;
                var id = ed.id;
                Builder.TinyMCE.NoInCorrectWord(id);
            }
        }

        function toggleEditor(disabled, editable) {
            /// http://neal.codes/blog/accessing-tinymce-4-controller-state/
            var toolbar = this.editor.theme.panel.find("toolbar *");
            if (toolbar && toolbar.length) {
                for (var i = 0; i < toolbar.length - 1; i++) {
                    if (!toolbar[i].icon || toolbar[i].icon() != "spellchecker") {
                        toolbar[i].disabled(disabled);
                    }
                }
            }

            var body = this.editor.getBody();
            body.setAttribute("contenteditable", editable);
            body.disabled = disabled;

            /// Below is a hack to make the editor readonly.
            if (disabled) {
                this.editor.off(); /// unbinds all editor events
            } else {
                this.editor.on(); /// binds all editor events
            }
        }

        function disableEditor() {
            this.toggleEditor(true, false);
        }

        function enableEditor() {
            this.toggleEditor(false, true);
        }


        function createSpellchecker() {
            var t = this;
            t.isSpellChecked = true;
            var IgnoreAll = Builder.Resources.IgnoreAll;
            var IgnoreWord = Builder.Resources.IgnoreWord;
            var $suggestBox = $('<div class="spellchecker-suggestbox" style="display:none;">' +
                '<div class="words"></div>' +
                '<div class="footer">' +
                '<a href="#" class="ignore-word">' +
                IgnoreWord +
                "</a>" +
                '<a href="#" class="ignore-all" style="display:block;">' +
                IgnoreAll +
                "</a>" +
                "</div>" +
                "</div>");

            $suggestBox.find("div.footer a.ignore-word").click(t.getIgnoreWordClickHandler());
            $suggestBox.find("div.footer a.ignore-all").click(t.getIgnoreAllClickHandler());

            $("body").append($suggestBox);
            $("html").click(function() {
                $suggestBox.hide();
            });
            //$container.click(function () {
            //    $suggestBox.hide();
            //});
        }

        function dispose() {
            var t = this;
            t.setBadWordElement(null);
            t.isSpellChecked = false;

            var ed = t.editor;
            var $container = $(ed.getBody());
            $container.find("span.spellchecker-word-highlight").each(function() {
                var $span = $(this);
                $span.off().replaceWith($span.text());
            });

            var $suggestBox = $("div.spellchecker-suggestbox");
            $suggestBox.find("a").off();
            $suggestBox.remove();
        }

        this.isSpellChecked = false;
        this.check = function(editor) {
            var text = editor.$ed.text();
            text = "" + text; // Typecast to string
            text = text.replace(/\xA0|\s+|(&nbsp;)/mg, " "); // Convert whitespace
            text = text.replace(new RegExp("<[^>]+>", "g"), " "); // Strip HTML tags
            text = $.trim(text.replace(/\s{2,}/g, " ")); // remove extra whitespace
            // Remove numbers
            text = $.map(text.split(" "),
                function(word) {
                    return (/^\d+$/.test(word)) ? null : word;
                }).join(" ");

            text = $("<div />").html(text).html(); // Escape special characters.

            console.log(text);
            $.ajax({
                url: "https://languagetool.org/api/v2/check",
                type: "POST",
                data: "text=" + text + "&language=en-US"
            }).then(function(data) {
                    console.log(data);
                    if (data && data.matches && data.matches.length) {
                        $.each(data.matches,
                            function(ind, item) {
                                var replaceText =
                                    $.trim(item.context.text.substr(item.context.offset, item.context.length));
                                editor.$ed.html(editor.$ed.html().replace(new RegExp(replaceText, "g"),
                                    '<span class="spellchecker-word-highlight">' + replaceText + "</span>"));
                                editor.$ed.find("span.spellchecker-word-highlight").not("[data-event-bound]")
                                    .attr("data-event-bound", "y")
                                    .click(getBadWordClickHandler(editor, item.replacements));
                            });
                    }
                },
                function(xhr, status, error) {
                    console.log(xhr);
                    console.log(status);
                    console.log(error);
                });
            //Builder.Http.Post({ url: "https://languagetool.org/api/v2/check", data: { text: text, language: "en-US" } }).then(function (data) {
            //    if (data && data.d && data.d.length) {
            //        var $container = $(ed.getBody());
            //        $.each(data.d, function (ind, item) {
            //            $container.find('*').not('span.spellchecker-word-highlight').each(function () {
            //                _replaceText(this, new RegExp(item, 'g'), '<span class="spellchecker-word-highlight">' + item + '</span>');
            //            });
            //        });

            //        $container.find('span.spellchecker-word-highlight').click(t.getBadWordClickHandler());
            //    } else {
            //        t.toggleCheck();
            //        t.noIncorrectWord();
            //    }
            //}, function (xhr, status, error) {
            //    console.log(xhr);
            //    console.log(status);
            //    console.log(error);
            //});
        };

        function getSuggestions() {
            var t = this;
            var ed = t.editor;
            var badWordElement = t.getBadWordElement();
            var $badWordElement = $(badWordElement);

            $.ajax({
                type: "POST",
                url: "/resume/resume-editor.aspx/GetSuggestions",
                data: JSON.stringify({ "badword": $badWordElement.text() }),
                contentType: "application/json; charset=utf-8",
                dataType: "json"
            }).then(function(data) {
                    var $suggestBox = $("div.spellchecker-suggestbox");
                    var $words = $suggestBox.find("div.words");
                    $words.empty();

                    if (data && data.d && data.d.length) {
                        for (var i = 0, len = Math.min(data.d.length, 5); i < len; i++) {
                            $words.append('<a href="#">' + data.d[i] + "</a>");
                        }
                    }

                    $words.find("a").click(t.getReplaceWordClickHandler());

                    t.positionSuggestBox(badWordElement);
                    $suggestBox.fadeIn(200);
                    $suggestBox.show();
                },
                function(xhr, status, error) {
                    console.log(xhr);
                    console.log(status);
                    console.log(error);
                });
        }

        function positionSuggestBox(editor, elem, replacements) {
            var $suggestBox = $("div.spellchecker-suggestbox");
            var $words = $suggestBox.find("div.words");
            $words.empty();
            if (replacements && replacements.length) {
                for (var i = 0, len = Math.min(replacements.length, 5); i < len; i++) {
                    $words.append('<a href="#">' + replacements[i].value + "</a>");
                }
            }
            $words.find("a").click(getReplaceWordClickHandler());

            var box = elem.getBoundingClientRect();

            var body = document.body;
            var docEl = document.documentElement;

            var scrollTop = window.pageYOffset || docEl.scrollTop || body.scrollTop;
            var scrollLeft = window.pageXOffset || docEl.scrollLeft || body.scrollLeft;

            var clientTop = docEl.clientTop || body.clientTop || 0;
            var clientLeft = docEl.clientLeft || body.clientLeft || 0;

            var top = box.top + scrollTop - clientTop - 40;
            var left = box.left + scrollLeft - clientLeft;
            $suggestBox.css({
                top: top,
                left: left
            }).show();
        }

        function getBadWordElement() {
            var t = this;
            return t.badWordElement;
        }

        function setBadWordElement(elemNode) {
            var t = this;
            t.badWordElement = elemNode;
        }

        function getBadWordClickHandler(editor, replacements) {
            return (function(editor, replacements) {
                return function(e) {
                    preventDefault(e);
                    positionSuggestBox(editor, this, replacements);
                };
            })(editor, replacements);
        }

        function getReplaceWordClickHandler() {
            var t = this;

            return function(e) {
                t.preventDefault(e);

                var $word = $(this);
                var wordToReplace = $word.text();

                var badWordElement = t.getBadWordElement();
                var $badWordElement = $(badWordElement);
                $badWordElement.replaceWith(wordToReplace);

                var $suggestBox = $("div.spellchecker-suggestbox");
                $suggestBox.hide();

                var ed = t.editor;
                var $container = $(ed.getBody());
                if ($container.find("span.spellchecker-word-highlight").length === 0) {
                    t.toggleCheck();
                }
            };
        }

        function getIgnoreWordClickHandler() {
            var t = this;

            return function(e) {
                t.preventDefault(e);

                var badWordElement = t.getBadWordElement();
                var $badWordElement = $(badWordElement);
                $badWordElement.replaceWith($badWordElement.text());

                var $suggestBox = $("div.spellchecker-suggestbox");
                $suggestBox.hide();

                var ed = t.editor;
                var $container = $(ed.getBody());
                if ($container.find("span.spellchecker-word-highlight").length === 0) {
                    t.toggleCheck();
                }
            };
        }

        function getIgnoreAllClickHandler() {
            var t = this;

            return function(e) {
                t.preventDefault(e);

                t.toggleCheck();
            };
        }

        function preventDefault(e) {
            if (e && e.preventDefault) {
                e.preventDefault();
            }
        }
    }

    function editorFactory(id) {
        var $editor = $("#" + id);
        this.Init = function(obj) {
            if (obj.placeholder) {
                $("#" + obj.id).attr("placeholder", obj.placeholder);
            }
            $editor.trumbowyg({
                resetCss: true,
                removeformatPasted: true,
                btns: [
                    ["bold", "italic", "underline"], ["unorderedList", "orderedList"], ["undo", "redo"], ["spellcheck"]
                ]
            });
        };
        this.GetContent = function() {
            return $editor.trumbowyg("html");
        };
        this.SetContent = function(content) {
            $editor.trumbowyg("html", content);
        };
        this.AddParagraph = function(content) {
            var $testDiv = $("<div />");
            $testDiv.html(this.GetContent());
            var $existingPara = $testDiv.find("p").last();
            if ($existingPara.length && $.trim($existingPara.text()).length === 0) {
                $existingPara.html(content);
            } else {
                $("<p/>").html(content).appendTo($testDiv);
            }
            this.SetContent($testDiv.html());
        };
        this.AddList = function(content) {
            var $testDiv = $("<div />");
            $testDiv.html(this.GetContent());
            var $last = $testDiv.children().last();
            if ($last.is("ul")) {
                $("<li/>").html(content).appendTo($last);
            } else {
                $testDiv.append($("<ul/>").append($("<li/>").append(content)));
            }
            this.SetContent($testDiv.html());
        };
    }

    function trumbowygEditorFactory() {
        function addSpellCheckPlugin(spellchecker) {
            $.extend(true,
                $.trumbowyg,
                {
                    langs: {
                        // jshint camelcase:false
                        en: {
                            spellcheck: "Code sample <pre>"
                        },
                        fr: {
                            spellcheck: "Exemple de code"
                        },
                        it: {
                            spellcheck: "Codice <pre>"
                        }
                    },
                    plugins: {
                        spellcheck: {
                            init: function(trumbowyg) {
                                var btnDef = {
                                    fn: function() {
                                        spellchecker.check(trumbowyg);
                                    },
                                    title: "spellcheck"
                                };
                                trumbowyg.addBtnDef("spellcheck", btnDef);
                            }
                        }
                    }
                });
        }

        var editors = {};
        this.Initialize = function() {
            var spellchecker = new spellCheckFactory();
            addSpellCheckPlugin(spellchecker);
        };
        this.Init = function(id, obj) {
            editors[id] = new editorFactory(id);
            editors[id].Init(obj);
        };
        this.GetContent = function(id) {
            return editors[id].GetContent();
        };
        this.SetContent = function(id, content) {
            editors[id].SetContent(content);
        };
        this.AddParagraph = function(id, content) {
            editors[id].AddParagraph(content);
        };
        this.AddList = function(id, content) {
            editors[id].AddList(content);
        };
    };

    win.Builder = win.Builder || {};
    var Builder = win.Builder;
    Builder.Editor = new trumbowygEditorFactory();
    Builder.Editor.Initialize();
})(window, window.jQuery);