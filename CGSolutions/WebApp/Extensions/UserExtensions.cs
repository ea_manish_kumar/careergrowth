﻿using Microsoft.AspNet.Identity;
using System.Security.Claims;
using System.Security.Principal;
using WebApp.Constants;

namespace WebApp.Extensions
{
    public static class UserExtensions
    {
        public static bool IsSignedIn(this IPrincipal user)
        {
            return user != null && user.Identity != null && user.Identity.IsAuthenticated;
        }

        public static int GetId(this IPrincipal user)
        {
            if (user != null && user.Identity != null && user.Identity.IsAuthenticated && (user.Identity is ClaimsIdentity claimsIdentity))
            {
                if (int.TryParse(claimsIdentity.FindFirstValue(CustomClaimTypes.UserId), out int userId) && userId > 0)
                    return userId;
            }
            return -1;
        }

        public static int GetLoginId(this IPrincipal user)
        {
            if (user != null && user.Identity != null && user.Identity.IsAuthenticated && (user.Identity is ClaimsIdentity claimsIdentity))
            {
                if (int.TryParse(claimsIdentity.FindFirstValue(CustomClaimTypes.UserLoginId), out int userLoginId) && userLoginId > 0)
                    return userLoginId;
            }
                return -1;
        }

        public static string GetDisplayName(this IPrincipal user)
        {
            if (user != null && user.Identity != null && user.Identity.IsAuthenticated && (user.Identity is ClaimsIdentity claimsIdentity))
                return claimsIdentity.FindFirstValue(CustomClaimTypes.DisplayName);
            return null;
        }

        public static string GetUserName(this IPrincipal user)
        {
            if (user != null && user.Identity != null && user.Identity.IsAuthenticated && (user.Identity is ClaimsIdentity claimsIdentity))
                return claimsIdentity.FindFirstValue(CustomClaimTypes.UserName);
            else
                return string.Empty;
        }

        public static string GetEmail(this IPrincipal user)
        {
            return user.GetUserName();
        }
    }
}