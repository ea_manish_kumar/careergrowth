﻿using App.Utility.Extensions;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Linq;
using System.Web.Routing;

namespace WebApp.Extensions
{
    public static class QueryStringExtensions
    {
        public static RouteValueDictionary ToRouteValues(this NameValueCollection source)
        {
            return source.ToRouteValues(null, null);
        }

        public static RouteValueDictionary ToRouteValues(this NameValueCollection source, IList<string> excludeKeys)
        {
            return source.ToRouteValues(null, excludeKeys);
        }

        public static RouteValueDictionary ToRouteValues(this NameValueCollection source, IDictionary<string, object> includeRouteValues)
        {
            return source.ToRouteValues(includeRouteValues, null);
        }

        public static RouteValueDictionary ToRouteValues(this NameValueCollection source, IDictionary<string, object> includeRouteValues, IList<string> excludeKeys)
        {
            if (source != null)
            {
                var qs = new Dictionary<string, object>();
                if (source.Keys.Count > 0)
                {
                    IEnumerable<string> allKeys = null;
                    if (excludeKeys.IsNotNullOrEmpty())
                        allKeys = source.AllKeys.Where(k => !excludeKeys.Contains(k));
                    else
                        allKeys = source.AllKeys.AsEnumerable();
                    foreach (var key in allKeys)
                        qs.Add(key, source[key]);
                }
                if (includeRouteValues != null)
                {
                    foreach (var includedRoute in includeRouteValues)
                        qs.Add(includedRoute.Key, includedRoute.Value);
                }
                return new RouteValueDictionary(qs);
            }
            return null;
        }
    }
}