﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Linq.Expressions;
using System.Reflection;
using System.Text;
using System.Web.Mvc;
using System.Web.Routing;
using Microsoft.Web.Mvc;

namespace WebApp.Extensions
{
    public static class RouteCollectionExtensions
    {
        public static CGRoute Action<TController>(this RouteCollection routes,
            Expression<Func<TController, object>> action, string url = "") where TController : IController, new()
        {
            var method = action.Body as MethodCallExpression;

            var route = new CGRoute(routes, typeof(TController), method.Method);
            route.SetUrl(url);

            return route;
        }

        public static CGRoute ActionWithDefaultParameters<TController>(this RouteCollection routes,
            Expression<Func<TController, object>> action, string url = "") where TController : IController, new()
        {
            var route = Action(routes, action, url);
            var method = action.Body as MethodCallExpression;
            var parameters = method.Method.GetParameters();
            var arguments = method.Arguments.ToArray();

            for (var i = 0; i < parameters.Length; ++i)
                if (arguments[i] is ConstantExpression)
                    route.AddDefaultParameterValue(parameters[i].Name, (arguments[i] as ConstantExpression).Value);
                else if (arguments[i] is MemberExpression)
                    route.AddDefaultParameterValue(parameters[i].Name,
                        Expression.Lambda(arguments[i]).Compile().DynamicInvoke());

            return route;
        }
    }

    public sealed class CGRoute
    {
        private readonly RouteCollection routes;

        public CGRoute(RouteCollection routes, Type controllerType, MethodInfo actionMethod)
        {
            this.routes = routes;
            ControllerType = controllerType;
            Action = actionMethod;
            Defaults = new RouteValueDictionary();
            Constraints = new RouteValueDictionary();
            var controllerName = controllerType.Name.Remove(controllerType.Name.LastIndexOf("Controller"));
            var actionName = actionMethod.Name;
            Name = string.Concat(controllerName, "_", actionName);
            Url = string.Concat(controllerName, "/", actionName);
            //this.Url = String.Join("/", actionMethod.GetParameters().Select(x => String.Concat("{", x.Name, "}")));
        }

        public string Url { get; private set; }

        public string Name { get; private set; }

        public RouteValueDictionary Defaults { get; }

        public RouteValueDictionary Constraints { get; }

        public Type ControllerType { get; }

        public MethodInfo Action { get; }

        public CGRoute SetUrl(string url)
        {
            if (string.IsNullOrWhiteSpace(url) == false) Url = url;

            return this;
        }

        public CGRoute SetName(string name)
        {
            Name = name;

            return this;
        }

        public CGRoute AddDefaultParameterValue(string parameterName, object value)
        {
            Defaults[parameterName] = value;

            return this;
        }

        public CGRoute SetOptionalParameter(string parameterName)
        {
            Defaults[parameterName] = UrlParameter.Optional;

            return this;
        }

        public void Map()
        {
            var route = routes.MapRoute(Name, Url, null, null);

            foreach (var @default in Defaults) route.Defaults[@default.Key] = @default.Value;

            foreach (var constraint in Constraints) route.Constraints[constraint.Key] = constraint.Value;

            route.Defaults["Controller"] = ControllerType.Name.Remove(ControllerType.Name.LastIndexOf("Controller"));
            route.Defaults["Action"] = Action.Name;
        }

        /*

        */

        public CGRoute AddConstraints(object constraints)
        {
            var props = TypeDescriptor.GetProperties(constraints);

            foreach (PropertyDescriptor prop in props) AddDefaultParameterValue(prop.Name, prop.GetValue(constraints));

            return this;
        }

        public CGRoute AddConstraint<TRouteConstraint>() where TRouteConstraint : IRouteConstraint, new()
        {
            Constraints[Guid.NewGuid().ToString()] = new TRouteConstraint();

            return this;
        }

        public CGRoute AddConstraint(IRouteConstraint constraint)
        {
            Constraints[Guid.NewGuid().ToString()] = constraint;

            return this;
        }

        public CGRoute AddConstraint(string parameterName, string regularExpression)
        {
            Constraints[parameterName] = regularExpression;

            return this;
        }

        public CGRoute AddConstraint(string parameterName, IRouteConstraint constraint)
        {
            Constraints[parameterName] = constraint;

            return this;
        }
    }
}