﻿using System;
using System.Web;
using WebApp.Models;

namespace WebApp.Extensions
{
    public static class BrowserExtensions
    {
        public static EditorType GetEditorType(this HttpBrowserCapabilitiesBase request)
        {
            var browser = request.Browser;
            if (browser.Equals("Firefox", StringComparison.OrdinalIgnoreCase))
                return EditorType.HtmlEditor;
            return EditorType.HtmlEditor;
        }

        //public static Browser GetBrowser(this HttpRequestBase request)
        //{
        //    var userAgent = request.UserAgent;
        //    if (userAgent!=null && userAgent.Contains("UCBrowser"))
        //        return Browser.UCBrowser;
        //    else
        //        return Browser.All;
        //}
    }
}