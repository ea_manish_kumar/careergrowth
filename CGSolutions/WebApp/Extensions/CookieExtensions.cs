﻿using System;
using System.Web;
using WebApp.Utility;

namespace WebApp.Extensions
{
    public static class CookieExtensions
    {
        public static string Read(this HttpCookieCollection cookies, string cookieName)
        {
            var cookie = cookies.Get(cookieName);
            if (cookie != null)
                return cookie.Value;
            return null;
        }

        public static void Write(this HttpCookieCollection cookies, string cookieName, string value)
        {
            cookies.Write(cookieName, value, false);
        }

        public static void Write(this HttpCookieCollection cookies, string cookieName, string value, bool isPermanent)
        {
            var cookie = cookies.Get(cookieName);
            if (cookie == null)
                cookie = new HttpCookie(cookieName);
            cookie.Value = value;
            cookie.Secure = true;
            cookie.HttpOnly = true;
            if (isPermanent)
                cookie.Expires = DateTime.Now.AddYears(5);
            cookies.Set(cookie);
        }

        public static string Decrypt(this HttpCookieCollection cookies, string cookieName)
        {
            var cookie = cookies.Get(cookieName);
            if (cookie != null)
                return CryptoGraphyUtility.Unprotect(cookie.Value);
            return null;
        }

        public static void Encrypt(this HttpCookieCollection cookies, string cookieName, string value)
        {
            cookies.Write(cookieName, CryptoGraphyUtility.Protect(value), false);
        }

        public static void Delete(this HttpCookieCollection cookies, string cookieName)
        {
            var cookie = cookies.Get(cookieName);
            if (cookie == null)
                cookie = new HttpCookie(cookieName);
            cookie.Expires = DateTime.UtcNow.AddMonths(-1);
            cookie.Value = string.Empty;
            cookies.Set(cookie);
        }
    }
}