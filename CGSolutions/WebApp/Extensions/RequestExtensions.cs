﻿using System.Linq;
using System.Net;
using System.Web;
using Utilities.Constants;

namespace WebApp.Extensions
{
    public static class RequestExtensions
    {
        public static bool HasRequestCookie(this HttpRequestBase httpRequest, string cookieName)
        {
            string requestCookie = null;
            if (httpRequest.ServerVariables != null && httpRequest.ServerVariables["HTTP_COOKIE"] != null)
                requestCookie = httpRequest.ServerVariables["HTTP_COOKIE"];
            if (httpRequest.Headers != null && httpRequest.Headers["COOKIE"] != null)
                requestCookie = httpRequest.Headers["COOKIE"];
            if (!string.IsNullOrEmpty(requestCookie))
                return requestCookie.Split(';').Any(s => !string.IsNullOrEmpty(s) && s.Trim().StartsWith(cookieName));
            return false;
        }
        public static string GetUserIPAddress(this HttpRequestBase httpRequest)
        {
            var ipAddress = string.Empty;
            var ipAddressList =
                !string.IsNullOrEmpty(httpRequest.ServerVariables[RequestHeader.HTTP_X_FORWARDED_FOR])
                    ? httpRequest.ServerVariables[RequestHeader.HTTP_X_FORWARDED_FOR]
                    : httpRequest.ServerVariables[RequestHeader.REMOTE_ADDR];
            if (!string.IsNullOrEmpty(ipAddressList))
            {
                var clientIpAddress = ipAddressList.Split(',')[0];
                ipAddress = StripPortFromIPAddress(clientIpAddress.Trim());
            }

            if (string.IsNullOrEmpty(ipAddress) || ipAddress == "::1") ipAddress = "127.0.0.1";
            return ipAddress;
        }


        private static string StripPortFromIPAddress(string ipAddress)
        {
            var splitList = ipAddress.Split(':');
            if (splitList.Length == 1) return ipAddress;

            if (splitList.Length == 2)
            {
                ipAddress = splitList[0];
            }
            else
            {
                if (IPAddress.TryParse(ipAddress, out IPAddress ip))
                    ipAddress = ip.ToString();
            }

            return ipAddress;
        }

    }
}