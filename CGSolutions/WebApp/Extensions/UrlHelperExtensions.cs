﻿using Microsoft.Web.Mvc;
using System;
using System.Collections.Generic;
using System.Linq.Expressions;
using System.Text;
using System.Web.Mvc;
using System.Web.Routing;

namespace WebApp.Extensions
{
    public static class UrlHelperExtensions
    {
        public static string BuildUrl<TController>(this UrlHelper urlHelper, Expression<Action<TController>> action, string routeName)
            where TController : Controller
        {
            RouteCollection routeCollection = null;
            // Check to ensure this route exists first. If not, don't filter the routes.
            if (urlHelper.RouteCollection[routeName] != null)
                routeCollection = new RouteCollection { urlHelper.RouteCollection[routeName] };
            if (routeCollection == null)
                routeCollection = urlHelper.RouteCollection;
            return LinkBuilder.BuildUrlFromExpression(null, routeCollection, action);
        }

        public static string BuildUrl<TController>(this UrlHelper helper, Expression<Action<TController>> action, IDictionary<string, string> routeQSValues, string routeName)
            where TController : Controller
        {
            var url = BuildUrl(helper, action, routeName);
            if (routeQSValues != null && routeQSValues.Count > 0)
            {
                var queryString = new StringBuilder("?");
                foreach (var kvp in routeQSValues) queryString.Append(string.Format("{0}={1}&", kvp.Key, kvp.Value));
                var finalQueryString = queryString.ToString().TrimEnd('&');
                return url + finalQueryString;
            }

            return url;
        }
    }
}