﻿using System;

namespace WebApp.Extensions
{
    public static class UriExtensions
    {
        public static string GetRootUrl(this Uri uri)
        {
            return uri.GetLeftPart(UriPartial.Authority);
        }

        public static string GetRootUrl(this Uri uri, string relativeVirtualPath)
        {
            return string.Concat(uri.GetLeftPart(UriPartial.Authority), relativeVirtualPath);
        }
    }
}