﻿using CommonModules.Configuration;
using Microsoft.AspNet.Identity;
using Microsoft.Owin;
using Microsoft.Owin.Security;
using Microsoft.Owin.Security.Cookies;
using Owin;
using System;
using System.Net;
using System.Web;
using Utilities.Constants;
using WebApp;
using WebApp.Constants;

[assembly: OwinStartup(typeof(Startup))]

namespace WebApp
{
    public class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            double.TryParse(ConfigManager.GetConfig("SESSION_EXTEND"), out double minutes);
            app.UseCookieAuthentication(new CookieAuthenticationOptions
            {
                AuthenticationMode = AuthenticationMode.Active,
                AuthenticationType = DefaultAuthenticationTypes.ApplicationCookie,
                LoginPath = new PathString("/account/login"),
                CookieName = CookieConstants.ApplicationCookie,
                CookieSecure = CookieSecureOption.Always,
                ReturnUrlParameter = CookieAuthenticationDefaults.ReturnUrlParameter,
                ExpireTimeSpan = TimeSpan.FromMinutes(minutes),
                SlidingExpiration = true,
                Provider = new CookieAuthenticationProvider
                {
                    OnApplyRedirect = context =>
                    {
                        if (!IsAjaxRequest(context.Request))
                        {
                            if (context.Response.StatusCode == (int) HttpStatusCode.Unauthorized)
                            {
                                var httpContext =
                                    context.OwinContext.Get<HttpContextBase>(typeof(HttpContextBase).FullName);
                                if (httpContext != null && httpContext.Items["LocalizedUrl"] != null)
                                {
                                    var localizedUrl = httpContext.Items["LocalizedUrl"] as string;
                                    if (!string.IsNullOrEmpty(localizedUrl))
                                    {
                                        var uri = new Uri(context.RedirectUri);
                                        var redirectUri = uri.GetLeftPart(UriPartial.Path);
                                        redirectUri += "?" + context.Options.ReturnUrlParameter + "=" +
                                                       HttpUtility.UrlEncode(localizedUrl);
                                        context.Response.Redirect(redirectUri);
                                    }
                                    else
                                    {
                                        context.Response.Redirect(context
                                            .RedirectUri); //Safe check if LocalizedUrl is empty or null
                                    }
                                }
                                else
                                {
                                    context.Response.Redirect(context.RedirectUri); //when URL Rewriter is off
                                }
                            }
                            else
                            {
                                context.Response.Redirect(context.RedirectUri); // ApplyRedirect Default Behavior
                            }
                        }
                        else if (context.Response.StatusCode == (int) HttpStatusCode.Unauthorized)
                        {
                            var redirectUri = new Uri(context.RedirectUri).GetLeftPart(UriPartial.Path) + "?" +
                                              context.Options.ReturnUrlParameter + "=";
                            context.Response.Headers.Add("x-ajax-redirect", new[] {redirectUri});
                        }
                    }
                }
            });
        }

        private static bool IsAjaxRequest(IOwinRequest request)
        {
            var query = request.Query;
            if (query != null && query["X-Requested-With"] == "XMLHttpRequest") return true;
            var headers = request.Headers;
            return headers != null && headers["X-Requested-With"] == "XMLHttpRequest";
        }
    }
}