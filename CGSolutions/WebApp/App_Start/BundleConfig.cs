﻿using CommonModules.Configuration;
using System;
using System.Web.Optimization;
using Utilities.Constants;
using WebApp.Constants;

namespace WebApp
{
    public static class BundleConfig
    {
        public static void RegisterBundles(BundleCollection bundles)
        {
            BundleTable.EnableOptimizations = Convert.ToBoolean(ConfigManager.GetConfig(ConfigKeys.EnableBundling).ToLower());

            bundles.Add(new StyleBundle(BundleConstants.JQueryUICss).Include("~/css/jquery-ui.min.css"));
            bundles.Add(new StyleBundle(BundleConstants.EditorCss).Include("~/trumbowyg/ui/trumbowyg.css", "~/trumbowyg/ui/custom.css"));

            bundles.Add(new StyleBundle(BundleConstants.SiteCss).Include("~/css/bootstrap.min.css", "~/css/font.css", "~/css/common.css", "~/css/style.css"));
            bundles.Add(new StyleBundle(BundleConstants.LpCss).Include("~/css/bootstrap.min.css", "~/css/common.css", "~/css/home.css"));
            bundles.Add(new StyleBundle(BundleConstants.WizardCss).Include("~/css/font-awesome.min.css", "~/css/bootstrap.min.css", "~/css/font.css", "~/css/common.css", "~/css/style.css"));
            bundles.Add(new StyleBundle(BundleConstants.StaticCss).Include("~/css/font-awesome.min.css", "~/css/bootstrap.min.css", "~/css/common.css", "~/css/static.css", "~/css/style.css"));
            bundles.Add(new StyleBundle(BundleConstants.OrderCss).Include("~/css/font-awesome.min.css", "~/css/bootstrap.min.css", "~/css/font.css", "~/css/style.css", "~/css/common.css", "~/css/home.css"));

            bundles.Add(new ScriptBundle(BundleConstants.JQueryUISortableJs).Include("~/scripts/jquery-ui.min.js", "~/scripts/plugins/jquery-ui.sortable.min.js"));
            bundles.Add(new ScriptBundle(BundleConstants.JQueryAutoCompleteExampleJs).Include("~/scripts/jquery-ui.min.js", "~/scripts/examples.js"));
            bundles.Add(new ScriptBundle(BundleConstants.EditorJs).Include("~/trumbowyg/trumbowyg.js", "~/trumbowyg/trumbowyg-wrapper.js"));

            bundles.Add(new ScriptBundle(BundleConstants.MainBundleJs).Include("~/scripts/jquery.min.js", "~/scripts/bootstrap.min.js", "~/scripts/global.js", "~/scripts/journey.js", "~/scripts/document/main.js"));
            bundles.Add(new ScriptBundle(BundleConstants.ValidationJs).Include("~/scripts/jquery.validate.min.js")); 
            bundles.Add(new ScriptBundle(BundleConstants.DocumentTemplateJs).Include("~/scripts/document/template.js"));

            #region Resume funnel
            bundles.Add(new ScriptBundle(BundleConstants.ResumeUserDetailsJS).Include("~/scripts/resume/user-details.js"));
            bundles.Add(new ScriptBundle(BundleConstants.ResumeWorkDetailsJS).Include("~/scripts/resume/work-details.js"));
            bundles.Add(new ScriptBundle(BundleConstants.ResumeWorkReviewJS).Include("~/scripts/resume/work-review.js"));
            bundles.Add(new ScriptBundle(BundleConstants.ResumeAcademicDetailsJS).Include("~/scripts/resume/academic-details.js"));
            bundles.Add(new ScriptBundle(BundleConstants.ResumeAcademicReviewJS).Include("~/scripts/resume/academic-review.js"));
            #endregion

            #region Old Resume Funnel JS
            bundles.Add(new ScriptBundle(BundleConstants.SelectTemplateJs).Include("~/scripts/journey/select-template.js"));
            bundles.Add(new ScriptBundle(BundleConstants.UserDetailsJs).Include("~/scripts/places/google.js", "~/scripts/journey/user-details.js"));
            bundles.Add(new ScriptBundle(BundleConstants.WorkDetailsJs).Include("~/scripts/places/google.js", "~/scripts/journey/work-details.js"));
            bundles.Add(new ScriptBundle(BundleConstants.WorkReviewJs).Include("~/scripts/journey/work-review.js"));
            bundles.Add(new ScriptBundle(BundleConstants.AcademicDetailsJs).Include("~/scripts/places/google.js", "~/scripts/journey/academic-details.js"));
            bundles.Add(new ScriptBundle(BundleConstants.AcademicReviewJs).Include("~/scripts/journey/academic-review.js"));
            bundles.Add(new ScriptBundle(BundleConstants.CompetenciesJs).Include("~/scripts/journey/competencies.js"));
            bundles.Add(new ScriptBundle(BundleConstants.SummaryJs).Include("~/scripts/journey/summary.js"));
            bundles.Add(new ScriptBundle(BundleConstants.CustomSectionJs).Include("~/scripts/journey/custom.js"));
            #endregion

            #region Pages
            bundles.Add(new ScriptBundle(BundleConstants.RegisterJs).Include("~/scripts/pages/register.js"));
            bundles.Add(new ScriptBundle(BundleConstants.LoginJs).Include("~/scripts/pages/login.js"));

            bundles.Add(new ScriptBundle(BundleConstants.PreviewJs).Include("~/scripts/resume/preview.js"));
            bundles.Add(new ScriptBundle(BundleConstants.DashboardJs).Include("~/scripts/pages/dashboard.js"));
            bundles.Add(new ScriptBundle(BundleConstants.HelpJs).Include("~/scripts/help.js"));
            #endregion

        }
    }
}