﻿using App.Data;
using App.Data.EF;
using App.Model.Filters;
using App.Model.Users;
using App.Services;
using SimpleInjector;
using SimpleInjector.Integration.Web;
using System.Reflection;
using System.Web;
using System.Web.Mvc;
using WebApp;
using WebApp.Extensions;
using WebApp.Managers;

[assembly: PreApplicationStartMethod(typeof(DependencyConfig), "Initialize")]

namespace WebApp
{
    public static class DependencyConfig
    {
        public static void Initialize()
        {
            var container = BuildContainer();
            DependencyResolver.SetResolver(new SimpleInjectorDependencyResolver(container));
        }

        private static Container BuildContainer()
        {
            var container = new Container();
            container.Options.DefaultScopedLifestyle = new WebRequestLifestyle();

            /// Filters
            container.Register(() => GetUserFilter(), Lifestyle.Scoped);

            /// Data
            container.Register<ApplicationDbContext>(Lifestyle.Scoped);
            container.Register<ITrackingDB, TrackingDB>(Lifestyle.Scoped);
            container.Register<IUserDB, UserDB>(Lifestyle.Scoped);
            container.Register<IDocumentDB, DocumentDB>(Lifestyle.Scoped);
            container.Register<IContentDB, ContentDB>(Lifestyle.Scoped);
            container.Register<IBillingDB, BillingDB>(Lifestyle.Scoped);

            /// Services
            container.Register<ITrackingService, TrackingService>(Lifestyle.Scoped);
            container.Register<IUserService, UserService>(Lifestyle.Scoped);
            container.Register<IDocumentService, DocumentService>(Lifestyle.Scoped);
            container.Register<IContentService, ContentService>(Lifestyle.Scoped);
            container.Register<IBillingService, BillingService>(Lifestyle.Scoped);

            /// Managers
            //container.Register(() => HttpContext.Current?.GetOwinContext().Authentication, Lifestyle.Scoped);
            container.Register<ISignInManager, SignInManager>(Lifestyle.Scoped);

            /// Options
            container.RegisterSingleton<SignInOptions>(() => new SignInOptions
            {
                TokenExpireMinutes = 30,
                LockoutExpireMinutes = 30,
                MaxPasswordFailedAttempts = 5,
                SessionExpireMinutes = 30,
                PersistentSessionExpireMinutes = 120
            });

            container.RegisterMvcControllers(Assembly.GetExecutingAssembly());
            //container.RegisterWebApiControllers(GlobalConfiguration.Configuration);
            //container.RegisterMvcIntegratedFilterProvider();

            container.Verify();

            return container;
        }

        public static UserFilter GetUserFilter()
        {
            var httpContext = HttpContext.Current;
            if (httpContext != null && httpContext.User != null)
            {
                return new UserFilter
                {
                    UserId = httpContext.User.GetId()
                };
            }
            return UserFilter.Empty;
        }
    }
}