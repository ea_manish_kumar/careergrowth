﻿using System.Web.Mvc;
using System.Web.Routing;
using WebApp.Components;
using WebApp.Constants;
using WebApp.Controllers;
using WebApp.Extensions;

namespace WebApp
{
    public class RouteConfig
    {
        public static void RegisterRoutes(RouteCollection routes)
        {
            #region Page Routes

            /// Funnel Routes

            /// How It Works
            routes.Action<WizardController>(wiz => wiz.HowItWorks(), RouteUrls.HowItWorks)
                .SetName(RouteNames.HowItWorks).Map();

            /// Choose Template
            routes.Action<WizardController>(wiz => wiz.ChooseTemplate(null, null),
                    RouteUrls.ChooseTemplate + "/{mode}/{docId}")
                .AddConstraint("mode", new IsValidWizardModeConstraint())
                .AddConstraint("docId", RegexConstants.RequiredInteger)
                .SetName(RouteNames.ChooseTemplate_Mode).Map();
            routes.Action<WizardController>(wiz => wiz.ChooseTemplate(null, null), RouteUrls.ChooseTemplate)
                .SetName(RouteNames.ChooseTemplate).Map();

            /// Contact
            routes.Action<WizardController>(wiz => wiz.Contact(null, null), RouteUrls.Contact + "/{mode}/{docId}")
                .AddConstraint("mode", new IsValidWizardModeConstraint())
                .AddConstraint("docId", RegexConstants.RequiredInteger)
                .SetName(RouteNames.Contact_Mode).Map();
            routes.Action<WizardController>(wiz => wiz.Contact(null, null), RouteUrls.Contact)
                .SetName(RouteNames.Contact).Map();

            /// Experience
            routes.Action<WizardController>(wiz => wiz.Experience(null, null, null),
                    RouteUrls.Experience + "/{mode}/{docId}/{paragraphId}")
                .AddConstraint("mode", new IsValidWizardReviewModeConstraint())
                .AddConstraint("docId", RegexConstants.RequiredInteger)
                .SetOptionalParameter("paragraphId").AddConstraint("docId", RegexConstants.OptionalInteger)
                .SetName(RouteNames.Experience_Mode).Map();
            routes.Action<WizardController>(wiz => wiz.Experience(null, null, null), RouteUrls.Experience)
                .SetName(RouteNames.Experience).Map();

            /// Experience-Review
            routes.Action<WizardController>(wiz => wiz.ExperienceReview(null, null),
                    RouteUrls.ExperienceReview + "/{mode}/{docId}")
                .AddConstraint("mode", new IsValidWizardModeConstraint())
                .AddConstraint("docId", RegexConstants.RequiredInteger)
                .SetName(RouteNames.ExperienceReview_Mode).Map();
            routes.Action<WizardController>(wiz => wiz.ExperienceReview(null, null), RouteUrls.ExperienceReview)
                .SetName(RouteNames.ExperienceReview).Map();

            /// Education
            routes.Action<WizardController>(wiz => wiz.Education(null, null, null),
                    RouteUrls.Education + "/{mode}/{docId}/{paragraphId}")
                .AddConstraint("mode", new IsValidWizardReviewModeConstraint())
                .AddConstraint("docId", RegexConstants.RequiredInteger)
                .SetOptionalParameter("paragraphId").AddConstraint("docId", RegexConstants.OptionalInteger)
                .SetName(RouteNames.Education_Mode).Map();
            routes.Action<WizardController>(wiz => wiz.Education(null, null, null), RouteUrls.Education)
                .SetName(RouteNames.Education).Map();

            /// Education-Review
            routes.Action<WizardController>(wiz => wiz.EducationReview(null, null),
                    RouteUrls.EducationReview + "/{mode}/{docId}")
                .AddConstraint("mode", new IsValidWizardModeConstraint())
                .AddConstraint("docId", RegexConstants.RequiredInteger)
                .SetName(RouteNames.EducationReview_Mode).Map();
            routes.Action<WizardController>(wiz => wiz.EducationReview(null, null), RouteUrls.EducationReview)
                .SetName(RouteNames.EducationReview).Map();

            /// Skill-Description
            routes.Action<WizardController>(wiz => wiz.SkillDescription(null, null),
                    RouteUrls.SkillDescription + "/{mode}/{docId}")
                .AddConstraint("mode", new IsValidWizardModeConstraint())
                .AddConstraint("docId", RegexConstants.RequiredInteger)
                .SetName(RouteNames.SkillDescription_Mode).Map();
            routes.Action<WizardController>(wiz => wiz.SkillDescription(null, null), RouteUrls.SkillDescription)
                .SetName(RouteNames.SkillDescription).Map();

            /// Summary-Description
            routes.Action<WizardController>(wiz => wiz.SummaryDescription(null, null),
                    RouteUrls.SummaryDescription + "/{mode}/{docId}")
                .AddConstraint("mode", new IsValidWizardModeConstraint())
                .AddConstraint("docId", RegexConstants.RequiredInteger)
                .SetName(RouteNames.SummaryDescription_Mode).Map();
            routes.Action<WizardController>(wiz => wiz.SummaryDescription(null, null), RouteUrls.SummaryDescription)
                .SetName(RouteNames.SummaryDescription).Map();

            /// Other Section Routes
            routes.Action<WizardController>(wiz => wiz.OtherSection(null, null, null, null),
                    "{sectionName}/{mode}/{docId}")
                .AddConstraint("mode", new IsValidWizardModeConstraint())
                .AddConstraint("sectionName", new IsValidOtherSectionUrlConstraint())
                .AddConstraint("docId", RegexConstants.RequiredInteger)
                .SetName(RouteNames.OtherSection_Mode).Map();

            /// Dashboard Folder Routes
            routes.Action<DashboardController>(res => res.Home(), RouteUrls.Dashboard).SetName(RouteNames.Dashboard)
                .Map();

            /// Order Folder Routes
            routes.Action<OrderController>(ord => ord.Routing(0, 0, null, null, null)).SetName(RouteNames.OrderRouting)
                .Map();
            routes.Action<OrderController>(ord => ord.SelectPlan(), RouteUrls.SelectPlan).SetName(RouteNames.SellPage)
                .Map();
            routes.Action<OrderController>(ord => ord.PlanSelected(null), RouteUrls.PlanSelected)
                .SetName(RouteNames.PlanSelected).Map();
            routes.Action<OrderController>(ord => ord.Checkout(0), RouteUrls.Checkout + "/{orderId}")
                .AddConstraint("orderId", RegexConstants.RequiredInteger).SetName(RouteNames.Checkout).Map();
            routes.Action<OrderController>(ord => ord.Confirmation(), RouteUrls.OrderConfirmation)
                .SetName(RouteNames.Confirmation).Map();

            /// User Folder Routes
            routes.Action<UserController>(user => user.SignIn(), RouteUrls.User + "/" + RouteUrls.SignIn)
                .SetName(RouteNames.UserSignIn).Map();
            routes.Action<UserController>(user => user.SignOut(), RouteUrls.User + "/" + RouteUrls.SignOut)
                .SetName(RouteNames.UserSignOut).Map();
            routes.Action<UserController>(user => user.ForgotPassword(),
                RouteUrls.User + "/" + RouteUrls.ForgotPassword).SetName(RouteNames.UserForgotPassword).Map();
            routes.Action<UserController>(user => user.ForgotPwdConfirmation(),
                RouteUrls.User + "/" + RouteUrls.ForgotPwdConfirmation).SetName(RouteNames.UserForgotPwdConfirm).Map();
            routes.Action<UserController>(user => user.ResetPassword(), RouteUrls.User + "/" + RouteUrls.ResetPassword)
                .SetName(RouteNames.UserResetPassword).Map();
            routes.Action<UserController>(user => user.Settings(), RouteUrls.Settings)
                .SetName(RouteNames.UserAccountSettings).Map();

            /// Home Pages
            routes.Action<HomeController>(home => home.Index(), string.Empty).SetName(RouteNames.Home).Map();
            routes.Action<HomeController>(home => home.ContactUs(), RouteUrls.ContactUs).SetName(RouteNames.ContactUs).Map();
            routes.Action<HomeController>(home => home.Terms(), RouteUrls.Terms).SetName(RouteNames.Terms).Map();
            routes.Action<HomeController>(home => home.Privacy(), RouteUrls.Privacy).SetName(RouteNames.Privacy).Map();
            routes.Action<HomeController>(home => home.HowToCreateResume(), RouteUrls.HowToCreateResume).SetName(RouteNames.HowToCreateResume).Map();
            routes.Action<HomeController>(home => home.InterviewTips(), RouteUrls.InterviewTips).SetName(RouteNames.InterviewTips).Map();
            routes.Action<HomeController>(home => home.FresherInterviewQuestions(), RouteUrls.FresherInterviewQuestions).SetName(RouteNames.FresherInterviewQuestions).Map();
            routes.Action<HomeController>(home => home.JobHunting(), RouteUrls.JobHunting).SetName(RouteNames.JobHunting).Map();
            routes.Action<HomeController>(home => home.ResumeWritingTips(), RouteUrls.ResumeWritingTips).SetName(RouteNames.ResumeWritingTips).Map();
            routes.Action<HomeController>(home => home.ResumeSamples(), RouteUrls.ResumeSamples).SetName(RouteNames.ResumeSamples).Map();

            /// Error Routes
            routes.Action<ErrorController>(err => err.ServerError()).SetName(RouteNames.ServerError);

            #endregion

            routes.MapMvcAttributeRoutes();

            routes.MapRoute(
                RouteConstants.Default,
                RouteConstants.ControllerActionId,
                new {controller = RouteConstants.Home, action = RouteConstants.Index, id = UrlParameter.Optional}
            );
        }
    }
}