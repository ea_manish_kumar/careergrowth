﻿using System.Web.Mvc;
using WebApp.Filters;

namespace WebApp
{
    public static class FilterConfig
    {
        public static void RegisterGlobalFilters(GlobalFilterCollection filters)
        {
            filters.Add(new CustomRequireHttpsAttribute(31536000) { IncludeSubDomains = true, Preload = true });
            filters.Add(new TrackingAttribute());
            filters.Add(new OutputCacheAttribute { VaryByParam = "*", Duration = 0, NoStore = true, Location = System.Web.UI.OutputCacheLocation.None });
        }
    }
}