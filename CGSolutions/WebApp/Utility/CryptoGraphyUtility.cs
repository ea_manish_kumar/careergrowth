﻿using System;
using System.Text;
using System.Web.Security;

namespace WebApp.Utility
{
    public class CryptoGraphyUtility
    {
        private const string DefaultPurpose = "cg";
        
        public static string Protect(string text)
        {
            return Protect(text, null);
        }
        public static string Protect(string text, string purpose)
        {
            if (string.IsNullOrEmpty(text))
                return null;
            try
            {
                byte[] unprotectedValue = Encoding.UTF8.GetBytes(text);
                byte[] protectedValue = MachineKey.Protect(unprotectedValue, purpose ?? DefaultPurpose);
                return Convert.ToBase64String(protectedValue);
            }
            catch
            {
                return null;
            }
        }

        public static string Unprotect(string text)
        {
            return Unprotect(text, null);
        }
        public static string Unprotect(string text, string purpose)
        {
            if (string.IsNullOrEmpty(text))
                return null;
            try
            {
                byte[] unprotectedValue = Convert.FromBase64String(text);
                byte[] protectedValue = MachineKey.Unprotect(unprotectedValue, purpose ?? DefaultPurpose);
                return Encoding.UTF8.GetString(protectedValue);
            }
            catch
            {
                return null;
            }
        }
    }
}