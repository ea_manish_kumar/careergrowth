﻿using System;

namespace WebApp.Utility
{
    public static class ConfigUtility
    {
        public static readonly string PackageId;

        static ConfigUtility()
        {
            PackageId = DateTime.UtcNow.Ticks.ToString();
        }
    }
}