﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Routing;
using WebApp.Constants;

namespace WebApp.Components
{
    public class IsValidWizardModeConstraint : IRouteConstraint
    {
        public bool Match(HttpContextBase httpContext, Route route, string parameterName, RouteValueDictionary values,
            RouteDirection routeDirection)
        {
            var mode = Convert.ToString(values[parameterName]);
            return RouteMode.Add.Equals(mode, StringComparison.OrdinalIgnoreCase) ||
                   RouteMode.Edit.Equals(mode, StringComparison.OrdinalIgnoreCase);
        }
    }

    public class IsValidWizardReviewModeConstraint : IRouteConstraint
    {
        private static readonly IList<string> allowedModes = new List<string>
        {
            RouteMode.Edit,
            RouteMode.Add,
            RouteReviewMode.AddDocAddPara,
            RouteReviewMode.AddDocEditPara,
            RouteReviewMode.EditDocAddPara,
            RouteReviewMode.EditDocEditPara,
            RouteReviewMode.CreateDocAddPara,
            RouteReviewMode.CreateDocEditPara
        };

        public bool Match(HttpContextBase httpContext, Route route, string parameterName, RouteValueDictionary values,
            RouteDirection routeDirection)
        {
            var mode = Convert.ToString(values[parameterName]);
            return allowedModes.Any(m => m.Equals(mode, StringComparison.OrdinalIgnoreCase));
        }
    }

    public class IsValidOtherSectionUrlConstraint : IRouteConstraint
    {
        private static readonly string[] Urls =
        {
            OtherSectionUrls.Accomplishments, OtherSectionUrls.Additional_Information, OtherSectionUrls.Affiliations,
            OtherSectionUrls.Awards, OtherSectionUrls.Certifications,
            OtherSectionUrls.ExtraCurricular, OtherSectionUrls.Language, OtherSectionUrls.Other,
            OtherSectionUrls.PersonalInformation, OtherSectionUrls.Portfolio,
            OtherSectionUrls.Presentations, OtherSectionUrls.Publication, OtherSectionUrls.References,
            OtherSectionUrls.SocialService, OtherSectionUrls.Volunteer_Work
        };

        public bool Match(HttpContextBase httpContext, Route route, string parameterName, RouteValueDictionary values,
            RouteDirection routeDirection)
        {
            var sectionName = Convert.ToString(values[parameterName]);
            return !string.IsNullOrEmpty(sectionName) &&
                   Urls.Any(url => url.Equals(sectionName, StringComparison.OrdinalIgnoreCase));
        }
    }

    public class IsValidExportModeConstraint : IRouteConstraint
    {
        private static readonly IList<string> allowedModes = new List<string>
        {
            ExportMode.Download,
            ExportMode.Print,
            ExportMode.Email
        };

        public bool Match(HttpContextBase httpContext, Route route, string parameterName, RouteValueDictionary values,
            RouteDirection routeDirection)
        {
            var mode = Convert.ToString(values[parameterName]);
            return allowedModes.Any(m => m.Equals(mode, StringComparison.OrdinalIgnoreCase));
        }
    }
}