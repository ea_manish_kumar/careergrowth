﻿using CommonModules.Configuration;
using System.Web;
using System.Web.Optimization;
using Utilities.Constants;

namespace WebApp.Components
{
    public class StaticResources
    {
        private const string ScriptTagFormat = "<script src=\"{0}\" type=\"text/javascript\" defer=\"defer\"></script>";
        private const string PreFetchTagFormat = "<link href=\"{0}\" rel=\"prefetch\">";

        public static readonly string CDN_Path1;
        public static readonly string CDN_Path2;
        public static readonly string PersistentCDNPath;

        static StaticResources()
        {
            CDN_Path1 = string.Concat(ConfigManager.GetConfig(AppSettings.CDN_Path));
            CDN_Path2 = string.Concat(ConfigManager.GetConfig(AppSettings.CDN_Path2));
            PersistentCDNPath = ConfigManager.GetConfig(AppSettings.STATIC_CDN_PATH);
        }


        public static IHtmlString GetPrefetch(params string[] bundle)
        {
            return Styles.RenderFormat(PreFetchTagFormat, bundle);
        }

        public static IHtmlString GetCss(params string[] bundles)
        {
            return Styles.Render(bundles);
        }


        public static IHtmlString GetJS(params string[] bundles)
        {
            return Scripts.RenderFormat(ScriptTagFormat, bundles);
        }
    }
}