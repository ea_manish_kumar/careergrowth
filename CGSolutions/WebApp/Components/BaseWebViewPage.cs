﻿using System.Web.Mvc;
using WebApp.Constants;
using WebApp.Extensions;

namespace WebApp.Components
{
    public class BaseWebViewPage<TModel> : WebViewPage<TModel>
    {
        protected int UserId { get { return User.GetId(); } }
        protected string UserName { get { return User.GetUserName(); } }
        protected string DisplayName { get { return User.GetDisplayName(); } }
        protected bool IsSignedIn { get { return User.IsSignedIn(); } }

        public override void Execute()
        {
        }

        protected virtual void AddJsonProp<T>(string key, T value)
        {
            ViewBag.JsonProps = ViewBag.JsonProps ?? new ViewDataDictionary();
            ViewBag.JsonProps[key] = value;
        }

        protected virtual void UpdateProgress(string progress)
        {
            AddJsonProp("UserProgress", progress);
            AddJsonProp("UpdateProgressAddress", Url.RouteUrl(AjaxRouteNames.TrackPageView, new { page = progress }));
        }

        protected virtual void TrackPageView(string page)
        {
            AddJsonProp("TrackPageViewAddress", Url.RouteUrl(AjaxRouteNames.TrackPageView, new { page }));
        }
    }
}