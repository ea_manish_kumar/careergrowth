﻿namespace WebApp.Constants
{
    public static class EmailRouteCode
    {
        public const string ForgotPassword = "fp";
    }

    public static class EmailViewNames
    {
        public const string Welcome = "~/Views/EmailTemplates/Welcome.cshtml";
        public const string ForgotPassword = "~/Views/EmailTemplates/ForgotPassword.cshtml";
        public const string SubscriptionConfirmation = "~/Views/EmailTemplates/SubscriptionConfirmation.cshtml";
        public const string Invoice = "~/Views/EmailTemplates/Invoice.cshtml";
    }
}