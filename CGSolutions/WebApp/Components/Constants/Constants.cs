﻿using Resources;

namespace WebApp.Constants
{
    public class AppConstants
    {
        public const string ClientAppName = "cg";
        public const string Present = "present";
        public const string Space = " ";
    }
    public class RouteConstants
    {
        public const string ControllerActionId = "{controller}/{action}/{id}";
        public const string Controller = "Controller";
        public const string Action = "Action";
        public const string Default = "Default";
        public const string Home = "Home";
        public const string Index = "Index";

        #region Non-Localizable Error Routes

        public const string Error = "Error";
        public const string NotFound = "NotFound";
        public const string ServerError = "ServerError";

        #endregion
    }

    public class RouteUrls
    {
        #region Journey Routes

        public static readonly string HowItWorks = "resume-journey";
        public static readonly string ChooseTemplate = "select-template";
        public static readonly string Contact = "contact-details";
        public static readonly string Register = "sign-up";
        public static readonly string Experience = "experience";
        public static readonly string ExperienceReview = "experience-details";
        public static readonly string Education = "academics";
        public static readonly string EducationReview = "academics-details";
        public static readonly string SkillDescription = "competencies";
        public static readonly string SummaryDescription = "summary";
        public static readonly string OtherSection = "other-section";

        #endregion

        #region User Routes

        public static readonly string User = "user";
        public static readonly string SignIn = "signin";
        public static readonly string SignOut = "signout";
        public static readonly string ForgotPassword = "forgot-password";
        public static readonly string ForgotPwdConfirmation = "forgot-password-confirmation";
        public static readonly string ResetPassword = "reset-password";
        public static readonly string Settings = "settings";

        #endregion

        #region Resume Routes

        public static readonly string Resume = "resume";
        public static readonly string CreateNewResume = "create-new-resume";
        public static readonly string Preview = "preview";
        public static readonly string Dashboard = "dashboard";

        #endregion

        #region Order Routes

        public static readonly string Order = "order";
        public static readonly string SelectPlan = "select-plan";
        public static readonly string PlanSelected = "plan-selected";
        public static readonly string Renew = "renew";
        public static readonly string Checkout = "checkout";
        public static readonly string OrderConfirmation = "order-confirmation";

        #endregion
        
        #region Home Routes

        public static readonly string ContactUs = "contact-us";
        public static readonly string Terms = "terms";
        public static readonly string Privacy = "privacy";
        public static readonly string HowToCreateResume = "how-to-create-resume";
        public static readonly string InterviewTips = "interview-tips";
        public static readonly string FresherInterviewQuestions = "fresher-interview-questions";
        public static readonly string JobHunting = "job-hunting";
        public static readonly string ResumeWritingTips = "resume-writing-tips";
        public static readonly string ResumeSamples = "resume-samples";

        #endregion

    }

    public static class RouteNames
    {
        public const string Home = "HomeIndex";
        public const string ServerError = "Error_ServerError";

        public const string HowItWorks = "Wizard_HowItWorks";
        public const string ChooseTemplate_Mode = "Wizard_ChooseTemplate_Mode";
        public const string ChooseTemplate = "Wizard_ChooseTemplate";
        public const string Contact_Mode = "Wizard_Contact_Mode";
        public const string Contact = "Wizard_Contact";
        public const string Register = "Wizard_Register";
        public const string Experience_Mode = "Wizard_Experience_Mode";
        public const string Experience = "Wizard_Experience";
        public const string ExperienceReview_Mode = "Wizard_ExperienceReview_Mode";
        public const string ExperienceReview = "Wizard_ExperienceReview";
        public const string Education_Mode = "Wizard_Education_Mode";
        public const string Education = "Wizard_Education";
        public const string EducationReview_Mode = "Wizard_EducationReview_Mode";
        public const string EducationReview = "Wizard_EducationReview";
        public const string SkillDescription_Mode = "Wizard_SkillDescription_Mode";
        public const string SkillDescription = "Wizard_SkillDescription";
        public const string SummaryDescription_Mode = "Wizard_SummaryDescription_Mode";
        public const string SummaryDescription = "Wizard_SummaryDescription";
        public const string OtherSection_Mode = "Wizard_OtherSection_Mode";

        public const string ResumeHowItWorks = "Resume_HowItWorks";
        public const string ResumeChooseTemplate = "Resume_ChooseTemplate";
        public const string ResumeContact = "Resume_Contact";
        public const string ResumeExperience = "Resume_Experience";
        public const string ResumeExperienceReview = "Resume_ExperienceReview";
        public const string ResumeEducation = "Resume_Education";
        public const string ResumeEducationReview = "Resume_EducationReview";
        public const string ResumeSkills = "Resume_SkillDescription";
        public const string ResumeSummary = "Resume_SummaryDescription";
        public const string ResumeOther = "Resume_Other";
        public const string ResumeFinalize = "Resume_Finalize";
        public const string CreateNewResume = "Resume_CreateNewResume";

        public const string Dashboard = "Dashboard_Home";
        public const string DocumentPreview = "Document_Preview";
        public const string OrderRouting = "Order_Routing";
        public const string SellPage = "Order_SellPage";
        public const string PlanSelected = "Order_PlanSelected";
        public const string Checkout = "Order_Checkout";
        public const string Confirmation = "Order_Confirmation";

        public const string UserSignIn = "User_SignIn";
        public const string UserSignOut = "User_SignOut";
        public const string UserResetPassword = "User_ResetPassword";
        public const string UserForgotPassword = "User_ForgotPassword";
        public const string UserForgotPwdConfirm = "User_ForgotPwdConfirmation";
        public const string UserAccountSettings = "User_AccountSettings";

        public const string AccountLogin = "Account_Login";
        public const string AccountRegister = "Account_Register";
        public const string AccountLogout = "Account_Logout";
        public const string AccountForgotPassword = "Account_ForgotPassword";
        public const string AccountForgotPasswordConfirmation = "Account_ForgotPasswordConfirmation";
        public const string AccountChangePassword = "Account_ChangePassword";
        public const string AccountResetPassword = "Account_ResetPassword";
        public const string AccountResetPasswordConfirmation = "Account_ResetPasswordConfirmation";
        public const string AccountProfileSettings = "Account_ProfileSettings";

        public const string ChooseDesign = "CoverLetter_ChooseTemplate";
        public const string LetterType = "CoverLetter_LetterType";
        public const string LetterSection = "CoverLetter_LetterSection";

        public const string ContactUs = "ContactUs";
        public const string Terms = "Terms";
        public const string Privacy = "Privacy";
        public const string HowToCreateResume = "HowToCreateResume";
        public const string InterviewTips = "InterviewTips";
        public const string ResumeSamples = "ResumeSamples";
        public const string FresherInterviewQuestions = "FresherInterviewQuestions";
        public const string JobHunting = "JobHunting";
        public const string ResumeWritingTips = "ResumeWritingTips";
    }

    public class AjaxRouteNames
    {
        public const string SaveSections = "SaveSections";
        public const string DeleteSection = "DeleteSection";
        public const string DeleteParagraph = "DeleteParagraph";
        public const string SortParagraphs = "SortParagraphs";
        public const string UserNameExists = "UserNameExists";
        public const string UpdateProgress = "UpdateProgress";
        public const string SkipRegistration = "SkipRegistration";
        public const string RegisterUser = "RegisterUser";
        public const string SaveTemplate = "SaveTemplate";
        public const string CreateDocument = "CreateDocument";
        public const string SaveDocument = "SaveDocument";
        public const string UpdateDocument = "UpdateDocument";
        public const string GetSection = "GetSection";
        public const string SaveSection = "SaveSection";
        public const string SortSubSections = "SortSubSections";
        public const string GetContactInfo = "GetContactInfo";
        public const string SaveContactInfo = "SaveContactInfo";
        public const string GetExperience = "GetExperience";
        public const string SaveExperience = "SaveExperience";
        public const string GetEducation = "GetEducation";
        public const string SaveEducation = "SaveEducation";
        public const string GetSkill = "GetSkill";
        public const string SaveSkill = "SaveSkill";
        public const string GetPortfolio = "GetPortfolio";
        public const string SavePortfolio = "SavePortfolio";
        public const string GetSummary = "GetSummary";
        public const string SaveSummary = "SaveSummary";
        public const string GetOtherSection = "GetOtherSection";
        public const string SaveOtherSection = "SaveOtherSection";
        public const string UpdateDocStyles = "UpdateDocStyles";
        public const string UpdateSkin = "UpdateSkin";
        public const string DownloadDocument = "DownloadDocument";
        public const string RenameDocument = "RenameDocument";
        public const string EmailDocument = "EmailDocument";
        public const string DuplicateDocument = "DuplicateDocument";
        public const string DeleteDocument = "DeleteDocument";
        public const string SortGroups = "SortGroups";
        public const string UpdatePassword = "UpdatePassword";
        public const string SaveSettings = "SaveSettings";
        public const string TrackPageView = "TrackPageView";

        public const string GetEducationExamples = "GetEducationExamples"; 
        public const string GetJobTitles = "GetJobTitles"; 
        public const string GetExamples = "GetExamples";
    }

    public class SkinRouteNames
    {
        public const string ABS1 = "ABS1";
        public const string Artistic = "Artistic";
        public const string Basic = "Basic";
        public const string Cool = "Cool";
        public const string Elegant = "Elegant";
        public const string Superb = "Superb";
        public const string Executive = "Executive";
        public const string MultiColor = "MultiColor";
        public const string Soothing = "Soothing";
        public const string Awesome = "Awesome";
        public const string Smart = "Smart";
        public const string Standout = "Standout";
        public const string Professional = "Professional";
    }

    public class AjaxRoutes
    {
        public const string Documents = "documents";
        public const string ContactInfo = "contact-info";
        public const string Experience = "experience";
        public const string Education = "education";
        public const string SkillDescription = "skill-description";
        public const string SummaryDescription = "summary-description";
        public const string Portfolio = "portfolio";
        public const string Users = "users";
        public const string UserNameExists = "user-exists";
        public const string RegisterUser = "register-user";
        public const string Examples = "examples";
        public const string EducationExamples = "education-examples";
        public const string JobTitles = "jobTitles";
        public const string Stage = "stage";
        public const string CreateDuplicateDoc = "create-duplicate-doc";
        public const string RenameDoc = "rename-doc";
        public const string DeleteDoc = "delete-doc";
        public const string DownloadDoc = "download-doc";
        public const string EmailDoc = "email-doc";
        public const string Resume = "resume";
        public const string SaveSections = "save-sections";
        public const string OtherSection = "other-section";
        public const string DeleteSection = "delete-section";
        public const string SaveTemplate = "save-template";
        public const string ContactUsEmail = "contact-us-email";
        public const string SaveSetting = "save-settings";
        public const string UpdatePassword = "update-password";
        public const string Letter = "letter";
        public const string Order = "order";
        public const string Sections = "sections";
        public const string Paragraphs = "paragraphs";
        public const string Delete = "delete";
        public const string Styles = "styles";
        public const string Skin = "skin";
        public const string Sort = "sort";
    }

    public static class RouteMode
    {
        public const string Add = "add";
        public const string Edit = "edit";
    }

    public static class RouteReviewMode
    {
        public const string EditDocEditPara = "edit-detail";
        public const string EditDocAddPara = "edit-add";
        public const string AddDocEditPara = "add-edit";
        public const string AddDocAddPara = "add-new";
        public const string CreateDocEditPara = "create-edit";
        public const string CreateDocAddPara = "create-new";
    }

    public static class ExportMode
    {
        public const string Download = "DWLD";
        public const string Print = "PRNT";
        public const string Email = "EMAL";
    }

    public static class RouteParameters
    {
        public const string Action = "action";
        public const string New = "new";
        public const string DocType = "docType";
        public const string SectionId = "sectionId";
        public const string DocumentId = "did";
        public const string Target = "target";
        public const string SkinCode = "skin";
        public const string Stage = "stage";
        public static readonly string Finalize = "finalize";
    }

    public static class OtherSectionUrls
    {
        public const string Volunteer_Work = "volunteer-work";
        public const string Language = "language";
        public const string Affiliations = "affiliations";
        public const string Awards = "awards";
        public const string Additional_Information = "additional-information";
        public const string Publication = "publication";
        public const string Accomplishments = "accomplishments";
        public const string Other = "other";
        public const string Certifications = "certifications";
        public const string ExtraCurricular = "extra-curricular";
        public const string SocialService = "social-service";
        public const string Presentations = "presentations";
        public const string Portfolio = "portfolio";
        public const string PersonalInformation = "personal-information";
        public const string References = "references";
    }
    
    public static class ProgressCode
    {
        public const string HomePage = "HOME";
        public const string UserJourney = "JOURNY";
        public const string UserDetails = "USER";
        public const string Format = "FORMAT";
        public const string Academics = "ACAD";
        public const string AcademicsDetails = "ACADS";
        public const string Experience = "EXPR";
        public const string ExperienceDetails = "EXPRS";
        public const string Competencies = "COMPT";
        public const string Summary = "SUMMRY";
        public const string Preview = "PRVW";
        public const string Dashboard = "DASHBD";
        public const string Plans = "PLANS";
        public const string Checkout = "CHECK";
        public const string Premium = "PREM";
        public const string Custom = "CSTM";
    }
    
    public static class RegexConstants
    {
        public const string RequiredInteger = @"\d{1,}";
        public const string OptionalInteger = @"\d{0,}";
    }

    public static class UserNavigationAction
    {
        public const string FinalizeDownload = "FLDW";
        public const string FinalizeEmail = "FLEM";
    }

    public static class PaymentGatewayMode
    {
        public const string DEMO = "demo";
        public const string LIVE = "live";
    }

    public static class SectionTypeCode
    {
        public const string Contact = "CNTC";
        public const string Other = "OTHR";
    }

    public static class DocumentTypeCode
    {
        public const string Resume = "RSME";
        public const string Letter = "LETR";
    }

    public static class DesktopRoute
    {
        public static readonly string Routing = "routing.ashx";
        public static readonly string Letter = Resource.url_letters;
        public static readonly string Resume = Resource.url_resume;
        public static readonly string User = Resource.url_user;
        public static readonly string ResetPassword = Resource.url_reset_password + ".aspx";
        public static readonly string Login = Resource.url_login + ".aspx";
    }

    public static class BundleConstants
    {
        public const string JQueryUICss = "~/bundles/css/jquery-ui";
        public const string EditorCss = "~/bundles/css/trumbowyg";

        public const string SiteCss = "~/bundles/css/site";
        public const string LpCss = "~/bundles/css/lp";
        public const string WizardCss = "~/bundles/css/wizard";
        public const string StaticCss = "~/bundles/css/static";
        public const string OrderCss = "~/bundles/css/order";

        public const string JQueryUISortableJs = "~/bundles/js/jquery-ui-sortable";
        public const string JQueryAutoCompleteExampleJs = "~/bundles/js/jquery-autocomplete-examples";
        public const string EditorJs = "~/bundles/js/trumbowyg";

        public const string MainBundleJs = "~/bundles/js/main";
        public const string ValidationJs = "~/bundles/js/validation";
        public const string DocumentTemplateJs = "~/bundles/js/document/template";

        #region Resume funnel
        public const string ResumeUserDetailsJS = "~/bundles/js/resume/user-details";
        public const string ResumeWorkDetailsJS = "~/bundles/js/resume/work-details";
        public const string ResumeWorkReviewJS = "~/bundles/js/resume/work-review";
        public const string ResumeAcademicDetailsJS = "~/bundles/js/resume/academic-details";
        public const string ResumeAcademicReviewJS = "~/bundles/js/resume/academic-review";
        #endregion

        #region Old Resume Funnel Constants
        public const string SelectTemplateJs = "~/bundles/js/select-template";
        public const string UserDetailsJs = "~/bundles/js/user-details";
        public const string WorkDetailsJs = "~/bundles/js/work-details";
        public const string WorkReviewJs = "~/bundles/js/work-review";
        public const string AcademicDetailsJs = "~/bundles/js/academic-details";
        public const string AcademicReviewJs = "~/bundles/js/academic-review";
        public const string CompetenciesJs = "~/bundles/js/competencies";
        public const string SummaryJs = "~/bundles/js/summary";
        public const string CustomSectionJs = "~/bundles/js/custom-section";
        #endregion

        #region Pages
        public const string RegisterJs = "~/bundles/js/register";
        public const string LoginJs = "~/bundles/js/login";

        public const string PreviewJs = "~/bundles/js/preview";
        public const string DashboardJs = "~/bundles/js/dashboard";
        public const string HelpJs = "~/bundles/js/help";
        #endregion
    }

    public static class SiteCookies
    {
        public const string SessionUID = "suid";
        public const string SessionBrowserUID = "buid";
        public const string DocumentCookie = "dc";
    }

    public static class SiteQueryStrings
    {
        public const string Utm_Source = "utm_source";
        public const string Utm_Medium = "utm_medium";
        public const string Utm_Campaign = "utm_campaign";
        public const string Source = "source";
        public const string Src = "src";
        public const string Referrer = "ref";
        public static readonly string Mode = "mode";
        public static readonly string ReviewMode = "reviewmode";
        public static readonly string DocumentId = "did";
        public static readonly string SubSectionId = "sbid";
    }

    public static class CustomClaimTypes
    {
        public const string SecurityStamp = "SecurityStamp";
        public const string DisplayName = "DisplayName";
        public const string UserName = "UserName";
        public const string UserId = "UserId";
        public const string Role = "Role";
        public const string UserLoginId = "UserLoginId";
        public const string UserLoginStamp = "UserLoginStamp";
    }
}