﻿namespace WebApp.Constants
{
    public class TrackingConstants
    {
        public const string QS_SOURCE = "source";
        public const string QS_SRC = "src";
        public const string GOOGLE = "google";
        public const string BING = "bing";
        public const string HTTP_REFERER = "HTTP_REFERER";
        public const string ORGANIC = "Organic";
        public const string DIRECT = "Direct";
        public const string REFERRAL = "Referral";
        public const string INTERNAL_MEDIUM = "Internal";
        public const string INTERNAL_REFERRAL = "Internal_Referral";
        public const string UTM_SOURCE = "utm_source"; //Required parameter to identify the source of the traffic 

        public const string
            UTM_MEDIUM =
                "utm_medium"; //Required parameter to identify the medium the link was used upon such as: email, CPC, or other method of sharing

        public const string
            UTM_CAMPAIGN =
                "utm_campaign"; //Required parameter to identify a specific product promotion or strategic campaign such as a spring sale or other promotion

        public const string
            UTM_TERM = "utm_term"; //Optional parameter suggested for paid search to identify keywords for the ad

        public const string
            UTM_KEYWORD =
                "utm_keyword"; //Our Marketing Team uses this instead of UTM_TERM, Optional parameter suggested for paid search to identify keywords for the ad

        public const string UTM_CONTENT = "utm_content";
        public const string UTM_ID = "utm_id";
        public const string AFFILIATE = "affiliate";
        public const char QS_SEPARATOR = '?';
        public const string ADMIN_SOURCE = "Admin";
    }
}