﻿using System;
using System.Collections.Generic;
using System.Linq;
using Utilities.Constants;
using Utilities.Constants.Documents;
using WebApp.Constants;
using WebApp.Extensions;
using WebApp.Models;

namespace WebApp.Components
{
    public class BaseWizardViewPage<TModel> : BaseWebViewPage<TModel>
    {
        private DocumentCookieModel documentCookieModel;
        private IList<string> wizardList;

        protected int DocumentId => Convert.ToInt32(ViewBag.DocumentId);

        protected int ParagraphId => Convert.ToInt32(ViewBag.ParagraphId);

        protected string Mode => Convert.ToString(ViewBag.Mode);

        protected bool HasData => Convert.ToBoolean(ViewBag.HasData);


        private string GetWizardRoute(string wizard)
        {
            var documentCookieModel = ParseDocumentCookie();
            WizardMenu wizardMenu = null;
            if (HasMode(RouteMode.Add))
                wizardMenu = WizardUtility.GetWizard(Url, wizard, RouteMode.Add, DocumentId, null, documentCookieModel);
            else
                wizardMenu = WizardUtility.GetWizard(Url, wizard, documentCookieModel);
            if (wizardMenu != null)
                return wizardMenu.SectionURL;
            return null;
        }

        private IList<string> GetWizardList()
        {
            if (wizardList != null)
                return wizardList;

            if (HasMode(RouteMode.Add))
            {
                var docCookie = ParseDocumentCookie();
                if (docCookie != null)
                {
                    var sections = docCookie.GetAdditionalSections();
                    if (sections != null && sections.Any())
                        wizardList = sections;
                }
            }
            else if (IsSignedIn)
            {
                wizardList = WizardUtility.GetWizardList()
                    .Where(wiz => !WizardConstants.Register.Equals(wiz, StringComparison.OrdinalIgnoreCase)).ToList();
            }
            else
            {
                wizardList = WizardUtility.GetWizardList();
            }

            return wizardList;
        }

        private DocumentCookieModel ParseDocumentCookie()
        {
            if (documentCookieModel != null)
                return documentCookieModel;

            var documentCookie = Request.Cookies.Read(CookieConstants.DocumentCookie);
            if (!string.IsNullOrEmpty(documentCookie))
                documentCookieModel = DocumentCookieModel.Parse(documentCookie);
            return documentCookieModel;
        }

        protected bool HasMode(string mode)
        {
            return string.Equals(Mode, mode, StringComparison.OrdinalIgnoreCase);
        }

        protected string GetPreviousRoute(string wizard)
        {
            if (DocumentId > 0 && HasMode(RouteMode.Edit))
                return Url.RouteUrl(RouteNames.ResumeFinalize, new { id = DocumentId });

            var wizardList = GetWizardList();
            var index = wizardList.IndexOf(wizard);
            if (index > 0)
            {
                var previousWizard = wizardList[index - 1];
                return GetWizardRoute(previousWizard);
            }

            return Url.RouteUrl(RouteNames.ResumeFinalize, new { id = DocumentId });
        }

        protected string GetNextRoute(string wizard)
        {
            if (DocumentId > 0 && HasMode(RouteMode.Edit))
                return Url.RouteUrl(RouteNames.ResumeFinalize, new { id = DocumentId });

            var wizardList = GetWizardList();
            var index = wizardList.IndexOf(wizard);
            if (index < wizardList.Count - 1)
            {
                var nextWizard = wizardList[index + 1];
                return GetWizardRoute(nextWizard);
            }

            return Url.RouteUrl(RouteNames.ResumeFinalize, new { id = DocumentId });
        }
    }
}