﻿using CommonModules.Configuration;
using DomainModel.User;
using Newtonsoft.Json;
using Resources;
using Services.User;
using System;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading;
using Utilities;
using Utilities.Constants;
using Utilities.GeoIPLookup;

namespace WebApp.Components
{
    public class Common
    {
        public static readonly string DomainName;
        public static readonly string PageTitle;

        public static string MetaData
        {
            get
            {
                return GetMetaData();
            }
        }

        static Common()
        {
            DomainName = CGUtility.GetDomainTitle();
            PageTitle = string.Format(Resource.Meta_Title_InstCreateResume, DomainName);
        }

        public static int GetPortalId()
        {
            return CGUtility.GetPortalId();
        }

        public static string GetDomainCD()
        {
            return CGUtility.GetDomainCDFromConfig();
        }

        public static string GetPortalName(string domianCD)
        {
            return "MyResume Builder";
        }

        public static string GetCustomerServiceNumber(string domianCD)
        {
            var customerServiceNumber = "<strong> {0}</strong>";
            switch (domianCD.ToUpper())
            {
                case DomainFriendlyNameConstants.FreeResume:
                    return string.Format(customerServiceNumber, Resource.CSPhoneNo_RG);
                case DomainFriendlyNameConstants.FresherResume:
                    return string.Format(customerServiceNumber, Resource.CSPhoneNo_RH);
                default:
                    return string.Empty;
            }
        }

        public static string GetCustomerServiceEmail(string domianCD)
        {
            switch (domianCD.ToUpper())
            {
                case DomainFriendlyNameConstants.FreeResume:
                    return Resource.ResumeGigCustomerService;
                case DomainFriendlyNameConstants.FresherResume:
                    return Resource.ResumeHelpCustomerService;
                default:
                    return string.Empty;
            }
        }

        public static string GetDomainURL()
        {
            return CGUtility.GetDomainURLFromConfig();
        }

        public static string GetSessionId()
        {
            return CGUtility.ReadCookie(CookieConstants.SessionId);
        }

        public static UserAgentTypeDTO GetDeviceDetails()
        {
            return new UserAgentTypeDTO();
        }

        private static string GetValue(object value)
        {
            if (value == null)
                return string.Empty;
            return value.ToString();
        }

        private static string GetMappingForDTDeviceType(string deviceType)
        {
            switch (deviceType.ToLower())
            {
                case "generic":
                    return "";
                case "tablet":
                    return "tablet";
                case "device":
                    return "mobile";
                default:
                    return deviceType;
            }
        }

        private static string GetMappingForDTDeviceOS(string deviceOS)
        {
            switch (deviceOS.ToLower())
            {
                case "iphone os":
                    return "ios";
                default:
                    return deviceOS;
            }
        }

        public static string GetCountryByIP(string ipAddress)
        {
            var geolookuplocation = string.Empty;
            var freegeoip = string.Empty;
            var location = string.Empty;
            try
            {
                var geoIPLookupService = new GeoIPLookupService();
                geolookuplocation = geoIPLookupService.GetCountryByIP(ipAddress);

                var url = "http://freegeoip.net/json/" + ipAddress;
                var client = new WebClient();
                var jsonstring = client.DownloadString(url);
                dynamic dynObj = JsonConvert.DeserializeObject(jsonstring);
                freegeoip = dynObj.country_code;
                if (freegeoip == "CA" || geolookuplocation == "CA")
                    location = "CA";
                else
                    location = geolookuplocation;
            }
            catch (Exception)
            {
            }

            return location;
        }

        public static void UnsubscribeJobAlert(int userId, int unsubscibeType)
        {
            //try
            //{
            //    UserService userService = new UserService();
            //    var user = userService.GetUserByID(userId);
            //    if (!string.IsNullOrEmpty(user.UserName))
            //    {
            //        var clientCD = GetJobAlertClient(user.DomainCD);
            //        var jobAlertClient = new JobAlertClient(clientCD);
            //        jobAlertClient.DeleteJobAlertSubscription(user.UserName, user.DomainCD, unsubscibeType);
            //    }
            //}
            //catch (Exception ex)
            //{
            //    Hashtable htParams = new Hashtable(2);
            //    htParams.Add("userId:", userId.ToString());
            //    htParams.Add("unsubscibeType:", unsubscibeType.ToString());
            //    MethodInfo objMethod = (MethodInfo)MethodBase.GetCurrentMethod();
            //    GlobalExceptionHandler.customExceptionHandler(ex, objMethod, htParams, RB3SubSystem.JobAlert, RB3Application.RB3, ErrorSeverityType.Normal, "Error while unsubscribing user from jobalerts", true);
            //}
        }

        public static bool BundlingEnabled()
        {
            return Convert.ToBoolean(ConfigManager.GetConfig(ConfigKeys.EnableBundling).ToLower());
        }

        public static string CreateDesktopRouteUrl(string pageName, string folderName)
        {
            var url = new StringBuilder(string.Empty);

            url.Append(GetDomainURL());

            if (!string.IsNullOrEmpty(folderName))
            {
                url.Append(folderName);
                url.Append("/");
            }

            if (!string.IsNullOrEmpty(pageName)) url.Append(pageName);

            return url.ToString();
        }

        public static string GetFaviconTag(string domainCD)
        {
            string favFolder = null;
            if (domainCD == DomainFriendlyNameConstants.FresherResume)
                favFolder = "fs";
            else if (domainCD == DomainFriendlyNameConstants.FreeResume)
                favFolder = "fr";
            else
                favFolder = "fs";

            var favImagePath = GetDomainURL() + "images/fav/" + favFolder;
            var deviceDetails = GetDeviceDetails();

            string deviceName = null;
            if (deviceDetails != null)
            {
                if (deviceDetails.DeviceType == "iPhone")
                {
                    var imagePath = favImagePath + "/touch-icon-iphone-retina.png";
                    deviceName = "<link rel=\"apple-touch-icon\"  sizes=\"120x120\" type=\"image/png\" href=\"" +
                                 imagePath + "\">";
                }
                else if (deviceDetails.DeviceType == "iPad")
                {
                    var imagePath = favImagePath + "/touch-icon-iphone-retina.png";
                    deviceName += "<link rel=\"apple-touch-icon\"  sizes=\"76X76\" type=\"image/png\" href=\"" +
                                  imagePath + "\">";
                    deviceName += "<link rel=\"apple-touch-icon\"  sizes=\"152X152\" type=\"image/png\" href=\"" +
                                  imagePath + "\">";
                }
            }

            if (string.IsNullOrEmpty(deviceName))
            {
                var imagePath = favImagePath + "/favicon.ico";
                deviceName += "<link rel=\"icon\" type=\"image/png\" href=\"" + imagePath + "\">";
                deviceName += "<link rel=\"apple-touch-icon\" type=\"image/png\" href=\"" + imagePath + "\">";
            }

            return deviceName;
        }

        private static string GetMetaData()
        {
            var result = new StringBuilder();
            var domainURL = CGUtility.GetDomainURLFromConfig();
            var domainCD = CGUtility.GetDomainCDFromConfig();
            var domainTitle = CGUtility.GetDomainTitle();
            string domainName = null;

            result.AppendFormat("<meta name=\"Description\" content=\"{0}\" />", Resource.MetaDescription);
            result.AppendFormat("<meta name=\"Keywords\" content=\"{0}\" />",
                string.Format(Resource.MetaKeywords, domainName));
            result.AppendFormat("<meta property=\"og:title\" content=\"{0}\" />",
                string.Format(Resource.Meta_Title_InstCreateResume, domainTitle));
            result.AppendFormat("<meta property=\"og:description\" content=\"{0}\" />", Resource.MetaDescription);
            //result.AppendFormat("<meta property=\"og:image\" content=\"{0}\" />", StaticResources.GetLogoUrl(domainCD)); //TODO: Change URL
            result.AppendFormat("<meta property=\"og:type\" content=\"{0}\" />", Resource.Meta_Og_Type_Article);
            result.AppendFormat("<meta property=\"og:site_name\" content=\"{0}\" />", domainTitle);
            result.AppendFormat("<meta name=\"og:url\" content=\"{0}\" />", domainURL);
            result.AppendFormat(
                "<meta name=\"google-site-verification\" content = \"7ic3S1lyEBwsM0oVSCj32akha7QikT4AyOSkR7QCV2Q\" />");
            return result.ToString();
        }

        #region Job Alert Related Methods

        public static bool IsValidJobAlertDomain(string email)
        {
            var isValidDomain = false;
            if (!string.IsNullOrEmpty(email))
            {
                var blockedDomainsList = ConfigManager.GetConfig("BlockedJobAlertDomains");
                if (!string.IsNullOrEmpty(blockedDomainsList))
                {
                    var blockedDomains = blockedDomainsList.Split(',');
                    isValidDomain = !blockedDomains.Any(x => email.ToLower().Contains(x));
                }
            }

            return isValidDomain;
        }

        public static void StartBackgroundThread(ThreadStart threadStart)
        {
            if (threadStart != null)
            {
                var thread = new Thread(threadStart);
                thread.IsBackground = true;
                thread.Start();
            }
        }

        public static string GetUserLocationByIP(string ipAddress)
        {
            var location = string.Empty;
            var geoIPLookupService = new GeoIPLookupService();
            location = geoIPLookupService.GetPostalCodeByIP(ipAddress);
            if (string.IsNullOrEmpty(location))
                location = geoIPLookupService.GetLocationByIP(ipAddress);
            if (string.IsNullOrEmpty(location))
                location = geoIPLookupService.GetStateByIP(ipAddress);
            return location;
        }

        public string GetUserLocation(int userId)
        {
            var userService = new UserService();
            var jobLocation = userService.GetLatestJobLocation(userId);
            return jobLocation;
        }

        #endregion
    }

    public static class DecimalExtensions
    {
        public static string ToCurrency(this decimal number)
        {
            if (number == Math.Truncate(number))
                return number.ToString("c0");
            return number.ToString("c");
        }
    }
}