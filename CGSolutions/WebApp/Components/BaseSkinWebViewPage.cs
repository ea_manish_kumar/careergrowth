﻿using System;
using WebApp.Constants;

namespace WebApp.Components
{
    public class BaseSkinWebViewPage<TModel> : BaseWebViewPage<TModel>
    {
        private const string clientPrefix = "data";
        private const string sectionAttribute = "sec";
        private const string paragraphAttribute = "para";
        private const string fieldAttribute = "field";
        private const string propertyAttribute = "prop";

        protected bool IsTemplate;

        protected override void InitializePage()
        {
            base.InitializePage();
            IsTemplate = Convert.ToBoolean(ViewBag.IsTemplate);
        }

        protected string GetAttr(string attrName, string attrValue)
        {
            if (IsTemplate)
                return string.Format("{0}-{1}-{2}=\"{3}\"", clientPrefix, AppConstants.ClientAppName, attrName,
                    attrValue);
            return null;
        }

        protected string GetSectionAttr(string sectionName)
        {
            return GetAttr(sectionAttribute, sectionName);
        }

        protected string GetParaAttr(int index)
        {
            return GetAttr(paragraphAttribute, index.ToString());
        }

        protected string GetFieldAttr(string fieldName)
        {
            return GetAttr(fieldAttribute, fieldName);
        }

        protected string GetPropertyAttr(string propertyName)
        {
            return GetAttr(propertyAttribute, propertyName);
        }
    }
}