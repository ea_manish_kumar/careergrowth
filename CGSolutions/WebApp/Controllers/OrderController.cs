﻿using App.DomainModel;
using App.Services;
using App.Utility.Constants;
using CCA.Util;
using CommonModules;
using CommonModules.Configuration;
using Microsoft.Web.Mvc;
using Newtonsoft.Json;
using Resources;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Web.Mvc;
using Utilities.Constants;
using WebApp.Components;
using WebApp.Extensions;
using WebApp.Models;

namespace WebApp.Controllers
{
    [Authorize]
    public class OrderController : BaseController
    {
        private readonly string AccessCode = ConfigManager.GetConfig("CcAvenueAccessCode");
        private readonly string CheckoutUrl = ConfigManager.GetConfig("CcAvenueCheckoutUrl");
        private readonly string MerchantId = ConfigManager.GetConfig("CcAvenueMerchantId");
        private readonly string WorkingKey = ConfigManager.GetConfig("CcAvenueWorkingKey");

        private readonly IBillingService BillingService;
        private readonly IUserService UserService;
        private readonly ITrackingService TrackingService;

        public OrderController()
        {
            BillingService = DependencyResolver.Current.GetService<IBillingService>();
            UserService = DependencyResolver.Current.GetService<IUserService>();
            TrackingService = DependencyResolver.Current.GetService<ITrackingService>();
        }

        #region Action Methods

        [HttpPost]
        public async Task<ActionResult> PaymentSuccessful()
        {
            var crypto = new CCACrypto();
            //var decryptedResponse = crypto.Decrypt(encResp, WorkingKey); //Commented for now
            //Need to update order Table for long bank_ref_no
            var decryptedResponse =
                "order_id=2&sku_id=1&tracking_id=306003191178&bank_ref_no=149709&order_status=Success&failure_message=&payment_mode=Net Banking&card_name=AvenuesTest&status_code=null&status_message=Y&currency=INR&amount=1.95&billing_name=udfuh&billing_address=usa&billing_city=v&billing_state=bb&billing_zip=345256&billing_country=Bahrain&billing_tel=9876543210&billing_email=gshg@hdh.com&delivery_name=udfuh&delivery_address=usa&delivery_city=v&delivery_state=bb&delivery_zip=345256&delivery_country=Bahrain&delivery_tel=9876543210&merchant_param1=&merchant_param2=&merchant_param3=&merchant_param4=&merchant_param5=&vault=N&offer_type=null&offer_code=null&discount_value=0.0&mer_amount=1.95&eci_value=null&retry=null&response_code=0&billing_notes=&trans_date=10/06/2017 17:21:22&bin_country=";
            var isSuccess = OnPaymentSuccessful(decryptedResponse);
            return View();
        }

        [HttpPost]
        public async Task<ActionResult> PaymentFailed(string encResp)
        {
            var crypto = new CCACrypto();
            var decryptedResponse = crypto.Decrypt(encResp, WorkingKey);
            //BillingService.FailOrder(UserId, decryptedResponse);//TODO
            return View();
        }

        //Payment_TODO
        private string BuildCcAvenueRequestParameters(Order order)
        {
            var queryParameters = new Dictionary<string, string>
            {
                {"order_id", order.Id.ToString()},
                //{"plan_id", order.PlanId.ToString()}, //Need to check if it works with CCAvenue
                {"merchant_id", MerchantId},
                {"amount", order.Amount.ToString()},
                {"currency", "INR"},
                {"tid", order.Id.ToString()}, //Unique transaction ID
                {"redirect_url", "http://localhost:789/Order/PaymentSuccessful"},
                {"cancel_url", "http://localhost:789/Order/PaymentFailed"},
                {"request_type", "JSON"},
                {"response_type", "JSON"},
                {"version", "1.1"}
            }.Select(item => string.Format("{0}={1}", item.Key, item.Value));
            return string.Join("&", queryParameters);
        }

        public async Task<ActionResult> Routing(int documentId, int skinId, string userAction, string docCategory, string docType)
        {
            if (!string.IsNullOrEmpty(userAction) && documentId > 0)
            {
                var actionData = JsonConvert.SerializeObject(new UserActionEvent
                    {DocumentId = documentId, Action = userAction});
                short eventId = await UserService.GetEventId(EventType.Navigation);
                UserEventData userEventData = new UserEventData()
                {
                    UserId = UserId,
                    CreatedOnUtc = DateTime.UtcNow,
                    EventId = eventId,
                    EventData = actionData
                };
                await UserService.SaveUserEvent(userEventData);
            }

            var subscriptions = await BillingService.GetAllSubscriptionsByUserId(UserId);
            if (!subscriptions.IsAny(x => x.ExpireDate >= DateTime.Now))
                return this.RedirectToAction(c => c.SelectPlan());
            return this.RedirectToAction(c => c.UpgradePlan(skinId));
        }

        /// <summary>
        ///     Url: Upgrade
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        public async Task<ActionResult> SelectPlan()
        {
            var viewModel = new PlanVM();
            var subscriptions = await BillingService.GetAllSubscriptionsByUserId(UserId);
            var activeSubscription = subscriptions.IsAny(x => x.ExpireDate > DateTime.Now);
            if (activeSubscription) return this.RedirectToAction<DashboardController>(c => c.Home());

            var showFreemiumPlan = !subscriptions.IsAny();
            var plans = await BillingService.GetAllPlansAvailableForThisPortal();
            if (plans != null)
            {
                var freemiumPlan = GetPlanByTypeId(plans, (int) PlanTypes.Freemium);
                var basicPlan = GetPlanByTypeId(plans, (int) PlanTypes.Basic);
                var professionalPlan = GetPlanByTypeId(plans, (int) PlanTypes.Professional);
                var premiumPlan = GetPlanByTypeId(plans, (int) PlanTypes.Premium);
                if (showFreemiumPlan && freemiumPlan != null)
                    viewModel.FreePlan = freemiumPlan;
                if (basicPlan != null)
                    viewModel.StandardPlan = basicPlan;
                if (professionalPlan != null)
                    viewModel.ProfessionalPlan = professionalPlan;
                if (premiumPlan != null)
                    viewModel.PremiumPlan = premiumPlan;
            }
            else
            {
                viewModel.ErrorMessage = "Seems like our systems are acting up, please try again or raise a ticket with helpdesk.";
            }

            return View(viewModel);
        }

        [HttpGet]
        public async Task<ActionResult> PlanSelected(int? planId)
        {
            var SessionCookie = Request.Cookies.Read(Constants.SiteCookies.SessionUID);
            Guid SessionGUID;
            Guid.TryParse(SessionCookie, out SessionGUID);
            var sessionId = await TrackingService.GetSessionId(SessionGUID);
            if (planId == null)
                return this.RedirectToAction(x => x.SelectPlan());
            var subscriptions = await BillingService.GetAllSubscriptionsByUserId(UserId);
            var activeSubscription = subscriptions.IsAny(x => x.ExpireDate > DateTime.Now);
            if (activeSubscription) return this.RedirectToAction<DashboardController>(c => c.Home());

            if (planId > 0 && sessionId.HasValue)
            {   
                var order = await BillingService.PlaceOrder(UserId, planId.Value, sessionId.Value);
                if (order != null && order.Id > 0)
                    return this.RedirectToAction(c => c.Checkout(order.Id));
            }

            return this.RedirectToAction(c => c.PaymentError());
        }

        [HttpGet]
        public async Task<ActionResult> UpgradePlan(int skinId)
        {
            var viewModel = new PlanVM();
            var subscriptions = await BillingService.GetAllSubscriptionsByUserId(UserId);
            var activeSubscription = subscriptions.Where(x => x.ExpireDate > DateTime.UtcNow)
                .OrderByDescending(x => x.ExpireDate).FirstOrDefault();
            if (activeSubscription == null)
            {
                return this.RedirectToAction(c => c.SelectPlan());
            }

            var plans = await BillingService.GetAllPlansAvailableForThisPortal();
            if (plans != null)
            {
                var activePlanTypeId = activeSubscription.PlanTypeID;
                var basicPlan = (int) PlanTypes.Basic > activePlanTypeId
                    ? GetPlanByTypeId(plans, (int) PlanTypes.Basic)
                    : null;
                var professionalPlan = (int) PlanTypes.Professional > activePlanTypeId
                    ? GetPlanByTypeId(plans, (int) PlanTypes.Professional)
                    : null;
                var premiumPlan = (int) PlanTypes.Premium > activePlanTypeId
                    ? GetPlanByTypeId(plans, (int) PlanTypes.Premium)
                    : null;
                if (basicPlan != null)
                    viewModel.StandardPlan = basicPlan;
                if (professionalPlan != null)
                    viewModel.ProfessionalPlan = professionalPlan;
                if (premiumPlan != null)
                    viewModel.PremiumPlan = premiumPlan;
            }
            else
            {
                viewModel.ErrorMessage = "Seems like our systems are acting up, please try again or raise a ticket with helpdesk.";
            }

            return View(viewModel);
        }

        [HttpPost]
        public async Task<ActionResult> UpgradePlan(int planId, int skinId)
        {
            var plans = await BillingService.GetAllPlansAvailableForThisPortal();
            var requestedPlan = plans.FirstOrDefault(x => x.Id == planId);
            if (requestedPlan != null)
            {
                var requestedPlanTypeId = requestedPlan.PlanTypeID;
                var subscriptions = await BillingService.GetAllSubscriptionsByUserId(UserId);
                var requestedPlanSubscribed =
                    subscriptions.IsAny(x => x.ExpireDate > DateTime.Now && x.PlanTypeID == requestedPlanTypeId);
                if (requestedPlanSubscribed) return this.RedirectToAction<DashboardController>(c => c.Home());

                var sessionId = Convert.ToInt32(Common.GetSessionId());
                var order = await BillingService.PlaceOrder(UserId, planId, sessionId);
                if (order != null && order.Id > 0)
                    return this.RedirectToAction(c => c.Checkout(order.Id));
            }

            return this.RedirectToAction(c => c.PaymentError());
        }


        public async Task<ActionResult> Checkout(int orderId)
        {
            var order = await BillingService.GetOrder(orderId);
            if (order != null)
            {
                var crypto = new CCACrypto();
                var model = new CcAvenueViewModel();
                model.EncryptedRequest = crypto.Encrypt(BuildCcAvenueRequestParameters(order), WorkingKey);
                model.AccessCode = AccessCode;
                model.CheckoutUrl = CheckoutUrl;
                return View("CcAvenue", model);
            }

            return this.RedirectToAction(ord => ord.PaymentError());
        }


        public async Task<ActionResult> Confirmation()
        {
            int orderId;
            if (int.TryParse(Request.QueryString["oid"], out orderId) && orderId > 0)
            {
                var domainCD = Request.QueryString["domain"]; //QS will be available for Rebate SKU flow only
                if (string.IsNullOrEmpty(domainCD))
                    domainCD = Common.GetDomainCD();
                //DomainTitle by DoaminCD
                int susbcriptionLevel;
                int.TryParse(Request.QueryString["sub"], out susbcriptionLevel);
                //if (susbcriptionLevel == 0) susbcriptionLevel = (int)UserSubscriptionLevel.Fresh;//Payment_TODO
                var model = PopulateConfirmationModel(susbcriptionLevel, domainCD);
                return View(model);
            }

            return this.RedirectToAction(error => error.PaymentError());
        }

        public async Task<ActionResult> PaymentError()
        {
            return View();
        }

        #endregion Action Methods

        #region Private Methods

        private async Task<bool> OnPaymentSuccessful(string response)
        {
            var paymentResponse = JsonConvert.DeserializeObject<PaymentGatewayResponse>(response);
            //1. Entry for Subscription
            var subscription = await BillingService.CreateNewSubscription();
            //2. Entry for Transaction
            var transaction = await BillingService.AddBillingTransaction();
            //3. Update Order Status
            var order = await BillingService.UpdateOrder();
            //4. Send Confirmation Mail
            return false;
        }

        private async Task<ConfirmationViewModel> PopulateConfirmationModel(int subscriptionLevel, string domainCD)
        {
            var domainTitle = Common.GetPortalName(Common.GetDomainCD());
            var model = new ConfirmationViewModel();
            //Payment_TODO
            //switch (subscriptionLevel)
            //{
            //    case (int)UserSubscriptionLevel.Suspended:
            //    case (int)UserSubscriptionLevel.SuspendedOnOldEcom:
            //        {
            //            model.ConfirmationHeading = Resource.PaymentDetailsConfirmed;
            //            model.ConfirmationSubHeading = Resource.AccountReactivatedMsg;
            //            model.IsSuspendedUser = true;
            //        }
            //        break;
            //    case (int)UserSubscriptionLevel.Expired:
            //    case (int)UserSubscriptionLevel.Fresh:
            //        {
            //            model.ConfirmationHeading = Resource.ThanksForPurchase;
            //            model.ConfirmationSubHeading = Resource.OrderConfirmationMailSent;
            //            model.CreditCardStatementText = Resource.CCStatementTextRG;
            //        }
            //        break;
            //}
            var customerServiceEmail = Common.GetCustomerServiceEmail(domainCD);
            var customerServicePhone = Common.GetCustomerServiceNumber(domainCD);
            model.SelfCancellationMessage = string.Format(Resource.SelfCancelSubscription, customerServiceEmail,
                customerServicePhone);
            return model;
        }

        private Plan GetPlanByTypeId(IList<Plan> allPlans, int planTypeId)
        {
            if (allPlans != null)
            {
                var plans = allPlans.Where(x => x.PlanTypeID == planTypeId)
                    .OrderByDescending(x => x.Id); //TODO: CountryCD and CurrencyCD check add
                if (plans.IsAny())
                    return plans.First();
            }

            return null;
        }

        #endregion Private Methods
    }
}