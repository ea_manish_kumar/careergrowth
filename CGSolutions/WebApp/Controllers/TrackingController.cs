﻿using App.Services;
using System;
using System.Threading.Tasks;
using System.Web.Mvc;
using WebApp.Constants;
using WebApp.Extensions;

namespace WebApp.Controllers
{
    [RoutePrefix("track")]
    public class TrackingController : Controller
    {
        readonly ITrackingService TrackingService;

        public TrackingController(ITrackingService trackingService)
        {
            TrackingService = trackingService;
        }

        [HttpPost]
        [Route("page-view/{page}", Name = AjaxRouteNames.TrackPageView)]
        public async Task<JsonResult> PageView(string page)
        {
            var sessionCookie = Request.Cookies.Read(SiteCookies.SessionUID);
            if (!string.IsNullOrEmpty(sessionCookie) && Guid.TryParse(sessionCookie, out Guid sessionUID))
            {
                int? userId = null;
                if (User.IsSignedIn())
                    userId = User.GetId();
                await TrackingService.SavePageView(page, sessionUID, userId);
                return Json(true);
            }
            return Json(false);
        }
    }
}