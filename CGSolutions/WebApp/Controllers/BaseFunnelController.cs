﻿using App.Model.Documents;
using App.Services;
using App.Utility;
using App.Utility.Constants;
using App.Utility.Extensions;
using Resources;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Web.Mvc;
using WebApp.Constants;
using WebApp.Extensions;
using WebApp.Utility;

namespace WebApp.Controllers
{
    public class BaseFunnelController : BaseController
    {
        protected readonly IDocumentService DocumentService;
        protected IList<string> DefaultMenuRoutes;
        protected string PreviewRouteName;
        protected string FirstFunnelRoute;

        public static readonly IList<WizardMenuDTO> RouteSectionMapping = new List<WizardMenuDTO>
        {
            new WizardMenuDTO { Route = RouteNames.ResumeChooseTemplate },
            new WizardMenuDTO { Route = RouteNames.ResumeContact, SectionType = SectionTypes.Contact, SectionLabel = Resource.CNTC, SortIndex = 0 },
            new WizardMenuDTO { Route = RouteNames.ResumeExperience, SectionType = SectionTypes.Experience, SectionLabel = Resource.EXPR, SortIndex = 3, HasReviewScreen = true, IsMultiSubSection = true },
            new WizardMenuDTO { Route = RouteNames.ResumeExperienceReview, SectionType = SectionTypes.Experience, SectionLabel = Resource.EXPR, SortIndex = 3, HasReviewScreen = true, IsMultiSubSection = true, RequireSubSection = true },
            new WizardMenuDTO { Route = RouteNames.ResumeEducation, SectionType = SectionTypes.Education, SectionLabel = Resource.EDUC, SortIndex = 4, HasReviewScreen = true, IsMultiSubSection = true },
            new WizardMenuDTO { Route = RouteNames.ResumeEducationReview, SectionType = SectionTypes.Education, SectionLabel = Resource.EDUC, SortIndex = 4, HasReviewScreen = true, IsMultiSubSection = true, RequireSubSection = true },
            new WizardMenuDTO { Route = RouteNames.ResumeSkills, SectionType = SectionTypes.Skills, SectionLabel = Resource.HILT, SortIndex = 2, IsMultiSubSection = true },
            new WizardMenuDTO { Route = RouteNames.ResumeSummary, SectionType = SectionTypes.Summary, SectionLabel = Resource.SUMM, SortIndex = 1 },
            new WizardMenuDTO { Route = RouteNames.ResumeOther, SectionType = SectionTypes.Other, SectionLabel = Resource.Other }
        };

        public BaseFunnelController(IDocumentService documentService)
        {
            DocumentService = documentService;
        }


        #region Document Cookie Methods

        protected void FlushDocumentCookie(DocumentCookieDTO documentCookieDTO)
        {
            var documentCookie = documentCookieDTO.ToJsonString();
            if (Strings.IsNotNullOrEmpty(documentCookie))
                Response.Cookies.Encrypt(SiteCookies.DocumentCookie, documentCookie);
        }

        protected DocumentCookieDTO ParseDocumentCookieDTO()
        {
            var documentCookie = Request.Cookies.Decrypt(SiteCookies.DocumentCookie);
            if (Strings.IsNotNullOrEmpty(documentCookie))
                return DocumentCookieDTO.Parse(documentCookie);
            return null;
        }

        protected void DeleteDocumentCookieDTO()
        {
            Response.Cookies.Delete(SiteCookies.DocumentCookie);
        }

        protected void AddSectionToDocumentCookie(SectionDTO savedSection)
        {
            var docCookie = ParseDocumentCookieDTO();
            if (docCookie == null)
                docCookie = new DocumentCookieDTO() { DocumentId = savedSection.DocumentId };
            if (savedSection.SubSections.Count > 0 && IsMultiSubSections(savedSection.Name))
                docCookie.AddSection(savedSection.Name, savedSection.SubSections.Count);
            else
                docCookie.AddSection(savedSection.Name);
            FlushDocumentCookie(docCookie);
        }

        #endregion


        #region Wizard Model Methods
        protected async Task<WizardDTO> CreateWizardModel(string route)
        {
            if (Strings.IsNullOrEmpty(route))
                return null;

            var documentCookieDTO = ParseDocumentCookieDTO();
            if (int.TryParse(Request.QueryString[SiteQueryStrings.DocumentId], out int documentId) && documentId > 0)
            {
                if (documentCookieDTO == null || documentId != documentCookieDTO.DocumentId)
                {
                    var document = await DocumentService.GetDocument(documentId);
                    if (document != null)
                    {
                        var sectionTypes = await DocumentService.GetSectionTypes();
                        documentCookieDTO = DocumentCookieDTO.Parse(document, sectionTypes);
                        FlushDocumentCookie(documentCookieDTO);
                    }
                    else
                    {
                        DeleteDocumentCookieDTO();
                        return null;
                    }
                }
            }
            else if (documentCookieDTO != null && documentCookieDTO.DocumentId > 0)
                documentId = documentCookieDTO.DocumentId;
            var model = new WizardDTO
            {
                DocumentId = documentId,
                DocumentCookieDTO = documentCookieDTO
            };
            if (int.TryParse(Request.QueryString[SiteQueryStrings.SubSectionId], out int subSectionId) && subSectionId > 0)
                model.SubSectionId = subSectionId;

            SetProperties(model, documentCookieDTO, route);
            SetMenuRoutes(model, documentCookieDTO, route);
            SetLoadData(model, documentCookieDTO);

            AddJsonProp(model);
            return model;
        }

        private void AddJsonProp(WizardDTO wizardDTO)
        {
            if (wizardDTO != null)
            {
                ViewBag.JsonProps = ViewBag.JsonProps ?? new ViewDataDictionary();
                ViewBag.JsonProps["PackageId"] = ConfigUtility.PackageId;
                ViewBag.JsonProps["DocumentId"] = wizardDTO.DocumentId;
                ViewBag.JsonProps["SubSectionId"] = wizardDTO.SubSectionId;
                ViewBag.JsonProps["ValidateSubSection"] = wizardDTO.ValidateSubSection;
                ViewBag.JsonProps["LoadData"] = wizardDTO.LoadData;
                ViewBag.JsonProps["BackRouteAddress"] = wizardDTO.BackRouteUrl;
                ViewBag.JsonProps["NextRouteAddress"] = wizardDTO.NextRouteUrl;
                if (Strings.IsNotNullOrEmpty(wizardDTO.ReviewRouteUrl))
                    ViewBag.JsonProps["ReviewRouteUrl"] = wizardDTO.ReviewRouteUrl;
                if (Strings.IsNotNullOrEmpty(wizardDTO.GetSectionUrl))
                    ViewBag.JsonProps["GetSectionAddress"] = wizardDTO.GetSectionUrl;
                if (Strings.IsNotNullOrEmpty(wizardDTO.SortSubSectionsUrl))
                    ViewBag.JsonProps["SortSubSectionsAddress"] = wizardDTO.SortSubSectionsUrl;
                ViewBag.JsonProps["SaveSectionAddress"] = Url.RouteUrl(AjaxRouteNames.SaveSection, new { documentId = wizardDTO.DocumentId });
            }
        }

        private void SetProperties(WizardDTO model, DocumentCookieDTO documentCookieDTO, string route)
        {
            var currentMenu = GetWizardMenu(route);
            if (currentMenu != null)
            {
                model.SectionSortIndex = currentMenu.SortIndex;
                model.SectionName = currentMenu.SectionType;
                if (currentMenu.HasReviewScreen)
                {
                    if (documentCookieDTO != null && documentCookieDTO.HasSection(model.SectionName, out int subSectionCount))
                        model.SubSectionCount = subSectionCount;
                    if (currentMenu.RequireSubSection)
                        model.SortSubSectionsUrl = Url.RouteUrl(AjaxRouteNames.SortSubSections, new { documentId = model.DocumentId });
                    else
                    {
                        model.ValidateSubSection = true;
                        var reviewWizardMenu = GetReviewWizardMenu(model.SectionName);
                        if (reviewWizardMenu != null)
                        {
                            model.ReviewRouteUrl = Url.RouteUrl(reviewWizardMenu.Route, Request.QueryString.ToRouteValues(new List<string> { SiteQueryStrings.SubSectionId }));
                            if (model.SubSectionCount > 0 && Strings.IsNotNullOrEmpty(model.ReviewRouteUrl))
                                model.NextRouteUrl = model.BackRouteUrl = model.ReviewRouteUrl;
                        }
                    }
                }
            }
        }

        private void SetMenuRoutes(WizardDTO model, DocumentCookieDTO documentCookieDTO, string route)
        {
            if (documentCookieDTO != null)
            {
                IList<string> sections = null;
                if (Strings.IsNotNullOrEmpty(Request.QueryString[SiteQueryStrings.DocumentId]))
                    sections = documentCookieDTO.GetSections();
                else
                    sections = documentCookieDTO.GetAdditionalSections();
                if (sections.IsNotNullOrEmpty())
                    model.MenuRoutes = ToMenuRoutes(documentCookieDTO, sections);
            }

            if (model.MenuRoutes.IsNullOrEmpty())
                model.MenuRoutes = ToDefaultMenuRoutes(documentCookieDTO);

            if (Strings.IsNotNullOrEmpty(Request.QueryString[SiteQueryStrings.DocumentId]))
                model.NextRouteUrl = model.BackRouteUrl = Url.RouteUrl(PreviewRouteName, new { id = model.DocumentId });
            else if (model.MenuRoutes.IsNotNullOrEmpty())
            {
                if (Strings.IsNullOrEmpty(model.NextRouteUrl))
                    model.NextRouteUrl = model.MenuRoutes.SkipWhile(r => !Strings.Equals(route, r.Route) && !Strings.Equals(model.SectionName, r.SectionType)).Skip(1).Select(r => r.SectionUrl).FirstOrDefault();
                if (Strings.IsNullOrEmpty(model.BackRouteUrl))
                    model.BackRouteUrl = model.MenuRoutes.TakeWhile(r => !Strings.Equals(route, r.Route) && !Strings.Equals(model.SectionName, r.SectionType)).Select(r => r.SectionUrl).LastOrDefault();
            }
        }

        private void SetLoadData(WizardDTO model, DocumentCookieDTO documentCookieDTO)
        {
            if (Strings.IsNotNullOrEmpty(model.SectionName))
            {
                model.LoadData = Strings.IsNotNullOrEmpty(Request.QueryString[SiteQueryStrings.DocumentId])
                                    || Strings.IsNotNullOrEmpty(Request.QueryString[SiteQueryStrings.SubSectionId])
                                    || (documentCookieDTO != null && documentCookieDTO.HasSection(model.SectionName));
                if (model.LoadData)
                    model.GetSectionUrl = Url.RouteUrl(AjaxRouteNames.GetSection, new { documentId = model.DocumentId, sectionType = model.SectionName });
            }
            model.SaveSectionUrl = Url.RouteUrl(AjaxRouteNames.SaveSection, new { documentId = model.DocumentId });
        }

        private IList<WizardMenuDTO> ToDefaultMenuRoutes(DocumentCookieDTO documentCookieDTO)
        {
            var menuRoutes = new List<WizardMenuDTO>();
            foreach (var menuRoute in DefaultMenuRoutes)
            {
                var sectionMenu = GetWizardMenu(menuRoute);
                if (sectionMenu != null && (!sectionMenu.HasReviewScreen || sectionMenu.RequireSubSection == documentCookieDTO?.HasSubSection(sectionMenu.SectionType)))
                {
                    sectionMenu.SectionUrl = Url.RouteUrl(sectionMenu.Route, Request.QueryString.ToRouteValues());
                    menuRoutes.Add(sectionMenu);
                }
            }
            return menuRoutes;
        }

        private IList<WizardMenuDTO> ToMenuRoutes(DocumentCookieDTO documentCookieDTO, IList<string> sections)
        {
            if (sections.IsNotNullOrEmpty() && documentCookieDTO != null)
            {
                var menuRoutes = new List<WizardMenuDTO>();
                foreach (var section in sections)
                {
                    if (documentCookieDTO.HasSection(section, out bool hasSubSection))
                    {
                        var sectionMenu = GetWizardMenu(section, hasSubSection);
                        if (sectionMenu != null)
                        {
                            sectionMenu.SectionUrl = Url.RouteUrl(sectionMenu.Route, Request.QueryString.ToRouteValues());
                            menuRoutes.Add(sectionMenu);
                        }
                    }
                }
                return menuRoutes;
            }
            return null;
        }

        private WizardMenuDTO GetWizardMenu(string route)
        {
            return RouteSectionMapping.FirstOrDefault(r => Strings.Equals(route, r.Route))?.Clone();
        }

        private WizardMenuDTO GetWizardMenu(string sectionType, bool requireSubSection)
        {
            return RouteSectionMapping.FirstOrDefault(r => Strings.Equals(r.SectionType, sectionType) && (!r.HasReviewScreen || r.RequireSubSection == requireSubSection))?.Clone();
        }

        private WizardMenuDTO GetNonReviewWizardMenu(string sectionType)
        {
            return RouteSectionMapping.FirstOrDefault(r => r.HasReviewScreen && !r.RequireSubSection && Strings.Equals(r.SectionType, sectionType))?.Clone();
        }

        private WizardMenuDTO GetReviewWizardMenu(string sectionType)
        {
            return RouteSectionMapping.FirstOrDefault(r => r.HasReviewScreen && r.RequireSubSection && Strings.Equals(r.SectionType, sectionType))?.Clone();
        }

        protected bool IsMultiSubSections(string sectionType)
        {
            return RouteSectionMapping.Any(r => r.IsMultiSubSection && Strings.Equals(sectionType, r.SectionType));
        }

        protected bool HasReviewScreen(string sectionType)
        {
            return RouteSectionMapping.Any(r => r.IsMultiSubSection && r.HasReviewScreen && Strings.Equals(sectionType, r.SectionType));
        }

        protected void InitializeRoutes(SectionDTO section)
        {
            if (section != null && section.SubSections.IsNotNullOrEmpty())
            {
                if (IsMultiSubSections(section.Name))
                {
                    section.SortSubSectionsUrl = Url.RouteUrl(AjaxRouteNames.SortSubSections, new { documentId = section.DocumentId, subSectionId = section.Id });
                }
                if (HasReviewScreen(section.Name))
                {
                    var nonReviewWizardMenu = GetNonReviewWizardMenu(section.Name);
                    if (nonReviewWizardMenu != null)
                    {
                        section.AddSubSectionUrl = Url.RouteUrl(nonReviewWizardMenu.Route, Request.QueryString.ToRouteValues());
                        foreach (var subSection in section.SubSections)
                        {
                            subSection.EditUrl = Url.RouteUrl(
                                nonReviewWizardMenu.Route,
                                Request.QueryString.ToRouteValues(new Dictionary<string, object> { 
                                    { SiteQueryStrings.SubSectionId, subSection.Id }
                                })
                            );
                        }
                    }
                }
            }
        }
        #endregion
    }
}