﻿using System;
using System.Web.Mvc;
using Utilities;
using WebApp.Components;
using WebApp.Constants;
using WebApp.Extensions;

namespace WebApp.Controllers
{
    [RoutePrefix("routing")]
    public class RoutingController : Controller
    {
        [HttpGet]
        [Route("email")]
        public RedirectToRouteResult Email()
        {
            var routeParams = Request.QueryString.ToRouteValues();
            if (routeParams.NotNullOrEmpty())
            {
                var redirectionCode = Convert.ToString(routeParams["rdc"]);
                if (!string.IsNullOrEmpty(redirectionCode))
                {
                    routeParams.Remove("rdc");
                    switch (redirectionCode.ToLower())
                    {
                        case EmailRouteCode.ForgotPassword: return RedirectToRoute(RouteNames.UserResetPassword, routeParams);
                    }
                }
            }
            return RedirectToRoute(RouteNames.Home, routeParams);
        }
    }
}