﻿using Microsoft.Web.Mvc;
using Newtonsoft.Json;
using Resources;
using Services.Core;
using Services.Documents;
using Services.User;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Web.Mvc;
using Utilities.Constants;
using WebApp.Components;
using WebApp.Constants;
using WebApp.Extensions;
using WebApp.Models;
using WebApp.UserTracking;
using DomainModel.User;
using Services.Billing;
using Servicess.Tests;
using Services.Communication;

namespace WebApp.Controllers
{
    [Authorize]
    public class UserController : BaseController
    {
        private readonly App.Services.IBillingService BillingService;
        public UserController()
        {
            BillingService = DependencyResolver.Current.GetService<App.Services.IBillingService>();
        }

        [AllowAnonymous]
        public ActionResult SignIn()
        {
            if (User.IsSignedIn())
                return Redirect(Url.BuildUrl<DashboardController>(dash => dash.Home(), RouteNames.Dashboard));

            var model = new SignInModel();
            model.RememberMe = true;
            model.Heading = Resource.WelcomeBackMsg;
            model.ErrorMessage = string.Empty;
            if (UserOperations.IsUserTimedOut)
            {
                //  Set Heading: Session Expired and Subheading: Your previous session has timed out. Please sign into your account again.
                model.Heading = Resource.SessionExpiredMsg;
                model.SubHeading = Resource.SessionTimeOutMsg;
            }
            else if (UserOperations.IsUserSignedOut)
            {
                var domainName = Common.DomainName;
                model.Heading = string.Format(Resource.LogoutMsg, domainName);
            }

            int docId;
            if (int.TryParse(Request.QueryString[RouteParameters.DocumentId], out docId) && docId > 0)
            {
                var sessionId = Common.GetSessionId();
                var coreService = new CoreService();
                coreService.SaveToSessionData(sessionId, SessionDataTypeConstants.DocumentId, docId);
                model.DocId = docId;
            }

            var targ = Request.QueryString[RouteParameters.Target];
            if (!string.IsNullOrEmpty(targ))
            {
                var sessionId = Common.GetSessionId();
                var coreService = new CoreService();
                coreService.SaveToSessionData(sessionId, SessionDataTypeConstants.PageTarget, targ);
            }

            return View(model);
        }

        [HttpPost]
        [AllowAnonymous]
        public ActionResult SignIn(SignInModel model)
        {
            if (User.IsSignedIn())
                return Redirect(Url.BuildUrl<DashboardController>(dash => dash.Home(), RouteNames.Dashboard));

            var errorMessage = string.Empty;
            if (ModelState.IsValid)
            {
                var userService = new UserService();
                var portalId = Common.GetPortalId();
                var user = userService.Login(model.UserName, model.Password, portalId);
                if (user == null || user.ID <= 0)
                {
                    model.ErrorMessage = Resource.EmailNotFoundMsg;
                    return View(model);
                }

                if (!user.IsActive)
                {
                    model.ErrorMessage = Resource.AccountDeactivatedMsg;
                    return View(model);
                }

                //BEGIN: Detect Right Code
                //Save SessionGUID in SessionUserAgent table.
                var sessionId = Common.GetSessionId();
                var coreService = new CoreService();

                UserOperations.IdentitySignIn(user.ID, user.UserName, UserOperations.GetDisplayName(user), UserConstants.User, UserAuthTypeCD.SignIn, model.RememberMe);
                userService.SetUserLastLoginDate(user.ID);

                if (user.ResetPwd)
                    return Redirect(Url.BuildUrl<UserController>(u => u.ResetPassword(), RouteNames.UserResetPassword));
                if (Request.QueryString.HasKeys() &&
                    !string.IsNullOrEmpty(Request.QueryString[QueryStringConstants.ReturnUrl]))
                {
                    /// Set it empty, Authentication Framework will automatically redirect.
                }
                else
                {
                    var docId = 0;
                    if (Request.QueryString[RouteParameters.DocumentId] != null && int.TryParse(
                            new CoreService().GetFromSessionData(sessionId, SessionDataTypeConstants.DocumentId),
                            out docId))
                    {
                        var svcDoc = new DocumentService();
                        svcDoc.UpdateUserOfDocument(docId, user.ID);
                        var targetUrl =
                            new CoreService().GetFromSessionData(sessionId, SessionDataTypeConstants.PageTarget);
                        if (!string.IsNullOrEmpty(targetUrl))
                        {
                            if (targetUrl.Contains("rreview"))
                            {
                                // targetUrl = utility.CreateRedirUrl(PageName.Review, FolderName.Review);
                            }
                            else if (targetUrl.Contains(RouteUrls.Preview))
                            {
                                return RedirectToRoute(RouteNames.ResumeFinalize, new { id = docId });
                            }
                        }

                        //else
                        //return Redirect(Url.BuildUrl<WizardController>(wiz => wiz.Experience(docId), RouteNames.Experience));
                    }
                    else
                    {
                        return Redirect(Url.BuildUrl<DashboardController>(dashboard => dashboard.Home(),
                            RouteNames.Dashboard));
                    }
                }
            }

            return View(model);
        }

        [AllowAnonymous]
        public ActionResult SignOut()
        {
            UserOperations.IdentitySignOut();
            return Redirect(Url.BuildUrl<UserController>(user => user.SignIn(), RouteNames.UserSignIn));
        }
        
        public ActionResult Settings()
        {
            var settingsModel = new AccountSettingsModel();
            var userService = new UserService();
            var user = userService.GetUserByID(UserId);
            settingsModel.Name = user.FirstName;
            settingsModel.MemberSince = user.CreationDate;
            settingsModel.EmailAddress = user.UserName;
            settingsModel.AccountId = UserId;
            var userSelection = userService.GetEmailSettingsByUserID(UserId);
            if (userSelection != null)
            {
                settingsModel.EmailsSubscribed =
                    userSelection.Any(x => x.OptinID == UserNotificationConstants.Emails && x.Response == 1);
                settingsModel.JobsSubscribed =
                    userSelection.Any(x => x.OptinID == UserNotificationConstants.Jobs && x.Response == 1);
            }

            settingsModel.Subscriptions = BillingService.GetAllSubscriptionsByUserId(UserId).Result;
            return View(settingsModel);
        }

        [HttpPost]
        [Route(AjaxRoutes.Users + "/" + AjaxRoutes.SaveSetting, Name = AjaxRouteNames.SaveSettings)]
        public ActionResult SaveSettings(IList<SettingsModel> data)
        {
            if (data != null && data.Count > 0)
            {
                var notifications = new List<UserOptin>();
                foreach (var notification in data)
                    notifications.Add(new UserOptin
                    {
                        UserID = UserId, OptinID = notification.NotificationId, TimeStamp = DateTime.UtcNow,
                        Response = Convert.ToInt16(notification.Value)
                    });
                var userService = new UserService();
                userService.SaveAllOptIn(notifications);
                return ToJson(true);
            }

            return HttpCodeJsonResponse(HttpStatusCode.BadRequest);
        }

        private string ConductABTest(int ABTestID)
        {
            if (UserId > 0)
            {
                var abtestservice = new ABTestService();
                return Convert.ToString(abtestservice.ConductABTest(UserId, ABTestID));
            }

            return null;
        }

        [AllowAnonymous]
        public ActionResult ForgotPassword()
        {
            return View(new ForgetPasswordModel());
        }

        [HttpPost]
        [AllowAnonymous]
        public ActionResult ForgotPassword(ForgetPasswordModel model)
        {
            if (!ModelState.IsValid)
                return this.RedirectToAction(login => login.SignIn());
            var userService = new UserService();
            var user = userService.GetUserByEmail(model.Email, Common.GetPortalId());
            if (user != null)
            {
                var domainURL = Common.GetDomainURL();
                var requestGuid = Guid.NewGuid();
                var loginLink = Common.CreateDesktopRouteUrl(DesktopRoute.Login, DesktopRoute.User);
                var resetPwdUrl = Common.CreateDesktopRouteUrl(DesktopRoute.ResetPassword, DesktopRoute.User);
                var resetPasswordLink = string.Format(
                    "<a href='" + resetPwdUrl + "?" + QueryStringConstants.UserId + "={0}&" +
                    QueryStringConstants.PasswordChangeGUID + "={1}'>" + Resource.PasswordRecoveryText + "</a>",
                    user.Id.ToString(), requestGuid);
                var mail = new CommunicationService();
                if (mail.SendPasswordRecoveryEmail(user.Id, user.UserName, user.FirstName, resetPasswordLink,
                    string.Empty, loginLink))
                    userService.SetUserDataByDataTypeCD(user.Id, UserDataType.PasswordEmailGuid,
                        requestGuid.ToString());
                return this.RedirectToAction(login => login.ForgotPwdConfirmation());
            }

            model.ErrorMessage = Resource.EmailNotRecognizedMsg;
            return View(model);
        }

        [AllowAnonymous]
        public ActionResult ForgotPwdConfirmation()
        {
            return View();
        }

        [AllowAnonymous]
        public ActionResult ResetPassword()
        {
            UserMaster user = null;
            if (!string.IsNullOrEmpty(Request.QueryString[QueryStringConstants.PasswordChangeGUID]))
            {
                user = GetUserForPasswordRecoveryLink();
            }
            else
            {
                var userService = new UserService();
                user = userService.GetUserByID(UserId);
            }

            if (user != null && !string.IsNullOrEmpty(user.UserName))
                return View(new ResetPasswordModel());

            return this.RedirectToAction(login => login.SignIn());
        }

        [HttpPost]
        [AllowAnonymous]
        public ActionResult ResetPassword(ResetPasswordModel model)
        {
            if (!ModelState.IsValid)
                return View();

            var userId = Request.QueryString[QueryStringConstants.UserId];
            var passwordGuid = Request.QueryString[QueryStringConstants.PasswordChangeGUID];
            if (string.IsNullOrEmpty(userId) || string.IsNullOrEmpty(passwordGuid))
            {
                model.ErrorMessage = Resource.RetryPasswordMsg;
                return View(model);
            }

            UserMaster user = null;
            var userService = new UserService();
            if (!string.IsNullOrEmpty(Request.QueryString[QueryStringConstants.PasswordChangeGUID]))
                user = GetUserForPasswordRecoveryLink();
            else
                user = userService.GetUserByID(UserId);

            if (user != null)
            {
                user.ResetPwd = false;
                if (userService.UpdateUser(user, model.NewPassword))
                {
                    var userAlreadyLoggedIn = User.IsSignedIn() && UserId == user.Id;
                    if (!userAlreadyLoggedIn)
                    {
                        userService.SetUserDataByDataTypeCD(user.Id, UserDataType.PasswordEmailGuid, string.Empty);
                        UserOperations.DoAutoLogin();
                    }

                    return Redirect(Url.BuildUrl<DashboardController>(dash => dash.Home(), RouteNames.Dashboard));
                }

                model.ErrorMessage = Resource.RetryPasswordMsg;
            }
            else
            {
                model.ErrorMessage = Resource.RetryPasswordMsg;
            }

            return View(model);
        }


        [HttpPost]
        [Route(AjaxRoutes.Users + "/" + AjaxRoutes.UpdatePassword, Name = AjaxRouteNames.UpdatePassword)]
        public ActionResult UpdatePassword(ResetPasswordModel model)
        {
            if (string.IsNullOrEmpty(model.Password))
                return ToJson(Resource.EnterPassword);
            if (string.IsNullOrEmpty(model.NewPassword))
                return ToJson(Resource.EnterConfirmPassword);

            var userService = new UserService();
            var userMaster = userService.GetUserByID(UserId);
            if (userMaster != null && userService.ValidatePassword(userMaster.HashedPwd, model.Password))
            {
                if (userService.UpdateUser(userMaster, model.NewPassword))
                    return ToJson(true);
            }
            else
            {
                return ToJson(Resource.CheckCredentialsMsg);
            }

            return ToJson(Resource.ProblemInPasswordChange);
        }

        public ActionResult Summary()
        {
            return View("dummy");
        }

        [HttpPost]
        public ActionResult Summary(List<string> newOptins, string oldOptins, string allOptins)
        {
            return this.RedirectToAction<DashboardController>(c => c.Home());
        }

        [HttpPost]
        [AllowAnonymous]
        public ActionResult LogInAsUser(string loginWithToken)
        {
            return View();
        }

        [HttpPost]
        [Route(AjaxRoutes.Users + "/" + AjaxRoutes.Stage, Name = AjaxRouteNames.UpdateProgress)]
        [AllowAnonymous]
        public ActionResult UpdateProgress(ProgressData userStageData)
        {
            var stageUpdated = false;
            if (userStageData != null && !string.IsNullOrEmpty(userStageData.StageCD))
            {
                var userService = new UserService();
                var identity = UserOperations.Identity;
                if (identity != null && identity.UserId > 0 && userStageData != null)
                {
                    if (userService.SetProgress(identity.UserId, userStageData.StageCD))
                        stageUpdated = true;
                    //Update User Stage and releated data for WelcomeBackModel
                    userService.SetUserDataByDataTypeCD(identity.UserId, UserDataType.WelcomeBackFlow,
                        JsonConvert.SerializeObject(userStageData));
                }
            }

            return ToJson(stageUpdated);
        }

        private UserMaster GetUserForPasswordRecoveryLink()
        {
            UserMaster userMaster = null;
            var uid = Request.QueryString[QueryStringConstants.UserId];
            var pguid = Request.QueryString[QueryStringConstants.PasswordChangeGUID];
            if (!string.IsNullOrEmpty(pguid) && !string.IsNullOrEmpty(uid))
            {
                var userId = 0;
                int.TryParse(uid, out userId);
                Guid passwordChangeGuid;
                Guid.TryParse(pguid, out passwordChangeGuid);
                if (userId > 0 && passwordChangeGuid != default(Guid))
                {
                    var userService = new UserService();
                    var userData = userService.GetUserDataByDataTypeCD(userId, UserDataType.PasswordEmailGuid);
                    if (userData != null && userData.Data == pguid) userMaster = userService.GetUserByID(userId);
                }
            }

            return userMaster;
        }
    }
}