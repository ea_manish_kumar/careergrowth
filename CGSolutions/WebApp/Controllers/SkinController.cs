﻿using App.Utility.Constants;
using DomainModel;
using Microsoft.Web.Mvc;
using Newtonsoft.Json;
using Services.Documents;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Text;
using System.Web.Mvc;
using Utilities;
using Utilities.Constants.Documents;
using WebApp.Constants;
using WebApp.Extensions;
using WebApp.Models;
using System.Threading.Tasks;

namespace WebApp.Controllers
{
    public class SkinController : BaseController
    {

        private readonly App.Services.IBillingService BillingService;
        private readonly App.Services.IUserService UserService;

        public SkinController()
        {
            BillingService = DependencyResolver.Current.GetService<App.Services.IBillingService>();
            UserService = DependencyResolver.Current.GetService<App.Services.IUserService>();
        }
        private List<string> SectionsList =>
            new List<string>
            {
                SectionTypeCD.Name,
                SectionTypeCD.Contact,
                SectionTypeCD.Skills,
                SectionTypeCD.Summary,
                SectionTypeCD.Experience,
                SectionTypeCD.Education,
                SectionTypeCD.Accomplishments,
                SectionTypeCD.Additional_Information,
                SectionTypeCD.Affiliations,
                SectionTypeCD.Awards,
                SectionTypeCD.Highlights,
                SectionTypeCD.Interests,
                SectionTypeCD.Language,
                SectionTypeCD.Objectives,
                SectionTypeCD.PersonalInformation,
                SectionTypeCD.References,
                SectionTypeCD.Volunteer,
                SectionTypeCD.Training
            };

        public JsonResult GetTemplates()
        {
            ViewBag.IsTemplate = true;
            var docModel = GetTestModel();
            var templates = new ArrayList
            {
                new { skin = "artistic", template = RenderRazorViewToString("Artistic", docModel) },
                new { skin = "basic", template = RenderRazorViewToString("Basic", docModel) },
                new { skin = "cool", template = RenderRazorViewToString("Cool", docModel) },
                new { skin = "elegant", template = RenderRazorViewToString("Elegant", docModel) },
                new { skin = "superb", template = RenderRazorViewToString("Superb", docModel) },
                new { skin = "Executive", template = RenderRazorViewToString("Executive", docModel) },
                new { skin = "multicolor", template = RenderRazorViewToString("MultiColor", docModel) },
                new { skin = "soothing", template = RenderRazorViewToString("Soothing", docModel) },
                new { skin = "awesome", template = RenderRazorViewToString("Awesome", docModel) },
                new { skin = "smart", template = RenderRazorViewToString("Smart", docModel) },
                new { skin = "standout", template = RenderRazorViewToString("StandOut", docModel) },
                new { skin = "professional", template = RenderRazorViewToString("professional", docModel) }
            };
            return Json(templates, JsonRequestBehavior.AllowGet);
        }

        [Route("document/preview/{id:int}", Name = RouteNames.DocumentPreview)]
        public ActionResult GetDocumentPreview(int id)
        {
            ViewBag.IsTemplate = true;
            if (id > 0)
            {
                var html = GetDocumentHTML(id);
                if (!string.IsNullOrEmpty(html)) return Content(html, "text/html; charset=utf-8");
                return Content(null, "text/html; charset=utf-8");
            }

            return Content(null, "text/html; charset=utf-8");
        }

        private string GetDocumentHTML(int documentId)
        {
            var documentService = new DocumentService();
            var document = documentService.GetDocumentByID(documentId, UserId);
            if (document != null && !string.IsNullOrEmpty(document.SkinCD))
                return this.RenderView(GetViewNameBySkinCD(document.SkinCD), SectionConvertor.ToDocumentModel(document));
            return string.Empty;
        }

        [HttpPost]
        [Route(AjaxRoutes.Documents + "/" + AjaxRoutes.EmailDoc, Name = AjaxRouteNames.EmailDocument)]
        public ActionResult EmailDocument(int? documentId, EmailModel emailObj, string userAction)
        {
            bool isSuccess = false;
            if (documentId.HasValue && documentId.Value > 0)
            {
                var documentService = new DocumentService();
                var document = documentService.GetDocumentById(documentId.Value);
                if (document == null)
                    return this.RedirectToAction<ErrorController>(c => c.ServerError());
                Skin selectedSkin;
                var skinId = 1;
                if (DocumentService.SkinMapping.TryGetValue(document.SkinCD, out selectedSkin))
                    skinId = selectedSkin.SkinID;
                var isvalidExportRequest = BillingService.IsValidExportRequest(UserId, skinId).Result;
                if (!isvalidExportRequest)
                    return Redirect(Url.BuildUrl<OrderController>(
                        c => c.Routing(documentId.Value, skinId, userAction, null, document.DocumentTypeCD),
                        RouteNames.OrderRouting));
                if (!documentService.IsDocumentRequestValid(document, UserId))
                    return HttpCodeJsonResponse(HttpStatusCode.Forbidden);

                var mimeType = string.Empty;
                string formatType;
                var docType = string.Empty;
                string extension;
                GetMimeType(ref docType, ref mimeType, out formatType, out extension);
                var docFile = GenerateDocument(document.Name, formatType, document);

                if (docFile != null)
                {
                    //TODO: Integrate the email functionality here

                    //CommunicationService mail = new CommunicationService();
                    //bool successFlag = mail.SendResumeByMail(formatType, docFile, emailObj.EmailTo, string.Empty, emailObj.EmailSubject, emailObj.EmailBody);
                    //return ToJson(successFlag);

                    isSuccess = true;
                }
                
                //Record the user's document email event
                var documentEmailFailedAction = JsonConvert.SerializeObject(new UserDownloadEvent
                { DocumentId = document.Id, Action = AjaxRouteNames.EmailDocument, IsSuccess = isSuccess });
                CaptureUserEvent(documentEmailFailedAction);

                return ToJson(isSuccess);
            }

            return HttpCodeJsonResponse(HttpStatusCode.BadRequest);
        }

        private File GenerateDocument(string name, string formatType, Document document)
        {
            var file = new File();
            file.FileTypeCD = formatType;
            file.Name = name;
            var pdfUtil = new PDFUtility();
            ViewBag.IsDownload = true;
            var html = GetDocumentHTML(document.Id);
            var uniqueName = string.Concat(document.Id, document.UserID);

            var pdfProps = new PDFProperties();
            pdfProps.FooterText = string.Empty;
            pdfProps.IsFooterPresent = false;
            var topBottomMargin = document.DocStyles.FirstOrDefault(x => x.StyleCD == Styles.TOPBOTTOMMARGINS);
            if (topBottomMargin != null)
                pdfProps.TopBottomMargin = GetMarginInMM(topBottomMargin.Value);

            var leftRightMargin = document.DocStyles.FirstOrDefault(x => x.StyleCD == Styles.SIDEMARGINS);
            if (leftRightMargin != null)
                pdfProps.LeftRightMargin = GetMarginInMM(leftRightMargin.Value);

            var pageSize = document.DocStyles.FirstOrDefault(x => x.StyleCD == Styles.PageSize);
            if (pageSize != null)
                pdfProps.PageSize = pageSize.Value;

            file.BinaryData = pdfUtil.ConvertHTMLToPDF(html, uniqueName, pdfProps);
            return file;
        }

        [HttpGet]
        [Route(AjaxRoutes.Documents + "/" + AjaxRoutes.DownloadDoc, Name = AjaxRouteNames.DownloadDocument)]
        public async Task<ActionResult> DownloadDocumentAsync(int? documentId, string docType, string userAction)
        {
            if (documentId.HasValue && documentId.Value > 0)
            {
                var documentService = new DocumentService();
                var document = documentService.GetDocumentById(documentId.Value);
                if (document == null)
                    return this.RedirectToAction<ErrorController>(c => c.ServerError());

                if (!docType.Equals(FileTypeCD.Text))
                {
                    Skin selectedSkin;
                    var skinId = 1;
                    if (DocumentService.SkinMapping.TryGetValue(document.SkinCD, out selectedSkin))
                        skinId = selectedSkin.SkinID;
                    //var isvalidExportRequest = await BillingService.IsValidExportRequest(UserId, skinId);
                    //if (!isvalidExportRequest)
                        //return this.RedirectToAction<OrderController>(c => c.Routing(documentId.Value, skinId, userAction, document.DocumentTypeCD, docType));
                }
                if (documentService.IsDocumentRequestValid(document, UserId))
                {
                    var mimeType = string.Empty;
                    string formatType;
                    string extension;
                    GetMimeType(ref docType, ref mimeType, out formatType, out extension);
                    var docFile = GenerateDocument(document.Name, formatType, document);
                    
                    if (docFile != null && (docFile.TextData != null || docFile.BinaryData != null))
                    {
                        var data = docFile.TextData != null
                            ? Encoding.ASCII.GetBytes(docFile.TextData)
                            : docFile.BinaryData;

                        //Since the document exists, record the valid document downloaded event
                        var downloadAction = JsonConvert.SerializeObject(new UserDownloadEvent
                        { DocumentId = document.Id, Action = userAction, IsSuccess = true });
                        //CaptureUserEvent(downloadAction);
                        return File(data, mimeType, docFile.Name + extension);
                    }

                    //Since the document doesn't exist, record the invalid document downloaded event
                    var downloadFailedAction = JsonConvert.SerializeObject(new UserDownloadEvent
                    { DocumentId = document.Id, Action = userAction, IsSuccess = false });
                    //CaptureUserEvent(downloadFailedAction);

                    return this.RedirectToAction<ErrorController>(c => c.NotFound());
                }

                return this.RedirectToAction<ErrorController>(c => c.Forbidden());
            }

            return this.RedirectToAction<ErrorController>(c => c.NotFound());
        }

        private void CaptureUserEvent(string actionData)
        {
            short eventId = UserService.GetEventId(EventType.DownloadAction).Result;
            App.DomainModel.UserEventData userEventData = new App.DomainModel.UserEventData()
            {
                UserId = UserId,
                CreatedOnUtc = DateTime.UtcNow,
                EventId = eventId,
                EventData = actionData
            };
            //Record the event that user performed
            UserService.SaveUserEvent(userEventData);
        }

        private void GetMimeType(ref string docType, ref string mimeType, out string formatType, out string extension)
        {
            if (string.IsNullOrEmpty(docType))
                docType = FileTypeCD.PDF;

            extension = string.Empty;
            CGUtility.GetFileFormat(docType, out formatType, out mimeType, out extension);
            if (docType.Equals(FileTypeCD.Word, StringComparison.InvariantCultureIgnoreCase))
                if (!string.IsNullOrEmpty(Request.UserAgent))
                {
                    var iOsMobileDevices = new List<string> { "iphone", "ipad", "ipod" };
                    var isIOsMobileDevice =
                        iOsMobileDevices.Any(device => Request.UserAgent.ToLower().Contains(device));
                    if (isIOsMobileDevice)
                        mimeType = "application/vnd.openxmlformats-officedocument.wordprocessingml.document";
                }
        }


        [HttpPost]
        [Route(AjaxRoutes.Documents + "/" + AjaxRoutes.RenameDoc, Name = AjaxRouteNames.RenameDocument)]
        public ActionResult RenameDocument(int docId, string docName)
        {
            bool isSuccess = false;
            if (docId > 0)
            {
                var docSvc = new DocumentService();
                if (docSvc.CheckDocumentName(docId, docName) > 0)
                {
                    docSvc.RenameDocument(docId, docName);
                    isSuccess = true;
                }

                //Since the document renamed by user, record the emailed event
                var documentRenameAction = JsonConvert.SerializeObject(new UserDocumentRenameEvent
                { DocumentId = docId, Action = AjaxRouteNames.RenameDocument, IsSuccess = isSuccess, NewDocumentName = docName });
                CaptureUserEvent(documentRenameAction);

                return ToJson(isSuccess);
            }

            return HttpCodeJsonResponse(HttpStatusCode.BadRequest);
        }

        [HttpPost]
        [Route(AjaxRoutes.Documents + "/" + AjaxRoutes.DeleteDoc, Name = AjaxRouteNames.DeleteDocument)]
        public ActionResult DeleteDocument(int docId)
        {
            if (docId > 0)
            {
                var docSvc = new DocumentService();
                docSvc.DeleteDocumentByID(docId);

                //Since the document deleted by user, record the deleted event
                var documentDeleteAction = JsonConvert.SerializeObject(new UserActionEvent
                { DocumentId = docId, Action = AjaxRouteNames.DeleteDocument});
                CaptureUserEvent(documentDeleteAction);

                return ToJson(true);
            }

            return HttpCodeJsonResponse(HttpStatusCode.BadRequest);
        }

        [HttpPost]
        [Route(AjaxRoutes.Documents + "/" + AjaxRoutes.CreateDuplicateDoc, Name = AjaxRouteNames.DuplicateDocument)]
        public ActionResult CreateDuplicateDocument(int oldDocId)
        {
            if (oldDocId > 0)
            {
                var docSvc = new DocumentService();
                var currentDocument = docSvc.GetDocumentByID(oldDocId, UserId);
                if (currentDocument != null)
                {
                    //TODO: Move the copy"Copy({0})" to resource file
                    var docName = String.Format("Copy({0})", currentDocument.Name);
                    var documentId = docSvc.CopyDocument(oldDocId, UserId, docName);

                    //Since the document copied, record the copied event
                    var documentCopyAction = JsonConvert.SerializeObject(new UserDocumentCopyEvent
                    { DocumentId = oldDocId, Action = AjaxRouteNames.DuplicateDocument, NewDocumentId = documentId });
                    CaptureUserEvent(documentCopyAction);

                    return ToJson(new
                    {
                        docId = documentId,
                        name = docName,
                        timestamp = DateTime.UtcNow.ToString(),
                        template = GetSelectedSkin(currentDocument.SkinCD)
                    });
                }

                return ToJson(false);
            }

            return HttpCodeJsonResponse(HttpStatusCode.BadRequest);
        }


        [HttpPost]
        [Route(AjaxRoutes.Documents + "/{documentId}/" + AjaxRoutes.Styles, Name = AjaxRouteNames.UpdateDocStyles)]
        public ActionResult UpdateDocStyles(int documentId, IDictionary<string, string> styles)
        {
            if (documentId > 0 && styles != null && styles.Count > 0)
            {
                var documentService = new DocumentService();
                var document = documentService.GetDocumentByID(documentId, UserId);
                if (document != null)
                {
                    if (styles.ContainsKey(Styles.THEME))
                    {
                        documentService.PopulateThemeStyles(document, styles);
                        documentService.UpdateStyles(document, styles);
                    }

                    var html = this.RenderView(GetViewNameBySkinCD(document.SkinCD), SectionConvertor.ToDocumentModel(document));
                    return Content(html, "text/html; charset=utf-8");
                }

                return HttpCodeJsonResponse(HttpStatusCode.BadRequest);
            }

            return HttpCodeJsonResponse(HttpStatusCode.BadRequest);
        }

        [HttpPost]
        [Route(AjaxRoutes.Documents + "/{documentId}/" + AjaxRoutes.Skin, Name = AjaxRouteNames.UpdateSkin)]
        public ActionResult UpdateSkin(int documentId, string templateCD)
        {
            if (documentId > 0 && !string.IsNullOrEmpty(templateCD))
            {
                var documentService = new DocumentService();
                var document = documentService.GetDocumentByID(documentId, UserId);
                if (document != null)
                {
                    documentService.UpdateDocSkin(document.Id, templateCD);
                    var html = this.RenderView(GetViewNameBySkinCD(document.SkinCD), SectionConvertor.ToDocumentModel(document));
                    return Content(html, "text/html; charset=utf-8");
                }

                return HttpCodeJsonResponse(HttpStatusCode.BadRequest);
            }

            return HttpCodeJsonResponse(HttpStatusCode.BadRequest);
        }

        [HttpPost]
        [Route(AjaxRoutes.Documents + "/{documentId}/" + AjaxRoutes.Sections + "/" + AjaxRoutes.Sort, Name = AjaxRouteNames.SortGroups)]
        public ActionResult SortSections(int documentId, SortedSection[] sortedGroups)
        {
            if (documentId > 0 && sortedGroups != null && sortedGroups.Length > 0)
            {
                var documentService = new DocumentService();
                var document = documentService.GetDocumentByID(documentId, UserId);
                if (document != null)
                {
                    if (document.Sections != null && document.Sections.Count > 0)
                    {
                        var doSave = false;
                        foreach (var sortedSection in sortedGroups)
                        {
                            var section = document.Sections.FirstOrDefault(sec => sec.Id == sortedSection.GroupId);
                            if (section != null && section.SortIndex != sortedSection.SortIndex)
                            {
                                section.SortIndex = sortedSection.SortIndex;
                                doSave = true;
                            }
                        }

                        if (doSave)
                        {
                            documentService.UpdateDocument(document);
                            var html = this.RenderView(GetViewNameBySkinCD(document.SkinCD), SectionConvertor.ToDocumentModel(document));
                            return Content(html, "text/html; charset=utf-8");
                        }
                    }

                    return Content(string.Empty, "text/html; charset=utf-8");
                }

                return HttpCodeJsonResponse(HttpStatusCode.BadRequest);
            }

            return HttpCodeJsonResponse(HttpStatusCode.BadRequest);
        }

        private string GetViewNameBySkinCD(string skinCD)
        {
            switch (skinCD)
            {
                case SkinCode.Artistic: return "~/Views/Skin/Artistic.cshtml";
                case SkinCode.Awesome: return "~/Views/Skin/Awesome.cshtml";
                case SkinCode.Basic: return "~/Views/Skin/Basic.cshtml";
                case SkinCode.Cool: return "~/Views/Skin/Cool.cshtml";
                case SkinCode.Elegant: return "~/Views/Skin/Elegant.cshtml";
                case SkinCode.Executive: return "~/Views/Skin/Executive.cshtml";
                case SkinCode.MultiColor: return "~/Views/Skin/MultiColor.cshtml";
                case SkinCode.Professional: return "~/Views/Skin/Professional.cshtml";
                case SkinCode.Smart: return "~/Views/Skin/Smart.cshtml";
                case SkinCode.Soothing: return "~/Views/Skin/Soothing.cshtml";
                case SkinCode.StandOut: return "~/Views/Skin/StandOut.cshtml";
                case SkinCode.Superb: return "~/Views/Skin/Superb.cshtml";
            }

            return "~/Views/Skin/Basic.cshtml";
        }

        private string GetSelectedSkin(string skinCD)
        {
            var skinDescription = string.Empty;
            Skin selectedSkin;
            if (DocumentService.SkinMapping.TryGetValue(skinCD, out selectedSkin))
                skinDescription = selectedSkin.Description;
            return skinDescription;
        }

        private double GetMarginInMM(string margin)
        {
            double updatedValue = 10;
            if (!string.IsNullOrEmpty(margin))
            {
                if (margin.Contains("in"))
                {
                    margin = margin.Replace("in", string.Empty);
                    if (margin.Contains("0 "))
                        margin = margin.Replace("0 ", string.Empty);
                    if (double.TryParse(margin, out updatedValue) && updatedValue > 0)
                        updatedValue = updatedValue * 24.5;
                }
                else
                    double.TryParse(margin, out updatedValue);
            }

            return updatedValue;
        }

        #region SKIN TEST VIEWS

        [Route("ABS1", Name = SkinRouteNames.ABS1)]
        public ActionResult ABS1()
        {
            ViewBag.IsTemplate = true;
            return View(GetTestModel());
        }

        [Route("Artistic", Name = SkinRouteNames.Artistic)]
        public ActionResult Artistic()
        {
            ViewBag.IsTemplate = true;
            return View(GetTestModel());
        }

        [Route("Basic", Name = SkinRouteNames.Basic)]
        public ActionResult Basic()
        {
            ViewBag.IsTemplate = true;
            return View(GetTestModel());
        }

        [Route("Cool", Name = SkinRouteNames.Cool)]
        public ActionResult Cool()
        {
            ViewBag.IsTemplate = true;
            return View(GetTestModel());
        }

        [Route("Elegant", Name = SkinRouteNames.Elegant)]
        public ActionResult Elegant()
        {
            ViewBag.IsTemplate = true;
            return View(GetTestModel());
        }

        [Route("Superb", Name = SkinRouteNames.Superb)]
        public ActionResult Superb()
        {
            ViewBag.IsTemplate = true;
            return View(GetTestModel());
        }

        [Route("Executive", Name = SkinRouteNames.Executive)]
        public ActionResult Executive()
        {
            ViewBag.IsTemplate = true;
            return View(GetTestModel());
        }

        [Route("MultiColor", Name = SkinRouteNames.MultiColor)]
        public ActionResult MultiColor()
        {
            ViewBag.IsTemplate = true;
            return View(GetTestModel());
        }

        [Route("Soothing", Name = SkinRouteNames.Soothing)]
        public ActionResult Soothing()
        {
            ViewBag.IsTemplate = true;
            return View(GetTestModel());
        }

        [Route("Awesome", Name = SkinRouteNames.Awesome)]
        public ActionResult Awesome()
        {
            ViewBag.IsTemplate = true;
            return View(GetTestModel());
        }

        [Route("Smart", Name = SkinRouteNames.Smart)]
        public ActionResult Smart()
        {
            ViewBag.IsTemplate = true;
            return View(GetTestModel());
        }

        [Route("Standout", Name = SkinRouteNames.Standout)]
        public ActionResult Standout()
        {
            ViewBag.IsTemplate = true;
            return View(GetTestModel());
        }

        [Route("Professional", Name = SkinRouteNames.Professional)]
        public ActionResult Professional()
        {
            ViewBag.IsTemplate = true;
            return View(GetTestModel());
        }

        public DocumentModel GetTestModel()
        {
            var model = new DocumentModel
            {
                Name = new NameSectionModel
                {
                    SortIndex = 1,
                    Name = "Jeniffer Lopez"
                },
                Contact = new ContactSectionModel
                {
                    SortIndex = 1,
                    Address = "111",
                    City = "London Street",
                    State = "London",
                    ZipCode = "12321",
                    Email = "info@myresume.com",
                    Phone = "9999999999",
                    AltPhone = "9999999999"
                },
                Experience = new ExperienceDetails
                {
                    SortIndex = 3,
                    Paragraphs = new List<Experience>
                    {
                        new Experience
                        {
                            JobTitle = "Customer Service Head",
                            Employer = "IBM",
                            City = "Mountain View",
                            State = "CA",
                            StartDateMonth = 2,
                            StartDateYear = 2014,
                            PresentlyWorkHere = true,
                            EndDateYear = 2017,
                            Description =
                            "<ul><li>Defined different inventory control processes, resulting in a reduction of inventory shrink to a good extent</li><li>Took ownership and accountable for all the operational activity in the store</li><li>Exhibited great leadership skills and maintained a team of multiple assistant managers and sales associates</li></ul>"
                        },
                        new Experience
                        {
                            JobTitle = "Sales Executive",
                            Employer = "Accenture",
                            City = "San Francisco",
                            State = "CA",
                            StartDateMonth = 1,
                            StartDateYear = 2010,
                            PresentlyWorkHere = true,
                            EndDateMonth = 5,
                            EndDateYear = 2014,
                            Description =
                            "<ul><li>Ensured that all the operational goals and sales targets are met</li><li>Ensured that all the defined processes and company policies are followed</li></ul>"
                        },
                        new Experience
                        {
                            JobTitle = "Store Manager",
                            Employer = "Burger King",
                            City = "San Francisco",
                            State = "CA",
                            StartDateMonth = 8,
                            StartDateYear = 2008,
                            PresentlyWorkHere = true,
                            EndDateMonth = 7,
                            EndDateYear = 2011,
                            Description =
                            "<ul><li>Defined different inventory control processes, resulting in a reduction of inventory shrink to a good extent</li><li>Ensured that all the operational goals and sales targets are met</li><li>Consistently delivered customer satisfaction by identifing their needs and established good rapport with guests</li></ul>"
                        }
                    }
                },
                Training = new ExperienceDetails
                {
                    SortIndex = 4,
                    Paragraphs = new List<Experience>
                    {
                        new Experience
                        {
                            JobTitle = "Customer Service Head",
                            Employer = "IBM",
                            City = "Mountain View",
                            State = "CA",
                            StartDateMonth = 2,
                            StartDateYear = 2014,
                            PresentlyWorkHere = true,
                            EndDateYear = 2017,
                            Description =
                            "<ul><li>Defined different inventory control processes, resulting in a reduction of inventory shrink to a good extent</li><li>Took ownership and accountable for all the operational activity in the store</li><li>Exhibited great leadership skills and maintained a team of multiple assistant managers and sales associates</li></ul>"
                        }
                    }
                },
                Education = new EducationDetails
                {
                    SortIndex = 5,
                    Paragraphs = new List<Education>
                    {
                        new Education
                        {
                            School = "OXFORD University",
                            Degree = "Master of Business Administration",
                            City = "Boston",
                            State = "CA",
                            SchoolMonth = 4,
                            SchoolYear = 2014,
                            Description =
                            "A dedicated professional with strong volunteer in customer management. Always kept customer first and took ownership in different areas."
                        }
                    }
                },
                Skills = new SkillDescription
                {
                    SortIndex = 6,
                    Description = "<table class='twocol'><tbody><tr><td class='field twocol_1' id='FIELD_SKC1'><ul><li>Outstanding customer service</li><li>Excellent multi-tasker</li><li>Energetic self-starter</li><li>High accuracy in cash handling</li></ul></td><td class='field twocol_2' id='FIELD_SKC2'><ul><li>Outstanding customer service</li><li>Excellent multi-tasker</li><li>Energetic self-starter</li><li>High accuracy in cash handling</li></ul></td></tr></tbody></table>"
                },
                Summary = new SummaryDescription
                {
                    SortIndex = 2,
                    Description = "Reliable and friendly professional who quickly learns and masters new concepts and skills. Very organized with ability to manage tasks given in an efficient manner. Always believe in working to the best of my ability and advancing my knowledge in the industry. Always exhibit a positive attitude and believe in assisting the customers in any way possible."
                },
                Dissertation = new OtherSection
                {
                    SortIndex = 7,
                    SectionLabel = "Dissertation",
                    Description = "A dedicated professional with strong volunteer in customer management. Always kept customer first and took ownership in different areas.",
                    SectionTypeCD = SectionTypeCD.Dissertation
                },
                Accomplishments = new OtherSection
                {
                    SortIndex = 8,
                    SectionLabel = "Accomplishments",
                    Description = "A dedicated professional with strong volunteer in customer management. Always kept customer first and took ownership in different areas.",
                    SectionTypeCD = SectionTypeCD.Accomplishments
                }
            };

            DocumentHelper.EvaluateDocumentProperties(model);
            return model;
        }

        #endregion
    }
}