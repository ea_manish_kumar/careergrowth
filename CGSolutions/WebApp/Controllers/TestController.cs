﻿using System;
using System.Collections.Generic;
using System.Web.Mvc;
using WebApp.Components;
using WebApp.Constants;
using WebApp.Extensions;
using WebApp.Models.Email;

namespace WebApp.Controllers
{
    [RoutePrefix("test")]
    [AllowAnonymous]
    public class TestController : Controller
    {
        [Route("")]
        public ActionResult Index()
        {
            if (Request.QueryString.HasKeys())
            {
                IDictionary<string, string> model = new Dictionary<string, string>();

                if (!string.IsNullOrEmpty(Request.QueryString["email_templates"]))
                    PopulateEmailTemplates(model);
                if (!string.IsNullOrEmpty(Request.QueryString["email_template"]))
                {
                    var htmlBody = GetEmailTemplateHtml(Request.QueryString["email_template"]);
                    if (!string.IsNullOrEmpty(htmlBody))
                        return Content(htmlBody, "text/html; charset=utf-8");
                    else
                        model.Add("Email template", "Not found");
                }

                return View(model);
            }
            return View();
        }

        private string GetEmailTemplateHtml(string emailTemplate)
        {
            if (!string.IsNullOrEmpty(emailTemplate))
            {
                switch (emailTemplate.ToLower())
                {
                    case "welcome":
                        return this.RenderView(EmailViewNames.Welcome, new WelcomeViewModel { Name = "Bajirao Singham" });
                    case "forgot-password":
                        return this.RenderView(EmailViewNames.ForgotPassword, new ForgotPasswordViewModel
                        {
                            Name = "Bajirao Singham",
                            LinkValidityTime = "1 Hour",
                            SetPasswordLink = Request.Url.GetRootUrl(Url.RouteUrl(RouteNames.AccountResetPassword))
                        });
                    case "subcription-confirmation":
                        return this.RenderView(EmailViewNames.SubscriptionConfirmation, new SubscriptionConfirmationViewModel
                        {
                            Name = "Bajirao Singham",
                            OrderId = "1",
                            Currency = "INR",
                            Amount = 100,
                            SubscriptionExpiryTime = DateTime.UtcNow,
                            SupportEmail = "support@cg.com"
                        });
                    case "invoice":
                        return this.RenderView(EmailViewNames.Invoice, new InvoiceViewModel
                        {
                            TransactionReferenceID = "1",
                            PaymentReferenceID = "1",
                            Currency = "INR",
                            Amount = 100,
                            TransactionTime = DateTime.UtcNow,
                            PaymentReceivedTowards = "CG"
                        });
                }
            }
            return string.Empty;
        }

        private void PopulateEmailTemplates(IDictionary<string, string> model)
        {
            model.Add(GetEmailTemplateUrl("Welcome", "welcome"));
            model.Add(GetEmailTemplateUrl("Forgot Password", "forgot-password"));
            model.Add(GetEmailTemplateUrl("Subcription Confirmation", "subcription-confirmation"));
            model.Add(GetEmailTemplateUrl("Invoice", "invoice"));
        }

        private KeyValuePair<string, string> GetEmailTemplateUrl(string email_template_name, string email_template)
        {
            return new KeyValuePair<string, string>
                (
                    email_template_name,
                    string.Format("<a href=\"{0}\" target=\"_blank\">View Template</a>", Request.Url.GetRootUrl(Url.Action("Index", new { email_template })))
                );
        }
    }
}