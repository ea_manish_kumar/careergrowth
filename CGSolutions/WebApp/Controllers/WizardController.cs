﻿using CommonModules.Configuration;
using DomainModel;
using DomainModel.User;
using Resources;
using Services.Documents;
using Services.User;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Web.Mvc;
using Utilities.Constants;
using Utilities.Constants.Documents;
using WebApp.Constants;
using WebApp.Extensions;
using WebApp.Models;

namespace WebApp.Controllers
{
    [Authorize]
    public class WizardController : BaseController
    {

        #region Action Methods

        [AllowAnonymous]
        public ActionResult HowItWorks()
        {
            return View();
        }

        public ActionResult ChooseTemplate(string mode, int? docId)
        {
            var model = CreateWizardModel(mode, docId, WizardConstants.ChooseTemplate);
            if (model.DocumentCookieModel != null)
            {
                ViewBag.DocumentId = model.DocumentCookieModel.DocumentId;
                ViewBag.SkinCode = model.DocumentCookieModel.SkinCode;
            }

            return View(model);
        }

        public ActionResult Contact(string mode, int? docId)
        {
            var model = CreateWizardModel(mode, docId, WizardConstants.Contact);
            if (CheckDocumentNotExists(model))
                return Redirect(GetWizardStartUrl());
            return View(model);
        }

        public ActionResult Experience(string mode, int? docId, int? paragraphId)
        {
            var model = CreateWizardModel(mode, docId, paragraphId, WizardConstants.Experience);
            if (CheckDocumentNotExists(model))
                return Redirect(GetWizardStartUrl());
            return View(model);
        }

        public ActionResult ExperienceReview(string mode, int? docId)
        {
            var model = CreateWizardModel(mode, docId, WizardConstants.Experience);
            if (CheckDocumentNotExists(model))
                return Redirect(GetWizardStartUrl());
            return View(model);
        }

        public ActionResult Education(string mode, int? docId, int? paragraphId)
        {
            var model = CreateWizardModel(mode, docId, paragraphId, WizardConstants.Education);
            if (CheckDocumentNotExists(model))
                return Redirect(GetWizardStartUrl());
            return View(model);
        }

        public ActionResult EducationReview(string mode, int? docId)
        {
            var model = CreateWizardModel(mode, docId, WizardConstants.Education);
            if (CheckDocumentNotExists(model))
                return Redirect(GetWizardStartUrl());
            return View(model);
        }

        public ActionResult SkillDescription(string mode, int? docId)
        {
            var model = CreateWizardModel(mode, docId, WizardConstants.Skills);
            if (CheckDocumentNotExists(model))
                return Redirect(GetWizardStartUrl());
            return View(model);
        }

        public ActionResult SummaryDescription(string mode, int? docId)
        {
            var model = CreateWizardModel(mode, docId, WizardConstants.Summary);
            if (CheckDocumentNotExists(model))
                return Redirect(GetWizardStartUrl());
            return View(model);
        }

        public ActionResult OtherSection(string mode, int? docId, string sectionName, int? sectionId)
        {
            var model = GetModelBySectionCode(sectionName);
            model.WizardViewModel = CreateWizardModel(mode, docId, model.Wizard);
            if (CheckDocumentNotExists(model.WizardViewModel))
                return Redirect(GetWizardStartUrl());
            return View(model);
        }

        #endregion

        #region Wizard Common Methods

        private WizardVM CreateWizardModel(string mode, int? docId)
        {
            return CreateWizardModel(mode, docId, null, null);
        }

        private WizardVM CreateWizardModel(string mode, int? docId, string wizard)
        {
            return CreateWizardModel(mode, docId, null, wizard);
        }

        private WizardVM CreateWizardModel(string mode, int? docId, int? paragraphId, string wizard)
        {
            var model = new WizardVM();
            //model.Wizard = wizard;
            model.Mode = mode;
            if (docId.HasValue)
                model.DocumentId = docId.Value;
            if (paragraphId.HasValue)
                model.ParagraphId = paragraphId.Value;
            model.DocumentCookieModel = ParseDocumentCookie();

            #region Verify docId in query string matches that with cookie value.

            if (model.DocumentId > 0)
            {
                if (model.DocumentCookieModel == null || !model.DocumentCookieModel.DocumentId.HasValue ||
                    model.DocumentId != model.DocumentCookieModel.DocumentId.Value)
                {
                    var documentService = new DocumentService();
                    var document = documentService.GetDocumentByID(model.DocumentId, UserId);
                    if (document != null)
                    {
                        model.DocumentCookieModel = DocumentCookieModel.Parse(document);
                        FlushDocumentCookie(model.DocumentCookieModel);
                    }
                    else
                    {
                        model.DocumentId = 0;
                        model.DocumentCookieModel = null;
                        Response.Cookies.Delete(CookieConstants.DocumentCookie);
                        return model;
                    }
                }
            }
            else if (model.DocumentCookieModel != null && model.DocumentCookieModel.DocumentId.HasValue)
            {
                model.DocumentId = model.DocumentCookieModel.DocumentId.Value;
            }

            #endregion

            #region Set LoadData

            if (!string.IsNullOrEmpty(wizard))
            {
                if (RouteMode.Edit.Equals(model.Mode, StringComparison.OrdinalIgnoreCase))
                    model.LoadData = true;
                else if (RouteReviewMode.EditDocEditPara.Equals(model.Mode, StringComparison.OrdinalIgnoreCase))
                    model.LoadData = true;
                else if (RouteReviewMode.AddDocEditPara.Equals(model.Mode, StringComparison.OrdinalIgnoreCase))
                    model.LoadData = true;
                else if (RouteReviewMode.CreateDocEditPara.Equals(model.Mode, StringComparison.OrdinalIgnoreCase))
                    model.LoadData = true;
                else if (model.DocumentCookieModel != null)
                    model.LoadData = model.DocumentCookieModel.HasSection(wizard);
            }

            #endregion

            WizardUtility.SetFunnelRoutes(Url, model, wizard);
            return model;
        }

        private bool CheckDocumentNotExists(WizardVM model)
        {
            return model == null || model.DocumentId < 1;
        }

        private string GetWizardStartUrl()
        {
            return Url.BuildUrl<WizardController>(wiz => wiz.HowItWorks(), RouteNames.HowItWorks);
        }

        private DocumentCookieModel ParseDocumentCookie()
        {
            var documentCookie = Request.Cookies.Read(CookieConstants.DocumentCookie);
            if (!string.IsNullOrEmpty(documentCookie))
                return DocumentCookieModel.Parse(documentCookie);
            return null;
        }

        private void FlushDocumentCookie(DocumentCookieModel documentCookieModel)
        {
            var documentCookie = DocumentCookieModel.ToJsonString(documentCookieModel);
            if (!string.IsNullOrEmpty(documentCookie))
                Response.Cookies.Write(CookieConstants.DocumentCookie, documentCookie);
        }

        private void DeleteDocumentCookie()
        {
            Response.Cookies.Delete(CookieConstants.DocumentCookie);
        }

        #endregion

        #region Ajax Calls

        [HttpPost]
        [Route(AjaxRoutes.Documents + "/" + AjaxRoutes.SaveTemplate, Name = AjaxRouteNames.SaveTemplate)]
        public ActionResult SaveTemplate(int? docId, string skinCD)
        {
            if (!string.IsNullOrEmpty(skinCD))
            {
                var docCookie = ParseDocumentCookie();
                if (docCookie == null)
                    docCookie = new DocumentCookieModel();
                if (docId.HasValue && docId.Value > 0)
                {
                    var docService = new DocumentService();
                    docService.UpdateSkin(docId.Value, skinCD);
                    docCookie.SkinCode = skinCD;
                }
                else
                {
                    var document = CreateDocument(skinCD);
                    docCookie.DocumentId = document.Id;
                    docCookie.SkinCode = document.SkinCD;
                }

                FlushDocumentCookie(docCookie);
                return ToJson(true);
            }

            return HttpCodeJsonResponse(HttpStatusCode.BadRequest);
        }

        private Document CreateDocument(string skinCD)
        {
            var docService = new DocumentService();
            var resumeCount = docService.GetResumeCountByUserID(UserId);
            var document = new Document();
            document.SkinCD = skinCD;
            document.UserID = UserId;
            document.TemplateCD = DocTemplateTypeCD.Default;
            document.DocumentTypeCD = DocumentTypeCD.Resume;
            document.DocStatusTypeCD = DocumentStatusTypeCD.Active;
            document.DateCreated = DateTime.Now;
            document.DateModified = DateTime.Now;
            document.Name = Resource.Resume + ++resumeCount;

            var saveDocument = docService.CreateDocument(document);
            return saveDocument;
        }

        [HttpGet]
        [Route(AjaxRoutes.Documents + "/{docId}/" + AjaxRoutes.ContactInfo, Name = AjaxRouteNames.GetContactInfo)]
        public ActionResult GetContactInfo(int docId)
        {
            if (docId > 0)
            {
                var docService = new DocumentService();
                var doc = docService.GetDocumentByID(docId, UserId);
                if (doc != null)
                {
                    var sectionService = new SectionService();
                    var nameSection = sectionService.GetSectionByDocAndTypeCD(doc, SectionTypeCD.Name);
                    var contactSection = sectionService.GetSectionByDocAndTypeCD(doc, SectionTypeCD.Contact);
                    var contactInfo = SectionConvertor.ToContactInfo(nameSection, contactSection);
                    return ToJson(contactInfo);
                }

                return HttpCodeJsonResponse(HttpStatusCode.Forbidden);
            }

            return HttpCodeJsonResponse(HttpStatusCode.BadRequest);
        }

        [HttpPost]
        [Route(AjaxRoutes.Documents + "/{docId}/" + AjaxRoutes.ContactInfo, Name = AjaxRouteNames.SaveContactInfo)]
        public ActionResult SaveContactInfo(int docId, ContactInfo contactInfo)
        {
            if (docId > 0 && contactInfo != null && contactInfo.Name != null && contactInfo.Contact != null &&
                ModelState.IsValid)
            {
                var docService = new DocumentService();
                var doc = docService.GetDocumentByID(docId, UserId);
                if (doc != null)
                {
                    var updateDoc = false;
                    var paraService = new ParagraphService();
                    var sectionService = new SectionService();
                    Paragraph namePara = null;
                    if (contactInfo.Name.ParagraphId > 0)
                        namePara = paraService.GetParagraph(contactInfo.Name.ParagraphId, UserId);
                    if (namePara != null)
                    {
                        if (string.IsNullOrEmpty(contactInfo.Name.Name))
                            SetDefaultNameSection(contactInfo);
                        SectionConvertor.PopulateName(ref namePara, contactInfo.Name);
                        paraService.UpdateParagraph(namePara);
                    }
                    else
                    {
                        Section nameSection = null;
                        if (contactInfo.Name.SectionId > 0)
                            nameSection = sectionService.GetSection(contactInfo.Name.SectionId, UserId);
                        else
                            nameSection = sectionService.GetSectionByDocAndTypeCD(doc, SectionTypeCD.Name);
                        if (nameSection == null)
                        {
                            nameSection = new Section(Resource.NameCaps);
                            nameSection.DocZoneTypeCD = DocZoneTypeCD.HEAD;
                            nameSection.SectionTypeCD = SectionTypeCD.Name;
                            nameSection.SortIndex = 1;
                            doc.AddSection(nameSection);
                        }
                        else if (nameSection.Paragraphs != null && nameSection.Paragraphs.Count > 0)
                        {
                            namePara = nameSection.Paragraphs.FirstOrDefault();
                        }

                        if (namePara == null)
                        {
                            namePara = new Paragraph();
                            namePara.SortIndex = 0;
                            nameSection.AddParagraph(namePara);
                        }

                        if (string.IsNullOrEmpty(contactInfo.Name.Name))
                            SetDefaultNameSection(contactInfo);
                        SectionConvertor.PopulateName(ref namePara, contactInfo.Name);
                        updateDoc = true;
                    }

                    Paragraph contactPara = null;
                    if (contactInfo.Contact.ParagraphId > 0)
                        contactPara = paraService.GetParagraph(contactInfo.Contact.ParagraphId, UserId);
                    if (contactPara != null)
                    {
                        SectionConvertor.PopulateContact(ref contactPara, contactInfo.Contact);
                        paraService.UpdateParagraph(contactPara);
                    }
                    else
                    {
                        Section contactSection = null;
                        if (contactInfo.Contact.SectionId > 0)
                            contactSection = sectionService.GetSection(contactInfo.Contact.SectionId, UserId);
                        else
                            contactSection = sectionService.GetSectionByDocAndTypeCD(doc, SectionTypeCD.Contact);
                        if (contactSection == null)
                        {
                            contactSection = new Section(Resource.CNTC);
                            contactSection.DocZoneTypeCD = DocZoneTypeCD.HEAD;
                            contactSection.SectionTypeCD = SectionTypeCD.Contact;
                            contactSection.SortIndex = 1;
                            doc.AddSection(contactSection);
                        }
                        else if (contactSection.Paragraphs != null && contactSection.Paragraphs.Count > 0)
                        {
                            contactPara = contactSection.Paragraphs.FirstOrDefault();
                        }

                        if (contactPara == null)
                        {
                            contactPara = new Paragraph();
                            contactPara.SortIndex = 0;
                            contactSection.AddParagraph(contactPara);
                        }

                        SectionConvertor.PopulateContact(ref contactPara, contactInfo.Contact);
                        updateDoc = true;
                    }

                    if (updateDoc)
                        docService.UpdateDocument(doc);
                    else
                        docService.UpdateDocumentDateModified(docId);
                    var docCookie = ParseDocumentCookie();
                    if (docCookie == null)
                        docCookie = new DocumentCookieModel();
                    docCookie.AddSection(WizardConstants.Contact);
                    FlushDocumentCookie(docCookie);
                    return ToJson(true);
                }

                return HttpCodeJsonResponse(HttpStatusCode.Forbidden);
            }

            return HttpCodeJsonResponse(HttpStatusCode.BadRequest);
        }

        [HttpGet]
        [Route(AjaxRoutes.Documents + "/{docId}/" + AjaxRoutes.Experience)]
        public ActionResult GetExperience(int docId)
        {
            if (docId > 0)
            {
                var docService = new DocumentService();
                var doc = docService.GetDocumentByID(docId, UserId);
                if (doc != null)
                {
                    var sectionService = new SectionService();
                    var expSection = sectionService.GetSectionByDocAndTypeCD(doc, SectionTypeCD.Experience);
                    if (expSection != null)
                    {
                        var experienceDetails = SectionConvertor.ToExperienceDetails(expSection);
                        return ToJson(experienceDetails);
                    }

                    return ToJson(null);
                }

                return HttpCodeJsonResponse(HttpStatusCode.Forbidden);
            }

            return HttpCodeJsonResponse(HttpStatusCode.BadRequest);
        }

        [HttpPost]
        [Route(AjaxRoutes.Documents + "/{docId}/" + AjaxRoutes.Experience)]
        public ActionResult SaveExperience(int docId, Experience experience)
        {
            if (docId > 0 && experience != null)
            {
                var docService = new DocumentService();
                var doc = docService.GetDocumentByID(docId, UserId);
                if (doc != null)
                {
                    ExperienceDetails savedExperience = null;
                    var sectionService = new SectionService();
                    var expSection = sectionService.GetSectionByDocAndTypeCD(doc, SectionTypeCD.Experience);
                    if (expSection == null)
                    {
                        /// Add section if not exists.
                        expSection = new Section(Resource.EXPR);
                        expSection.DocZoneTypeCD = DocZoneTypeCD.BODY;
                        expSection.SectionTypeCD = SectionTypeCD.Experience;
                        expSection.SortIndex = 4;
                        doc.AddSection(expSection);
                    }

                    Paragraph expPara = null;
                    if (!SectionConvertor.IsExperienceEmpty(experience))
                    {
                        var paraService = new ParagraphService();
                        if (experience.ParagraphId > 0 && expSection.Paragraphs != null)
                            expPara = expSection.Paragraphs.FirstOrDefault(para => para.Id == experience.ParagraphId);
                        if (expPara == null)
                        {
                            expPara = new Paragraph();
                            expPara.SortIndex = experience.ParaSortIndex;
                            expSection.AddParagraph(expPara);
                        }

                        SectionConvertor.PopulateExperience(ref expPara, experience);
                    }

                    docService.SortExprSection(expSection);
                    docService.UpdateDocument(doc);
                    savedExperience = SectionConvertor.ToExperienceDetails(expSection);

                    if (savedExperience != null)
                    {
                        var docCookie = ParseDocumentCookie();
                        if (docCookie == null)
                            docCookie = new DocumentCookieModel();
                        docCookie.AddSection(WizardConstants.Experience);
                        if (savedExperience.Paragraphs != null && savedExperience.Paragraphs.Any())
                            docCookie.AddSectionPara(WizardConstants.Experience);
                        FlushDocumentCookie(docCookie);
                        return ToJson(savedExperience);
                    }

                    return HttpCodeJsonResponse(HttpStatusCode.Gone);
                }

                return HttpCodeJsonResponse(HttpStatusCode.Forbidden);
            }

            return HttpCodeJsonResponse(HttpStatusCode.BadRequest);
        }


        [HttpPost]
        [Route(AjaxRoutes.Documents + "/{docId}/" + AjaxRoutes.Portfolio)]
        public ActionResult SavePortfolio(int docId, Portfolio portfolio)
        {
            if (docId > 0 && portfolio != null)
                //DocumentService docService = new DocumentService();
                //var doc = docService.GetDocumentByID(docId, UserId);
                //if (doc != null)
                //{
                //    SectionService sectionService = new SectionService();
                //    Section portfolioSection = sectionService.GetSectionByDocAndTypeCD(doc, SectionTypeCD.Portfolio);
                //    if (portfolioSection == null)
                //    {
                //        /// Add section if not exists.
                //        portfolioSection = new Section("Portfolio");
                //        portfolioSection.DocZoneTypeCD = DocZoneTypeCD.BODY;
                //        portfolioSection.SectionTypeCD = SectionTypeCD.Portfolio;
                //        portfolioSection.SortIndex = 12;
                //        doc.AddSection(portfolioSection);
                //    }

                //    docService.UpdateDocument(doc);
                //    savedExperience = SectionConvertor.ToPortfolioDetails(portfolioSection);

                //    if (savedExperience != null)
                //    {
                //        if (experience.SectionId <= 0 || experience.ParagraphId <= 0)
                //        {
                //            var docCookie = ParseDocumentCookie();
                //            if (docCookie == null)
                //                docCookie = new DocumentCookieModel();
                //            docCookie.AddSection(WizardConstants.Experience);
                //            if (savedExperience.Paragraphs != null && savedExperience.Paragraphs.Any())
                //                docCookie.AddSectionPara(WizardConstants.Experience);
                //            FlushDocumentCookie(docCookie);
                //        }
                //        return ToJson(savedExperience);
                //    }
                //    else
                //        return HttpCodeJsonResponse(System.Net.HttpStatusCode.Gone);
                //}
                //else
                return HttpCodeJsonResponse(HttpStatusCode.Forbidden);
            return HttpCodeJsonResponse(HttpStatusCode.BadRequest);
        }

        [HttpGet]
        [Route(AjaxRoutes.Documents + "/{docId}/" + AjaxRoutes.Education, Name = AjaxRouteNames.GetEducation)]
        public ActionResult GetEducation(int docId)
        {
            if (docId > 0)
            {
                var docService = new DocumentService();
                var doc = docService.GetDocumentByID(docId, UserId);
                if (doc != null)
                {
                    var sectionService = new SectionService();
                    var educSection = sectionService.GetSectionByDocAndTypeCD(doc, SectionTypeCD.Education);
                    if (educSection != null)
                    {
                        var educationDetails = SectionConvertor.ToEducationDetails(educSection);
                        return ToJson(educationDetails);
                    }

                    return ToJson(null);
                }

                return HttpCodeJsonResponse(HttpStatusCode.Forbidden);
            }

            return HttpCodeJsonResponse(HttpStatusCode.BadRequest);
        }

        [HttpPost]
        [Route(AjaxRoutes.Documents + "/{docId}/" + AjaxRoutes.Education, Name = AjaxRouteNames.SaveEducation)]
        public ActionResult SaveEducation(int docId, Education education)
        {
            if (docId > 0 && education != null)
            {
                var docService = new DocumentService();
                var doc = docService.GetDocumentByID(docId, UserId);
                if (doc != null)
                {
                    EducationDetails savedEducation = null;
                    var sectionService = new SectionService();
                    var educSection = sectionService.GetSectionByDocAndTypeCD(doc, SectionTypeCD.Education);
                    if (educSection == null)
                    {
                        /// Add section if not exists.
                        educSection = new Section(Resource.EDUC);
                        educSection.DocZoneTypeCD = DocZoneTypeCD.BODY;
                        educSection.SectionTypeCD = SectionTypeCD.Education;
                        educSection.SortIndex = 5;
                        doc.AddSection(educSection);
                    }

                    if (!SectionConvertor.IsEducationEmpty(education))
                    {
                        Paragraph educPara = null;
                        var paraService = new ParagraphService();
                        if (education.ParagraphId > 0 && educSection.Paragraphs != null)
                            educPara = educSection.Paragraphs.FirstOrDefault(para => para.Id == education.ParagraphId);
                        if (educPara == null)
                        {
                            educPara = new Paragraph();
                            educPara.SortIndex = education.ParaSortIndex;
                            educSection.AddParagraph(educPara);
                        }

                        SectionConvertor.PopulateEducation(ref educPara, education);
                    }

                    docService.SortEducationSection(educSection);
                    docService.UpdateDocument(doc);
                    savedEducation = SectionConvertor.ToEducationDetails(educSection);

                    if (savedEducation != null)
                    {
                        var docCookie = ParseDocumentCookie();
                        if (docCookie == null)
                            docCookie = new DocumentCookieModel();
                        docCookie.AddSection(WizardConstants.Education);
                        if (savedEducation.Paragraphs != null && savedEducation.Paragraphs.Any())
                            docCookie.AddSectionPara(WizardConstants.Education);
                        FlushDocumentCookie(docCookie);
                        return ToJson(savedEducation);
                    }

                    return HttpCodeJsonResponse(HttpStatusCode.Gone);
                }

                return HttpCodeJsonResponse(HttpStatusCode.Forbidden);
            }

            return HttpCodeJsonResponse(HttpStatusCode.BadRequest);
        }

        [HttpGet]
        [Route(AjaxRoutes.Documents + "/{docId}/" + AjaxRoutes.SkillDescription, Name = AjaxRouteNames.GetSkill)]
        public ActionResult GetSkill(int docId)
        {
            if (docId > 0)
            {
                var docService = new DocumentService();
                var doc = docService.GetDocumentByID(docId, UserId);
                if (doc != null)
                {
                    var sectionService = new SectionService();
                    var skillSection = sectionService.GetSectionByDocAndTypeCD(doc, SectionTypeCD.Skills);
                    if (skillSection != null)
                    {
                        var skill = SectionConvertor.ToSkills(skillSection);
                        return ToJson(skill);
                    }

                    return ToJson(null);
                }

                return HttpCodeJsonResponse(HttpStatusCode.Forbidden);
            }

            return HttpCodeJsonResponse(HttpStatusCode.BadRequest);
        }

        [HttpPost]
        [Route(AjaxRoutes.Documents + "/{docId}/" + AjaxRoutes.SkillDescription, Name = AjaxRouteNames.SaveSkill)]
        public ActionResult SaveSkill(int docId, SkillDescription skills)
        {
            if (docId > 0 && skills != null)
            {
                var docService = new DocumentService();
                var doc = docService.GetDocumentByID(docId, UserId);
                if (doc != null)
                {
                    SkillDescription savedSkills = null;
                    var sectionService = new SectionService();
                    var skillSection = sectionService.GetSectionByDocAndTypeCD(doc, SectionTypeCD.Skills);
                    if (skillSection == null)
                    {
                        /// Add section if not exists.
                        skillSection = new Section(Resource.HILT);
                        skillSection.DocZoneTypeCD = DocZoneTypeCD.BODY;
                        skillSection.SectionTypeCD = SectionTypeCD.Skills;
                        skillSection.SortIndex = 3;
                        doc.AddSection(skillSection);
                    }

                    Paragraph skillPara = null;
                    var paraService = new ParagraphService();
                    if (skills.ParagraphId > 0)
                        skillPara = paraService.GetParagraph(skills.ParagraphId, UserId);
                    else
                        skillPara = skillSection.Paragraphs.FirstOrDefault();
                    if (skillPara == null)
                    {
                        skillPara = new Paragraph();
                        skillPara.SortIndex = skills.ParaSortIndex;
                        skillSection.AddParagraph(skillPara);
                    }

                    SectionConvertor.PopulateSkills(ref skillPara, skills);
                    docService.UpdateDocument(doc);
                    savedSkills = SectionConvertor.ToSkills(skillSection);

                    if (savedSkills != null)
                    {
                        var docCookie = ParseDocumentCookie();
                        if (docCookie == null)
                            docCookie = new DocumentCookieModel();
                        docCookie.AddSection(WizardConstants.Skills);
                        FlushDocumentCookie(docCookie);
                        return ToJson(savedSkills);
                    }

                    return HttpCodeJsonResponse(HttpStatusCode.Gone);
                }

                return HttpCodeJsonResponse(HttpStatusCode.Forbidden);
            }

            return HttpCodeJsonResponse(HttpStatusCode.BadRequest);
        }

        [HttpGet]
        [Route(AjaxRoutes.Documents + "/{docId}/" + AjaxRoutes.SummaryDescription, Name = AjaxRouteNames.GetSummary)]
        public ActionResult GetSummary(int docId)
        {
            if (docId > 0)
            {
                var docService = new DocumentService();
                var doc = docService.GetDocumentByID(docId, UserId);
                if (doc != null)
                {
                    var sectionService = new SectionService();
                    var summarySection = sectionService.GetSectionByDocAndTypeCD(doc, SectionTypeCD.Summary);
                    if (summarySection != null)
                    {
                        var summary = SectionConvertor.ToSummary(summarySection);
                        return ToJson(summary);
                    }

                    return ToJson(null);
                }

                return HttpCodeJsonResponse(HttpStatusCode.Forbidden);
            }

            return HttpCodeJsonResponse(HttpStatusCode.BadRequest);
        }

        [HttpPost]
        [Route(AjaxRoutes.Documents + "/{docId}/" + AjaxRoutes.SummaryDescription, Name = AjaxRouteNames.SaveSummary)]
        public ActionResult SaveSummary(int docId, SummaryDescription summary)
        {
            if (docId > 0 && summary != null)
            {
                var docService = new DocumentService();
                var doc = docService.GetDocumentByID(docId, UserId);
                if (doc != null)
                {
                    SummaryDescription savedSummary = null;
                    var sectionService = new SectionService();
                    var summarySection = sectionService.GetSectionByDocAndTypeCD(doc, SectionTypeCD.Summary);
                    if (summarySection == null)
                    {
                        /// Add section if not exists.
                        summarySection = new Section(Resource.ProfessionalSummary);
                        summarySection.DocZoneTypeCD = DocZoneTypeCD.BODY;
                        summarySection.SectionTypeCD = SectionTypeCD.Summary;
                        summarySection.SortIndex = 2;
                        doc.AddSection(summarySection);
                    }

                    Paragraph summaryPara = null;
                    var paraService = new ParagraphService();
                    if (summary.ParagraphId > 0)
                        summaryPara = paraService.GetParagraph(summary.ParagraphId, UserId);
                    else
                        summaryPara = summarySection.Paragraphs.FirstOrDefault();
                    if (summaryPara == null)
                    {
                        summaryPara = new Paragraph();
                        summaryPara.SortIndex = summary.ParaSortIndex;
                        summarySection.AddParagraph(summaryPara);
                    }

                    SectionConvertor.PopulateSummary(ref summaryPara, summary);
                    docService.UpdateDocument(doc);
                    savedSummary = SectionConvertor.ToSummary(summarySection);

                    if (savedSummary != null)
                    {
                        var docCookie = ParseDocumentCookie();
                        if (docCookie == null)
                            docCookie = new DocumentCookieModel();
                        docCookie.AddSection(WizardConstants.Summary);
                        FlushDocumentCookie(docCookie);
                        return ToJson(savedSummary);
                    }

                    return HttpCodeJsonResponse(HttpStatusCode.Gone);
                }

                return HttpCodeJsonResponse(HttpStatusCode.Forbidden);
            }

            return HttpCodeJsonResponse(HttpStatusCode.BadRequest);
        }

        [HttpGet]
        [Route(AjaxRoutes.Documents + "/" + AjaxRoutes.OtherSection, Name = AjaxRouteNames.GetOtherSection)]
        public ActionResult GetOtherSection(int docId, string wizard, int? sectionId)
        {
            if (docId > 0 && !string.IsNullOrEmpty(wizard))
            {
                var docService = new DocumentService();
                var doc = docService.GetDocumentByID(docId, UserId);
                if (doc != null)
                {
                    var sectionService = new SectionService();
                    Section otherSection = null;
                    if (sectionId.HasValue && sectionId > 0)
                    {
                        otherSection = sectionService.GetSection(sectionId.Value, UserId);
                    }
                    else
                    {
                        var sectionTypeCD = WizardUtility.GetSectionCDFromWizard(wizard);
                        if (!string.IsNullOrEmpty(sectionTypeCD))
                            otherSection = sectionService.GetSectionByDocAndTypeCD(doc, sectionTypeCD);
                    }

                    var otherData = SectionConvertor.ToOtherSection(otherSection, wizard);
                    return ToJson(otherData);
                }

                return HttpCodeJsonResponse(HttpStatusCode.Forbidden);
            }

            return HttpCodeJsonResponse(HttpStatusCode.BadRequest);
        }

        [HttpPost]
        [Route(AjaxRoutes.Documents + "/" + AjaxRoutes.OtherSection, Name = AjaxRouteNames.SaveOtherSection)]
        public ActionResult SaveOtherSection(int docId, OtherSection desc)
        {
            if (docId > 0 && desc != null && !string.IsNullOrEmpty(desc.Wizard))
            {
                var sectionTypeCD = WizardUtility.GetSectionCDFromWizard(desc.Wizard);
                if (!string.IsNullOrEmpty(sectionTypeCD))
                {
                    var docService = new DocumentService();
                    var doc = docService.GetDocumentByID(docId, UserId);
                    if (doc != null)
                    {
                        var sectionService = new SectionService();
                        var paraService = new ParagraphService();
                        Paragraph para = null;
                        Section section = null;
                        /// ParagraphId is present. Update call.
                        if (desc.ParagraphId > 0)
                        {
                            para = paraService.GetParagraph(desc.ParagraphId, UserId);
                        }
                        else
                        {
                            if (desc.SectionId > 0)
                                section = sectionService.GetSection(desc.SectionId, UserId);
                            if (section == null)
                                section = sectionService.GetSectionByDocAndTypeCD(doc, sectionTypeCD);

                            if (section != null && section.Paragraphs != null)
                                para = section.Paragraphs.FirstOrDefault();
                        }

                        if (para != null)
                        {
                            SectionConvertor.PopulateOtherSectionDescription(ref para, desc);
                            paraService.UpdateParagraph(para);
                            section = para.Section;
                        }
                        else if (section != null && para == null)
                        {
                            para = new Paragraph();
                            para.SortIndex = 0;
                            SectionConvertor.PopulateOtherSectionDescription(ref para, desc);
                            section.AddParagraph(para);
                            sectionService.UpdateSection(section);
                        }
                        else
                        {
                            /// Add Paragraph,Section to document call.
                            para = new Paragraph();
                            para.SortIndex = 0;
                            SectionConvertor.PopulateOtherSectionDescription(ref para, desc);
                            var label = desc.SectionLabel != null
                                ? desc.SectionLabel
                                : sectionService.GetLabelBySectionType(sectionTypeCD);
                            section = new Section(label);
                            section.DocZoneTypeCD = DocZoneTypeCD.BODY;
                            section.SectionTypeCD = sectionTypeCD;
                            section.SortIndex = sectionService.GetSectionSortIndexByCode(sectionTypeCD, 5);
                            section.AddParagraph(para);
                            doc.AddSection(section);
                            sectionService.AddSection(section);
                        }

                        if (section != null)
                        {
                            docService.UpdateDocumentDateModified(docId);
                            var docCookie = ParseDocumentCookie();
                            if (docCookie == null)
                                docCookie = new DocumentCookieModel();
                            docCookie.AddSection(desc.Wizard);
                            FlushDocumentCookie(docCookie);
                            var otherSectionModel = SectionConvertor.ToOtherSection(section, desc.Wizard);
                            return ToJson(otherSectionModel);
                        }

                        return HttpCodeJsonResponse(HttpStatusCode.Gone);
                    }

                    return HttpCodeJsonResponse(HttpStatusCode.Forbidden);
                }

                return HttpCodeJsonResponse(HttpStatusCode.BadRequest);
            }

            return HttpCodeJsonResponse(HttpStatusCode.BadRequest);
        }

        [HttpPost]
        [Route(AjaxRoutes.Paragraphs + "/" + AjaxRoutes.Delete, Name = AjaxRouteNames.DeleteParagraph)]
        public ActionResult DeleteParagraph(int? id)
        {
            if (id.HasValue && id.Value > 0)
            {
                var paraService = new ParagraphService();
                var paragraph = paraService.GetParagraph(id.Value, UserId);
                if (paragraph != null && paragraph.Section != null && paragraph.Section.Document != null)
                {
                    var sectionTypeCD = paragraph.Section.SectionTypeCD;
                    var documentId = paragraph.Section.Document.Id;
                    bool isSectionDeleted, isDocumentDeleted;
                    paraService.DeleteParagraph(paragraph, out isSectionDeleted, out isDocumentDeleted);
                    if (isDocumentDeleted)
                    {
                        DeleteDocumentCookie();
                    }
                    else if (isSectionDeleted)
                    {
                        var documentCookieModel = ParseDocumentCookie();
                        if (documentCookieModel != null)
                        {
                            var wizard = WizardUtility.GetWizardFromSectionCD(sectionTypeCD);
                            documentCookieModel.RemoveSection(wizard);
                            documentCookieModel.RemoveSectionPara(wizard);
                            FlushDocumentCookie(documentCookieModel);
                        }
                    }

                    return ToJson(new {success = true, isGroupDeleted = isSectionDeleted, isDocumentDeleted});
                }

                return HttpCodeJsonResponse(HttpStatusCode.Forbidden);
            }

            return HttpCodeJsonResponse(HttpStatusCode.BadRequest);
        }

        [HttpPost]
        [Route(AjaxRoutes.Sections + "/{sectionId}/" + AjaxRoutes.Paragraphs + "/" + AjaxRoutes.Sort, Name = AjaxRouteNames.SortParagraphs)]
        public ActionResult SortParagraphs(int sectionId, SortedParagraph[] sortedItems)
        {
            if (sectionId > 0 && sortedItems != null && sortedItems.Length > 0)
            {
                var sectionService = new SectionService();
                var section = sectionService.GetSection(sectionId, UserId);
                if (section != null)
                {
                    if (section.Paragraphs != null && section.Paragraphs.Count > 0)
                    {
                        var doSave = false;
                        foreach (var sortedItem in sortedItems)
                        {
                            var paragraph = section.Paragraphs.FirstOrDefault(sec => sec.Id == sortedItem.ParagraphId);
                            if (paragraph != null && paragraph.SortIndex != sortedItem.SortIndex)
                            {
                                paragraph.SortIndex = sortedItem.SortIndex;
                                doSave = true;
                            }
                        }

                        if (doSave)
                        {
                            sectionService.UpdateSection(section);
                            var docService = new DocumentService();
                            docService.UpdateDocument(section.Document);
                            if (SectionTypeCD.Experience.Equals(section.SectionTypeCD,
                                StringComparison.OrdinalIgnoreCase))
                                return ToJson(SectionConvertor.ToExperienceDetails(section));
                            if (SectionTypeCD.Education.Equals(section.SectionTypeCD,
                                StringComparison.OrdinalIgnoreCase))
                                return ToJson(SectionConvertor.ToEducationDetails(section));
                        }
                    }

                    return ToJson(null);
                }

                return HttpCodeJsonResponse(HttpStatusCode.Forbidden);
            }

            return HttpCodeJsonResponse(HttpStatusCode.BadRequest);
        }

        #endregion

        #region Private Methods/Properties

        private int CultureID
        {
            get
            {
                int cultureID;
                int.TryParse(ConfigManager.GetConfig("DomainCultureID"), out cultureID);
                return cultureID;
            }
        }

        private string UpdateUserDetails(int documentId, UserMaster user, ContactInfo contactInfo)
        {
            var phoneNumber = string.Empty;
            string firstName = null, lastName = null;
            var docService = new DocumentService();
            var userService = new UserService();
            Document document = null;
            if (contactInfo != null && contactInfo.Name != null)
            {
                SectionConvertor.GetName(contactInfo.Name.Name, ref firstName, ref lastName);
            }
            else
            {
                document = docService.GetDocumentByID(documentId, user.Id);
                if (document != null)
                {
                    var userContactDetails = GetUserContactDetails(document, user.Id);
                    firstName = userContactDetails.FirstName;
                    lastName = userContactDetails.LastName;
                }
            }

            if (string.IsNullOrEmpty(firstName) || CompareNameDefaults(FieldTypeCD.FirstName, firstName.Trim()))
                user.FirstName = string.Empty;
            else
                user.FirstName = firstName;

            if (string.IsNullOrEmpty(lastName) || CompareNameDefaults(FieldTypeCD.LastName, lastName.Trim()))
                user.LastName = string.Empty;
            else
                user.LastName = lastName;
            userService.UpdateUser(user);

            // now check to fetch address
            Address userAddress = null;
            if (contactInfo != null && contactInfo.Contact != null)
            {
                userAddress = new Address();
                userAddress.UserID = user.Id;
                userAddress.CountryCD = "US";
                userAddress.City = contactInfo.Contact.City;
                userAddress.Address1 = contactInfo.Contact.Address;
                userAddress.PostalCode = contactInfo.Contact.ZipCode;
                userAddress.StateProvince = contactInfo.Contact.State;
                userAddress.PhoneNumber = contactInfo.Contact.Phone;
            }
            else
            {
                if (document == null) document = docService.GetDocumentByID(documentId, user.Id);
                if (document != null) userAddress = GetUserAddress(document, user.Id);
            }

            if (userAddress != null)
            {
                phoneNumber = userAddress.PhoneNumber;
                userService.SaveUserAddress(userAddress);
            }

            return phoneNumber;
        }

        private UserContactDetails GetUserContactDetails(Document document, int userid)
        {
            UserContactDetails userContactDetails = null;
            if (document != null && document.Sections != null)
            {
                var sectionService = new SectionService();
                var nameSection = sectionService.GetSectionByDocAndTypeCD(document, SectionTypeCD.Name);
                if (nameSection != null && nameSection.Paragraphs != null)
                {
                    var nameParagraph = nameSection.Paragraphs.FirstOrDefault(para => para.SortIndex == 0);
                    if (nameParagraph != null && nameParagraph.DocData != null && nameParagraph.DocData.Count > 0)
                    {
                        userContactDetails = new UserContactDetails();
                        foreach (var dd in nameParagraph.DocData)
                            switch (dd.FieldCD)
                            {
                                case FieldTypeCD.FirstName:
                                    userContactDetails.FirstName = dd.CharValue;
                                    break;
                                case FieldTypeCD.LastName:
                                    userContactDetails.LastName = dd.CharValue;
                                    break;
                            }
                    }
                }
            }

            return userContactDetails;
        }

        private Address GetUserAddress(Document document, int userId)
        {
            Address userAddress = null;
            if (document != null && document.Sections != null)
            {
                var sectionService = new SectionService();
                var contactSection = sectionService.GetSectionByDocAndTypeCD(document, SectionTypeCD.Contact);
                if (contactSection != null && contactSection.Paragraphs != null)
                {
                    var contactParagraph = contactSection.Paragraphs.FirstOrDefault(para => para.SortIndex == 0);
                    if (contactParagraph != null && contactParagraph.DocData != null &&
                        contactParagraph.DocData.Count > 0)
                    {
                        userAddress = new Address();
                        userAddress.UserID = userId;
                        userAddress.CountryCD = "US";
                        foreach (var dd in contactParagraph.DocData)
                            switch (dd.FieldCD)
                            {
                                case FieldTypeCD.Street:
                                    userAddress.Address1 = dd.CharValue;
                                    break;
                                case FieldTypeCD.City:
                                    userAddress.City = dd.CharValue;
                                    break;
                                case FieldTypeCD.State:
                                    userAddress.StateProvince = dd.CharValue;
                                    break;
                                case FieldTypeCD.ZipCode:
                                    userAddress.PostalCode = dd.CharValue;
                                    break;
                                case FieldTypeCD.HomePhone:
                                    userAddress.PhoneNumber = dd.CharValue;
                                    break;
                            }
                    }
                }
            }

            return userAddress;
        }

        private bool CompareNameDefaults(string fieldType, string fieldValue)
        {
            if (fieldType == FieldTypeCD.FirstName && (fieldValue.ToUpper() == Resource.ContactCaps ||
                                                       fieldValue.ToUpper() == Resource.FirstNameCaps))
                return true;
            if (fieldType == FieldTypeCD.LastName && (fieldValue.ToUpper() == Resource.InformationCaps ||
                                                      fieldValue.ToUpper() == Resource.LastNameCap))
                return true;
            return false;
        }

        private OtherSectionModel GetModelBySectionCode(string sectionName)
        {
            var model = new OtherSectionModel();
            model.Examples = new List<string>();
            var examples = new List<string>();
            if (OtherSectionUrls.Language.Equals(sectionName, StringComparison.OrdinalIgnoreCase))
            {
                model.Title = Resource.AddLanguages;
                model.Wizard = WizardConstants.Language;
                model.Examples = new List<string>
                {
                    Resource.LanguageExamples1, Resource.LanguageExamples2, Resource.LanguageExamples3,
                    Resource.LanguageExamples4, Resource.LanguageExamples5
                };
            }
            else if (OtherSectionUrls.Affiliations.Equals(sectionName, StringComparison.OrdinalIgnoreCase))
            {
                model.Title = Resource.DescProfessionalAffiliations;
                model.Wizard = WizardConstants.Affiliations;
                model.Examples = new List<string>
                {
                    Resource.AffiliationExamples1, Resource.AffiliationExamples2, Resource.AffiliationExamples3,
                    Resource.AffiliationExamples4,
                    Resource.AffiliationExamples5, Resource.AffiliationExamples6, Resource.AffiliationExamples7,
                    Resource.AffiliationExamples8
                };
            }
            else if (OtherSectionUrls.Awards.Equals(sectionName, StringComparison.OrdinalIgnoreCase))
            {
                model.Title = Resource.DescAwards;
                model.Wizard = WizardConstants.Awards;
                model.Examples = new List<string>
                {
                    Resource.AwardExamples1, Resource.AwardExamples2, Resource.AwardExamples3, Resource.AwardExamples4,
                    Resource.AwardExamples5, Resource.AwardExamples6, Resource.AwardExamples7, Resource.AwardExamples8
                };
            }
            else if (OtherSectionUrls.Accomplishments.Equals(sectionName, StringComparison.OrdinalIgnoreCase))
            {
                model.Title = Resource.DescAccomplishments;
                model.Wizard = WizardConstants.Accomplishments;
            }
            else if (OtherSectionUrls.Certifications.Equals(sectionName, StringComparison.OrdinalIgnoreCase))
            {
                model.Title = Resource.CERT;
                model.Wizard = WizardConstants.Certifications;
            }
            else if (OtherSectionUrls.ExtraCurricular.Equals(sectionName, StringComparison.OrdinalIgnoreCase))
            {
                model.Title = Resource.EXCL;
                model.Wizard = WizardConstants.ExtraCurricular;
            }
            else if (OtherSectionUrls.PersonalInformation.Equals(sectionName, StringComparison.OrdinalIgnoreCase))
            {
                model.Title = Resource.PRIN;
                model.Wizard = WizardConstants.PersonalInformation;
            }
            else if (OtherSectionUrls.References.Equals(sectionName, StringComparison.OrdinalIgnoreCase))
            {
                model.Title = Resource.REFE;
                model.Wizard = WizardConstants.References;
            }
            else if (OtherSectionUrls.SocialService.Equals(sectionName, StringComparison.OrdinalIgnoreCase))
            {
                model.Title = Resource.SCSV;
                model.Wizard = WizardConstants.SocialService;
            }
            else if (OtherSectionUrls.Other.Equals(sectionName, StringComparison.OrdinalIgnoreCase))
            {
                model.Title = Resource.Other;
            }

            return model;
        }

        private void SetDefaultNameSection(ContactInfo contactInfo)
        {
            if (string.IsNullOrEmpty(contactInfo.Contact.State)
                && string.IsNullOrEmpty(contactInfo.Contact.City)
                && string.IsNullOrEmpty(contactInfo.Contact.ZipCode)
                && string.IsNullOrEmpty(contactInfo.Contact.Email)
                && string.IsNullOrEmpty(contactInfo.Contact.Phone)
                && string.IsNullOrEmpty(contactInfo.Contact.Address)
                && string.IsNullOrEmpty(contactInfo.Contact.City))
                contactInfo.Name.Name = Resource.ContactInfoCaps;
            else
                contactInfo.Name.Name = Resource.FirstNameLastNameCaps;
        }

        #endregion
    }
}