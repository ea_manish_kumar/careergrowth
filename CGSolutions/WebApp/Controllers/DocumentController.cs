﻿using App.DomainModel;
using App.Model;
using App.Model.Documents;
using App.Services;
using App.Utility;
using App.Utility.Constants;
using Resources;
using System;
using System.Linq;
using System.Net;
using System.Threading.Tasks;
using System.Web.Mvc;
using WebApp.Constants;
using WebApp.Extensions;

namespace WebApp.Controllers
{
    [Authorize]
    [RoutePrefix("documents")]
    public class DocumentController : BaseFunnelController
    {
        public DocumentController(IDocumentService documentService) : base(documentService)
        {
        }

        [HttpPost]
        [Route("save/{docType}", Name = AjaxRouteNames.SaveDocument)]
        public async Task<ActionResult> SaveDocument(string docType, string skinCD)
        {
            string[] validDocTypes = { DocumentTypes.Resume, DocumentTypes.Letter };
            if (Strings.IsNullOrEmpty(skinCD) || !validDocTypes.Any(d => d.Equals(docType, StringComparison.OrdinalIgnoreCase)))
                return HttpCodeJsonResponse(HttpStatusCode.BadRequest);

            var docCookie = ParseDocumentCookieDTO();
            if (docCookie != null && docCookie.DocumentId <= 0)
                return HttpCodeJsonResponse(HttpStatusCode.BadRequest);

            var resumeCount = await DocumentService.GetDocumentCount(DocumentTypes.Resume);
            var document = new Document
            {
                SkinCD = skinCD,
                DocTypeCD = docType,
                DocName = Resource.Resume + ++resumeCount
            };

            await DocumentService.SaveDocument(document);
            var sectionTypes = await DocumentService.GetSectionTypes();
            docCookie = DocumentCookieDTO.Parse(document, sectionTypes);
            FlushDocumentCookie(docCookie);
            return ToJson(true);
        }

        [HttpPost]
        [Route("update/{docId}", Name = AjaxRouteNames.UpdateDocument)]
        public async Task<ActionResult> UpdateDocument(int docId, string skinCD)
        {
            if (!string.IsNullOrEmpty(skinCD) && docId > 0)
            {
                var docCookie = ParseDocumentCookieDTO();
                if (docCookie == null)
                    docCookie = new DocumentCookieDTO();
                var result = await DocumentService.UpdateSkin(docId, skinCD);
                if (result == ResponseCode.Ok)
                {
                    docCookie.DocumentId = docId;
                    docCookie.SkinCode = skinCD;

                    FlushDocumentCookie(docCookie);
                    return ToJson(true);
                }
                else
                {
                    Response.Cookies.Delete(SiteCookies.DocumentCookie);
                    return HttpCodeJsonResponse(result);
                }
            }

            return HttpCodeJsonResponse(HttpStatusCode.BadRequest);
        }

        [HttpGet]
        [Route("{documentId}/sections/{sectionType}", Name = AjaxRouteNames.GetSection)]
        public async Task<ActionResult> GetSection(int documentId, string sectionType)
        {
            if (documentId < 0 || Strings.IsNullOrEmpty(sectionType))
                return HttpCodeJsonResponse(HttpStatusCode.BadRequest);

            var section = await DocumentService.GetSection(documentId, sectionType);
            InitializeRoutes(section);
            if (section != null)
                return ToJson(section);
            return HttpCodeJsonResponse(HttpStatusCode.NotFound);
        }

        [HttpPost]
        [Route("{documentId}/sections/save", Name = AjaxRouteNames.SaveSection)]
        public async Task<ActionResult> SaveSection(int documentId, SectionDTO section)
        {
            if (section == null || documentId < 0)
                return HttpCodeJsonResponse(HttpStatusCode.BadRequest);

            section.DocumentId = documentId;
            var response = await DocumentService.SaveSection(section);
            if (response != null)
            {
                if (response.IsSuccess)
                {
                    var savedSection = response.Data;
                    InitializeRoutes(savedSection);
                    AddSectionToDocumentCookie(savedSection);
                    return ToJson(savedSection);
                }
                else
                    return HttpCodeJsonResponse(response.ResponseCode);
            }
            return HttpCodeJsonResponse(HttpStatusCode.BadRequest);
        }

        [HttpPost]
        [Route("{documentId}/sections/sort-subsections", Name = AjaxRouteNames.SortSubSections)]
        public async Task<ActionResult> SortSubSections(int documentId, SectionDTO section)
        {
            if (section == null || documentId < 0)
                return HttpCodeJsonResponse(HttpStatusCode.BadRequest);

            section.DocumentId = documentId;
            var response = await DocumentService.SortSubSections(section);
            if (response != null)
            {
                if (response.IsSuccess)
                    return ToJson(response.Data);
                else
                    return HttpCodeJsonResponse(response.ResponseCode);
            }
            return HttpCodeJsonResponse(HttpStatusCode.BadRequest);
        }
    }
}