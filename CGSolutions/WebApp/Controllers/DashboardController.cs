﻿using App.DomainModel;
using Services.Documents;
using System.Linq;
using System.Web.Mvc;
using Utilities.Constants.Documents;
using WebApp.Constants;
using WebApp.Models;

namespace WebApp.Controllers
{
    [Authorize]
    public class DashboardController : BaseController
    {
        private readonly App.Services.IUserService UserService;

        public DashboardController()
        {
            UserService = DependencyResolver.Current.GetService<App.Services.IUserService>();
        }
        
        // GET: Dashboard

        public ActionResult HomeHtml()
        {
            return View();
        }

        public ActionResult HomeBackUp()
        {
            var homePageModel = new Dashboard();
            var docService = new DocumentService();
            var userDocs =
                docService.GetDocumentsByUserID(UserId,
                    DocumentTypeCD.Resume); /// 2nd args will be empty when letter integrated.
            if (userDocs != null && userDocs.Count > 0)
            {
                homePageModel.Documents = userDocs.OrderByDescending(doc => doc.DateModified).Select(doc =>
                    new DocumentDetails {Id = doc.Id, DocumentType = doc.DocumentTypeCD});
                homePageModel.SelectedDocId = homePageModel.Documents.FirstOrDefault().Id;
            }

            var user = UserService.Get(UserId);
            var userLevelDetails = new UserLevelDetails(); //TODO: Payment_TODO:
            homePageModel.IsPremiumUser = userLevelDetails != null && userLevelDetails.IsPremiumUser;
            if (homePageModel.IsPremiumUser)
            {
                homePageModel.Name = user.Result.Name;
                ViewBag.UserAction = Request.QueryString[RouteParameters.Action];
                ViewBag.DocType = Request.QueryString[RouteParameters.DocType];
            }

            return View(homePageModel);
        }


        public ActionResult Home()
        {
            var viewModel = new DashboardVM();
            var docService = new DocumentService();
            var documents = docService.GetDocumentsByUserID(UserId);
            if (documents != null && documents.Count > 0)
                viewModel.Documents = documents.OrderByDescending(doc => doc.DateModified).Select(d =>
                    new DocumentDetails
                    {
                        Id = d.Id,
                        Text = d.Name,
                        DateCreated = d.DateCreated.ToString(),
                        DateModified = d.DateModified.ToString(),
                        TemplateSelected = GetSelectedSkin(d),
                        Name = d.Name
                    });
            //if (!String.IsNullOrEmpty(Request.Cookies.Read(CookieConstants.DocumentCookie)))
            //    Response.Cookies.Delete(CookieConstants.DocumentCookie);
            return View(viewModel);
        }

        private string GetSelectedSkin(DomainModel.Document doc)
        {
            var skinDescription = string.Empty;
            DomainModel.Skin selectedSkin;
            if (DocumentService.SkinMapping.TryGetValue(doc.SkinCD, out selectedSkin))
                skinDescription = selectedSkin.Description;
            return skinDescription;
        }
    }
}