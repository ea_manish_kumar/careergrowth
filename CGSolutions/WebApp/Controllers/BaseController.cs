﻿using App.Model;
using Newtonsoft.Json;
using System;
using System.IO;
using System.Net;
using System.Web.Mvc;
using WebApp.Extensions;

namespace WebApp.Controllers
{
    public class BaseController : Controller
    {
        protected int UserId { get { return User.GetId(); } }

        public string RenderRazorViewToString(string viewName, object model)
        {
            using (var sw = new StringWriter())
            {
                ViewData.Model = model;
                var viewResult = ViewEngines.Engines.FindPartialView(ControllerContext, viewName);
                var viewContext = new ViewContext(ControllerContext, viewResult.View, ViewData, TempData, sw);
                viewResult.View.Render(viewContext, sw);
                viewResult.ViewEngine.ReleaseView(ControllerContext, viewResult.View);
                return sw.GetStringBuilder().ToString().Trim();
            }
        }

        #region ToJson() to handle Json Response effectively with Json.Net.

        protected ContentResult HttpCodeJsonResponse(HttpStatusCode statusCode)
        {
            Response.StatusCode = Convert.ToInt32(statusCode);
            return Content(null, "application/json");
        }

        protected ContentResult HttpCodeJsonResponse(ResponseCode responseCode)
        {
            Response.StatusCode = Convert.ToInt32(responseCode);
            return Content(null, "application/json");
        }

        protected ContentResult ToJson(object data)
        {
            return Content(JsonConvert.SerializeObject(data), "application/json");
        }

        #endregion
    }
}