﻿using App.Services;
using App.Utility.Constants;
using System.Collections.Generic;
using System.Threading.Tasks;
using System.Web.Mvc;
using WebApp.Constants;

namespace WebApp.Controllers
{
    public class ContentController : BaseController
    {
        private readonly IContentService ContentService;

        public ContentController()
        {
            ContentService = DependencyResolver.Current.GetService<IContentService>();
        }

        [HttpGet]
        [Route(AjaxRoutes.Documents + "/" + AjaxRoutes.EducationExamples, Name = AjaxRouteNames.GetEducationExamples)]
        public async Task<ActionResult> GetEducationExamples()
        {
            IList<string> examples = await ContentService.GetGenericExamples(ContentType.Degree);
            return ToJson(new { examples });
        }

        [HttpGet]
        [Route(AjaxRoutes.Documents + "/" + AjaxRoutes.JobTitles, Name = AjaxRouteNames.GetJobTitles)]
        public async Task<ActionResult> GetJobTitles(string query)
        {
            if (!string.IsNullOrWhiteSpace(query))
                query = string.Format("%{0}%", query.Trim());
            var jobTitles = await ContentService.GetJobTitles(query);
            return ToJson(jobTitles);
        }


        [HttpGet]
        [Route(AjaxRoutes.Documents + "/" + AjaxRoutes.Examples, Name = AjaxRouteNames.GetExamples)]
        public async Task<ActionResult> GetExamples(string jobTitle, ContentType contentType, bool fromSearch)
        {
            var examples = await ContentService.GetExamples(contentType, jobTitle);
            return ToJson(new { fromSearch, examples });
        }
    }
}