﻿using CommonModules.Configuration;
using CommonModules.Exceptions;
using Services.Communication;
using System;
using System.Collections;
using System.Collections.Specialized;
using System.Net;
using System.Reflection;
using System.Text;
using System.Web.Mvc;
using Utilities;
using Utilities.Communication;
using Utilities.Constants;
using WebApp.Components;
using WebApp.Constants;
using WebApp.Extensions;

namespace WebApp.Controllers
{
    public class ErrorController : BaseController
    {
        public ActionResult IISNotFound()
        {
            Response.StatusCode = Convert.ToInt32(HttpStatusCode.NotFound);
            return View(RouteConstants.NotFound);
        }

        public ActionResult NotFound()
        {
            Response.StatusCode = Convert.ToInt32(HttpStatusCode.NotFound);
            return View();
        }

        public ActionResult ServerError()
        {
            return View();
        }

        [HttpPost]
        public ActionResult ServerError(string reason, string additionalDetails)
        {
            if (string.IsNullOrEmpty(reason))
            {
                ViewBag.SelectOption = true;
                return View();
            }
            SendNotificationMail(reason, additionalDetails);
            if (User.IsSignedIn())
                return RedirectToRoute(RouteNames.Dashboard);
            else
                return Redirect(Common.GetDomainURL());
        }

        public ActionResult Forbidden()
        {
            return View();
        }

        [HttpGet]
        public ActionResult ThrowError()
        {
            throw new Exception("ThrowError action called. This error is thrown explicitly.");
        }

        private void SendNotificationMail(string action, string description)
        {
            try
            {
                var sessionId = Common.GetSessionId();
                var domainName = Common.GetDomainCD();
                var mailBody = new StringBuilder();
                string fromAddress = null;
                if (User.IsSignedIn())
                {
                    fromAddress = User.GetEmail();
                    if (string.IsNullOrEmpty(fromAddress))
                        fromAddress = CGUtility.GetOutgoingEmailAddress(domainName);
                }

                var toAddress = string.Empty;
                if (domainName == DomainFriendlyNameConstants.FreeResume)
                    toAddress = ConfigManager.GetConfig(MailerConstants.RESUMEGIG_ERROR_NOTIFY_EMAIL);
                else
                    toAddress = ConfigManager.GetConfig(MailerConstants.RESUMEHELP_ERROR_NOTIFY_EMAIL);
                var bccAddress = ConfigManager.GetConfig(MailerConstants.RESUMEHELP_SUPPORT_EMAIL);

                var mailSubject = string.Format("{0} Error Notification", domainName);
                //Creating formatted Mail Content
                mailBody.Clear();
                mailBody.Append("<table width=600px;><tr><td>");
                mailBody.AppendLine("<table border=\"0\" cellpadding=\"0\" cellspacing=\"0\">");
                mailBody.Append("<tr><td>User Action: " + action + "</td></tr><br>");
                mailBody.Append("<tr><td>User Session GUID: " + sessionId + "</td></tr><br>");
                mailBody.Append("<tr><td>UserId: " + UserId + "</td></tr><br>");
                mailBody.Append("<tr><td>Additional Detail: " + description + "</td></tr>");
                mailBody.AppendLine("</table>");
                mailBody.Append("</td></tr></Table>");

                var sdRecipients = new StringDictionary
                {
                    {TemplateIndex.eTI_To, toAddress},
                    {TemplateIndex.eTI_From, fromAddress},
                    {TemplateIndex.eTI_Subject, mailSubject},
                    {TemplateIndex.eTI_HTMLBody, mailBody.ToString()},
                    {TemplateIndex.eTI_Header, "SendGrid=send_contactus"},
                    {TemplateIndex.eTI_BCC, bccAddress}
                };

                var service = new CommunicationService();
                service.SendEmail(sdRecipients, null);
            }
            catch (Exception ex)
            {
                var htParam = new Hashtable(3);
                htParam.Add("Action", action);
                htParam.Add("Description", description);
                htParam.Add("UserId", UserId);
                var objMethod = (MethodInfo) MethodBase.GetCurrentMethod();
                GlobalExceptionHandler.customExceptionHandler(ex, objMethod, htParam, RouteConstants.ServerError,
                    Application.WebApp, ErrorSeverityType.High, ex.Message, true);
            }
        }
    }
}