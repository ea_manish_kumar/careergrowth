﻿using App.Services;
using App.Utility;
using DomainModel;
using Resources;
using Services.Documents;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Threading.Tasks;
using System.Web.Mvc;
using Utilities.Constants;
using Utilities.Constants.Documents;
using WebApp.Constants;
using WebApp.Extensions;
using WebApp.Models;

namespace WebApp.Controllers
{
    [Authorize]
    public class ResumeController : BaseFunnelController
    {
        private static readonly IList<string> MenuRoutes = new List<string>
        {
            RouteNames.ResumeChooseTemplate, RouteNames.ResumeContact, RouteNames.ResumeExperience, RouteNames.ResumeExperienceReview,
            RouteNames.ResumeEducation, RouteNames.ResumeEducationReview, RouteNames.ResumeSkills, RouteNames.ResumeSummary
        };

        public ResumeController(IDocumentService documentService) : base(documentService)
        {
            PreviewRouteName = RouteNames.ResumeFinalize;
            DefaultMenuRoutes = MenuRoutes;
            FirstFunnelRoute = MenuRoutes.First();
        }

        #region Preview
        [Route("resume/preview/{id:int}", Name = RouteNames.ResumeFinalize)]
        public ActionResult Finalize(int id)
        {
            if (id < 1)
                return Redirect(Url.BuildUrl<DashboardController>(dash => dash.Home(), RouteUrls.Dashboard));
            var documentService = new Services.Documents.DocumentService();
            var document = documentService.GetDocumentByID(id, UserId);
            if (document != null)
            {
                var model = new FinalizeVM
                {
                    DocumentId = document.Id,
                    SkinCD = document.SkinCD
                };
                var wizardMenuModel = DocumentCookieModel.Parse(document);
                PopulateSections(model, document, wizardMenuModel);
                return View(model);
            }

            return Redirect(Url.BuildUrl<DashboardController>(dash => dash.Home(), RouteNames.Dashboard));
        }
        #endregion

        #region Funnel Actions

        [HttpGet]
        [AllowAnonymous]
        [Route("resume/journey", Name = RouteNames.ResumeHowItWorks)]
        public ViewResult HowItWorks()
        {
            ViewBag.NextRouteUrl = Url.RouteUrl(FirstFunnelRoute);
            return View();
        }

        [HttpGet]
        [Route("resume/template", Name = RouteNames.ResumeChooseTemplate)]
        public async Task<ActionResult> ChooseTemplate()
        {
            var model = await CreateWizardModel(RouteNames.ResumeChooseTemplate);
            if (model != null)
                return View(model);
            else
                return RedirectToRoute(FirstFunnelRoute);
        }

        [HttpGet]
        [Route("resume/user-details", Name = RouteNames.ResumeContact)]
        public async Task<ActionResult> Contact()
        {
            var model = await CreateWizardModel(RouteNames.ResumeContact);
            if (model != null)
                return View(model);
            else
                return RedirectToRoute(FirstFunnelRoute);
        }

        [HttpGet]
        [Route("resume/work-details", Name = RouteNames.ResumeExperience)]
        public async Task<ActionResult> Experience()
        {
            var model = await CreateWizardModel(RouteNames.ResumeExperience);
            if (model != null)
                return View(model);
            else
                return RedirectToRoute(FirstFunnelRoute);
        }

        [HttpGet]
        [Route("resume/work-history", Name = RouteNames.ResumeExperienceReview)]
        public async Task<ActionResult> ExperienceReview()
        {
            var model = await CreateWizardModel(RouteNames.ResumeExperienceReview);
            if (model != null)
                return View(model);
            else
                return RedirectToRoute(FirstFunnelRoute);
        }

        [HttpGet]
        [Route("resume/academic-details", Name = RouteNames.ResumeEducation)]
        public async Task<ActionResult> Education()
        {
            var model = await CreateWizardModel(RouteNames.ResumeEducation);
            if (model != null)
                return View(model);
            else
                return RedirectToRoute(FirstFunnelRoute);
        }

        [HttpGet]
        [Route("resume/academic-history", Name = RouteNames.ResumeEducationReview)]
        public async Task<ActionResult> EducationReview()
        {
            var model = await CreateWizardModel(RouteNames.ResumeEducationReview);
            if (model != null)
                return View(model);
            else
                return RedirectToRoute(FirstFunnelRoute);
        }

        [HttpGet]
        [Route("resume/competencies", Name = RouteNames.ResumeSkills)]
        public async Task<ActionResult> Skills()
        {
            var model = await CreateWizardModel(RouteNames.ResumeSkills);
            if (model != null)
                return View(model);
            else
                return RedirectToRoute(FirstFunnelRoute);
        }

        [HttpGet]
        [Route("resume/summary", Name = RouteNames.ResumeSummary)]
        public async Task<ActionResult> Summary()
        {
            var model = await CreateWizardModel(RouteNames.ResumeSummary);
            if (model != null)
                return View(model);
            else
                return RedirectToRoute(FirstFunnelRoute);
        }

        #endregion

        [Route("create-new-resume", Name = RouteNames.CreateNewResume)]
        public ActionResult CreateNewResume()
        {
            DeleteDocumentCookie();
            return RedirectToRoute(RouteNames.ResumeChooseTemplate);
        }

        private void PopulateSections(FinalizeVM model, Document document, DocumentCookieModel wizardMenuModel)
        {
            IEnumerable<string> existingWizardCDs = null;
            if (wizardMenuModel != null)
                existingWizardCDs = wizardMenuModel.GetSections();
            model.Sections = WizardUtility.GetWizardMenuList(Url, RouteMode.Edit, document, wizardMenuModel);
            model.MissingSections =
                WizardUtility.GetMissingMenuList(Url, RouteMode.Edit, document.Id, wizardMenuModel, existingWizardCDs);
        }

        private Section GetNewSection(string sectionTypeCD, short sortIndex)
        {
            var section = new Section
            {
                SectionTypeCD = sectionTypeCD,
                SortIndex = sortIndex
            };
            switch (sectionTypeCD)
            {
                case SectionTypeCD.Name:
                    section.Label = Resource.NAME;
                    section.DocZoneTypeCD = DocZoneTypeCD.HEAD;
                    section.SortIndex = 0;
                    break;
                case SectionTypeCD.Contact:
                    section.Label = Resource.CNTC;
                    section.DocZoneTypeCD = DocZoneTypeCD.HEAD;
                    section.SortIndex = 0;
                    break;
                case SectionTypeCD.Experience:
                    section.Label = Resource.EXPR;
                    section.DocZoneTypeCD = DocZoneTypeCD.BODY;
                    break;
                case SectionTypeCD.Education:
                    section.Label = Resource.EDUC;
                    section.DocZoneTypeCD = DocZoneTypeCD.BODY;
                    break;
                case SectionTypeCD.Summary:
                    section.Label = Resource.SUMM;
                    section.DocZoneTypeCD = DocZoneTypeCD.BODY;
                    break;
                case SectionTypeCD.Skills:
                    section.Label = Resource.HILT;
                    section.DocZoneTypeCD = DocZoneTypeCD.BODY;
                    break;
                case SectionTypeCD.Accomplishments:
                    section.Label = Resource.ACCM;
                    section.DocZoneTypeCD = DocZoneTypeCD.BODY;
                    break;
                case SectionTypeCD.Awards:
                    section.Label = Resource.AWAR;
                    section.DocZoneTypeCD = DocZoneTypeCD.BODY;
                    break;
                case SectionTypeCD.Certifications:
                    section.Label = Resource.CERT;
                    section.DocZoneTypeCD = DocZoneTypeCD.BODY;
                    break;
                case SectionTypeCD.ExtraCurricular:
                    section.Label = Resource.EXCL;
                    section.DocZoneTypeCD = DocZoneTypeCD.BODY;
                    break;
                case SectionTypeCD.Language:
                    section.Label = Resource.LANG;
                    section.DocZoneTypeCD = DocZoneTypeCD.BODY;
                    break;
                case SectionTypeCD.PersonalInformation:
                    section.Label = Resource.PRIN;
                    section.DocZoneTypeCD = DocZoneTypeCD.BODY;
                    break;
                case SectionTypeCD.References:
                    section.Label = Resource.REFE;
                    section.DocZoneTypeCD = DocZoneTypeCD.BODY;
                    break;
                case SectionTypeCD.SocialService:
                    section.Label = Resource.SCSV;
                    section.DocZoneTypeCD = DocZoneTypeCD.BODY;
                    break;
                case SectionTypeCD.Other:
                    section.Label = Resource.Other;
                    section.DocZoneTypeCD = DocZoneTypeCD.BODY;
                    break;
            }

            return section;
        }

        #region Ajax Methods

        [HttpPost]
        [Route(AjaxRoutes.Documents + "/{docId}/" + AjaxRoutes.SaveSections, Name = AjaxRouteNames.SaveSections)]
        public ActionResult SaveSections(int docId, SaveSectionModel model)
        {
            if (docId > 0 && model != null && model.Sections != null && model.Sections.Count > 0)
            {
                var sectionCDs = WizardUtility.GetSectionCDsFromWizards(model.Sections);
                var docService = new Services.Documents.DocumentService();
                var doc = docService.GetDocumentByID(docId, UserId);
                if (doc != null)
                {
                    IEnumerable<string> addSections = null;
                    var sectionService = new SectionService();

                    var existingSections = sectionService.GetSectionByDocAndTypeCD(doc, sectionCDs.ToArray());
                    if (existingSections != null && existingSections.Count > 0)
                        addSections = sectionCDs.Where(sec => existingSections.Any(ex =>
                            string.Equals(ex.SectionTypeCD, sec, StringComparison.OrdinalIgnoreCase)));
                    else
                        addSections = sectionCDs.AsEnumerable();
                    if (addSections != null && addSections.Any())
                    {
                        var docCookie = ParseDocumentCookie();
                        if (docCookie == null)
                            docCookie = DocumentCookieModel.Parse(doc);
                        if (docCookie == null)
                            docCookie = new DocumentCookieModel();
                        var maxSortIndex = sectionService.GetMaxSortIndex(doc);
                        var nextSortIndex = ++maxSortIndex;
                        foreach (var sectionTypeCD in addSections)
                        {
                            var section = GetNewSection(sectionTypeCD, nextSortIndex);
                            doc.AddSection(section);
                            docCookie.AddAdditionalSection(WizardUtility.GetWizardFromSectionCD(sectionTypeCD));
                            nextSortIndex++;
                        }

                        docService.UpdateDocument(doc);
                        FlushDocumentCookie(docCookie);
                        string nextRoute = null;
                        var firstWizard = WizardUtility.GetWizard(Url, model.Sections.FirstOrDefault(), RouteMode.Add,
                            docId, null, docCookie);
                        if (firstWizard != null)
                            nextRoute = firstWizard.SectionURL;
                        return ToJson(new { result = true, nextRoute });
                    }

                    return HttpCodeJsonResponse(HttpStatusCode.Conflict);
                }

                return HttpCodeJsonResponse(HttpStatusCode.Forbidden);
            }

            return HttpCodeJsonResponse(HttpStatusCode.BadRequest);
        }

        [HttpPost]
        [Route(AjaxRoutes.Sections + "/" + AjaxRoutes.Delete, Name = AjaxRouteNames.DeleteSection)]
        public ActionResult DeleteSection(int? groupId)
        {
            if (groupId.HasValue && groupId.Value > 0)
            {
                var sectionService = new SectionService();
                var section = sectionService.GetSection(groupId.Value, UserId);
                if (section != null && section.Document != null)
                {
                    var documentId = section.Document.Id;
                    Section anotherSectionToDelete = null;
                    if (SectionTypeCD.Name.Equals(section.SectionTypeCD, StringComparison.OrdinalIgnoreCase))
                        anotherSectionToDelete =
                            sectionService.GetSectionByDocAndTypeCD(section.Document, SectionTypeCD.Contact);
                    else if (SectionTypeCD.Contact.Equals(section.SectionTypeCD, StringComparison.OrdinalIgnoreCase))
                        anotherSectionToDelete =
                            sectionService.GetSectionByDocAndTypeCD(section.Document, SectionTypeCD.Name);

                    bool isDocumentDeleted;
                    sectionService.DeleteSection(section, out isDocumentDeleted);
                    if (anotherSectionToDelete != null)
                        sectionService.DeleteSection(anotherSectionToDelete, out isDocumentDeleted);

                    if (isDocumentDeleted)
                    {
                        DeleteDocumentCookie();
                    }
                    else
                    {
                        if (!string.IsNullOrEmpty(Request.Cookies.Read(CookieConstants.DocumentCookie)))
                            FlushDocumentCookie(DocumentCookieModel.Parse(section.Document));
                    }

                    return ToJson(new { success = true, isDocumentDeleted });
                }

                return HttpCodeJsonResponse(HttpStatusCode.Forbidden);
            }

            return HttpCodeJsonResponse(HttpStatusCode.BadRequest);
        }

        #endregion


        private DocumentCookieModel ParseDocumentCookie()
        {
            var documentCookie = Request.Cookies.Decrypt(SiteCookies.DocumentCookie);
            if (Strings.IsNotNullOrEmpty(documentCookie))
                return DocumentCookieModel.Parse(documentCookie);
            return null;
        }

        private void DeleteDocumentCookie()
        {
            Response.Cookies.Delete(CookieConstants.DocumentCookie);
        }

        private void FlushDocumentCookie(DocumentCookieModel documentCookieModel)
        {
            var documentCookie = DocumentCookieModel.ToJsonString(documentCookieModel);
            if (Strings.IsNotNullOrEmpty(documentCookie))
                Response.Cookies.Encrypt(SiteCookies.DocumentCookie, documentCookie);
        }

    }
}