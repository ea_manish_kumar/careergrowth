﻿using App.DomainModel;
using App.Model.Users;
using App.Services;
using App.Utility;
using Resources;
using System;
using System.Linq;
using System.Threading.Tasks;
using System.Web.Mvc;
using WebApp.Constants;
using WebApp.Extensions;
using WebApp.Managers;
using WebApp.Models.Email;

namespace WebApp.Controllers
{
    [RoutePrefix("account")]
    public class AccountController : Controller
    {
        private readonly ITrackingService TrackingService;
        private readonly ISignInManager SignInManager;

        #region Action Methods

        public AccountController(ITrackingService trackingService, ISignInManager signInManager)
        {
            TrackingService = trackingService;
            SignInManager = signInManager;
        }

        [HttpGet]
        [AllowAnonymous]
        [Route("login", Name = RouteNames.AccountLogin)]
        public ActionResult Login()
        {
            if (User.IsSignedIn())
                return RedirectToRoute(RouteNames.Dashboard);

            var model = new LoginModel
            {
                RememberMe = true,
            };
            if (!string.IsNullOrEmpty(Request.QueryString["to"]))
            {
                ViewBag.Heading = Resource.SessionExpiredMsg;
                ViewBag.SubHeading = Resource.SessionTimeOutMsg;
            }
            else if (!string.IsNullOrEmpty(Request.QueryString["so"]))
                ViewBag.Heading = Resource.LogoutMsg;
            else
                ViewBag.Heading = Resource.WelcomeBackMsg;

            return View(model);
        }

        [HttpPost]
        [AllowAnonymous]
        [Route("login")]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> Login(LoginModel model, string ReturnUrl)
        {
            if (User.IsSignedIn())
                return RedirectToRoute(RouteNames.Dashboard);

            if (ModelState.IsValid)
            {
                if (Guid.TryParse(Request.Cookies.Read(SiteCookies.SessionUID), out Guid sessionUID))
                {
                    var result = await SignInManager.PasswordSignIn(model.UserName, model.Password, model.RememberMe);
                    if (result == SignInResult.Success)
                    {
                        if (Url.IsLocalUrl(ReturnUrl))
                            return Redirect(ReturnUrl);
                        else
                            return RedirectToRoute(RouteNames.Dashboard);
                    }
                    else if (result == SignInResult.LockedOut)
                        ViewBag.ErrorMessage = Resource.UserLockedOut;
                    else if (result == SignInResult.PasswordMismatch || result == SignInResult.NotFound)
                        ViewBag.ErrorMessage = Resource.UsernameOrPasswordIncorrect;
                    else if (result == SignInResult.Deactivated)
                        ViewBag.ErrorMessage = Resource.AccountDeactivatedMsg;
                    else
                        ViewBag.ErrorMessage = Resource.TechnicalError;
                }
                else
                    ViewBag.ErrorMessage = Resource.TechnicalError;
                return View(model);
            }
            else
                ViewBag.ErrorMessage = Resource.TechnicalError;

            return View(model);
        }

        [HttpGet]
        [AllowAnonymous]
        [Route("register", Name = RouteNames.AccountRegister)]
        public ActionResult Register()
        {
            if (User.IsSignedIn())
                return RedirectToRoute(RouteNames.Dashboard);

            return View();
        }

        [HttpPost]
        [AllowAnonymous]
        [Route("register")]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> Register(RegisterModel model, string ReturnUrl)
        {
            if (User.IsSignedIn())
                return RedirectToRoute(RouteNames.Dashboard);

            if (ModelState.IsValid)
            {
                var result = await SignInManager.RegisterUser(model.Name, model.UserName, model.Password);
                if (result == SignInResult.Success)
                {
                    if (Url.IsLocalUrl(ReturnUrl))
                        return Redirect(ReturnUrl);
                    else
                        return RedirectToRoute(RouteNames.Dashboard);
                }
                else if (result == SignInResult.LockedOut)
                    ViewBag.ErrorMessage = Resource.UserLockedOut;
                else
                    ViewBag.ErrorMessage = Resource.UsernameOrPasswordIncorrect;
            }
            else
                ViewBag.ErrorMessage = Resource.TechnicalError;

            return View(model);
        }


        [Authorize]
        [Route("logout", Name = RouteNames.AccountLogout)]
        public async Task<RedirectToRouteResult> Logout()
        {
            Response.Cookies.Delete(SiteCookies.DocumentCookie);
            await SignInManager.SignOut();
            return RedirectToRoute(RouteNames.AccountLogin, new { so = 1 });
        }

        [HttpGet]
        [AllowAnonymous]
        [Route("forgot-password", Name = RouteNames.AccountForgotPassword)]
        public ActionResult ForgotPassword()
        {
            if (User.IsSignedIn())
                return RedirectToRoute(RouteNames.Dashboard);

            return View();
        }

        [HttpPost]
        [AllowAnonymous]
        [Route("forgot-password")]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> ForgotPassword(string username)
        {
            if (User.IsSignedIn())
                return RedirectToRoute(RouteNames.Dashboard);

            if (ModelState.IsValid)
            {
                var userToken = await SignInManager.GenerateUserToken(username, UserTokenType.ResetPassword);
                if (userToken != null)
                {
                    var forgotPasswordViewModel = new ForgotPasswordViewModel
                    {
                        Name = userToken.User.Name,
                        LinkValidityTime = DateTimeUtility.GetFormattedTimePeriod(userToken.ExpiryTime),
                        SetPasswordLink = Url.RouteUrl(RouteNames.AccountResetPassword, new { tuid = userToken.UID.ToString(), tkn = userToken.HashedToken })
                    };

                    //TODO: Send email to the user
                    var htmlBody = this.RenderView(EmailViewNames.ForgotPassword, forgotPasswordViewModel);
                    return Content(htmlBody, "text/html");
                }
            }

            return RedirectToRoute(RouteNames.AccountForgotPasswordConfirmation);
        }

        [HttpGet]
        [AllowAnonymous]
        [Route("forgot-password-confirmation", Name = RouteNames.AccountForgotPasswordConfirmation)]
        public ActionResult ForgotPasswordConfirmation()
        {
            if (User.IsSignedIn())
                return RedirectToRoute(RouteNames.Dashboard);

            return View();
        }

        [HttpGet]
        [AllowAnonymous]
        [Route("reset-password", Name = RouteNames.AccountResetPassword)]
        public ActionResult ResetPassword()
        {
            if (User.IsSignedIn())
                return RedirectToRoute(RouteNames.Dashboard);

            return View();
        }

        [HttpPost]
        [AllowAnonymous]
        [Route("reset-password")]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> ResetPassword(string tuid, string tkn, ResetPasswordModel model)
        {
            if (User.IsSignedIn())
                return RedirectToRoute(RouteNames.Dashboard);

            if (ModelState.IsValid)
            {
                if (Guid.TryParse(tuid, out Guid uid) && !string.IsNullOrEmpty(tkn))
                {
                    var passwordReset = await SignInManager.ResetPassword(uid, tkn, model.Password);
                    if (passwordReset)
                        return RedirectToRoute(RouteNames.AccountResetPasswordConfirmation);
                }
                ViewBag.ErrorMessage = Resource.InvalidLink;
            }
            return View();
        }

        [HttpGet]
        [AllowAnonymous]
        [Route("reset-password-confirmation", Name = RouteNames.AccountResetPasswordConfirmation)]
        public ActionResult ResetPasswordConfirmation()
        {
            if (User.IsSignedIn())
                return RedirectToRoute(RouteNames.Dashboard);

            return View();
        }

        [HttpGet]
        [Authorize]
        [Route("settings", Name = RouteNames.AccountProfileSettings)]
        public ActionResult Settings()
        {
            var profileSettings = new ProfileSettingsModel
            {
                AccountId = User.GetId(),
                EmailAddress = User.GetEmail(),
                Name = User.GetDisplayName(),
                
            };
            return View(profileSettings);
        }

        [HttpGet]
        [Authorize]
        [Route("change-password", Name = RouteNames.AccountChangePassword)]
        public ActionResult ChangePassword()
        {
            return View();
        }

        [HttpPost]
        [Authorize]
        [Route("change-password")]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> ChangePassword(ChangePasswordModel model)
        {
            if (ModelState.IsValid)
            {
                var result = await SignInManager.ChangePassword(User.GetId(), model.Password, model.NewPassword);
                if (result == SignInResult.Success)
                    ViewBag.IsSuccess = true;
                else if (result == SignInResult.LockedOut)
                    ViewBag.ErrorMessage = Resource.UserLockedOut;
                else if (result == SignInResult.PasswordMismatch || result == SignInResult.NotFound)
                    ViewBag.ErrorMessage = Resource.PasswordIncorrect;
                else if (result == SignInResult.Deactivated)
                    ViewBag.ErrorMessage = Resource.AccountDeactivatedMsg;
                else
                    ViewBag.ErrorMessage = Resource.TechnicalError;
            }
            return View();
        }

        #endregion


        #region Ajax Methods
        [HttpPost]
        [AllowAnonymous]
        [Route("exists", Name = AjaxRouteNames.UserNameExists)]
        public async Task<JsonResult> UserNameExists(string username)
        {
            return Json(await SignInManager.UserExists(username));
        }
        #endregion
    }
}