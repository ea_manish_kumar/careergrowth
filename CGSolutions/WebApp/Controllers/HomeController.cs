﻿using Resources;
using Services.Communication;
using System;
using System.Net;
using System.Web.Mvc;
using Utilities;
using WebApp.Components;
using WebApp.Constants;
using WebApp.Models;

namespace WebApp.Controllers
{
    public class HomeController : BaseController
    {
        public ActionResult Index()
        {
            //if (Request.IsAuthenticated)
            //    return Redirect(Url.BuildUrl<DashboardController>(dash => dash.Home(), RouteNames.Dashboard));
            return View();
        }

        public static string GetYear()
        {
            return DateTime.Today.Year.ToString();
        }

        public ActionResult ContactUs()
        {
            var domainCD = Common.GetDomainCD();
            ViewBag.DomainName = Common.GetPortalName(domainCD);
            ViewBag.Title = Resource.TitleContactUs + Common.DomainName;
            return View();
        }

        [HttpPost]
        [Route(AjaxRoutes.ContactUsEmail)]
        public ActionResult SendContacUsEmail(EmailModel emailObj)
        {
            var domainCD = Common.GetDomainCD();
            if (emailObj == null)
                return HttpCodeJsonResponse(HttpStatusCode.BadRequest);
            var emailTo = emailObj.RecipientAddress;
            if (string.IsNullOrEmpty(emailTo))
                emailTo = CGUtility.GetOutgoingEmailAddress(domainCD);

            if (string.IsNullOrEmpty(emailTo))
                return HttpCodeJsonResponse(HttpStatusCode.ExpectationFailed);
            var cs = new CommunicationService();
            if (cs.SendContactUsEmail(emailTo, domainCD, emailObj.EmailSubject, emailObj.EmailBody, emailObj.FromName,
                Resource.ContactUsName, Resource.ContactUsEmailAddress))
                return Json(true);
            return Json(false);
        }

        public ActionResult Terms()
        {
            ViewBag.DomainCD = Common.GetDomainCD();
            ViewBag.Title = Resource.TitleUserAgreement + Common.DomainName;
            return View();
        }

        public ActionResult Privacy()
        {
            ViewBag.DomainCD = Common.GetDomainCD();
            ViewBag.Title = Resource.TitlePrivacyPolicy + Common.DomainName;
            return View();
        }
        
        public ActionResult HowToCreateResume()
        {
            return View();
        }

        public ActionResult ResumeSamples()
        {
            return View();
        }

        public ActionResult InterviewTips()
        {
            return View();
        }

        public ActionResult FresherInterviewQuestions()
        {
            return View();
        }
        
        public ActionResult JobHunting()
        {
            return View();
        }

        public ActionResult ResumeWritingTips()
        {
            return View();
        }
    }
}