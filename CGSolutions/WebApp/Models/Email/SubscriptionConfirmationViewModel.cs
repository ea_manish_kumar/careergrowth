﻿using System;

namespace WebApp.Models.Email
{
    public class SubscriptionConfirmationViewModel
    {
        public string Name { get; set; }
        public string OrderId { get; set; }
        public string Currency { get; set; }
        public decimal Amount { get; set; }
        public DateTime SubscriptionExpiryTime { get; set; }
        public string SupportEmail { get; set; }
    }
}