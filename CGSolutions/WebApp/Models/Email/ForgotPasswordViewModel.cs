﻿namespace WebApp.Models.Email
{
    public class ForgotPasswordViewModel
    {
        public string Name { get; set; }
        public string SetPasswordLink { get; set; }
        public string LinkValidityTime { get; set; }
    }
}