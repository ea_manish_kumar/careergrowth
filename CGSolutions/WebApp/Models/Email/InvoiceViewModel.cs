﻿using System;

namespace WebApp.Models.Email
{
    public class InvoiceViewModel
    {
        public string TransactionReferenceID { get; set; }
        public string PaymentReferenceID { get; set; }
        public string PaymentReceivedTowards { get; set; }
        public string Currency { get; set; }
        public decimal Amount { get; set; }
        public DateTime TransactionTime { get; set; }
    }
}