﻿namespace WebApp.Models
{
    public class ConfirmationViewModel
    {
        public string CreditCardStatementText { get; set; }
        public bool IsSuspendedUser { get; set; }
        public string ConfirmationHeading { get; set; }
        public string ConfirmationSubHeading { get; set; }
        public string ProductAccessDuration { get; set; }
        public string RecurringText { get; set; }
        public string TotalAmount { get; set; }
        public string SelfCancellationMessage { get; set; }
    }
}