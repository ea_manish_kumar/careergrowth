﻿namespace WebApp.Models
{
    public class CcAvenueViewModel
    {
        public string EncryptedRequest { get; set; }
        public string AccessCode { get; set; }
        public string CheckoutUrl { get; set; }
    }
}