﻿namespace WebApp.Models
{
    public class WizardVM
    {
        public string Mode { get; set; }

        public bool LoadData { get; set; }

        public int DocumentId { get; set; }

        public int SectionId { get; set; }

        public int ParagraphId { get; set; }

        public string NextRouteUrl { get; set; }

        public string BackRouteUrl { get; set; }

        public DocumentCookieModel DocumentCookieModel { get; set; }
    }

    public class WizardMenu
    {
        public string SectionCode { get; set; }
        public string SectionURL { get; set; }
        public int SectionId { get; set; }
        public string SectionDisplayValue { get; set; }
        public string Icon { get; set; }
    }
}