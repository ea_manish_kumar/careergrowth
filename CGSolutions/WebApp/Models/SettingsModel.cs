﻿using Newtonsoft.Json;

namespace WebApp.Models
{
    public class SettingsModel
    {
        [JsonProperty("notificationId")] public short NotificationId { get; set; }

        [JsonProperty("value")] public bool Value { get; set; }
    }
}