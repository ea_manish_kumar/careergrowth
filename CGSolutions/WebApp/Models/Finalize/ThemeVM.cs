﻿namespace WebApp.Models
{
    public class ThemeVM
    {
        public string ThemeCD { get; set; }
        public string ThemeName { get; set; }
    }
}