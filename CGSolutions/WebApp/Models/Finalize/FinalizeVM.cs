﻿using System.Collections.Generic;

namespace WebApp.Models
{
    public class FinalizeVM
    {
        public int DocumentId { get; set; }
        public string SkinCD { get; set; }
        public IEnumerable<WizardMenu> Sections { get; set; }
        public IEnumerable<WizardMenu> MissingSections { get; set; }
    }
}