﻿using Newtonsoft.Json;

namespace WebApp.Models
{
    public class SortedSection
    {
        [JsonProperty("groupId")] public int GroupId { get; set; }

        [JsonProperty("sortIndex")] public short SortIndex { get; set; }
    }
}