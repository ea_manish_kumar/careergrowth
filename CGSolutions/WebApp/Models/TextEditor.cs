﻿using System.ComponentModel.DataAnnotations;
using System.Web.Mvc;

namespace WebApp.Models
{
    public enum EditorType
    {
        HtmlEditor = 1,
        TinyMceEditor = 2,
        Trumbowyg = 3,
        Squire = 4
    }

    public class TextEditorModel
    {
        public TextEditorModel()
        {
            //EditorType = EditorType.Squire;
            EditorType = EditorType.Trumbowyg;
        }

        [Required]
        [AllowHtml]
        [Display(Name = "Page Content")]
        public string PageContent { get; set; }

        public string EditorId { get; set; }

        public string PlaceHolder { get; set; }

        public string DocumentField { get; set; }

        public EditorType EditorType { get; set; }
    }
}