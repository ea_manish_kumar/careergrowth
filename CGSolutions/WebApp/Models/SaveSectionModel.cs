﻿using System.Collections.Generic;
using Newtonsoft.Json;

namespace WebApp.Models
{
    public class SaveSectionModel
    {
        [JsonProperty("sections")] public IList<string> Sections { get; set; }

        [JsonProperty("otherText")] public string OtherSectionText { get; set; }
    }
}