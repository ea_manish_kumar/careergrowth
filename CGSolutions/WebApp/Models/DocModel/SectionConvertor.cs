﻿using System;
using System.Linq;
using DomainModel;
using Resources;
using Utilities.Constants.Documents;
using WebApp.Constants;

namespace WebApp.Models
{
    public class SectionConvertor
    {
        public static DocumentModel ToDocumentModel(Document document)
        {
            DocumentModel model = null;
            if (document != null)
            {
                model = new DocumentModel();
                model.DocumentId = document.Id;
                if (document.Sections != null && document.Sections.Count > 0)
                {
                    foreach (var section in document.Sections.Where(sec =>
                        sec.Paragraphs != null && sec.Paragraphs.Count > 0))
                        switch (section.SectionTypeCD)
                        {
                            case SectionTypeCD.Name:
                                model.Name = ToName(section);
                                break;
                            case SectionTypeCD.Contact:
                                model.Contact = ToContact(section);
                                break;
                            case SectionTypeCD.Experience:
                                model.Experience = ToExperienceDetails(section);
                                break;
                            case SectionTypeCD.Education:
                                model.Education = ToEducationDetails(section);
                                break;
                            case SectionTypeCD.Skills:
                                model.Skills = ToSkills(section);
                                break;
                            case SectionTypeCD.Summary:
                                model.Summary = ToSummary(section);
                                break;
                            case SectionTypeCD.Awards:
                                model.Awards = ToOtherSection(section);
                                break;
                            case SectionTypeCD.Accomplishments:
                                model.Accomplishments = ToOtherSection(section);
                                break;
                            case SectionTypeCD.Additional_Information:
                                model.AdditionalInformation = ToOtherSection(section);
                                break;
                            case SectionTypeCD.Affiliations:
                                model.Affiliations = ToOtherSection(section);
                                break;
                            case SectionTypeCD.Certifications:
                                model.Certifications = ToOtherSection(section);
                                break;
                            case SectionTypeCD.ExtraCurricular:
                                model.ExtraCurricular = ToOtherSection(section);
                                break;
                            case SectionTypeCD.Interests:
                                model.Interests = ToOtherSection(section);
                                break;
                            case SectionTypeCD.Language:
                                model.Language = ToOtherSection(section);
                                break;
                            case SectionTypeCD.Other:
                                model.Other = ToOtherSection(section);
                                break;
                            case SectionTypeCD.PersonalInformation:
                                model.PersonalInformation = ToOtherSection(section);
                                break;
                            case SectionTypeCD.Presentations:
                                model.Presentations = ToOtherSection(section);
                                break;
                            case SectionTypeCD.Publication:
                                model.Publication = ToOtherSection(section);
                                break;
                            case SectionTypeCD.References:
                                model.References = ToOtherSection(section);
                                break;
                            case SectionTypeCD.SocialService:
                                model.SocialService = ToOtherSection(section);
                                break;
                            //case SectionTypeCD.Training: model.Training = ToOtherSection(section); break;
                            //case SectionTypeCD.Portfolio: model.Portfolio = ToOtherSection(section); break;
                            //case SectionTypeCD.Volunteer: model.Volunteer = ToOtherSection(section); break;
                        }

                    if (document.DocStyles != null && document.DocStyles.Count > 0)
                    {
                        foreach (var style in document.DocStyles)
                            model.AddStyles(style.StyleCD, style.Value);
                    }

                    DocumentHelper.EvaluateDocumentProperties(model);
                }
            }

            return model;
        }

        public static void GetName(string name, ref string firstName, ref string lastName)
        {
            if (!string.IsNullOrEmpty(name))
            {
                name = name.Trim();
                if (name.Contains(' '))
                {
                    var lastSpaceIndex = name.LastIndexOf(' ');
                    var nameLength = name.Length;
                    firstName = name.Substring(0, lastSpaceIndex);
                    lastName = name.Substring(lastSpaceIndex + 1, nameLength - lastSpaceIndex - 1);
                }
                else
                {
                    firstName = name;
                }
            }
        }

        public static void PopulateName(ref Paragraph namePara, NameSectionModel nameSection)
        {
            string name = nameSection.Name, firstName = null, lastName = null;
            if (!string.IsNullOrEmpty(name))
            {
                name = name.Trim();
                if (name.Contains(' '))
                {
                    var lastSpaceIndex = name.LastIndexOf(' ');
                    var nameLength = name.Length;
                    firstName = name.Substring(0, lastSpaceIndex);
                    lastName = name.Substring(lastSpaceIndex + 1, nameLength - lastSpaceIndex - 1);
                }
                else
                {
                    firstName = name;
                }
            }

            SaveOrUpdateDocData(ref namePara, FieldTypeCD.FirstName, firstName);
            SaveOrUpdateDocData(ref namePara, FieldTypeCD.LastName, lastName);
        }

        public static void PopulateContact(ref Paragraph contactPara, ContactSectionModel contact)
        {
            SaveOrUpdateDocData(ref contactPara, FieldTypeCD.Email, contact.Email);
            SaveOrUpdateDocData(ref contactPara, FieldTypeCD.Street, contact.Address);
            SaveOrUpdateDocData(ref contactPara, FieldTypeCD.City, contact.City);
            SaveOrUpdateDocData(ref contactPara, FieldTypeCD.State, contact.State);
            SaveOrUpdateDocData(ref contactPara, FieldTypeCD.ZipCode, contact.ZipCode);
            SaveOrUpdateDocData(ref contactPara, FieldTypeCD.HomePhone, contact.Phone);
            SaveOrUpdateDocData(ref contactPara, FieldTypeCD.AltPhone, contact.AltPhone);
        }

        public static NameSectionModel ToName(Section nameSection)
        {
            NameSectionModel nameSectionModel = null;
            if (nameSection != null)
            {
                nameSectionModel = new NameSectionModel();
                nameSectionModel.SectionId = nameSection.Id;
                nameSectionModel.SortIndex = nameSection.SortIndex;
                if (nameSection.Paragraphs != null && nameSection.Paragraphs.Count > 0)
                {
                    var namePara = nameSection.Paragraphs.FirstOrDefault();
                    if (namePara != null)
                    {
                        nameSectionModel.ParagraphId = namePara.Id;
                        nameSectionModel.ParaSortIndex = namePara.SortIndex;
                        if (namePara.DocData != null && namePara.DocData.Count > 0)
                            foreach (var docData in namePara.DocData)
                            {
                                if (docData.FieldCD == FieldTypeCD.FirstName)
                                    nameSectionModel.Name = docData.CharValue;

                                if (docData.FieldCD == FieldTypeCD.LastName && !string.IsNullOrEmpty(docData.CharValue))
                                    nameSectionModel.Name += " " + docData.CharValue;
                            }
                    }
                }
            }

            return nameSectionModel;
        }

        public static ContactSectionModel ToContact(Section contactSection)
        {
            ContactSectionModel contact = null;
            if (contactSection != null)
            {
                contact = new ContactSectionModel();
                contact.SectionId = contactSection.Id;
                contact.SortIndex = contactSection.SortIndex;
                if (contactSection.Paragraphs != null && contactSection.Paragraphs.Count > 0)
                {
                    var contactPara = contactSection.Paragraphs[0];
                    if (contactPara != null)
                    {
                        contact.ParagraphId = contactPara.Id;
                        contact.ParaSortIndex = contactPara.SortIndex;
                        if (contactPara.DocData != null && contactPara.DocData.Count > 0)
                            foreach (var docData in contactPara.DocData)
                                switch (docData.FieldCD)
                                {
                                    case FieldTypeCD.Email:
                                        contact.Email = docData.CharValue;
                                        break;
                                    case FieldTypeCD.Street:
                                        contact.Address = docData.CharValue;
                                        break;
                                    case FieldTypeCD.City:
                                        contact.City = docData.CharValue;
                                        break;
                                    case FieldTypeCD.State:
                                        contact.State = docData.CharValue;
                                        break;
                                    case FieldTypeCD.ZipCode:
                                        contact.ZipCode = docData.CharValue;
                                        break;
                                    case FieldTypeCD.HomePhone:
                                        contact.Phone = docData.CharValue;
                                        break;
                                    case FieldTypeCD.AltPhone:
                                        contact.AltPhone = docData.CharValue;
                                        break;
                                }
                    }
                }
            }

            return contact;
        }

        public static ContactInfo ToContactInfo(Section nameSection, Section contactSection)
        {
            var contactInfo = new ContactInfo();
            contactInfo.Name = ToName(nameSection);
            contactInfo.Contact = ToContact(contactSection);
            return contactInfo;
        }

        public static void PopulateExperience(ref Paragraph expPara, Experience experience)
        {
            SaveOrUpdateDocData(ref expPara, FieldTypeCD.Company, experience.Employer);
            SaveOrUpdateDocData(ref expPara, FieldTypeCD.JobCity, experience.City);
            SaveOrUpdateDocData(ref expPara, FieldTypeCD.JobState, experience.State);
            SaveOrUpdateDocData(ref expPara, FieldTypeCD.JobTitle, experience.JobTitle);
            if (experience.StartDateMonth > 0 && experience.StartDateYear > 0)
                SaveOrUpdateDocData(ref expPara, FieldTypeCD.JobStartDate,
                    experience.StartDateMonth + "/" + experience.StartDateYear);
            else
                SaveOrUpdateDocData(ref expPara, FieldTypeCD.JobStartDate, string.Empty);
            if (experience.PresentlyWorkHere)
                SaveOrUpdateDocData(ref expPara, FieldTypeCD.JobEndDate, Resource.Present);
            else if (experience.EndDateMonth > 0 && experience.EndDateYear > 0)
                SaveOrUpdateDocData(ref expPara, FieldTypeCD.JobEndDate,
                    experience.EndDateMonth + "/" + experience.EndDateYear);
            else
                SaveOrUpdateDocData(ref expPara, FieldTypeCD.JobEndDate, string.Empty);
            SaveOrUpdateDocData(ref expPara, FieldTypeCD.JobDescription, experience.Description);
        }

        public static Experience ToExperience(Paragraph expPara)
        {
            Experience experience = null;
            if (expPara != null)
            {
                experience = new Experience();
                experience.ParagraphId = expPara.Id;
                experience.ParaSortIndex = expPara.SortIndex;
                if (expPara.DocData != null)
                    foreach (var docData in expPara.DocData)
                        switch (docData.FieldCD)
                        {
                            case FieldTypeCD.Company:
                                experience.Employer = docData.CharValue;
                                break;
                            case FieldTypeCD.JobCity:
                                experience.City = docData.CharValue;
                                break;
                            case FieldTypeCD.JobState:
                                experience.State = docData.CharValue;
                                break;
                            case FieldTypeCD.JobTitle:
                                experience.JobTitle = docData.CharValue;
                                break;
                            case FieldTypeCD.JobDescription:
                                experience.Description = docData.CharValue;
                                break;
                            case FieldTypeCD.JobStartDate:
                                if (!string.IsNullOrEmpty(docData.CharValue) && docData.CharValue.Contains("/"))
                                {
                                    int month, year;
                                    var dateArr = docData.CharValue.Split('/');
                                    if (int.TryParse(dateArr[0], out month) && int.TryParse(dateArr[1], out year) &&
                                        month > 0 && year > 0)
                                    {
                                        experience.StartDateMonth = month;
                                        experience.StartDateYear = year;
                                    }
                                }

                                break;
                            case FieldTypeCD.JobEndDate:
                                if (!string.IsNullOrEmpty(docData.CharValue))
                                {
                                    if (docData.CharValue.Equals(AppConstants.Present))
                                    {
                                        var currentDate = DateTime.Now.Date;
                                        experience.EndDateMonth = currentDate.Month;
                                        experience.EndDateYear = currentDate.Year;
                                        experience.PresentlyWorkHere = true;
                                    }
                                    else if (docData.CharValue.Contains("/"))
                                    {
                                        int month, year;
                                        var dateArr = docData.CharValue.Split('/');
                                        if (int.TryParse(dateArr[0], out month) && int.TryParse(dateArr[1], out year) &&
                                            month > 0 && year > 0)
                                        {
                                            experience.EndDateMonth = month;
                                            experience.EndDateYear = year;
                                        }
                                    }
                                }

                                break;
                        }
            }

            return experience;
        }

        public static bool IsExperienceEmpty(Experience experience)
        {
            return string.IsNullOrEmpty(experience.Employer) && string.IsNullOrEmpty(experience.JobTitle);
        }

        public static bool IsExperienceEmpty(ExperienceDetails experience)
        {
            if (experience.Paragraphs != null)
            {
                var count = experience.Paragraphs.Count();
                if (count > 1)
                    return false;
                if (count > 0)
                    return IsExperienceEmpty(experience.Paragraphs.FirstOrDefault());
            }

            return true;
        }

        public static ExperienceDetails ToExperienceDetails(Section expSection)
        {
            ExperienceDetails expDetails = null;
            if (expSection != null)
            {
                expDetails = new ExperienceDetails();
                expDetails.SectionId = expSection.Id;
                expDetails.SortIndex = expSection.SortIndex;
                if (expSection.Paragraphs != null)
                    expDetails.Paragraphs = expSection.Paragraphs.OrderBy(ord => ord.SortIndex)
                        .Where(p => p.DocData != null).Select(p => ToExperience(p));
            }

            return expDetails;
        }

        public static void PopulateEducation(ref Paragraph educPara, Education education)
        {
            SaveOrUpdateDocData(ref educPara, FieldTypeCD.SchoolName, education.School);
            SaveOrUpdateDocData(ref educPara, FieldTypeCD.University, education.University);
            SaveOrUpdateDocData(ref educPara, FieldTypeCD.SchoolCity, education.City);
            SaveOrUpdateDocData(ref educPara, FieldTypeCD.SchoolState, education.State);
            SaveOrUpdateDocData(ref educPara, FieldTypeCD.DegreeEarned, education.Degree);
            SaveOrUpdateDocData(ref educPara, FieldTypeCD.DegreeSpecialization, education.DegreeSpecialization);
            SaveOrUpdateDocData(ref educPara, FieldTypeCD.CGPA, education.CGPA);
            //SaveOrUpdateDocData(ref educPara, FieldTypeCD.FieldOfExpertise, education.FieldOfStudy);
            if (education.SchoolMonth > 0 && education.SchoolYear > 0)
                SaveOrUpdateDocData(ref educPara, FieldTypeCD.GraduationYear,
                    education.SchoolMonth + "/" + education.SchoolYear);
            else
                SaveOrUpdateDocData(ref educPara, FieldTypeCD.GraduationYear, string.Empty);
            SaveOrUpdateDocData(ref educPara, FieldTypeCD.FreeFormat, education.Description);
        }

        public static Education ToEducation(Paragraph expPara)
        {
            Education education = null;
            if (expPara != null)
            {
                education = new Education();
                education.ParagraphId = expPara.Id;
                education.ParaSortIndex = expPara.SortIndex;
                if (expPara.DocData != null)
                    foreach (var docData in expPara.DocData)
                        switch (docData.FieldCD)
                        {
                            case FieldTypeCD.SchoolName:
                                education.School = docData.CharValue;
                                break;
                            case FieldTypeCD.University:
                                education.University = docData.CharValue;
                                break;
                            case FieldTypeCD.SchoolCity:
                                education.City = docData.CharValue;
                                break;
                            case FieldTypeCD.SchoolState:
                                education.State = docData.CharValue;
                                break;
                            case FieldTypeCD.DegreeEarned:
                                education.Degree = docData.CharValue;
                                break;
                            case FieldTypeCD.DegreeSpecialization:
                                education.DegreeSpecialization = docData.CharValue;
                                break;
                            case FieldTypeCD.CGPA:
                                education.CGPA = docData.CharValue;
                                break;
                            //case FieldTypeCD.FieldOfExpertise: education.FieldOfStudy = docData.CharValue; break;
                            case FieldTypeCD.FreeFormat:
                                education.Description = docData.CharValue;
                                break;
                            case FieldTypeCD.GraduationYear:
                                if (!string.IsNullOrEmpty(docData.CharValue) && docData.CharValue.Contains("/"))
                                {
                                    int month, year;
                                    var dateArr = docData.CharValue.Split('/');
                                    if (int.TryParse(dateArr[0], out month) && int.TryParse(dateArr[1], out year) &&
                                        month > 0 && year > 0)
                                    {
                                        education.SchoolMonth = month;
                                        education.SchoolYear = year;
                                    }
                                }

                                break;
                        }
            }

            return education;
        }

        public static bool IsEducationEmpty(Education education)
        {
            return string.IsNullOrEmpty(education.School);
        }

        public static bool IsEducationEmpty(EducationDetails education)
        {
            if (education.Paragraphs != null)
            {
                var count = education.Paragraphs.Count();
                if (count > 1)
                    return false;
                if (count > 0)
                    return IsEducationEmpty(education.Paragraphs.FirstOrDefault());
            }

            return true;
        }

        public static EducationDetails ToEducationDetails(Section educSection)
        {
            EducationDetails educDetails = null;
            if (educSection != null)
            {
                educDetails = new EducationDetails();
                educDetails.SectionId = educSection.Id;
                educDetails.SortIndex = educSection.SortIndex;
                if (educSection.Paragraphs != null)
                    educDetails.Paragraphs = educSection.Paragraphs.OrderBy(ord => ord.SortIndex)
                        .Where(p => p.DocData != null).Select(p => ToEducation(p));
            }

            return educDetails;
        }

        public static void PopulateSkills(ref Paragraph expPara, SkillDescription skills)
        {
            SaveOrUpdateDocData(ref expPara, FieldTypeCD.SkillsCollection_1, skills.Description);
        }

        public static SkillDescription ToSkills(Section skillSection)
        {
            if (skillSection != null)
            {
                var skills = new SkillDescription();
                skills.SectionId = skillSection.Id;
                skills.SortIndex = skillSection.SortIndex;
                if (skillSection.Paragraphs != null)
                {
                    var skillPara = skillSection.Paragraphs.FirstOrDefault();
                    if (skillPara != null)
                    {
                        skills.ParagraphId = skillPara.Id;
                        skills.ParaSortIndex = skillPara.SortIndex;
                        if (skillPara.DocData != null)
                        {
                            var docData =
                                skillPara.DocData.FirstOrDefault(dd => dd.FieldCD == FieldTypeCD.SkillsCollection_1);
                            if (docData != null)
                                skills.Description = docData.CharValue;
                        }
                    }
                }

                return skills;
            }

            return null;
        }

        public static void PopulateSummary(ref Paragraph summaryPara, SummaryDescription summaryDesc)
        {
            SaveOrUpdateDocData(ref summaryPara, FieldTypeCD.FreeFormat, summaryDesc.Description);
        }

        public static SummaryDescription ToSummary(Section summarySection)
        {
            if (summarySection != null && summarySection.Paragraphs != null)
            {
                var summary = new SummaryDescription();
                summary.SectionId = summarySection.Id;
                summary.SortIndex = summarySection.SortIndex;
                if (summarySection.Paragraphs != null)
                {
                    var summaryPara = summarySection.Paragraphs.FirstOrDefault();
                    if (summaryPara != null)
                    {
                        summary.ParagraphId = summaryPara.Id;
                        summary.ParaSortIndex = summaryPara.SortIndex;
                        if (summaryPara.DocData != null)
                        {
                            var docData =
                                summaryPara.DocData.FirstOrDefault(dd => dd.FieldCD == FieldTypeCD.FreeFormat);
                            if (docData != null)
                                summary.Description = docData.CharValue;
                        }
                    }
                }

                return summary;
            }

            return null;
        }

        public static void PopulateOtherSectionDescription(ref Paragraph summaryPara, OtherSection summaryDesc)
        {
            SaveOrUpdateDocData(ref summaryPara, FieldTypeCD.Description, summaryDesc.Description);
        }

        public static OtherSection ToOtherSection(Section othrSec)
        {
            return ToOtherSection(othrSec, WizardUtility.GetWizardFromSectionCD(othrSec.SectionTypeCD));
        }

        public static OtherSection ToOtherSection(Section othrSec, string wizard)
        {
            OtherSection otherSection = null;
            if (othrSec != null)
            {
                otherSection = new OtherSection();
                otherSection.SectionId = othrSec.Id;
                otherSection.SortIndex = othrSec.SortIndex;
                otherSection.SectionLabel = othrSec.Label;
                otherSection.SectionTypeCD = othrSec.SectionTypeCD;

                if (othrSec.Paragraphs != null && othrSec.Paragraphs.Any())
                {
                    var para = othrSec.Paragraphs.FirstOrDefault();
                    otherSection.ParagraphId = para.Id;
                    otherSection.ParaSortIndex = para.SortIndex;
                    otherSection.Wizard = wizard;
                    if (para.DocData != null)
                    {
                        var docData = para.DocData.FirstOrDefault(dd => dd.FieldCD == FieldTypeCD.Description);
                        if (docData != null)
                            otherSection.Description = docData.CharValue;
                    }
                }
            }

            return otherSection;
        }

        private static void SaveOrUpdateDocData(ref Paragraph paragraph, string fieldCD, string charValue)
        {
            var docData = paragraph.DocData.FirstOrDefault(dd => dd.FieldCD == fieldCD);
            if (docData == null)
            {
                if (!string.IsNullOrEmpty(charValue))
                {
                    /// In case DocData is to be added, then add only when charValue is not null or empty.
                    docData = new DocData();
                    docData.FieldCD = fieldCD;
                    docData.CharValue = charValue;
                    paragraph.AddDocData(docData);
                }
            }
            else
            {
                docData.CharValue = charValue ?? string.Empty;
            }
        }

        public static void PopulateDate(ref Paragraph datePara, DateSectionModel dateSection)
        {
            SaveOrUpdateDocData(ref datePara, FieldTypeCD.FreeFormat, dateSection.Date);
        }

        public static void PopulateRecipient(ref Paragraph recipientPara, RecipientSectionModel recipientSection)
        {
            SaveOrUpdateDocData(ref recipientPara, FieldTypeCD.FirstName, recipientSection.FirstName);
            SaveOrUpdateDocData(ref recipientPara, FieldTypeCD.LastName, recipientSection.LastName);
            SaveOrUpdateDocData(ref recipientPara, FieldTypeCD.Company, recipientSection.CompanyName);
            SaveOrUpdateDocData(ref recipientPara, FieldTypeCD.Email, recipientSection.Email);
            SaveOrUpdateDocData(ref recipientPara, FieldTypeCD.Street, recipientSection.Address);
            SaveOrUpdateDocData(ref recipientPara, FieldTypeCD.City, recipientSection.City);
            SaveOrUpdateDocData(ref recipientPara, FieldTypeCD.State, recipientSection.State);
            SaveOrUpdateDocData(ref recipientPara, FieldTypeCD.ZipCode, recipientSection.ZipCode);
            SaveOrUpdateDocData(ref recipientPara, FieldTypeCD.OfficePhone, recipientSection.Phone);
        }

        public static RecipientInfo ToRecipientInfo(Section recipientSection, Section dateSection)
        {
            var recipientInfo = new RecipientInfo();
            if (recipientSection != null)
                recipientInfo.Recipient = ToRecipient(recipientSection);
            if (dateSection != null)
                recipientInfo.Date = ToDate(dateSection);
            return recipientInfo;
        }

        public static RecipientSectionModel ToRecipient(Section recipientSection)
        {
            RecipientSectionModel recipient = null;
            if (recipientSection != null)
            {
                recipient = new RecipientSectionModel();
                recipient.SectionId = recipientSection.Id;
                recipient.SortIndex = recipientSection.SortIndex;
                if (recipientSection.Paragraphs != null && recipientSection.Paragraphs.Count > 0)
                {
                    var recipientPara = recipientSection.Paragraphs[0];
                    if (recipientPara != null)
                    {
                        recipient.ParagraphId = recipientPara.Id;
                        recipient.ParaSortIndex = recipientPara.SortIndex;
                        if (recipientPara.DocData != null && recipientPara.DocData.Count > 0)
                            foreach (var docData in recipientPara.DocData)
                                switch (docData.FieldCD)
                                {
                                    case FieldTypeCD.FirstName:
                                        recipient.FirstName = docData.CharValue;
                                        break;
                                    case FieldTypeCD.LastName:
                                        recipient.LastName = docData.CharValue;
                                        break;
                                    case FieldTypeCD.Company:
                                        recipient.CompanyName = docData.CharValue;
                                        break;
                                    case FieldTypeCD.Email:
                                        recipient.Email = docData.CharValue;
                                        break;
                                    case FieldTypeCD.Street:
                                        recipient.Address = docData.CharValue;
                                        break;
                                    case FieldTypeCD.City:
                                        recipient.City = docData.CharValue;
                                        break;
                                    case FieldTypeCD.State:
                                        recipient.State = docData.CharValue;
                                        break;
                                    case FieldTypeCD.ZipCode:
                                        recipient.ZipCode = docData.CharValue;
                                        break;
                                    case FieldTypeCD.OfficePhone:
                                        recipient.Phone = docData.CharValue;
                                        break;
                                }
                    }
                }
            }

            return recipient;
        }

        public static DateSectionModel ToDate(Section nameSection)
        {
            DateSectionModel nameSectionModel = null;
            if (nameSection != null)
            {
                nameSectionModel = new DateSectionModel();
                nameSectionModel.SectionId = nameSection.Id;
                nameSectionModel.SortIndex = nameSection.SortIndex;
                if (nameSection.Paragraphs != null && nameSection.Paragraphs.Count > 0)
                {
                    var namePara = nameSection.Paragraphs.FirstOrDefault();
                    if (namePara != null)
                    {
                        nameSectionModel.ParagraphId = namePara.Id;
                        nameSectionModel.ParaSortIndex = namePara.SortIndex;
                        if (namePara.DocData != null && namePara.DocData.Count > 0)
                            foreach (var docData in namePara.DocData)
                                if (docData.FieldCD == FieldTypeCD.FreeFormat)
                                    nameSectionModel.Date = docData.CharValue;
                    }
                }
            }

            return nameSectionModel;
        }
    }
}