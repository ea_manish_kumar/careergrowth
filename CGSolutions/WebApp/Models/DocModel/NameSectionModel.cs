﻿using Newtonsoft.Json;

namespace WebApp.Models
{
    public class NameSectionModel : BaseParagraphModel
    {
        [JsonProperty(DocFields.Name)] public string Name { get; set; }
    }
}