﻿using System.Collections.Generic;
using Newtonsoft.Json;

namespace WebApp.Models
{
    public class BaseSectionModel
    {
        [JsonProperty(DocFields.SectionId)] public int SectionId { get; set; }

        [JsonProperty(DocFields.SortIndex)] public short SortIndex { get; set; }
    }

    public class BaseSectionSingleParaModel<TPara> : BaseSectionModel
        where TPara : BaseParagraphModel
    {
        [JsonProperty(SectionFields.Paragraph)]
        public TPara Paragraph { get; set; }
    }

    public class BaseSectionMultiParaModel<TPara> : BaseSectionModel
        where TPara : BaseParagraphModel
    {
        [JsonProperty(SectionFields.Paragraphs)]
        public IEnumerable<TPara> Paragraphs { get; set; }
    }
}