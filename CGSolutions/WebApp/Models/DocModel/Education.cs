﻿using Newtonsoft.Json;

namespace WebApp.Models
{
    public class Education : BaseParagraphModel
    {
        [JsonProperty(DocFields.School)] public string School { get; set; }

        [JsonProperty(DocFields.University)] public string University { get; set; }

        [JsonProperty(DocFields.SchoolCity)] public string City { get; set; }

        [JsonProperty(DocFields.SchoolState)] public string State { get; set; }

        [JsonProperty(DocFields.Degree)] public string Degree { get; set; }

        [JsonProperty(DocFields.DegreeSpecialization)]
        public string DegreeSpecialization { get; set; }

        [JsonProperty(DocFields.SchoolMonth)] public int SchoolMonth { get; set; }

        [JsonProperty(DocFields.SchoolYear)] public int SchoolYear { get; set; }

        [JsonProperty(DocFields.SchoolDescription)]
        public string Description { get; set; }

        [JsonProperty(DocFields.CGPA)] public string CGPA { get; set; }

        //[JsonProperty(DocFields.FieldOfStudy)]
        //public string FieldOfStudy { get; set; }
    }

    public class EducationDetails : BaseSectionMultiParaModel<Education>
    {
    }
}