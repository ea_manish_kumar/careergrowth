﻿using Newtonsoft.Json;

namespace WebApp.Models
{
    public class BaseDescription : BaseParagraphModel
    {
        [JsonProperty("sectionTypeCD", NullValueHandling = NullValueHandling.Ignore)]
        public virtual string SectionTypeCD { get; set; }

        [JsonProperty("description", NullValueHandling = NullValueHandling.Ignore)]
        public virtual string Description { get; set; }
    }
}