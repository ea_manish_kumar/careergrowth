﻿using System.Collections.Generic;
using Newtonsoft.Json;

namespace WebApp.Models
{
    public class DocumentProperties
    {
        [JsonProperty(DocProperties.AddressOrCityAndState)]
        public bool? AddressOrCityAndState { get; set; }

        [JsonProperty(DocProperties.CityOrState)]
        public bool? CityOrState { get; set; }

        [JsonProperty(DocProperties.ExpDate)] public IDictionary<int, string> ExpDate { get; set; }

        [JsonProperty(DocProperties.ExpCityOrState)]
        public IDictionary<int, string> ExpCityOrState { get; set; }

        [JsonProperty(DocProperties.EducSchoolYear)]
        public IDictionary<int, string> EducSchoolYear { get; set; }

        [JsonProperty(DocProperties.FirstName)]
        public string FirstName { get; set; }

        [JsonProperty(DocProperties.LastName)] public string LastName { get; set; }
    }
}