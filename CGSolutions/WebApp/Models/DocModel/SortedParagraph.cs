﻿using Newtonsoft.Json;

namespace WebApp.Models
{
    public class SortedParagraph
    {
        [JsonProperty(DocFields.ParagraphId)] public int ParagraphId { get; set; }

        [JsonProperty("sortIndex")] public short SortIndex { get; set; }
    }
}