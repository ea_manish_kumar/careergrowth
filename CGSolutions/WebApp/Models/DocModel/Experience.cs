﻿using Newtonsoft.Json;

namespace WebApp.Models
{
    public class Experience : BaseParagraphModel
    {
        [JsonProperty(DocFields.Employer)] public string Employer { get; set; }

        [JsonProperty(DocFields.City)] public string City { get; set; }

        [JsonProperty(DocFields.State)] public string State { get; set; }

        [JsonProperty(DocFields.JobTitle)] public string JobTitle { get; set; }

        [JsonProperty(DocFields.StartDateMonth)]
        public int StartDateMonth { get; set; }

        [JsonProperty(DocFields.StartDateYear)]
        public int StartDateYear { get; set; }

        [JsonProperty(DocFields.EndDateMonth)] public int EndDateMonth { get; set; }

        [JsonProperty(DocFields.EndDateYear)] public int EndDateYear { get; set; }

        [JsonProperty(DocFields.PresentlyWorkHere)]
        public bool PresentlyWorkHere { get; set; }

        [JsonProperty(DocFields.JobDescription)]
        public string Description { get; set; }
    }

    public class ExperienceSection : BaseSectionSingleParaModel<Experience>
    {
    }

    public class ExperienceDetails : BaseSectionMultiParaModel<Experience>
    {
    }
}