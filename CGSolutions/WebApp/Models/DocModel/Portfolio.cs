﻿using Newtonsoft.Json;

namespace WebApp.Models
{
    public class Portfolio : BaseSectionModel
    {
        [JsonProperty(DocFields.LinkedIn)] public string LinkedIn { get; set; }

        [JsonProperty(DocFields.Website)] public string Website { get; set; }

        [JsonProperty(DocFields.Twitter)] public string Twitter { get; set; }

        [JsonProperty(DocFields.GooglePlus)] public string GooglePlus { get; set; }
    }
}