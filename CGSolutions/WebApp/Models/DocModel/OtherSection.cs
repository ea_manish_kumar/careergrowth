﻿using Newtonsoft.Json;

namespace WebApp.Models
{
    public class OtherSection : BaseDescription
    {
        [JsonProperty("wizard", NullValueHandling = NullValueHandling.Ignore)]
        public string Wizard { get; set; }

        [JsonProperty("sectionLabel", NullValueHandling = NullValueHandling.Ignore)]
        public string SectionLabel { get; set; }
    }
}