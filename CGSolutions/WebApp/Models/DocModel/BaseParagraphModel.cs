﻿using Newtonsoft.Json;

namespace WebApp.Models
{
    public class BaseParagraphModel : BaseSectionModel
    {
        [JsonProperty(DocFields.ParagraphId)] public int ParagraphId { get; set; }

        [JsonProperty(DocFields.ParaSortIndex)]
        public short ParaSortIndex { get; set; }
    }

    public class BaseSingleParagraphModel : BaseSectionModel
    {
        [JsonProperty(DocFields.ParagraphId)] public int ParagraphId { get; set; }

        [JsonProperty(DocFields.ParaSortIndex)]
        public short ParaSortIndex { get; set; }
    }
}