﻿using System;
using System.Collections.Generic;
using Newtonsoft.Json;
using Utilities.Constants.Documents;
using WebApp.Components;

namespace WebApp.Models
{
    public class DocumentModel
    {
        private readonly IDictionary<string, string> _styles;

        public DocumentModel()
        {
            Properties = new DocumentProperties();
            _styles = new Dictionary<string, string>();
        }

        public int DocumentId { get; set; }

        public DateTime TimeStamp { get; set; }

        [JsonProperty(DocSections.Name)] public NameSectionModel Name { get; set; }

        [JsonProperty(DocSections.Contact)] public ContactSectionModel Contact { get; set; }

        [JsonProperty(DocSections.Experience)] public ExperienceDetails Experience { get; set; }

        [JsonProperty(DocSections.Training)] public ExperienceDetails Training { get; set; }

        [JsonProperty(DocSections.Education)] public EducationDetails Education { get; set; }

        [JsonProperty(DocSections.Skills)] public SkillDescription Skills { get; set; }

        [JsonProperty("portfolio")] public Portfolio Portfolio { get; set; }

        [JsonProperty("summary")] public SummaryDescription Summary { get; set; }


        [JsonProperty("accomplishments")] public OtherSection Accomplishments { get; set; }

        [JsonProperty("additionalInformation")]
        public OtherSection AdditionalInformation { get; set; }

        [JsonProperty("affiliations")] public OtherSection Affiliations { get; set; }

        [JsonProperty("awards")] public OtherSection Awards { get; set; }

        [JsonProperty("certifications")] public OtherSection Certifications { get; set; }

        [JsonProperty("dissertation")] public OtherSection Dissertation { get; set; }

        [JsonProperty("extraCurricular")] public OtherSection ExtraCurricular { get; set; }

        [JsonProperty("interests")] public OtherSection Interests { get; set; }

        [JsonProperty("language")] public OtherSection Language { get; set; }

        [JsonProperty("other")] public OtherSection Other { get; set; }

        [JsonProperty("personalInformation")] public OtherSection PersonalInformation { get; set; }

        [JsonProperty("presentations")] public OtherSection Presentations { get; set; }

        [JsonProperty("publication")] public OtherSection Publication { get; set; }

        [JsonProperty("references")] public OtherSection References { get; set; }

        [JsonProperty("communityService")] public OtherSection SocialService { get; set; }


        [JsonProperty("custom")] public IEnumerable<OtherSection> CustomSections { get; set; }


        [JsonProperty("properties")] public DocumentProperties Properties { get; set; }

        public string GetStyles(string key, string defaultValue)
        {
            double fontSize = 14;
            double iconsWidth = 21;
            string styleValue;
            if (!_styles.TryGetValue(key, out styleValue))
                styleValue = defaultValue;

            switch (key)
            {
                case Styles.BACKGROUNDGRAPHIC:
                    if (!string.IsNullOrEmpty(styleValue))
                        return string.Concat(StaticResources.CDN_Path1, styleValue);
                    else
                        return string.Empty;
                case Styles.FONTSIZEADDRESS:
                case Styles.FONTSIZENAME:
                case Styles.FONTSIZEH1:
                case Styles.FONTSIZEH2:
                    var zoomFactor = CalculateZoomFactor(Styles.FONTSIZE);
                    double.TryParse(styleValue.Replace("px", string.Empty), out fontSize);
                    return string.Format("{0}px", fontSize * zoomFactor);
                case Styles.FAICONSWIDTH:
                    double.TryParse(GetStyles(Styles.FONTSIZE, defaultValue).Replace("px", string.Empty),
                        out iconsWidth);
                    return string.Format("{0}px", iconsWidth * 1.5);
                default:
                    return styleValue;
            }
        }

        private double CalculateZoomFactor(string key)
        {
            var zoomFactor = 1.0;
            string zoomStyleValue;
            if (_styles.TryGetValue(key, out zoomStyleValue))
                switch (zoomStyleValue)
                {
                    case "10px":
                        zoomFactor = 0.8;
                        break;
                    case "11px":
                        zoomFactor = 0.9;
                        break;
                    case "13px":
                        zoomFactor = 1.1;
                        break;
                    case "14px":
                        zoomFactor = 1.2;
                        break;
                    default:
                        zoomFactor = 1.0;
                        break;
                }

            return zoomFactor;
        }

        public void AddStyles(string key, string value)
        {
            _styles.Add(key, value);
        }
    }
}