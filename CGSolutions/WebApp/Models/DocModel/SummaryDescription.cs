﻿using Newtonsoft.Json;

namespace WebApp.Models
{
    public class SummaryDescription : BaseParagraphModel
    {
        [JsonProperty("description")] public virtual string Description { get; set; }
    }
}