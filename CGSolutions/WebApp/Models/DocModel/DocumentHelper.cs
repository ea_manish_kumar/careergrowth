﻿using System;
using System.Collections.Generic;
using System.Linq;
using Resources;
using Utilities.Constants.Documents;
using WebApp.Constants;

namespace WebApp.Models
{
    public static class DocumentHelper
    {
        public static void EvaluateDocumentProperties(DocumentModel model)
        {
            if (model != null)
            {
                if (model.Contact != null)
                {
                    var contact = model.Contact;
                    model.Properties.AddressOrCityAndState =
                        !string.IsNullOrEmpty(contact.Address) &&
                        (!string.IsNullOrEmpty(contact.City) || !string.IsNullOrEmpty(contact.State));
                    model.Properties.CityOrState =
                        !string.IsNullOrEmpty(contact.City) || !string.IsNullOrEmpty(contact.State);
                }

                if (model.Name != null)
                {
                    var fullName = model.Name.Name;
                    if (!string.IsNullOrEmpty(fullName))
                    {
                        var nameDetails = fullName.Split(AppConstants.Space.ToCharArray(),
                            StringSplitOptions.RemoveEmptyEntries);
                        if (nameDetails != null && nameDetails.Length >= 2)
                        {
                            var lastName = nameDetails.Last();
                            model.Properties.LastName = lastName;
                            model.Properties.FirstName = fullName.Substring(0, fullName.Length - lastName.Length);
                        }
                    }
                }

                if (model.Experience != null && model.Experience.Paragraphs != null)
                {
                    var index = 0;
                    model.Properties.ExpDate = new Dictionary<int, string>();
                    model.Properties.ExpCityOrState = new Dictionary<int, string>();
                    foreach (var expPara in model.Experience.Paragraphs)
                    {
                        string dateValue = null;
                        if (HasDate(expPara.StartDateMonth, expPara.StartDateYear) &&
                            (expPara.PresentlyWorkHere || HasDate(expPara.EndDateMonth, expPara.EndDateYear)))
                            dateValue = expPara.StartDateYear + " - " + (expPara.PresentlyWorkHere
                                            ? Resource.Present
                                            : expPara.EndDateYear.ToString());
                        model.Properties.ExpDate.Add(index, dateValue);

                        string cityStateValue = null;
                        bool hasCity = HasValue(expPara.City), hasState = HasValue(expPara.State);
                        if (hasCity && hasState)
                            cityStateValue = expPara.City + ", " + expPara.State;
                        else if (hasCity)
                            cityStateValue = expPara.City;
                        else if (hasState)
                            cityStateValue = expPara.State;
                        model.Properties.ExpCityOrState.Add(index, cityStateValue);

                        index++;
                    }
                }

                if (model.Education != null && model.Education.Paragraphs != null)
                {
                    var index = 0;
                    model.Properties.EducSchoolYear = new Dictionary<int, string>();
                    foreach (var educPara in model.Education.Paragraphs)
                    {
                        string schoolYear = null;
                        if (HasValue(educPara.School))
                            schoolYear = educPara.School + (HasDate(educPara.SchoolMonth, educPara.SchoolYear)
                                             ? " (" + educPara.SchoolYear + ")"
                                             : string.Empty);
                        model.Properties.EducSchoolYear.Add(index, schoolYear);

                        index++;
                    }
                }
            }
        }

        public static bool HasDate(int month, int year)
        {
            return month > 0 && month <= 12 && year > 0;
        }

        public static bool HasValue(string value)
        {
            return value != null;
        }

        public static bool HasValue(bool? value)
        {
            return value.HasValue && value.Value;
        }

        public static IEnumerable<string> GetSortedSections(DocumentModel model)
        {
            IDictionary<string, int> sortedSectionTypeCDs = new Dictionary<string, int>();
            if (model.Accomplishments != null)
                sortedSectionTypeCDs.Add(SectionTypeCD.Accomplishments, model.Accomplishments.SortIndex);
            if (model.AdditionalInformation != null)
                sortedSectionTypeCDs.Add(SectionTypeCD.Additional_Information, model.AdditionalInformation.SortIndex);
            if (model.Affiliations != null)
                sortedSectionTypeCDs.Add(SectionTypeCD.Affiliations, model.Affiliations.SortIndex);
            if (model.Awards != null) sortedSectionTypeCDs.Add(SectionTypeCD.Awards, model.Awards.SortIndex);
            if (model.Certifications != null)
                sortedSectionTypeCDs.Add(SectionTypeCD.Certifications, model.Certifications.SortIndex);
            if (model.Contact != null) sortedSectionTypeCDs.Add(SectionTypeCD.Contact, model.Contact.SortIndex);
            if (model.Dissertation != null)
                sortedSectionTypeCDs.Add(SectionTypeCD.Dissertation, model.Dissertation.SortIndex);
            if (model.Education != null) sortedSectionTypeCDs.Add(SectionTypeCD.Education, model.Education.SortIndex);
            if (model.ExtraCurricular != null)
                sortedSectionTypeCDs.Add(SectionTypeCD.ExtraCurricular, model.ExtraCurricular.SortIndex);
            if (model.Experience != null)
                sortedSectionTypeCDs.Add(SectionTypeCD.Experience, model.Experience.SortIndex);
            if (model.Skills != null) sortedSectionTypeCDs.Add(SectionTypeCD.Skills, model.Skills.SortIndex);
            if (model.Interests != null) sortedSectionTypeCDs.Add(SectionTypeCD.Interests, model.Interests.SortIndex);
            if (model.Language != null) sortedSectionTypeCDs.Add(SectionTypeCD.Language, model.Language.SortIndex);
            if (model.Name != null) sortedSectionTypeCDs.Add(SectionTypeCD.Name, model.Name.SortIndex);
            if (model.Other != null) sortedSectionTypeCDs.Add(SectionTypeCD.Other, model.Other.SortIndex);
            if (model.PersonalInformation != null)
                sortedSectionTypeCDs.Add(SectionTypeCD.PersonalInformation, model.PersonalInformation.SortIndex);
            if (model.Presentations != null)
                sortedSectionTypeCDs.Add(SectionTypeCD.Presentations, model.Presentations.SortIndex);
            if (model.Portfolio != null) sortedSectionTypeCDs.Add(SectionTypeCD.Portfolio, model.Portfolio.SortIndex);
            if (model.Publication != null)
                sortedSectionTypeCDs.Add(SectionTypeCD.Publication, model.Publication.SortIndex);
            if (model.References != null)
                sortedSectionTypeCDs.Add(SectionTypeCD.References, model.References.SortIndex);
            if (model.Summary != null) sortedSectionTypeCDs.Add(SectionTypeCD.Summary, model.Summary.SortIndex);
            if (model.Training != null) sortedSectionTypeCDs.Add(SectionTypeCD.Training, model.Training.SortIndex);
            if (model.ExtraCurricular != null)
                sortedSectionTypeCDs.Add(SectionTypeCD.Volunteer, model.ExtraCurricular.SortIndex);
            if (model.SocialService != null)
                sortedSectionTypeCDs.Add(SectionTypeCD.SocialService, model.SocialService.SortIndex);
            return sortedSectionTypeCDs.OrderBy(o => o.Value).Select(s => s.Key);
        }

        public static SectionProperties GetSectionProperties(string sectionTypeCD)
        {
            var prop = new SectionProperties();
            switch (sectionTypeCD)
            {
                case SectionTypeCD.Accomplishments:
                    prop.JsonProperty = "accomplishments";
                    prop.Icon = "flag";
                    break;
                case SectionTypeCD.Affiliations:
                    prop.JsonProperty = "affiliations";
                    prop.Icon = "houzz";
                    break;
                case SectionTypeCD.Awards:
                    prop.JsonProperty = "awards";
                    prop.Icon = "trophy";
                    break;
                case SectionTypeCD.Certifications:
                    prop.JsonProperty = "certifications";
                    prop.Icon = "certificate";
                    break;
                case SectionTypeCD.Contact:
                    prop.JsonProperty = "contact";
                    prop.Icon = "contact";
                    break;
                case SectionTypeCD.Dissertation:
                    prop.JsonProperty = "dissertation";
                    prop.Icon = "flask";
                    break;
                case SectionTypeCD.Education:
                    prop.JsonProperty = "education";
                    prop.Icon = "education";
                    break;
                case SectionTypeCD.ExtraCurricular:
                    prop.JsonProperty = "extracurricular";
                    prop.Icon = "gamepad";
                    break;
                case SectionTypeCD.Experience:
                    prop.JsonProperty = "experience";
                    prop.Icon = "experience";
                    break;
                case SectionTypeCD.Skills:
                    prop.JsonProperty = "skills";
                    prop.Icon = "skills";
                    break;
                case SectionTypeCD.Interests:
                    prop.JsonProperty = "interests";
                    prop.Icon = "futbol-o";
                    break;
                case SectionTypeCD.Language:
                    prop.JsonProperty = "language";
                    prop.Icon = "language";
                    break;
                case SectionTypeCD.Name:
                    prop.JsonProperty = "name";
                    prop.Icon = "name";
                    break;
                case SectionTypeCD.Other:
                    prop.JsonProperty = "other";
                    prop.Icon = "pencil";
                    break;
                case SectionTypeCD.PersonalInformation:
                    prop.JsonProperty = "personalinformation";
                    prop.Icon = "address-card";
                    break;
                case SectionTypeCD.Presentations:
                    prop.JsonProperty = "presentations";
                    prop.Icon = "file-text";
                    break;
                case SectionTypeCD.Portfolio:
                    prop.JsonProperty = "portfolio";
                    prop.Icon = "star";
                    break;
                case SectionTypeCD.Publication:
                    prop.JsonProperty = "publication";
                    prop.Icon = "newspaper-o";
                    break;
                case SectionTypeCD.References:
                    prop.JsonProperty = "references";
                    prop.Icon = "group";
                    break;
                case SectionTypeCD.SocialService:
                    prop.JsonProperty = "socialService";
                    prop.Icon = "handshake-o";
                    break;
            }

            return prop;
        }
    }
}