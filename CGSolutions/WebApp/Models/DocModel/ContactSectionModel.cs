﻿using System.ComponentModel.DataAnnotations;
using Newtonsoft.Json;
using Utilities.Constants;

namespace WebApp.Models
{
    public class ContactSectionModel : BaseParagraphModel
    {
        [JsonProperty(DocFields.Address)] public string Address { get; set; }

        [JsonProperty(DocFields.City)] public string City { get; set; }

        [JsonProperty(DocFields.State)] public string State { get; set; }

        [JsonProperty(DocFields.ZipCode)] public string ZipCode { get; set; }

        [RegularExpression(RegexPatterns.Email)]
        [JsonProperty(DocFields.Email)]
        public string Email { get; set; }

        [JsonProperty(DocFields.Phone)] public string Phone { get; set; }

        [JsonProperty(DocFields.AltPhone)] public string AltPhone { get; set; }
    }
}