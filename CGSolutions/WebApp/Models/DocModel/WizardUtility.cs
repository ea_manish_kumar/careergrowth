﻿using DomainModel;
using Resources;
using Services.Documents;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.Mvc;
using Utilities.Constants.Documents;
using WebApp.Constants;
using WebApp.Controllers;
using WebApp.Extensions;

namespace WebApp.Models
{
    public class WizardUtility
    {
        #region Wizard Routing

        private static readonly IList<string> WizardMenuPages = new List<string>
        {
            WizardConstants.ChooseTemplate, WizardConstants.Contact, WizardConstants.Experience,
            WizardConstants.Education, WizardConstants.Skills, WizardConstants.Summary
        };

        private static readonly IList<string> WizardPages = new List<string>
        {
            WizardConstants.ChooseTemplate, WizardConstants.Contact, WizardConstants.Experience, WizardConstants.Education, WizardConstants.Skills, WizardConstants.Summary
        };

        private static readonly IList<string> AllWizardPages = new List<string>
        {
            WizardConstants.ChooseTemplate, WizardConstants.Contact, WizardConstants.ExtraCurricular, WizardConstants.Experience,
            WizardConstants.Education, WizardConstants.Skills, WizardConstants.Summary, WizardConstants.Language,
            WizardConstants.Accomplishments, WizardConstants.Awards, WizardConstants.Certifications,
            WizardConstants.PersonalInformation, WizardConstants.References/*, WizardConstants.SocialService*/
        };

        public static WizardMenu GetWizard(UrlHelper Url, string wizard)
        {
            return GetWizard(Url, wizard, null, null, null, null);
        }

        public static WizardMenu GetWizard(UrlHelper Url, string wizard, DocumentCookieModel docCookie)
        {
            return GetWizard(Url, wizard, null, null, null, docCookie);
        }

        public static WizardMenu GetWizard(UrlHelper Url, string wizard, string mode, int? documentId, int? sectionId,
            DocumentCookieModel docCookie)
        {
            var wizardMenu = new WizardMenu();
            wizardMenu.SectionCode = wizard;
            if (sectionId.HasValue && sectionId.Value > 0)
                wizardMenu.SectionId = sectionId.Value;
            if (RouteReviewMode.EditDocAddPara.Equals(mode) || RouteReviewMode.EditDocEditPara.Equals(mode))
                mode = RouteMode.Edit;
            else if (RouteReviewMode.AddDocAddPara.Equals(mode) || RouteReviewMode.AddDocEditPara.Equals(mode))
                mode = RouteMode.Add;
            switch (wizard)
            {
                case WizardConstants.HowItWorks:
                    wizardMenu.SectionURL =
                        Url.BuildUrl<WizardController>(wiz => wiz.HowItWorks(), RouteNames.HowItWorks);
                    break;
                case WizardConstants.ChooseTemplate:
                    wizardMenu.SectionDisplayValue = "Select Template";
                    wizardMenu.SectionURL = Url.BuildUrl<WizardController>(wiz => wiz.ChooseTemplate(mode, documentId),
                        RouteNames.ChooseTemplate);
                    wizardMenu.Icon = "icon-SelectTemplate";
                    break;
                case WizardConstants.Contact:
                    wizardMenu.SectionURL = Url.BuildUrl<WizardController>(wiz => wiz.Contact(mode, documentId),
                        mode != null ? RouteNames.Contact_Mode : RouteNames.Contact);
                    wizardMenu.SectionDisplayValue = Resource.ResumeHeading;
                    wizardMenu.Icon = "icon-Contact";
                    break;
                case WizardConstants.Experience:
                    string expUrl = null;
                    if (docCookie != null && docCookie.HasSectionPara(WizardConstants.Experience))
                        expUrl = Url.BuildUrl<WizardController>(wiz => wiz.ExperienceReview(mode, documentId),
                            mode != null ? RouteNames.ExperienceReview_Mode : RouteNames.ExperienceReview);
                    else
                        expUrl = Url.BuildUrl<WizardController>(wiz => wiz.Experience(mode, documentId, null),
                            mode != null ? RouteNames.Experience_Mode : RouteNames.Experience);
                    wizardMenu.SectionURL = expUrl;
                    wizardMenu.SectionDisplayValue = Resource.EXPR;
                    wizardMenu.Icon = "icon-Experience";
                    break;
                case WizardConstants.Education:
                    string eduUrl = null;
                    if (docCookie != null && docCookie.HasSectionPara(WizardConstants.Education))
                        eduUrl = Url.BuildUrl<WizardController>(wiz => wiz.EducationReview(mode, documentId),
                            mode != null ? RouteNames.EducationReview_Mode : RouteNames.EducationReview);
                    else
                        eduUrl = Url.BuildUrl<WizardController>(wiz => wiz.Education(mode, documentId, null),
                            mode != null ? RouteNames.Education_Mode : RouteNames.Education);
                    wizardMenu.SectionURL = eduUrl;
                    wizardMenu.SectionDisplayValue = Resource.EDUC;
                    wizardMenu.Icon = "icon-Education";
                    break;
                case WizardConstants.Skills:
                    wizardMenu.SectionURL = Url.BuildUrl<WizardController>(
                        wiz => wiz.SkillDescription(mode, documentId),
                        mode != null ? RouteNames.SkillDescription_Mode : RouteNames.SkillDescription);
                    wizardMenu.SectionDisplayValue = Resource.HILT;
                    wizardMenu.Icon = "icon-Skill";
                    break;
                case WizardConstants.Summary:
                    wizardMenu.SectionURL = Url.BuildUrl<WizardController>(
                        wiz => wiz.SummaryDescription(mode, documentId),
                        mode != null ? RouteNames.SummaryDescription_Mode : RouteNames.SummaryDescription);
                    wizardMenu.SectionDisplayValue = Resource.SUMM;
                    wizardMenu.Icon = "icon-SummaryTemplate";
                    break;
                case WizardConstants.Accomplishments:
                    wizardMenu.SectionURL = Url.BuildUrl<WizardController>(
                        wiz => wiz.OtherSection(mode, documentId, OtherSectionUrls.Accomplishments, null),
                        RouteNames.OtherSection_Mode);
                    wizardMenu.SectionDisplayValue = Resource.ACCM;
                    wizardMenu.Icon = "icon-Accomplishments";
                    break;
                case WizardConstants.Awards:
                    wizardMenu.SectionURL = Url.BuildUrl<WizardController>(
                        wiz => wiz.OtherSection(mode, documentId, OtherSectionUrls.Awards, null),
                        RouteNames.OtherSection_Mode);
                    wizardMenu.SectionDisplayValue = Resource.AWAR;
                    wizardMenu.Icon = "icon-Awards";
                    break;
                case WizardConstants.Certifications:
                    wizardMenu.SectionURL = Url.BuildUrl<WizardController>(
                        wiz => wiz.OtherSection(mode, documentId, OtherSectionUrls.Certifications, null),
                        RouteNames.OtherSection_Mode);
                    wizardMenu.SectionDisplayValue = Resource.CERT;
                    wizardMenu.Icon = "icon-Certifications";
                    break;
                case WizardConstants.ExtraCurricular:
                    wizardMenu.SectionURL = Url.BuildUrl<WizardController>(
                        wiz => wiz.OtherSection(mode, documentId, OtherSectionUrls.ExtraCurricular, null),
                        RouteNames.OtherSection_Mode);
                    wizardMenu.SectionDisplayValue = Resource.EXCL;
                    wizardMenu.Icon = "icon-ExtraCurricular";
                    break;
                case WizardConstants.Language:
                    wizardMenu.SectionURL = Url.BuildUrl<WizardController>(
                        wiz => wiz.OtherSection(mode, documentId, OtherSectionUrls.Language, null),
                        RouteNames.OtherSection_Mode);
                    wizardMenu.SectionDisplayValue = Resource.LANG;
                    wizardMenu.Icon = "icon-Language";
                    break;
                case WizardConstants.PersonalInformation:
                    wizardMenu.SectionURL = Url.BuildUrl<WizardController>(
                        wiz => wiz.OtherSection(mode, documentId, OtherSectionUrls.PersonalInformation, null),
                        RouteNames.OtherSection_Mode);
                    wizardMenu.SectionDisplayValue = Resource.PRIN;
                    wizardMenu.Icon = "icon-PersonalInformation";
                    break;
                case WizardConstants.References:
                    wizardMenu.SectionURL = Url.BuildUrl<WizardController>(
                        wiz => wiz.OtherSection(mode, documentId, OtherSectionUrls.References, null),
                        RouteNames.OtherSection_Mode);
                    wizardMenu.SectionDisplayValue = Resource.REFE;
                    wizardMenu.Icon = "icon-References";
                    break;
                case WizardConstants.SocialService:
                    wizardMenu.SectionURL = Url.BuildUrl<WizardController>(
                        wiz => wiz.OtherSection(mode, documentId, OtherSectionUrls.SocialService, null),
                        RouteNames.OtherSection_Mode);
                    wizardMenu.SectionDisplayValue = Resource.SCSV;
                    wizardMenu.Icon = "icon-SocialServices";
                    break;
                default:
                    //TODO: Manish to check if additional custom section logic has been handled or not.
                    wizardMenu.Icon = "icon-Others";
                    break;
            }

            return wizardMenu;
        }

        public static IList<WizardMenu> GetWizardMenuList(UrlHelper Url, DocumentCookieModel docCookie)
        {
            IList<WizardMenu> menuList = new List<WizardMenu>();
            foreach (var wizard in WizardMenuPages)
                menuList.Add(GetWizard(Url, wizard, null, null, null, docCookie));
            return menuList;
        }

        public static IList<WizardMenu> GetWizardMenuList(UrlHelper Url, string mode, int? documentId,
            DocumentCookieModel docCookie)
        {
            string _mode = null;
            int? _docId = null;
            IList<string> wizardList = null;
            if (IsEditMode(mode))
            {
                _mode = RouteMode.Edit;
                _docId = documentId;
                wizardList = docCookie.GetSections();
            }
            else if (IsAddMode(mode))
            {
                _mode = RouteMode.Add;
                _docId = documentId;
                wizardList = docCookie.GetAdditionalSections();
            }
            else
            {
                wizardList = WizardMenuPages;
            }

            if (wizardList != null)
            {
                IList<WizardMenu> menuList = new List<WizardMenu>();
                foreach (var wizard in wizardList)
                    menuList.Add(GetWizard(Url, wizard, _mode, _docId, null, docCookie));
                return menuList;
            }

            return null;
        }

        public static IList<WizardMenu> GetWizardMenuList(UrlHelper Url, string mode, Document document,
            DocumentCookieModel docCookie)
        {
            var sections = document.Sections;
            if (sections != null)
            {
                IList<WizardMenu> menuList = new List<WizardMenu>();
                var isContactAdded = false;
                foreach (var section in sections)
                {
                    if (SectionTypeCD.Name.Equals(section.SectionTypeCD) ||
                        SectionTypeCD.Contact.Equals(section.SectionTypeCD))
                    {
                        if (isContactAdded)
                            continue;
                        isContactAdded = true;
                    }

                    menuList.Add(GetWizard(Url, GetWizardFromSectionCD(section.SectionTypeCD), mode, document.Id,
                        section.Id, docCookie));
                }

                return menuList;
            }

            return null;
        }

        public static IList<WizardMenu> GetMissingMenuList(UrlHelper Url, string mode, int? documentId,
            DocumentCookieModel docCookie, IEnumerable<string> existingWizardCDs)
        {
            IList<WizardMenu> menuList = new List<WizardMenu>();
            if (existingWizardCDs != null && existingWizardCDs.Any())
                foreach (var wizard in AllWizardPages.Where(wiz => !existingWizardCDs.Contains(wiz)))
                    menuList.Add(GetWizard(Url, wizard, null, null, null, docCookie));
            return menuList;
        }

        public static IList<string> GetWizardList()
        {
            return WizardPages;
        }

        public static string GetWizardFromSectionCD(string sectionTypeCD)
        {
            switch (sectionTypeCD)
            {
                case SectionTypeCD.Accomplishments: return WizardConstants.Accomplishments;
                case SectionTypeCD.Awards: return WizardConstants.Awards;
                case SectionTypeCD.Certifications: return WizardConstants.Certifications;
                case SectionTypeCD.Name: return WizardConstants.Contact;
                case SectionTypeCD.Contact: return WizardConstants.Contact;
                case SectionTypeCD.Education: return WizardConstants.Education;
                case SectionTypeCD.ExtraCurricular: return WizardConstants.ExtraCurricular;
                case SectionTypeCD.Experience: return WizardConstants.Experience;
                case SectionTypeCD.Skills: return WizardConstants.Skills;
                case SectionTypeCD.Summary: return WizardConstants.Summary;
                case SectionTypeCD.Language: return WizardConstants.Language;
                case SectionTypeCD.PersonalInformation: return WizardConstants.PersonalInformation;
                case SectionTypeCD.References: return WizardConstants.References;
                case SectionTypeCD.SocialService: return WizardConstants.SocialService;
                case SectionTypeCD.Other: return WizardConstants.Other;
            }

            return null;
        }

        public static string GetSectionCDFromWizard(string wizard)
        {
            switch (wizard)
            {
                case WizardConstants.Accomplishments: return SectionTypeCD.Accomplishments;
                case WizardConstants.Awards: return SectionTypeCD.Awards;
                case WizardConstants.Certifications: return SectionTypeCD.Certifications;
                case WizardConstants.Contact: return SectionTypeCD.Contact;
                case WizardConstants.Education: return SectionTypeCD.Education;
                case WizardConstants.ExtraCurricular: return SectionTypeCD.ExtraCurricular;
                case WizardConstants.Experience: return SectionTypeCD.Experience;
                case WizardConstants.Skills: return SectionTypeCD.Skills;
                case WizardConstants.Summary: return SectionTypeCD.Summary;
                case WizardConstants.Language: return SectionTypeCD.Language;
                case WizardConstants.PersonalInformation: return SectionTypeCD.PersonalInformation;
                case WizardConstants.References: return SectionTypeCD.References;
                case WizardConstants.SocialService: return SectionTypeCD.SocialService;
            }

            return null;
        }

        public static IList<string> GetSectionCDsFromWizards(IList<string> wizardCDs)
        {
            if (wizardCDs != null && wizardCDs.Any())
            {
                IList<string> sectionCDs = new List<string>();
                foreach (var wizardCD in wizardCDs)
                    switch (wizardCD)
                    {
                        case WizardConstants.Accomplishments:
                            sectionCDs.Add(SectionTypeCD.Accomplishments);
                            break;
                        case WizardConstants.Awards:
                            sectionCDs.Add(SectionTypeCD.Awards);
                            break;
                        case WizardConstants.Certifications:
                            sectionCDs.Add(SectionTypeCD.Certifications);
                            break;
                        case WizardConstants.Contact:
                            sectionCDs.Add(SectionTypeCD.Name);
                            sectionCDs.Add(SectionTypeCD.Contact);
                            break;
                        case WizardConstants.Education:
                            sectionCDs.Add(SectionTypeCD.Education);
                            break;
                        case WizardConstants.ExtraCurricular:
                            sectionCDs.Add(SectionTypeCD.ExtraCurricular);
                            break;
                        case WizardConstants.Experience:
                            sectionCDs.Add(SectionTypeCD.Experience);
                            break;
                        case WizardConstants.Skills:
                            sectionCDs.Add(SectionTypeCD.Skills);
                            break;
                        case WizardConstants.Summary:
                            sectionCDs.Add(SectionTypeCD.Summary);
                            break;
                        case WizardConstants.Language:
                            sectionCDs.Add(SectionTypeCD.Language);
                            break;
                        case WizardConstants.PersonalInformation:
                            sectionCDs.Add(SectionTypeCD.PersonalInformation);
                            break;
                        case WizardConstants.References:
                            sectionCDs.Add(SectionTypeCD.References);
                            break;
                        case WizardConstants.SocialService:
                            sectionCDs.Add(SectionTypeCD.SocialService);
                            break;
                    }

                return sectionCDs;
            }

            return null;
        }

        #region Funnel Routing

        public static void SetFunnelRoutes(UrlHelper Url, WizardVM model, string wizard)
        {
            if (IsEditMode(model.Mode))
            {
                model.BackRouteUrl = model.NextRouteUrl = Url.RouteUrl(RouteNames.ResumeFinalize, new { id = model.DocumentId });
            }
            else
            {
                string mode = null;
                int? docId = null;
                if (IsAddMode(model.Mode))
                {
                    mode = RouteMode.Add;
                    docId = model.DocumentId;
                }

                IList<string> wizardList = null;
                if (RouteMode.Add.Equals(model.Mode, StringComparison.OrdinalIgnoreCase) &&
                    model.DocumentCookieModel != null)
                    wizardList = model.DocumentCookieModel.GetAdditionalSections();
                else
                    wizardList = GetWizardList();
                var index = wizardList.IndexOf(wizard);
                if (index > 0)
                {
                    var previousWizard = wizardList[index - 1];
                    model.BackRouteUrl = GetWizardRoute(Url, previousWizard, mode, docId, model.DocumentCookieModel);
                }
                else
                {
                    model.BackRouteUrl = Url.RouteUrl(RouteNames.ResumeFinalize, new { id = model.DocumentId });
                }

                if (index < wizardList.Count - 1)
                {
                    var nextWizard = wizardList[index + 1];
                    model.NextRouteUrl = GetWizardRoute(Url, nextWizard, mode, docId, model.DocumentCookieModel);
                }
                else
                {
                    model.NextRouteUrl = Url.RouteUrl(RouteNames.ResumeFinalize, new { id = model.DocumentId });
                }
            }
        }

        private static string GetWizardRoute(UrlHelper Url, string wizard, string mode, int? docId,
            DocumentCookieModel docCookie)
        {
            var wizardMenu = GetWizard(Url, wizard, mode, docId, null, docCookie);
            if (wizardMenu != null)
                return wizardMenu.SectionURL;
            return null;
        }

        #endregion

        public static bool IsEditMode(string mode)
        {
            return RouteMode.Edit.Equals(mode, StringComparison.OrdinalIgnoreCase)
                   || RouteReviewMode.EditDocEditPara.Equals(mode, StringComparison.OrdinalIgnoreCase)
                   || RouteReviewMode.EditDocAddPara.Equals(mode, StringComparison.OrdinalIgnoreCase);
        }

        public static bool IsAddMode(string mode)
        {
            return RouteMode.Add.Equals(mode, StringComparison.OrdinalIgnoreCase)
                   || RouteReviewMode.AddDocEditPara.Equals(mode, StringComparison.OrdinalIgnoreCase)
                   || RouteReviewMode.AddDocAddPara.Equals(mode, StringComparison.OrdinalIgnoreCase);
        }

        public static bool IsCreateMode(string mode)
        {
            return RouteReviewMode.CreateDocEditPara.Equals(mode, StringComparison.OrdinalIgnoreCase)
                   || RouteReviewMode.CreateDocAddPara.Equals(mode, StringComparison.OrdinalIgnoreCase);
        }

        #endregion
    }
}