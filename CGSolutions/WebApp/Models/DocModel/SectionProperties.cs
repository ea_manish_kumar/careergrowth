﻿namespace WebApp.Models
{
    public class SectionProperties
    {
        public string CssClass { get; set; }
        public string Icon { get; set; }
        public string JsonProperty { get; set; }
    }
}