﻿using Newtonsoft.Json;

namespace WebApp.Models
{
    public class ContactInfo
    {
        [JsonProperty(DocSections.Name)] public NameSectionModel Name { get; set; }

        [JsonProperty(DocSections.Contact)] public ContactSectionModel Contact { get; set; }
    }
}