﻿using Newtonsoft.Json;

namespace WebApp.Models
{
    public class SkillDescription : BaseParagraphModel
    {
        [JsonProperty(DocFields.Description)] public virtual string Description { get; set; }
    }
}