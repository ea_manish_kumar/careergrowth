﻿using System.Collections.Generic;

namespace WebApp.Models
{
    public class OtherSectionModel
    {
        public string Title { get; set; }

        public string Wizard { get; set; }

        public List<string> Examples { get; set; }

        public WizardVM WizardViewModel { get; set; }

        //public List<string> Tips { get; set; }
    }
}