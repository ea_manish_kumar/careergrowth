﻿namespace WebApp.Models
{
    public class DocSections
    {
        public const string Accomplishments = "accomplishments";
        public const string Additional_Information = "additionalinformation";
        public const string Affiliations = "affiliations";
        public const string Awards = "awards";
        public const string Body = "body";
        public const string Certifications = "certifications";
        public const string Closer = "closer";
        public const string Contact = "contact";
        public const string CalltoAction = "calltoaction";
        public const string Date = "date";
        public const string Dissertation = "dissertation";
        public const string Education = "education";
        public const string ExtraCurricular = "extracurricular";
        public const string Experience = "experience";
        public const string Training = "training";
        public const string Greeting = "greeting";
        public const string Skills = "skills";
        public const string Summary = "summary";
        public const string Interests = "interests";
        public const string Language = "language";
        public const string Name = "name";
        public const string Opener = "opener";
        public const string Other = "other";
        public const string PersonalInformation = "personalinformation";
        public const string Presentations = "presentations";
        public const string Portfolio = "portfolio";
        public const string Publication = "publication";
        public const string Recipient = "recipient";
        public const string References = "references";
    }

    public class SectionFields
    {
        public const string Paragraph = "paragraph";
        public const string Paragraphs = "paragraphs";
    }

    public class DocFields
    {
        /// Miscellaneuos fields
        public const string SectionId = "sectionId";

        public const string ParagraphId = "paragraphId";
        public const string ParaSortIndex = "paraSortIndex";
        public const string SortIndex = "sortIndex";

        /// Name Section fields
        public const string Name = "name";

        /// Contact Section fields
        public const string Address = "address";

        public const string City = "city";
        public const string State = "state";
        public const string ZipCode = "zipCode";
        public const string Email = "email";
        public const string Phone = "phone";
        public const string AltPhone = "altPhone";

        /// Experience Section fields
        public const string Employer = "employer";

        public const string JobTitle = "jobTitle";
        public const string StartDateMonth = "startDateMonth";
        public const string StartDateYear = "startDateYear";
        public const string EndDateMonth = "endDateMonth";
        public const string EndDateYear = "endDateYear";
        public const string PresentlyWorkHere = "presentlyWorkHere";
        public const string JobDescription = "description";

        /// Education Section fields
        public const string School = "school";

        public const string SchoolCity = "city";
        public const string SchoolState = "state";
        public const string SchoolDescription = "description";
        public const string University = "university";
        public const string Degree = "degree";
        public const string DegreeSpecialization = "degreeSpecialization";
        public const string FieldOfStudy = "fieldOfStudy";
        public const string SchoolMonth = "schoolMonth";
        public const string SchoolYear = "schoolYear";
        public const string CGPA = "cgpa";

        //Portfolio Section Fields
        public const string Website = "website";
        public const string LinkedIn = "linkedin";
        public const string GooglePlus = "googleplus";
        public const string Twitter = "twitter";

        /// Other Section
        public const string Wizard = "wizard";

        public const string Description = "description";
    }

    public class DocProperties
    {
        public const string AddressOrCityAndState = "addressOrCityAndState";
        public const string CityOrState = "cityOrState";
        public const string ExpDate = "expDate";
        public const string ExpCityOrState = "expCityOrState";
        public const string EducSchoolYear = "educDate";
        public const string FirstName = "fname";
        public const string LastName = "lname";
    }
}