﻿using System;
using System.Collections.Generic;
using System.Linq;
using DomainModel;
using Newtonsoft.Json;
using Utilities.Constants.Documents;

namespace WebApp.Models
{
    public class DocumentCookieModel
    {
        [JsonProperty("sk")]
        public string SkinCode { get; set; }

        [JsonProperty("did")]
        public int? DocumentId { get; set; }

        [JsonProperty("onv")]
        public IList<string> OtherSectionsNav { get; set; }

        [JsonProperty("olb")]
        public string OtherSectionLabel { get; set; }


        [JsonProperty("sec")]
        private IList<string> Sections { get; set; }

        [JsonProperty("scpr")]
        private IList<string> SectionParas { get; set; }

        [JsonProperty("asc")]
        private IList<string> AdditionalSections { get; set; }

        public IList<string> GetSections()
        {
            return Sections;
        }

        public IList<string> GetAdditionalSections()
        {
            return AdditionalSections;
        }

        public void AddSection(string sectionCD)
        {
            if (Sections == null)
                Sections = new List<string>();
            if (!HasSection(sectionCD))
                Sections.Add(sectionCD);
        }

        public void AddSectionPara(string sectionCD)
        {
            if (SectionParas == null)
                SectionParas = new List<string>();
            if (!HasSectionPara(sectionCD))
                SectionParas.Add(sectionCD);
        }

        public bool HasSection(string sectionCD)
        {
            if (!string.IsNullOrEmpty(sectionCD) && Sections != null)
                return Sections.Any(s => sectionCD.Equals(s, StringComparison.OrdinalIgnoreCase));
            return false;
        }

        public bool HasSectionPara(string sectionCD)
        {
            if (!string.IsNullOrEmpty(sectionCD) && SectionParas != null)
                return SectionParas.Any(s => sectionCD.Equals(s, StringComparison.OrdinalIgnoreCase));
            return false;
        }

        public void RemoveSection(string sectionCD)
        {
            if (Sections != null && HasSection(sectionCD))
                Sections.Remove(sectionCD);
        }

        public void RemoveSectionPara(string sectionCD)
        {
            if (SectionParas != null && HasSectionPara(sectionCD))
                SectionParas.Remove(sectionCD);
        }

        public void AddAdditionalSection(string additionalSectionCD)
        {
            if (AdditionalSections == null)
                AdditionalSections = new List<string>();
            if (!string.IsNullOrEmpty(additionalSectionCD) && !AdditionalSections.Any(s =>
                    additionalSectionCD.Equals(s, StringComparison.OrdinalIgnoreCase)))
                AdditionalSections.Add(additionalSectionCD);
        }

        public static DocumentCookieModel Parse(string documentCookie)
        {
            return JsonConvert.DeserializeObject<DocumentCookieModel>(documentCookie);
        }

        public static DocumentCookieModel Parse(Document document)
        {
            if (document != null)
            {
                var docCookieModel = new DocumentCookieModel();
                docCookieModel.DocumentId = document.Id;
                if (document.Sections != null)
                    foreach (var section in document.Sections.OrderBy(s => s.SortIndex))
                    {
                        var wizard = WizardUtility.GetWizardFromSectionCD(section.SectionTypeCD);
                        if (!string.IsNullOrEmpty(wizard))
                        {
                            docCookieModel.AddSection(wizard);
                            if (SectionTypeCD.Experience.Equals(section.SectionTypeCD,
                                    StringComparison.OrdinalIgnoreCase) ||
                                SectionTypeCD.Education.Equals(section.SectionTypeCD,
                                    StringComparison.OrdinalIgnoreCase))
                                if (section.Paragraphs != null && section.Paragraphs.Any())
                                    docCookieModel.AddSectionPara(wizard);
                        }
                    }

                return docCookieModel;
            }

            return null;
        }

        public static string ToJsonString(DocumentCookieModel documentCookieModel)
        {
            return JsonConvert.SerializeObject(documentCookieModel);
        }
    }
}