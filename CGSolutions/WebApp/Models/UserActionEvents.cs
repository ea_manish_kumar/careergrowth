﻿using Newtonsoft.Json;

namespace WebApp.Models
{
    public class UserActionEvent
    {
        [JsonProperty("action")] public string Action { get; set; }

        [JsonProperty("docId")] public int DocumentId { get; set; }
    }

    public class UserDownloadEvent: UserActionEvent
    {
        [JsonProperty("isSuccess")] public bool IsSuccess { get; set; }
    }

    public class UserDocumentCopyEvent : UserActionEvent
    {
        [JsonProperty("newDocumentId")] public int NewDocumentId { get; set; }
    }

    public class UserDocumentRenameEvent : UserActionEvent
    {
        [JsonProperty("newDocumentName")] public string NewDocumentName { get; set; }
        [JsonProperty("isSuccess")] public bool IsSuccess { get; set; }
    }
}