﻿using Newtonsoft.Json;

namespace WebApp.Models
{
    public class RegistrationDetails
    {
        [JsonProperty("username")] public string UserName { get; set; }

        [JsonProperty("password")] public string Password { get; set; }

        //public short EmailOptIn { get; set;}
        [JsonProperty("contactInfo")] public ContactInfo ContactInfo { get; set; }
    }

    public class UserContactDetails
    {
        public string FirstName { get; set; }
        public string LastName { get; set; }
    }
}