﻿using Newtonsoft.Json;

namespace WebApp.Models
{
    public class DateSectionModel : BaseParagraphModel
    {
        [JsonProperty("date")] public string Date { get; set; }
    }
}