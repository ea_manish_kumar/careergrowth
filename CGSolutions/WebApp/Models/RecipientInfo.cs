﻿using Newtonsoft.Json;

namespace WebApp.Models
{
    public class RecipientInfo
    {
        [JsonProperty("date")] public DateSectionModel Date { get; set; }

        [JsonProperty("recipient")] public RecipientSectionModel Recipient { get; set; }
    }
}