﻿using App.DomainModel;

namespace WebApp.Models
{
    public class SellPageModel
    {
        public string SkuCurrency { get; set; }
        public int WeeklySku { get; set; }
        public int WeeklySkuSubTextPrice { get; set; }
        public double WeeklySkuSuperTextPrice { get; set; }
        public decimal WeeklySkuUnitPrice { get; set; }
        public string weeklySkuRenewMessage { get; set; }
        public int? WeeklyTrialDays { get; set; }
        public string WeeklyFullAccessText { get; set; }
        public string UpgradePrintingDownloading { get; set; }
        public int AnnualSku { get; set; }
        public decimal AnnualSkuUnitPrice { get; set; }

        public int AnnualSkuSubTextPrice { get; set; }
        public int AnnualSkuSuperTextPrice { get; set; }
        public int MonthlySku { get; set; }

        public decimal MonthlySkuUnitPrice { get; set; }
        public int MonthlySkuSubTextPrice { get; set; }
        public int MonthlySkuSuperTextPrice { get; set; }
        public int SkuId { get; set; }

        public string savingPercent { get; set; }
        public string skuCurrencyName { get; set; }
    }

    public class RebateSellPageModel
    {
        public RebateSkuVM MonthlyFreeSku { get; set; }
        public RebateSkuVM MonthlyPaidSku { get; set; }
        public RebateSkuVM AnnualSku { get; set; }
    }

    public class RebateSkuVM
    {
        public int SkuID { get; set; }
        public string Amount { get; set; }
    }

    public class PlanVM
    {
        public string ErrorMessage { get; set; }
        public Plan FreePlan { get; set; }
        public Plan StandardPlan { get; set; }
        public Plan ProfessionalPlan { get; set; }
        public Plan PremiumPlan { get; set; }
    }

    public class SkuDetails
    {
        public int SkuId { get; set; }
        public decimal? TrialAmount { get; set; }
        public decimal UnitPrice { get; set; }
        public string SkuTypeCD { get; set; }
        public string Description { get; set; }
        public string ScheduleTypeCD { get; set; }
        public string CurrencySymbol { get; set; }
    }
}