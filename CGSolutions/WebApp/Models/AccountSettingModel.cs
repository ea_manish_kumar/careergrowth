﻿using System;
using System.Collections.Generic;
using App.DomainModel;

namespace WebApp.Models
{
    public class AccountSettingsModel
    {
        public string Name { get; set; }
        public int AccountId { get; set; }
        public string EmailAddress { get; set; }
        public DateTime MemberSince { get; set; }
        public IList<Subscription> Subscriptions { get; set; }
        public string AccountType { get; set; }
        public bool EmailsSubscribed { get; set; }
        public bool JobsSubscribed { get; set; }
    }
}