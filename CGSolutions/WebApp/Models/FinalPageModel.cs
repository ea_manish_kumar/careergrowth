﻿namespace WebApp.Models
{
    public class FinalPageModel
    {
        public DocumentModel Document { get; set; }
        public bool IsPremiumUser { get; set; }
        public string SkinCode { get; set; }
    }
}