﻿using System;

namespace WebApp.Models
{
    public class PaymentGatewayResponse
    {
        public int OrderId { get; set; }
        public int PlanId { get; set; }
        public long TrackingId { get; set; }
        public long BankReferenceNo { get; set; }
        public decimal Amount { get; set; }
        public DateTime TransactionDate { get; set; }
    }
}