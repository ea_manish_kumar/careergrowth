﻿namespace WebApp.Models
{
    public class DropdownOption<ValueType>
    {
        public ValueType Value { get; set; }
        public string Text { get; set; }
    }
}