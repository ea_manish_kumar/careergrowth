﻿using Newtonsoft.Json;

namespace WebApp.Models
{
    public class UserDetail
    {
        [JsonProperty("username")] public string UserName { get; set; }


        [JsonProperty("password")] public string Password { get; set; }

        [JsonProperty("newpassword")] public string NewPassword { get; set; }

        [JsonProperty("email")] public string Email { get; set; }

        [JsonProperty("rememberme")] public bool RememberMe { get; set; }

        [JsonProperty("errormessage")] public string ErrorMessage { get; set; }


        [JsonProperty("heading")] public string Heading { get; set; }

        [JsonProperty("subheading")] public string SubHeading { get; set; }

        [JsonProperty("docId")] public int DocId { get; set; }
    }
}