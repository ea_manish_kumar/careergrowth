﻿using Resources;
using System.Collections.Generic;
using Utilities.Constants.Documents;

namespace WebApp.Models
{
    public class SkinVM
    {
        public string Code { get; set; }

        public string Name { get; set; }

        public SkinType SkinType { get; set; }

        public TemplatePageType TemplatePageType { get; set; }

        public string ImagePath => string.Format("/images/skins/{0}.png", Name);

        public readonly static IEnumerable<SkinVM> ResumeSkins = new List<SkinVM>
        {
            new SkinVM { Code = SkinCode.Basic, TemplatePageType = TemplatePageType.MultiPage, SkinType = SkinType.Free, Name = Resource.SkinNameBasic },
            new SkinVM { Code = SkinCode.Cool, TemplatePageType = TemplatePageType.MultiPage, SkinType = SkinType.Free, Name = Resource.SkinNameCool },
            new SkinVM { Code = SkinCode.Awesome, TemplatePageType = TemplatePageType.OnePage, SkinType = SkinType.Premium, Name = Resource.SkinNameAwesome },
            new SkinVM { Code = SkinCode.Artistic, TemplatePageType = TemplatePageType.MultiPage, SkinType = SkinType.Premium, Name = Resource.SkinNameArtistic },
            new SkinVM { Code = SkinCode.Elegant, TemplatePageType = TemplatePageType.MultiPage, SkinType = SkinType.Premium, Name = Resource.SkinNameElegant },
            new SkinVM { Code = SkinCode.Executive, TemplatePageType = TemplatePageType.MultiPage, SkinType = SkinType.Premium, Name = Resource.SkinNameExecutive },
            new SkinVM { Code = SkinCode.MultiColor, TemplatePageType = TemplatePageType.MultiPage, SkinType = SkinType.Premium, Name = Resource.SkinNameMultiColor },
            new SkinVM { Code = SkinCode.Professional, TemplatePageType = TemplatePageType.MultiPage, SkinType = SkinType.Premium, Name = Resource.SkinNameProfessional },
            new SkinVM { Code = SkinCode.Smart, TemplatePageType = TemplatePageType.OnePage, SkinType = SkinType.Premium, Name = Resource.SkinNameSmart },
            new SkinVM { Code = SkinCode.Soothing, TemplatePageType = TemplatePageType.OnePage, SkinType = SkinType.Premium, Name = Resource.SkinNameSoothing },
            new SkinVM { Code = SkinCode.StandOut, TemplatePageType = TemplatePageType.OnePage, SkinType = SkinType.Premium, Name = Resource.SkinNameStandOut },
            new SkinVM { Code = SkinCode.Superb, TemplatePageType = TemplatePageType.MultiPage, SkinType = SkinType.Premium, Name = Resource.SkinNameSuperb }
        };
    }

    public enum SkinType
    {
        Free,
        Premium
    }
    public enum TemplatePageType
    {
        OnePage,
        MultiPage
    }
}