﻿using System.Collections.Generic;

namespace WebApp.Models
{
    public class FinalizeModel
    {
        public string ActiveSections { get; set; }
        public IList<EditSectionModel> EditSections { get; set; }
        public IList<EditSectionModel> AddSections { get; set; }
        public string ActiveOtherSections { get; set; }
        public bool IsPremiumUser { get; set; }
        public string SkinCode { get; set; }
    }

    public class EditSectionModel
    {
        public string SectionCode { get; set; }
        public string SectionURL { get; set; }
        public int SectionId { get; set; }
        public string SectionDisplayValue { get; set; }
    }
}