﻿using System.Collections.Generic;

namespace WebApp.Models
{
    public class DashboardVM
    {
        public IEnumerable<DocumentDetails> Documents { get; set; }
    }
}