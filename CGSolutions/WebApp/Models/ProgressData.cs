﻿namespace WebApp.Models
{
    public class ProgressData
    {
        public int DocumentID { get; set; }
        public bool EditMode { get; set; }
        public string StageCD { get; set; }
    }
}