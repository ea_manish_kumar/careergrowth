﻿using System.Collections.Generic;

namespace WebApp.Models
{
    public class Dashboard
    {
        public string ImageUrl { get; set; }
        public int SelectedDocId { get; set; }
        public IEnumerable<DocumentDetails> Documents { get; set; }
        public bool IsPremiumUser { get; set; }
        public string Name { get; set; }
        public string CustomerSupport { get; set; }
    }
}