﻿using System.ComponentModel.DataAnnotations;
using Newtonsoft.Json;

namespace WebApp.Models
{
    public class ForgetPasswordModel
    {
        [JsonProperty("email")] [Required] public string Email { get; set; }

        [JsonProperty("errormessage")] public string ErrorMessage { get; set; }
    }
}