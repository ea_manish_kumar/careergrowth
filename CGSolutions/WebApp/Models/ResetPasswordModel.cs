﻿using System.ComponentModel.DataAnnotations;
using Newtonsoft.Json;

namespace WebApp.Models
{
    public class ResetPasswordModel
    {
        [JsonProperty("password")] [Required] public string Password { get; set; }

        [JsonProperty("newpassword")]
        [Required]

        public string NewPassword { get; set; }

        [JsonProperty("errormessage")] public string ErrorMessage { get; set; }
    }
}