﻿using Newtonsoft.Json;

namespace WebApp.Models
{
    public class EmailModel
    {
        [JsonProperty("recipientAddress")]
        public string RecipientAddress { get; set; }

        [JsonProperty("fromName")]
        public string FromName { get; set; }

        [JsonProperty("emailSubject")]
        public string EmailSubject { get; set; }

        [JsonProperty("emailBody")]
        public string EmailBody { get; set; }
    }
}