﻿using System.ComponentModel.DataAnnotations;
using Newtonsoft.Json;

namespace WebApp.Models
{
    public class SignInModel
    {
        [JsonProperty("username")] [Required] public string UserName { get; set; }


        [JsonProperty("password")] [Required] public string Password { get; set; }


        [JsonProperty("rememberme")] public bool RememberMe { get; set; }

        [JsonProperty("errormessage")] public string ErrorMessage { get; set; }


        [JsonProperty("heading")] public string Heading { get; set; }

        [JsonProperty("subheading")] public string SubHeading { get; set; }

        [JsonProperty("docId")] public int DocId { get; set; }
    }
}