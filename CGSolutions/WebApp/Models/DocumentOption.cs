﻿namespace WebApp.Models
{
    public class DocumentDetails
    {
        public int Id { get; set; }
        public string Text { get; set; }
        public string Name { get; set; }
        public string TemplateSelected { get; set; }
        public string DateCreated { get; set; }
        public string DateModified { get; set; }
        public string DocumentType { get; set; }
    }
}