﻿using System.ComponentModel.DataAnnotations;
using Newtonsoft.Json;
using Utilities.Constants;

namespace WebApp.Models
{
    public class RecipientSectionModel : BaseParagraphModel
    {
        [JsonProperty("firstname")] public string FirstName { get; set; }


        [JsonProperty("lastname")] public string LastName { get; set; }

        [JsonProperty("companyname")] public string CompanyName { get; set; }


        [JsonProperty("address")] public string Address { get; set; }

        [JsonProperty("city")] public string City { get; set; }

        [JsonProperty("state")] public string State { get; set; }

        [JsonProperty("zipCode")] public string ZipCode { get; set; }

        [RegularExpression(RegexPatterns.Email)]
        [JsonProperty("email")]
        public string Email { get; set; }

        [JsonProperty("phone")] public string Phone { get; set; }
    }
}