﻿(function ($, Builder) {
    var isOnceClicked = false;

    function validate() {
        $("#registrationError").text("").hide();
        var isValid = true;
        if (!validateEmail()) {
            isValid = false;
        }
        if (!validatePwd()) {
            isValid = false;
        }
        return isValid;
    }

    function validateEmail() {
        if (isOnceClicked) {
            $("#errEmailReqMsg,#errEmailValidMsg").hide();
            $("#username").removeClass("invalid");
            var contactEmail = $("#username").val();
            if (contactEmail) {
                var emailRegex = new RegExp(Builder.Data.Get("EmailRegexPattern"));
                if (!emailRegex.test(contactEmail)) {
                    $("#errEmailValidMsg").show();
                    $("#username").addClass("invalid");
                    return false;
                }
            } else {
                $("#errEmailReqMsg").show();
                $("#username").addClass("invalid");
                return false;
            }
        }
        return true;
    }

    function userNameExistsUIUpdates(userNameExists) {
        if (userNameExists) {
            $("#signUpHeading,#signUpText,#nameRow").addClass("display-none");
            $("#signInHeading,#signInText,#lnkForgotPassword").removeClass("display-none");
            $("#alreadyHaveAnAccount").prop("checked", true);
        } else {
            $("#signUpHeading,#signUpText,#nameRow").removeClass("display-none");
            $("#signInHeading,#signInText,#lnkForgotPassword").addClass("display-none");
            $("#alreadyHaveAnAccount").prop("checked", false);
        }
    }

    function validatePwd() {
        if (isOnceClicked) {
            $("#password").removeClass("invalid");
            $("#errPwdReq").hide();
            var pwd = $.trim($("#password").val());
            if (!pwd) {
                $("#errPwdReq").show();
                $("#password").addClass("invalid");
                return false;
            }
        }
        return true;
    }

    function checkUserName() {
        var email = $("#username").val();
        if (email) {
            var emailRegex = new RegExp(Builder.Data.Get("EmailRegexPattern"));
            if (emailRegex.test(email)) {
                var data = { "username": email };
                Builder.Http.Post({ url: Builder.Data.Get("UserNameExistsAddress"), data: data }).then(function (result) {
                    if (result) {
                        $("#userAlreadyExists").removeClass("display-none");
                    } else {
                        $("#userAlreadyExists").addClass("display-none");
                    }
                    userNameExistsUIUpdates(result);
                });
            }
        }
    }

    $(function () {
        $("#username").on("input", validateEmail).on("blur", checkUserName);
        $("#password").on("input", validatePwd);
        $("#alreadyHaveAnAccount").on("click", function () {
            userNameExistsUIUpdates($(this).is(":checked"));
        });
        $("#btnRegister").on("click", function () {
            isOnceClicked = true;
            return validate();
        });
    });
})(window.jQuery, window.Builder);