﻿(function ($, Builder) {
    var isOnceClicked = false;

    function validateEmail() {
        if (isOnceClicked) {
            var $username = $("#UserName");
            $("#errEmailReqMsg,#errEmailValidMsg").hide();
            $username.removeClass("invalid");
            var email = $username.val();
            if (email) {
                var emailRegex = new RegExp(Builder.Data.Get("EmailPattern"));
                if (!emailRegex.test(email)) {
                    $("#errEmailValidMsg").show();
                    $username.addClass("invalid");
                    return false;
                }
            } else {
                $("#errEmailReqMsg").show();
                $username.addClass("invalid");
                return false;
            }
        }
        return true;
    }

    function validatePwd() {
        if (isOnceClicked) {
            var $password = $("#Password");
            $password.removeClass("invalid");
            $("#errPwdReq").hide();
            var pwd = $.trim($password.val());
            if (!pwd) {
                $("#errPwdReq").show();
                $password.addClass("invalid");
                return false;
            }
        }
        return true;
    }

    $(function () {
        $("#loginSubmit").on("click", function () {
            isOnceClicked = true;
            var isValid = true;
            if (!validateEmail()) {
                isValid = false;
            }
            if (!validatePwd()) {
                isValid = false;
            }
            return isValid;
        });

        $("#UserName").on("input", validateEmail);
        $("#Password").on("input", validatePwd);
    });
})(jQuery, Builder);