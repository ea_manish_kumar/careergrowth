(function($, Builder) {
    var $docListContainer = $("#docListContainer");
    var $ddlDocument = $docListContainer.find("#ddlDocument");
    var $hdnDocUrls = $docListContainer.find("#hdnDocUrls");

    var $previewContainer = $("#previewContainer");
    var $docContainer = $previewContainer.find("#document-container");
    var $iframe = $docContainer.find("#iFrameDocContainer");

    var $btnEditResume = $previewContainer.find("#btnEditResume");
    var $btnDownloadDocument = $previewContainer.find("#btnDownloadDocument");
    var $btnEmailDocument = $previewContainer.find("#btnEmailDocument");
    var $btnPrintDocument = $previewContainer.find("#btnPrintDocument");
    var $btnRenameDocument = $previewContainer.find("#btnRenameDocument");
    var $btnDuplicateDocument = $previewContainer.find("#btnDuplicateDocument");
    var $btnDeleteDocument = $previewContainer.find("#btnDeleteDocument");

    var $documentDetailInfo = $("#documentDetailInfo");
    var $dateModified = $documentDetailInfo.find("#dateModified");
    var $dateCreated = $documentDetailInfo.find("#dateCreated");
    var $templateSelected = $documentDetailInfo.find("#templateSelected");
    var $documentName = $documentDetailInfo.find("#documentName");
    var currentResumeIframe = document.getElementById("iFrameDocContainer");

    function bindDocHtml(html) {
        if ($.trim(html)) {
            currentResumeIframe.contentWindow.document.open();
            currentResumeIframe.contentWindow.document.write(html);
            currentResumeIframe.contentWindow.document.close();
            //TODO: Check on different screens/resolutions if this calculation works
            //25% of the width is reserved for the left section
            var originalWidth = 892.5;
            var contWidth = $("#resumeContainer").width();
            var ratio = (contWidth) / originalWidth;
            if (ratio > 1)
                ratio = 1;
            $("#iFrameDocContainer").css({
                'transform': "scale(" + ratio + ")",
                '-webkit-transform': "scale(" + ratio + ")",
                '-moz-transform': "scale(" + ratio + ")",
                '-o-transform': "scale(" + ratio + ")",
                '-ms-transform': "scale(" + ratio + ")",
                '-webkit-transform-origin': "top left",
                '-moz-transformtransform-origin': "top left",
                '-ms-transformtransform-origin': "top left",
                'transform-origin': "top left"
            });
        }
    }

    function UpdateDocPreview() {
        Builder.Loader.Append("#document-container");
        var docId = $ddlDocument.val();
        var previewURL = $hdnDocUrls.attr("preview-url").replace("1", docId);

        Builder.Http.Get({ url: previewURL, dataType: "html" }).then(bindDocHtml).always(
            function () {
                Builder.Loader.Remove("#document-container");
            });
    }

    //(function init() {
    //    /// Set IFrame
    //    var contWidth = $docContainer.width(), ratio = 0.9, height = 550;
    //    //Builder.Loader.Append("#document-container");
    //    $iframe.css({ "height": height + "px" }).on("load",
    //        function() {
    //            Builder.Loader.Remove("#document-container");
    //            $iframe.hide();
    //            var contWidth = $docContainer.width(), ratio = 0.9;
    //            $iframe.contents().find("html").css({
    //                '-webkit-font-smoothing': "antialiased",
    //                'transform': "scale(" + ratio + ")",
    //                '-webkit-transform': "scale(" + ratio + ")",
    //                '-moz-transform': "scale(" + ratio + ")",
    //                '-o-transform': "scale(" + ratio + ")",
    //                '-ms-transform': "scale(" + ratio + ")",
    //                '-webkit-transform-origin': "top left",
    //                '-moz-transformtransform-origin': "top left",
    //                '-ms-transformtransform-origin': "top left",
    //                'transform-origin': "top left"
    //            });
    //            $iframe.show();
    //            alert(1);
    //        });
    //})();
    
    //function UpdateDocPreview() {
    //    Builder.Loader.Append("");
    //    var docId = $ddlDocument.val();
    //    $iframe.prop("src", $hdnDocUrls.attr("preview-url").replace("1", docId));
    //}

    function PopulateDocumentDetails() {
        var $selectOption = $ddlDocument.find("option:selected");
        $dateModified.text($selectOption.attr("data-date-mod"));
        $dateCreated.text($selectOption.attr("data-date-crtd"));
        $templateSelected.text($selectOption.attr("data-template"));
        $documentName.text($selectOption.text());
    }

    function PopulateButtons(id) {
        $btnEditResume.prop("href", $hdnDocUrls.attr("edit-url").replace("1", id));
        $btnDownloadDocument.prop("href", $hdnDocUrls.attr("download-url").replace("1", id));
        $btnRenameDocument.attr("rename-url", $hdnDocUrls.attr("rename-url").replace("1", id));
        $btnDuplicateDocument.attr("duplicate-url", $hdnDocUrls.attr("duplicate-url").replace("1", id));
        $btnDeleteDocument.attr("delete-url", $hdnDocUrls.attr("delete-url").replace("1", id));
    }

    $(function() {
        if ($ddlDocument.find("option").length) {
            PopulateButtons($ddlDocument.val());
            $btnRenameDocument.on("click",
                function(e) {
                    if (e && e.preventDefault()) {
                        e.preventDefault();
                    }
                    $("#txt-rename").val($documentName.text());
                    $("#rename-modal").modal();
                });
            $ddlDocument.on("change",
                function() {
                    var docId = $ddlDocument.val();
                    $iframe.prop("src", $hdnDocUrls.attr("preview-url").replace("1", docId));
                    PopulateButtons(docId);
                    PopulateDocumentDetails();
                });
            $("#btn-rename-resume").on("click",
                function() {
                    var newName = $.trim($("#txt-rename").val());
                    if (!newName) {
                        return;
                    }
                    if ($documentName.text().toLowerCase() == newName.toLowerCase()) {
                        $("#rename-modal").modal("hide");
                        return;
                    }
                    var alreadyExists = false;
                    $ddlDocument.find("option").each(function() {
                        if (newName.toLowerCase() == $(this).text().toLowerCase()) {
                            alreadyExists = true;
                            return false;
                        }
                    });
                    if (alreadyExists) {
                        $("#txt-rename").addClass("invalid");
                        $("#err-already-exists").show();
                        return;
                    }
                    Builder.Http.Post({ "url": $btnRenameDocument.attr("rename-url"), "data": { "docName": newName } })
                        .then(function() {
                            $ddlDocument.find("option:selected").text(newName);
                            $documentName.text(newName);
                            $("#rename-modal").modal("hide");
                        });
                });
            $btnDeleteDocument.on("click",
                function() {
                    $("#delete-modal").modal();
                });
            $("#btn-delete-resume").on("click",
                function() {
                    Builder.Http.Post({ "url": $btnDeleteDocument.attr("delete-url") }).then(function(data) {
                        $ddlDocument.find("option:selected").remove();
                        UpdateDocPreview();
                        PopulateDocumentDetails();
                        PopulateButtons($ddlDocument.val());
                        $("#delete-modal").modal("hide");
                    });
                });
            $btnDuplicateDocument.on("click",
                function() {
                    Builder.Http.Post({ "url": $btnDuplicateDocument.attr("duplicate-url") }).then(function(data) {
                        $ddlDocument.prepend($("<option />").text(data.name).val(data.docId).attr({
                            "data-date-mod": data.timestamp,
                            "data-date-crtd": data.timestamp,
                            "data-template": data.template
                        })).val(data.docId);
                        UpdateDocPreview();
                        PopulateDocumentDetails();
                        PopulateButtons(data.docId);
                    });
                });
        } else {
            var newResumeUrl = $hdnDocUrls.attr("new-url");
            $btnEditResume.prop("href", newResumeUrl);
            $btnDownloadDocument.prop("href", newResumeUrl);
            $btnPrintDocument.prop("href", newResumeUrl);
            $btnEmailDocument.prop("href", newResumeUrl);
            $btnRenameDocument.prop("href", newResumeUrl);
            $btnDuplicateDocument.prop("href", newResumeUrl);
            $btnDeleteDocument.prop("href", newResumeUrl);
        }
    });
    UpdateDocPreview();
})(jQuery, Builder);