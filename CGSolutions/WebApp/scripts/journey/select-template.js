﻿(function($, Builder) {
    $(function() {
        $("#select-template a[info-template]").on("click",
            function(e) {
                if (e && e.preventDefault) {
                    e.preventDefault();
                }
                var template = $(this).attr("info-template");
                Builder.Http.Post({ url: Builder.Data.Get("SelectResumeAddress"), data: { "skinCD": template } }).then(
                    function(isUpdated) {
                        if (isUpdated) {
                            location.href = Builder.Data.Get("NextScreenAddress");
                        }
                    });
            });
    });
})(jQuery, Builder);