﻿(function($, Builder) {
    Builder.Listeners = new function() {
        var funcColl = {};
        this.On = function(name, func) {
            funcColl[name] = func;
        };
        this.Execute = function(name) {
            funcColl[name]();
        };
    };
    Builder.Listeners.On("contentReady",
        function() {
            $("#divHistRoot span[data-action=delete]").on("click",
                function(e) {
                    $("#btn-delete-item").attr("data-item-id", $(this).attr("data-item-id"));
                    $("#delete-modal").modal();
                });
            $("#btn-delete-item").on("click",
                function() {
                    var paraId = $(this).attr("data-item-id");
                    Builder.Http.Post({ "url": Builder.Data.Get("DeleteItemAddress"), "data": { "id": paraId } }).then(
                        function(result) {
                            if (result && result.success === true) {
                                if (result.isDocumentDeleted === true) {
                                    Builder.BrowserRepo.Remove(Builder.Data.Get("DocumentId"));
                                    location.href = Builder.Data.Get("DashboardAddress");
                                } else if (result.isGroupDeleted === true) {
                                    Builder.BrowserRepo.Set(Builder.Data.Get("DocumentId"),
                                        Builder.Data.Get("Academic"),
                                        null);
                                    location.href = Builder.Data.Get("AllItemDeletedAddress");
                                } else {
                                    $("#divHistRoot [data-action=delete][data-item-id=" + paraId + "]")
                                        .parents(".experienceSectionRow").remove();
                                    var data = Builder.BrowserRepo.Get(Builder.Data.Get("DocumentId"),
                                        Builder.Data.Get("Academic"));
                                    if (data) {
                                        var paragraphs = data[Builder.Data.Get("Items")];
                                        if (paragraphs && paragraphs.length) {
                                            var itemIdField = Builder.Data.Get("ItemIdField");
                                            for (var i = 0, len = paragraphs.length; i < len; i++) {
                                                if (paraId == paragraphs[itemIdField]) {
                                                    paragraphs.splice(i, 1);
                                                    break;
                                                }
                                            }
                                            data[Builder.Data.Get("Items")] = paragraphs;
                                        }
                                    }
                                    Builder.BrowserRepo.Set(Builder.Data.Get("DocumentId"),
                                        Builder.Data.Get("Academic"),
                                        data);
                                    $("#delete-modal").modal("hide");
                                }
                            }
                        });
                });
            initSorting();
        });

    function getEditUrl(paragraphId) {
        return Builder.Data.Get("EditAddressFormat").replace("{itemId}", paragraphId);
    }

    function getSortUrl(sectionId) {
        return Builder.Data.Get("SortAddressFormat").replace("{groupId}", sectionId);
    }

    function initSorting() {
        var $divHistRoot = $("#divHistRoot");
        $divHistRoot.sortable({
            items: "[data-sort-item-id]",
            containment: "#sortBoundary",
            stop: function(e) {
                var sortedParagraphs = [];
                $divHistRoot.find("[data-sort-item-id]").each(function(ind) {
                    var para = { "sortIndex": ind };
                    para[Builder.Data.Get("ItemIdField")] = $(this).attr("data-sort-item-id");
                    sortedParagraphs.push(para);
                });
                if (sortedParagraphs && sortedParagraphs.length) {
                    Builder.Http.Post({
                        url: getSortUrl($divHistRoot.attr("data-group-id")),
                        data: { "sortedItems": sortedParagraphs }
                    }).then(function(educHist) {
                        if (educHist) {
                            Builder.BrowserRepo.Set(Builder.Data.Get("DocumentId"),
                                Builder.Data.Get("Academic"),
                                educHist);
                        }
                    });
                }
            }
        });
    }

    function setEducationData(data) {
        if (data) {
            var $dvHist = $("#divHistRoot").attr("data-group-id", data[Builder.Data.Get("GroupIdField")]);
            var paragraphs = data[Builder.Data.Get("Items")];
            if (paragraphs && paragraphs.length) {
                $dvHist.empty();
                for (var i = 0, len = paragraphs.length; i < len; i++) {

                    var $dvCompanyInfo = $("<div />").addClass("experCompanyInfo");
                    var $dvPosition = $("<div />").addClass("experPositionInfo");

                    var degree = paragraphs[i][Builder.Data.Get("DegreeField")];
                    var degreeSpecialization = paragraphs[i][Builder.Data.Get("DegreeSpecField")];
                    var school = paragraphs[i][Builder.Data.Get("SchoolField")];
                    var university = paragraphs[i][Builder.Data.Get("UniversityField")];

                    var schoolDetails = [];
                    if (university) {
                        schoolDetails.push(university);
                    }
                    if (school) {
                        schoolDetails.push(school);
                    }

                    var degreeDetails = [];
                    if (degree) {
                        degreeDetails.push(degree);
                    }
                    if (degreeSpecialization) {
                        degreeDetails.push(degreeSpecialization);
                    }

                    $dvPosition.html(degreeDetails.join(", "));
                    $dvCompanyInfo.html(schoolDetails.join(", "));

                    var $dvLocation = $("<span />").addClass("experCitiInfo");
                    var city = paragraphs[i][Builder.Data.Get("CityField")];
                    var state = paragraphs[i][Builder.Data.Get("StateField")];
                    var locationArr = [];
                    if (city) {
                        locationArr.push(city);
                    }
                    if (state) {
                        locationArr.push(state);
                    }
                    if (locationArr.length) {
                        $dvLocation.html(" (" + locationArr.join(", ") + ")");
                    }

                    var $dvDescription = $("<div />").addClass("experCompanyDetailInfo");
                    var description = paragraphs[i][Builder.Data.Get("SDField")];
                    if (description) {
                        $dvDescription.html(description);
                    }

                    $dvCompanyInfo.append($dvLocation);

                    var paragraphId = paragraphs[i][Builder.Data.Get("ItemIdField")];
                    var $icoSort = $("<i />").addClass("icon-Move");
                    var $btnSort = $("<span />").addClass("experLinkInfo").prop("title", Builder.Data.Get("SortText"))
                        .append($icoSort);
                    var $icoEdit = $("<i />").addClass("icon-Edit");
                    var $anchEdit = $("<a />").addClass("experLinkInfo")
                        .prop({ "title": Builder.Data.Get("EditText"), "href": getEditUrl(paragraphId) })
                        .append($icoEdit);
                    var $icoDelete = $("<i />").addClass("icon-Trash");
                    var $btnDelete = $("<span />").addClass("experLinkInfo")
                        .prop("title", Builder.Data.Get("DeleteText"))
                        .attr({ "data-action": "delete", "data-item-id": paragraphId }).append($icoDelete);

                    var $dvLinks = $("<div />").addClass("experienceEditInfo");
                    $dvLinks.append($btnSort);
                    $dvLinks.append($anchEdit);
                    $dvLinks.append($btnDelete);

                    var $dvRow = $("<div />").addClass("experienceSectionRow").attr("data-sort-item-id", paragraphId);
                    $dvRow.append($dvLinks);
                    $dvRow.append($dvCompanyInfo);
                    $dvRow.append($dvPosition);
                    $dvRow.append($dvDescription);

                    $dvHist.append($dvRow);
                }
                $dvHist.show();
                Builder.Listeners.Execute("contentReady");
            }
        }
    }

    (function init() {
        var educationData = Builder.BrowserRepo.Get(Builder.Data.Get("DocumentId"), Builder.Data.Get("Academic"));
        if (educationData) {
            setEducationData(educationData);
        } else {
            Builder.Http.Get({ url: Builder.Data.Get("GetAcademicAddress") }).then(function(educHist) {
                if (educHist) {
                    setEducationData(educHist);
                    Builder.BrowserRepo.Set(Builder.Data.Get("DocumentId"), Builder.Data.Get("Academic"), educHist);
                }
            });
        }
    })();
})(jQuery, Builder);