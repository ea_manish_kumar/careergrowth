﻿(function($, Builder) {
    function editor(content) {
        if (content) {
            Builder.Editor.SetContent(Builder.Data.Get("EditorId"), content);
        } else {
            return Builder.Editor.GetContent(Builder.Data.Get("EditorId"));
        }
    }

    function getEducationData() {
        return Builder.State.Get(Builder.Data.Get("Academic"));
    }

    function getExamples() {
        Builder.Examples.Load(Builder.Data.Get("EditorId"), Builder.Data.Get("LoadExamplesAddress"));
    }

    function setEducationData(data) {
        if (data) {
            var paragraphs = data[Builder.Data.Get("AcademicItems")];
            if (paragraphs && paragraphs.length) {
                var paragraphId = Builder.Data.Get("AcademicItemId");
                var educPara = null;
                for (var i = 0, len = paragraphs.length; i < len; i++) {
                    if (paragraphs[i][Builder.Data.Get("AcademicItemIdField")] === paragraphId) {
                        educPara = paragraphs[i];
                        break;
                    }
                }
                Builder.State.Set(Builder.Data.Get("Academic"), educPara);
            }
        }
    }

    (function init() {
        Builder.Editor.Init(Builder.Data.Get("EditorId"), { placeholder: Builder.Data.Get("EditorPlaceHolder") });
        Builder.State.Select({ "selector": "input[type=text],input[type=hidden],select" });
        Builder.State.Select({
            "selector": "#" + Builder.Data.Get("EditorId"),
            infoField: Builder.Data.Get("SDField"),
            infoFieldGroup: Builder.Data.Get("Academic"),
            val: editor
        });
        if (Builder.Data.Get("FetchData")) {
            var educationData = Builder.BrowserRepo.Get(Builder.Data.Get("DocumentId"), Builder.Data.Get("Academic"));
            if (educationData) {
                setEducationData(educationData);
                Builder.State.Record();
            } else {
                Builder.Http.Get({ url: Builder.Data.Get("GetEducationAddress") }).then(function(data) {
                    if (data) {
                        setEducationData(data);
                        Builder.State.Record();
                        Builder.BrowserRepo.Set(Builder.Data.Get("DocumentId"), Builder.Data.Get("Academic"), data);
                    }
                });
            }
        }
    })();
    $(function() {
        $("#exampleContainer i").on("click",
            function() { Builder.Editor.AddParagraph(Builder.Data.Get("EditorId"), $(this).next("p").html()); });
        $("#btnSaveAndNext").on("click",
            function(e) {
                if (e && e.preventDefault) {
                    e.preventDefault();
                }
                isOnceClicked = true;
                if (Builder.State.IsDirty()) {
                    var educationData = getEducationData();
                    Builder.Http.Post({ url: Builder.Data.Get("SaveEducationAddress"), data: educationData }).then(
                        function(data) {
                            if (data) {
                                Builder.BrowserRepo.Set(Builder.Data.Get("DocumentId"),
                                    Builder.Data.Get("Academic"),
                                    data);
                                var paragraphs = data[Builder.Data.Get("AcademicItems")];
                                if (paragraphs && paragraphs.length) {
                                    location.href = Builder.Data.Get("EducationReviewAddress");
                                } else {
                                    location.href = Builder.Data.Get("NextScreenAddress");
                                }
                            }
                        });
                } else {
                    location.href = Builder.Data.Get("FetchData")
                        ? Builder.Data.Get("EducationReviewAddress")
                        : Builder.Data.Get("NextScreenAddress");
                }
            });
        getExamples();

    });
    //Builder.Plugin('google').LoadScript(Builder.Data.Get("GoogleAutoComplete")).Ready(function () {
    //    Builder.Places.Init({
    //        elemId: "school",
    //        fields: { "school": "placeName", "city": "locality", "state": "administrative_area_level_1" },
    //        types: ["establishment"],
    //        wizard: "place"
    //    });
    //    Builder.Places.Init({
    //        elemId: "city",
    //        fields: { "city": "locality", "state": "administrative_area_level_1" },
    //        types: ["(cities)"]
    //    });
    //});
})(jQuery, Builder);