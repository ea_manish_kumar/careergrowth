﻿(function($, Builder) {
    function getExperienceData() {
        return Builder.State.Get(Builder.Data.Get("Work"));
    }

    function setExperienceData(data) {
        if (data) {
            var paragraphs = data[Builder.Data.Get("WorkItems")];
            if (paragraphs && paragraphs.length) {
                var paragraphId = Builder.Data.Get("WorkItemId");
                var expPara = null;
                for (var i = 0, len = paragraphs.length; i < len; i++) {
                    if (paragraphs[i][Builder.Data.Get("WorkItemIdField")] === paragraphId) {
                        expPara = paragraphs[i];
                        break;
                    }
                }
                Builder.State.Set(Builder.Data.Get("Work"), expPara);
                if (expPara && expPara.jobTitle) {
                    Builder.Examples.Load(Builder.Data.Get("EditorId"),
                        Builder.Data.Get("LoadExamplesAddress"),
                        expPara.jobTitle);
                }
            }
        }
    }

    function editor(content) {
        if (content) {
            Builder.Editor.SetContent(Builder.Data.Get("EditorId"), content);
        } else {
            return Builder.Editor.GetContent(Builder.Data.Get("EditorId"));
        }
    }

    function setPresentlyWorkHere(isChecked) {
        $("#endMonth,#endYear").attr("data-val-preserve",
            function() {
                if (isChecked) {
                    return $(this).val();
                }
            }).val(function() {
            return isChecked ? $(this).attr("data-curr-date") : $(this).attr("data-val-preserve");
        }).prop("disabled", isChecked);
    }

    function presentlyWorkHere() {
        if ($("#presentlyWorkHere").prop("checked")) {
            setPresentlyWorkHere(true);
        }
    }

    var isOnceClicked = false;

    function validate() {
        var isValid = true;
        if (!validateEmployerJobTitle()) {
            isValid = false;
        }
        if (!validateStartDate()) {
            isValid = false;
        }
        if (!validateEndtDate()) {
            isValid = false;
        }
        if (!validateDateComparison()) {
            isValid = false;
        }
        return isValid;
    }

    function validateEmployerJobTitle() {
        if (isOnceClicked) {
            $("#errProvideEmpMsg,#errProvideJobTitleMsg").hide();
            $("#employer,#jobTitle").removeClass("invalid");
            var isEmpExists = $.trim($("#employer").val()) !== "";
            var isJobTitleExists = $.trim($("#jobTitle").val()) !== "";
            if (isEmpExists !== isJobTitleExists) {
                if (!isEmpExists) {
                    $("#errProvideEmpMsg").show();
                    $("#employer").addClass("invalid");
                }
                if (!isJobTitleExists) {
                    $("#errProvideJobTitleMsg").show();
                    $("#jobTitle").addClass("invalid");
                }
                return false;
            }
        }
        return true;
    }

    function validateDate(errMsgId, ddlMonthId, ddlYearId) {
        $(errMsgId).hide();
        $(ddlMonthId + "," + ddlYearId).removeClass("invalid");
        var month = parseInt($(ddlMonthId).val());
        var year = parseInt($(ddlYearId).val());
        if ((month > 0 && year <= 0) || (year > 0 && month <= 0)) {
            $(errMsgId).show();
            if (month > 0) {
                $(ddlYearId).addClass("invalid")
            }
            if (year > 0) {
                $(ddlMonthId).addClass("invalid")
            }
            return false;
        }
        return true;
    }

    function validateStartDate() {
        return !isOnceClicked || validateDate("#errStartDateMsg", "#startMonth", "#startYear");
    }

    function validateEndtDate() {
        return !isOnceClicked || validateDate("#errEndDateMsg", "#endMonth", "#endYear");
    }

    function validateDateComparison() {
        if (isOnceClicked) {
            var startMonth = parseInt($("#startMonth").val());
            var startYear = parseInt($("#startYear").val());
            var endMonth = parseInt($("#endMonth").val());
            var endYear = parseInt($("#endYear").val());
            if (startMonth > 0 && startYear > 0 && endMonth > 0 && endYear > 0) {
                if ((endYear < startYear) || (endYear == startYear && endMonth < startMonth)) {
                    $("#errStartEndDateMsg").show();
                    return false;
                }
            }
            $("#errStartEndDateMsg").hide();
        }
        return true;
    }

    (function init() {
        Builder.Editor.Init(Builder.Data.Get("EditorId"), { placeholder: Builder.Data.Get("EditorPlaceHolder") });
        Builder.State.Select({ "selector": "input[type=text],input[type=hidden],select" });
        Builder.State.Select({
            "selector": "#" + Builder.Data.Get("EditorId"),
            infoField: Builder.Data.Get("JDField"),
            infoFieldGroup: Builder.Data.Get("Work"),
            val: editor
        });
        if (Builder.Data.Get("FetchData")) {
            var experienceData = Builder.BrowserRepo.Get(Builder.Data.Get("DocumentId"), Builder.Data.Get("Work"));
            if (experienceData) {
                setExperienceData(experienceData);
                presentlyWorkHere();
                Builder.State.Record();
            } else {
                Builder.Http.Get({ url: Builder.Data.Get("GetExperienceAddress") }).then(function(data) {
                    if (data) {
                        setExperienceData(data);
                        presentlyWorkHere();
                        Builder.State.Record();
                        Builder.BrowserRepo.Set(Builder.Data.Get("DocumentId"), Builder.Data.Get("Work"), data);
                    }
                });
            }
        }
    })();
    $(function() {
        $("#employer,#jobTitle").on("keyup", function() { validateEmployerJobTitle(); });
        $("#startMonth,#startYear").on("change",
            function() {
                validateStartDate();
                validateDateComparison();
            });
        $("#endMonth,#endYear").on("change",
            function() {
                validateEndtDate();
                validateDateComparison();
            });
        $("#presentlyWorkHere").on("click",
            function(e) {
                var isChecked = $(this).prop("checked");
                setPresentlyWorkHere(isChecked);
            });
        $("#exampleContainer i").on("click",
            function() { Builder.Editor.AddParagraph(Builder.Data.Get("EditorId"), $(this).next("p").html()); });
        $("#btnSaveAndNext").on("click",
            function(e) {
                if (e && e.preventDefault) {
                    e.preventDefault();
                }
                isOnceClicked = true;
                if (validate()) {
                    if (Builder.State.IsDirty()) {
                        var experienceData = getExperienceData();
                        Builder.Http.Post({ url: Builder.Data.Get("SaveExperienceAddress"), data: experienceData })
                            .then(function(data) {
                                if (data) {
                                    Builder.BrowserRepo.Set(Builder.Data.Get("DocumentId"),
                                        Builder.Data.Get("Work"),
                                        data);
                                    var paragraphs = data[Builder.Data.Get("WorkItems")];
                                    if (paragraphs && paragraphs.length) {
                                        location.href = Builder.Data.Get("ExprerienceReviewAddress");
                                    } else {
                                        location.href = Builder.Data.Get("NextScreenAddress");
                                    }
                                }
                            });
                    } else {
                        location.href = Builder.Data.Get("FetchData")
                            ? Builder.Data.Get("ExprerienceReviewAddress")
                            : Builder.Data.Get("NextScreenAddress");
                    }
                }
            });
        Builder.Examples.Load(Builder.Data.Get("EditorId"), Builder.Data.Get("LoadExamplesAddress"), "");
        Builder.Examples.Autocomplete(Builder.Data.Get("EditorId"),
            Builder.Data.Get("SearchJobTitlesAddress"),
            Builder.Data.Get("SearchExamplesAddress"));
    });
    //Builder.Plugin('google').LoadScript(Builder.Data.Get("GoogleAutoComplete")).Ready(function () {
    //    Builder.Places.Init({
    //        elemId: "city",
    //        fields: { "city": "locality", "state": "administrative_area_level_1" },
    //        types: ["(cities)"]
    //    });
    //});
})(jQuery, Builder);