﻿(function($, Builder) {
    function getUserDetails() {
        var data = {};
        data[Builder.Data.Get("Name")] = Builder.State.Get(Builder.Data.Get("Name"));
        data[Builder.Data.Get("Contact")] = Builder.State.Get(Builder.Data.Get("Contact"));
        return data;
    }

    function setUserDetails(data) {
        if (data) {
            var name = data[Builder.Data.Get("Name")];
            if (name) {
                Builder.State.Set(Builder.Data.Get("Name"), name);
            }
            var contact = data[Builder.Data.Get("Contact")];
            if (contact) {
                Builder.State.Set(Builder.Data.Get("Contact"), contact);
            }
        }
    }

    var isOnceClicked = false;

    function verifyEmail() {
        if (isOnceClicked) {
            $("#errEmailValid").hide();
            $("#email").removeClass("invalid");
            var contactEmail = $("#email").val();
            if (contactEmail) {
                var emailRegex = new RegExp(Builder.Data.Get("EmailPattern"));
                if (!emailRegex.test(contactEmail)) {
                    $("#errEmailValid").show();
                    $("#email").addClass("invalid");
                    return false;
                }
            }
        }
        return true;
    }

    function verifyPhone() {
        $("#errPhoneValidMsg").hide();
        $("#phone").removeClass("invalid");
        var contactNumber = $("#phone").val();
        if (contactNumber) {
            if (contactNumber.length > 15) {
                $("#phone").addClass("invalid");
                return false;
            }
            var phoneRegex = new RegExp(Builder.Data.Get("PhoneNumberPattern"));
            if (!phoneRegex.test(contactNumber)) {
                $("#errPhoneValidMsg").show();
                $("#phone").addClass("invalid");
                return false;
            }
        }
        return true;
    }

    function verifyAltPhone() {
        $("#errAltPhoneValidMsg").hide();
        $("#altPhone").removeClass("invalid");
        var contactNumber = $("#altPhone").val();
        if (contactNumber) {
            if (contactNumber.length > 15) {
                $("#altPhone").addClass("invalid");
                return false;
            }
            var phoneRegex = new RegExp(Builder.Data.Get("PhoneNumberPattern"));
            if (!phoneRegex.test(contactNumber)) {
                $("#errAltPhoneValidMsg").show();
                $("#altPhone").addClass("invalid");
                return false;
            }
        }
        return true;
    }

    function verifyZipCode() {
        $("#errZipCodeValid").hide();
        $("#zipCode").removeClass("invalid");
        var zipCode = $("#zipCode").val();
        if (zipCode) {
            if (zipCode.length > 10) {
                $("#zipCode").addClass("invalid");
                return false;
            }
            var zipCodeRegex = new RegExp(Builder.Data.Get("ZipCodePattern"));
            if (!zipCodeRegex.test(zipCode)) {
                $("#errZipCodeValid").show();
                $("#zipCode").addClass("invalid");
                return false;
            }
        }
        return true;
    }

    (function init() {
        Builder.State.Select({ "selector": "form input[type=hidden],select,input[type=text]" });
        if (Builder.Data.Get("FetchData")) {
            var name = Builder.BrowserRepo.Get(Builder.Data.Get("ResumeId"), Builder.Data.Get("Name"));
            var contact = Builder.BrowserRepo.Get(Builder.Data.Get("ResumeId"), Builder.Data.Get("Contact"));
            if (name && contact) {
                var data = {};
                data[Builder.Data.Get("Name")] = name;
                data[Builder.Data.Get("Contact")] = contact;
                setUserDetails(data);
                Builder.State.Record();
            } else {
                Builder.Http.Get({ url: Builder.Data.Get("UserDetailsFetchAddress") }).then(function(userDetails) {
                    if (userDetails) {
                        setUserDetails(userDetails);
                        Builder.State.Record();
                        Builder.BrowserRepo.Set(Builder.Data.Get("ResumeId"),
                            Builder.Data.Get("Name"),
                            userDetails[Builder.Data.Get("Name")]);
                        Builder.BrowserRepo.Set(Builder.Data.Get("ResumeId"),
                            Builder.Data.Get("Contact"),
                            userDetails[Builder.Data.Get("Contact")]);
                    }
                });
            }
        }
    })();

    function saveChanges() {
        var contactInfo = getUserDetails();
        Builder.Http.Post({ url: Builder.Data.Get("UserDetailsSaveAddress"), data: contactInfo }).then(
            function(result) {
                if (result) {
                    Builder.BrowserRepo.Set(Builder.Data.Get("ResumeId"),
                        Builder.Data.Get("Name"),
                        contactInfo[Builder.Data.Get("Name")]);
                    Builder.BrowserRepo.Set(Builder.Data.Get("ResumeId"),
                        Builder.Data.Get("Contact"),
                        contactInfo[Builder.Data.Get("Contact")]);
                    location.href = Builder.Data.Get("NextScreenAddress");
                }
            });
    }

    $(function() {
        $("#email").on("keyup", function() { verifyEmail(); });
        $("#phone").on("keyup", function() { verifyPhone(); });
        $("#zipCode").on("keyup", function() { verifyZipCode(); });
        $("#btnProceed").on("click",
            function(e) {
                if (e && e.preventDefault) {
                    e.preventDefault();
                }
                isOnceClicked = true;
                if (!verifyPhone() || !verifyAltPhone() || !verifyZipCode() || !verifyEmail()) {
                    return;
                }
                if (Builder.State.IsDirty()) {
                    saveChanges();
                } else {
                    location.href = Builder.Data.Get("NextScreenAddress");
                }
            });
    });
    //Builder.Plugin('google').LoadScript(Builder.Data.Get("GoogleAutoComplete")).Ready(function () {
    //    Builder.Places.Init({
    //        elemId: "address",
    //        fields: { "address": "route", "city": "locality", "state": "administrative_area_level_1", "zipCode": "postal_code" },
    //        types: ["address"],
    //        wizard: "address"
    //    });
    //    Builder.Places.Init({
    //        elemId: "city",
    //        fields: { "city": "locality", "state": "administrative_area_level_1", "zipCode": "postal_code" },
    //        types: ["(cities)"]
    //    });
    //});
})(jQuery, Builder);