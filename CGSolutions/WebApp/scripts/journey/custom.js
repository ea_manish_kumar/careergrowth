﻿(function($, Builder) {
    function editor(content) {
        if (content) {
            Builder.Editor.SetContent(Builder.Data.Get("EditorId"), content);
        } else {
            return Builder.Editor.GetContent(Builder.Data.Get("EditorId"));
        }
    }

    function getOtherData() {
        return Builder.State.Get(Builder.Data.Get("Custom"));
    }

    function setOtherData(data) {
        Builder.State.Set(Builder.Data.Get("Custom"), data);
    }

    (function init() {
        Builder.Editor.Init(Builder.Data.Get("EditorId"), { placeholder: Builder.Data.Get("EditorPlaceholder") });
        Builder.State.Select({ "selector": "form input[type=text],input[type=hidden],select" });
        Builder.State.Select({
            "selector": "#" + Builder.Data.Get("EditorId"),
            infoField: Builder.Data.Get("DescField"),
            infoFieldGroup: Builder.Data.Get("Custom"),
            val: editor
        });
        if (Builder.Data.Get("FetchData")) {
            var otherData = Builder.BrowserRepo.Get(Builder.Data.Get("DocumentId"), Builder.Data.Get("Group"));
            if (otherData) {
                setOtherData(otherData);
                Builder.State.Record();
            } else {
                Builder.Http.Get({ url: Builder.Data.Get("GetCustomAddress") }).then(function(data) {
                    if (data) {
                        setOtherData(data);
                        Builder.State.Record();
                        Builder.BrowserRepo.Set(Builder.Data.Get("DocumentId"), Builder.Data.Get("Group"), data);
                    }
                });
            }
        }
    })();
    $(function() {
        $("#exampleContainer i").on("click",
            function() { Builder.Editor.AddList(Builder.Data.Get("EditorId"), $(this).next("p").html()); });
        $("#btnSaveAndNext").on("click",
            function(e) {
                if (e && e.preventDefault) {
                    e.preventDefault();
                }
                if (Builder.State.IsDirty()) {
                    var otherData = getOtherData();
                    Builder.Http.Post({ url: Builder.Data.Get("SaveCustomAddress"), data: otherData }).then(
                        function(data) {
                            if (data) {
                                Builder.BrowserRepo.Set(Builder.Data.Get("DocumentId"),
                                    Builder.Data.Get("Group"),
                                    data);
                                location.href = Builder.Data.Get("NextScreenAddress");
                            }
                        });
                } else {
                    location.href = Builder.Data.Get("NextScreenAddress");
                }
            });
    });
})(jQuery, Builder);