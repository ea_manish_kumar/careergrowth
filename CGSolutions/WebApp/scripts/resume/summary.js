﻿(function($, Builder) {
    function editor(content) {
        if (content) {
            Builder.Editor.SetContent(Builder.Data.Get("EditorId"), content);
        } else {
            return Builder.Editor.GetContent(Builder.Data.Get("EditorId"));
        }
    }

    function getSummary() {
        return Builder.State.Get(Builder.Data.Get("Summary"));
    }

    function setSummary(data) {
        Builder.State.Set(Builder.Data.Get("Summary"), data);
    }

    (function init() {
        Builder.Editor.Init(Builder.Data.Get("EditorId"), { placeholder: Builder.Data.Get("EditorPlaceholder") });
        Builder.State.Select({ "selector": "form input[type=text],input[type=hidden],select" });
        Builder.State.Select({
            "selector": "#" + Builder.Data.Get("EditorId"),
            infoField: Builder.Data.Get("DescField"),
            infoFieldGroup: Builder.Data.Get("Summary"),
            val: editor
        });
        if (Builder.Data.Get("FetchData")) {
            var summary = Builder.BrowserRepo.Get(Builder.Data.Get("DocumentId"), Builder.Data.Get("Summary"));
            if (summary) {
                setSummary(summary);
                Builder.State.Record();
            } else {
                Builder.Http.Get({ url: Builder.Data.Get("GetSummaryAddress") }).then(function(data) {
                    if (data) {
                        setSummary(data);
                        Builder.State.Record();
                        Builder.BrowserRepo.Set(Builder.Data.Get("DocumentId"), Builder.Data.Get("Summary"), data);
                    }
                });
            }
        }
    })();
    $(function() {
        $("#exampleContainer i").on("click",
            function() { Builder.Editor.AddList(Builder.Data.Get("EditorId"), $(this).next("p").html()); });
        $("#btnSaveAndNext").on("click",
            function(e) {
                if (e && e.preventDefault) {
                    e.preventDefault();
                }
                if (Builder.State.IsDirty()) {
                    var summary = getSummary();
                    Builder.Http.Post({ url: Builder.Data.Get("SaveSummaryAddress"), data: summary }).then(
                        function(data) {
                            if (data) {
                                Builder.BrowserRepo.Set(Builder.Data.Get("DocumentId"),
                                    Builder.Data.Get("Summary"),
                                    data);
                                location.href = Builder.Data.Get("NextScreenAddress");
                            }
                        });
                } else {
                    location.href = Builder.Data.Get("NextScreenAddress");
                }
            });
        getExamples(Builder.Data.Get("LoadExamplesAddress"), "");
    });

    function getExamples(url, jobTitle) {
        var jobTitleToBeSearched = RemoveSpecialCharacters(jobTitle);
        var examplesUrl = url + "&jobTitle=" + jobTitleToBeSearched + "&sectionCD=SUMM";
        Builder.Examples.Load(Builder.Data.Get("EditorId"), examplesUrl);
    }

    function RemoveSpecialCharacters(input) {
        if (input) {
            input = input.replace(/[\\\'\"\^\)\(\*\[\]]/g, "");
        }
        return input;
    }
})(jQuery, Builder);