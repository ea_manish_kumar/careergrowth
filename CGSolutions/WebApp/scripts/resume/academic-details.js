﻿(function($, Builder) {
    function getExamples() {
        Builder.Examples.Load(Builder.Data.Get("EditorId"), Builder.Data.Get("LoadExamplesAddress"));
    }

    Builder.Editor.Init(Builder.Data.Get("EditorId"), { placeholder: Builder.Data.Get("EditorPlaceHolder") });

    $(function() {
        $("#exampleContainer i").on("click",
            function() { Builder.Editor.AddParagraph(Builder.Data.Get("EditorId"), $(this).next("p").html()); });
        $("#btnSaveAndNext").on("click",
            function (e) {
                if (e && e.preventDefault) {
                    e.preventDefault();
                }
                isOnceClicked = true;
                Builder.Funnel.SaveSection();
            });
        getExamples();
    });
    //Builder.Plugin('google').LoadScript(Builder.Data.Get("GoogleAutoComplete")).Ready(function () {
    //    Builder.Places.Init({
    //        elemId: "school",
    //        fields: { "school": "placeName", "city": "locality", "state": "administrative_area_level_1" },
    //        types: ["establishment"],
    //        wizard: "place"
    //    });
    //    Builder.Places.Init({
    //        elemId: "city",
    //        fields: { "city": "locality", "state": "administrative_area_level_1" },
    //        types: ["(cities)"]
    //    });
    //});
})(jQuery, Builder);