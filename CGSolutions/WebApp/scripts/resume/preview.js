(function ($, Builder) {
    var currentSkinCD = Builder.Data.Get("Template"),
        $hdnSaveGroups = $("#hdnSaveGroups"),
        $txtOther = $("#txtOther"),
        $selectOptionMessage = $("#select-otion-error-message"),
        $editContainer = $("#edit-container"),
        $missingSections = $("#missing-groups"),
        $btnAddSectionContainer = $("#btnAddSectionContainer"),
        $addCheckboxes = $missingSections.find("input[type=checkbox][name=add-group-options]"),
        $skinTemplates = $("#all-templates"),
        //$skinColorThemes = $("#skin-color-themes"),
        $tabIcons = $("#tab-icons"),
        $tabContainer = $("#tab-container"),
        $themeContainer = $("#themeContainer"),
        $formattingContainer = $("#formattingContainer"),
        currentResumeIframe = document.getElementById("currentResume");

    function bindDocHtml(html) {
        if ($.trim(html)) {
            currentResumeIframe.contentWindow.document.open();
            currentResumeIframe.contentWindow.document.write(html);
            currentResumeIframe.contentWindow.document.close();
            //TODO: Check on different screens/resolutions if this calculation works
            var height = ($(pageSize).val() === "A4") ? 841.68 : 792;
            //Letter Size: 11inch * 72 dpi = 792px;
            //A4 Size: 11.69 inch * 72 dpi = 841.68;
            var iframeHeight = (Math.ceil(($(currentResumeIframe.contentWindow.document).height() / height)) > 1 ? $(currentResumeIframe.contentWindow.document).height() : height) + "px";

            //25% of the width is reserved for the left section
            var originalWidth = 892.5;
            var contWidth = $("#resumeContainer").width();
            var ratio = (contWidth) / originalWidth;
            if (ratio > 1)
                ratio = 1;
            $("#currentResume").css({
                'transform': "scale(" + ratio + ")",
                '-webkit-transform': "scale(" + ratio + ")",
                '-moz-transform': "scale(" + ratio + ")",
                '-o-transform': "scale(" + ratio + ")",
                '-ms-transform': "scale(" + ratio + ")",
                '-webkit-transform-origin': "top left",
                '-moz-transformtransform-origin': "top left",
                '-ms-transformtransform-origin': "top left",
                'transform-origin': "top left",
                'height': iframeHeight
            });
        }
    }

    function FetchDocumentPreview() {
        Builder.Loader.Append("#resumeContainer");
        Builder.Http.Get({ url: Builder.Data.Get("GetPreviewAddress"), dataType: "html" }).then(bindDocHtml).always(
            function () {
                Builder.Loader.Remove("#resumeContainer");
            });
    }

    function MissingCheckboxClick() {
        var addVal = $hdnSaveGroups.val();
        var additionalSectionsArr = addVal ? $hdnSaveGroups.val().split(",") : [];
        if ($(this).is(":checked")) {
            additionalSectionsArr.push($(this).val());
        } else {
            var index = additionalSectionsArr.indexOf($(this).val());
            if (index > -1) {
                additionalSectionsArr.splice(index, 1);
            }
        }
        if (additionalSectionsArr.length) {
            $selectOptionMessage.removeClass("display-block");
        }
        $hdnSaveGroups.val(additionalSectionsArr.join(","));
    }

    function AddToMissingSection(screenCD, sectionLabel) {
        var $span = $("<span />").addClass("sectionTitle").text(sectionLabel);
        var $input = $("<input />").addClass("input")
            .attr({ "type": "checkbox", "name": "add-group-options", "id": ("options-" + screenCD.toLowerCase()) })
            .val(screenCD).on("click", MissingCheckboxClick);
        var $label = $("<label />").addClass("check").attr("for", ("options-" + screenCD.toLowerCase()));
        var $div = $("<div />").addClass("addSectionRow");
        $div.append($span, $input, $label);
        $btnAddSectionContainer.before($div);
    }

    function UpdateDocStyle(styles) {
        Builder.Loader.Append("#resumeContainer");
        Builder.Http.Post({
            url: Builder.Data.Get("UpdateFormatAddress"),
            data: { "styles": styles },
            dataType: "html"
        }).then(bindDocHtml).always(function () {
            Builder.Loader.Remove("#resumeContainer");
        });
    }

    function UpdateSkin(skinCD) {
        Builder.Loader.Append("#resumeContainer");
        Builder.Http.Post({
            url: Builder.Data.Get("UpdateTemplateAddress"),
            data: { "templateCD": skinCD },
            dataType: "html"
        }).then(bindDocHtml).then(ToggleColorTheme.bind(null, skinCD)).always(function () {
            Builder.Loader.Remove("#resumeContainer");
            CloseChangeTemplateOverlay();
        });
    }

    function CloseChangeTemplateOverlay() {
        var oldTabName = $("#changeTemplateContainer").hide().attr("data-old-tab-name");
        $tabIcons.find("li[data-action-tabs=change-template]").removeClass("active");
        $tabIcons.find("li[data-action-tabs=" + oldTabName + "]").addClass("active");
        $tabContainer.find("[data-action-tab-cont=" + oldTabName + "]").show();
        $("body").removeClass("overflowHidden");
    }
    function iframeZoom() {
        var $this = $(this);
        var contWidth = $this.parent().width(), ratio = (contWidth) / 226;
        var IframeHeight = ($this.parent().height() - 10).toString() + "px";
        $this.css({ 'height': IframeHeight }).contents().find("html").css({
            'transform': "scale(" + ratio + ")",
            '-webkit-transform': "scale(" + ratio + ")",
            '-moz-transform': "scale(" + ratio + ")",
            '-o-transform': "scale(" + ratio + ")",
            '-ms-transform': "scale(" + ratio + ")",
            '-webkit-transform-origin': "top left",
            '-moz-transformtransform-origin': "top left",
            '-ms-transformtransform-origin': "top left",
            'transform-origin': "top left"
        });
    }

    $(window).on("load",
        function () {
            $skinTemplates.find("iframe").each(iframeZoom);
        });

    function ToggleColorTheme(skinCD) {
        $themeContainer.find("[data-template-cd]").each(function () {
            var $this = $(this);
            if ($this.attr("data-template-cd") == skinCD) {
                $this.removeClass("display-none");
            } else {
                $this.addClass("display-none");
            }
        });
    }

    function BindDelete() {
        $editContainer.find("button[data-action=delete]").on("click",
            function (e) {
                $("#btn-delete-group").attr("data-group-id", $(this).attr("data-group-id"));
                $("#delete-modal").modal();
            });
        $("#btn-delete-group").on("click",
            function () {
                var sectionId = $(this).attr("data-group-id");
                var $sectionItem = $editContainer.find("[data-group-edit-id=" + sectionId + "]");
                if ($sectionItem.length) {
                    Builder.Http
                        .Post({ "url": Builder.Data.Get("DeleteGroupAddress"), "data": { "groupId": sectionId } }).then(
                            function (result) {
                                if (result && result.success === true) {
                                    if (result.isDocumentDeleted === true) {
                                        location.href = Builder.Data.Get("DashboardAddress");
                                    } else {
                                        FetchDocumentPreview();
                                        var screenCD = $sectionItem.attr("data-screen-cd");
                                        var sectionLabel = $sectionItem.attr("data-section-label");
                                        var sectionIcon = $sectionItem.attr("data-section-icon");
                                        AddToMissingSection(screenCD, sectionLabel, sectionIcon);
                                        $sectionItem.remove();
                                        $("#delete-modal").modal("hide");
                                    }
                                }
                            });
                }
            });
    }

    function BindAddSection() {
        $("#addSection").on("focus",
            "#txtOTHR",
            function () {
                $('#addSection :input[value="OTHR"]').attr("checked", "checked");
                $(this).removeClass("invalid");
                $("#errtxtOthers").removeClass("display-block");
            });
        $addCheckboxes.on("click", MissingCheckboxClick);
        $("#btnAddSections").on("click",
            function () {
                var addVal = $hdnSaveGroups.val();
                var additionalSectionsArr = addVal ? $hdnSaveGroups.val().split(",") : [];
                if (additionalSectionsArr.length > 0) {
                    $selectOptionMessage.removeClass("display-block");
                    Builder.Http
                        .Post({
                            "url": Builder.Data.Get("SaveGroupAddress"),
                            "data": { "sections": additionalSectionsArr, "otherText": $txtOther.val() }
                        }).then(function (data) {
                            if (data && data.result === true && data.nextRoute) {
                                location.href = data.nextRoute;
                            }
                        });
                } else {
                    $selectOptionMessage.addClass("display-block");
                }
            });
    }

    function validateEmail() {
        if (isOnceClicked) {
            $("#errEmailTo").hide();
            var $this = $("#recipient-address");
            $this.removeClass("invalid");
            var email = $this.val();
            if (email) {
                var emailRegex = new RegExp(Builder.Data.Get("EmailPattern"));
                if (!emailRegex.test(email)) {
                    $("#errEmailTo").show();
                    $this.addClass("invalid");
                    return false;
                }
            } else {
                $("#errEmailTo").show();
                $this.addClass("invalid");
                return false;
            }
        }
        return true;
    }

    function validateFromName() {
        $("#errFromName").hide();
        var $this = $("#from-name");
        $this.removeClass("invalid");
        var fromName = $this.val();
        if (!fromName) {
            $("#errFromName").show();
            $this.addClass("invalid");
            return false;
        }
        return true;
    }

    var isOnceClicked = false;

    function BindInputs() {
        $("#recipient-address").on("input", validateEmail);
        $("#from-name").on("input", validateFromName);
    }

    function BindBtnClicks() {
        $("#downloadResume").on("click",
            function () {
                location.href = Builder.Data.Get("DownloadAddress");
            });

        $("#printResume").on("click",
            function () {
                //var printContents = currentResumeIframe.contentDocument.body.innerHTML;
                //var originalContents = document.body.innerHTML;
                //document.body.innerHTML = printContents;
                //window.print();
                currentResumeIframe.contentWindow.print();
                //document.body.innerHTML = originalContents;
            });

        $("#emailResume").on("click",
            function () {
                $("#email-modal").modal();
            });

        $("#btn-email-section").on("click",
            function () {
                isOnceClicked = true;
                var validation = [validateEmail(), validateFromName()];
                if (validation.indexOf(false) > -1) {
                    return;
                }
                var objEmail = {
                    "recipientAddress": $("#recipient-address").val(),
                    "fromName": $("#from-name").val(),
                    "emailSubject": $("#email-subject").val(),
                    "emailBody": $("#email-body").val()
                };
                Builder.Http.Post({ "url": Builder.Data.Get("EmailAddress"), "data": objEmail }).then(function (result) {
                    if (result === true) {
                        $("#email-modal").modal("hide");
                    }
                });
            });
        $("#closeIcon").on("click", CloseChangeTemplateOverlay);
    }

    function BindFormattingOptions() {
        $formattingContainer.find("#fontFamily,#headingFontFamily,#fontSize,#resumeMargin,#pageSize,#resumeBg").on("change",
            function () {
                var $this = $(this);
                var styleCD = $this.attr("data-style-cd");
                var styleCDs = {};
                styleCDs[styleCD] = $this.val();
                UpdateDocStyle(styleCDs);
            });
    }

    function BindSkinChanges() {
        $tabIcons.find("li").on("click",
            function () {
                var $currentTab = $tabIcons.find("li.active");
                $currentTab.removeClass("active");
                var oldTabName = $currentTab.attr("data-action-tabs");
                $tabContainer.find("[data-action-tab-cont=" + oldTabName + "]").hide();
                var newTabName = $(this).addClass("active").attr("data-action-tabs");
                if (newTabName === "change-template") {
                    $("#changeTemplateContainer").attr("data-old-tab-name", oldTabName).show();
                    $("body").addClass("overflowHidden");
                } else {
                    if ($("#changeTemplateContainer").attr("data-old-tab-name")) {
                        $("#changeTemplateContainer").attr("data-old-tab-name", "").hide();
                        $("body").removeClass("overflowHidden");
                    }
                    $tabContainer.find("[data-action-tab-cont=" + newTabName + "]").show();
                }
            });
        $themeContainer.find("[data-theme-cd]").on("click",
            function () {
                var themeCD = {};
                themeCD[Builder.Data.Get("FormatOptionTheme")] = $(this).attr("data-theme-cd");
                UpdateDocStyle(themeCD);
            });
        $skinTemplates.find("[data-template-cd]").on("click",
            function () {
                UpdateSkin($(this).attr("data-template-cd"));
            });
    }

    function InitializeSorting() {
        $editContainer.sortable({
            items: "[data-group-edit-id]:not([data-screen-cd=" + Builder.Data.Get("UserDetailsScreenCode") + "])",
            containment: $editContainer,
            stop: function () {
                var sortedSections = [];
                $editContainer.find("[data-group-edit-id]").each(function (ind) {
                    sortedSections.push({
                        "groupId": $(this).attr("data-group-edit-id"),
                        sortIndex: (ind + 1).toString()
                    });
                });
                if (sortedSections && sortedSections.length) {
                    Builder.Loader.Append("#resumeContainer");
                    Builder.Http.Post({
                        url: Builder.Data.Get("SortGroupAddress"),
                        data: { "sortedGroups": sortedSections },
                        dataType: "html"
                    }).then(bindDocHtml).always(function () {
                        Builder.Loader.Remove("#resumeContainer");
                    });
                }
            }
        });
    }

    FetchDocumentPreview();
    $(function () {
        ToggleColorTheme(currentSkinCD);
        BindInputs();
        BindDelete();
        BindAddSection();
        BindBtnClicks();
        BindFormattingOptions();
        BindSkinChanges();
        InitializeSorting();
    });
})(jQuery, Builder);