﻿(function($, Builder) {
    $(function () {
        var $sortBoundary = $("#sortBoundary");
        $sortBoundary.sortable({
            items: "[data-sort-item]",
            containment: "#sortBoundary",
            stop: function () {
                Builder.Funnel.SortSubSections();
            }
        });
    });
})(jQuery, Builder);