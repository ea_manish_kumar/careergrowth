﻿(function ($, Builder) {
    var isOnceClicked = false;
    function verifyEmail() {
        if (isOnceClicked) {
            $("#errEmailValid").hide();
            $("#email").removeClass("invalid");
            var contactEmail = $("#email").val();
            if (contactEmail) {
                var emailRegex = new RegExp(Builder.Data.Get("EmailPattern"));
                if (!emailRegex.test(contactEmail)) {
                    $("#errEmailValid").show();
                    $("#email").addClass("invalid");
                    return false;
                }
            }
        }
        return true;
    }

    function verifyPhone() {
        $("#errPhoneValidMsg").hide();
        $("#phone").removeClass("invalid");
        var contactNumber = $("#phone").val();
        if (contactNumber) {
            if (contactNumber.length > 15) {
                $("#phone").addClass("invalid");
                return false;
            }
            var phoneRegex = new RegExp(Builder.Data.Get("PhoneNumberPattern"));
            if (!phoneRegex.test(contactNumber)) {
                $("#errPhoneValidMsg").show();
                $("#phone").addClass("invalid");
                return false;
            }
        }
        return true;
    }

    function verifyAltPhone() {
        $("#errAltPhoneValidMsg").hide();
        $("#altPhone").removeClass("invalid");
        var contactNumber = $("#altPhone").val();
        if (contactNumber) {
            if (contactNumber.length > 15) {
                $("#altPhone").addClass("invalid");
                return false;
            }
            var phoneRegex = new RegExp(Builder.Data.Get("PhoneNumberPattern"));
            if (!phoneRegex.test(contactNumber)) {
                $("#errAltPhoneValidMsg").show();
                $("#altPhone").addClass("invalid");
                return false;
            }
        }
        return true;
    }

    function verifyZipCode() {
        $("#errZipCodeValid").hide();
        $("#zipCode").removeClass("invalid");
        var zipCode = $("#zipCode").val();
        if (zipCode) {
            if (zipCode.length > 10) {
                $("#zipCode").addClass("invalid");
                return false;
            }
            var zipCodeRegex = new RegExp(Builder.Data.Get("ZipCodePattern"));
            if (!zipCodeRegex.test(zipCode)) {
                $("#errZipCodeValid").show();
                $("#zipCode").addClass("invalid");
                return false;
            }
        }
        return true;
    }

    $(function () {
        $("#email").on("keyup", function () { verifyEmail(); });
        $("#phone").on("keyup", function () { verifyPhone(); });
        $("#zipCode").on("keyup", function () { verifyZipCode(); });
        $("#btnProceed").on("click",
            function (e) {
                isOnceClicked = true;
                if (e && e.preventDefault) {
                    e.preventDefault();
                }
                var phoneVerified = verifyPhone();
                var altPhoneVerified = verifyAltPhone();
                var zipCodeVerified = verifyZipCode();
                var emailVerified = verifyEmail();
                if (!phoneVerified || !altPhoneVerified || !zipCodeVerified || !emailVerified) {
                    return;
                }
                Builder.Funnel.SaveSection();
            });
    });
    //Builder.Plugin('google').LoadScript(Builder.Data.Get("GoogleAutoComplete")).Ready(function () {
    //    Builder.Places.Init({
    //        elemId: "address",
    //        fields: { "address": "route", "city": "locality", "state": "administrative_area_level_1", "zipCode": "postal_code" },
    //        types: ["address"],
    //        wizard: "address"
    //    });
    //    Builder.Places.Init({
    //        elemId: "city",
    //        fields: { "city": "locality", "state": "administrative_area_level_1", "zipCode": "postal_code" },
    //        types: ["(cities)"]
    //    });
    //});
})(jQuery, Builder);