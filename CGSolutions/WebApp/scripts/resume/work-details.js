﻿(function($, Builder) {
    function setPresentlyWorkHere(isChecked) {
        $("#endMonth,#endYear").attr("data-val-preserve",
            function() {
                if (isChecked) {
                    return $(this).val();
                }
            }).val(function() {
            return isChecked ? $(this).attr("data-curr-date") : $(this).attr("data-val-preserve");
        }).prop("disabled", isChecked);
    }

    function presentlyWorkHere() {
        if ($("#presentlyWorkHere").prop("checked")) {
            setPresentlyWorkHere(true);
        }
    }

    var isOnceClicked = false;

    function validate() {
        var isValid = true;
        if (!validateEmployerJobTitle()) {
            isValid = false;
        }
        if (!validateStartDate()) {
            isValid = false;
        }
        if (!validateEndtDate()) {
            isValid = false;
        }
        if (!validateDateComparison()) {
            isValid = false;
        }
        return isValid;
    }

    function validateEmployerJobTitle() {
        if (isOnceClicked) {
            $("#errProvideEmpMsg,#errProvideJobTitleMsg").hide();
            $("#employer,#jobTitle").removeClass("invalid");
            var isEmpExists = $.trim($("#employer").val()) !== "";
            var isJobTitleExists = $.trim($("#jobTitle").val()) !== "";
            if (isEmpExists !== isJobTitleExists) {
                if (!isEmpExists) {
                    $("#errProvideEmpMsg").show();
                    $("#employer").addClass("invalid");
                }
                if (!isJobTitleExists) {
                    $("#errProvideJobTitleMsg").show();
                    $("#jobTitle").addClass("invalid");
                }
                return false;
            }
        }
        return true;
    }

    function validateDate(errMsgId, ddlMonthId, ddlYearId) {
        $(errMsgId).hide();
        $(ddlMonthId + "," + ddlYearId).removeClass("invalid");
        var month = parseInt($(ddlMonthId).val());
        var year = parseInt($(ddlYearId).val());
        if ((month > 0 && year <= 0) || (year > 0 && month <= 0)) {
            $(errMsgId).show();
            if (month > 0) {
                $(ddlYearId).addClass("invalid")
            }
            if (year > 0) {
                $(ddlMonthId).addClass("invalid")
            }
            return false;
        }
        return true;
    }

    function validateStartDate() {
        return !isOnceClicked || validateDate("#errStartDateMsg", "#startMonth", "#startYear");
    }

    function validateEndtDate() {
        return !isOnceClicked || validateDate("#errEndDateMsg", "#endMonth", "#endYear");
    }

    function validateDateComparison() {
        if (isOnceClicked) {
            var startMonth = parseInt($("#startMonth").val());
            var startYear = parseInt($("#startYear").val());
            var endMonth = parseInt($("#endMonth").val());
            var endYear = parseInt($("#endYear").val());
            if (startMonth > 0 && startYear > 0 && endMonth > 0 && endYear > 0) {
                if ((endYear < startYear) || (endYear == startYear && endMonth < startMonth)) {
                    $("#errStartEndDateMsg").show();
                    return false;
                }
            }
            $("#errStartEndDateMsg").hide();
        }
        return true;
    }

    Builder.Editor.Init(Builder.Data.Get("EditorId"), { placeholder: Builder.Data.Get("EditorPlaceHolder") });
    $(function() {
        $("#employer,#jobTitle").on("keyup", function() { validateEmployerJobTitle(); });
        $("#startMonth,#startYear").on("change",
            function() {
                validateStartDate();
                validateDateComparison();
            });
        $("#endMonth,#endYear").on("change",
            function() {
                validateEndtDate();
                validateDateComparison();
            });
        $("#presentlyWorkHere").on("click",
            function(e) {
                var isChecked = $(this).prop("checked");
                setPresentlyWorkHere(isChecked);
            });
        $("#exampleContainer i").on("click",
            function() { Builder.Editor.AddParagraph(Builder.Data.Get("EditorId"), $(this).next("p").html()); });
        $("#btnSaveAndNext").on("click",
            function(e) {
                if (e && e.preventDefault) {
                    e.preventDefault();
                }
                isOnceClicked = true;
                if (validate()) {
                    Builder.Funnel.SaveSection();
                }
            });
        Builder.Examples.Load(Builder.Data.Get("EditorId"), Builder.Data.Get("LoadExamplesAddress"), "");
        Builder.Examples.Autocomplete(Builder.Data.Get("EditorId"), Builder.Data.Get("SearchJobTitlesAddress"), Builder.Data.Get("SearchExamplesAddress"));
    });
    //Builder.Plugin('google').LoadScript(Builder.Data.Get("GoogleAutoComplete")).Ready(function () {
    //    Builder.Places.Init({
    //        elemId: "city",
    //        fields: { "city": "locality", "state": "administrative_area_level_1" },
    //        types: ["(cities)"]
    //    });
    //});
})(jQuery, Builder);