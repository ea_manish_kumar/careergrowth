﻿(function($, App) {
    function skinFactory() {
        var skins = null;
        var data = null;
        var getTemplates = null;

        this.Init = function() {
            getTemplates = Builder.Http.Get({ url: Builder.Urls.GetSkinTemplates }).then(function(templates) {
                if (templates) {
                    skins = {};
                    var skinParent = document.createElement("div");
                    skinParent.style.display = "none";
                    document.body.appendChild(skinParent);
                    for (var i = 0, len = templates.length; i < len; i++) {
                        var iframe = document.createElement("iframe");
                        iframe.style.display = "none";
                        iframe.setAttribute("skin", templates[i].skin);
                        skinParent.appendChild(iframe);
                        var iDoc = iframe.contentWindow.document;
                        iDoc.open();
                        iDoc.write(templates[i].template);
                        iDoc.close();
                        skins[templates[i].skin] = { "skinNode": iframe };
                    }
                    window._skins = skins;
                    window._templates = templates;
                }
            });
        };
        this.SetData = function(docData) {
            data = docData;
            return this;
        };
        this.Ready = function(func) {
            getTemplates.then(function() {
                func();
            });
            return this;
        };
        this.GetSkinNode = function(skin) {
            if (skins && skins[skin]) {
                return skins[skin].skinNode.cloneNode();
            }
            return null;
        };
    }

    $(function() {
        Builder.Skins = new skinFactory();
        Builder.Skins.Init();
    });
})(window.jQuery, window.Builder);