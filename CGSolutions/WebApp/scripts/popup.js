(function($, Builder) {
    function popupFactory() {
        var $html = null,
            $popupContainer = null,
            $popupContentArea = null,
            $popupClose = null,
            $popupModalTitle = null,
            $popupContent = null,
            $popupBtnCancel = null,
            $popupBtnCTA = null,
            settings = null;

        this.Open = function(options) {
            $popupContainer.show();
            settings = options;
            if (settings.customClass) {
                $popupContentArea.addClass(settings.customClass);
            }

            var $elemId = $("#" + settings.elemId).show();
            if ($elemId.attr("data-modal-popup") !== "true") {
                $elemId.attr("data-modal-popup", "true").show();
                $popupContent.append($elemId);
            }
            $popupModalTitle.text(settings.title);
            $popupBtnCancel.text(settings.cancelButtonText);
            $popupBtnCancel.addClass(settings.ctaCancelClass);
            $popupBtnCTA.text(settings.ctaButtonText);
            $popupBtnCTA.addClass(settings.ctaClass);
            $popupContainer.addClass("show-me");
            $html.addClass("hide-body-scroll");
        };
        this.Close = function() {
            $popupContainer.removeClass("show-me");
            $html.removeClass("hide-body-scroll");
            if (settings != null && settings.customClass)
                $popupContentArea.removeClass(settings.customClass);

            if (settings != null && settings.ctaClass)
                $popupBtnCTA.removeClass(settings.ctaClass);

            if (settings != null && settings.ctaCancelClass)
                $popupBtnCancel.removeClass(settings.ctaCancelClass);

            if (settings != null) {
                $("#" + settings.elemId).hide();
                $popupContainer.hide();
            }
            settings = null;
        };
        this.Init = function() {
            $(function() {
                $html = $("html");
                $popupContainer = $("#popupContainer");
                $popupContentArea = $("#popupContentArea");
                $popupClose = $("#popupClose");
                $popupModalTitle = $("#popupModalTitle");
                $popupContent = $("#popupContent");
                $popupBtnCancel = $("#popupBtnCancel");
                $popupBtnCTA = $("#popupBtnCTA");
                $popupClose.on("click", this.Close);
                $popupBtnCancel.on("click",
                    function(e) {
                        if (settings.cancelButtonFunc) {
                            settings.cancelButtonFunc.apply(this, arguments);
                        }
                        if (e && e.preventDefault) {
                            e.preventDefault();
                        }
                        this.Close();
                    }.bind(this));
                $popupBtnCTA.on("click",
                    function() {
                        if (settings.ctaButtonFunc) {
                            settings.ctaButtonFunc.apply(this, arguments);
                        }
                    });
            }.bind(this));
        };
    };

    Builder.PopUp = new popupFactory();
    Builder.PopUp.Init();
}(jQuery, window.Builder));