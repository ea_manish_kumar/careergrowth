﻿(function(Builder, $) {
    var $exampleContainer = null, $searchBox = null, $searchButton = null;
    $(function() {
        $exampleContainer = $("#exampleContainer");
        $searchBox = $("#searchBox");
        $searchButton = $("#searchButton");
    });

    window.Builder = window.Builder || {};
    window.Builder.Examples = window.Builder.Examples || {};
    var Examples = window.Builder.Examples;
    Examples.Load = function(editorId, url, jobTitle) {
        Builder.Http.Get({ url: url + "&jobTitle=" + EscapeSpecialCharacters(jobTitle) }).then(function(result) {
            if (result && result.examples) {
                renderExamples(editorId, result.examples);
            }
        });
    };

    Examples.Autocomplete = function(editorId, searchJobTitlesUrl, examplesUrl) {
        $searchBox.autocomplete({
            minLength: 3,
            delay: 100,
            source: function(request, response) {
                Builder.Http.Get({ url: searchJobTitlesUrl + "?jobTitle=" + EscapeSpecialCharacters(request.term) })
                    .then(function(data) {
                        response($.map(data,
                            function(item) {
                                return { label: item };
                            }));
                    });
            },
            open: function(event, ui) {
                $(".ui-autocomplete").off("menufocus hover mouseover mouseenter");
            },
            select: function(event, ui) {
                $searchBox.val(ui.item.label);
                Examples.Load(editorId, examplesUrl, ui.item.label);
            },
            change: function(event, ui) {
                if (ui.item != null) {
                    if (ui.item) {
                        $("#hdnJobTitle").val(ui.item.label);
                    }
                }
            }
        });
        $searchButton.on("click",
            function() {
                var searchbox = $searchBox.val();
                if (searchbox !== "")
                    Examples.Load(editorId, examplesUrl, ui.item.label);
            });
    };

    function renderExamples(editorId, examples) {
        $exampleContainer.empty();
        for (var i = 0; i < examples.length; i++) {
            var $liExampleListItem = $("<li />").attr("title", "Click to add").addClass("hoverExample");
            var $iExampleAddIcon = $("<i />").addClass("fa-li fa fa-plus add-example").on("click",
                (function(editorId, example) { return function() { addExampleToEditor(editorId, example); } })(editorId,
                    examples[i]));
            var $pExampleText = $("<span />").addClass("example-text").text(examples[i]).on("click",
                (function(editorId, example) { return function() { addExampleToEditor(editorId, example); } })(editorId,
                    examples[i]));
            $liExampleListItem.append($iExampleAddIcon);
            $liExampleListItem.append($pExampleText);
            $exampleContainer.append($liExampleListItem);
        }
    }

    function EscapeSpecialCharacters(input) {
        if (input) {
            input = input.replace(/[\\\'\"\^\)\(\*\[\]]/g, "");
        }
        return input;
    }

    function addExampleToEditor(editorId, example) {
        Builder.Editor.AddList(editorId, example);
    }
})(window.Builder, jQuery);