﻿/*
 * Undo.js - A undo/redo framework for JavaScript
 * 
 * http://jzaefferer.github.com/undo
 *
 * Copyright (c) 2011 Jörn Zaefferer
 * 
 * MIT licensed.
 */
(function() {

    // based on Backbone.js' inherits	
    var ctor = function() {};
    var inherits = function(parent, protoProps) {
        var child;

        if (protoProps && protoProps.hasOwnProperty("constructor")) {
            child = protoProps.constructor;
        } else {
            child = function() { return parent.apply(this, arguments); };
        }

        ctor.prototype = parent.prototype;
        child.prototype = new ctor();

        if (protoProps) extend(child.prototype, protoProps);

        child.prototype.constructor = child;
        child.__super__ = parent.prototype;
        return child;
    };

    function extend(target, ref) {
        var name, value;
        for (name in ref) {
            value = ref[name];
            if (value !== undefined) {
                target[name] = value;
            }
        }
        return target;
    };

    var Undo = {
        version: "0.1.15"
    };

    Undo.Stack = function() {
        this.commands = [];
        this.stackPosition = -1;
        this.savePosition = -1;
    };

    extend(Undo.Stack.prototype,
        {
            execute: function(command) {
                this._clearRedo();
                command.execute();
                this.commands.push(command);
                this.stackPosition++;
                this.changed();
            },
            undo: function() {
                this.commands[this.stackPosition].undo();
                this.stackPosition--;
                this.changed();
            },
            canUndo: function() {
                return this.stackPosition >= 0;
            },
            redo: function() {
                this.stackPosition++;
                this.commands[this.stackPosition].redo();
                this.changed();
            },
            canRedo: function() {
                return this.stackPosition < this.commands.length - 1;
            },
            save: function() {
                this.savePosition = this.stackPosition;
                this.changed();
            },
            dirty: function() {
                return this.stackPosition != this.savePosition;
            },
            _clearRedo: function() {
                // TODO there's probably a more efficient way for this
                this.commands = this.commands.slice(0, this.stackPosition + 1);
            },
            changed: function() {
                // do nothing, override
            }
        });

    Undo.Command = function(name) {
        this.name = name;
    };

    var up = new Error("override me!");

    extend(Undo.Command.prototype,
        {
            execute: function() {
                throw up;
            },
            undo: function() {
                throw up;
            },
            redo: function() {
                this.execute();
            }
        });

    Undo.Command.extend = function(protoProps) {
        var child = inherits(this, protoProps);
        child.extend = Undo.Command.extend;
        return child;
    };

    // AMD support
    if (typeof define === "function" && define.amd) {
        // Define as an anonymous module
        define(Undo);
    } else if (typeof module != "undefined" && module.exports) {
        module.exports = Undo;
    } else {
        this.Undo = Undo;
    }
}).call(this);

/*@addyosmani*/
(function(win, $) {
    function htmlEditor(id) {
        var x = document.querySelector.bind(document);
        var $dv = $("#" + id);
        this.Init = function() {
            var text = x("#" + id);
            var startValue = text.innerHTML;
            var blocked = false;
            var undo = x(".undo");
            var redo = x(".redo");
            var list = x(".list");
            var stack = new Undo.Stack();
            var newValue = "";

            var MutationObserver =
                window.MutationObserver || window.WebKitMutationObserver || window.MozMutationObserver;

            var observer = new MutationObserver(function(mutations) {
                if (blocked) {
                    blocked = false;
                    return;
                }
                newValue = text.innerHTML;
                stack.execute(new EditCommand(text, startValue, newValue));
                startValue = newValue;
            });

            observer.observe(text,
                {
                    attributes: true,
                    childList: true,
                    characterData: true,
                    characterDataOldValue: true,
                    subtree: true
                });

            var EditCommand = Undo.Command.extend({
                constructor: function(textarea, oldValue, newValue) {
                    this.textarea = textarea;
                    this.oldValue = oldValue;
                    this.newValue = newValue;
                },
                execute: function() {},
                undo: function() {
                    blocked = true;
                    this.textarea.innerHTML = this.oldValue;
                },

                redo: function() {
                    blocked = true;
                    this.textarea.innerHTML = this.newValue;
                }

            });


            function stackUI() {
                redo.disabled = !stack.canRedo();
                undo.disabled = !stack.canUndo();
            }

            stackUI();

            function insertUnorderedList() {
                document.execCommand("insertUnorderedList", false, null);
            }


            undo.addEventListener("mousedown",
                function() {
                    stack.undo();
                });

            redo.addEventListener("mousedown",
                function() {
                    stack.redo();
                });

            list.addEventListener("mousedown",
                function() {
                    insertUnorderedList();
                });
            stack.changed = function() {
                stackUI();
            };
        };
        this.GetContent = function() {
            return $dv.html();
        };
        this.SetContent = function(html) {
            $dv.html(html);
        };
        this.AddParagraph = function(content) {
            $("<p/>").html(content).appendTo($dv);
        };

        this.AddList = function(content) {
            var list = $dv.find("ul");
            if (list.length == 0)
                list = $dv.append("<ul></ul>").find("ul");
            $("<li/>").html(content).appendTo(list);
        };

        $dv.on("focus", (function() { $dv.find("span").css("color", "") }));

        $dv.on("click",
            "span",
            function() {
                var text_node = $(this)[0];
                var range = document.createRange();
                range.setStart(text_node, 0);
                range.setEnd(text_node, 1);
                var sel = window.getSelection();
                sel.removeAllRanges();
                sel.addRange(range);
            });
    };

    win.HtmlEditor = win.HtmlEditor ||
    {
        Create: function(id) {
            var editor = new htmlEditor(id);
            editor.Init();
            return editor;
        }
    };
})(window, window.jQuery);