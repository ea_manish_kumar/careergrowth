﻿

(function(win, $) {

    function editorFactory() {
        var editors = {};
        var HtmlEditor = win.HtmlEditor;
        this.Init = function(id, obj) {
            editors[id] = HtmlEditor.Create(id);
        };
        this.GetContent = function(id) {
            return editors[id].GetContent();
        };
        this.SetContent = function(id, content) {
            editors[id].SetContent(content);
        };
        this.AddParagraph = function(id, content) {
            editors[id].AddParagraph(content);
        };
        this.AddList = function(id, content) {
            editors[id].AddList(content);
        };
    };

    win.Builder = win.Builder || {};
    var Builder = win.Builder;
    Builder.Editor = new editorFactory();
})(window, window.jQuery);