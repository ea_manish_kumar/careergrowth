﻿(function($, Builder) {
    function fillInAddress(autoComplete, fields, wizard) {
        var isLocalityAvailable = false;
        var localityField = {
            'colloquial_area': "",
            'sublocality': "",
            'administrative_area_level_2': "",
            'administrative_area_level_3': ""
        };
        var streetField = {
            'street_number': "",
            'route': "",
            'neighborhood': "",
            'street_address': "",
            'sublocality_level_3': "",
            'sublocality_level_2': "",
            'sublocality_level_1': ""
        };
        var place = autoComplete.getPlace();
        for (var keyId in fields) {
            $("#" + keyId).val("");
        }
        for (var j = 0, len = place.address_components.length; j < len; j++) {
            var types = place.address_components[j].types;
            if (types && types.length) {
                var att = types[0];
                if (att === "locality") {
                    isLocalityAvailable = true;
                }
                if (att in localityField) {
                    localityField[att] = place.address_components[j].long_name;
                }
                if (att in streetField) {
                    streetField[att] = place.address_components[j].long_name;
                }
                if (att === "administrative_area_level_1") {
                    fillFormInput(att, place.address_components[j].short_name, fields);
                } else if (att != "route") {
                    fillFormInput(att, place.address_components[j].long_name, fields);
                }
            }
        }
        if (!isLocalityAvailable) {
            fillFormInput("locality", getField(localityField), fields);
        }
        if (wizard === "address") {
            fillFormInput("route", getRouteField(streetField), fields);
        }
        if (wizard === "place") {
            fillFormInput("placeName", place.name, fields);
        }
    }

    function fillFormInput(att, val, wizard) {
        for (var c in wizard) {
            if (wizard[c] === att) {
                $("#" + c).val(val);
            }
        }
    }

    function getField(field) {
        for (var prop in field) {
            if (field[prop]) {
                return field[prop];
            }
        }
        return "";
    }

    function getRouteField(streetField) {
        var outputString = "";
        for (var prop in streetField) {
            if (streetField[prop]) {
                outputString = outputString + " " + streetField[prop];
            }
        }
        return $.trim(outputString);
    }

    function placesFactory() {
        this.Init = function(obj) {
            if (obj) {
                var autoComp =
                    new google.maps.places.Autocomplete(document.getElementById(obj.elemId), { types: obj.types });
                google.maps.event.addListener(autoComp,
                    "place_changed",
                    function() { fillInAddress(autoComp, obj.fields, obj.wizard); });
            }
        };
    }

    Builder.Places = new placesFactory();
})(jQuery, window.Builder);