﻿(function($) {
    window.Builder = window.Builder || {};
    var Builder = window.Builder;

    var _data = {};
    Builder.Data = {
        Get: function(key) {
            return _data[key];
        },
        Set: function(obj) {
            $.extend(_data, obj);
        }
    };
    //var $jsonProps = $("#jsonProps");
    //Builder.Data = {
    //    Get: function (key) {
    //        return $jsonProps.attr(key);
    //    }
    //};

    if (Builder.TempData) {
        Builder.Data.Set(Builder.TempData);
        Builder.TempData = undefined;
    }

    var _plugins = {};
    Builder.Plugin = function(namespace) {
        if (!_plugins[namespace]) {
            _plugins[namespace] = new _plugin(namespace);
        }
        return _plugins[namespace];
    };

    function ajaxSuccess(data) { return data; }

    function ajaxError(err) { return err; }

    function ajax(obj, method) {
        var ajaxLoadTimeout = null;
        if (obj.disableLoader !== true) {
            ajaxLoadTimeout = setTimeout(Builder.Loader.Show, 300);
        }
        return $.ajax({
            url: obj.url,
            type: method,
            data: JSON.stringify(obj.data),
            contentType: "application/json; charset=utf-8",
            dataType: (obj.dataType || "json"),
            async: true,
            timeout: obj.timeout
        }).then(ajaxSuccess, ajaxError).always(function() {
            if (obj.disableLoader !== true) {
                Builder.Loader.Hide();
                if (ajaxLoadTimeout) {
                    clearTimeout(ajaxLoadTimeout);
                }
            }
        });
    }

    Builder.Http = {};
    Builder.Http.Get = function(obj) { return ajax(obj, "GET"); };
    Builder.Http.Post = function (obj) { return ajax(obj, "POST"); };
    Builder.User = {};
    Builder.User.UpdateProgress = function () {
        var url = Builder.Data.Get("TrackPageViewAddress");
        if (url) {
            var obj = { url: url, disableLoader: true };
            ajax(obj, "POST");
        }
    };

    Builder.Loader = {
        Show: function() {
        },
        Hide: function() {
        },
        Append: function(selector) {
            $(selector).prepend($("<div/>").addClass("loading-widget"));
        },
        Remove: function(selector) {
            $(selector).find(".loading-widget").remove();
        }
    };

    function trackPageView() {
        var url = Builder.Data.Get("TrackPageViewAddress");
        if (url) {
            ajax({ url: url, disableLoader: true }, "POST");
        }
    }

    $(function() {
        $("#menu").click(function() { $(".collapseMenu").addClass("showMenu"); });
        $("#menuRight").click(function() { $(".collapseMenuRight").addClass("showMenuRight"); });
        trackPageView();
    });
})(jQuery);