﻿(function ($, Builder) {
    function gotoNextRoute() {
        location.href = Builder.Data.Get("NextRouteAddress");
    }

    $(function () {
        $("#select-template a[info-template]").on("click", function (e) {
            if (e && e.preventDefault) {
                e.preventDefault();
            }
            var template = $(this).attr("info-template");
            if (template != Builder.Data.Get("SkinCD")) {
                var url = Builder.Data.Get("SaveDocumentAddress") || Builder.Data.Get("UpdateDocumentAddress");
                Builder.Http.Post({ url: url, data: { "skinCD": template } }).then(function () {
                    gotoNextRoute();
                });
            } else {
                gotoNextRoute();
            }
        });
    });
})(jQuery, Builder);