﻿(function($, Builder) {
    function squireEditor(id) {
        var editor = null, $root = null, $dv = null, $placeholder = null, $undo = null, $redo = null, $btns = null;

        function initEditorEvents() {
            editor.addEventListener("focus", hidePlaceHolder).addEventListener("blur",
                function() {
                    checkAndShowPlaceHolder();
                    syncUndoRedo();
                }).addEventListener("willPaste", willPaste);
        }

        function initButtons() {
            $btns = $("#" + id + "_buttons").find("button[data-action]");
            $btns.on("click",
                function() {
                    var action = $(this).attr("data-action");
                    switch (action) {
                    case "alignLeft":
                        setTextAlignment("left");
                        break;
                    case "alignJustify":
                        setTextAlignment("justify");
                        break;
                    case "alignCenter":
                        setTextAlignment("center");
                        break;
                    case "alignRight":
                        setTextAlignment("right");
                        break;
                    case "bold":
                        executeCommand(editor.hasFormat("b") ? "removeBold" : "bold");
                        break;
                    case "italic":
                        executeCommand(editor.hasFormat("i") ? "removeItalic" : "italic");
                        break;
                    case "underline":
                        executeCommand(editor.hasFormat("u") ? "removeUnderline" : "underline");
                        break;
                    case "makeUnorderedList":
                        executeCommand(editor.hasFormat("ul") ? "removeList" : "makeUnorderedList");
                        break;
                    case "undo":
                        executeCommand("undo");
                        syncPlaceholder();
                        syncUndoRedo();
                        break;
                    case "redo":
                        executeCommand("redo");
                        syncPlaceholder();
                        syncUndoRedo();
                        break;
                    }
                });
            $undo = $btns.find("button[data-action=undo]");
            $redo = $btns.find("button[data-action=redo]");
        }

        function syncUndoRedo() {
            if (editor._undoIndex !== 0) {
                $undo.addClass("active").removeClass("inactive");
            } else {
                $undo.addClass("inactive").removeClass("active");
            }
            if (editor._undoIndex + 1 < editor._undoStackLength && editor._isInUndoState) {
                $redo.addClass("active").removeClass("inactive");
            } else {
                $redo.addClass("inactive").removeClass("active");
            }
        }

        function willPaste(e) {
        }

        function setTextAlignment(alignment) {
            editor.setTextAlignment(alignment);
        }

        function executeCommand(command) {
            editor[command]();
        }

        function formattedText() {
            return $.trim($root.text());
        }

        function syncPlaceholder() {
            if (formattedText()) {
                $placeholder.addClass("display-none");
            } else {
                $placeholder.removeClass("display-none");
            }
        }

        function initPlaceHolder() {
            $placeholder.on("click", function() { editor.focus(); });
        }

        function checkAndShowPlaceHolder() {
            if (!formattedText()) {
                showPlaceHolder();
            }
        }

        function showPlaceHolder() {
            $placeholder.removeClass("display-none");
        }

        function hidePlaceHolder() {
            $placeholder.addClass("display-none");
        }

        function getAllChildNodes(node, index) {
            var children = [], index = 0;

            function traverseNode(node) {
                if (node) {
                    children.push({ "node": node, "index": index });
                    index++;
                    if (node.childNodes && node.childNodes.length) {
                        for (var i = 0, len = node.childNodes.length; i < len; i++) {
                            traverseNode(node.childNodes[i]);
                        }
                    }
                }
            }

            traverseNode(node);
            return children;
        }

        function getLastNonEmptyNode() {
            var $lastElement = null;
            var root = $root.get(0);
            if (root && root.childNodes && root.childNodes.length) {
                // Iterate only level 1 descendents
                $.each(root.childNodes,
                    function() {
                        var $this = $(this);
                        if ($.trim($this.text())) {
                            $lastElement = $this;
                        }
                    });
            }
            return $lastElement;
        }

        function sanitizeHtml($container) {
            var rootNode = $container.get(0);
            if (rootNode) {
                var children = getAllChildNodes(rootNode);
                if (children && children.length) {
                    var gotNonEmptyNode = false;
                    // The reverse loop for trimEnd functionality
                    for (var i = children.length - 1; i > -1; i--) {
                        var node = children[i].node;
                        var $node = $(node);
                        if (!gotNonEmptyNode && $.trim($node.text())) {
                            gotNonEmptyNode = true;
                        }
                        if (!gotNonEmptyNode || $node.is("br")) {
                            $node.remove();
                        }
                    }
                }
            }
        }

        function insertContent($content) {
            editor.saveUndoState();
            var $lastElement = getLastNonEmptyNode();
            if ($lastElement) {
                $lastElement.after($content);
            } else {
                $root.prepend($content);
            }
            hidePlaceHolder();
            syncUndoRedo();
        }

        this.Init = function() {
            $dv = $("#" + id);
            editor = new Squire($dv.get(0), { "blockTag": "p" });
            $root = $(editor.getRoot());
            $placeholder = $dv.prev(".placeholder");
            showPlaceHolder();
            initEditorEvents();
            initButtons();
            initPlaceHolder();
        };
        this.GetContent = function() {
            var html = editor.getHTML();
            if (html) {
                var $html = $("<div />").html(html);
                sanitizeHtml($html);
                return $html.html();
            }
            return null;
        };
        this.SetContent = function(content) {
            editor.setHTML(content);
            syncPlaceholder();
        };
        this.AddHtml = function(content) {
            insertContent($("<div>" + content + "</div>"));
        };
        this.AddParagraph = function(content) {
            insertContent($("<p>" + content + "</p>"));
        };
        this.AddList = function(content) {
            editor.saveUndoState();
            var $lastElement = getLastNonEmptyNode();
            if ($lastElement) {
                if ($lastElement.is("ul")) {
                    var $li = $("<li>" + content + "</li>");
                    $lastElement.append($li);
                } else {
                    var $ul = $("<ul><li>" + content + "</li></ul>");
                    $lastElement.after($ul);
                }
            } else {
                var $ul = $("<ul><li>" + content + "</li></ul>");
                $root.prepend($ul);
            }
            hidePlaceHolder();
            syncUndoRedo();
        };
    };

    function editorFactory() {
        var editors = {};
        this.Init = function(id, obj) {
            editors[id] = new squireEditor(id);
            editors[id].Init();
        };
        this.GetContent = function(id) {
            return editors[id].GetContent();
        };
        this.SetContent = function(id, content) {
            editors[id].SetContent(content);
        };
        this.AddParagraph = function(id, content) {
            editors[id].AddParagraph(content);
        };
        this.AddList = function(id, content) {
            editors[id].AddList(content);
        };
        this.AddHtml = function(id, content) {
            editors[id].AddHtml(content);
        };
    };

    Builder.Editor = new editorFactory();
})(window.jQuery, window.Builder);