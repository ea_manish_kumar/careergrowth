﻿using App.DomainModel;
using App.Model.Users;
using App.Services;
using App.Utility;
using App.Utility.Constants;
using Microsoft.AspNet.Identity;
using Microsoft.Owin.Security;
using System;
using System.Security.Claims;
using System.Threading.Tasks;
using System.Web;
using WebApp.Constants;
using WebApp.Extensions;

namespace WebApp.Managers
{
    public class SignInManager : ISignInManager
    {
        private readonly IUserService UserService;
        private readonly ITrackingService TrackingService;
        private readonly IAuthenticationManager AuthenticationManager;
        private readonly SignInOptions SignInOptions;
        private readonly string SessionCookie;

        public SignInManager(IUserService userService, ITrackingService trackingService, SignInOptions signInOptions)
        {
            UserService = userService;
            TrackingService = trackingService;
            SignInOptions = signInOptions;
            AuthenticationManager = HttpContext.Current?.GetOwinContext().Authentication;
            SessionCookie = HttpContext.Current?.Request.Cookies.Read(SiteCookies.SessionUID);
        }


        public async Task<bool> UserExists(string username)
        {
            return await UserService.UserExists(username);
        }

        public async Task<SignInResult> PasswordSignIn(string username, string password, bool isPersistent)
        {
            var user = await UserService.Get(username);
            if (user == null)
                return SignInResult.NotFound;
            if (!user.IsActive)
                return SignInResult.Deactivated;
            if (user.LockoutEndDateUTC.HasValue && user.LockoutEndDateUTC.Value > DateTime.UtcNow)
                return SignInResult.LockedOut;

            var passwordVerification = await VerifyPassword(user, password);
            if (!passwordVerification)
                return SignInResult.PasswordMismatch;

            await SignIn(user, isPersistent, UserLoginType.SignIn);
            return SignInResult.Success;
        }

        public async Task<SignInResult> RegisterUser(string name, string username, string password)
        {
            var signInResult = await PasswordSignIn(username, password, false);
            if (signInResult == SignInResult.NotFound)
            {
                var user = new User
                {
                    Name = name,
                    UserName = username,
                    Email = username,
                    HashedPassword = HashUtility.HashPassword(password),
                    SecurityStamp = Guid.NewGuid().ToString(),
                    RoleId = await UserService.GetRoleId(UserRoles.User)
                };
                await UserService.Save(user);
                await SignIn(user, false, UserLoginType.SignUp);
                return SignInResult.Success;
            }
            else
                return signInResult;
        }

        public async Task<UserLogin> SaveUserLogin(int userId, bool isPersistent, UserLoginType loginType)
        {
            var expiryTime = TimeSpan.FromMinutes(isPersistent ? SignInOptions.PersistentSessionExpireMinutes : SignInOptions.SessionExpireMinutes);
            var userLogin = await UserService.SaveUserLogin(userId, loginType, expiryTime);
            if (userLogin != null && Guid.TryParse(SessionCookie, out Guid sessionUID))
            {
                var sessionId = await TrackingService.GetSessionId(sessionUID);
                if (sessionId.HasValue && sessionId.Value > 0)
                {
                    await SaveUserLoginSession(userLogin.Id, sessionId.Value);
                }
            }
            return userLogin;
        }

        public async Task SaveUserLoginSession(int userLoginId, int sessionId)
        {
            await UserService.SaveUserLoginSession(userLoginId, sessionId);
        }

        public async Task<UserToken> GenerateUserToken(string username, UserTokenType tokenType)
        {
            var user = await UserService.Get(username);
            if (user != null)
            {
                var uid = Guid.NewGuid();
                var hashedToken = HashUtility.HashToken(uid.ToString());
                return await UserService.GenerateUserToken(user.Id, tokenType, TimeSpan.FromMinutes(SignInOptions.LockoutExpireMinutes), uid, hashedToken);
            }
            return null;
        }

        public async Task<bool> ResetPassword(Guid uid, string hashedToken, string newPassword)
        {
            var userToken = await UserService.GetUserToken(uid, hashedToken);
            if (userToken != null && userToken.User != null)
            {
                userToken.VerifiedOnUtc = DateTime.UtcNow;
                userToken.User.HashedPassword = HashUtility.HashPassword(newPassword.Trim());
                userToken.User.SecurityStamp = Guid.NewGuid().ToString();
                if (userToken.User.LockoutEndDateUTC.HasValue && userToken.User.LockoutEndDateUTC.Value > DateTime.UtcNow)
                {
                    userToken.User.LockoutEndDateUTC = null;
                    userToken.User.AccessFailedCount = null;
                }
                await UserService.SaveUserToken(userToken);
                return true;
            }
            return false;
        }

        public async Task<SignInResult> ChangePassword(int userId, string oldPassword, string newPassword)
        {
            var user = await UserService.Get(userId);
            if (user == null)
                return SignInResult.NotFound;
            if (!user.IsActive)
                return SignInResult.Deactivated;
            if (user.LockoutEndDateUTC.HasValue && user.LockoutEndDateUTC.Value > DateTime.UtcNow)
                return SignInResult.LockedOut;

            var passwordVerification = await VerifyPassword(user, oldPassword);
            if (!passwordVerification)
                return SignInResult.PasswordMismatch;

            user.HashedPassword = HashUtility.HashPassword(newPassword);
            user.SecurityStamp = Guid.NewGuid().ToString();
            await UserService.Update(user);

            await SignIn(user, false, UserLoginType.ChangePassword);

            return SignInResult.Success;
        }

        public async Task SignOut()
        {
            await ExpireCurrentUserLogin();
            AuthenticationManager.SignOut(DefaultAuthenticationTypes.ApplicationCookie);
        }

        private async Task SignIn(User user, bool isPersistent, UserLoginType loginType)
        {
            await ExpireCurrentUserLogin();
            AuthenticationManager.SignOut(DefaultAuthenticationTypes.ExternalCookie, DefaultAuthenticationTypes.TwoFactorCookie);

            var userIdentity = await CreateUserIdentity(user, isPersistent, loginType);
            if (isPersistent)
            {
                AuthenticationManager.SignIn(new AuthenticationProperties
                {
                    IsPersistent = isPersistent,
                    ExpiresUtc = DateTime.UtcNow.AddMinutes(SignInOptions.PersistentSessionExpireMinutes)
                }, userIdentity);
            }
            else
                AuthenticationManager.SignIn(userIdentity);
        }

        private async Task<ClaimsIdentity> CreateUserIdentity(User user, bool isPersistent, UserLoginType loginType)
        {
            var userLogin = await SaveUserLogin(user.Id, isPersistent, loginType);

            var userIdentity = new ClaimsIdentity(DefaultAuthenticationTypes.ApplicationCookie, CustomClaimTypes.UserId, CustomClaimTypes.Role);
            userIdentity.AddClaim(new Claim(CustomClaimTypes.UserId, user.Id.ToString()));
            userIdentity.AddClaim(new Claim(CustomClaimTypes.Role, user.RoleId.ToString()));
            userIdentity.AddClaim(new Claim(CustomClaimTypes.UserName, user.UserName));
            userIdentity.AddClaim(new Claim(CustomClaimTypes.DisplayName, user.Name));
            userIdentity.AddClaim(new Claim(CustomClaimTypes.SecurityStamp, user.SecurityStamp));
            if (userLogin != null)
            {
                userIdentity.AddClaim(new Claim(CustomClaimTypes.UserLoginId, userLogin.Id.ToString()));
                userIdentity.AddClaim(new Claim(CustomClaimTypes.UserLoginStamp, userLogin.UniqueStamp));
            }
            if (!string.IsNullOrEmpty(user.Name))
                userIdentity.AddClaim(new Claim(CustomClaimTypes.DisplayName, user.Name));
            return userIdentity;
        }

        private async Task ExpireCurrentUserLogin()
        {
            if (AuthenticationManager.User != null && AuthenticationManager.User.Identity != null && AuthenticationManager.User.Identity.IsAuthenticated)
            {
                if (AuthenticationManager.User.Identity is ClaimsIdentity claimsIdentity)
                {
                    var userLoginIdClaim = claimsIdentity?.FindFirstValue(CustomClaimTypes.UserLoginId);
                    var userLoginStampClaim = claimsIdentity?.FindFirstValue(CustomClaimTypes.UserLoginStamp);
                    if (int.TryParse(userLoginIdClaim, out int userLoginId) && userLoginId > 0 && !string.IsNullOrEmpty(userLoginStampClaim))
                        await UserService.ExpireUserLogin(userLoginId, userLoginStampClaim);
                }
            }
        }

        private async Task<bool> VerifyPassword(User user, string password)
        {
            if (!HashUtility.ValidatePassword(password, user.HashedPassword))
            {
                if (user.AccessFailedCount.HasValue)
                {
                    if (user.AccessFailedCount.Value >= SignInOptions.MaxPasswordFailedAttempts)
                        user.LockoutEndDateUTC = DateTime.UtcNow.AddMinutes(SignInOptions.LockoutExpireMinutes);
                    else
                        user.AccessFailedCount = user.AccessFailedCount.Value + 1;
                }
                else
                    user.AccessFailedCount = 1;
                await UserService.Update(user);
                return false;
            }
            return true;
        }
    }
}