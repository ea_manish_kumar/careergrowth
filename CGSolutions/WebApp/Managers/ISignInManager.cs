﻿using App.DomainModel;
using App.Model.Users;
using System;
using System.Threading.Tasks;

namespace WebApp.Managers
{
    public interface ISignInManager
    {
        Task<bool> UserExists(string username);
        Task<SignInResult> PasswordSignIn(string username, string password, bool isPersistent);
        Task<SignInResult> RegisterUser(string name, string username, string password);
        Task<UserLogin> SaveUserLogin(int userId, bool isPersistent, UserLoginType loginType);
        Task SaveUserLoginSession(int userLoginId, int sessionId);
        Task<UserToken> GenerateUserToken(string username, UserTokenType tokenType);
        Task<bool> ResetPassword(Guid uid, string hashedToken, string newPassword);
        Task<SignInResult> ChangePassword(int userId, string oldPassword, string newPassword);
        Task SignOut();
    }
}