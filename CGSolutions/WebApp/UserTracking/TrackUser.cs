﻿using System;
using System.Collections;
using System.Reflection;
using System.Web;
using CommonModules.Configuration;
using CommonModules.Exceptions;
using DomainModel.Core;
using Services.Core;
using Utilities;
using Utilities.Constants;

namespace WebApp.UserTracking
{
    public class TrackUser
    {
        public static string SessionId => CGUtility.ReadCookie(CookieConstants.SessionId);

        public static void Track()
        {
            var sessionId = CGUtility.ReadCookie(CookieConstants.SessionId);
            if (string.IsNullOrEmpty(sessionId))
            {
                sessionId = SaveSession();
                CGUtility.WriteCookie(CookieConstants.SessionId, sessionId, false);
                TrackBrowser(sessionId);
            }
            else
            {
                var browserId = CGUtility.ReadCookie(CookieConstants.BrowserId);
                if (string.IsNullOrEmpty(browserId))
                    TrackBrowser(sessionId); //LogInAsUser from CS Portal
            }
        }

        private static string SaveSession()
        {
            var sessionId = string.Empty;
            var sessionExtend = 0; //In Minutes
            int.TryParse(ConfigManager.GetConfig("SESSION_EXTEND"), out sessionExtend);
            if (sessionExtend == 0)
                sessionExtend = 30;
            var coreService = new CoreService();
            Session session = null;
            try
            {
                session = new Session();
                session.UserAgent = HttpContext.Current.Request.UserAgent;
                //session.VisitType = CheckVisitType();
                session.IPAddress = CGUtility.GetUserIPAddress();
                session.CreatedOn = DateTime.Now;
                //session.ExpiredOn = DateTime.Now.AddMinutes(sessionExtend);
                session.Source = CGUtility.GetUTMSource();
                session.Campaign = CGUtility.GetUTMCampaign();
                session.Medium = CGUtility.GetUTMMedium();
                session.Utm_Source = CGUtility.GetUnprocessedUTMSource();
                session.URL_Referrer = CGUtility.GetUrlReferrer();
                session.Requested_URL = CGUtility.GetFullRequestedURL();
                session.Utm_Term = CGUtility.GetUTMTerm();
                session.Utm_Keyword = CGUtility.GetUTMKeyword();
                session.Utm_Content = CGUtility.GetUTMContent();
                session.DomainCD = CGUtility.GetDomainCDFromConfig();
                //session.IsAuth = false;
                coreService.SaveSession(session);
                //this.AppSession = session.GetClone(); /// Clone is fetched to secure this object from getting saved via NHibernate.
                sessionId = session.Id.ToString();
            }
            catch (Exception ex)
            {
                var htParams = new Hashtable(1);
                htParams.Add("Session", session);
                var objMethod = (MethodInfo) MethodBase.GetCurrentMethod();
                GlobalExceptionHandler.customExceptionHandler(ex, objMethod, htParams, SubSystem.Core,
                    Application.WebApp, ErrorSeverityType.High, string.Empty, true);
            }

            return sessionId;
        }

        private static void TrackBrowser(string sessionId)
        {
            if (!string.IsNullOrEmpty(sessionId))
            {
                var browserId = CGUtility.ReadCookie(CookieConstants.BrowserId);
                if (string.IsNullOrEmpty(browserId))
                {
                    browserId = Guid.NewGuid().ToString();
                    CGUtility.WriteCookie(CookieConstants.BrowserId, browserId, true);
                }

                var coreService = new CoreService();
                coreService.SaveSessionExtended(new SessionExtended(new Guid(sessionId), new Guid(browserId)));
            }
        }
    }
}