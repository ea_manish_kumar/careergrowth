﻿using CommonModules.Configuration;
using CommonModules.Exceptions;
using DomainModel.User;
using Microsoft.AspNet.Identity;
using Microsoft.Owin.Security;
using Services.Communication;
using Services.User;
using Servicess.Tests;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Reflection;
using System.Security.Claims;
using System.Security.Principal;
using System.Text;
using System.Threading;
using System.Web;
using Utilities;
using Utilities.Constants;

namespace WebApp.UserTracking
{
    public static class UserOperations
    {
        private const string SignOut = "so";
        private const string TimeOut = "to";

        public static bool IsSignedIn
        {
            get
            {
                if (ClaimsPrincipal.Current != null && ClaimsPrincipal.Current.Identity != null)
                {
                    var identity = ClaimsPrincipal.Current.Identity;
                    return identity.IsAuthenticated
                           && identity is ClaimsIdentity
                           && ((ClaimsIdentity) identity).HasClaim(x =>
                               ClaimsType.Role.Equals(x.Type, StringComparison.OrdinalIgnoreCase) &&
                               Roles.User.Equals(x.Value, StringComparison.OrdinalIgnoreCase));
                }

                return false;
            }
        }

        public static bool IsAdminSignedIn => AdminIdentity != null;

        public static MyUserIdentity Identity
        {
            get
            {
                if (ClaimsPrincipal.Current != null)
                {
                    var identity = ClaimsPrincipal.Current.Identity;
                    if (identity != null && identity.IsAuthenticated)
                    {
                        var claimsIdentity = identity as ClaimsIdentity;
                        if (claimsIdentity != null)
                        {
                            var userRoles = new List<string> {Roles.User, Roles.Guest};
                            if (claimsIdentity.HasClaim(x =>
                                ClaimsType.Role.Equals(x.Type, StringComparison.OrdinalIgnoreCase) &&
                                userRoles.Contains(x.Value)))
                                return new MyUserIdentity(claimsIdentity);
                        }
                    }
                }

                return null;
            }
        }

        public static MyUserIdentity AdminIdentity
        {
            get
            {
                if (ClaimsPrincipal.Current != null)
                {
                    var identity = ClaimsPrincipal.Current.Identity;
                    if (identity != null && identity.IsAuthenticated)
                    {
                        var claimsIdentity = identity as ClaimsIdentity;
                        if (claimsIdentity != null)
                        {
                            var adminRoles = new List<string> {Roles.Admin, Roles.AdminWithoutReport};
                            if (claimsIdentity.HasClaim(x =>
                                ClaimsType.AdminRole.Equals(x.Type, StringComparison.OrdinalIgnoreCase) &&
                                adminRoles.Contains(x.Value)))
                                return new MyUserIdentity(claimsIdentity);
                        }
                    }
                }

                return null;
            }
        }

        public static bool IsRememberMe
        {
            get
            {
                var signedIn = CGUtility.ReadCookie(CookieConstants.SignedIn);
                return string.IsNullOrEmpty(signedIn) && IsSignedIn;
            }
        }

        public static bool IsSessionTimedOut
        {
            get
            {
                var signedIn = CGUtility.ReadCookie(CookieConstants.SignedIn);
                return !string.IsNullOrEmpty(signedIn) && !IsSignedIn;
            }
        }


        public static bool IsUserTimedOut => TimeOut.Equals(CGUtility.ReadCookie(CookieConstants.SignOutType),
            StringComparison.OrdinalIgnoreCase);

        public static bool IsUserSignedOut => SignOut.Equals(CGUtility.ReadCookie(CookieConstants.SignOutType),
            StringComparison.OrdinalIgnoreCase);

        public static IAuthenticationManager AuthenticationManager =>
            HttpContext.Current.GetOwinContext().Authentication;

        public static void GuestIdentitySignIn(int userId, int roleId, string authTypeCD)
        {
            var userAuthId = SaveUserAuth(userId, authTypeCD, DateTime.Now.AddYears(1));
            var sessionId = CGUtility.ReadCookie(CookieConstants.SessionId);
            Guid sessionGuid;
            Guid.TryParse(sessionId, out sessionGuid);
            SaveUserAuthSession(userAuthId, sessionGuid);

            CGUtility.WriteCookie(CookieConstants.LastUpdated, DateTime.UtcNow.Ticks.ToString(), false);
            CGUtility.DeleteCookie(CookieConstants.SignedIn);

            var role = GetUserRole(roleId);
            var identity = new MyUserIdentity(userId, role, userAuthId);
            AuthenticationManager.SignIn(new AuthenticationProperties {ExpiresUtc = DateTime.UtcNow.AddYears(1)},
                identity);
            Thread.CurrentPrincipal = new ClaimsPrincipal(identity);
            if (HttpContext.Current != null)
                HttpContext.Current.User = new ClaimsPrincipal(identity);
        }

        public static void IdentitySignIn(int userId, string username, string displayName, int roleId,
            string authTypeCD, bool rememberMe = false)
        {
            DateTime? expiredOn = null;
            if (rememberMe)
            {
                expiredOn = DateTime.Now;
            }
            else
            {
                double minutes;
                double.TryParse(ConfigManager.GetConfig("SESSION_EXTEND"), out minutes);
                expiredOn = DateTime.Now.AddMinutes(minutes);
            }

            var userAuthId = SaveUserAuth(userId, authTypeCD, expiredOn);
            var sessionId = CGUtility.ReadCookie(CookieConstants.SessionId);
            Guid sessionGuid;
            Guid.TryParse(sessionId, out sessionGuid);
            SaveUserAuthSession(userAuthId, sessionGuid);

            CGUtility.WriteCookie(CookieConstants.LastUpdated, DateTime.UtcNow.Ticks.ToString(), false);
            CGUtility.WriteCookie(CookieConstants.SignedIn, "Y", false);

            var role = GetUserRole(roleId);
            var identity = new MyUserIdentity(userId, username, displayName, role, userAuthId);
            if (rememberMe)
                AuthenticationManager.SignIn(
                    new AuthenticationProperties {IsPersistent = true, ExpiresUtc = DateTime.UtcNow.AddYears(1)},
                    identity);
            else
                AuthenticationManager.SignIn(identity);
            Thread.CurrentPrincipal = new ClaimsPrincipal(identity);
            if (HttpContext.Current != null)
                HttpContext.Current.User = new ClaimsPrincipal(identity);
        }

        public static void AdminIdentitySignIn(int userId, string username, int roleId, bool rememberMe = false)
        {
            DateTime? expiredOn = null;
            if (rememberMe)
            {
                expiredOn = DateTime.Now;
            }
            else
            {
                double minutes;
                double.TryParse(ConfigManager.GetConfig("SESSION_EXTEND"), out minutes);
                expiredOn = DateTime.Now.AddMinutes(minutes);
            }

            var userAuthId = SaveUserAuth(userId, UserAuthTypeCD.Admin, expiredOn);
            var adminRole = GetUserRole(roleId);
            var userRole = GetUserRole(UserConstants.User);

            CGUtility.WriteCookie(CookieConstants.LastUpdated, DateTime.UtcNow.Ticks.ToString(), false);
            CGUtility.WriteCookie(CookieConstants.SignedIn, "Y", false);

            var identity = new MyUserIdentity(userId, username, "Admin", userRole, userAuthId);
            identity.AddClaim(new Claim(ClaimsType.AdminRole, adminRole));
            identity.AddClaim(new Claim(ClaimsType.AdminId, userId.ToString()));
            if (rememberMe)
                AuthenticationManager.SignIn(
                    new AuthenticationProperties {IsPersistent = true, ExpiresUtc = DateTime.UtcNow.AddYears(1)},
                    identity);
            else
                AuthenticationManager.SignIn(identity);
            Thread.CurrentPrincipal = new ClaimsPrincipal(identity);
            if (HttpContext.Current != null)
                HttpContext.Current.User = new ClaimsPrincipal(identity);
        }

        public static void HandleRememberMeUser()
        {
            var sessionId = TrackUser.SessionId;
            if (string.IsNullOrEmpty(sessionId))
            {
                var identity = Identity;
                if (identity != null && identity.UserAuthId > 0)
                {
                    Guid sessionGuid;
                    if (Guid.TryParse(sessionId, out sessionGuid) && !Guid.Empty.Equals(sessionGuid))
                        SaveUserAuthSession(identity.UserAuthId, sessionGuid);
                }

                CGUtility.WriteCookie(CookieConstants.SignedIn, "Y", false);
                CGUtility.WriteCookie(CookieConstants.LastUpdated, DateTime.UtcNow.Ticks.ToString(), false);
            }
        }

        public static void HandleSessionTimeOut()
        {
            DeleteUserCookies();
            CGUtility.WriteCookie(CookieConstants.SignOutType, TimeOut, false);
        }

        public static void DeleteSignOutCookie()
        {
            CGUtility.DeleteCookie(CookieConstants.SignOutType);
        }

        public static void ImpersonateUser(int userId, int roleId)
        {
            //var userRole = GetUserRole(roleId);
            //var identity = AdminIdentity;
            //var adminId = identity.AdminId;
            //var adminRole = identity.AdminRole;
            //var impersonatedIdentity = new ResumeHelpIdentity(userId, userRole);
            //impersonatedIdentity.AddClaim(new Claim(ClaimsType.AdminId, adminId.ToString()));
            //impersonatedIdentity.AddClaim(new Claim(ClaimsType.AdminRole, adminRole));
            ////AuthenticationManager.SignOut(DefaultAuthenticationTypes.ApplicationCookie);
            //AuthenticationManager.SignIn(new AuthenticationProperties() { IsPersistent = false }, impersonatedIdentity);
        }

        public static void IdentitySignOut()
        {
            SignOutUserAuth();
            DeleteUserCookies();
            CGUtility.WriteCookie(CookieConstants.SignOutType, SignOut, false);
            AuthenticationManager.SignOut(DefaultAuthenticationTypes.ApplicationCookie);
            HttpContext.Current.User = new GenericPrincipal(new GenericIdentity(string.Empty), null);
        }

        public static void DeleteUserCookies()
        {
            CGUtility.DeleteCookie(CookieConstants.LastUpdated);
            CGUtility.DeleteCookie(CookieConstants.SignedIn);
            CGUtility.DeleteCookie(CookieConstants.DocumentCookie);
        }

        public static int RegisterGuestUser(string sessionId)
        {
            var userId = 0;
            UserMaster user = null;

            try
            {
                if (string.IsNullOrEmpty(sessionId))
                {
                    HttpContext.Current.Response.Redirect(CGUtility.GetDomainURLFromConfig());
                }
                else
                {
                    var userService = new UserService();
                    var portalID = CGUtility.GetPortalId();
                    user = userService.RegisterGuestUser(sessionId, portalID);
                    if (user != null)
                    {
                        var abTestService = new ABTestService();
                        abTestService.MoveSessionABTestInMainTable(user, sessionId);
                        userId = user.ID;
                        if (userId > 0)
                            GuestIdentitySignIn(userId, UserConstants.GuestUser, UserAuthTypeCD.Guest);
                        //userService.TestUserDataInsert(userId);
                        //var identity1 = HttpContext.Current.User.Identity as ResumeHelpIdentity;
                    }
                }
            }
            catch (Exception ex)
            {
                var ht = new Hashtable(3);
                ht.Add("SessionID", sessionId);
                ht.Add("GuestUser", user);
                var objMethod = (MethodInfo) MethodBase.GetCurrentMethod();
                GlobalExceptionHandler.customExceptionHandler(ex, objMethod, ht, ErrorSeverityType.Normal);
            }

            return userId;
        }

        public static bool IsAutoLoginViaEmail()
        {
            var uid = HttpContext.Current.Request.QueryString[QueryStringConstants.UserId];
            var eguid = HttpContext.Current.Request.QueryString[QueryStringConstants.EmailGUID];
            if (!string.IsNullOrEmpty(eguid) && !string.IsNullOrEmpty(uid))
            {
                var userId = 0;
                int.TryParse(uid, out userId);
                var userAlreadyLoggedIn = IsSignedIn && Identity.UserId == userId;
                if (!userAlreadyLoggedIn)
                {
                    Guid emailGuid;
                    Guid.TryParse(eguid, out emailGuid);
                    if (userId > 0 && emailGuid != default(Guid))
                    {
                        var userService = new UserService();
                        var userData = userService.GetUserDataByDataTypeCD(userId, UserDataType.Email_Guid);
                        if (userData != null && userData.Data == eguid)
                        {
                            var userPortalId = userService.GetPortalByUserID(userId);
                            var domainPortalId = CGUtility.GetPortalId();
                            if (domainPortalId == userPortalId)
                                return true;
                        }
                    }
                }
            }

            return false;
        }

        public static void DoAutoLogin()
        {
            int userId;
            var uid = HttpContext.Current.Request.QueryString[QueryStringConstants.UserId];
            int.TryParse(uid, out userId);
            if (userId > 0)
            {
                var userService = new UserService();
                var user = userService.GetUserByID(userId);
                if (user != null)
                {
                    IdentitySignIn(user.Id, user.UserName, GetDisplayName(user), UserConstants.User,
                        UserAuthTypeCD.AutoLogin);
                    SetUserLastLoginDate(userId);
                    TrackEmailLoginEvent();
                }
            }
        }

        private static void SetUserLastLoginDate(int userId)
        {
            var userService = new UserService();
            userService.SetUserLastLoginDate(userId);
        }

        private static void TrackEmailLoginEvent()
        {
            int linkId = 0, userId = 0;
            var campaignType = string.Empty;
            if (HttpContext.Current.Request.QueryString[QueryStringConstants.LinkId] != null)
                int.TryParse(HttpContext.Current.Request.QueryString["lid"], out linkId);
            if (HttpContext.Current.Request.QueryString[QueryStringConstants.UserId] != null)
                int.TryParse(HttpContext.Current.Request.QueryString["uid"], out userId);
            if (HttpContext.Current.Request.QueryString[QueryStringConstants.UtmCamapign] != null)
                campaignType = HttpContext.Current.Request.QueryString["utm_campaign"];

            //CoreService coreService = new CoreService();
            //string emailUserSessionData = new CoreService().GetFromSessionData(sessionId, SessionDataTypeConstants.EmailRegisteredUser); ;
            //if (String.IsNullOrEmpty(emailUserSessionData))
            //    oprocs.SetEmailUserSessionData(campaignType);
        }

        private static string GetUserRole(int roleId)
        {
            switch (roleId)
            {
                case UserConstants.GuestUser:
                    return Roles.Guest;
                case UserConstants.User:
                    return Roles.User;
                case UserConstants.AdminUser:
                    return Roles.Admin;
                case UserConstants.AdminUserWithoutReport:
                    return Roles.AdminWithoutReport;
                default:
                    return Roles.Guest;
            }
        }

        public static int GetUserRole(string role)
        {
            switch (role)
            {
                case Roles.Guest:
                    return UserConstants.GuestUser;
                case Roles.User:
                    return UserConstants.User;
                case Roles.Admin:
                    return UserConstants.AdminUser;
                case Roles.AdminWithoutReport:
                    return UserConstants.AdminUserWithoutReport;
                default:
                    return UserConstants.GuestUser;
            }
        }

        public static int SaveUserAuth(int userId, string authTypeCD, DateTime? expiredOn)
        {
            var userAuth = new UserAuth();
            userAuth.UserID = userId;
            userAuth.AuthTypeCD = authTypeCD;
            userAuth.ExpiredOn = expiredOn;
            var userService = new UserService();
            userService.SaveUserAuth(userAuth);
            return userAuth.Id;
        }

        public static int SaveUserAuthSession(int userAuthId, Guid sessionId)
        {
            var userAuthSession = new UserAuthSession();
            userAuthSession.UserAuthID = userAuthId;
            userAuthSession.SessionID = sessionId;
            var userService = new UserService();
            userService.SaveUserAuthSession(userAuthSession);
            return userAuthSession.Id;
        }

        public static void SignOutUserAuth()
        {
            var identity = Identity;
            if (identity != null && identity.UserAuthId > 0)
            {
                var userService = new UserService();
                userService.SignOutUserAuth(identity.UserAuthId, DateTime.Now);
            }
        }

        public static void RefreshUserAuth()
        {
            var lastUpdated = CGUtility.ReadCookie(CookieConstants.LastUpdated);
            if (!string.IsNullOrEmpty(lastUpdated))
            {
                long lastUpdatedTicks;
                if (long.TryParse(lastUpdated, out lastUpdatedTicks) && lastUpdatedTicks > 0)
                {
                    double minutes;
                    double.TryParse(ConfigManager.GetConfig("SESSION_EXTEND"), out minutes);
                    var currentUtc = DateTimeOffset.UtcNow;
                    var lastUpdatedUtc = new DateTimeOffset(lastUpdatedTicks, DateTimeOffset.UtcNow.Offset);
                    var expiresWindowUtc = lastUpdatedUtc.AddMinutes(minutes);
                    if (expiresWindowUtc >= currentUtc)
                    {
                        var timeRemaining = expiresWindowUtc.Subtract(currentUtc);
                        var timeElapsed = currentUtc.Subtract(lastUpdatedUtc);
                        if (timeRemaining < timeElapsed)
                        {
                            var identity = Identity;
                            if (identity != null && identity.UserAuthId > 0)
                            {
                                var userService = new UserService();
                                userService.RefreshUserAuth(identity.UserAuthId, DateTime.Now);
                                CGUtility.WriteCookie(CookieConstants.LastUpdated, currentUtc.UtcTicks.ToString(),
                                    false);
                            }
                        }
                    }
                }
            }
        }

        public static string GetDisplayName(UserMaster user)
        {
            var custName = new StringBuilder(string.Empty);
            if (!string.IsNullOrEmpty(user.FirstName))
            {
                custName.Append(user.FirstName.Trim());
                if (!string.IsNullOrEmpty(user.LastName))
                {
                    custName.Append(" ");
                    custName.Append(user.LastName.Trim());
                }
            }
            else if (!string.IsNullOrEmpty(user.LastName))
            {
                custName.Append(user.LastName.Trim());
            }

            return custName.ToString().Trim();
        }

        #region For impersonate/deimpersonate admin as User

        //public static void IdentitySignOut()
        //{
        //    if (Identity != null && Identity.UserImpersonation)
        //    {
        //        var adminIdentity = DeimpersonateAdmin();
        //        System.Threading.Thread.CurrentPrincipal = new ClaimsPrincipal(adminIdentity);
        //        if (HttpContext.Current != null)
        //            HttpContext.Current.User = new ClaimsPrincipal(adminIdentity);
        //    }
        //    else
        //    {
        //        AuthenticationManager.SignOut(DefaultAuthenticationTypes.ApplicationCookie);
        //        HttpContext.Current.User = new GenericPrincipal(new GenericIdentity(string.Empty), null);
        //    }
        //}

        //public static void ImpersonateUser(string userId, int roleId)
        //{
        //    var role = GetUserRole(roleId);
        //    var adminUserId = AdminIdentity.UserId;
        //    var adminUserRole = AdminIdentity.Role;
        //    var impersonatedIdentity = new ResumeHelpIdentity(userId, role);
        //    impersonatedIdentity.AddClaim(new Claim(ClaimsType.UserImpersonation, "True"));
        //    impersonatedIdentity.AddClaim(new Claim(ClaimsType.AdminId, adminUserId.ToString()));
        //    impersonatedIdentity.AddClaim(new Claim(ClaimsType.AdminRole, adminUserRole));
        //    //AuthenticationManager.SignOut(DefaultAuthenticationTypes.ApplicationCookie);
        //    AuthenticationManager.SignIn(new AuthenticationProperties() { IsPersistent = false }, impersonatedIdentity);
        //}

        //public static ResumeHelpIdentity DeimpersonateAdmin()
        //{
        //    var adminId = Identity.AdminId;
        //    var adminRole = Identity.AdminRole;
        //    var adminIdentity = new ResumeHelpIdentity(adminId.ToString(), adminRole);
        //    //AuthenticationManager.SignOut(DefaultAuthenticationTypes.ApplicationCookie);
        //    AuthenticationManager.SignIn(new AuthenticationProperties() { IsPersistent = false }, adminIdentity);
        //    return adminIdentity;
        //}

        #endregion
    }
}