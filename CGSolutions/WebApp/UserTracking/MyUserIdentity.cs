﻿using System.Security.Claims;
using System.Text;
using Microsoft.AspNet.Identity;
using Utilities.Constants;

namespace WebApp.UserTracking
{
    public class MyUserIdentity : ClaimsIdentity
    {
        public MyUserIdentity(ClaimsIdentity identity) : base(identity)
        {
        }

        public MyUserIdentity(int userId, string role, int userAuthId)
            : base(DefaultAuthenticationTypes.ApplicationCookie, ClaimsType.UserId, ClaimsType.Role)
        {
            AddClaim(new Claim(ClaimsType.UserId, userId.ToString()));
            AddClaim(new Claim(ClaimsType.Role, role));
        }

        public MyUserIdentity(int userId, string username, string displayName, string role, int userAuthId)
            : base(DefaultAuthenticationTypes.ApplicationCookie, ClaimsType.UserId, ClaimsType.Role)
        {
            AddClaim(new Claim(ClaimsType.UserId, userId.ToString()));
            AddClaim(new Claim(ClaimsType.UserName, username));
            AddClaim(new Claim(ClaimsType.DisplayName, displayName));
            AddClaim(new Claim(ClaimsType.Role, role));
            AddClaim(new Claim(ClaimsType.UserAuthId, userAuthId.ToString()));
        }

        public int UserId
        {
            get
            {
                var claim = GetClaimValue(ClaimsType.UserId);
                var userId = 0;
                if (int.TryParse(claim, out userId))
                    return userId;
                return userId;
            }
        }

        public string UserName => GetClaimValue(ClaimsType.UserName);

        public string DisplayName => GetClaimValue(ClaimsType.DisplayName);

        public string Role => GetClaimValue(ClaimsType.Role);

        public int UserAuthId
        {
            get
            {
                var claim = GetClaimValue(ClaimsType.UserAuthId);
                int userSessionId;
                if (int.TryParse(claim, out userSessionId))
                    return userSessionId;
                return userSessionId;
            }
        }

        public int AdminId
        {
            get
            {
                var claim = GetClaimValue(ClaimsType.AdminId);
                var adminId = 0;
                if (int.TryParse(claim, out adminId))
                    return adminId;
                return adminId;
            }
        }

        public string AdminRole => GetClaimValue(ClaimsType.AdminRole);

        private string GetClaimValue(string claimType)
        {
            var claim = FindFirst(claimType);
            if (claim != null) return claim.Value;
            return string.Empty;
        }

        #region override ToString()

        public override string ToString()
        {
            var builder = new StringBuilder(8);
            builder.AppendLine("--ResumeHelpIdentity starts--");
            builder.AppendLine("UserId: " + UserId);
            builder.AppendLine("Role: " + (Role ?? "<NULL>"));
            builder.AppendLine("AdminId: " + AdminId);
            builder.AppendLine("AdminRole: " + (AdminRole ?? "<NULL>"));
            builder.AppendLine("--ResumeHelpIdentity ends--");
            return builder.ToString();
        }

        #endregion
    }
}