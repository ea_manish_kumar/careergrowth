﻿using System;
using System.Collections.Generic;

namespace ResumeGenerator
{
    class ExamplesService
    {
        public List<string> GetRandomExamples(List<string> examples, int count = 3)
        {
            List<string> givenExamples = new List<string>(examples);
            int randomIndex = 0;
            List<string> randomExamples = null;
            if(givenExamples != null && givenExamples.Count > 0)
            {
                randomExamples = new List<string>();
                var random = new Random(DateTime.Now.Millisecond);
                for (int index = 0; index < count; index++)
                {
                    randomIndex = random.Next(givenExamples.Count);
                    randomExamples.Add(givenExamples[randomIndex]);
                    givenExamples.RemoveAt(randomIndex);
                }
            }
            return randomExamples;
        }
    }
}
