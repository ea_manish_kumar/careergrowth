﻿using RB3.DomainModel.Documents;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ResumeGenerator
{
    class Program
    {
        static void Main(string[] args)
        {
            var occupationsList = OnetData.AllOccupations;
            var jobTitlesList = OnetData.AllJobTitles;
            var examples = OnetData.AllExamples;
            var exampleService = new ExamplesService();
            foreach (var occupation in occupationsList)
            {
                var usedJobTitles = new List<string>();
                var experienceExamples = examples.Where(x => x.OccupationID == occupation.ID && x.SectionTypeID == (int)SectionType.Experience).Select(x => x.Text).Distinct().ToList();
                var skillsExamples = examples.Where(x => x.OccupationID == occupation.ID && x.SectionTypeID == (int)SectionType.Skills).Select(x => x.Text).Distinct().ToList();
                var educationExamples = examples.Where(x => x.OccupationID == 9999 && x.SectionTypeID == (int)SectionType.Education).Select(x => x.Text).Distinct().ToList();
                var jobTitlesByOccupationID = jobTitlesList.Where(x => x.OccupationID == occupation.ID && !usedJobTitles.Contains(x.Name)).Select(x => x.Name).ToList();
                var selectedTitles = exampleService.GetRandomExamples(jobTitlesByOccupationID);
                var usedExperienceExamples = new List<string>();
                var usedSkillsExamples = new List<string>();
                var usedEducationExamples = new List<string>();
                usedJobTitles.AddRange(selectedTitles);
                foreach (var jobTitle in selectedTitles) 
                {                    
                    var selectedExpExamples = experienceExamples.Where(x => !usedExperienceExamples.Contains(x)).ToList();
                    var randomExperienceExamples = exampleService.GetRandomExamples(selectedExpExamples);
                    usedExperienceExamples.AddRange(selectedExpExamples);
                    var selectedSkillExamples = skillsExamples.Where(x => !usedSkillsExamples.Contains(x)).ToList();
                    var randomSkillsExamples = exampleService.GetRandomExamples(selectedSkillExamples);
                    usedSkillsExamples.AddRange(selectedSkillExamples);
                    var selectedEducationExamples = educationExamples.Where(x => !usedEducationExamples.Contains(x)).ToList();
                    var randomEducationExamples = exampleService.GetRandomExamples(selectedEducationExamples);
                    usedEducationExamples.AddRange(selectedEducationExamples);
                    //var document = new Document();
                }
            }
            Console.ReadLine();
        }

    }
}
