﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;

namespace ResumeGenerator
{
    public class OnetData
    {
        static List<Occupation> occupations;

        static List<JobTitle> jobTitles;

        static List<Example> examples;

        public static List<Occupation> AllOccupations
        {
            get
            {
                if (occupations == null)
                    occupations = GetAllOccupations();
                return occupations;
            }
        }
        public static List<JobTitle> AllJobTitles
        {
            get
            {
                if (jobTitles == null)
                    jobTitles = GetAllJobTitles();
                return jobTitles;
            }
        }
        public static List<Example> AllExamples
        {
            get
            {
                if (examples == null)
                    examples = GetAllExamples();
                return examples;
            }
        }
        private static List<Occupation> GetAllOccupations()
        {
            List<Occupation> occupations = null;
            using (var connection = new SqlConnection(Properties.Settings.Default.ConnectionString))
            {
                connection.Open();
                var command = new SqlCommand("select OccupationID, Title from [dbo].[Occupations] order by 1", connection);
                using (var reader = command.ExecuteReader())
                {
                    if (reader.HasRows)
                    {
                        occupations = new List<Occupation>();
                        while (reader.Read())
                        {
                            var occupation = new Occupation() { ID = reader.GetInt32(0), Name = reader[1].ToString()};
                            occupations.Add(occupation);
                        }
                    }
                }
            }
            return occupations;
        }
        private static List<JobTitle> GetAllJobTitles()
        {
            List<JobTitle> jobTitles = null;
            using (var connection = new SqlConnection(Properties.Settings.Default.ConnectionString))
            {
                connection.Open();
                var command = new SqlCommand("select JobTitleID, JobTitle, OccupationID from [dbo].[JobTitles]", connection);
                using (var reader = command.ExecuteReader())
                {
                    if (reader.HasRows)
                    {
                        jobTitles = new List<JobTitle>();
                        while (reader.Read())
                        {
                            var jobTitle = new JobTitle() { ID = reader.GetInt32(0), Name = reader[1].ToString(), OccupationID = reader.GetInt32(2) };
                            jobTitles.Add(jobTitle);
                        }
                    }
                }
            }
            return jobTitles;
        }
        private static List<Example> GetAllExamples()
        {
            List<Example> examples = null;
            using (var connection = new SqlConnection(Properties.Settings.Default.ConnectionString))
            {
                connection.Open();
                var command = new SqlCommand("select OccupationID, SectionTypeID, Example, Content_Type from [dbo].[Content]", connection);
                using (var reader = command.ExecuteReader())
                {
                    if (reader.HasRows)
                    {
                        examples = new List<Example>();
                        while (reader.Read())
                        {
                            var example = new Example() { OccupationID = reader.GetInt32(0), SectionTypeID = reader.GetInt32(1), Text = reader[2].ToString(), ContentTypeID = reader.GetInt32(3) };
                            examples.Add(example);
                        }
                    }
                }
            }
            return examples;
        }

    }

    public class Occupation
    {
        public int ID { get; set; }
        public string Name { get; set; }
    }

    public class JobTitle
    {
        public int ID { get; set; }
        public string Name { get; set; }
        public int OccupationID { get; set; }
    }

    public class Example
    {
        public int SectionTypeID { get; set; }
        public int ContentTypeID { get; set; }
        public string Text { get; set; }
        public int OccupationID { get; set; }
    }

    public enum SectionType
    {
        Achievement = 1,
        Education,
        Experience,
        Skills,
        Summary
    }

}
