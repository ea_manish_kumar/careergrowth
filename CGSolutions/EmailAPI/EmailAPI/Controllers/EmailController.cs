﻿using Amazon;
using Amazon.SimpleEmail;
using Amazon.SimpleEmail.Model;
using AWSAPI.Model;
using System;
using System.Collections.Generic;
using System.Security.Cryptography;
using System.Text;
using System.Web.Http;

namespace AWSAPI.Controllers
{
    [Route("api/[controller]")]
    public class EmailController : System.Web.Mvc.Controller
    {
        // Replace sender@example.com with your "From" address.
        // This address must be verified with Amazon SES.
        static readonly string senderAddress = "sender@example.com";
        
        // The configuration set to use for this email. If you do not want to use a
        // configuration set, comment out the following property and the
        // ConfigurationSetName = configSet argument below. 
        static readonly string configSet = "ConfigSet";

        private bool ValidateHash(string hash, string email)
        {
            if (!string.IsNullOrEmpty(email))
                //Match the hash from the one sent from application
                if (hash == HashText(email))
                    return true;
            return false;
        }

        private void SendEmail(string recipientAddress, string subject, string replyToEmail, string htmlBody, string textBody )
        {
            // Replace USWest2 with the AWS Region you're using for Amazon SES.
            // Acceptable values are EUWest1, USEast1, and USWest2.

            //Reply to email address
            List<string> replyTo = new List<string>();
            if (!String.IsNullOrEmpty(replyToEmail))
                replyTo.Add(replyToEmail);
            else
                replyTo.Add(senderAddress);

            using (var client = new AmazonSimpleEmailServiceClient(RegionEndpoint.USWest2))
            {
                var sendRequest = new SendEmailRequest
                {
                    Source = senderAddress,
                    ReplyToAddresses = replyTo,
                    Destination = new Destination
                    {
                        ToAddresses =
                        new List<string> { recipientAddress }
                    },
                    Message = new Message
                    {
                        Subject = new Content(subject),
                        Body = new Body
                        {
                            Html = new Content
                            {
                                Charset = "UTF-8",
                                Data = htmlBody
                            },
                            Text = new Content
                            {
                                Charset = "UTF-8",
                                Data = textBody
                            }
                        }
                    },
                    // If you are not using a configuration set, comment
                    // or remove the following line 
                    ConfigurationSetName = configSet
                };
                try
                {
                    Console.WriteLine("Sending email using Amazon SES...");
                    var response = client.SendEmailAsync(sendRequest).Result;
                    VerifyEmailAddressRequest req = new VerifyEmailAddressRequest();
                    req.EmailAddress = "";

                    Console.WriteLine("The email was sent successfully.");
                }
                catch (Exception ex)
                {
                    Console.WriteLine("The email was not sent.");
                    Console.WriteLine("Error message: " + ex.Message);

                }
            }
        }

        public static string HashText(string text)
        {
            SHA512Managed hasher = new SHA512Managed();
            string salt = "W@e$lc0me@199916h@$&$#";
            byte[] textWithSaltBytes = Encoding.UTF8.GetBytes(string.Concat(text, salt));
            byte[] hashedBytes = hasher.ComputeHash(textWithSaltBytes);
            hasher.Clear();
            return Convert.ToBase64String(hashedBytes);
        }

        // POST: api/Email
        [HttpPost]
        [Route("WelcomeEmail")]
        public void Post([FromBody] WelcomeEmail model)
        {
            // The subject line for the email.
            string subject = "";

            // Replace recipient@example.com with a "To" address. If your account
            // is still in the sandbox, this address must be verified.
            if (ValidateHash(model.Hash, model.RecipientAddress))
            {

                // The email body for recipients with non-HTML email clients.
                string textBody = "Amazon SES Test (.NET)\r\n"
                                                + "This email was sent through Amazon SES "
                                                + "using the AWS SDK for .NET.";

                string htmlBody = "";

                //Call SendEmail with Parameters
                SendEmail(model.RecipientAddress, subject, model.SenderAddress, htmlBody, textBody);

                Console.Write("Press any key to continue...");
                Console.ReadKey();
            }
        }

        // POST: api/Email
        [HttpPost]
        [Route("ForgotEmail")]
        public void Post([FromBody] ForgotEmail model)
        {
            // The subject line for the email.
            string subject = "";

            // Replace recipient@example.com with a "To" address. If your account
            // is still in the sandbox, this address must be verified.
            if (ValidateHash(model.Hash, model.RecipientAddress))
            {

                // The email body for recipients with non-HTML email clients.
                string textBody = "Amazon SES Test (.NET)\r\n"
                                                + "This email was sent through Amazon SES "
                                                + "using the AWS SDK for .NET.";

                string htmlBody = "";

                //Call SendEmail with Parameters
                SendEmail(model.RecipientAddress, subject, model.SenderAddress, htmlBody, textBody);

                Console.Write("Press any key to continue...");
                Console.ReadKey();
            }
        }


        // POST: api/Email
        [HttpPost]
        [Route("CustomEmail")]
        public void Post([FromBody] CustomEmail model)
        {
            // The subject line for the email.
            string subject = "";

            // Replace recipient@example.com with a "To" address. If your account
            // is still in the sandbox, this address must be verified.
            if (ValidateHash(model.Hash, model.RecipientAddress))
            {
                //Call SendEmail with Parameters
                //TODO:Handle Attachments
                SendEmail(model.RecipientAddress, subject, model.SenderAddress, model.HTMLBody, model.TextBody);

                Console.Write("Press any key to continue...");
                Console.ReadKey();
            }
        }
    }
}
