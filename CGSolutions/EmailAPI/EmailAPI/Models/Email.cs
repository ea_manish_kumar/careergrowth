﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace AWSAPI.Model
{
    public class Email
    {
        public string RecipientAddress { get; set; }
        public string SenderAddress  { get; set;}
        public string Hash { get; set; }
    }

    public class WelcomeEmail:Email
    {
        public string Name { get; set; }
        public string DashboardLink { get; set; }
    }

    public class ForgotEmail : Email
    {
        public string Name { get; set; }
        public string PasswordLink { get; set; }
    }

    public class CustomEmail: Email
    {
        public string Recipient { get; set; }
        public string Sender { get; set; }
        public string Subject { get; set; }
        public string HTMLBody { get; set; }

        public string TextBody { get; set; }
        public byte[] File { get; set; } 
    }
}
