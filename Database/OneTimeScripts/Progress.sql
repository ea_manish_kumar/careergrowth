USE [TestDB]
GO
/****** Object:  Table [dbo].[Progress]    Script Date: 30-05-2019 19:40:29 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Progress](
	[ProgressID] [int] IDENTITY(1,1) NOT FOR REPLICATION NOT NULL,
	[ProgressCD] [char](6) NULL,
	[Definition] [nvarchar](50) NULL,
	[StageIndex] [tinyint] NULL,
	[DocumentTypeCD] [char](4) NULL,
 CONSTRAINT [PK_Progress] PRIMARY KEY CLUSTERED 
(
	[ProgressID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET IDENTITY_INSERT [dbo].[Progress] ON 

INSERT [dbo].[Progress] ([ProgressID], [ProgressCD], [Definition], [StageIndex], [DocumentTypeCD]) VALUES (1, N'HOME  ', N'Home Page', 1, N'RSME')
INSERT [dbo].[Progress] ([ProgressID], [ProgressCD], [Definition], [StageIndex], [DocumentTypeCD]) VALUES (2, N'JOURNY', N'User Journey', 2, N'RSME')
INSERT [dbo].[Progress] ([ProgressID], [ProgressCD], [Definition], [StageIndex], [DocumentTypeCD]) VALUES (3, N'FORMAT', N'Format Selected', 4, N'RSME')
INSERT [dbo].[Progress] ([ProgressID], [ProgressCD], [Definition], [StageIndex], [DocumentTypeCD]) VALUES (5, N'ACAD ', N'Academics', 5, N'RSME')
INSERT [dbo].[Progress] ([ProgressID], [ProgressCD], [Definition], [StageIndex], [DocumentTypeCD]) VALUES (6, N'USER  ', N'User Details', 3, N'RSME')
INSERT [dbo].[Progress] ([ProgressID], [ProgressCD], [Definition], [StageIndex], [DocumentTypeCD]) VALUES (8, N'EXPR  ', N'Work Experience', 7, N'RSME')
INSERT [dbo].[Progress] ([ProgressID], [ProgressCD], [Definition], [StageIndex], [DocumentTypeCD]) VALUES (9, N'COMPT ', N'Competencies', 9, N'RSME')
INSERT [dbo].[Progress] ([ProgressID], [ProgressCD], [Definition], [StageIndex], [DocumentTypeCD]) VALUES (12, N'PRVW  ', N'Preview', 11, N'RSME')
INSERT [dbo].[Progress] ([ProgressID], [ProgressCD], [Definition], [StageIndex], [DocumentTypeCD]) VALUES (13, N'PLANS ', N'Plans', 14, N'RSME')
INSERT [dbo].[Progress] ([ProgressID], [ProgressCD], [Definition], [StageIndex], [DocumentTypeCD]) VALUES (14, N'SUMMRY', N'Summary', 10, N'RSME')
INSERT [dbo].[Progress] ([ProgressID], [ProgressCD], [Definition], [StageIndex], [DocumentTypeCD]) VALUES (17, N'CHECK ', N'Checkout', 15, N'RSME')
INSERT [dbo].[Progress] ([ProgressID], [ProgressCD], [Definition], [StageIndex], [DocumentTypeCD]) VALUES (18, N'PREM  ', N'Premium User', 16, N'RSME')
INSERT [dbo].[Progress] ([ProgressID], [ProgressCD], [Definition], [StageIndex], [DocumentTypeCD]) VALUES (19, N'ACADS ', N'Academics Details', 6, N'RSME')
INSERT [dbo].[Progress] ([ProgressID], [ProgressCD], [Definition], [StageIndex], [DocumentTypeCD]) VALUES (21, N'EXPRS ', N'Experience Details', 8, N'RSME')
INSERT [dbo].[Progress] ([ProgressID], [ProgressCD], [Definition], [StageIndex], [DocumentTypeCD]) VALUES (24, N'DASHBD', N'Dashboard', 12, N'RSME')
INSERT [dbo].[Progress] ([ProgressID], [ProgressCD], [Definition], [StageIndex], [DocumentTypeCD]) VALUES (25, N'CSTM', N'Custom Section', 13, N'RSME')
SET IDENTITY_INSERT [dbo].[Progress] OFF
