USE [TestDB]
GO

/****** Object:  Table [dbo].[ProgressHistory]    Script Date: 30-05-2019 20:04:00 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE TABLE [dbo].[ProgressHistory](
	[UserID] [int] NOT NULL,
	[PreferenceCD] [varchar](4) NULL,
	[Value] [varchar](100) NULL,
	[DateTime] [datetime] NULL,
PRIMARY KEY CLUSTERED 
(
	[UserID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO


